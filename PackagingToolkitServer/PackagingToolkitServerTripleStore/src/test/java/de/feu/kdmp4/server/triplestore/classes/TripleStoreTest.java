package de.feu.kdmp4.server.triplestore.classes;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.junit.Ignore;
import org.junit.Test;

/**
 * This class isnt a traditional test class. It reads the content of the triple store and prints it on the 
 * standard output. If the test is started after every action in the PackagingToolkit that should concern
 * the triple store, you can compare the content of the triple store with the expected content.
 * @author Christopher Olbertz
 *
 */
public class TripleStoreTest {
	private Repository repository;
	private File repositoryFile;
	
	/**
	 * The absolute path to the directory that contains the data of the triple store.
	 */
	private static final String PATH_TO_DATA_DIRECTORY = "/home/christopher/Dokumente/Studium/Masterarbeit/workspace/PackagingToolkitServer/"
			+ "PackagingToolkitServerApplication/work/data";
	
	@Test
	public void printTripleStoreContent() {
		initialize();
		try (RepositoryConnection connection = repository.getConnection();) {
			RepositoryResult<Statement> statements = connection.getStatements(null, null, null);
			List<Statement> statementsAsList = Iterations.asList(statements);
			
			for (Statement statement: statementsAsList) {
				System.out.println(statement.getSubject().toString() + " ------ " + statement.getPredicate().stringValue() 
						+ " --------- " + statement.getObject().stringValue());
			}

		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void initialize() {
		repositoryFile = new File(PATH_TO_DATA_DIRECTORY);
		repository = new SailRepository(new NativeStore(repositoryFile));
		repository.initialize();
		
		boolean directoryExists = repositoryFile.exists();
		assertTrue(directoryExists);
	}
	
	@Test
	@Ignore
	public void deleteSomeStatements() {
		initialize();
		
		try (RepositoryConnection connection = repository.getConnection();) {
			ValueFactory valueFactory = SimpleValueFactory.getInstance();
			IRI iriOfPrediate = valueFactory.createIRI("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasIntegerProperty");
			IRI iriOfSubject = valueFactory.createIRI("http://de.feu.kdmp4.pkgtkt#8166bdc0-c3dd-45a6-b3a5-9ed938883398");
			
			RepositoryResult<Statement> statements = connection.getStatements(iriOfSubject, iriOfPrediate, null);
			List<Statement> statementsAsList = Iterations.asList(statements);
			
			for (int i = 1; i < statementsAsList.size(); i++) { 
				Statement statement = statementsAsList.get(i);
				connection.remove(statement);
			}
			
			
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
