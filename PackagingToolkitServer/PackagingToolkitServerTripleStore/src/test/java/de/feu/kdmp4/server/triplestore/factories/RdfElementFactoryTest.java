package de.feu.kdmp4.server.triplestore.factories;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;

public class RdfElementFactoryTest {
	private static final String NAMESPACE_PACKACKING_TOOLKIT = "http://de.feu.kdmp4.pkgtkt";
	private static final String PREDICATE_FILE_REFERENCE = "hasFileReference";
	private static final String PREDICATE_REMOTE_REFERENCE = "hasRemoteReference";
	
	@Test
	public void testCreateStorageFileStatement() {
		Uuid uuidOfInformationPackage = new Uuid();
		Uuid uuidOfFile = new Uuid();
		TripleStatement tripleStatement = RdfElementFactory.createStorageFileStatement(uuidOfInformationPackage, uuidOfFile);
		
		TripleSubject tripleSubject = tripleStatement.getSubject();
		TriplePredicate triplePredicate = tripleStatement.getPredicate();
		TripleObject tripleObject = tripleStatement.getObject();
		
		assertThat(tripleSubject.getIri(), is(equalTo(uuidOfInformationPackage.toString())));
		assertThat(tripleObject.getValue().toString(), is(equalTo(uuidOfFile.toString())));
		assertThat(triplePredicate.getLocalName(), is(equalTo(PREDICATE_FILE_REFERENCE)));
		assertThat(triplePredicate.getNamespace(), is(equalTo(NAMESPACE_PACKACKING_TOOLKIT)));
	}
	
	@Test
	public void testCreateRemoteFileStatement() {
		Uuid uuidOfInformationPackage = new Uuid();
		String urlOfFile = "http://www.my-file.com";
		TripleStatement tripleStatement = RdfElementFactory.createRemoteFileStatement(uuidOfInformationPackage, urlOfFile);
		
		TripleSubject tripleSubject = tripleStatement.getSubject();
		TriplePredicate triplePredicate = tripleStatement.getPredicate();
		TripleObject tripleObject = tripleStatement.getObject();
		
		assertThat(tripleSubject.getIri(), is(equalTo(uuidOfInformationPackage.toString())));
		assertThat(tripleObject.getValue().toString(), is(equalTo(urlOfFile)));
		assertThat(triplePredicate.getLocalName().toString(), is(equalTo(PREDICATE_REMOTE_REFERENCE)));
		assertThat(triplePredicate.getNamespace(), is(equalTo(NAMESPACE_PACKACKING_TOOLKIT)));
	}

}