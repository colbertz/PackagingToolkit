package de.feu.kdmp4.server.triplestore.classes;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TriplePredicateImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleSubjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreOperations;

public class TripleStoreOperationsTest {
	@Rule
	public TemporaryFolder temporaryFold = new TemporaryFolder();
	private TripleStoreOperations tripleStoreOperations;
	
	@Before
	public void setUp() {
		tripleStoreOperations = new TripleStoreOperationsRdf4JImpl();
	}
	
	/**
	 * Creates an iri for the aggregatedBy predicate.
	 * @return The iri.
	 */
	private IRI createAggregatedByIri() {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		TriplePredicate aggregatedByPredicate = RdfElementFactory.createAggregatedByPredicate();
		Iri aggregatedByPredicateAsString = aggregatedByPredicate.getIri();
		IRI iriOfAggregatedByPredicate = valueFactory.createIRI(aggregatedByPredicateAsString.toString());
		return iriOfAggregatedByPredicate;
	}
	
	/**
	 * Creates an iri for the type predicate.
	 * @return The iri.
	 */
	private IRI createTypeIri() {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		TriplePredicate typePredicate = RdfElementFactory.createTypePredicate();
		Iri typePredicateAsString = typePredicate.getIri();
		IRI iriOfTypePredicate = valueFactory.createIRI(typePredicateAsString.toString());
		return iriOfTypePredicate;
	}
	
	// ************************************************
	// testSaveInTripleStore()
	// ************************************************
	@Test
	public void testSaveInTripleStore() throws IOException {
		WorkingDirectory workingDirectory = prepareTest_testSaveInTripleStore_workingDirectory();
		TripleStatementCollection tripleStatements = prepareTest_testSaveInTripleStore_tripleStatementList();
		tripleStoreOperations.addTriplesToStore(tripleStatements, workingDirectory);
		
		File dataDirectory = workingDirectory.getDataDirectory();
		assertTrue(dataDirectory.exists());
		assertTrue(dataDirectory.isDirectory());
		File[] filesInDataDirectory = dataDirectory.listFiles();
		assertThat(filesInDataDirectory.length, is(not(0)));
	}
	
	/**
	 * Prepares a working directory for the test.
	 * @return A working directory that is used during the test.
	 * @throws IOException 
	 */
	private WorkingDirectory prepareTest_testSaveInTripleStore_workingDirectory() throws IOException {
		File workingDirectoryFile = temporaryFold.newFolder("work");
		WorkingDirectory workingDirectory = new WorkingDirectoryImpl(workingDirectoryFile.getAbsolutePath());
		return workingDirectory;
	}
	
	private TripleStatementCollection prepareTest_testSaveInTripleStore_tripleStatementList() {
		TripleStatementCollection tripleStatements = new TripleStatementCollectionImpl();
		prepareTest_testSaveInTripleStore_addFirstStatement(tripleStatements);
		prepareTest_testSaveInTripleStore_addSecondStatement(tripleStatements);
		prepareTest_testSaveInTripleStore_addThirdStatement(tripleStatements);
		
		return tripleStatements;
	}
	
	private void prepareTest_testSaveInTripleStore_addFirstStatement(TripleStatementCollection tripleStatements) {
		TripleSubject subject = new TripleSubjectImpl("http://abc#def");
		TriplePredicate predicate = new TriplePredicateImpl("http://abc#predicate");
		TripleObject object = new TripleObjectImpl(12);
		
		TripleStatement tripleStatement = new TripleStatementImpl(subject, predicate, object);
		tripleStatements.addTripleStatement(tripleStatement);
	}
	
	private void prepareTest_testSaveInTripleStore_addSecondStatement(TripleStatementCollection tripleStatements) {
		TripleSubject subject = new TripleSubjectImpl("http://abc#xyz");
		TriplePredicate predicate = new TriplePredicateImpl("http://abc#predicate");
		TripleObject object = new TripleObjectImpl(1);
		
		TripleStatement tripleStatement = new TripleStatementImpl(subject, predicate, object);
		tripleStatements.addTripleStatement(tripleStatement);
	}
	
	private void prepareTest_testSaveInTripleStore_addThirdStatement(TripleStatementCollection tripleStatements) {
		TripleSubject subject = new TripleSubjectImpl("http://abc#def");
		TriplePredicate predicate = new TriplePredicateImpl("http://abc#anotherPredicate");
		TripleObject object = new TripleObjectImpl(-6);
		
		TripleStatement tripleStatement = new TripleStatementImpl(subject, predicate, object);
		tripleStatements.addTripleStatement(tripleStatement);
	}
	
	// ************************************************
	// testDeleteAllTriplesOfInformationPackage()
	// ************************************************
	@Test
	public void testDeleteAllTriplesOfInformationPackage() throws IOException {
		Model model = new LinkedHashModel();
		Uuid uuidOfInformationPackage = prepareTest_testDeleteAllTriplesOfInformationPackage_createTwoInformationPackages(model);
		File dataDirectory = prepareTest_testDeleteAllTriplesOfInformationPackage_saveModel(model);
		WorkingDirectory workingDirectory = new WorkingDirectoryImpl(dataDirectory.getAbsolutePath());
		
		tripleStoreOperations.deleteAllTriplesOfInformationPackage(uuidOfInformationPackage, workingDirectory);
		checkResults_testDeleteAllTriplesOfInformationPackage(workingDirectory, uuidOfInformationPackage);
	}
	
	private Uuid prepareTest_testDeleteAllTriplesOfInformationPackage_createTwoInformationPackages(Model model) {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		
		IRI iriOfAggregatedByPredicate = createAggregatedByIri();
		Uuid uuidOfInformationPackageToDelete = null; 
		
		for (int  i = 0; i < 2; i++) {
			Uuid uuidOfInformationPackage = new Uuid(); 
			if (i == 0) {
				uuidOfInformationPackageToDelete = uuidOfInformationPackage;
			}
			Iri informationPackageIri = ServerModelFactory.createIri(uuidOfInformationPackage);
			IRI iriOfInformationPackage = valueFactory.createIRI(informationPackageIri.toString());
			
			for (int j = 0; j < 2; j++) {
				Uuid uuidOfIndividual = new Uuid();
				Iri individualIri = ServerModelFactory.createIri(uuidOfIndividual);
				IRI iriOfIndividual = valueFactory.createIRI(individualIri.toString());
				Statement aggregatedByStatement = valueFactory.createStatement(iriOfIndividual, iriOfAggregatedByPredicate, 
						iriOfInformationPackage);
				model.add(aggregatedByStatement);
				for (int k = 0; k < 2; k++) {
					Iri propertyIri = ServerModelFactory.createIri("property" + i + j + k);
					IRI iriOfProperty = valueFactory.createIRI(propertyIri.toString());
					Literal propertyValue = valueFactory.createLiteral(k);
					Statement propertyStatement = valueFactory.createStatement(iriOfIndividual, iriOfProperty, propertyValue);
					model.add(propertyStatement);
				}
			}
		}
		return uuidOfInformationPackageToDelete;
	}
	
	private File prepareTest_testDeleteAllTriplesOfInformationPackage_saveModel(Model model) throws IOException {
		File workingDirectory = temporaryFold.newFolder();
		File dataDirectory = new File(workingDirectory, "data");
		dataDirectory.mkdir();
		
		Repository repository = new SailRepository(new NativeStore(dataDirectory));
		repository.initialize();

		try (RepositoryConnection connection = repository.getConnection();) {
			connection.add(model);
			RepositoryResult<Statement> repositoryResult = connection.getStatements(null, null, null);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
		
		return workingDirectory;
	}
	
	private void checkResults_testDeleteAllTriplesOfInformationPackage(WorkingDirectory workingDirectory, Uuid uuidOfInformationPackage) {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		File dataDirectory = workingDirectory.getDataDirectory();
		Repository repository = new SailRepository(new NativeStore(dataDirectory));
		repository.initialize();
		
		try (RepositoryConnection connection = repository.getConnection();) {
			Iri informationPackageIri = ServerModelFactory.createIri(uuidOfInformationPackage);
			IRI iriOfInformationPackage = valueFactory.createIRI(informationPackageIri.toString());
			RepositoryResult<Statement> statementsWithInformationPackageAsSubject = connection.getStatements(iriOfInformationPackage, null, null);
			assertFalse(statementsWithInformationPackageAsSubject.hasNext());
			RepositoryResult<Statement> statementsWithInformationPackageAsObject = connection.getStatements(null, null, iriOfInformationPackage);
			assertFalse(statementsWithInformationPackageAsObject.hasNext());
			RepositoryResult<Statement> allStatements = connection.getStatements(null, null, null);
			assertThat(allStatements.asList().size(), is(equalTo(6)));
			
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
	}

	// ************************************************
	// testFindTriplesByInformationPackageAndClass()
	// ************************************************
	@Test
	public void testFindTriplesByInformationPackageAndClass() throws IOException {
		Model model = new LinkedHashModel();
		List<IRI> irisOfInformationPackages = prepareTest_testFindTriplesByInformationPackageAndClass_createTwoInformationPackages();
		List<IRI> irisOfIndividuals = prepareTest_testFindTriplesByInformationPackageAndClass_createIndividuals(
				irisOfInformationPackages, model);
		List<IRI> irisOfClasses = prepareTest_testFindTriplesByInformationPackageAndClass_createClasses(irisOfIndividuals, model);
		IRI iriOfClassToLookFor = irisOfClasses.get(0);
		IRI iriOfInformationPackageToLookFor = irisOfInformationPackages.get(0);
		
		File dataDirectory = prepareTest_testDeleteAllTriplesOfInformationPackage_saveModel(model);
		WorkingDirectory workingDirectory = new WorkingDirectoryImpl(dataDirectory.getAbsolutePath());
		Uuid uuidOfInformationPackageToLookFor = new Uuid(iriOfInformationPackageToLookFor.getLocalName());
		
		
		TripleStatementCollection tripleStatementList = tripleStoreOperations.findTriplesByInformationPackageAndClass(workingDirectory, 
				uuidOfInformationPackageToLookFor, iriOfClassToLookFor.stringValue());
		
		assertThat(tripleStatementList.getTripleStatementsCount(), is(equalTo(4)));
	}
	
	/**
	 * Creates some individuals that are contained in the information packages given by the parameter. The 
	 * individuals contain some properties.
	 * @param irisOfInformationPackages The uuids of the information packages.
	 * @param model The model the statements are entered in.
	 * @return A List with the iris of the newly created individuals.
	 */
	private List<IRI> prepareTest_testFindTriplesByInformationPackageAndClass_createIndividuals(
			List<IRI> irisOfInformationPackages, Model model) {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		List<IRI> irisOfIndividuals = new ArrayList<>();
		
		for (IRI aIriOfInformationPackage: irisOfInformationPackages) {
			for (int j = 0; j < 3; j++) {
				Uuid uuidOfIndividual = new Uuid();
				Iri individualIri = ServerModelFactory.createIri(uuidOfIndividual);
				IRI iriOfIndividual = valueFactory.createIRI(individualIri.toString());
				IRI iriOfAggregatedByPredicate = createAggregatedByIri();
				Statement aggregatedByStatement = valueFactory.createStatement(iriOfIndividual, iriOfAggregatedByPredicate, 
						aIriOfInformationPackage);
				model.add(aggregatedByStatement);
				for (int k = 0; k < 2; k++) {
					Iri propertyIri = ServerModelFactory.createIri("property" + j + k);
					IRI iriOfProperty = valueFactory.createIRI(propertyIri.toString());
					Literal propertyValue = valueFactory.createLiteral(k);
					Statement propertyStatement = valueFactory.createStatement(iriOfIndividual, iriOfProperty, propertyValue);
					model.add(propertyStatement);
				}
				irisOfIndividuals.add(iriOfIndividual);
			}
		}
		
		return irisOfIndividuals;
	}
	
	/**
	 * Creates some classes for the individuals. 
	 * @param irisOfIndividuals The iris of the individuals we want to assign a class to.
	 * @param model The model that contains the statements.
	 * @return A list with the IRIS of the newly created classes.
	 */
	private List<IRI> prepareTest_testFindTriplesByInformationPackageAndClass_createClasses(
			List<IRI> irisOfIndividuals, Model model) {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		List<IRI> irisOfClasses = new ArrayList<>();
		
		for(int i = 0; i < irisOfIndividuals.size(); i++) {
			IRI aIriOfIndividual = irisOfIndividuals.get(i);
			
			Iri classIri = null;
			if (i % 2 == 0) {
				classIri = ServerModelFactory.createIri("Class1");
			} else {
				classIri = ServerModelFactory.createIri("Class2");
			}
			
			IRI iriOfClass = valueFactory.createIRI(classIri.toString());
			irisOfClasses.add(iriOfClass);
			IRI iriOfTypePredicate = createTypeIri();
			Statement typeStatement = valueFactory.createStatement(aIriOfIndividual, iriOfTypePredicate, iriOfClass);
			model.add(typeStatement);
		}
		
		return irisOfClasses;
	}
	
	/**
	 * Creates the iris for two information packages for the test.
	 * @return The iris of the created information packages.
	 */
	private List<IRI> prepareTest_testFindTriplesByInformationPackageAndClass_createTwoInformationPackages() {
		List<IRI> irisOfInformationPackages = new ArrayList<IRI>();
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		
		for (int  i = 0; i < 2; i++) {
			Uuid uuidOfInformationPackage = new Uuid();
			Iri informationPackageIri = ServerModelFactory.createIri(uuidOfInformationPackage);
			IRI iriOfInformationPackage = valueFactory.createIRI(informationPackageIri.toString());

			irisOfInformationPackages.add(iriOfInformationPackage);
		}
		return irisOfInformationPackages;
	}
	
	// ************************************************
	// testFindTriplesByInformationPackage()
	// ************************************************
	@Test
	public void testFindTriplesByInformationPackage() throws IOException {
		Model model = new LinkedHashModel();
		List<IRI> irisOfInformationPackages = prepareTest_testFindTriplesByInformationPackage_createTwoInformationPackages();
		IRI iriOfInformationPackageToLookFor = irisOfInformationPackages.get(0);
		List<IRI> irisOfIndividuals = prepareTest_testFindTriplesByInformationPackage_createIndividuals(
				irisOfInformationPackages, model);
		prepareTest_testFindTriplesByInformationPackage_createClasses(irisOfIndividuals, model);
		
		File dataDirectory = prepareTest_testDeleteAllTriplesOfInformationPackage_saveModel(model);
		WorkingDirectory workingDirectory = new WorkingDirectoryImpl(dataDirectory.getAbsolutePath());
		Uuid uuidOfInformationPackageToLookFor = new Uuid(iriOfInformationPackageToLookFor.getLocalName());
		
		TripleStatementCollection tripleStatementList = tripleStoreOperations.findTriplesByInformationPackage(workingDirectory, 
				uuidOfInformationPackageToLookFor);
		
		assertThat(tripleStatementList.getTripleStatementsCount(), is(equalTo(3)));
	}
	
	/**
	 * Creates some classes for the individuals. 
	 * @param irisOfIndividuals The iris of the individuals we want to assign a class to.
	 * @param model The model that contains the statements.
	 */
	private void prepareTest_testFindTriplesByInformationPackage_createClasses(
			List<IRI> irisOfIndividuals, Model model) {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		
		for(int i = 0; i < irisOfIndividuals.size(); i++) {
			IRI aIriOfIndividual = irisOfIndividuals.get(i);
			
			Iri classIri = null;
			if (i % 2 == 0) {
				classIri = ServerModelFactory.createIri("Class1");
			} else {
				classIri = ServerModelFactory.createIri("Class2");
			}
			
			IRI iriOfClass = valueFactory.createIRI(classIri.toString());
			IRI iriOfTypePredicate = createTypeIri();
			Statement typeStatement = valueFactory.createStatement(aIriOfIndividual, iriOfTypePredicate, iriOfClass);
			model.add(typeStatement);
		}
	}
	
	/**
	 * Creates some individuals that are contained in the information packages given by the parameter. The 
	 * individuals contain some properties.
	 * @param irisOfInformationPackages The uuids of the information packages.
	 * @param model The model the statements are entered in.
	 * @return A List with the iris of the newly created individuals.
	 */
	private List<IRI> prepareTest_testFindTriplesByInformationPackage_createIndividuals(
			List<IRI> irisOfInformationPackages, Model model) {
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		List<IRI> irisOfIndividuals = new ArrayList<>();
		
		for (IRI aIriOfInformationPackage: irisOfInformationPackages) {
			for (int j = 0; j < 3; j++) {
				Uuid uuidOfIndividual = new Uuid();
				Iri individualIri = ServerModelFactory.createIri(uuidOfIndividual);
				IRI iriOfIndividual = valueFactory.createIRI(individualIri.toString());
				IRI iriOfAggregatedByPredicate = createAggregatedByIri();
				Statement aggregatedByStatement = valueFactory.createStatement(iriOfIndividual, iriOfAggregatedByPredicate, 
						aIriOfInformationPackage);
				model.add(aggregatedByStatement);
				for (int k = 0; k < 2; k++) {
					Iri propertyIri = ServerModelFactory.createIri("property" + j + k);
					IRI iriOfProperty = valueFactory.createIRI(propertyIri.toString());
					Literal propertyValue = valueFactory.createLiteral(k);
					Statement propertyStatement = valueFactory.createStatement(iriOfIndividual, iriOfProperty, propertyValue);
					model.add(propertyStatement);
				}
				irisOfIndividuals.add(iriOfIndividual);
			}
		}
		
		return irisOfIndividuals;
	}
	
	/**
	 * Creates the iris for two information packages for the test.
	 * @return The iris of the created information packages.
	 */
	private List<IRI> prepareTest_testFindTriplesByInformationPackage_createTwoInformationPackages() {
		List<IRI> irisOfInformationPackages = new ArrayList<IRI>();
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		
		for (int  i = 0; i < 2; i++) {
			Uuid uuidOfInformationPackage = new Uuid();
			Iri informationPackageIri = ServerModelFactory.createIri(uuidOfInformationPackage);
			IRI iriOfInformationPackage = valueFactory.createIRI(informationPackageIri.toString());
			irisOfInformationPackages.add(iriOfInformationPackage);
		}
		return irisOfInformationPackages;
	}
}
