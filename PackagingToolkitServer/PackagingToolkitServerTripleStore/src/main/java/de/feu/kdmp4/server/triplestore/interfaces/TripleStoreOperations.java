package de.feu.kdmp4.server.triplestore.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * Contains the atomar operations of the module TripleStore. This interface is implemented with a concrete
 * technology for storing triple in a triple store. 
 * @author Christopher Olbertz
 *
 */
public interface TripleStoreOperations {
	/**
	 * Determines all tripel for a given information package and ontology class from the triple store.
	 * @param workingDirectory The working directory of the server.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @param iriOfClass The iri of the class whose individuals we are looking for.
	 * @return All triples of the information package with the uuid uuidOfInformationPackage and with 
	 * individuals of the desired class. 
	 */
	TripleStatementCollection findTriplesByInformationPackageAndClass(final WorkingDirectory workingDirectory, final Uuid uuidOfInformationPackage, final String iriOfClass);
	/**
	 * Adds triple to the data of an information package. If there are no existing data to this information package, an
	 * information package is created in the triple store. 
	 * @param tripleStatementList The list with the statements that have to be inserted into the triple store.
	 * @param workingDirectory The working directory that contains the data of the triple store. 
	 */
	void addTriplesToStore(final TripleStatementCollection tripleStatementList, final WorkingDirectory workingDirectory);
	/**
	 * Deletes the information about one information package from the triple store. Deletes are all statements that describe
	 * the properties of the individuals of the information package and the statements that describe the aggregatedBy
	 * relationship between an information package and its individuals. 
	 * @param uuidOfInformationPackage The uuid of the information package that should be deleted.
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 */
	void deleteAllTriplesOfInformationPackage(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory);
	/**
	 * Determines all tripel for a given information package from the triple store.
	 * @param workingDirectory The working directory of the server.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return All triples of the information package with the uuid uuidOfInformationPackage.. 
	 */
	TripleStatementCollection findTriplesByInformationPackage(final WorkingDirectory workingDirectory,
			final Uuid uuidOfInformationPackage);
	/**
	 * Finds all triples that describe an individual in the triple store. These tripels are describing the properties of this
	 * individual. 
	 * @param workingDirectory The working directory of the server that contains the triple store.
	 * @param uuidOfIndividual The uuid of the individual whose properties we are looking for.
	 * @return All statements that describe the properties of this individual.
	 */
	TripleStatementCollection findTriplesByIndividual(final WorkingDirectory workingDirectory, final Uuid uuidOfIndividual);
	/**
	 * Determines the datatype of a property with the help of the triple store. In the triple store there is a tripel for
	 * every property. This tripel has the form: property hasDatatype datatype. The tripel for the given property is read
	 * from the tripel store and the datatype is determined.
	 * @param workingDirectory The working directory of the server that contains the triple store.
	 * @param propertyIri The iri of the property and the starting basis for the search in the triple store.
	 * @return The datatype determined from the properties in the triple store.
	 */
	Optional<OntologyDatatype> determineDatatypeOfProperty(final WorkingDirectory workingDirectory, final String propertyIri);
	/**
	 * Checks if a statement exists in the triple store. The statement is searched with the help of the subject and 
	 * the predicate. The object is not relevant. Because of the use of unique uris every statement is unique in
	 * the tripel store. 
	 * @param tripleSubject The subject of the statement we are looking for.
	 * @param triplePredicate The predicate of the statement we are looking for.
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 * @return True if the statement has been found, false otherwise.
	 */
	boolean statementExists(final TripleSubject tripleSubject, final TriplePredicate triplePredicate,
			final WorkingDirectory workingDirectory);
	/**
	 * Determines the datatype of a property from the triple store. The predicate of the relationship we are looking for is
	 * hasDatatype and the object of the hasDatatype statement is the datatype.
	 * @param iriOfProperty The iri of the property whose datatype we are looking for.
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 * @return The statement that contains the information about the datatype.
	 */
	Optional<TripleStatement> findDatatypeOfProperty(final String iriOfProperty, final WorkingDirectory workingDirectory);
	/**
	 * Checks if an information package exist in the triple store.
	 * @param uuidOfInformationPackage The uuid of the information package we are looking for.
	 * @param workingDirectory The working directory that contains the directory with the data of the triple store.
	 * @return True if the information package exists in the triple store false otherwise.
	 */
	boolean informationPackageExists(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory);
	/**
	 * Checks if an individual exist in the triple store.
	 * @param uuidOfIndividual The uuid of the individual we are looking for.
	 * @param workingDirectory The working directory that contains the directory with the data of the triple store.
	 * @return True if the individual exists in the triple store false otherwise.
	 */
	boolean individualExists(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory);
	/**
	 * Deletes the information about one individual from the triple store.  
	 * @param uuidOfIndividual The uuid of the individual that should be deleted.
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 */
	void deleteAllTriplesOfIndividual(final Uuid uuidOfIndividual, final WorkingDirectory workingDirectory);
	/**
	 * Finds all triples that describe individuals of a certain class. 
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 * @param iriOfClass The iri of the class whose individuals we are looking for.
	 * @return All triples that describe the individuals of this class.
	 */
	TripleStatementCollection findTriplesByClass(final WorkingDirectory workingDirectory, final Iri iriOfClass);
	/**
	 * Deletes all triples that are related to an information package from the triple store. At the moment these are the 
	 * aggregatedBy triples that contains the relation to the individuals and the triples that contain the references
	 * to the digital objects.
	 * @param uuidOfInformationPackage The uuid of the information package that should be deleted.
	 * @param workingDirectory The working directory that contains the data of the triple store. 
	 */
	// DELETE_ME
	//void deleteAllTripleOfInformationPackage(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory);
	/**
	 * Determines the class of an individual from the triple store. The type predicate is used
	 * for determining the class. 
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 * @param iriOfIndividual The iri of the individual whose class we want to determine.
	 * @return The iri of class of the individual as string or an empty string if the iri of the individual was not
	 * found in the triple store.
	 */
	String findOntologyClassOfIndividual(WorkingDirectory workingDirectory, Iri iriOfIndividual);
	/**
	 * Deletes all aggregated-by statements that have a certain information package as object.
	 * @param uuidOfInformationPackage The uuid of the information we want to delete all
	 * aggregated-by statements from.
	 * @param workingDirectory The directory that contains the data of the triple store.  
	 */
	void unassignAllIndividualsFromInformationPackage(Uuid uuidOfInformationPackage, WorkingDirectory workingDirectory);
	/**
	 * Creates a statement that describes that an individual in a taxonomy has been selected by the user. 
	 * @param taxonomyIndividualIri The iri of the individual in a taxonomy has been selected by the user.
	 * @param iriOfInformationPackage The iri of the information package the user has selected the individuals
	 * in. 
	 * @param workingDirectory The directory that contains the data of the triple store.
	 */
	void createSelectTaxonomyIndividualStatement(String taxonomyIndividualIri, String iriOfInformationPackage,
			WorkingDirectory workingDirectory);
	/**
	 * Deletes all triples from the triple store that describe the assignment of taxonomy individuals to
	 * an information package. 
	 * @param uuidOfInformationPackage The information package whose taxonomy individual assignment we want
	 * to delete. 
	 * @param workingDirectory The directory that contains the data of the triple store.
	 */
	void deleteAllTaxomonyIndividualsOfInformationPackage(Uuid uuidOfInformationPackage,
			WorkingDirectory workingDirectory);
	/**
	 * Finds all triples in the triple store with a certain subject, predicate and object. All parameters may
	 * be null. If a parameter is null, it is not considered in the search. If all parameters are null, all
	 * statements in the triple store are found.  
	 * @param tripleSubject The subject of the statements we are looking for. May be null.
	 * @param triplePredicate The predicate of the statements we are looking for. May be null.
	 * @param tripleObject The object of the statements we are looking for. May be null.
	 * @param workingDirectory The directory that contains the data of the triple store.
	 * @return The statements found in the triple store with subject, predicate and object. 
	 */
	TripleStatementCollection findAllTriplesBySubjectPredicatObject(TripleSubject tripleSubject,
			TriplePredicate triplePredicate, TripleObject tripleObject, WorkingDirectory workingDirectory);
}