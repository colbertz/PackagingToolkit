package de.feu.kdmp4.server.triplestore.classes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreOperations;

public class TripleStoreOperationsRdf4JImpl implements TripleStoreOperations {
	/**
	 * The value factory of RDF4J creates all objects needed for the work with RDF4J
	 */
	private ValueFactory valueFactory;

	/**
	 * Initializes the value factory. 
	 */
	public TripleStoreOperationsRdf4JImpl() {
		 valueFactory = SimpleValueFactory.getInstance();
	}
	
	@Override
	public void createSelectTaxonomyIndividualStatement(final String taxonomyIndividualIri, 
														final String iriOfInformationPackage,
														final WorkingDirectory workingDirectory) {
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(taxonomyIndividualIri);
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(iriOfInformationPackage);
		final TriplePredicate predicate = RdfElementFactory.createTaxonomyIndividualIsSelectedPredicate();
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		final TripleStatement statement = RdfElementFactory.createStatement(tripleSubject, predicate, tripleObject);
		tripleStatements.addTripleStatement(statement);
		addTriplesToStore(tripleStatements, workingDirectory);
	}
	
	@Override
	public boolean statementExists(final TripleSubject tripleSubject, final TriplePredicate triplePredicate, final WorkingDirectory workingDirectory) {
		final String iriOfSubjectAsString = tripleSubject.getIri().toString();
		final IRI iriOfSubject = valueFactory.createIRI(iriOfSubjectAsString);
		
		final String iriOfPredicateAsString = triplePredicate.getIri().toString();
		final IRI iriOfPredicate = valueFactory.createIRI(iriOfPredicateAsString);
		return statementsExists(iriOfSubject, iriOfPredicate, null, workingDirectory);
	}
	
	@Override
	public Optional<OntologyDatatype> determineDatatypeOfProperty(final WorkingDirectory workingDirectory, final String propertyIri) {
		final Repository repository = initializeRepository(workingDirectory);
		final TriplePredicate datatypePredicate = RdfElementFactory.createHasDatatypePredicate();
		final IRI hasDatatypeIri = valueFactory.createIRI(datatypePredicate.getIri().toString());
		final IRI iriOfProperty = valueFactory.createIRI(propertyIri);
		
		try (RepositoryConnection connection = repository.getConnection();) {
			final RepositoryResult<Statement> statementsOfDatatype = connection.getStatements(iriOfProperty, hasDatatypeIri, null);
			final List<Statement> hasDatatypeStatements = Iterations.asList(statementsOfDatatype);
			if (!hasDatatypeStatements.isEmpty()) {
				final Statement datatypeStatement = hasDatatypeStatements.get(0);
				final String datatypeIri = datatypeStatement.getObject().stringValue();
				final OntologyDatatype ontologyDatatype = OntologyDatatype.getDatatypeByIri(datatypeIri);
				return ServerOptionalFactory.createOptionalWithOntologyDatatype(ontologyDatatype);
			}
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			repository.shutDown();
		}
		
		return ServerOptionalFactory.createEmptyOptionalWithOntologyDatatype();
	}
	
	/**
	 * Checks if statements exist in the triple store.
	 * @param iriOfSubject The iri of the subject of the statement to look for. May be null if the subject is not considered in the search
	 * @param iriOfPredicate The iri of the predicate of the statement to look for. May be null if the predicate is not considered in the search
	 * @param iriOfObject The iri of the object of the statement to look for. May be null if the object is not considered in the search
	 * @param workingDirectory The working directory that contains the directory with the data of the triple store.
	 * @return True if statements that satisfy the conditions exist in the triple store.
	 */
	private boolean statementsExists(final IRI iriOfSubject, final IRI iriOfPredicate, final IRI iriOfObject, final WorkingDirectory workingDirectory) {
		final Repository repository = initializeRepository(workingDirectory);
		
		try (RepositoryConnection connection = repository.getConnection();) {
			final RepositoryResult<Statement> statements = connection.getStatements(iriOfSubject, iriOfPredicate, iriOfObject);
			if (statements.hasNext()) {
				return true;
			} else {
				return false;
			}
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			repository.shutDown();
		}
	}
	
	@Override
	public boolean informationPackageExists(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory) {
		final Repository repository = initializeRepository(workingDirectory);
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		
		try (RepositoryConnection connection = repository.getConnection();) {
			final Iri iri = ServerModelFactory.createIri(uuidOfInformationPackage);
			final IRI iriOfInformationPackage = valueFactory.createIRI(iri.toString());
			final RepositoryResult<Statement> statements = connection.getStatements(null, null, iriOfInformationPackage);
			final boolean notEmpty = statements.hasNext();
			return notEmpty;
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			repository.shutDown();
		}
	}
	
	@Override
	public boolean individualExists(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory) {
		final Repository repository = initializeRepository(workingDirectory);
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		
		try (RepositoryConnection connection = repository.getConnection();) {
			final Iri iri = ServerModelFactory.createIri(uuidOfInformationPackage);
			final IRI iriOfIndividual = valueFactory.createIRI(iri.toString());
			final RepositoryResult<Statement> statements = connection.getStatements(iriOfIndividual, null, null);
			final boolean notEmpty = statements.hasNext();
			return notEmpty;
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			repository.shutDown();
		}
	}
	
	@Override
	public TripleStatementCollection findAllTriplesBySubjectPredicatObject(final TripleSubject tripleSubject, 
			final TriplePredicate triplePredicate, final TripleObject tripleObject, final WorkingDirectory workingDirectory) {
		final Repository repository = initializeRepository(workingDirectory);
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final TripleStatementCollection resultTripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		try (RepositoryConnection connection = repository.getConnection();) {
			IRI iriOfSubject = null;
			if (tripleSubject != null) {
				iriOfSubject = valueFactory.createIRI(tripleSubject.getIri().toString()); 
			}
			
			IRI iriOfPredicate = null;
			if (triplePredicate != null) {
				iriOfPredicate = valueFactory.createIRI(triplePredicate.getIri().toString()); 
			}
			
			IRI iriOfObject = null;
			if (tripleObject != null) {
				iriOfObject = valueFactory.createIRI(tripleObject.getIri().toString()); 
			}
			
			final RepositoryResult<Statement> statements = connection.getStatements(iriOfSubject, iriOfPredicate, iriOfObject);
			resultTripleStatements.addStatements(convertModelToTripleStatements(statements));
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
		
		return resultTripleStatements;
	}
	
	/**
	 * Converts the elements in a RepositoryResult object that have been found in the triple store in a 
	 * list with objects of TripleStatements. 
	 * @param repositoryResult Contains the statements found in the triple store.
	 * @return Contains the statements an TripleStatement objects.
	 */
	private TripleStatementCollection convertModelToTripleStatements(final RepositoryResult<Statement> repositoryResult) {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		final List<Statement> statementsAsList = Iterations.asList(repositoryResult);
		
		for (final Statement statement: statementsAsList) {
			final Resource resultSubject = statement.getSubject();
			final IRI resultPredidacateIri = statement.getPredicate();
			final Value resultObject = statement.getObject();
			final TripleSubject resultTripleSubject = RdfElementFactory.createTripleSubject(resultSubject.stringValue());
			final TriplePredicate resultTriplePredicate = RdfElementFactory.createTriplePredicate(resultPredidacateIri.stringValue(), null);
			final TripleObject resultTripleObject = RdfElementFactory.createTripleObject(resultObject.stringValue());
			final TripleStatement tripleStatement = RdfElementFactory.createStatement(resultTripleSubject, 
					resultTriplePredicate, resultTripleObject);
			tripleStatements.addTripleStatement(tripleStatement);
		}
		
		return tripleStatements;
	}

	@Override
	public void addTriplesToStore(final TripleStatementCollection tripleStatementList, final WorkingDirectory workingDirectory) {
		final Model model = convertTripleStatementsToModel(tripleStatementList);
		final Repository repository = initializeRepository(workingDirectory);
			
		try (RepositoryConnection connection = repository.getConnection();) {
			connection.add(model);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
	}
	
	@Override
	public void deleteAllTriplesOfInformationPackage(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory) {
		final Iri iri = ServerModelFactory.createIri(uuidOfInformationPackage);
		final IRI iriOfInformationPackage = valueFactory.createIRI(iri.toString());
		
		final Repository repository = initializeRepository(workingDirectory);
		try (final RepositoryConnection connection = repository.getConnection();) {
			/*final List<Statement> aggregationStatementsAsList = determineAggregationOfInformationPackage(connection, iriOfInformationPackage);
			
			for (final Statement statement: aggregationStatementsAsList) {
				deleteStatementsOfIndividual(connection, statement, iriOfInformationPackage);
			}*/
			deleteAggregatedByStatements(iriOfInformationPackage, connection);
			deleteReferenceStatementsOf(iriOfInformationPackage, connection);
			
			//connection.remove(aggregationStatementsAsList);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
	}
	
	@Override
	public void deleteAllTaxomonyIndividualsOfInformationPackage(final Uuid uuidOfInformationPackage, 
																 final WorkingDirectory workingDirectory) {
		final Iri iri = ServerModelFactory.createIri(uuidOfInformationPackage);
		final IRI iriOfInformationPackage = valueFactory.createIRI(iri.toString());
		final String iriOfTaxonomyIndividualPredicateAsString = RdfElementFactory.createTaxonomyIndividualIsSelectedPredicate().toString();
		final IRI iriOfTaxonomyIndividualPredicate = valueFactory.createIRI(iriOfTaxonomyIndividualPredicateAsString);
		
		final Repository repository = initializeRepository(workingDirectory);
		try (final RepositoryConnection connection = repository.getConnection();) {
			final RepositoryResult<Statement> aggregatedByStatement = connection.getStatements(null, iriOfTaxonomyIndividualPredicate, 
					iriOfInformationPackage);
			final List<Statement> statementsOfInformationPackageAsList = Iterations.asList(aggregatedByStatement);
			
			for (final Statement statement: statementsOfInformationPackageAsList) {
				connection.remove(statement);
			}	
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
	}
	
	@Override
	public void deleteAllTriplesOfIndividual(final Uuid uuidOfIndividual, final WorkingDirectory workingDirectory) {
		final Iri iri = ServerModelFactory.createIri(uuidOfIndividual);
		
		final IRI iriOfIndividual = valueFactory.createIRI(iri.toString());
		
		final Repository repository = initializeRepository(workingDirectory);
		try (final RepositoryConnection connection = repository.getConnection();) {
			final RepositoryResult<Statement> statementsOfIndividual = connection.getStatements(iriOfIndividual, null, null);
			final List<Statement> statementsOfIndividualAsList = Iterations.asList(statementsOfIndividual);
			
			for (final Statement statement: statementsOfIndividualAsList) {
				connection.remove(statement);
			}
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
	}
	
	//REMOVE_ME
	/*@Override
	public void deleteAllTripleOfInformationPackage(final Uuid uuidOfInformationPackage, final WorkingDirectory workingDirectory) {
		final Iri iri = ServerModelFactory.createIri(uuidOfInformationPackage);
		final IRI iriOfInformationPackage = valueFactory.createIRI(iri.toString());
		final Repository repository = initializeRepository(workingDirectory);
		
		try (final RepositoryConnection connection = repository.getConnection();) {
			deleteAggregatedByStatements(iriOfInformationPackage, connection);
			deleteReferenceStatementsOf(iriOfInformationPackage, connection);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
	}*/
	
	/**
	 * Deletes the statements of an information package that aggregate the individuals of this information package. 
	 * @param iriOfInformationPackage The iri of the information package whose aggregatedBy statements should be deleted.
	 * @param connection The connection to the triple store.
	 */
	private void deleteAggregatedByStatements(final IRI iriOfInformationPackage , final RepositoryConnection connection) {
		final RepositoryResult<Statement> aggregatedByStatement = connection.getStatements(null, null, iriOfInformationPackage);
		final List<Statement> statementsOfInformationPackageAsList = Iterations.asList(aggregatedByStatement);
		
		for (final Statement statement: statementsOfInformationPackageAsList) {
			connection.remove(statement);
		}		
	}
	
	/**
	 * Deletes the statements of an information package that reference to a digital object. 
	 * @param iriOfInformationPackage The iri of the information package whose references should be deleted.
	 * @param connection The connection to the triple store.
	 */
	private void deleteReferenceStatementsOf(final IRI iriOfInformationPackage, final RepositoryConnection connection) {
		final RepositoryResult<Statement> referenceStatements = connection.getStatements(iriOfInformationPackage, null, null);
		final List<Statement> referenceStatementsAsList = Iterations.asList(referenceStatements);
		
		for (final Statement statement: referenceStatementsAsList) {
			connection.remove(statement);
		}
	}
	
	/**
	 * Determines the statements from the triple store that describe the aggregation of an information package. This means the 
	 * statements that say which individuals are assigned to a given information package.
	 * @param connection The connection to the repository.
	 * @param iriOfInformationPackage The iri of the information package whose aggregatedBy statements we are looking for.
	 * @return The aggregatedBy statements of this information package.
	 */
	// REMOVE_ME
	private List<Statement> determineAggregationOfInformationPackage(final RepositoryConnection connection, final IRI iriOfInformationPackage) {
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final TriplePredicate aggregatedByPredicate = RdfElementFactory.createAggregatedByPredicate();
		final Iri iri = aggregatedByPredicate.getIri();
		final IRI iriOfaggregatedByProperty = valueFactory.createIRI(iri.toString());
		
		final RepositoryResult<Statement> aggregationOfInformationPackage = connection.getStatements(null, iriOfaggregatedByProperty, iriOfInformationPackage);
		final List<Statement> aggregationStatementsAsList = Iterations.asList(aggregationOfInformationPackage);
		return aggregationStatementsAsList;
	}
	
	/**
	 * Deletes the statements that describe the properties of one individual. 
	 * @param connection The connection to the repository.
	 * @param aggregatedByStatement The statement that describes the assignment of the information package.
	 * @param iriOfInformationPackage The iri of the information package that contains the individual that should be deleted.
	 */
	// REMOVE_ME
	private void deleteStatementsOfIndividual(final RepositoryConnection connection, final Statement aggregatedByStatement, final IRI iriOfInformationPackage) {
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final Resource uuidOfIndividualResource = aggregatedByStatement.getSubject();
		final String individualAsString = uuidOfIndividualResource.stringValue();
		final IRI iriOfIndividual = valueFactory.createIRI(individualAsString);
		
		final RepositoryResult<Statement> individualStatements = connection.getStatements(iriOfIndividual, null, null);
		final List<Statement> individualStatementList = Iterations.asList(individualStatements);
		
		for (final Statement statement: individualStatementList) {
			connection.remove(statement);
		}
	}
	
	/**
	 * Converts the triple statements that are independent from RDF4J into a RDF4J model. 
	 * @param tripleStatementList The list with the statement that are used for coReousenvertion.
	 * @return The RDF4J independent model object that contains the statements.
	 */
	private Model convertTripleStatementsToModel(final TripleStatementCollection tripleStatementList) {
		Model model = convertDataPropertiesToModel(tripleStatementList);
		model = convertObjectPropertiesToModel(tripleStatementList, model);
		return model;
	}
	
	/**
	 * Creates a model for RDF4J and adds statements that represent the datatype properties of
	 * some individuals. 
	 * @param tripleStatements Contains the statements that represent the properties of the
	 * individuals.
	 * @return The model that contains the converted statements.
	 */
	private Model convertDataPropertiesToModel(final TripleStatementCollection tripleStatements) {
		final Model model  = new LinkedHashModel();
		
		while(tripleStatements.hasNextLiteralStatement()) {
			final TripleStatement tripleStatement = tripleStatements.getNextLiteralStatement();
			final TripleSubject tripleSubject = tripleStatement.getSubject();
			final Iri subjectIri = tripleSubject.getIri();
			final IRI iriOfSubject = valueFactory.createIRI(subjectIri.toString());
			
			final TriplePredicate triplePredicate = tripleStatement.getPredicate();
			final Iri predicateIri = triplePredicate.getIri();
			final IRI iriOfPredicate = valueFactory.createIRI(predicateIri.toString());
			
			final TripleObject tripleObject = tripleStatement.getObject();
			final Literal literalOfObject = valueFactory.createLiteral(tripleObject.getValue().toString());
		
			final Statement statement = valueFactory.createStatement(iriOfSubject, iriOfPredicate, literalOfObject);
			model.add(statement);
		}
		
		return model;
	}
	
	/**
	 * Creates a model for RDF4J and adds statements that represent the object properties of
	 * some individuals. 
	 * @param tripleStatementList Contains the statements that represent the properties of the
	 * individuals.
	 * @param model The model that already contains the statements for the datatype properties. The 
	 * statements for the object properties are inserted into this object.
	 * @return The model that contains the converted statements.
	 */
	private Model convertObjectPropertiesToModel(final TripleStatementCollection tripleStatementList, final Model model) {
		while(tripleStatementList.hasNextNotLiteralStatement()) {
			final TripleStatement tripleStatement = tripleStatementList.getNextNotLiteralStatement();
			final TripleSubject tripleSubject = tripleStatement.getSubject();
			final Iri subjectIri = tripleSubject.getIri();
			final IRI iriOfSubject = valueFactory.createIRI(subjectIri.toString());
			
			final TriplePredicate triplePredicate = tripleStatement.getPredicate();
			final Iri predicateIri = triplePredicate.getIri();
			final IRI iriOfPredicate = valueFactory.createIRI(predicateIri.toString());
			
			final TripleObject tripleObject = tripleStatement.getObject();
			final String iriOfObjectAsStrig = tripleObject.getNamespace() + PackagingToolkitConstants.NAMESPACE_SEPARATOR + tripleObject.getLocalName();
			final IRI iriOfObject = valueFactory.createIRI(iriOfObjectAsStrig);
		
			final Statement statement = valueFactory.createStatement(iriOfSubject, iriOfPredicate, iriOfObject);
			model.add(statement);
		}
		
		return model;
	}
	
	@Override
	public TripleStatementCollection findTriplesByInformationPackageAndClass(final WorkingDirectory workingDirectory, final Uuid uuidOfInformationPackage, 
			String iriOfClass) {
		return findTriplesInStore(workingDirectory, uuidOfInformationPackage, iriOfClass); 
	}
	
	@Override
	public TripleStatementCollection findTriplesByClass(final WorkingDirectory workingDirectory, final Iri iriOfClass) {
		return findTriplesInStoreByClass(workingDirectory, iriOfClass); 
	}
	
	@Override
	public TripleStatementCollection findTriplesByInformationPackage(final WorkingDirectory workingDirectory, final Uuid uuidOfInformationPackage) {
		return findTriplesOfInformationPackageInStore(workingDirectory, uuidOfInformationPackage);
	}
	
	@Override
	public Optional<TripleStatement> findDatatypeOfProperty(final String iriOfProperty, final WorkingDirectory workingDirectory) {
		final Repository repository = initializeRepository(workingDirectory);
		final IRI iriOfDatatypeProperty = valueFactory.createIRI(iriOfProperty);
		final TriplePredicate hasDatatypePredicate = RdfElementFactory.createHasDatatypePredicate();
		final IRI iriOfHasDatatypePredicate = valueFactory.createIRI(hasDatatypePredicate.getIri().toString());
		
		try (final RepositoryConnection connection = repository.getConnection();) {
			final RepositoryResult<Statement> statementsOfIndividual = connection.getStatements(iriOfDatatypeProperty, iriOfHasDatatypePredicate, null);
			final List<Statement> statementsOfPropertyAsList = Iterations.asList(statementsOfIndividual);
			final Statement statementOfProperty = statementsOfPropertyAsList.get(0);
			final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(iriOfProperty);
			final TripleObject tripleObject = RdfElementFactory.createTripleObject(statementOfProperty.getObject().stringValue());
			final TripleStatement statementOfDatatype = RdfElementFactory.createStatement(tripleSubject, hasDatatypePredicate, tripleObject);
			
			return ServerOptionalFactory.createOptionalWithTripleStatement(statementOfDatatype);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ServerOptionalFactory.createEmptyOptionalWithTripleStatement();
		}  finally {
			repository.shutDown();
		}
	}
	
	private TripleStatementCollection findTriplesOfInformationPackageInStore(final WorkingDirectory workingDirectory, final Uuid uuidOfInformationPackage) {
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final Repository repository = initializeRepository(workingDirectory);
		final Iri informationPackageIri = ServerModelFactory.createIri(uuidOfInformationPackage);
		final IRI iriOfInformationPackage = valueFactory.createIRI(informationPackageIri.toString());
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		try (final RepositoryConnection connection = repository.getConnection();) {
			final TripleStatementCollection individualTriples = findIndividualsOfInformationPackage(connection, iriOfInformationPackage);
			tripleStatements.addStatements(individualTriples);
			
			final TripleStatementCollection localFileStatements = findLocalFileStatementsOfInformationPackage(connection, iriOfInformationPackage);
			tripleStatements.addStatements(localFileStatements);
			
			final TripleStatementCollection remoteFileStatements = findRemoteFileStatementsOfInformationPackage(connection, iriOfInformationPackage);
			tripleStatements.addStatements(remoteFileStatements);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			repository.shutDown();
		}
		
		return tripleStatements;
	}
	
	@Override
	public TripleStatementCollection findTriplesByIndividual(final WorkingDirectory workingDirectory, final Uuid uuidOfIndividual) {
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final Repository repository = initializeRepository(workingDirectory);
		final Iri individualIri = ServerModelFactory.createIri(uuidOfIndividual);
		final IRI iriOfIndividual = valueFactory.createIRI(individualIri.toString());
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		try (final RepositoryConnection connection = repository.getConnection();) {
			final TripleStatementCollection individualTriples = findPropertiesOfIndividual(connection, iriOfIndividual);
			tripleStatements.addStatements(individualTriples);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			repository.shutDown();
		}
		
		return tripleStatements;
	}
	
	/**
	 * Creates a list with all statements that describe individuals of the information package.
	 * @param connection A connection to the repository.
	 * @param iriOfInformationPackage The iri of the information package.
	 * @return Contains triples for all individuals of this information package.
	 */
	private TripleStatementCollection findIndividualsOfInformationPackage(final RepositoryConnection connection, final IRI iriOfInformationPackage) {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		final TriplePredicate aggregatedByPredicate = RdfElementFactory.createAggregatedByPredicate();
		final String iriOfAggregatedByPredicateAsString = aggregatedByPredicate.getIri().toString();
		final IRI iriOfAggregatedByPredicate = valueFactory.createIRI(iriOfAggregatedByPredicateAsString);
		
		final RepositoryResult<Statement> statementsOfIndividual = connection.getStatements(null, iriOfAggregatedByPredicate, 
				iriOfInformationPackage);
		final List<Statement> statementsOfIndividualAsList = Iterations.asList(statementsOfIndividual);
		
		for (final Statement statement: statementsOfIndividualAsList) {
			final Resource individualAsResource = statement.getSubject();
			final String uuidOfIndividual = individualAsResource.stringValue();
			final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual);
			final TripleObject uuidOfInformationPackageObject = RdfElementFactory.createTripleObject(iriOfInformationPackage.stringValue());
			
			final TripleStatement aggregatedByStatement = RdfElementFactory.createAggregatedByStatement(tripleSubject, uuidOfInformationPackageObject);
			tripleStatements.addTripleStatement(aggregatedByStatement);
		}
		
		return tripleStatements;
	}
	
	/**
	 * Creates a list with all statements that describe the properties of an individual.
	 * @param connection A connection to the repository.
	 * @param iriOfIndividual The iri of the individual.
	 * @return Contains triples for all properties of this individual.
	 */
	//Werden eigentlich die ObjectProperties auch aus dem TripleStore gelesen? Ich glaube nicht.
	private TripleStatementCollection findPropertiesOfIndividual(final RepositoryConnection connection, final IRI iriOfIndividual) {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		final RepositoryResult<Statement> statementsOfIndividual = connection.getStatements(iriOfIndividual, null, null);
		final List<Statement> statementsOfIndividualAsList = Iterations.asList(statementsOfIndividual);
		
		for (Statement statement: statementsOfIndividualAsList) {
			final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(iriOfIndividual.stringValue());
			
			final Resource resourceOfPredicate = statement.getPredicate();
			final String iriOfPredicate = resourceOfPredicate.stringValue();
			final Value valueOfObject = statement.getObject();
			final TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(iriOfPredicate, 
					valueOfObject.stringValue());
			final TripleObject valueOfProperty = RdfElementFactory.createTripleObject(valueOfObject.stringValue());
			
			final TripleStatement propertyStatement = RdfElementFactory.createStatement(tripleSubject, 
					triplePredicate, valueOfProperty);
			tripleStatements.addTripleStatement(propertyStatement);
		}
		
		return tripleStatements;
	}
	
	/**
	 * Finds all statement that describe the references to local files of the information package.
	 * @param connection A connection to the repository.
	 * @param iriOfInformationPackage The iri of the information package.
	 * @return Contains all statements that describe the references to local files of the information package.
	 */
	private TripleStatementCollection findLocalFileStatementsOfInformationPackage(final RepositoryConnection connection, final IRI iriOfInformationPackage) {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		final String uuidOfInformationPackage = iriOfInformationPackage.getLocalName();
		final TriplePredicate localFileReferencePredicate = RdfElementFactory.createFileReferencePredicate();
		final IRI iriOfLocalFileReference =  valueFactory.createIRI(localFileReferencePredicate.getIri().toString());
		final RepositoryResult<Statement> localFileReferences = connection.getStatements(iriOfInformationPackage, iriOfLocalFileReference, null);
		final List<Statement> localFileReferencesAsList = Iterations.asList(localFileReferences);
		
		for (final Statement statement: localFileReferencesAsList) {
			final Value valueOfObject = statement.getObject();
			final String iriOfFile = valueOfObject.stringValue();
			final String uuidOfFile = StringUtils.extractLocalNameFromIri(iriOfFile);
			
			final TripleStatement localFileStatement = RdfElementFactory.createStorageFileStatement(new Uuid(uuidOfInformationPackage), new Uuid(uuidOfFile));
			tripleStatements.addTripleStatement(localFileStatement);
		}
		
		return tripleStatements;
	}
	
	/**
	 * Finds all statement that describe the references to remote files of the information package.
	 * @param connection A connection to the repository.
	 * @param iriOfInformationPackage The iri of the information package.
	 * @return Contains all statements that describe the references to remote files of the information package.
	 */
	private TripleStatementCollection findRemoteFileStatementsOfInformationPackage(final RepositoryConnection connection, final IRI iriOfInformationPackage) {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		final 	String uuidOfInformationPackage = iriOfInformationPackage.getLocalName();
		
		final TriplePredicate remoteFileReferencePredicate = RdfElementFactory.createRemoteFileReferencePredicate();
		final IRI iriOfRemoteFileReference =  valueFactory.createIRI(remoteFileReferencePredicate.getIri().toString());
		final RepositoryResult<Statement> remoteFileReferences = connection.getStatements(iriOfInformationPackage, iriOfRemoteFileReference, null);
		final List<Statement> remoteFileReferencesAsList = Iterations.asList(remoteFileReferences);
		
		for (final Statement statement: remoteFileReferencesAsList) {
			final Value valueOfObject = statement.getObject();
			final String iriOfFile = valueOfObject.stringValue();
			final String urlOfFile = StringUtils.extractLocalNameFromIri(iriOfFile);
			
			final TripleStatement remoteFileStatement = RdfElementFactory.createRemoteFileStatement(new Uuid(uuidOfInformationPackage), urlOfFile);
			tripleStatements.addTripleStatement(remoteFileStatement);
		}
		
		return tripleStatements;
	}
	
	/**
	 * Looks in the triple store for triples determined by their class.
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 * @param iriOfClass The iri of the class the triples belong to. Is null if the class is not relevant for the search.
	 * @return The found triples.
	 */
	private TripleStatementCollection findTriplesInStoreByClass(final WorkingDirectory workingDirectory, final Iri iriOfClass) {
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final Repository repository = initializeRepository(workingDirectory);
		final TripleStatementCollection tripleStatementList = RdfElementFactory.createEmptyTripleStatementCollection();
		final IRI classIri = valueFactory.createIRI(iriOfClass.toString());
		
		try (final RepositoryConnection connection = repository.getConnection();) {
			final List<IRI> irisOfIndividuals = determineIrisOfIndividualsClass(connection, classIri);
			
			for (final IRI iriOfIndividual: irisOfIndividuals) {
				final RepositoryResult<Statement> statementsOfIndividual = connection.getStatements(iriOfIndividual, null, null);
				final List<Statement> statementsOfIndividualAsList = Iterations.asList(statementsOfIndividual);
				
				for (final Statement statement: statementsOfIndividualAsList) {
					processStatementWithTypePredicate(statement, tripleStatementList, iriOfClass.toString(), statementsOfIndividualAsList);
				}
			}
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			repository.shutDown();
		}
		
		return tripleStatementList;
	}
	
	@Override
	public String findOntologyClassOfIndividual(final WorkingDirectory workingDirectory, final Iri iriOfIndividual) {
		String iriOfOntologyClass = StringUtils.EMPTY_STRING;
		
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final Repository repository = initializeRepository(workingDirectory);
		final IRI individualIri = valueFactory.createIRI(iriOfIndividual.toString());
		final TriplePredicate typeTriplePredicate = RdfElementFactory.createTypePredicate();
		final IRI iriOfTypePredicate = valueFactory.createIRI(typeTriplePredicate.getIri().toString());
		
		try (final RepositoryConnection connection = repository.getConnection();) {
			final RepositoryResult<Statement> statementOfIndividual = connection.getStatements(individualIri, iriOfTypePredicate, null);
			final Statement statement = Iterations.asList(statementOfIndividual).get(0);
			iriOfOntologyClass = statement.getObject().stringValue();

		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			repository.shutDown();
		}
		
		return iriOfOntologyClass;
	}
	
	/**
	 * Looks in the triple store for triples determined by their information package and their class. If the class is not relevant,
	 * the parameter iriOfClass is null.
	 * @param workingDirectory The working directory that contains the data of the triple store.
	 * @param uuidOfInformationPackage The uuid of the information package the triples belong to.
	 * @param iriOfClass The iri of the class the triples belong to. Is null if the class is not relevant for the search.
	 * @return The found triples.
	 */
	private TripleStatementCollection findTriplesInStore(final WorkingDirectory workingDirectory, final Uuid uuidOfInformationPackage, 
			final String iriOfClass) {
		final ValueFactory valueFactory = SimpleValueFactory.getInstance();
		final Repository repository = initializeRepository(workingDirectory);
		final Iri informationPackageIri = ServerModelFactory.createIri(uuidOfInformationPackage);
		final IRI iriOfInformationPackage = valueFactory.createIRI(informationPackageIri.toString());
		final TripleStatementCollection tripleStatementList = RdfElementFactory.createEmptyTripleStatementCollection();
		
		try (final RepositoryConnection connection = repository.getConnection();) {
			final List<IRI> irisOfIndividuals = determineIrisOfIndividualsInInformationPackage(connection, 
					iriOfInformationPackage);
			
			for (final IRI iriOfIndividual: irisOfIndividuals) {
				final RepositoryResult<Statement> statementsOfIndividual = connection.getStatements(iriOfIndividual, null, null);
				final List<Statement> statementsOfIndividualAsList = Iterations.asList(statementsOfIndividual);
				
				for (final Statement statement: statementsOfIndividualAsList) {
					processStatementWithTypePredicate(statement, tripleStatementList, iriOfClass, statementsOfIndividualAsList);
				}
			}
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			repository.shutDown();
		}
		
		return tripleStatementList;
	}
	
	/**
	 * If statement is a statement with a type predicate it defines an individual. The method checks if the 
	 * individual is an instance of the class that is defined by iriOfClass. If so, the statements are 
	 * searched for the properties of this individual. The respective statements are inserted into a list
	 * from which the individuals are generated that will be send to the client.
	 * @param statement The statement that is checked.
	 * @param tripleStatementList Contains the statements that are used for the generation of the data for the client.
	 * @param iriOfClass The iri of the class whose individuals we are looking for or null if all individuals in an information 
	 * package are wanted.
	 * @param statementsOfIndividualAsList All statements
	 */
	private void processStatementWithTypePredicate(final Statement statement, final TripleStatementCollection tripleStatementList,
			final String iriOfClass, final List<Statement> statementsOfIndividualAsList) {
		final IRI predicateIri = statement.getPredicate();
		final String predicateIriAsString = predicateIri.toString();
		
		if (RdfElementFactory.isTypePredicate(predicateIriAsString)) {
			final Value valueOfObject = statement.getObject();
			final String valueOfObjectAsString = valueOfObject.stringValue();
			final Resource individualAsResource = statement.getSubject();
			final String uuidOfIndividual = individualAsResource.stringValue();
			
			// If the iri of the class is null, all individuals in this information package are added.
			if (iriOfClass != null) {
				if (StringUtils.areStringsEqual(valueOfObjectAsString, iriOfClass)) {
					addStatementToListIfPropertyOfIndividual(statementsOfIndividualAsList, uuidOfIndividual, tripleStatementList);
				}
			} else {
				addStatementToListIfPropertyOfIndividual(statementsOfIndividualAsList, uuidOfIndividual, tripleStatementList);
			}
		}
	}
	
	/**
	 * Adds the statements in statementList to tripleStatementList if the statement describes a property of uuidOfIndividual1.
	 * statementList is searched and if a property predicate is found with the same uuid as uuidOfIndividual1, the statement
	 * is converted into a TripleStatement and inserted into tripleStatementList.
	 * @param statementList Contains the statements read from the triple store.
	 * @param uuidOfIndividual1 The uuid of the individual whose properties we are looking for.
	 * @param tripleStatementList The list with the statements for the client. 
	 */
	private void addStatementToListIfPropertyOfIndividual(final List<Statement> statementList, final String uuidOfIndividual1,
			final TripleStatementCollection tripleStatementList) {
		for (final Statement statement: statementList) {
			final Resource individualAsResource = statement.getSubject();
			final String uuidOfIndiviudal2 = individualAsResource.stringValue();
			final IRI predicateIri = statement.getPredicate();
			final String predicateIriAsString = predicateIri.toString();
			
			if (isPropertyPredicate(predicateIriAsString)) {
				if (StringUtils.areStringsEqual(uuidOfIndividual1, uuidOfIndiviudal2)) {
					final Resource subjectResource = statement.getSubject();
					final IRI iriOfPredicate = statement.getPredicate();
					final Value objectValue = statement.getObject();
					
					final String uuidOfSubject = subjectResource.stringValue();
					final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfSubject);
					final TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(iriOfPredicate.stringValue(), null);
					final TripleObject tripleObject = RdfElementFactory.createTripleObject(objectValue.stringValue());
					
					final String localname = StringUtils.extractLocalNameFromIri(objectValue.stringValue());
					if (StringUtils.isUuid(localname)) {
						triplePredicate.setPredicateType(PredicateType.OBJECT_PROPERTY);
					}
					
					final TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
					
					tripleStatementList.addTripleStatement(tripleStatement);
				}
			}
		}
		
	}
	
	/**
	 * Checks if the iri of a predicate represents a predicate that describes a property. That means that
	 * it is not a type or a aggregatedBy property.
	 * @param iriOfPredicate The iri of the predicate that should be checked.
	 * @return True if it is a property predicate, false otherwise.
	 */
	private boolean isPropertyPredicate(final String iriOfPredicate) {
		if (!RdfElementFactory.isTypePredicate(iriOfPredicate) &&
			 !RdfElementFactory.isAggregatedByPredicate(iriOfPredicate)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Reads the iris of the individuals of an information package from the triple store. 
	 * @param connection The connection to the triple store.
	 * @param iriOfInformationPackage The iri of the information package we are interested in. 
	 * @return The list with the iris of all individuals in the information package.
	 */
	private List<IRI> determineIrisOfIndividualsInInformationPackage(final RepositoryConnection connection, final IRI iriOfInformationPackage) {
		final RepositoryResult<Statement> statementsOfInformationPackage = connection.getStatements(null, null, iriOfInformationPackage);
		
		final List<Statement> statementsOfInformationPackageAsList = Iterations.asList(statementsOfInformationPackage);
		final List<IRI> irisOfIndividuals = new ArrayList<IRI>();
		
		for (final Statement statement: statementsOfInformationPackageAsList) {
			final Resource individualAsResource = statement.getSubject();
			final String iriOfIndividualAsString = individualAsResource.stringValue();
			final IRI iriOfIndividual = valueFactory.createIRI(iriOfIndividualAsString);
			irisOfIndividuals.add(iriOfIndividual);
		}
		
		return irisOfIndividuals;
	}
	
	/**
	 * Reads the iris of the individuals of an ontology class from the triple store. 
	 * @param connection The connection to the triple store.
	 * @param iriOfClass The iri of the class we are interested in. 
	 * @return The list with the iris of all individuals of the class.
	 */
	private List<IRI> determineIrisOfIndividualsClass(final RepositoryConnection connection, final IRI iriOfClass) {
		final TriplePredicate typePredicate = RdfElementFactory.createTypePredicate();
		final Iri iriOfTypePredicate = typePredicate.getIri();
		final IRI typePredicateIri = valueFactory.createIRI(iriOfTypePredicate.toString());
		final RepositoryResult<Statement> statementsOfClass = connection.getStatements(null, typePredicateIri, iriOfClass);
		
		final List<Statement> statementsOfClassAsList = Iterations.asList(statementsOfClass);
		final List<IRI> irisOfIndividuals = new ArrayList<IRI>();
		
		for (final Statement statement: statementsOfClassAsList) {
			final Resource individualAsResource = statement.getSubject();
			final String iriOfIndividualAsString = individualAsResource.stringValue();
			final IRI iriOfIndividual = valueFactory.createIRI(iriOfIndividualAsString);
			irisOfIndividuals.add(iriOfIndividual);
		}
		
		return irisOfIndividuals;
	}
	
	/**
	 * Initializes the repository. The data are saved in the data directory. 
	 * @param workingDirectory Contains the data directory where the data are stored in.
	 * @return The repository for further processing.
	 */
	private Repository initializeRepository(final WorkingDirectory workingDirectory) {
		File dataDirectory = workingDirectory.getDataDirectory();
		Repository repository = new SailRepository(new NativeStore(dataDirectory));
		repository.initialize();
		return repository;
	}

	@Override
	public void unassignAllIndividualsFromInformationPackage(final Uuid uuidOfInformationPackage,
			final WorkingDirectory workingDirectory) {
		final Iri iri = ServerModelFactory.createIri(uuidOfInformationPackage);
		final IRI iriOfInformationPackage = valueFactory.createIRI(iri.toString());
		
		final Repository repository = initializeRepository(workingDirectory);
		try (final RepositoryConnection connection = repository.getConnection();) {
			deleteAggregatedByStatements(iriOfInformationPackage, connection);
		} catch (RDFParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			repository.shutDown();
		}
	}
}