package de.feu.kdmp4.server.triplestore.facades;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreService;

/**
 * Contains the methods for accessing the module TripleStore from the module Storage.
 * @author Christopher Olbertz
 *
 */
public class StorageTripleStoreFacade {
	/**
	 * Contains the business logic of the 
	 */
	private TripleStoreService tripleStoreService;
	
	/**
	 * Deletes all triples that are related to a specific information package from the triple store.
	 * @param uuidOfInformationPackage The uuid of the information package that should be removed from the 
	 * triple store.
	 */
	public void deleteAllTriplesOfInformationPackageIfExists(final Uuid uuidOfInformationPackage)  {
		tripleStoreService.deleteAllTriplesOfInformationPackageIfExists(uuidOfInformationPackage);
	}
	
	/**
	 * Sets a reference to the object that contains the business logic of the module TripleStore. 
	 * @param tripleStoreService A reference to the object that contains the business logic of the module TripleStore. 
	 */
	public void setTripleStoreService(final TripleStoreService tripleStoreService) {
		this.tripleStoreService = tripleStoreService;
	}
}
