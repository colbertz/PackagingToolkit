package de.feu.kdmp4.server.triplestore.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.TripleStoreConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;

/**
 * An interface for the business logic of the module TripleStore.
 * @author Christopher Olbertz
 *
 */
public interface TripleStoreService {
	/**
	 * Sets a reference to the operations layer.
	 * @param tripleStoreOperations The object that represents the operations layer.
	 */
	void setTripleStoreOperations(final TripleStoreOperations tripleStoreOperations);
	/**
	 * Saves tripels in the tripel store.
	 * @param tripleStatementList Contains the tripel that should be saved.
	 */
	void saveTriples(final TripleStatementCollection tripleStatementList);
	/**
	 * Sets the facade object that encapsulates the configuration module for the triple store module.
	 * @param tripleStoreConfigurationFacade The object that controls the access to the configuration module.
	 */
	void setTripleStoreConfigurationFacade(final TripleStoreConfigurationFacade tripleStoreConfigurationFacade);
	/**
	 * Looks for the triples that determine a certain class in the triple store. 
	 * @param uuidOfInformationPackage The uuid of the information package we are interested in.
	 * @param iriOfClass The iri of the class whose tripel we want to find.
	 * @return The triples of this class in this information package,
	 */
	TripleStatementCollection findTriplesByInformationPackageAndClass(final Uuid uuidOfInformationPackage, final String iriOfClass);
	/**
	 * Determines all tripel for a given information package from the triple store.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return All triples of the information package with the uuid uuidOfInformationPackage. 
	 */
	TripleStatementCollection findTriplesByInformationPackage(final Uuid uuidOfInformationPackage);
	/**
	 * Adds a reference to a file in the local storage of the server to the triple store.
	 * @param uuidOfInformationPackage The uuid of the information package that points to a 
	 * file.
	 * @param uuidOfFile The uuid of the file used in the database.
	 */
	void addFileReferenceToInformationPackage(final Uuid uuidOfInformationPackage, final Uuid uuidOfFile);
	/**
	 * Finds all triples that describe an individual in the triple store. These tripels are describing the properties of this
	 * individual. 
	 * @param uuidOfIndividual The uuid of the individual whose properties we are looking for.
	 * @return All statements that describe the properties of this individual.
	 */
	TripleStatementCollection findTriplesByIndividual(final Uuid uuidOfIndividual);
	/**
	 * Determines the datatype of a property with the help of the triple store. In the triple store there is a tripel for
	 * every property. This tripel has the form: property hasDatatype datatype. The tripel for the given property is read
	 * from the tripel store and the datatype is determined.
	 * @param propertyIri The iri of the property and the starting basis for the search in the triple store.
	 * @return The datatype determined from the properties in the triple store.
	 */
	Optional<OntologyDatatype> determineDatatypeOfProperty(final String propertyIri);
	/**
	 * Determines the datatype of a property from the triple store. The predicate of the relationship we are looking for is
	 * hasDatatype and the object of the hasDatatype statement is the datatype.
	 * @param iriOfProperty The iri of the property whose datatype we are looking for.
	 * @return The statement that contains the information about the datatype.
	 */
	Optional<TripleStatement> findDatatypeOfProperty(final String iriOfProperty);
	/**
	 * Deletes all triples that are related to a specific information package from the triple store.
	 * @param uuidOfInformationPackage The uuid of the information package that should be removed from the 
	 * triple store.
	 */
	void deleteAllTriplesOfInformationPackageIfExists(final Uuid uuidOfInformationPackage);
	/**
	 * Deletes all triples that are related to a specific individual from the triple store.
	 * @param uuidOfIndividual The uuid of the individual that should be removed from the 
	 * triple store.
	 */
	void deleteAllTriplesOfIndividualIfExists(final Uuid uuidOfIndividual);
	/**
	 * Finds all triples describing the individuals of a certain class from the triple store.
	 * @param iriOfClass The iri of the class whose individuals we are looking for.
	 * @return The triple describing the individuals of this class.
	 */
	TripleStatementCollection findTriplesByClass(Iri iriOfClass);
	/**
	 * Determines the class of an individual from the triple store. The type predicate is used
	 * for determining the class. 
	 * @param iriOfIndividual The iri of the individual whose class we want to determine.
	 * @return The iri of class of the individual as string or an empty string if the iri of the individual was not
	 * found in the triple store.
	 */
	String findOntologyClassOfIndividual(Iri iriOfIndividual);
	/**
	 * Deletes all aggregated-by statements that have a certain information package as object.
	 * @param uuidOfInformationPackage The uuid of the information we want to delete all
	 * aggregated-by statements from.
	 */
	void unassignAllIndividualsFromInformationPackage(Uuid uuidOfInformationPackage);
	/**
	 * Marks some individuals as selected in the triple store.
	 * @param taxonomyIndividuals The individuals that have been selected.
	 * @param iriOfInformationPackage The iri of the information package the user has selected the individuals
	 * in. 
	 */
	void selectTaxonomyIndividuals(TaxonomyIndividualListResponse taxonomyIndividuals, String iriOfInformationPackage);
	/**
	 * Deletes all triples from the triple store that describe the assignment of taxonomy individuals to
	 * an information package. 
	 * @param uuidOfInformationPackage The information package whose taxonomy individual assignment we want
	 * to delete. 
	 */
	void deleteAllTaxomonyIndividualsOfInformationPackage(Uuid uuidOfInformationPackage);
	/**
	 * Finds the triples of all taxonomy individuals that are assigned to a certain information
	 * package.
	 * @param uuidOfInformationPackage The uuid of the information package whose taxonomy individuals 
	 * we want to find.
	 * @return The triples that represent the taxonomy individuals assigned to the information package
	 * with the uuid uuidOfInformationPackage.  
	 */
	TripleStatementCollection findAllTriplesOfAssignedTaxonomyIndividuals(Uuid uuidOfInformationPackage);
}
