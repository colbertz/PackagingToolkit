/**
 * Contains the classes that contain the main logic for the module triple store. 
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.server.triplestore.classes;