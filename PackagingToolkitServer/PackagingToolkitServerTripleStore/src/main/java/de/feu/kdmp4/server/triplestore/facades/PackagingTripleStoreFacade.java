package de.feu.kdmp4.server.triplestore.facades;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreService;

/**
 * Contains the method that are used for accessing the methods of the triple store module by the
 * packaging module.
 * @author Christopher Olbertz
 *
 */
public class PackagingTripleStoreFacade {
	/**
	 * A reference to the class that contains the business logic of the triple store module.
	 */
	private TripleStoreService tripleStoreService;
	
	/**
	 * Looks for the triples that determine a certain class in the triple store. 
	 * @param uuidOfInformationPackage The uuid of the information package we are interested in.
	 * @param iriOfClass The iri of the class whose tripel we want to find.
	 * @return The triples of this class in this information package,
	 */
	public TripleStatementCollection findTriplesByInformationPackageAndClass(final Uuid uuidOfInformationPackage, final String iriOfClass) {
		return tripleStoreService.findTriplesByInformationPackageAndClass(uuidOfInformationPackage, iriOfClass);
	}
	
	/**
	 * Marks some individuals as selected in the triple store.
	 * @param taxonomyIndividuals The individuals that have been selected.
	 * @param iriOfInformationPackage The iri of the information package the user has selected the individuals
	 * in. 
	 */
	public void selectTaxonomyIndividuals(final TaxonomyIndividualListResponse taxonomyIndividuals,
			  							  final String iriOfInformationPackage) {
		tripleStoreService.selectTaxonomyIndividuals(taxonomyIndividuals, iriOfInformationPackage);
	}
	
	/**
	 * Sets the reference to the class that contains the business logic of the triple store module.
	 * @param tripleStoreService A reference to an object class that contains the business logic of the triple store module.
	 */
	public void setTripleStoreService(final TripleStoreService tripleStoreService) {
		this.tripleStoreService = tripleStoreService;
	}
}
