package de.feu.kdmp4.server.triplestore.facades;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreService;

/**
 * Contains the methods that the module Ontology can user to access the methods of the module
 * TripleStore.
 * @author Christopher Olbertz
 *
 */
public class OntologyTripleStoreFacade {
	/**
	 * Contains the business logic of the module TripleStore.
	 */
	private TripleStoreService tripleStoreService;
	
	/**
	 * Looks for the triples that determine a certain class in the triple store. 
	 * @param uuidOfInformationPackage The uuid of the information package we are interested in.
	 * @param iriOfClass The iri of the class whose tripel we want to find.
	 * @return The triples of this class in this information package,
	 */
	public TripleStatementCollection findTriplesByInformationPackageAndClass(final Uuid uuidOfInformationPackage, final String iriOfClass) {
		return tripleStoreService.findTriplesByInformationPackageAndClass(uuidOfInformationPackage, iriOfClass);
	}
	
	/**
	 * Finds all triples describing the individuals of a certain class from the triple store.
	 * @param iriOfClass The iri of the class whose individuals we are looking for.
	 * @return The triple describing the individuals of this class.
	 */
	public TripleStatementCollection findTriplesByClass(final Iri iriOfClass) {
		return tripleStoreService.findTriplesByClass(iriOfClass);
	}
	
	/**
	 * Determines the class of an individual from the triple store. The type predicate is used
	 * for determining the class. 
	 * @param iriOfIndividual The iri of the individual whose class we want to determine.
	 * @return The iri of class of the individual as string or an empty string if the iri of the individual was not
	 * found in the triple store.
	 */
	public String findOntologyClassOfIndividual(final Iri iriOfIndividual) {
		return tripleStoreService.findOntologyClassOfIndividual(iriOfIndividual);
	}
	
	/**
	 * Sets the reference to the business logic of the module TripleStore.
	 * @param tripleStoreService The reference to the business logic of the module TripleStore.
	 */
	public void setTripleStoreService(final TripleStoreService tripleStoreService) {
		this.tripleStoreService = tripleStoreService;
	}
	
	/**
	 * Determines the datatype of a property from the triple store. The predicate of the relationship we are looking for is
	 * hasDatatype and the object of the hasDatatype statement is the datatype.
	 * @param iriOfProperty The iri of the property whose datatype we are looking for.
	 * @return The statement that contains the information about the datatype.
	 */
	public Optional<TripleStatement> findDatatypeOfProperty(final String iriOfProperty) {
		return tripleStoreService.findDatatypeOfProperty(iriOfProperty);
	}
	
	/**
	 * Saves tripels in the tripel store.
	 * @param tripleStatementList Contains the tripel that should be saved.
	 */
	public void saveTriples(final TripleStatementCollection tripleStatementList) {
		tripleStoreService.saveTriples(tripleStatementList);
	}
}
