/**
 * Contains the facade classes that hide the inner structure of the module triple store from
 * the other modules. There is a facade for every module that wants to communicate with the
 * triple store module. The facade classes only contain these methods that must be visible for the
 * other module.  Every facade class name starts with the name of the module that wants to access
 * the triple store module followed by TripleStoreFacade. 
 * <br />
 * Example: MyModuleTripleStoreFacade contains the methods that are necessary for the module
 * MyModule to access the functions of the triple store. 
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.server.triplestore.facades;