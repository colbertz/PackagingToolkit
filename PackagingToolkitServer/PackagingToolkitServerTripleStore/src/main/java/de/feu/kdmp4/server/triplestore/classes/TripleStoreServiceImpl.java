package de.feu.kdmp4.server.triplestore.classes;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.TripleStoreConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreOperations;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreService;

public class TripleStoreServiceImpl implements TripleStoreService {
	/**
	 * A reference to the atomar operations of this module.
	 */
	private TripleStoreOperations tripleStoreOperations;
	/**
	 * Contains the methods for the access to the module configuration.
	 */
	private TripleStoreConfigurationFacade tripleStoreConfigurationFacade; 
	
	@Override
	public Optional<OntologyDatatype> determineDatatypeOfProperty(final String propertyIri) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		return tripleStoreOperations.determineDatatypeOfProperty(workingDirectory, propertyIri);
	}
	
	@Override
	public void selectTaxonomyIndividuals(final TaxonomyIndividualListResponse taxonomyIndividuals,
										  final String iriOfInformationPackage) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		
		for (int i = 0; i < taxonomyIndividuals.getIndividuals().size(); i++) {
			final TaxonomyIndividualResponse individual = taxonomyIndividuals.getIndividuals().get(i);
			final String taxonomyIndividualIri = individual.getIri();
			tripleStoreOperations.createSelectTaxonomyIndividualStatement(taxonomyIndividualIri, iriOfInformationPackage, 
					workingDirectory);
		}
	}
	
	@Override
	public 	void deleteAllTaxomonyIndividualsOfInformationPackage(final Uuid uuidOfInformationPackage) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		tripleStoreOperations.deleteAllTaxomonyIndividualsOfInformationPackage(uuidOfInformationPackage, workingDirectory);
	}
	
	@Override
	public void unassignAllIndividualsFromInformationPackage(final Uuid uuidOfInformationPackage) {
		final ServerConfigurationData serverConfigurationData = tripleStoreConfigurationFacade.readServerConfiguration();
		final WorkingDirectory workingDirectory = serverConfigurationData.getWorkingDirectory();
		tripleStoreOperations.unassignAllIndividualsFromInformationPackage(uuidOfInformationPackage, workingDirectory);
	}
	
	@Override
	public void saveTriples(final TripleStatementCollection tripleStatementList) {
		final ServerConfigurationData serverConfigurationData = tripleStoreConfigurationFacade.readServerConfiguration();
		final WorkingDirectory workingDirectory = serverConfigurationData.getWorkingDirectory();
		tripleStoreOperations.addTriplesToStore(tripleStatementList, workingDirectory);
	}
	
	@Override
	public void deleteAllTriplesOfInformationPackageIfExists(final Uuid uuidOfInformationPackage) {
		final ServerConfigurationData serverConfigurationData = tripleStoreConfigurationFacade.readServerConfiguration();
		final WorkingDirectory workingDirectory = serverConfigurationData.getWorkingDirectory();
		if (tripleStoreOperations.informationPackageExists(uuidOfInformationPackage, workingDirectory)) {
			tripleStoreOperations.deleteAllTriplesOfIndividual(uuidOfInformationPackage, workingDirectory);
			tripleStoreOperations.deleteAllTriplesOfInformationPackage(uuidOfInformationPackage, workingDirectory);
		}
	}
	
	@Override
	public void deleteAllTriplesOfIndividualIfExists(final Uuid uuidOfInformationPackage) {
		final ServerConfigurationData serverConfigurationData = tripleStoreConfigurationFacade.readServerConfiguration();
		final WorkingDirectory workingDirectory = serverConfigurationData.getWorkingDirectory();
		if (tripleStoreOperations.individualExists(uuidOfInformationPackage, workingDirectory)) {
			tripleStoreOperations.deleteAllTriplesOfIndividual(uuidOfInformationPackage, workingDirectory);
		}
	}
	
	@Override
	public TripleStatementCollection findTriplesByInformationPackageAndClass(final Uuid uuidOfInformationPackage, 
			String iriOfClass) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		return tripleStoreOperations.findTriplesByInformationPackageAndClass(workingDirectory, uuidOfInformationPackage, iriOfClass);
	}
	
	@Override
	public TripleStatementCollection findTriplesByClass(final Iri iriOfClass) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		return tripleStoreOperations.findTriplesByClass(workingDirectory, iriOfClass);
	}
	
	@Override
	public void addFileReferenceToInformationPackage(final Uuid uuidOfInformationPackage, final Uuid uuidOfFile) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		final TripleStatement fileReferenceStatement = RdfElementFactory.createStorageFileStatement(uuidOfInformationPackage, uuidOfFile);
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		tripleStatements.addTripleStatement(fileReferenceStatement);
		tripleStoreOperations.addTriplesToStore(tripleStatements, workingDirectory);
	}

	@Override
	public Optional<TripleStatement> findDatatypeOfProperty(final String iriOfProperty) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		return tripleStoreOperations.findDatatypeOfProperty(iriOfProperty, workingDirectory);
	}
	
	@Override
	public TripleStatementCollection findTriplesByInformationPackage(final Uuid uuidOfInformationPackage) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		return tripleStoreOperations.findTriplesByInformationPackage(workingDirectory, uuidOfInformationPackage);
	}
	
	@Override
	public String findOntologyClassOfIndividual(final Iri iriOfIndividual) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		return tripleStoreOperations.findOntologyClassOfIndividual(workingDirectory, iriOfIndividual);
	}
	
	@Override
	public TripleStatementCollection findAllTriplesOfAssignedTaxonomyIndividuals(final Uuid uuidOfInformationPackage) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		final TriplePredicate triplePredicate = RdfElementFactory.createTaxonomyIndividualIsSelectedPredicate();
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(uuidOfInformationPackage);
		return tripleStoreOperations.findAllTriplesBySubjectPredicatObject(null, triplePredicate, tripleObject, workingDirectory);
	}
	
	@Override
	public TripleStatementCollection findTriplesByIndividual(final Uuid uuidOfIndividual) {
		final WorkingDirectory workingDirectory = tripleStoreConfigurationFacade.getWorkingDirectory();
		return tripleStoreOperations.findTriplesByIndividual(workingDirectory, uuidOfIndividual);
	}
	
	@Override
	public void setTripleStoreOperations(final TripleStoreOperations tripleStoreOperations) {
		this.tripleStoreOperations = tripleStoreOperations;
	}
	
	@Override
	public void setTripleStoreConfigurationFacade(final TripleStoreConfigurationFacade tripleStoreConfigurationFacade) {
		this.tripleStoreConfigurationFacade = tripleStoreConfigurationFacade;
	}
}
