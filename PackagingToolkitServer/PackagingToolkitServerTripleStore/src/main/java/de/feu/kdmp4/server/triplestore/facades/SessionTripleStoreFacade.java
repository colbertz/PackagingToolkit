package de.feu.kdmp4.server.triplestore.facades;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreService;

/**
 * Contains the access methods for the triple store module that are used from the session module. Every method the session
 * module is allowed to call on the triple store module must be contained in this class. The session module may not know
 * any other class.
 * @author Christopher Olbertz
 *
 */
public class SessionTripleStoreFacade {
	/**
	 * A reference to the class that contains the business logic for the triple store module.
	 */
	private TripleStoreService tripleStoreService;
	
	/**
	 * Saves triple in the triple store. 
	 * @param tripleStatementList Contains the triples to save.
	 */
	public void saveTriples(final TripleStatementCollection tripleStatementList) {
		tripleStoreService.saveTriples(tripleStatementList);
	}
	
	/**
	 * Determines the class of an individual from the triple store. The type predicate is used
	 * for determining the class. 
	 * @param iriOfIndividual The iri of the individual whose class we want to determine.
	 * @return The class of the individual or an empty string if the iri of the individual was not
	 * found in the triple store.
	 */
	public String findOntologyClassOfIndividual(final Iri iriOfIndividual) {
		return tripleStoreService.findOntologyClassOfIndividual(iriOfIndividual);
	}
	
	/**
	 * Deletes all triples from the triple store that describe the assignment of taxonomy individuals to
	 * an information package. 
	 * @param uuidOfInformationPackage The information package whose taxonomy individual assignment we want
	 * to delete. 
	 */
	public 	void deleteAllTaxomonyIndividualsOfInformationPackage(final Uuid uuidOfInformationPackage) {
		tripleStoreService.deleteAllTaxomonyIndividualsOfInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Deletes all triples that are related to a specific individual from the triple store.
	 * @param uuidOfIndividual The uuid of the individual that should be removed from the 
	 * triple store.
	 */
	public void deleteAllTriplesOfIndividualIfExists(final Uuid uuidOfIndividual) {
		tripleStoreService.deleteAllTriplesOfIndividualIfExists(uuidOfIndividual);
	}
	
	/**
	 * Determines all tripel for a given information package from the triple store.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return All triples of the information package with the uuid uuidOfInformationPackage. 
	 */
	public TripleStatementCollection findTriplesOfInformationPackage(final Uuid uuidOfInformationPackage) {
		return tripleStoreService.findTriplesByInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Determines the datatype of a property with the help of the triple store. In the triple store there is a tripel for
	 * every property. This tripel has the form: property hasDatatype datatype. The tripel for the given property is read
	 * from the tripel store and the datatype is determined.
	 * @param propertyIri The iri of the property and the starting basis for the search in the triple store.
	 * @return The datatype determined from the properties in the triple store.
	 */
	public Optional<OntologyDatatype> determineDatatypeOfProperty(final String propertyIri) {
		return tripleStoreService.determineDatatypeOfProperty(propertyIri);
	}
	
	/**
	 * Finds all triples that describe an individual in the triple store. These tripels are describing the properties of this
	 * individual. 
	 * @param uuidOfIndividual The uuid of the individual whose properties we are looking for.
	 * @return All statements that describe the properties of this individual.
	 */
	public TripleStatementCollection findTriplesByIndividual(final Uuid uuidOfIndividual) {
		return tripleStoreService.findTriplesByIndividual(uuidOfIndividual);
	}
	
	/**
	 * Sets the reference to the object that contains the business logic of the triple store module.
	 * @param tripleStoreService Contains the business logic of the triple store module.
	 */
	public void setTripleStoreService(final TripleStoreService tripleStoreService) {
		this.tripleStoreService = tripleStoreService;
	}
	
	/**
	 * Deletes all aggregated-by statements that have a certain information package as object.
	 * @param uuidOfInformationPackage The uuid of the information we want to delete all
	 * aggregated-by statements from.
	 */
	public void unassignAllIndividualsFromInformationPackage(Uuid uuidOfInformationPackage) {
		tripleStoreService.unassignAllIndividualsFromInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Finds the triples of all taxonomy individuals that are assigned to a certain information
	 * package.
	 * @param uuidOfInformationPackage The uuid of the information package whose taxonomy individuals 
	 * we want to find.
	 * @return The triples that represent the taxonomy individuals assigned to the information package
	 * with the uuid uuidOfInformationPackage.  
	 */
	public TripleStatementCollection findAllTriplesOfAssignedTaxonomyIndividuals(final Uuid uuidOfInformationPackage) {
		return tripleStoreService.findAllTriplesOfAssignedTaxonomyIndividuals(uuidOfInformationPackage);
	}
}
