package de.feu.kdmp4.packagingtoolkit.server.serialization.classes;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationOperations;

public class SerializationOperationsTest {
	private SerializationOperations serializationOperations;
	
	private static final String RULE_ONTOLOGY_SOURCE_PATH = "KDMP4-OAIS-OAI-ORE.owl";
	private static final String BASE_ONTOLOGY_SOURCE_PATH = "baseOntology.owl";
	
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	
	@Before
	public void setUp() {
		serializationOperations = new SerializationOperationsOwlApiImpl();
	}
	
	@Test
	public void testSerialize() throws IOException {
		Uuid uuidOfInformationPackage = new Uuid();
		OntologyIndividualCollection ontologyIndividualList = prepareTest_testSerialize_individuals(uuidOfInformationPackage);
		
		String ruleOntologyFileUrl = getClass().getClassLoader().getResource(RULE_ONTOLOGY_SOURCE_PATH).getFile();
		File ruleOntology = new File(ruleOntologyFileUrl);
		String baseOntologyFileUrl = getClass().getClassLoader().getResource(BASE_ONTOLOGY_SOURCE_PATH).getFile();
		File baseOntology = new File(baseOntologyFileUrl);
		
		File expectedTargetFile = temporaryFolder.newFile("targetOntology.owl");
		
		File actualTargetFile = serializationOperations.startSerialization(baseOntology.getAbsolutePath(), ruleOntology.getAbsolutePath(), 
				uuidOfInformationPackage, ontologyIndividualList, expectedTargetFile.getAbsolutePath(), SerializationFormat.OAIORE);
		
		boolean exists = actualTargetFile.exists();
		assertTrue(exists);
		assertThat(actualTargetFile.getAbsolutePath(), is(equalTo(expectedTargetFile.getAbsolutePath())));
	}
	
	private OntologyIndividualCollection prepareTest_testSerialize_individuals(Uuid uuidOfInformationPackage) {
		OntologyIndividualCollection ontologyIndividualList = new OntologyIndividualCollectionImpl();
		
		OntologyClass dataObjectClass = new OntologyClassImpl(new NamespaceImpl("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP"), 
				new LocalNameImpl("Data_Object"));
		
		OntologyDatatypeProperty hasBooleanProperty1 = new OntologyBooleanPropertyImpl(
				"http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasBooleanProperty", 
				"hasBooleanProperty", "http://www.w3.org/2001/XMLSchema#boolean");
		hasBooleanProperty1.setPropertyValue(true);
		OntologyDatatypeProperty hasByteProperty1 = new OntologyBytePropertyImpl("hasByteProperty", false, "hasByteProperty", 
				"http://www.w3.org/2001/XMLSchema#byte");
		hasByteProperty1.setPropertyValue(12);
		
		OntologyIndividual ontologyIndividual1 = new OntologyIndividualImpl(dataObjectClass, uuidOfInformationPackage, new 
				DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		ontologyIndividual1.addDatatypeProperty(hasBooleanProperty1);
		ontologyIndividual1.addDatatypeProperty(hasByteProperty1);
		
		ontologyIndividualList.addIndividual(ontologyIndividual1);
		
		OntologyDatatypeProperty hasBooleanProperty2 = new OntologyBooleanPropertyImpl(
				"http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasBooleanProperty", 
				"hasBooleanProperty", "http://www.w3.org/2001/XMLSchema#boolean");
		hasBooleanProperty2.setPropertyValue(false);
		OntologyDatatypeProperty hasByteProperty2 = new OntologyBytePropertyImpl("hasByteProperty", false, "hasByteProperty", 
				"http://www.w3.org/2001/XMLSchema#byte");
		hasByteProperty2.setPropertyValue(-15);
		
		OntologyIndividual ontologyIndividual2 = new OntologyIndividualImpl(dataObjectClass, uuidOfInformationPackage, new 
				DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		ontologyIndividual2.addDatatypeProperty(hasBooleanProperty2);
		ontologyIndividual2.addDatatypeProperty(hasByteProperty2);
		ontologyIndividual2.setOntologyClass(dataObjectClass);
		
		ontologyIndividualList.addIndividual(ontologyIndividual2);
		return ontologyIndividualList;
	}
}
