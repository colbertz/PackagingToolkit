package de.feu.kdmp4.packagingtoolkit.server.serialization.classes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Ignore;
import org.junit.Test;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;

@Ignore
public class ReasonerTest {
	
	OWLOntologyManager ontologyManager;
	OWLDataFactory factory;
	OWLOntology baseontology;
	OWLReasoner reasoner;
	IRI ontologyIRI;
	
	@Test
	@Ignore
	public void addDataPropertyTestWithKDMP4HasEarnings() throws OWLOntologyCreationException, OWLOntologyStorageException {	
		OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
		OWLReasonerConfiguration config = new SimpleConfiguration();
		
		ontologyManager = OWLManager.createOWLOntologyManager();
		factory = ontologyManager.getOWLDataFactory();
		File ontologyfile = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "KDMP4-OAISrdf.owl");
		System.out.println(ontologyfile.getAbsolutePath());
		baseontology = ontologyManager.loadOntologyFromOntologyDocument(ontologyfile);

	
		OWLReasoner reasoner = reasonerFactory.createReasoner(baseontology, config);
		OWLIndividual individual = createIndividual();	
		
		OWLDataProperty dataprop = factory.getOWLDataProperty(IRI.create("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasEarnings"));	
		OWLLiteral literal = factory.getOWLLiteral(10.0);		
		OWLDataPropertyAssertionAxiom propertyAxiom = factory.getOWLDataPropertyAssertionAxiom(dataprop, individual,
				literal);
		OWLDatatype datatype = literal.getDatatype();
		//<xsd:pattern>0000-000(1-[5-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9])</xsd:pattern>
		
		//OWLLiteral literal2 = factory.getOWLLiteral(100);
		//OWLDatatype datatype2 = literal2.getDatatype();	
		ontologyManager.addAxiom(baseontology, propertyAxiom);
		
		OWLDataPropertyRangeAxiom owlDataPropertyRangeAxiom = factory.getOWLDataPropertyRangeAxiom(dataprop,datatype);
		//OWLDataPropertyRangeAxiom owlDataPropertyRangeAxiom2 = factory.getOWLDataPropertyRangeAxiom(dataprop,datatype2);
		
		assertFalse("does not contain: ", baseontology.containsAxiom(owlDataPropertyRangeAxiom));
		assertFalse("is NOT Entailed: ", reasoner.isEntailed(owlDataPropertyRangeAxiom));
		
		//assertFalse("does  contain: ", baseontology.containsAxiom(owlDataPropertyRangeAxiom2)); //!!!! ??????
		//assertTrue("is Entailed: ", reasoner.isEntailed(owlDataPropertyRangeAxiom2));
									
		assertTrue("is Consistent: ", reasoner.isConsistent()); //!!!! ??????
		
		reasoner = reasonerFactory.createReasoner(baseontology, config); // new reasoner
		assertFalse("is NOT Consistent: ", reasoner.isConsistent());
	}
	
	@Test
	public void addDataPropertyTestWithKDMP4() throws OWLOntologyCreationException, OWLOntologyStorageException {	
		OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
		OWLReasonerConfiguration config = new SimpleConfiguration();
		
		ontologyManager = OWLManager.createOWLOntologyManager();
		factory = ontologyManager.getOWLDataFactory();
		File ontologyfile = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "KDMP4-OAISrdf.owl");
		System.out.println(ontologyfile.getAbsolutePath());
		baseontology = ontologyManager.loadOntologyFromOntologyDocument(ontologyfile);

	
		OWLReasoner reasoner = reasonerFactory.createReasoner(baseontology, config);
		OWLIndividual individual = createIndividual();	
		
		OWLDataProperty dataprop = factory.getOWLDataProperty(IRI.create("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#has_ORCID"));	
		OWLLiteral literal = factory.getOWLLiteral("A");		
		OWLDataPropertyAssertionAxiom propertyAxiom = factory.getOWLDataPropertyAssertionAxiom(dataprop, individual,
				literal);
		OWLDatatype datatype = literal.getDatatype();
		//<xsd:pattern>0000-000(1-[5-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9])</xsd:pattern>
		
		OWLLiteral literal2 = factory.getOWLLiteral(100);
		OWLDatatype datatype2 = literal2.getDatatype();	
		ontologyManager.addAxiom(baseontology, propertyAxiom);
		
		OWLDataPropertyRangeAxiom owlDataPropertyRangeAxiom = factory.getOWLDataPropertyRangeAxiom(dataprop,datatype);
		OWLDataPropertyRangeAxiom owlDataPropertyRangeAxiom2 = factory.getOWLDataPropertyRangeAxiom(dataprop,datatype2);
		
		assertFalse("does not contain: ", baseontology.containsAxiom(owlDataPropertyRangeAxiom));
		assertFalse("is NOT Entailed: ", reasoner.isEntailed(owlDataPropertyRangeAxiom));
		
		assertFalse("does  contain: ", baseontology.containsAxiom(owlDataPropertyRangeAxiom2)); //!!!! ??????
		assertTrue("is Entailed: ", reasoner.isEntailed(owlDataPropertyRangeAxiom2));
									
		assertTrue("is Consistent: ", reasoner.isConsistent()); //!!!! ??????
		
		reasoner = reasonerFactory.createReasoner(baseontology, config); // new reasoner
		assertFalse("is NOT Consistent: ", reasoner.isConsistent());
	}
	
	@Test
	@Ignore
	public void addDataPropertyTest2() throws OWLOntologyCreationException, OWLOntologyStorageException {	
		OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
		OWLReasonerConfiguration config = new SimpleConfiguration();
		
		ontologyManager = OWLManager.createOWLOntologyManager();
		factory = ontologyManager.getOWLDataFactory();
		//File ontologyfile = new File("src\\main\\resources\\"+baseOntology+".owl");
		File ontologyfile = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "premis_v1.rdf");
		System.out.println(ontologyfile.getAbsolutePath());
		baseontology = ontologyManager.loadOntologyFromOntologyDocument(ontologyfile);

	
		OWLReasoner reasoner = reasonerFactory.createReasoner(baseontology, config);
		OWLIndividual individual = createIndividual();	
		
		OWLDataProperty dataprop = factory.getOWLDataProperty(IRI.create("http://www.loc.gov/premis/rdf/v1#hasSize"));	
		OWLLiteral literal = factory.getOWLLiteral("ABC");		
		OWLDataPropertyAssertionAxiom propertyAxiom = factory.getOWLDataPropertyAssertionAxiom(dataprop, individual,
				literal);
		OWLDatatype datatype = literal.getDatatype();
		
		OWLLiteral literal2 = factory.getOWLLiteral(100);
		OWLDatatype datatype2 = literal2.getDatatype();	
		ontologyManager.addAxiom(baseontology, propertyAxiom);
		
		OWLDataPropertyRangeAxiom owlDataPropertyRangeAxiom = factory.getOWLDataPropertyRangeAxiom(dataprop,datatype);
		OWLDataPropertyRangeAxiom owlDataPropertyRangeAxiom2 = factory.getOWLDataPropertyRangeAxiom(dataprop,datatype2);
		
		assertFalse("does not contain: ", baseontology.containsAxiom(owlDataPropertyRangeAxiom));
		assertFalse("is NOT Entailed: ", reasoner.isEntailed(owlDataPropertyRangeAxiom));
		
		assertFalse("does  contain: ", baseontology.containsAxiom(owlDataPropertyRangeAxiom2)); //!!!! ??????
		assertTrue("is Entailed: ", reasoner.isEntailed(owlDataPropertyRangeAxiom2));
									
		assertTrue("is Consistent: ", reasoner.isConsistent()); //!!!! ??????
		
		reasoner = reasonerFactory.createReasoner(baseontology, config); // new reasoner
		assertFalse("is NOT Consistent: ", reasoner.isConsistent());
	}
	

	protected OWLIndividual createIndividual() {
		OWLClass premisClass = factory.getOWLClass(IRI.create("http://www.loc.gov/premis/rdf/v1#Bitstream"));
		OWLIndividual individual = factory.getOWLNamedIndividual(IRI.create(ontologyIRI+"testBitstream"));
		AddAxiom addAxiom = new AddAxiom(baseontology, factory.getOWLClassAssertionAxiom(premisClass, individual ));
        // We now use the manager to apply the change
		ontologyManager.applyChange(addAxiom);
		//ontologyManager.addAxiom(baseontology,factory.getOWLClassAssertionAxiom(premisClass, individual ));
		return individual;
	}
	

}