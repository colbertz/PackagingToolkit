package de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.SerializationConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.server.triplestore.facades.PackagingTripleStoreFacade;

public interface SerializationService {
	void setSerializationOperations(SerializationOperations serializationOperations);
	void setSerializationTripleStoreFacade(PackagingTripleStoreFacade serializationTripleStoreFacade);
	void setSerializationConfigurationFacade(SerializationConfigurationFacade serializationConfigurationFacade);
	/**
	 * Starts the serialization of an ontology. 
	 * @param ontologyIndividualList The individuals that have to be serialized.
	 * @param uuidOfInformationPackage The uuid of the information package that should be 
	 * serialized in a manifest file.
	 * @param serializationFormat The format that is used for the manifest file. At the moment only
	 * OAI-ORE is possible.
	 * @return The file that contains the manifest.
	 */
	File startSerialization(OntologyIndividualCollection ontologyIndividualList, Uuid uuidOfInformationPackage,
			SerializationFormat serializationFormat);
}
