package de.feu.kdmp4.packagingtoolkit.server.serialization.facade;

import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationService;

// DELETE_ME
public class SessionSerializationFacade {
	private SerializationService serializationService;
	
	public void startSerialization(OntologyIndividualCollection ontologyIndividualList, Uuid uuidOfInformationPackage) {
		serializationService.startSerialization(ontologyIndividualList, uuidOfInformationPackage, SerializationFormat.OAIORE);
	}
	
	public void setSerializationService(SerializationService serializationService) {
		this.serializationService = serializationService;
	}
}
