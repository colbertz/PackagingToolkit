package de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

public interface SerializationOperations {
	//TripleStatementList startSerialization(String baseOntologyPath, String ruleOntologyPath, WorkingDirectory workingDirectory,
	//		Uuid uuid);

	/*TripleStatementList startSerialization(String baseOntologyPath, String ruleOntologyPath, Uuid uuid,
			OntologyIndividualList ontologyIndividualList);*/

	/**
	 * Starts the serialization of an ontology with OAI-ORE. 
	 * @param baseOntologyPath The path to the ontology that is serialized. 
	 * @param ruleOntologyPath The path to the ontology that contains the rules for Serialization.
	 * @param workingDirectory The working directory of the server.
	 * @param uuid The uuid of the information package the ontology belongs to.
	 * @param serializationFormat The format that is used for the manifest file. At the moment only
	 * OAI-ORE is possible.
	 * @return The list of statements in RDF.
	 */
	File startSerialization(String baseOntologyPath, String ruleOntologyPath, Uuid uuidOfInformationPackage,
			OntologyIndividualCollection ontologyIndividualList, String targetPathForOntologyFile,
			SerializationFormat serializationFormat);

}
