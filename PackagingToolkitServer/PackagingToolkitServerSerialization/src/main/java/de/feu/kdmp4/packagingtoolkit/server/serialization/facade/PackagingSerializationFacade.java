package de.feu.kdmp4.packagingtoolkit.server.serialization.facade;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationService;

public class PackagingSerializationFacade {
	private SerializationService serializationService;
	
	/**
	 * Creates a manifest file. 
	 * @param ontologyIndividualList The individuals that have to be serialized.
	 * @param uuidOfInformationPackage The uuid of the information package that should be 
	 * serialized in a manifest file.
	 * @param serializationFormat The format that is used for the manifest file. At the moment only
	 * OAI-ORE is possible.
	 * @return The file that contains the manifest.
	 */
	public File createManifest(final Uuid uuidOfInformationPackage, final OntologyIndividualCollection ontologyIndividualList,
			final SerializationFormat serializationFormat) {
		return serializationService.startSerialization(ontologyIndividualList, uuidOfInformationPackage, serializationFormat);
	}
	
	public void setSerializationService(final SerializationService serializationService) {
		this.serializationService = serializationService;
	}
}
