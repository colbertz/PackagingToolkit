package de.feu.kdmp4.packagingtoolkit.server.serialization.classes;

import java.io.File;
import java.math.BigInteger;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddImport;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.swrlapi.core.SWRLRuleEngine;
import org.swrlapi.factory.SWRLAPIFactory;

import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationOperations;


public class SerializationOperationsOwlApiImpl implements SerializationOperations {
	// **************** Attributes *****************
	private OWLOntologyManager ontologyManager;
	private OWLOntology baseOntology;
	private OWLOntology ruleOntology;
	private OWLReasoner reasoner;
	private OWLDataFactory owlDataFactory;
	
	private static final String IRI_OAIS_IP = "http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP";
	
	public SerializationOperationsOwlApiImpl() {
		 ontologyManager = OWLManager.createOWLOntologyManager();
		 owlDataFactory = ontologyManager.getOWLDataFactory();
	}
	
	private void addIndividuals(final OntologyIndividualCollection individualList) {
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
			final OntologyClass classOfIndiviual = ontologyIndividual.getOntologyClass();
			final OWLClass owlApiClass = lookForOwlClassByIri(classOfIndiviual.getFullName());
			final IRI iriOfIndividual = IRI.create(ontologyIndividual.getIri().toString());
			final OWLNamedIndividual owlApiIndividual = owlDataFactory.getOWLNamedIndividual(iriOfIndividual);
			final OWLClassAssertionAxiom addIndividualAxiom = owlDataFactory.getOWLClassAssertionAxiom(owlApiClass, owlApiIndividual);

			final boolean isSatisfiable = reasoner.isSatisfiable(owlApiClass);
	        if(isSatisfiable){					
				ontologyManager.addAxiom(ruleOntology, addIndividualAxiom);
				addDatatypeProperties(ontologyIndividual, owlApiIndividual);
				addObjectProperties(ontologyIndividual, owlApiIndividual);
	        }
		}
	}
	
	private void addObjectProperties(final OntologyIndividual ontologyIndividual, final OWLIndividual owlIndividual) { 
		for (int i = 0; i < ontologyIndividual.getObjectPropertiesCount(); i++) {
			final OntologyObjectProperty ontologyObjectProperty = ontologyIndividual.getObjectPropertyAt(i);
			final IRI iri = IRI.create(ontologyObjectProperty.getPropertyId());
			final OWLObjectProperty owlObjectProperty = owlDataFactory.getOWLObjectProperty(iri);
			final OntologyIndividualCollection ontologyIndividuals = ontologyObjectProperty.getPropertyValue();
			final IRI iriOfRange = IRI.create(ontologyIndividual.getIri().toString());
			final OWLNamedIndividual namedIndividual = owlDataFactory.getOWLNamedIndividual(iriOfRange);
			
			for (int j = 0; j < ontologyIndividuals.getIndiviualsCount(); j++) {
				final OntologyIndividual theOtherOntologyIndividual = ontologyIndividuals.getIndividual(j);
				final IRI iriOfTheOtherOntologyIndividual = IRI.create(theOtherOntologyIndividual.getIri().toString());
				final OWLNamedIndividual theOtherNamedIndividual = owlDataFactory.
						getOWLNamedIndividual(iriOfTheOtherOntologyIndividual);
				final OWLObjectPropertyAssertionAxiom objectPropertyAssertionAxiom = owlDataFactory.
						getOWLObjectPropertyAssertionAxiom(owlObjectProperty, namedIndividual, theOtherNamedIndividual);
				ontologyManager.addAxiom(ruleOntology, objectPropertyAssertionAxiom);
			}
		}
	}
	
	/**
	 * Adds the datatype properties of an individual to the rule ontology. 
	 * @param ontologyIndividual The individual that contains the datatype properties we want to insert into the
	 * rule ontology. 
	 * @param owlIndividual The individual the properties are added to.
	 */
	private void addDatatypeProperties(final OntologyIndividual ontologyIndividual, final OWLIndividual owlIndividual) {
		for (int i = 0; i < ontologyIndividual.getDatatypePropertiesCount(); i++) {
			final OntologyDatatypeProperty ontologyDatatypeProperty = ontologyIndividual.getDatatypePropertyAt(i);
			final IRI iri = IRI.create(ontologyDatatypeProperty.getPropertyId());
			final OWLDataProperty owlDataProperty = owlDataFactory.getOWLDataProperty(iri);
			OWLDataPropertyAssertionAxiom dataPropertyAssertionAxiom = null;
			final Object valueOfProperty = ontologyDatatypeProperty.getPropertyValue(); 
			
			if (ontologyDatatypeProperty.isBooleanProperty()) {
				final boolean value = (boolean)valueOfProperty;
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isByteProperty()) {
				final short value = (short)valueOfProperty;
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isDateProperty()) {
				final String value = ontologyDatatypeProperty.toString();
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isDateTimeProperty()) {
				final String value = ontologyDatatypeProperty.toString();
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isDoubleProperty()) {
				final double value = (double)valueOfProperty;
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isDurationProperty()) {
				final OntologyDuration duration = (OntologyDuration)valueOfProperty;
				final OWLLiteral value = owlDataFactory.getOWLLiteral(duration.toString());
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isFloatProperty()) {
				final float value = (float)valueOfProperty;
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isIntegerProperty()) {
				final long value = (long)valueOfProperty;
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isLongProperty()) {
				final BigInteger value = (BigInteger)valueOfProperty;
				final OWLLiteral literal = owlDataFactory.getOWLLiteral(value.toString());
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, literal);
			} else if (ontologyDatatypeProperty.isObjectProperty()) { 
			} else if (ontologyDatatypeProperty.isShortProperty()) {
				final int value = (int)valueOfProperty;
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isStringProperty()) {
				final String value = (String)valueOfProperty;
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else if (ontologyDatatypeProperty.isTimeProperty()) {
				final String value = ontologyDatatypeProperty.toString();
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			} else {
				final OWLLiteral value = owlDataFactory.getOWLLiteral(valueOfProperty.toString());
				dataPropertyAssertionAxiom = owlDataFactory.getOWLDataPropertyAssertionAxiom(owlDataProperty, owlIndividual, value);
			}
			
			ontologyManager.addAxiom(ruleOntology, dataPropertyAssertionAxiom);
		}
	}
	
	// *************** Public methods ***************
	@Override
	public File startSerialization(final String baseOntologyPath, final String ruleOntologyPath, 
			final Uuid uuidOfInformationPackage, final OntologyIndividualCollection ontologyIndividualList, 
			final String targetPathForOntologyFile, final SerializationFormat serializationFormat) {
		try {
			if (baseOntology == null) {
				baseOntology = loadOntology(ontologyManager, baseOntologyPath);
			}
			reasoner = createReasoner(baseOntology);
			
			switch(serializationFormat) {
				case OAIORE: return serializeWithOAIORE(ruleOntologyPath, targetPathForOntologyFile, ontologyIndividualList);
			}
		} catch (OWLOntologyCreationException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} 
		return null;
	}
	
	private File serializeWithOAIORE(final String ruleOntologyPath, final String targetPathForOntologyFile,
			final OntologyIndividualCollection ontologyIndividualList) {
		File ontologyTargetFile = null;
		try {
			if (ruleOntology == null ) {
				 ruleOntology = loadOntology(ontologyManager, ruleOntologyPath);
			}
			final SWRLRuleEngine ruleEngine = SWRLAPIFactory.createSWRLRuleEngine(ruleOntology);
			 
			addIndividuals(ontologyIndividualList);
			 //ruleOntology = serializeToOaiOre(ruleOntology);
			ruleEngine.infer();
			 
			//addToRuleOntology(baseOntology, ruleOntology, reasoner, ontologyManager);
			
			ontologyTargetFile = new File(targetPathForOntologyFile);
			//TripleStatementList tripleStatementList = createTripleStatementList(ruleOntology);
			OWLImportsDeclaration importDeclaration = ontologyManager.getOWLDataFactory().
					getOWLImportsDeclaration(IRI.create(IRI_OAIS_IP));
			ontologyManager.applyChange(new AddImport(ruleOntology, importDeclaration));
			//File outputFile = serializeToOaiOre(ruleOntology, ontologyManager, uuid, workingDirectory);
			
			//return outputFile;
			IRI outputIri = IRI.create(ontologyTargetFile);
			ontologyManager.saveOntology(ruleOntology, outputIri);
		} catch (OWLOntologyStorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OWLOntologyCreationException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} 
		return ontologyTargetFile;
	}
	
	private OWLClass lookForOwlClassByIri(final String iri) {
		for (final OWLClass owlClass: baseOntology.getClassesInSignature()) {
			final String theOtherIri = owlClass.getIRI().toString();
			if (theOtherIri.equals(iri)) {
				return owlClass;
			}
		}
		
		// Only for satifing the compiler. May not be reached.
		return null;
	}
	
	// ***************** Private methods **************
	/**
	 * Adds the axioms from the base ontology to the rule ontology.
	 */
	private void addToRuleOntology(OWLOntology baseOntology, OWLOntology ruleOntology, 
			OWLReasoner reasoner, OWLOntologyManager ontologyManager) {
		for(OWLClass owlClass: baseOntology.getClassesInSignature()) {
			boolean satisfied = reasoner.isSatisfiable(owlClass);
			
			if (satisfied) {
				Set<OWLClassAssertionAxiom> assertionAxioms = baseOntology.
						getClassAssertionAxioms(owlClass);
				
				for (OWLClassAssertionAxiom assertionAxiom: assertionAxioms) {
					ontologyManager.addAxiom(ruleOntology, assertionAxiom);
				}
			}
		}
	}

	/**
	 * Creates the reasoner and initializes it with the configuration. Changes on the reasoner
	 * have to be done in this method.
	 * @param ontology The ontology the reasoner should work with.
	 * @return The newly created reasoner.
	 */
	private OWLReasoner createReasoner(OWLOntology ontology) {
		ConsoleProgressMonitor progressMonitor = new ConsoleProgressMonitor();
		OWLReasonerConfiguration configuration = new SimpleConfiguration(progressMonitor);
		OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
		
		return reasonerFactory.createReasoner(ontology, configuration);
	}
	
	/**
	 * Loads an ontology in the manager.
	 * @param ontologyManager The ontology manager the ontology is loaded in.
	 * @param ontologyFilePath The path of the ontology to load.
	 * @return The ontology.
	 * @throws OWLOntologyCreationException
	 */
	private OWLOntology loadOntology(OWLOntologyManager ontologyManager, String ontologyFilePath) throws OWLOntologyCreationException {	
		File ontologyFile = new File(ontologyFilePath);
		return ontologyManager.loadOntologyFromOntologyDocument(ontologyFile);
	}
}