package de.feu.kdmp4.packagingtoolkit.server.serialization.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.SerializationConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationOperations;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationService;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.server.triplestore.facades.PackagingTripleStoreFacade;

public class SerializationServiceImpl implements SerializationService {
	private SerializationOperations serializationOperations;
	private SerializationConfigurationFacade serializationConfigurationFacade;
	private PackagingTripleStoreFacade serializationTripleStoreFacade;
	
	@Override
	public File startSerialization(final OntologyIndividualCollection ontologyIndividualList, final Uuid uuidOfInformationPackage,
			final SerializationFormat serializationFormat) {
		final ServerConfigurationData serverConfigurationData = serializationConfigurationFacade.readServerConfiguration();
		final String baseOntologyPath = serverConfigurationData.getBaseOntologyPath();
		final String ruleOntologyPath = serverConfigurationData.getRuleOntologyPath();
		final String targetPathForOntologyFile = serverConfigurationData.getTemporaryDirectory() + File.separator + uuidOfInformationPackage + 
				PackagingToolkitFileUtils.OWL_FILE_EXTENSION;
		
		final File manifestFile = serializationOperations.startSerialization(baseOntologyPath, ruleOntologyPath, 
				uuidOfInformationPackage, ontologyIndividualList, targetPathForOntologyFile, serializationFormat);
		return manifestFile;
		/*TripleStatementList tripleStatementList = serializationOperations.startSerialization(baseOntologyPath, ruleOntologyPath, 
						uuidOfInformationPackage, ontologyIndividualList);
		serializationTripleStoreFacade.saveTriples(tripleStatementList, uuidOfInformationPackage);*/
	}
	
	@Override  
	public void setSerializationOperations(SerializationOperations serializationOperations) {
		this.serializationOperations = serializationOperations;
	}

	@Override
	public void setSerializationConfigurationFacade(SerializationConfigurationFacade serializationConfigurationFacade) {
		this.serializationConfigurationFacade = serializationConfigurationFacade;
	}
	
	@Override
	// DELETE_ME Werde ich wahrscheinlich nicht mehr brauchen. 
	public void setSerializationTripleStoreFacade(PackagingTripleStoreFacade serializationTripleStoreFacade) {
		this.serializationTripleStoreFacade = serializationTripleStoreFacade;
	}
}
