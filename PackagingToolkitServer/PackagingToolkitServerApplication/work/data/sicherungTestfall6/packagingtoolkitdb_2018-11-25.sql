-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: packagingtoolkitdb
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classes_in_view`
--

DROP TABLE IF EXISTS `classes_in_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes_in_view` (
  `view_id_fk` int(11) NOT NULL,
  `ontology_class_id_fk` int(11) NOT NULL,
  KEY `FKfa7hi72ws0gt51oxrr33a2f4d` (`ontology_class_id_fk`),
  KEY `FKqlbamwhdijbjocwew8pv9rakl` (`view_id_fk`),
  CONSTRAINT `FKfa7hi72ws0gt51oxrr33a2f4d` FOREIGN KEY (`ontology_class_id_fk`) REFERENCES `ontology_class` (`ontology_class_id`),
  CONSTRAINT `FKqlbamwhdijbjocwew8pv9rakl` FOREIGN KEY (`view_id_fk`) REFERENCES `view` (`view_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes_in_view`
--

LOCK TABLES `classes_in_view` WRITE;
/*!40000 ALTER TABLE `classes_in_view` DISABLE KEYS */;
INSERT INTO `classes_in_view` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `classes_in_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `digital_object`
--

DROP TABLE IF EXISTS `digital_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `digital_object` (
  `digital_object_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_path_in_storage` varchar(255) DEFAULT NULL,
  `md5checksum` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`digital_object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `digital_object`
--

LOCK TABLES `digital_object` WRITE;
/*!40000 ALTER TABLE `digital_object` DISABLE KEYS */;
INSERT INTO `digital_object` VALUES (1,'testDigitalObject1.txt','d116a860911d552e90543cf54e0c8b0e','a7dd0d45-d39b-4ce5-98c3-4b7cf6d49fd9'),(2,'testDigitalObject2.txt','e22045155decc195b2e07da303a01a46','59b04f82-5dce-4b2a-8fab-7f4ad569291d');
/*!40000 ALTER TABLE `digital_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `digital_object_has_metadata`
--

DROP TABLE IF EXISTS `digital_object_has_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `digital_object_has_metadata` (
  `digital_object_has_metadata_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  `digital_object_digital_object_id` bigint(20) DEFAULT NULL,
  `metadata_metadata_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`digital_object_has_metadata_id`),
  KEY `FKeg1bk723g26bfi22dpbpithbc` (`digital_object_digital_object_id`),
  KEY `FKkfhdq2kb7laj5tsvp57hyk80l` (`metadata_metadata_id`),
  CONSTRAINT `FKeg1bk723g26bfi22dpbpithbc` FOREIGN KEY (`digital_object_digital_object_id`) REFERENCES `digital_object` (`digital_object_id`),
  CONSTRAINT `FKkfhdq2kb7laj5tsvp57hyk80l` FOREIGN KEY (`metadata_metadata_id`) REFERENCES `metadata` (`metadata_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `digital_object_has_metadata`
--

LOCK TABLES `digital_object_has_metadata` WRITE;
/*!40000 ALTER TABLE `digital_object_has_metadata` DISABLE KEYS */;
INSERT INTO `digital_object_has_metadata` VALUES (1,'LF',1,1),(2,'US-ASCII',1,2),(3,'c3986c6d-6dd0-458a-92af-9e59522edad1',1,3),(4,'6402',1,4),(5,'/home/christopher/Dokumente/Studium/Masterarbeit/workspace/PackagingToolkit/PackagingToolkitMediator/temp/c3986c6d-6dd0-458a-92af-9e59522edad1',1,5),(6,'d116a860911d552e90543cf54e0c8b0e',1,6),(7,'1543149825000',1,7),(8,'LF',2,1),(9,'US-ASCII',2,2),(10,'9ce85bf9-7abe-41e8-a735-89eb0b055d69',2,3),(11,'39767',2,4),(12,'/home/christopher/Dokumente/Studium/Masterarbeit/workspace/PackagingToolkit/PackagingToolkitMediator/temp/9ce85bf9-7abe-41e8-a735-89eb0b055d69',2,5),(13,'e22045155decc195b2e07da303a01a46',2,6),(14,'1543150003000',2,7);
/*!40000 ALTER TABLE `digital_object_has_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `digital_object_information_packages`
--

DROP TABLE IF EXISTS `digital_object_information_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `digital_object_information_packages` (
  `digital_object_entity_digital_object_id` bigint(20) NOT NULL,
  `information_packages_information_package_id` bigint(20) NOT NULL,
  KEY `FKqx408io641pklnosap8w33n36` (`information_packages_information_package_id`),
  KEY `FKcfhx5qxntgo3kkofdbf6cva4a` (`digital_object_entity_digital_object_id`),
  CONSTRAINT `FKcfhx5qxntgo3kkofdbf6cva4a` FOREIGN KEY (`digital_object_entity_digital_object_id`) REFERENCES `digital_object` (`digital_object_id`),
  CONSTRAINT `FKqx408io641pklnosap8w33n36` FOREIGN KEY (`information_packages_information_package_id`) REFERENCES `information_package` (`information_package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `digital_object_information_packages`
--

LOCK TABLES `digital_object_information_packages` WRITE;
/*!40000 ALTER TABLE `digital_object_information_packages` DISABLE KEYS */;
INSERT INTO `digital_object_information_packages` VALUES (1,3),(1,4),(2,5);
/*!40000 ALTER TABLE `digital_object_information_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_package`
--

DROP TABLE IF EXISTS `information_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_package` (
  `information_package_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `package_type` int(11) DEFAULT NULL,
  `serialization_format` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`information_package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_package`
--

LOCK TABLES `information_package` WRITE;
/*!40000 ALTER TABLE `information_package` DISABLE KEYS */;
INSERT INTO `information_package` VALUES (1,'2018-11-25 13:27:14','2018-11-25 13:32:59',0,0,'first information package','89997194-54ad-4be1-8cc0-ccdf1befde21'),(2,'2018-11-25 13:36:56','2018-11-25 13:42:45',0,0,'second information package','df0da9bf-e080-4c27-a0b0-e3b99bae643c'),(3,'2018-11-25 13:43:44','2018-11-25 13:44:27',0,0,'third information package','f2e46e91-b6c8-4eba-8a93-bd4b8c6a6676'),(4,'2018-11-25 13:44:53','2018-11-25 13:46:27',0,0,'fourth information package','a12c7ac3-f8c2-45a3-9f1f-873771967e18'),(5,'2018-11-25 13:46:42','2018-11-25 13:50:15',0,0,'fifth information package','9fb2a3b2-77ca-46e4-a81c-ac00985cbd05');
/*!40000 ALTER TABLE `information_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata`
--

DROP TABLE IF EXISTS `metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata` (
  `metadata_id` int(11) NOT NULL AUTO_INCREMENT,
  `metadata_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`metadata_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata`
--

LOCK TABLES `metadata` WRITE;
/*!40000 ALTER TABLE `metadata` DISABLE KEYS */;
INSERT INTO `metadata` VALUES (1,'linebreak'),(2,'charset'),(3,'filename'),(4,'size'),(5,'filepath'),(6,'md5checksum'),(7,'fslastmodified');
/*!40000 ALTER TABLE `metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ontology_class`
--

DROP TABLE IF EXISTS `ontology_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ontology_class` (
  `ontology_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) NOT NULL,
  `namespace` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ontology_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ontology_class`
--

LOCK TABLES `ontology_class` WRITE;
/*!40000 ALTER TABLE `ontology_class` DISABLE KEYS */;
INSERT INTO `ontology_class` VALUES (1,'Content_Information','http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP'),(2,'Preservation_Description_Information','http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP');
/*!40000 ALTER TABLE `ontology_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view`
--

DROP TABLE IF EXISTS `view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view` (
  `view_id` int(11) NOT NULL AUTO_INCREMENT,
  `view_name` varchar(255) NOT NULL,
  PRIMARY KEY (`view_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view`
--

LOCK TABLES `view` WRITE;
/*!40000 ALTER TABLE `view` DISABLE KEYS */;
INSERT INTO `view` VALUES (1,'compl');
/*!40000 ALTER TABLE `view` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-25 13:51:37
