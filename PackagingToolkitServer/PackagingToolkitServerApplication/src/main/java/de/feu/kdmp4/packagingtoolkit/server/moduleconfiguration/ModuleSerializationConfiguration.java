package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.SerializationConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.serialization.classes.SerializationOperationsOwlApiImpl;
import de.feu.kdmp4.packagingtoolkit.server.serialization.classes.SerializationServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.serialization.facade.PackagingSerializationFacade;
import de.feu.kdmp4.packagingtoolkit.server.serialization.facade.SessionSerializationFacade;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationOperations;
import de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.SerializationService;
import de.feu.kdmp4.server.triplestore.facades.PackagingTripleStoreFacade;

@Configuration
public class ModuleSerializationConfiguration {
	@Autowired
	private SerializationConfigurationFacade serializationConfigurationFacade;
	@Autowired
	private PackagingTripleStoreFacade serializationTripleStoreFacade; 
	
	@Bean
	public SerializationOperations serializationOperations() {
		SerializationOperations serializationOperations = new SerializationOperationsOwlApiImpl();
		
		return serializationOperations;
	}
	
	@Bean
	public SerializationService serializationService() {
		SerializationService serializationService = new SerializationServiceImpl();
		serializationService.setSerializationConfigurationFacade(serializationConfigurationFacade);
		serializationService.setSerializationOperations(serializationOperations());
		serializationService.setSerializationTripleStoreFacade(serializationTripleStoreFacade);
		
		return serializationService;
	}
	
	@Bean
	public SessionSerializationFacade sessionSerializationFacade() {
		SessionSerializationFacade sessionSerializationFacade = new SessionSerializationFacade();
		sessionSerializationFacade.setSerializationService(serializationService());
		
		return sessionSerializationFacade;
	}
	
	@Bean
	public PackagingSerializationFacade packagingSerializationFacade() {
		PackagingSerializationFacade packagingSerializationFacade = new PackagingSerializationFacade();
		packagingSerializationFacade.setSerializationService(serializationService());
		return packagingSerializationFacade;
	}
}
