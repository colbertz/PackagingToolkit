/**
 * Contains the classes that configure the modules of the server. 
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;