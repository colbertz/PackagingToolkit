package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.database.facades.SessionDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ObjectPropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.serialization.facade.SessionSerializationFacade;
import de.feu.kdmp4.packagingtoolkit.server.session.classes.SessionServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.session.facade.PackagingSessionFacade;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.SessionService;
import de.feu.kdmp4.server.triplestore.facades.SessionTripleStoreFacade;

@Configuration
public class ModuleSessionConfiguration {
	@Autowired
	private SessionDatabaseFacade sessionDatabaseFacade;
	@Autowired
	private SessionSerializationFacade sessionSerializationFacade; 
	@Autowired
	private SessionTripleStoreFacade sessionTripleStoreFacade;
	@Autowired
	private OntologyCache ontologyCache;
	@Autowired
	private PropertyFactory propertyFactory;
	@Autowired
	private ObjectPropertyFactory objectPropertyFactory;
	
	@Bean
	public SessionService sessionService() {
		SessionService sessionService = new SessionServiceImpl();
		sessionService.setSessionTripleStoreFacade(sessionTripleStoreFacade);
		sessionService.setSessionDatabaseFacade(sessionDatabaseFacade);
		sessionService.setOntologyCache(ontologyCache);
		sessionService.setPropertyFactory(propertyFactory);
		sessionService.setObjectPropertyFactory(objectPropertyFactory);
		
		return sessionService;
	}
	
	@Bean
	public PackagingSessionFacade packagingSessionFacade() {
		PackagingSessionFacade packagingSessionFacade = new PackagingSessionFacade();
		packagingSessionFacade.setSessionService(sessionService());
		
		return packagingSessionFacade;
	}
}
