package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.StorageConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.StorageDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.classes.StorageOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.server.storage.classes.StorageServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.storage.facade.PackagingStorageFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageOperations;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageService;
import de.feu.kdmp4.server.triplestore.facades.StorageTripleStoreFacade;

@Configuration
public class ModuleStorageConfiguration {
	@Autowired
	private StorageConfigurationFacade storageConfigurationFacade; 
	@Autowired
	private StorageDatabaseFacade storageDatabaseFacade;
	@Autowired
	private StorageTripleStoreFacade storageTripleStoreFacade;
	
	@Bean
	public StorageOperations storageOperations() {
		StorageOperations storageOperations = new StorageOperationsImpl();
		
		return storageOperations;
	}
	
	@Bean
	public StorageService storageService() {
		StorageService storageService = new StorageServiceImpl();
		storageService.setStorageConfigurationFacade(storageConfigurationFacade);
		storageService.setStorageDatabaseFacade(storageDatabaseFacade);
		storageService.setStorageTripleStoreFacade(storageTripleStoreFacade);
		storageService.setStorageOperations(storageOperations());
		
		return storageService;
	}
	
	@Bean
	public PackagingStorageFacade packagingStorageFacade() {
		PackagingStorageFacade packagingStorageFacade = new PackagingStorageFacade();
		packagingStorageFacade.setStorageService(storageService());
		
		return packagingStorageFacade;
	}
}
