package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.TripleStoreConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationService;
import de.feu.kdmp4.server.triplestore.classes.TripleStoreOperationsRdf4JImpl;
import de.feu.kdmp4.server.triplestore.classes.TripleStoreServiceImpl;
import de.feu.kdmp4.server.triplestore.facades.OntologyTripleStoreFacade;
import de.feu.kdmp4.server.triplestore.facades.PackagingTripleStoreFacade;
import de.feu.kdmp4.server.triplestore.facades.SessionTripleStoreFacade;
import de.feu.kdmp4.server.triplestore.facades.StorageTripleStoreFacade;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreOperations;
import de.feu.kdmp4.server.triplestore.interfaces.TripleStoreService;

@Configuration
public class ModuleTripleStoreConfiguration {
	@Autowired
	private TripleStoreConfigurationFacade tripleStoreConfigurationFacade;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private ConfigurationOperations configurationOperations;
	
	@Bean
	public TripleStoreOperations tripleStoreOperations() {
		TripleStoreOperations tripleStoreOperations = new TripleStoreOperationsRdf4JImpl();
		
		return tripleStoreOperations;
	}
	
	@Bean 
	public TripleStoreService tripleStoreService() {
		TripleStoreService tripleStoreService = new TripleStoreServiceImpl();
		tripleStoreService.setTripleStoreConfigurationFacade(tripleStoreConfigurationFacade);
		tripleStoreService.setTripleStoreOperations(tripleStoreOperations());
		
		return tripleStoreService;
	}
	
	@Bean
	public OntologyTripleStoreFacade ontologyTripleStoreFacade() {
		OntologyTripleStoreFacade ontologyTripleStoreFacade = new OntologyTripleStoreFacade();
		ontologyTripleStoreFacade.setTripleStoreService(tripleStoreService());
		
		return ontologyTripleStoreFacade;
	}
	
	@Bean
	public PackagingTripleStoreFacade packagingTripleStoreFacade() {
		PackagingTripleStoreFacade packagingTripleStoreFacade = new PackagingTripleStoreFacade();
		packagingTripleStoreFacade.setTripleStoreService(tripleStoreService());
		
		return packagingTripleStoreFacade;
	}
	
	@Bean
	public SessionTripleStoreFacade sessionTripleStoreFacade() {
		SessionTripleStoreFacade sessionTripleStoreFacade = new SessionTripleStoreFacade();
		sessionTripleStoreFacade.setTripleStoreService(tripleStoreService());
		
		return sessionTripleStoreFacade;
	}
	
	@Bean
	public StorageTripleStoreFacade storageTripleStoreFacade() {
		StorageTripleStoreFacade storageTripleStoreFacade = new StorageTripleStoreFacade();
		storageTripleStoreFacade.setTripleStoreService(tripleStoreService());
		
		return storageTripleStoreFacade;
	}
}
