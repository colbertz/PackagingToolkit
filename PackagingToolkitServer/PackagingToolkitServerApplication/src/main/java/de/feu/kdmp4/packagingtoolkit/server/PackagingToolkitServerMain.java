package de.feu.kdmp4.packagingtoolkit.server;

import javax.annotation.PostConstruct;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import de.feu.kdmp4.packagingtoolkit.utils.LogUtils;

/**
 * Contains the main method for the application and references to the configuration.
 * @author Christopher Olbertz
 *
 */
@SpringBootApplication
@Import({ServerApplicationConfiguration.class})
@ComponentScan(basePackages = "de.feu.kdmp4.packagingtoolkit")
public class PackagingToolkitServerMain extends SpringBootServletInitializer {
	@PostConstruct
	public void init() {
		
	}
	
	public static void main(String[] args) {
		SpringApplicationBuilder application = new PackagingToolkitServerMain()
				.configure(new SpringApplicationBuilder(PackagingToolkitServerMain.class));
		LogUtils.logInfo("Server started");
		application.run();
	}
}