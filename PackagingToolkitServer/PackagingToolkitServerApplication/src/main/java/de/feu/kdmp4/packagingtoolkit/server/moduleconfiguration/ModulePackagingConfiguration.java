package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.PackagingConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.PackagingDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.metadata.facades.PackagingMetaDataFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.facades.PackagingOntologyFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.packaging.classes.PackagingOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.server.packaging.classes.PackagingServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingOperations;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingService;
import de.feu.kdmp4.packagingtoolkit.server.serialization.facade.PackagingSerializationFacade;
import de.feu.kdmp4.packagingtoolkit.server.session.facade.PackagingSessionFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.facade.PackagingStorageFacade;

@Configuration
public class ModulePackagingConfiguration {
	@Autowired
	private PackagingMetaDataFacade packagingMetaDataFacade; 
	@Autowired
	private PackagingSessionFacade packagingSessionFacade;
	@Autowired
	private PackagingConfigurationFacade packagingConfigurationFacade;
	@Autowired
	private PackagingOntologyFacade packagingOntologyFacade;
	@Autowired
	private PackagingStorageFacade packagingStorageFacade; 
	@Autowired
	private PackagingDatabaseFacade packagingDatabaseFacade;
	@Autowired
	private OntologyModelFactory ontologyModelFactory;
	@Autowired
	private PackagingSerializationFacade packagingSerializationFacade;
	@Autowired
	private OntologyCache ontologyCache;
	
	@Bean
	public PackagingOperations packagingOperations() {
		PackagingOperations packagingOperations = new PackagingOperationsImpl();
		return packagingOperations;
	}
	
	@Bean
	public PackagingService packagingService() {
		PackagingService packagingService = new PackagingServiceImpl();
		packagingService.setPackagingConfigurationFacade(packagingConfigurationFacade);
		packagingService.setPackagingOntologyFacade(packagingOntologyFacade);
		packagingService.setPackagingOperations(packagingOperations());
		packagingService.setPackagingSessionFacade(packagingSessionFacade);
		packagingService.setPackagingStorageFacade(packagingStorageFacade);
		packagingService.setPackagingDatabaseFacade(packagingDatabaseFacade);
		packagingService.setOntologyModelFactory(ontologyModelFactory);
		packagingService.setPackagingSerializationFacade(packagingSerializationFacade);
		packagingService.setOntologyCache(ontologyCache);
		packagingService.setPackagingMetaDataFacade(packagingMetaDataFacade);
		
		return packagingService;
	}
}
