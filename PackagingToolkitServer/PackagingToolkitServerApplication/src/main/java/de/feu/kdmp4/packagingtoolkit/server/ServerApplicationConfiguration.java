package de.feu.kdmp4.packagingtoolkit.server;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModuleConfigurationConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModuleDatabaseConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModuleOntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModulePackagingConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModuleSerializationConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModuleSessionConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModuleStorageConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration.ModuleTripleStoreConfiguration;

/**
 * References to the configuration files of the application.
 * @author Christopher Olbertz
 *
 */
@Configuration
@Import({ModuleConfigurationConfiguration.class,
				  ModuleDatabaseConfiguration.class,
				  ModuleOntologyConfiguration.class,
				  ModulePackagingConfiguration.class,
				  ModuleSerializationConfiguration.class,
				  ModuleSessionConfiguration.class,
				  ModuleStorageConfiguration.class,
				  ModuleTripleStoreConfiguration.class})
public class ServerApplicationConfiguration {
}
