package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.MetaDataConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.metadata.classes.MetaDataOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.server.metadata.classes.MetaDataServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.metadata.facades.PackagingMetaDataFacade;
import de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces.MetaDataOperations;
import de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces.MetaDataService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;

/**
 * Configures the MetaData module.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class ModuleMetaDataConfiguration {
	/**
	 * Contains the methods that the module MetaData can use for accessing the module
	 * Configuration.
	 */
	@Autowired
	private MetaDataConfigurationFacade metaDataConfigurationFacade;
	/**
	 * A reference to the cache that caches the values from the ontologies.
	 */
	@Autowired
	private OntologyCache ontologyCache;
	
	/**
	 * Creates a bean for the business logic of the module.
	 * @return The implementing object of the interface.
	 */
	@Bean
	public MetaDataService metaDataService() {
		MetaDataService metaDataService = new MetaDataServiceImpl();
		metaDataService.setOntologyCache(ontologyCache);
		metaDataService.setMetaDataOperations(metaDataOperations());
		metaDataService.setMetaDataConfigurationFacade(metaDataConfigurationFacade);
		
		return metaDataService;
	}
	
	/**
	 * Creates a bean for the operations of the module with an implementation with 
	 * properties files.
	 * @return The implementing object of the interface.
	 */
	@Bean
	public MetaDataOperations metaDataOperations() {
		MetaDataOperations metaDataOperatations = new MetaDataOperationsImpl();
		metaDataOperatations.setOntologyCache(ontologyCache);
		
		return metaDataOperatations;
	}
	
	/**
	 * Creates an object for the facade that the module Packaging can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public PackagingMetaDataFacade packagingMetaDataFacade() {
		PackagingMetaDataFacade packagingMetaDataFacade = new PackagingMetaDataFacade();
		packagingMetaDataFacade.setMetaDataService(metaDataService());
		
		return packagingMetaDataFacade;
	}
}
