package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;

/**
 * Contains the configuration for the module Utils.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class ModuleModelConfiguration {
	/**
	 * Creates an object of the class that creates the objects for sending data via http.
	 * @return An object of the class that creates the objects for sending data via http.
	 */
	@Bean
	public ServerResponseFactory serverResponseFactory() {
		ServerResponseFactory serverResponseFactory = new ServerResponseFactory();
		
		return serverResponseFactory;
	}
}
