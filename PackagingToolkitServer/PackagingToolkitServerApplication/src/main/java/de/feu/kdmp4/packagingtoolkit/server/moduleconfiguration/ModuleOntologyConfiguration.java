package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.OntologyConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.DataStructureFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyOperationsJenaImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.TaxonomyOperationsJenaImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.TaxonomyServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.facades.PackagingOntologyFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ObjectPropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.RestrictionFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDatePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDateTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDurationPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyFloatPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyLongPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.MaxInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.MinInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.RestrictionCollection;
import de.feu.kdmp4.server.triplestore.facades.OntologyTripleStoreFacade;

@Configuration
public class ModuleOntologyConfiguration {
	@Autowired
	private OntologyConfigurationFacade ontologyConfigurationFacade;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired 
	private OntologyTripleStoreFacade ontologyTripleStoreFacade;
	@Autowired
	private ServerResponseFactory serverResponseFactory;
	
	@Bean
	public OntologyService ontologyService() {
		OntologyService ontologyService = new OntologyServiceImpl();
		ontologyService.setOntologyCache(getOntologyCache());
		ontologyService.setOntologyModelFactory(ontologyModelFactory());
		ontologyService.setOntologyTripleStoreFacade(ontologyTripleStoreFacade);
		ontologyService.setServerResponseFactory(serverResponseFactory);
		ontologyService.setOntologyConfigurationFacade(ontologyConfigurationFacade);
		return ontologyService;
	}
	
	@Bean(name = "datatypePropertyList")
	@Scope("prototype")
	public DatatypePropertyCollection getDatatypePropertyList() {
		return new DatatypePropertyCollectionImpl();
	}
	
	@Bean(name = "objectPropertyList")
	@Scope("prototype")
	public ObjectPropertyCollection getObjectPropertyList() {
		return new ObjectPropertyCollectionImpl();
	}
	
	@Bean(name = "OntologyBooleanPropertyImpl")
	@Scope("prototype")
	public OntologyBooleanProperty getOntologyBooleanProperty() {
		return new OntologyBooleanPropertyImpl();
	}
	
	@Bean(name = "OntologyBytePropertyImpl")
	@Scope("prototype")
	public OntologyByteProperty getOntologyByteProperty() {
		return new OntologyBytePropertyImpl();
	}
	
	@Bean(name = "ontologyCache")
	public OntologyCache getOntologyCache() {
		OntologyCache ontologyCache = new OntologyCache();
		ontologyCache.setDataStructureFactory(dataStructureFactory());
		ontologyCache.setOntologyConfigurationFacade(ontologyConfigurationFacade);
		ontologyCache.setOntologyOperations(ontologyOperations());
		ontologyCache.setOntologyTripleStoreFacade(ontologyTripleStoreFacade);
		
		return ontologyCache;
	}
	
	@Bean(name = "OntologyClassImpl")
	@Scope("prototype")
	public OntologyClass getOntologyClass(DatatypePropertyCollection datatypePropertyList, ObjectPropertyCollection objectPropertyList) {
		return new OntologyClassImpl(datatypePropertyList, objectPropertyList);
	}
	
	@Bean(name = "OntologyDatePropertyImpl")
	@Scope("prototype")
	public OntologyDateProperty getOntologyDateProperty() {
		return new OntologyDatePropertyImpl();
	}
	
	@Bean(name = "OntologyDateTimePropertyImpl")
	@Scope("prototype")
	public OntologyDateTimeProperty getOntologyDateTimeProperty() {
		return new OntologyDateTimePropertyImpl();
	}
	
	@Bean(name = "OntologyDoublePropertyImpl")
	@Scope("prototype")
	public OntologyDoubleProperty getOntologyDoubleProperty() {
		return new OntologyDoublePropertyImpl();
	}
	
	@Bean(name = "OntologyDurationPropertyImpl")
	@Scope("prototype")
	public OntologyDurationProperty getOntologyDurationProperty() {
		return new OntologyDurationPropertyImpl();
	}
	
	@Bean(name = "OntologyFloatPropertyImpl")
	@Scope("prototype")
	public OntologyFloatProperty getOntologyFloatProperty() {
		return new OntologyFloatPropertyImpl();
	}
	
	@Bean(name = "OntologyIndividualImpl")
	@Scope("prototype")
	public OntologyIndividual getOntologyIndividual() {
		return new OntologyIndividualImpl();
	}
	
	@Bean(name = "OntologyIntegerPropertyImpl")
	@Scope("prototype")
	public OntologyIntegerProperty getOntologyIntegerProperty() {
		return new OntologyIntegerPropertyImpl();
	}
	
	@Bean(name = "OntologyLongPropertyImpl")
	@Scope("prototype")
	public OntologyLongProperty getOntologyLongProperty() {
		return new OntologyLongPropertyImpl();
	}
	
	@Bean(name = "MaxInclusiveImpl")
	@Scope("prototype")
	public MaxInclusive getMaxInclusive() {
		return new MaxInclusiveImpl();
	}
	
	@Bean(name = "MinInclusiveImpl")
	@Scope("prototype")
	public MinInclusive getMinInclusive() {
		return new MinInclusiveImpl();
	}
	
	@Bean(name = "restrictionList")
	@Scope("prototype")
	public RestrictionCollection getRestrictionList() {
		return new RestrictionCollectionImpl();
	}
	
	@Bean(name = "OntologyShortPropertyImpl")
	@Scope("prototype")
	public OntologyShortProperty getOntologyShortProperty() {
		return new OntologyShortPropertyImpl();
	}
	
	@Bean(name = "OntologyStringPropertyImpl")
	@Scope("prototype")
	public OntologyStringProperty getOntologyStringProperty() {
		return new OntologyStringPropertyImpl();
	}
	
	@Bean(name = "OntologyTimePropertyImpl")
	@Scope("prototype")
	public OntologyTimeProperty getOntologyTimeProperty() {
		return new OntologyTimePropertyImpl();
	}
	
	@Bean
	public PackagingOntologyFacade packagingOntologyFacade() {
		PackagingOntologyFacade packagingOntologyFacade = new PackagingOntologyFacade();
		packagingOntologyFacade.setOntologyOperations(ontologyOperations());
		packagingOntologyFacade.setOntologyCache(getOntologyCache());
		packagingOntologyFacade.setOntologyService(ontologyService());
		
		return packagingOntologyFacade;
	}
	
	@Bean
	public OntologyOperations ontologyOperations() {
		OntologyOperations ontologyOperations = new OntologyOperationsJenaImpl();
		ontologyOperations.setOntologyModelFactory(ontologyModelFactory());
		ontologyOperations.setPropertyFactory(propertyFactory());
		ontologyOperations.setObjectPropertyFactory(objectPropertyFactory());
		ontologyOperations.setTaxonomyOperations(taxonomyOperations());
		
		return ontologyOperations;
	}
	
	@Bean
	public OntologyModelFactory ontologyModelFactory() {
		OntologyModelFactory ontologyModelFactory = new OntologyModelFactory();
		ontologyModelFactory.setApplicationContext(applicationContext);
		ontologyModelFactory.setPropertyFactory(propertyFactory());
		
		return ontologyModelFactory;
	}
	
	@Bean
	public PropertyFactory propertyFactory() {
		PropertyFactory propertyFactory = new PropertyFactory();
		propertyFactory.setApplicationContext(applicationContext);
		
		return propertyFactory;
	}
	
	@Bean
	public ObjectPropertyFactory objectPropertyFactory() {
		ObjectPropertyFactory objectPropertyFactory = new ObjectPropertyFactory();
		
		return objectPropertyFactory;
	}
	
	@Bean
	public DataStructureFactory dataStructureFactory() { 
		DataStructureFactory dataStructureFactory = new DataStructureFactory();
		
		return dataStructureFactory;
	}
	
	@Bean
	public RestrictionFactory restrictionFactory() {
		RestrictionFactory restrictionFactory = new RestrictionFactory();
		restrictionFactory.setApplicationContext(applicationContext);
		
		return restrictionFactory;
	}
	
	@Bean
	public TaxonomyOperations taxonomyOperations() {
		final TaxonomyOperations taxonomyOperations = new TaxonomyOperationsJenaImpl();
		return taxonomyOperations;
	}
	
	@Bean
	public TaxonomyService taxonomyService() {
		final TaxonomyService taxonomyService = new TaxonomyServiceImpl();
		taxonomyService.setTaxonomyOperations(taxonomyOperations());
		
		return taxonomyService;
	}
}