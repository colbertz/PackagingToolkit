package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.database.classes.DatabaseServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.database.classes.StorageDatabaseServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.PackagingDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.SessionDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.StorageDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.ViewDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.DatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.StorageDatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.DigitalObjectRepository;

/**
 * Configures the Database module.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class ModuleDatabaseConfiguration {
	/**
	 * The repository for the digital objects in the database.
	 */
	@Autowired
	private DigitalObjectRepository digitalObjectRepository;

	/**
	 * Creates a bean for the business logic for the storage of the module.
	 * @return The implementing object of the interface.
	 */
	@Bean
	public StorageDatabaseService storageDatabaseService() {
		StorageDatabaseService storageDatabaseService = new StorageDatabaseServiceImpl();
		storageDatabaseService.setDigitalObjectRepository(digitalObjectRepository);
		
		return storageDatabaseService;
	}
	
	/**
	 * Creates an object for the facade that the module Storage can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public StorageDatabaseFacade storageDatabaseFacade() {
		StorageDatabaseFacade storageDatabaseFacade = new StorageDatabaseFacade();
		storageDatabaseFacade.setStorageDatabaseService(storageDatabaseService());
		storageDatabaseFacade.setDatabaseService(databaseService());
		  
		return storageDatabaseFacade;
	}
	
	/**
	 * Creates an object for the facade that the module Packaging can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public PackagingDatabaseFacade packagingDatabaseFacade() {
		PackagingDatabaseFacade packagingDatabaseFacade = new PackagingDatabaseFacade();
		packagingDatabaseFacade.setStorageDatabaseService(storageDatabaseService());
		
		return packagingDatabaseFacade;
	}
	
	/**
	 * Creates an object for the facade that the module View can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public ViewDatabaseFacade viewDatabaseFacade() {
		ViewDatabaseFacade viewDatabaseFacade = new ViewDatabaseFacade();
		viewDatabaseFacade.setDatabaseService(databaseService());
		
		return viewDatabaseFacade;
	}
	
	/**
	 * Creates an object for the facade that the module Session can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public SessionDatabaseFacade sessionDatabaseFacade() {
		SessionDatabaseFacade sessionDatabaseFacade = new SessionDatabaseFacade();
		sessionDatabaseFacade.setDatabaseService(databaseService());
		sessionDatabaseFacade.setStorageDatabaseService(storageDatabaseService());
		
		return sessionDatabaseFacade;
	}
	
	/**
	 * Creates a bean for the business logic of the module.
	 * @return The implementing object of the interface.
	 */
	@Bean
	public DatabaseService databaseService() {
		DatabaseService databaseService = new DatabaseServiceImpl();
		return databaseService;
	}
}
