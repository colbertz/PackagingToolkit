package de.feu.kdmp4.packagingtoolkit.server.storage.interfaces;

import java.io.File;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.StorageConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.StorageDatabaseFacade;
import de.feu.kdmp4.server.triplestore.facades.StorageTripleStoreFacade;

/**
 * Contains the business logic for the storage module.
 * @author Christopher Olbertz
 *
 */
public interface StorageService {
	/**
	 * Sets a reference to the object that represents the operations layer.
	 * @param storageOperations The object that represents the operations layer.
	 */
	void setStorageOperations(final StorageOperations storageOperations);
	/**
	 * Sets a reference to the object that represents the facade to the configuration module.
	 * @param storageConfigurationFacade The object that represents the facade to the configuration module.
	 */
	void setStorageConfigurationFacade(final StorageConfigurationFacade storageConfigurationFacade);
	/**
	 * Sets a reference to the object that represents the facade to the database module.
	 * @param storageDatabaseFacade The object that represents the facade to the database module.
	 */
	void setStorageDatabaseFacade(final StorageDatabaseFacade storageDatabaseFacade);
	/**
	 * Checks if a file exists in the storage. For the comparison the md5 checksum of the file is used.
	 * @param md5Checksum The md5 checksum for the comparison.
	 * @return True if the file exists, false otherwise.
	 */
	boolean fileExistsInStorage(final String md5Checksum);
	/**
	 * Deletes an information package. The file of the digital object the information package is pointing to are
	 * only deleted if there are no other information packages referencing to these files. 
	 * @param uuidOfInformationPackageResponse The uuid of the information package that should be
	 * deleted
	 */
	void deleteInformationPackage(final String uuidOfInformationPackage);
	/**
	 * Determines the path of a file in the storage directory. For the search the md5 checksum of the file is used.
	 * @param md5Checksum The md5 checksum for the search.
	 * @return The path of the file in the storage directory.
	 */
	String findFilePathByChecksum(final String md5Checksum);
	/**
	 * Saves the data of a file in the storage. The data has been received via http. 
	 * @param dataOfFile The data that are contained in the file.
	 * @param fileName The name of the file. The data are saved under this name in the storage.
	 * @return The uuid the file has get during the saving process.
	 */
	Uuid saveFileDataInStorage(final byte[] dataOfFile, final String fileName);
	/**
	 * Checks if a file exists in the storage. For the comparison the data of the file are used.
	 * @param dataOfFile The content of the file. 
	 * @return True if the file exists, false otherwise.
	 */
	boolean fileExistsInStorage(final byte[] dataOfFile);
	/**
	 * Determines a file in the storage directory. For the search the md5 checksum of the file is used.
	 * @param md5Checksum The md5 checksum for the search.
	 * @return The file in the storage directory.
	 */
	File findFileByChecksum(final String md5Checksum);
	/**
	 * Determines the uuid of the digital object of a file. The file is identified by its checksum.
	 * @param md5Checksum The checksum that is used for identifying the file.
	 * @return The uuid of the digital object of the file or an empty optional. 
	 */
	Optional<Uuid> findUuidOfFileByChecksum(String md5Checksum);
	/**
	 * Sets a reference to the object that represents the facade to the TripleStore module.
	 * @param storageConfigurationFacade The object that represents the facade to the TripleStore  module.
	 */
	void setStorageTripleStoreFacade(StorageTripleStoreFacade storageTripleStoreFacade);
}