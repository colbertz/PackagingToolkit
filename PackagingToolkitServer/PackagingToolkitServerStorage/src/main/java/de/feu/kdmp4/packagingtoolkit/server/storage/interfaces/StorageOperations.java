package de.feu.kdmp4.packagingtoolkit.server.storage.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * The interface for the operations that have to do with the  storage on the server. 
 * The storage is the directory on the server that contains the files the information
 * packages are referencing to.
 * @author Christopher Olbertz
 *   
 */
public interface StorageOperations {
	/**
	 * Deletes a digital object from the storage directory.
	 * @param workingDirectory The working directory that contains the storage.
	 * @param reference Contains the reference to the digital object that should be deleted.
	 */
	void deleteDigitalObjectFromStorage(final WorkingDirectory workingDirectory, final Reference reference);
}