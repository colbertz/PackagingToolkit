/**
 * Contains the facade classes that are the interfaces for accessing the
 * storage module. Every module that wants to access the storage module has
 * its own facade class. The names of the facade classes are following this scheme:
 * AccessingModulStorageFacade.  
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.storage.facade;