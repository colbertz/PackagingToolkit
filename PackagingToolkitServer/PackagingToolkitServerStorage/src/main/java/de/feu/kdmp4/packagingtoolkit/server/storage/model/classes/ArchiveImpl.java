package de.feu.kdmp4.packagingtoolkit.server.storage.model.classes;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.storage.model.interfaces.Archive;

public class ArchiveImpl implements Archive {
	/**
	 * The type of the package. 
	 */
	private PackageType packageType;
	/**
	 * The serialization format of the package.
	 */
	private SerializationFormat serializationFormat;
	/**
	 * The title of the package. 
	 */
	private String title;
	/**
	 * The uuid for identifying the archive on the server. 
	 */
	private Uuid uuid;
	/**
	 * True, if the archive contains an virtual information package, false otherwise. 
	 */
	private boolean virtual;
	/**
	 * The date and time the archive was saved on the server.
	 */
	private LocalDateTime creationDate;
	/**
	 * The date and time the archive was modified the last time on the server.
	 */
	private LocalDateTime lastModificationDate;
	
	/**
	 * Constructs a new archive object.
	 * @param packageType The package type of this archive. At the moment only SIP is supported.
	 * @param title The title of this archive.
	 * @param lastModificationDate The date and time the archive was modified the last time on the server.
	 * @param creationDate The date and time the archive was saved on the server.
	 * @param uuid The uuid of this archive.
	 * @param serializationFormat The format this archive is serialized in. At the moment only OAI-ORE is 
	 * supported.
	 */
	public ArchiveImpl(final PackageType packageType, final String title, 
			final LocalDateTime lastModificationDate, final LocalDateTime creationDate, final Uuid uuid, 
			final SerializationFormat serializationFormat) {
		this.packageType = packageType;
		this.title = title;
		this.uuid = uuid;
		this.creationDate = creationDate;
		this.lastModificationDate = lastModificationDate;
		this.serializationFormat = serializationFormat;
	}
	
	@Override
	public boolean areUuidsEqual(final Uuid uuid) {
		final int compareTo = this.uuid.compareTo(uuid); 
		if (compareTo == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int compareTo(final Archive archive) {
		final String myUuid = this.uuid.toString();
		String theOtherUud = archive.getUuid().toString();
		return myUuid.compareTo(theOtherUud);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArchiveImpl other = (ArchiveImpl) obj;
		if (packageType != other.packageType)
			return false;
		if (serializationFormat != other.serializationFormat)
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		if (virtual != other.virtual)
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((packageType == null) ? 0 : packageType.hashCode());
		result = prime * result + ((serializationFormat == null) ? 0 : serializationFormat.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		result = prime * result + (virtual ? 1231 : 1237);
		return result;
	}
	
	@Override
	public PackageType getPackageType() {
		return packageType;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public Uuid getUuid() {
		return uuid;
	}

	@Override
	public SerializationFormat getSerializationFormat() {
		return serializationFormat;
	}
	
	@Override
	public void setUuid(Uuid uuid) {
		this.uuid = uuid;
	}

	@Override
	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	@Override
	public LocalDateTime getLastModificationDate() {
		return lastModificationDate;
	}
}
