/**
 * Contains the classes with the logic and the communication interfaces for this module.
 * @author Christopher Olbertz 
 */

package de.feu.kdmp4.packagingtoolkit.server.storage.classes;