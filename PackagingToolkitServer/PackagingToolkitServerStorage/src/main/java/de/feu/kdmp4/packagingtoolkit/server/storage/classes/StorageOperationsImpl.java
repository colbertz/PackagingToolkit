package de.feu.kdmp4.packagingtoolkit.server.storage.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageOperations;

/**
 * The implementation of the interface for the operations on the storage of the server. 
 * Dependencies are injected by Spring. 
 * @author Christopher Olbertz
 *
 */
public class StorageOperationsImpl implements StorageOperations {
	@Override
	public void deleteDigitalObjectFromStorage(final WorkingDirectory workingDirectory, final Reference reference) {
		final String filename = reference.getUrl();
		final File storageDirectory = workingDirectory.getStorageDirectory();
		final File fileInStorage = new File(storageDirectory, filename);
		fileInStorage.delete();
	}
}