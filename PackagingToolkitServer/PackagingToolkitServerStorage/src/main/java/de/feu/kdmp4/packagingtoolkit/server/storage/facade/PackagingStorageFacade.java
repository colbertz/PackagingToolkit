package de.feu.kdmp4.packagingtoolkit.server.storage.facade;

import java.io.File;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageService;

/**
 * Contains the methods that are used for accessing the methods of the storage module by the
 * packaging module.
 * @author Christopher Olbertz
 *
 */
public class PackagingStorageFacade {
	/**
	 * A reference to an object of the class that contains the business logic of the storage module.
	 */
	private StorageService storageService;
	
	/**
	 * Checks if a file exists in the storage. For the comparison the data of the file are used.
	 * @param dataOfFile The content of the file. 
	 * @return True if the file exists, false otherwise.
	 */
	public boolean fileExistsInStorage(final byte[] dataOfFile) {
		return storageService.fileExistsInStorage(dataOfFile);
	}
	
	/**
	 * Determines the path of a file in the storage directory. For the search the md5 checksum of the file is used.
	 * @param md5Checksum The md5 checksum for the search.
	 * @return The path of the file in the storage directory.
	 */
	public String findFilePathByChecksum(final String md5Checksum) {
		return storageService.findFilePathByChecksum(md5Checksum);
	}
	
	/**
	 * Determines a file in the storage directory. For the search the md5 checksum of the file is used.
	 * @param md5Checksum The md5 checksum for the search.
	 * @return The file in the storage directory.
	 */
	public File findFileByChecksum(final String md5Checksum)  {
		return storageService.findFileByChecksum(md5Checksum);
	}
	
	/**
	 * Checks if a file exists in the storage. For the comparison the md5 checksum of the file is used.
	 * @param md5Checksum The md5 checksum for the comparison.
	 * @return True if the file exists, false otherwise.
	 */
	public boolean fileExistsInStorage(final String md5Checksum) {
		return storageService.fileExistsInStorage(md5Checksum);
	}
	
	/**
	 * Deletes an information package. The file of the digital object the information package is pointing to are
	 * only deleted if there are no other information packages referencing to these files. 
	 * @param uuidOfInformationPackageResponse The uuid of the information package that should be
	 * deleted
	 */
	public void deleteInformationPackage(final String uuidOfInformationPackage) {
		storageService.deleteInformationPackage(uuidOfInformationPackage);
	}

	/**
	 * Determines the uuid of the digital object of a file. The file is identified by its checksum.
	 * @param md5Checksum The checksum that is used for identifying the file.
	 * @return The uuid of the digital object of the file or an empty optional. 
	 */
	public Optional<Uuid> findUuidOfFileByChecksum(final String md5Checksum) {
		return storageService.findUuidOfFileByChecksum(md5Checksum);
	}
	
	/**
	 * Saves the data of a file in the storage. The data has been received via http. 
	 * @param dataOfFile The data that are contained in the file.
	 * @param fileName The name of the file. The data are saved under this name in the storage.
	 * @return The uuid the file has get during the saving process.
	 */
	public Uuid saveFileDataInStorage(final byte[] dataOfFile, final String fileName) {
		return storageService.saveFileDataInStorage(dataOfFile, fileName);
	}
	
	/**
	 * Sets a reference to the object that contains the business logic of the storage module.
	 * @param storageService A reference to the object that contains the business logic of the storage module.
	 */
	public void setStorageService(final StorageService storageService) {
		this.storageService = storageService;
	}
}
