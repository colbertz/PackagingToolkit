/**
 * Contains the implementations of the interfaces for the business logic of the module
 * Storage and the communication interface.
 * @author Christopher Olbertz 
 */

package de.feu.kdmp4.packagingtoolkit.server.storage.model.classes;