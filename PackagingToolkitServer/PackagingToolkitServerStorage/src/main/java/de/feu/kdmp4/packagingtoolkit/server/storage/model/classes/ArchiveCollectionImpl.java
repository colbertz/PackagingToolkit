package de.feu.kdmp4.packagingtoolkit.server.storage.model.classes;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.storage.factories.StorageOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.storage.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.server.storage.model.interfaces.ArchiveCollection;

public class ArchiveCollectionImpl extends AbstractList implements ArchiveCollection { 
	@Override
	public void addArchiveToList(final Archive archive) {
		checkUuidInArchiveList(archive.getUuid());
			super.add(archive);
	}
	
	@Override
	public Optional<Archive> getArchiveAt(final int index) {
		Archive archive;
		archive = (Archive)super.getElement(index);
		Optional<Archive> optionalWithArchive = StorageOptionalFactory.createOptionalWithArchive(archive);
		return optionalWithArchive;
	}
	
	@Override
	public int getArchiveCount() {
		return super.getSize();
	}
	
	@Override
	public void removeArchive(final Archive archive) {
			super.remove(archive);
	}
	
	/**
	 * Checks, if an information package with a given uuid is
	 * already in the list. Throws an UuidAlreadyInArchiveListException
	 * when the uuid is found.
	 * @param uuid The uuid to look for.
	 */
	private void checkUuidInArchiveList(final Uuid uuid) {
		for (int i = 0; i < getArchiveCount(); i++) {
			ArchiveImpl archive;
			//try {
				archive = (ArchiveImpl)super.getElement(i);
				if (archive.areUuidsEqual(uuid)) {
					// TODO Programming Exception werfen.
					/*throw new UuidAlreadyInArchiveListException(uuid, 
								this.getClass().getName(), 
								METHOD_CHECK_UUID_IN_ARCHIVE_LIST, 
								PackagingToolkitComponent.SERVER);*/
				}
			/*} catch (IndexNotInRangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}
}
