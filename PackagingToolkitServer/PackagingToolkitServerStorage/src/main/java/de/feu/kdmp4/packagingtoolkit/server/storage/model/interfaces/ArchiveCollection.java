package de.feu.kdmp4.packagingtoolkit.server.storage.model.interfaces;

import java.util.Optional;

/**
 * Encapsulates a collection with archives. An archive is defined as an information package 
 * that is stored on the server and can be downloaded by the user.
 * @author Christopher Olbertz
 *
 */
public interface ArchiveCollection {
	/**
	 * Removes an archive from the collection. Because the collection represents only 
	 * archives that are in the memory of the server, the archive is not deleted from
	 * the storage directory of the server.
	 * @param archive The archive we want to remove.
	 */
	void removeArchive(final Archive archive);
	/**
	 * Counts the archives in the collection.
	 * @return The number of archives in the collection.
	 */
	int getArchiveCount();
	/**
	 * Determines an archive at a given position in the collection starting with 0.
	 * @param index Describes the position of the archive.
	 * @return The found archive or an empty optional.
	 */
	Optional<Archive> getArchiveAt(final int index);
	/**
	 * Adds an archive to the collection. 
	 * @param archive The archive we want to add to the collection.
	 */
	void addArchiveToList(final Archive archive);
}
