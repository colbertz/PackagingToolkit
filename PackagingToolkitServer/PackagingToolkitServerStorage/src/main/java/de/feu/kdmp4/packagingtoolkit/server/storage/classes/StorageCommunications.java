package de.feu.kdmp4.packagingtoolkit.server.storage.classes;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageService;

/**
 * The RESTful interface for the communication between the client and the 
 * storage module. It contains the methods for working with archives stored
 * in the storage directory.
 * @author Christopher Olbertz
 *
 */
@RestController
@RequestMapping(PathConstants.SERVER_STORAGE_INTERFACE)
public class StorageCommunications {
	/**
	 * Contains the business logic of the module Storage.
	 */
	@Autowired
	private StorageService storageService;
	
	/**
	 * Is called if the client wants to upload a file in the storage via http. 
	 * @param file Contains the content of the file that should be uploaded and some meta data
	 * like the filename.
	 * @return An response that contains either the http status ok or an exception.
	 */
	@PostMapping(value = PathConstants.SERVER_UPLOAD_FILE)
	public ResponseEntity<String> uploadFileInStorage(@RequestParam("file") MultipartFile file) {
		try {
			final byte[] dataOfFile = file.getBytes();
			final String fileName = file.getOriginalFilename();
			storageService.saveFileDataInStorage(dataOfFile, fileName);
			return ResponseEntity.status(HttpStatus.OK).build(); 
		} catch (IOException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	/**
	 * Is called if the user wants to delete an information package from the server.
	 * @param uuidOfInformationPackage The uuid of the information package that should be deleted.
	 */
	@RequestMapping(value = PathConstants.SERVER_DELETE_ARCHIVE, method = RequestMethod.DELETE)
	public void deleteInformationPackage(@PathVariable(ParamConstants.PARAM_UUID) String uuidOfInformationPackage) {
		storageService.deleteInformationPackage(uuidOfInformationPackage);
	}
}
