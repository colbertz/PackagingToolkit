/**
 * Contains factories that create objects necessary for the Storage module without
 * Spring or other external frameworks.
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.server.storage.factories;