/**
 * Contains the interfaces for the classes that contain the logic of the Storage module and
 * the communication interfaces. 
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.server.storage.interfaces;