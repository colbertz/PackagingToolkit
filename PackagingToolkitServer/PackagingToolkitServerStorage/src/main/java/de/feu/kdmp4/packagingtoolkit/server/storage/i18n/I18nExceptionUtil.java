package de.feu.kdmp4.packagingtoolkit.server.storage.i18n;

import java.util.ResourceBundle;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nUtil;

/**
 * Encapsulates the keys for internationalization. Java code is not allowed
 * to call the message bundles but it has to use this class and its static
 * methods. Therefore the other classes do not have to know the names of 
 * the keys. This class contains the keys only for exceptions.
 * @author Christopher Olbertz
 *
 */
public class I18nExceptionUtil {
	private static final String ARCHIVE_DOES_NOT_EXIST_IN_STORAGE = 
			"archive-does-not-exist-in-storage";
	private static final String DATE_MAY_NOT_BE_NULL = "date-may-not-be-null";
	private static final String INFORMATION_PACKAGE_CLASS_NOT_FOUND = "information-package-class-not-found";
	private static final String INFORMATION_PACKAGE_NOT_AVAILABLE = "information-package-not-available";
	private static final String ZIP_FILE_COULD_NOT_BE_CREATED = "zip-file-could-no-created";
	
	private static ResourceBundle messagesResourceBundle;
	
	static {
		messagesResourceBundle = I18nUtil.getExceptionsResourceBundle();
	}
	
	public static String getArchiveDoesNotExistInStorageString() {
		return messagesResourceBundle.getString(ARCHIVE_DOES_NOT_EXIST_IN_STORAGE);
	}
	
	public static String getDateMayNotBeNullString() {
		return messagesResourceBundle.getString(DATE_MAY_NOT_BE_NULL);
	}
	
	public static String getInformationPackageClassNotFoundString() {
		return messagesResourceBundle.getString(INFORMATION_PACKAGE_CLASS_NOT_FOUND);
	}
	
	public static String getInformationPackageNotAvailableString() {
		return messagesResourceBundle.getString(INFORMATION_PACKAGE_NOT_AVAILABLE);
	}
	
	public static String getZipFileCouldNotBeCreatedString() {
		return messagesResourceBundle.getString(ZIP_FILE_COULD_NOT_BE_CREATED);
	}
}