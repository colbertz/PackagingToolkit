package de.feu.kdmp4.packagingtoolkit.server.storage.factories;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.storage.model.interfaces.Archive;

/**
 * Creates the optional objects for the Storage module.
 * @author Christopher Olbertz
 *
 */
public class StorageOptionalFactory {
	/**
	 * Creates an optional with an archive.
	 * @param archive The archive that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<Archive> createOptionalWithArchive(final Archive archive) {
		if (archive != null) {
			return Optional.of(archive);
		} else {
			return createEmptyOptionalWithArchive();
		}
	}
	
	/**
	 * Creates an empty optional for a triple statements.
	 * @return The created empty Optional.
	 */
	public final static Optional<Archive> createEmptyOptionalWithArchive() {
		return Optional.empty();
	}
}
