/**
 * Contains the interfaces for the business logic and the communication interfaces for 
 * the module Storage.
 * @author Christopher Olbertz 
 */

package de.feu.kdmp4.packagingtoolkit.server.storage.model.interfaces;