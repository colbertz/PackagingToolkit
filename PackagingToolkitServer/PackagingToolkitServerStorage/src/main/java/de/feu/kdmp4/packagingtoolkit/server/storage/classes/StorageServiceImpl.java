package de.feu.kdmp4.packagingtoolkit.server.storage.classes;

import java.io.File;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.factories.OptionalFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.StorageConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.StorageDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageOperations;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageService;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.server.triplestore.facades.StorageTripleStoreFacade;

/**
 * An implementation of {@link de.feu.kdmp4.packagingtoolkit.server.
 * storage.interfaces.StorageService} with Spring annotations.
 * @author Christopher Olbertz
 *
 */
public class StorageServiceImpl implements StorageService {
	/**
	 * This object contains the methods of the operations layer of this module.
	 */
	private StorageOperations storageOperations;
	/**
	 * A reference to the object that contains the access methods to the configuration module.
	 */
	private StorageConfigurationFacade storageConfigurationFacade;
	/**
	 * A reference to the object that contains the access methods to the database module.
	 */
	private StorageDatabaseFacade storageDatabaseFacade;
	/**
	 * A reference to the object that contains the access methods to the TripleStore module.
	 */
	private StorageTripleStoreFacade storageTripleStoreFacade;
	
	@Override
	public boolean fileExistsInStorage(final byte[] dataOfFile) {
		final String md5Checksum = PackagingToolkitFileUtils.calculateMD5(dataOfFile);
		return fileExistsInStorage(md5Checksum);
	}
	
	@Override
	public boolean fileExistsInStorage(final String md5Checksum) {
		final Optional<DigitalObjectExchange> optionalWithDigitalObject = storageDatabaseFacade.findDigitalObjectByMd5Checksum(md5Checksum);
		if (optionalWithDigitalObject.isPresent()) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String findFilePathByChecksum(final String md5Checksum) {
		return storageDatabaseFacade.findFilePathByChecksum(md5Checksum);
	}
	
	@Override
	public File findFileByChecksum(final String md5Checksum) {
		final WorkingDirectory workingDirectory = storageConfigurationFacade.getWorkingDirectory();
		final String filename = findFilePathByChecksum(md5Checksum);
		final File file = workingDirectory.getFileInStorageDirectory(filename);
		return file;
	}
	
	@Override
	public  Optional<Uuid> findUuidOfFileByChecksum(final String md5Checksum) {
		Optional<DigitalObjectExchange> optionalWithdigitalObject= storageDatabaseFacade.findDigitalObjectByMd5Checksum(md5Checksum);
		if (optionalWithdigitalObject.isPresent()) {
			DigitalObjectExchange digitalObjectExchange = optionalWithdigitalObject.get();
			Uuid uuid = digitalObjectExchange.getUuidOfDigitalObject();
			return OptionalFactory.createOptionalWithUuid(uuid);
		} else {
			return OptionalFactory.createEmptyOptionalWithUuid();
		}
	}
	
	@Override
	public Uuid saveFileDataInStorage(final byte[] dataOfFile, final String fileName) {
		final WorkingDirectory workingDirectory = storageConfigurationFacade.getWorkingDirectory();
		final File storageDirectory = workingDirectory.getStorageDirectory();
		final File digitalObjectFile = new File(storageDirectory, fileName);
		PackagingToolkitFileUtils.writeByteArrayToFile(digitalObjectFile, dataOfFile);
		final String md5Checksum = PackagingToolkitFileUtils.calculateMD5(digitalObjectFile);
		return storageDatabaseFacade.saveDigitalObject(fileName, md5Checksum);
	}
	
	@Override
	public void deleteInformationPackage(final String uuidOfInformationPackageAsString) {
		deleteReferencesOfInformationPackage(uuidOfInformationPackageAsString);
		storageDatabaseFacade.deleteInformationPackage(uuidOfInformationPackageAsString);
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(uuidOfInformationPackageAsString);
		storageTripleStoreFacade.deleteAllTriplesOfInformationPackageIfExists(uuidOfInformationPackage);
	}
	
	/**
	 * Checks if the references of an information package can be deleted. If no other information package is pointing to a 
	 * digital object, the digital object can be deleted. 
	 * @param uuidOfInformationPackage
	 */
	private void deleteReferencesOfInformationPackage(final String uuidOfInformationPackage) {
		final ReferenceCollection referencesOfInformationPackage = storageDatabaseFacade.
				findReferencesOfInformationPackage(uuidOfInformationPackage);
		final WorkingDirectory workingDirectory = storageConfigurationFacade.getWorkingDirectory();
		
		for (int i = 0; i < referencesOfInformationPackage.getReferencesCount(); i++) {
			final Reference reference = referencesOfInformationPackage.getReference(i);
			final String uuidOfDigitalObject = reference.getUuidOfFile().toString();
			final long countIpsPointingToDigitalObject = storageDatabaseFacade.countReferencesOfDigitalObject(uuidOfDigitalObject);
			
			/* Only this information package is pointing to the file. Therefore we can delete the digital object. */
			if (countIpsPointingToDigitalObject == 1) {
				storageDatabaseFacade.deleteDigitalObject(uuidOfDigitalObject);
				storageOperations.deleteDigitalObjectFromStorage(workingDirectory, reference);
			}
		}
	}
	
	@Override
	public void setStorageOperations(final StorageOperations storageOperations) {
		this.storageOperations = storageOperations;
	}

	@Override
	public void setStorageConfigurationFacade(final StorageConfigurationFacade storageConfigurationFacade) {
		this.storageConfigurationFacade = storageConfigurationFacade;
	}
	
	@Override
	public void setStorageDatabaseFacade(final StorageDatabaseFacade storageDatabaseFacade) {
		this.storageDatabaseFacade = storageDatabaseFacade;
	}
	
	@Override
	public void setStorageTripleStoreFacade(final StorageTripleStoreFacade storageTripleStoreFacade) {
		this.storageTripleStoreFacade = storageTripleStoreFacade;
	}
}