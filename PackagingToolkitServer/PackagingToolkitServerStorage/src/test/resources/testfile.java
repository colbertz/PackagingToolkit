package de.feu.kdmp4.packagingtoolkit.server.storage.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.StorageConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.StorageDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceList;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageOperations;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageService;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * An implementation of {@link de.feu.kdmp4.packagingtoolkit.server.
 * storage.interfaces.StorageService} with Spring annotations.
 * @author Christopher Olbertz
 *
 */
public class StorageServiceImpl implements StorageService {
	/**
	 * This object contains the methods of the operations layer of this module.
	 */
	private StorageOperations storageOperations;
	/**
	 * A reference to the object that contains the access methods to the configuration module.
	 */
	private StorageConfigurationFacade storageConfigurationFacade;
	/**
	 * A reference to the object that contains the access methods to the database module.
	 */
	private StorageDatabaseFacade storageDatabaseFacade;

	@Override
	public boolean fileExistsInStorage(final byte[] dataOfFile) {
		final String md5Checksum = PackagingToolkitFileUtils.calculateMD5(dataOfFile);
		return fileExistsInStorage(md5Checksum);
	}
	
	@Override
	public boolean fileExistsInStorage(final String md5Checksum) {
		final DigitalObject digitalObject = storageDatabaseFacade.findDigitalObjectByMd5Checksum(md5Checksum);
		if (digitalObject == null) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public String findFilePathByChecksum(final String md5Checksum) {
		return storageDatabaseFacade.findFilePathByChecksum(md5Checksum);
	}
	
	@Override
	public Uuid saveFileDataInStorage(final byte[] dataOfFile, final String fileName) {
		final WorkingDirectory workingDirectory = storageConfigurationFacade.getWorkingDirectory();
		final File storageDirectory = workingDirectory.getStorageDirectory();
		final File digitalObjectFile = new File(storageDirectory, fileName);
		PackagingToolkitFileUtils.writeByteArrayToFile(digitalObjectFile, dataOfFile);
		final String md5Checksum = PackagingToolkitFileUtils.calculateMD5(digitalObjectFile);
		return storageDatabaseFacade.saveDigitalObject(fileName, md5Checksum);
	}
	
	@Override
	public void deleteInformationPackage(final String uuidOfInformationPackage) {
		deleteReferencesOfInformationPackage(uuidOfInformationPackage);
		deleteInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Checks if the references of an information package can be deleted. If no other information package is pointing to a 
	 * digital object, the digital object can be deleted. 
	 * @param uuidOfInformationPackage
	 */
	private void deleteReferencesOfInformationPackage(final String uuidOfInformationPackage) {
		final ReferenceList referencesOfInformationPackage = storageDatabaseFacade.findReferencesOfInformationPackage(uuidOfInformationPackage);
		final WorkingDirectory workingDirectory = storageConfigurationFacade.getWorkingDirectory();
		
		for (int i = 0; i < referencesOfInformationPackage.getReferencesCount(); i++) {
			final Reference reference = referencesOfInformationPackage.getReference(i);
			final String uuidOfDigitalObject = reference.getUuidOfFile().toString();
			final long countIpsPointingToDigitalObject = storageDatabaseFacade.countReferencesOfDigitalObject(uuidOfDigitalObject);
			
			/* Only this information package is pointing to the file. Therefore we can delete the reference. */
			if (countIpsPointingToDigitalObject == 1) {
				storageDatabaseFacade.deleteDigitalObject(uuidOfDigitalObject);
				storageOperations.deleteDigitalObjectFromStorage(workingDirectory, reference);
			}
		}
	}
	
	@Override
	public void setStorageOperations(final StorageOperations storageOperations) {
		this.storageOperations = storageOperations;
	}

	@Override
	public void setStorageConfigurationFacade(final StorageConfigurationFacade storageConfigurationFacade) {
		this.storageConfigurationFacade = storageConfigurationFacade;
	}
	
	@Override
	public void setStorageDatabaseFacade(final StorageDatabaseFacade storageDatabaseFacade) {
		this.storageDatabaseFacade = storageDatabaseFacade;
	}
}