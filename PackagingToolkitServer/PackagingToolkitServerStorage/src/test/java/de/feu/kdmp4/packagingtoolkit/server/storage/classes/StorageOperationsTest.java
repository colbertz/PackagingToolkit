package de.feu.kdmp4.packagingtoolkit.server.storage.classes;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.ReferenceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageOperations;
import de.feu.kdmp4.packagingtoolkit.server.storage.test.api.StorageOperationsTestApi;

/**
 * Contains the test of the operations layer of the storage module.
 * @author Christopher Olbertz
 *
 */
public class StorageOperationsTest {
	/**
	 * The object that contains the operations of the storage module.
	 */
	private StorageOperations storageOperations;
	/**
	 * Is used for creating the temporary working and storage directory.
	 */
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	
	/**
	 * Creates the opject for the operations layer.
	 */
	@Before
	public void setUp() {
		storageOperations = new StorageOperationsImpl();
	}
	
	/**
	 * Tests if an existing file can be deleted from the storage directory.
	 * @throws IOException
	 */
	@Test
	public void testDeleteDigitalObjectFromStorage_withExistingFile() throws IOException {
		final WorkingDirectory workingDirectory = StorageOperationsTestApi.createWorkingDirectory(temporaryFolder);
		final List<File> filesInStorage = StorageOperationsTestApi.createFilesInStorage(workingDirectory);
		final File fileToDelete =  filesInStorage.get(1);
		final Reference referenceToDelete = ReferenceImpl.createLocalFileReference(fileToDelete.getAbsolutePath(), new Uuid());
		
		storageOperations.deleteDigitalObjectFromStorage(workingDirectory, referenceToDelete);
		
		final boolean fileDeleted = checkResults_testDeleteDigitalObjectFromStorage_withExistingFile_fileDeleted(fileToDelete, workingDirectory);
		assertTrue(fileDeleted);
	}
	
	/**
	 * Checks the results of the test. 
	 * @param file The file that should be deleted.
	 * @param workingDirectory The working directory that contains the storage directory.
	 * @return True if the file  has been deleted, false otherwise.
	 */
	private boolean checkResults_testDeleteDigitalObjectFromStorage_withExistingFile_fileDeleted(final File file, final WorkingDirectory workingDirectory) {
		final File storageDirectory = workingDirectory.getStorageDirectory();
		final File[] filesInStorage = storageDirectory.listFiles();
		boolean fileDeleted = true;
		
		if (filesInStorage != null) {
			for (File fileInStorage: filesInStorage) {
				if (fileInStorage.equals(file)) {
					fileDeleted = false;
				}
			}
		}
		
		return fileDeleted;
	}
	
	/**
	 * Tests the deletion of a file in the storage directory if the file does not exist.
	 * @throws IOException
	 */
	@Test
	public void testDeleteDigitalObjectFromStorage_withNotExistingFile() throws IOException {
		final WorkingDirectory workingDirectory = StorageOperationsTestApi.createWorkingDirectory(temporaryFolder);
		final File fileToDelete =  new File(workingDirectory.getStorageDirectory(), "notExistingFile");
		final List<File> filesInStorage = StorageOperationsTestApi.createFilesInStorage(workingDirectory);
		final Reference referenceToDelete = ReferenceImpl.createLocalFileReference(fileToDelete.getAbsolutePath(), new Uuid());
		
		storageOperations.deleteDigitalObjectFromStorage(workingDirectory, referenceToDelete);
		
		final boolean noOtherFilesDeleted = checkResults_testDeleteDigitalObjectFromStorage_withNotExistingFile_filesNotDeleted(workingDirectory, filesInStorage);
		assertTrue(noOtherFilesDeleted);
	}
	
	/**
	 * Checks the results of the test. No file in the storage directory may have been deleted.
	 * @param workingDirectory The working directory that contains the storage directory.
	 * @return True if no file has been deleted, false otherwise.
	 */
	private boolean checkResults_testDeleteDigitalObjectFromStorage_withNotExistingFile_filesNotDeleted(final WorkingDirectory 
			workingDirectory, final List<File> expectedFilesInStorage) {
		final File storageDirectory = workingDirectory.getStorageDirectory();
		final File[] actualFilesInStorage = storageDirectory.listFiles();
		
		if (actualFilesInStorage != null) {
			final int actualSize = actualFilesInStorage.length;
			final int expectedSize = expectedFilesInStorage.size();
			if (expectedSize != actualSize) {
				return false;
			} else {
				return true;
			}
		}
		
		return false;
	}
}
