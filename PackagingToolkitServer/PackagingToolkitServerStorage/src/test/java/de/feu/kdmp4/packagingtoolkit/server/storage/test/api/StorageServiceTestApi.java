package de.feu.kdmp4.packagingtoolkit.server.storage.test.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * Creates some test data for the test of the service layer of the storage module.
 * @author Christopher Olbertz
 *
 */
public class StorageServiceTestApi {
	private static final String PATH_TEST_FILE = "testfile.java";
	
	/**
	 * Prepares a working directory for the tests. Creates a working directory in the temporary folder of the operating system. 
	 * Copies the files from the resources directory to this folder.
	 * @return The working directory for the test.
	 * @throws IOException
	 */
	public static final WorkingDirectory createWorkingDirectory(final TemporaryFolder temporaryFolder) throws IOException {
		final File workingDirectoryFile = temporaryFolder.newFolder("work");
		final WorkingDirectory workingDirectory = new WorkingDirectoryImpl(workingDirectoryFile.getAbsolutePath());
		copyTestfileToStorage(workingDirectory);
		
		return workingDirectory;
	}
	
	private static void copyTestfileToStorage(final WorkingDirectory workingDirectory) throws IOException {
		final File storageDirectory = workingDirectory.getStorageDirectory();
		final InputStream inputStream = StorageServiceTestApi.class.getResourceAsStream(File.separator + PATH_TEST_FILE);
		final File testfile = new File(storageDirectory, PATH_TEST_FILE);
				
		PackagingToolkitFileUtils.createFileFromInputStream(testfile, inputStream);
	}
	
	
}
