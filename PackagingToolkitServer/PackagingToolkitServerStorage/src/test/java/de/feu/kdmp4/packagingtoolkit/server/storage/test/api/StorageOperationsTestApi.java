package de.feu.kdmp4.packagingtoolkit.server.storage.test.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * Creates some test data for the test of the operations layer of the storage module.
 * @author Christopher Olbertz
 *
 */
public class StorageOperationsTestApi {
	/**
	 * Prepares a working directory for the tests. Creates a working directory in the temporary folder of the operating system.
	 * @return The working directory for the test.
	 * @throws IOException
	 */
	public static final WorkingDirectory createWorkingDirectory(final TemporaryFolder temporaryFolder) throws IOException {
		final File workingDirectoryFile = temporaryFolder.newFolder("work");
		final WorkingDirectory workingDirectory = new WorkingDirectoryImpl(workingDirectoryFile.getAbsolutePath());
		return workingDirectory;
	}
	
	/**
	 *  Creates three files in the storage directory.
	 * @param workingDirectory The working directory that contains the storage directory.
	 * @return A list that should contain the files in the storage.
	 * @throws IOException
	 */
	public static final List<File> createFilesInStorage(final WorkingDirectory workingDirectory) throws IOException {
		final List<File> archiveFilesInStorage = new ArrayList<>();
		final Uuid uuid1 = new Uuid();
		final Uuid uuid2 = new Uuid();
		final Uuid uuid3 = new Uuid();
		
		final File storageDir = workingDirectory.getStorageDirectory();
		final File archiveFile1 = new File(storageDir, uuid1.toString() + ".txt");
		final File archiveFile2 = new File(storageDir, uuid2.toString() + ".rdf");
		final File archiveFile3 = new File(storageDir, uuid3.toString() + ".owl");
		archiveFile1.createNewFile();
		archiveFile2.createNewFile();
		archiveFile3.createNewFile();
		
		archiveFilesInStorage.add(archiveFile1);
		archiveFilesInStorage.add(archiveFile2);
		archiveFilesInStorage.add(archiveFile3);
		
		return archiveFilesInStorage;
	}
}
