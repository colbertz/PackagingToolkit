/**
 * Contains classes that are used doing work related to the tests of the storage module.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.storage.test.api;