package de.feu.kdmp4.packagingtoolkit.server.storage.classes;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
//import static org.mockito.
import org.mockito.junit.MockitoRule;

import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.StorageConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageOperations;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageService;

public class StorageServiceTest {
	private StorageService storageService;
	@Rule
	public TemporaryFolder temporaryFolder;
	@Rule
	public MockitoRule mockitoRule;
	
	@Mock
	private StorageOperations storageOperations;
	@Mock
	private StorageConfigurationFacade storageConfigurationFacade;
	
	public StorageServiceTest() {
		temporaryFolder = new TemporaryFolder();
		mockitoRule = MockitoJUnit.rule();
	}
	
	@Before
	public void setUp() {
		storageService = new StorageServiceImpl();
		storageService.setStorageOperations(storageOperations);
		storageService.setStorageConfigurationFacade(storageConfigurationFacade);
	}
	
	@Test
	public void testFileExistsInStorage_withMd5Checksum() {
		final String expectedMd5Checksum = "636915470c70a836f7292e366af12542";
		
	}
}
