package de.feu.kdmp4.packagingtoolkit.server.configuration.classes.testapi;

/**
 * This enums contains the key of the properties in the property file for the tests.
 * @author Christopher Olbertz
 *
 */
public enum ConfigurationFileProperties {
	BASE_ONTOLOGY("baseOntology"), RULE_ONTOLOGY("ruleOntology"), SKOS_ONTOLOGY("skosOntology"), 
	PREMIS_ONTOLOGY("premisOntology"), WORKING_DIRECTORY("workingDirectory");
	
	/**
	 * A name for the property. This is used as key in the properties file.
	 */
	private String propertyName;
	
	private ConfigurationFileProperties(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
}
