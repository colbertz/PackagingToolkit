package de.feu.kdmp4.packagingtoolkit.server.configuration.classes.testapi;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;
import java.util.Properties;

import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.ServerConfigurationDataImpl;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Contains methods and constants important for all tests.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationTestApi {
	/**
	 * The test value for the property with the name of the rule ontology.
	 */
	private static final String BASE_ONTOLOGY = "baseOntology.owl";
	/**
	 * The test value for the property with the name of the rule ontology.
	 */
	private static final String RULE_ONTOLOGY = "ruleOntology.owl";
	/**
	 * The test value for the property with the name of the premis ontology.
	 */
	private static final String PREMIS_ONTOLOGY = "premisOntology.owl";
	/**
	 * The test value for the property with the name of the SKOS ontology.
	 */
	private static final String SKOS_ONTOLOGY = "skosOntology.owl";
	/**
	 * The value for the property with the working directory for testing
	 * purposes.
	 */
	private static final String WORKING_DIRECTORY = "work";
	/**
	 * A meta data element for testing the mapping.
	 */
	private static final String MAPPING_FILE_SIZE = "fileSize";
	/**
	 * A meta data element for testing the mapping.
	 */
	private static final String MAPPING_PAGE_COUNT = "pageCount";
	
	/**
	 * The test value for the property with the name of the base ontology.
	 */
	private static final String NEW_BASE_ONTOLOGY = "baseOntology2.owl";
	/**
	 * The test value for the property with the name of the rule ontology.
	 */
	private static final String NEW_RULE_ONTOLOGY = "ruleOntology2.owl";
	/**
	 * The test value for the property with the name of the premis ontology.
	 */
	private static final String NEW_PREMIS_ONTOLOGY = "premisOntology2.owl";
	/**
	 * The test value for the property with the name of the SKOS ontology.
	 */
	private static final String NEW_SKOS_ONTOLOGY = "skosOntology2.owl";
	/**
	 * The content of the ontology file that should be added in the temporary directory.
	 */
	private static final String ONTOLGOY_FILE_CONTENT = "abcdefghijklmnopqrstuvwxyz";
	/**
	 * The path of the configuration file for the tests.
	 */
	private static final String PATH_CONFIGURATION_FILE = "config.properties";
	/**
	 * Contains a test mapping for meta data elements to properties in the ontologies.
	 */
	private static final String PATH_MAPPING_CONFIGURATION_FILE = "digitalObjectsMapping.properties";
	/**
	 * The working directory the tests are working with.
	 */
	private WorkingDirectory workingDirectory;
	/**
	 * The configuration data of the server we are testing with.
	 */
	private ServerConfigurationData serverConfigurationData;
	/**
	 * The properties file that contains the configuration of the server.
	 */
	private File configFile;
	/**
	 * The properties file that contains the mapping information for digital objects.
	 */
	private File mappingFile;
	
	/**
	 * Creates an object and creates a properties file with configuration data for testing
	 * purposes. 
	 * @param workingDirectoryFolder Contains the working directory.
	 * @throws IOException
	 */
	public ConfigurationTestApi(File workingDirectoryFolder) throws IOException {
		workingDirectory = new WorkingDirectoryImpl(workingDirectoryFolder.getAbsolutePath());
		createTestServerConfigurationData();
		configFile = new File(workingDirectoryFolder, PATH_CONFIGURATION_FILE);
		mappingFile = new File(workingDirectory.getConfigurationDirectory(), PATH_MAPPING_CONFIGURATION_FILE);
		createTestConfigurationFile();
		createTestMappingConfigurationFile();
	}
	
	/**
	 * Creates a properties file for the tests. 
	 * @throws IOException
	 */
	private void createTestConfigurationFile() throws IOException {
		final Properties properties = new Properties();
		properties.setProperty(ConfigurationFileProperties.WORKING_DIRECTORY.getPropertyName(), WORKING_DIRECTORY);
		properties.setProperty(ConfigurationFileProperties.RULE_ONTOLOGY.getPropertyName(), RULE_ONTOLOGY);
		properties.setProperty(ConfigurationFileProperties.BASE_ONTOLOGY.getPropertyName(), BASE_ONTOLOGY);
		properties.setProperty(ConfigurationFileProperties.PREMIS_ONTOLOGY.getPropertyName(), PREMIS_ONTOLOGY);
		properties.setProperty(ConfigurationFileProperties.SKOS_ONTOLOGY.getPropertyName(), SKOS_ONTOLOGY);
		final OutputStream outputStream = new FileOutputStream(configFile);
		properties.store(outputStream, "");
	}
	
	/**
	 * Creates some configuration data for the test. These data are written in a properties file.
	 * @return The configuration data for the test.
	 */
	private ServerConfigurationData createTestServerConfigurationData() {
		final byte[] fileContent = ONTOLGOY_FILE_CONTENT.getBytes();
		serverConfigurationData = new ServerConfigurationDataImpl();
		serverConfigurationData.setWorkingDirectory(workingDirectory);
		serverConfigurationData.setConfiguredBaseOntologyName(NEW_BASE_ONTOLOGY);
		serverConfigurationData.setConfiguredRuleOntologyName(NEW_RULE_ONTOLOGY);
		serverConfigurationData.setConfiguredPremisOntologyName(NEW_PREMIS_ONTOLOGY);
		serverConfigurationData.setConfiguredSkosOntologyName(NEW_SKOS_ONTOLOGY);
		serverConfigurationData.setRuleOntologyContent(fileContent);
		serverConfigurationData.setPremisOntologyContent(fileContent);
		serverConfigurationData.setBaseOntologyContent(fileContent);
		serverConfigurationData.setSkosOntologyContent(fileContent);
		
		return serverConfigurationData;
	}
	
	/**
	 * Creates some test data for the configuration of the mapping for digital objects.
	 * @throws IOException
	 */
	private void createTestMappingConfigurationFile() throws IOException {
		final Properties properties = new Properties();
		final Namespace namespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
		final LocalName localNameOfProperty1 = ServerModelFactory.createLocalName(MAPPING_PAGE_COUNT);
		final LocalName localNameOfProperty2 = ServerModelFactory.createLocalName(MAPPING_FILE_SIZE);
		final Iri iriOfProperty1 = ServerModelFactory.createIri(namespace, localNameOfProperty1);
		final Iri iriOfProperty2 = ServerModelFactory.createIri(namespace, localNameOfProperty2);
		properties.setProperty(MAPPING_PAGE_COUNT, iriOfProperty1.toString());
		properties.setProperty(MAPPING_FILE_SIZE, iriOfProperty2.toString());
		
		final OutputStream outputStream = new FileOutputStream(mappingFile);
		properties.store(outputStream, "");
	}
	
	/**
	 * Returns the file that contains the test configuration for the mapping.
	 * @return The file with the configuration for the mapping.
	 */
	public File getMappingFile() {
		return mappingFile;
	}

	/**
	 * Checks the mapping read from the configuration file.
	 * @param mappingMap The mapping read from the configuration file by the test. 
	 * @return True if the mapping was read correctly false otherwise.
	 */
	public boolean checkMapping(final MetadataMappingMap mappingMap) {
		if (!checkMetadataElementPageCount(mappingMap)) {
			return false;
		}
		
		if (!checkMetadataElementFileSize(mappingMap)) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Checks if the meta data element pageCount has been read correctly from the configuration file.
	 * @param mappingMap The mapping read from the configuration file by the test. 
	 * @return True if the mapping was read correctly false otherwise.
	 */
	private boolean checkMetadataElementPageCount(final MetadataMappingMap mappingMap) {
		Optional<Iri> optionalWithIri = mappingMap.getIriOfPropertyMappedBy(MAPPING_PAGE_COUNT);
		
		if (!optionalWithIri.isPresent()) {
			return false;
		}
		
		Iri iriOfPageCount = optionalWithIri.get();
		
		LocalName localNameOfPageCount = iriOfPageCount.getLocalName();
		if (StringUtils.areStringsUnequal(localNameOfPageCount.toString(), MAPPING_PAGE_COUNT)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if the meta data element fileSize has been read correctly from the configuration file.
	 * @param mappingMap The mapping read from the configuration file by the test. 
	 * @return True if the mapping was read correctly false otherwise.
	 */
	private boolean checkMetadataElementFileSize(final MetadataMappingMap mappingMap) {
		Optional<Iri> optionalWithIriOfFileSize = mappingMap.getIriOfPropertyMappedBy(MAPPING_FILE_SIZE);
		if (!optionalWithIriOfFileSize.isPresent()) {
			return false;
		}
		
		Iri iriOfFileSize = optionalWithIriOfFileSize.get();
		LocalName localNameOfFileSize = iriOfFileSize.getLocalName();
		if (StringUtils.areStringsUnequal(localNameOfFileSize.toString(), MAPPING_FILE_SIZE)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if the configuration file exists. Throws an assertion error if the file does not exist.
	 */
	public void checkConfigurationFileExists() {
		final boolean configFileExists = configFile.exists();
		assertTrue(configFileExists);
	}
	
	/**
	 * Checks if the configuration file that is used for the tests contains the excepted value for a certain property.
	 * @param configurationFileProperties The property whose values we want to check.
	 * @param expectedValue The value we are expceting.
	 */
	public void checkConfigurationFileProperty(final ConfigurationFileProperties configurationFileProperties, final String expectedValue) {
		final Properties properties = new Properties();
		
		try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(configFile))) {
			properties.load(bufferedInputStream);
		} catch (IOException ex) {
			fail();
		}
		
		final String ruleOntologyPathProperty = properties.getProperty(configurationFileProperties.getPropertyName());
		assertThat(ruleOntologyPathProperty, is(equalTo(expectedValue)));
	}
	
	/**
	 * Creates a path for a file in the configuration directory of the server. The file path is contained in the working
	 * directory.
	 * @param filename The file that should be contained in the configuration directory.
	 * @return A path for a file in the configuration directory.
	 */
	public String createConfigurationDirectoryPath(final String filename) {
		return workingDirectory.getConfigurationDirectory().getAbsolutePath() + File.separator +  filename;
	}
	
	/**
	 * Returns the working directory for the tests.
	 * @return The working directory for the tests.
	 */
	public WorkingDirectory getWorkingDirectory() {
		return workingDirectory;
	}
	
	/**
	 * Returns the configuration data we are using for the test. 
	 * @return  The configuration data we are using for the test.
	 */
	public ServerConfigurationData getServerConfigurationData() {
		return serverConfigurationData;
	}
	
	/**
	 * Returns the configuration file we are using for the test. 
	 * @return  The configuration file we are using for the test.
	 */
	public File getConfigFile() {
		return configFile;
	}
	
	/**
	 * Returns the path of the rule ontology that has been initialized in the configuration file. 
	 * @return  The path of the rule ontology.
	 */
	public String getOriginalRuleOntologyPropertyValue() {
		return RULE_ONTOLOGY;
	}

	/**
	 * Returns the path of the SKOS ontology that has been initialized in the configuration file. 
	 * @return  The path of the SKOS ontology.
	 */
	public String getOriginalSkosOntologyPropertyValue() {
		return SKOS_ONTOLOGY;
	}
	
	/**
	 * Returns the path of the base ontology that has been initialized in the configuration file. 
	 * @return  The path of the base ontology.
	 */
	public String getOriginalBaseOntologyPropertyValue() {
		return BASE_ONTOLOGY;
	}
	
	/**
	 * Returns the path of the Premis ontology that has been initialized in the configuration file. 
	 * @return  The path of the Premis ontology.
	 */
	public String getOriginalPremisOntologyPropertyValue() {
		return PREMIS_ONTOLOGY;
	}
	
	/**
	 * Returns the new path of the rule ontology that after the path has been modified.  
	 * @return  The new path of the rule ontology.
	 */
	public String getExpectedRuleOntologyPropertyValue() {
		return NEW_RULE_ONTOLOGY;
	}

	/**
	 * Returns the new path of the SKOS ontology that after the path has been modified.  
	 * @return  The new path of the SKOS ontology.
	 */
	public String getExpectedSkosOntologyPropertyValue() {
		return NEW_SKOS_ONTOLOGY;
	}
	
	/**
	 * Returns the new path of the base ontology that after the path has been modified.  
	 * @return  The new path of the base ontology.
	 */
	public String getExpectedBaseOntologyPropertyValue() {
		return NEW_BASE_ONTOLOGY;
	}
	
	/**
	 * Returns the new path of the Premis ontology that after the path has been modified.  
	 * @return  The new path of the Premis ontology.
	 */
	public String getExpectedPremisOntologyPropertyValue() {
		return NEW_PREMIS_ONTOLOGY;
	}
	
	/**
	 * Checks the content of the SKOS ontology. 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void checkSkosOntologyFileContent() throws FileNotFoundException, IOException {
		final String expectedOntologyPath =  workingDirectory.getConfigurationDirectory().getAbsolutePath() + File.separator +  getExpectedSkosOntologyPropertyValue();
		checkOntologyFileContent(expectedOntologyPath);
	}
	
	/**
	 * Checks the content of the Premis ontology. 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void checkPremisOntologyFileContent() throws FileNotFoundException, IOException {
		final String expectedOntologyPath =  workingDirectory.getConfigurationDirectory().getAbsolutePath() + File.separator +  getExpectedPremisOntologyPropertyValue();
		checkOntologyFileContent(expectedOntologyPath);
	}
	
	/**
	 * Checks the content of the rule ontology. 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void checkRuleOntologyFileContent() throws FileNotFoundException, IOException {
		final String expectedOntologyPath =  workingDirectory.getConfigurationDirectory().getAbsolutePath() + File.separator +  getExpectedRuleOntologyPropertyValue();
		checkOntologyFileContent(expectedOntologyPath);
	}
	
	/**
	 * Checks the content of the base ontology. 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void checkBaseOntologyFileContent() throws FileNotFoundException, IOException {
		final String expectedOntologyPath =  workingDirectory.getConfigurationDirectory().getAbsolutePath() + File.separator +  getExpectedBaseOntologyPropertyValue();
		checkOntologyFileContent(expectedOntologyPath);
	}
	
	/**
	 * Checks the content of an ontology file. For the check the constants defined in this class are used. 
	 * @param ontologyPath The path of the file we want to check.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void checkOntologyFileContent(final String ontologyPath) throws FileNotFoundException, IOException {
		File expectedOntology = new File(ontologyPath);
		assertTrue(expectedOntology.exists());
		
		try (FileReader fileReader = new FileReader(expectedOntology);
			 BufferedReader bufferedReader = new BufferedReader(fileReader);) {
			String content = bufferedReader.readLine();
			assertThat(content, is(equalTo(ONTOLGOY_FILE_CONTENT)));
		}
	}
}
