package de.feu.kdmp4.packagingtoolkit.server.configuration.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;

import de.feu.kdmp4.packagingtoolkit.exceptions.ConfigurationException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.testapi.ConfigurationFileProperties;
import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.testapi.ConfigurationTestApi;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * Tests the operations layer of the configuration module. 
 * @author Christopher Olbertz
 *
 */
public class ConfigurationOperationsTest  {
	/**
	 * The operations layer is mocked with Mockito. Therefore not the real methods are called.
	 */
	@Mock
	private ConfigurationOperations configurationOperations;
	/**
	 * Creates a temporary working directory for the test. It is removed after the test.
	 */
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	/**
	 * The working directory the tests arae working with.
	 */
	private File workingDirectoryFolder;
	/**
	 * Contains some method that are relevant for all tests for example methods that create test data.
	 */
	private ConfigurationTestApi configurationTestApi;
	
	// ************ Setup and tear down ************
	/**
	 * Sets up the tests. Creates an implementation for the operation class and
	 * creates an object for the configuration file. 
	 * @throws IOException 
	 */
	@Before
	public void setup() throws IOException {
		configurationOperations = new ConfigurationOperationsPropertiesImpl();
		workingDirectoryFolder = temporaryFolder.newFolder();
		configurationTestApi = new ConfigurationTestApi(workingDirectoryFolder);
	}
	
	// ************** Test methods **************
	/**
	 * Tests the configuration of the SKOS ontology. The test is successful if
	 * <ul>
	 * 	<li>the property file exists</li> 
	 *     <li>if the value of the property skosOntology has the correct string</li>
	 *     <li>The ontology file itself has been created in the configuration directory 
	 *  of the server and has the correct content.</li>
	 * </ul> 
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	@Test
	public void testConfigureSkosOntology() throws FileNotFoundException, IOException {
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		final ServerConfigurationData serverConfigurationData = configurationTestApi.getServerConfigurationData();
		final File configFile = configurationTestApi.getConfigFile();
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		
		spiedConfigurationOperations.configureServer(serverConfigurationData);
		configurationTestApi.checkConfigurationFileExists();
		final String expectedSkosOntologyPath = configurationTestApi.getExpectedSkosOntologyPropertyValue();
		configurationTestApi.checkConfigurationFileProperty(ConfigurationFileProperties.SKOS_ONTOLOGY, expectedSkosOntologyPath);
		configurationTestApi.checkPremisOntologyFileContent();
	}
	
	/**
	 * Tests the configuration of Premis ontology. The test is successful if
	 * <ul>
	 * 	<li>the property file exists</li> 
	 *     <li>if the value of the property premisOntology has the correct string</li>
	 *     <li>The ontology file itself has been created in the configuration directory 
	 *  of the server and has the correct content.</li>
	 * </ul> 
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	@Test
	public void testConfigurePremisOntology() throws FileNotFoundException, IOException {
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		final ServerConfigurationData serverConfigurationData = configurationTestApi.getServerConfigurationData();
		final File configFile = configurationTestApi.getConfigFile();
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		
		spiedConfigurationOperations.configureServer(serverConfigurationData);
		configurationTestApi.checkConfigurationFileExists();
		final String expectedPremisOntologyPath = configurationTestApi.getExpectedPremisOntologyPropertyValue();
		configurationTestApi.checkConfigurationFileProperty(ConfigurationFileProperties.PREMIS_ONTOLOGY, expectedPremisOntologyPath);
		configurationTestApi.checkPremisOntologyFileContent();
	}
	
	/**
	 * Tests the configuration of the rule ontology. The test is successful if
	 * <ul>
	 * 	<li>the property file exists</li> 
	 *     <li>if the value of the property ruleOntology has the correct string</li>
	 *     <li>The ontology file itself has been created in the configuration directory 
	 *  of the server and has the correct content.</li>
	 * </ul> 
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	@Test
	public void testConfigureRuleOntology() throws FileNotFoundException, IOException {
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		final ServerConfigurationData serverConfigurationData = configurationTestApi.getServerConfigurationData();
		final File configFile = configurationTestApi.getConfigFile();
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		
		spiedConfigurationOperations.configureServer(serverConfigurationData);
		configurationTestApi.checkConfigurationFileExists();
		final String expectedRuleOntologyPath = configurationTestApi.getExpectedRuleOntologyPropertyValue();
		configurationTestApi.checkConfigurationFileProperty(ConfigurationFileProperties.RULE_ONTOLOGY, expectedRuleOntologyPath);
		configurationTestApi.checkRuleOntologyFileContent();
	}
	
	/**
	 * Tests the configuration of the base ontology. The test is successful if
	 * <ul>
	 * 	<li>the property file exists</li> 
	 *     <li>if the value of the property baseOntology has the correct string</li>
	 *     <li>The ontology file itself has been created in the configuration directory 
	 *  of the server and has the correct content.</li>
	 * </ul> 
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	@Test
	public void testConfigureBaseOntology() throws FileNotFoundException, IOException {
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		final ServerConfigurationData serverConfigurationData = configurationTestApi.getServerConfigurationData();
		final File configFile = configurationTestApi.getConfigFile();
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		
		spiedConfigurationOperations.configureServer(serverConfigurationData);
		configurationTestApi.checkConfigurationFileExists();
		final String expectedBaseOntologyPath = configurationTestApi.getExpectedBaseOntologyPropertyValue();
		configurationTestApi.checkConfigurationFileProperty(ConfigurationFileProperties.BASE_ONTOLOGY, expectedBaseOntologyPath);
		configurationTestApi.checkBaseOntologyFileContent();
	}
	
	/**
	 * Tests the configuration of the working directory. The test is successful if the 
	 * property file exists and if the value of the property workingDirectory has
	 * the correct string.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testConfigureWorkingDirectory() throws FileNotFoundException, IOException {
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		final ServerConfigurationData serverConfigurationData = configurationTestApi.getServerConfigurationData();
		final File configFile = configurationTestApi.getConfigFile();
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		
		spiedConfigurationOperations.configureServer(serverConfigurationData);
		configurationTestApi.checkConfigurationFileExists();
		final String expectedWorkingDirectoryPath = configurationTestApi.getWorkingDirectory().getWorkingDirectoryPath();
		configurationTestApi.checkConfigurationFileProperty(ConfigurationFileProperties.WORKING_DIRECTORY, expectedWorkingDirectoryPath);
	}
	
	/**
	 * Tests the configuration of the working directory if the configuration file 
	 * does not exist. A 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.configuration.exceptions.ConfigurationException}
	 * has to be thrown and its message has to be the correct message.
	 */
	@Test
	public void testConfigureWorkingDirectoryNotWritable() {
		try {
			final ServerConfigurationData serverConfigurationData = configurationTestApi.getServerConfigurationData();
			final File configFile = configurationTestApi.getConfigFile();
			final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
			configFile.setWritable(false);
			doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
			spiedConfigurationOperations.configureServer(serverConfigurationData);
			fail();
		} catch (ConfigurationException ex) {
			String actualMessage = ex.getMessage();
			actualMessage = String.format(actualMessage, configurationTestApi.getConfigFile());
			String expectedMessage = I18nExceptionUtil.getConfigurationFileNotWritableString();
			expectedMessage = String.format(actualMessage, configurationTestApi.getConfigFile());
			assertThat(actualMessage, is(equalTo(expectedMessage)));
		}
	}

	/**
	 * Tests getting the path of the rule ontology from the properties file.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testGetRuleOntologyPath() throws FileNotFoundException, IOException {
		final File configFile = configurationTestApi.getConfigFile();
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		final ServerConfigurationData configurationData = spiedConfigurationOperations.readServerConfiguration();
		final String actualRuleOntologyPath = configurationData.getConfiguredRuleOntologyName();
		
		// Read the expected rule ontology path from the properties file.
		final String expectedRuleOntologyPath = configurationTestApi.getOriginalRuleOntologyPropertyValue();
		assertThat(actualRuleOntologyPath, is(equalTo(expectedRuleOntologyPath)));
	}
	
	/**
	 * Tests getting the path of the base ontology from the properties file.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testGetBaseOntologyPath() throws FileNotFoundException, IOException {
		final File configFile = configurationTestApi.getConfigFile();
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		final ServerConfigurationData configurationData = spiedConfigurationOperations.readServerConfiguration();
		final String actualBaseOntologyPath = configurationData.getConfiguredBaseOntologyName();
		
		// Read the expected base ontology path from the properties file.
		final String expectedBaseOntologyPath = configurationTestApi.getOriginalBaseOntologyPropertyValue();
		assertThat(actualBaseOntologyPath, is(equalTo(expectedBaseOntologyPath)));
	}
	
	/**
	 * Tests getting the path of the Premis ontology from the properties file.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testGetPremisOntologyPath() throws FileNotFoundException, IOException {
		final File configFile = configurationTestApi.getConfigFile();
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		final ServerConfigurationData configurationData = spiedConfigurationOperations.readServerConfiguration();
		final String actualPremisOntologyPath = configurationData.getConfiguredPremisOntologyName();
		
		// Read the expected premis ontology path from the properties file.
		final String expectedPremisOntologyPath = configurationTestApi.getOriginalPremisOntologyPropertyValue();
		assertThat(actualPremisOntologyPath, is(equalTo(expectedPremisOntologyPath)));
	}
	
	/**
	 * Tests getting the path of the SKOS ontology from the properties file.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testGetSkosOntologyPath() throws FileNotFoundException, IOException {
		final File configFile = configurationTestApi.getConfigFile();
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
		final ServerConfigurationData configurationData = spiedConfigurationOperations.readServerConfiguration();
		final String actualSkosOntologyPath = configurationData.getConfiguredSkosOntologyName();
		
		// Read the expected SKOS ontology path from the properties file.
		final String expectedSkosOntologyPath = configurationTestApi.getOriginalSkosOntologyPropertyValue();
		assertThat(actualSkosOntologyPath, is(equalTo(expectedSkosOntologyPath)));
	}
	
	/**
	 * Tests the configuration of the working directory if the configuration file 
	 * does not exist. A 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.configuration.exceptions.ConfigurationException}
	 * has to be thrown and its message has to be the correct message. Before the
	 * test starts, the configuration file is deleted.
	 */
	@Test
	public void testGetWorkingDirectoryNotExistent() {
		try {
			final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
			final File configFile = configurationTestApi.getConfigFile();
			configFile.delete();
			doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
			spiedConfigurationOperations.readServerConfiguration();
			fail();
		} catch (ConfigurationException ex) {
			String actualMessage = ex.getMessage();
			actualMessage = String.format(actualMessage, configurationTestApi.getConfigFile());
			String expectedMessage = I18nExceptionUtil.getConfigurationFileDoesNotExistString();
			expectedMessage = String.format(actualMessage, configurationTestApi.getConfigFile());
			assertThat(actualMessage, is(equalTo(expectedMessage)));			
		}
	}
	
	/**
	 * Tests the configuration of the working directory if the configuration file 
	 * is not readable. A 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.configuration.exceptions.ConfigurationException}
	 * has to be thrown and its message has to be the correct message.
	 */
	@Test
	public void testGetWorkingDirectoryNotReadable() {
		try {
			final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
			final File configFile = configurationTestApi.getConfigFile();
			configFile.setReadable(false);
			doReturn(configFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfConfigurationFile();
			spiedConfigurationOperations.readServerConfiguration();
			fail();
		} catch (ConfigurationException ex) {
			String actualMessage = ex.getMessage();
			actualMessage = String.format(actualMessage, configurationTestApi.getConfigFile());
			String expectedMessage = I18nExceptionUtil.getConfigurationFileNotReadableString();
			expectedMessage = String.format(actualMessage, configurationTestApi.getConfigFile());
			assertThat(actualMessage, is(equalTo(expectedMessage)));
		}
	}
	
	@Test
	public void testReadMetadataMappingConfiguration() {
		final WorkingDirectory workingDirectory = configurationTestApi.getWorkingDirectory();
		final File mappingFile = configurationTestApi.getMappingFile();
		final ConfigurationOperations spiedConfigurationOperations = spy(configurationOperations);
		doReturn(mappingFile.getAbsolutePath()).when(spiedConfigurationOperations).getPathOfMappingFile(workingDirectory);
		
		final MetadataMappingMap mappingMap = spiedConfigurationOperations.readMetadataMappingConfiguration(workingDirectory);
		
		boolean mappingMapCorrect = configurationTestApi.checkMapping(mappingMap);
		assertTrue(mappingMapCorrect);
	}
}
