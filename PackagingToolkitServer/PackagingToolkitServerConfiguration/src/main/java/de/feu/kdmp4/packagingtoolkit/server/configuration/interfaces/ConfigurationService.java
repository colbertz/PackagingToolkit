package de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces;

import de.feu.kdmp4.packagingtoolkit.exceptions.ConfigurationException;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * The interface for the service methods for configuring the server. 
 * @author Christopher Olbertz
 *
 */
public interface ConfigurationService {
	/**
	 * Configures the working directory of the server.
	 * @param workingDirectory The path of the new working directory.
	 */
	void configureServer(final ServerConfigurationDataResponse serverConfigurationData);
	/**
	 * Determines the working directory from the configuration file.
	 * @return The working directory.
	 */
	WorkingDirectory getWorkingDirectory();
	/**
	 * Reads the configuration of the server and prepares it for sending to
	 * the client.
	 * @return The configuration data of the server.
	 */
	ServerConfigurationDataResponse readServerConfiguration();
	/**
	 * Configures the rule ontology on the server. The file is written to the configuration
	 * directory and the path is saved in the configuration file.
	 * @param fileName the name of the file that contains the rule ontology.
	 * @param fileContent Contains the data of the file.
	 * @throws ConfigurationException if the file is not writable or there were any other error while
	 * writing the file 
	 */
	void configureRuleOntology(final String fileName, final byte[] fileContent);
	/**
	 * Configures the base ontology on the server. The file is written to the configuration
	 * directory and the path is saved in the configuration file.
	 * @param fileName the name of the file that contains the base ontology.
	 * @param fileContent Contains the data of the file.
	 * @throws ConfigurationException if the file is not writable or there were any other error while
	 * writing the file 
	 */
	void configureBaseOntology(final String fileName, final byte[] fileContent);
	/**
	 * Sets the configuration operations object.
	 * @param configurationOperations The configuration operations object.
	 */
	void setConfigurationOperations(final ConfigurationOperations configurationOperations);
	/**
	 * Saves a Premis ontology file in the configuration directory of the server.
	 * @param fileName The name of the file.
	 * @param fileContent The content of the file received from the client.
	 */
	void configurePremisOntology(final String fileName, final byte[] fileContent);
	/**
	 * Saves a SKOS ontology file in the configuration directory of the server.
	 * @param fileName The name of the file.
	 * @param fileContent The content of the file received from the client.
	 */
	void configureSkosOntology(final String fileName, final byte[] fileContent);
	/**
	 * Reads the mapping of meta data elements to properties from the ontology from the 
	 * configuration file. 
	 * @return A map that contains the mapping configuration.
	 */
	MetadataMappingMap readMetadataMappingConfiguration();
}
