/**
 * Contains all implementing classes of the configuration module. This module is used 
 * to configure the server.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.configuration.classes;