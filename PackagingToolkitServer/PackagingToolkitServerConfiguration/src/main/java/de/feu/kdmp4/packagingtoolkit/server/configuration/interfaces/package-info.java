/**
 * Contains the interfaces for the configuration module.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces;