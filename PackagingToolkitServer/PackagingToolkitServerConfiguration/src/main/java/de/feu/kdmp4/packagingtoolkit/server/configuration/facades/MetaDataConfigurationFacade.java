package de.feu.kdmp4.packagingtoolkit.server.configuration.facades;

import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationService;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;

/**
 * Contains the methods the module MetaData uses for accessing the methods of the module Configuration.
 * @author Christopher Olbertz
 *
 */
public class MetaDataConfigurationFacade {
	/**
	 * Contains the business logic.
	 */
	private ConfigurationService configurationService;
	
	/**
	 * Reads the mapping of meta data elements to properties from the ontology from the 
	 * configuration file. 
	 * @return A map that contains the mapping configuration.
	 */
	public MetadataMappingMap readMetadataMappingConfiguration() {
		return configurationService.readMetadataMappingConfiguration();
	}
	
	/**
	 * Sets the object that contains the business logic.
	 * @param configurationOperations The object that contains the business logic.
	 */
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
