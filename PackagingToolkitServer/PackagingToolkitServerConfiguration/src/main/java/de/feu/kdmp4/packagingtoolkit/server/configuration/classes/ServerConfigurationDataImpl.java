package de.feu.kdmp4.packagingtoolkit.server.configuration.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Contains the data that are used for configuring the server. 
 * @author Christopher Olbertz
 *
 */
public class ServerConfigurationDataImpl implements ServerConfigurationData {
	/**
	 * The name of the base ontology in the configuration directory of the server. 
	 */
	private String configuredBaseOntologyPath;
	/**
	 * The name of the rule ontology in the configuration directory of the server. 
	 */
	private String configuredRuleOntologyPath;
	/**
	 * The name of the Premis ontology in the configuration directory of the server. 
	 */
	private String configuredPremisOntologyPath;
	/**
	 * The name of the SKOS ontology in the configuration directory of the server. 
	 */
	private String configuredSkosOntologyPath;
	/**
	 * The class in the ontology that contains the domain specific data. In the most
	 * cases it will be the class Information_Package. At the moment it may not be
	 * configured by the user. It may be only configured by manually modification
	 * of the properties file. 
	 */
	private String baseOntologyClass;
	/**
	 * The class in the ontology that is the starting point for SKOS in the base ontology. In the most
	 * cases it will be the class Research_Domain_Information. At the moment it may not be
	 * configured by the user. It may be only configured by manually modification
	 * of the properties file. 
	 */
	private String mainSkosClass;
	/**
	 * The working directory of the server. It contains a directory called configuration that contains
	 * all information about the configuration of the server.
	 */
	private WorkingDirectory workingDirectory;
	/**
	 * The content of the base ontology received from the client if the user wants to upload a new 
	 * base ontology.
	 */
	private byte[] baseOntologyContent;
	/**
	 * The content of the Premis ontology received from the client if the user wants to upload a new 
	 * Premis ontology.
	 */
	private byte[] premisOntologyContent;
	/**
	 * The content of the rule ontology received from the client if the user wants to upload a new 
	 * rule ontology.
	 */
	private byte[] ruleOntologyContent;
	/**
	 * The content of the SKOS ontology received from the client if the user wants to upload a new 
	 * SKOS ontology.
	 */
	private byte[] skosOntologyContent;
	
	/**
	 * Creates a new object without any data.
	 */
	public ServerConfigurationDataImpl() { }
	
	/**
	 * Creates a new object with some data. Ths constructor is used if the user wants to configure the base and 
	 * the rule ontology.
	 * @param configuredBaseOntologyPath The path of the base ontology in the configuration directory.
	 * @param baseOntologyContent Some content for the base ontology.
	 * @param configuredRuleOntologyPath The path of the rule ontology in the configuration directory.
	 * @param ruleOntologyContent Some content for the rule ontology.
	 */
	public ServerConfigurationDataImpl(final String configuredBaseOntologyPath, 
			final byte[] baseOntologyContent, final String configuredRuleOntologyPath,  
			final byte[] ruleOntologyContent) {
		this(configuredBaseOntologyPath, baseOntologyContent,
			 configuredRuleOntologyPath, ruleOntologyContent, null);
	}
	
	/**
	 * Creates a new object with some data. Ths constructor is used if the user wants to configure the base and 
	 * the rule ontology.
	 * @param configuredBaseOntologyPath The path of the base ontology in the configuration directory.
	 * @param baseOntologyContent Some content for the base ontology.
	 * @param configuredRuleOntologyPath The path of the rule ontology in the configuration directory.
	 * @param ruleOntologyContent Some content for the rule ontology.
	 * @param workingDirectory The working directory of the server. It contains a directory called configuration that contains
	 * all information about the configuration of the server.
	 */
	public ServerConfigurationDataImpl(final String configuredBaseOntologyPath, 
			final byte[] baseOntologyContent, final String configuredRuleOntologyPath,  
			final byte[] ruleOntologyContent, final WorkingDirectory workingDirectory) {
		this.configuredBaseOntologyPath = configuredBaseOntologyPath;
		this.baseOntologyContent = baseOntologyContent;
		this.configuredRuleOntologyPath = configuredRuleOntologyPath;
		this.ruleOntologyContent = ruleOntologyContent;
		this.workingDirectory = workingDirectory;
	}
	
	/**
	 * Returns the working directory of the server. The working directory of the server. It contains a directory called configuration that contains
	 * all information about the configuration of the server.
	 * @return The working directory of the server.
	 */
	@Override
	public WorkingDirectory getWorkingDirectory() {
		return workingDirectory;
	}
	
	/**
	 * Determines only the path of the working directory of the server.
	 * @return The path of the working directory of the server.
	 */
	@Override
	public String getWorkingDirectoryPath() {
		if (workingDirectory != null) {
			return workingDirectory.getWorkingDirectoryPath();
		} else {
			return StringUtils.EMPTY_STRING;
		}
	}

	@Override
	public String getConfiguredRuleOntologyName() {
		return configuredRuleOntologyPath;
	}

	@Override
	public String getConfiguredBaseOntologyName() {
		return configuredBaseOntologyPath;
	}

	@Override
	public void setConfiguredBaseOntologyName(final String configuredBaseOntologyPath) {
		this.configuredBaseOntologyPath = configuredBaseOntologyPath;
	}

	@Override
	public void setConfiguredRuleOntologyName(final String configuredRuleOntologyPath) {
		this.configuredRuleOntologyPath = configuredRuleOntologyPath;
	}

	@Override
	public void setWorkingDirectory(final WorkingDirectory workingDirectory) {
		this.workingDirectory = workingDirectory;
	}

	@Override
	public String getBaseOntologyClass() {
		return baseOntologyClass;
	}

	@Override
	public void setBaseOntologyClass(final String baseOntologyClass) {
		this.baseOntologyClass = baseOntologyClass;
	}
	
	@Override
	public File getBaseOntologyFile() {
		String baseOntologyPath = getBaseOntologyPath();
		return new File(baseOntologyPath);
	}
	
	@Override
	public File getPremisOntologyFile() {
		String premisOntologyPath = getPremisOntologyPath();
		return new File(premisOntologyPath);
	}
	
	@Override
	public File getSkosOntologyFile() {
		String skosOntologyPath = getSkosOntologyPath();
		return new File(skosOntologyPath);
	}

	@Override
	public String getTemporaryDirectory() {
		final WorkingDirectory workingDirectory = getWorkingDirectory();
		return workingDirectory.getTemporaryDirectory().getAbsolutePath();
	}
	
	@Override
	public String getPremisOntologyPath() {
		return workingDirectory.getConfigurationDirectory() + File.separator + configuredPremisOntologyPath;
	}
	
	@Override
	public String getSkosOntologyPath() {
		return workingDirectory.getConfigurationDirectory() + File.separator + configuredSkosOntologyPath;
	}
	
	@Override
	public String getBaseOntologyPath() {
		return workingDirectory.getConfigurationDirectory() + File.separator + configuredBaseOntologyPath;
	}
	
	@Override
	public String getRuleOntologyPath() {
		return workingDirectory.getConfigurationDirectory() + File.separator + configuredRuleOntologyPath;
	}
	
	@Override
	public String getConfiguredPremisOntologyName() {
		return configuredPremisOntologyPath;
	}
	
	@Override
	public String getConfiguredSkosOntologyName() {
		return configuredSkosOntologyPath;
	}

	@Override
	public void setConfiguredPremisOntologyName(String configuredPremisOntologyPath) {
		this.configuredPremisOntologyPath = configuredPremisOntologyPath;
	}

	@Override
	public void setConfiguredSkosOntologyName(String configuredSkosOntologyPath) {
		this.configuredSkosOntologyPath = configuredSkosOntologyPath;
	}

	@Override
	public void setPremisOntologyContent(byte[] fileContent) {
		premisOntologyContent = fileContent;
	}
	
	@Override
	public byte[] getPremisOntologyContent() {
		return premisOntologyContent;
	}

	@Override
	public void setRuleOntologyContent(byte[] fileContent) {
		this.ruleOntologyContent = fileContent;
	}
	
	@Override
	public byte[] getRuleOntologyContent() {
		return ruleOntologyContent;
	}
	
	@Override
	public void setBaseOntologyContent(byte[] baseOntologyContent) {
		this.baseOntologyContent = baseOntologyContent;
	}
	
	@Override
	public byte[] getBaseOntologyContent() {
		return baseOntologyContent;
	}
	
	@Override
	public void setSkosOntologyContent(byte[] skosOntologyContent) {
		this.skosOntologyContent = skosOntologyContent;
	}
	
	@Override
	public byte[] getSkosOntologyContent() {
		return skosOntologyContent;
	}
	
	@Override
	public void setMainSkosClass(String mainSkosClass) {
		this.mainSkosClass = mainSkosClass;
	}
	
	@Override
	public String getMainSkosClass() {
		return mainSkosClass;
	}
}