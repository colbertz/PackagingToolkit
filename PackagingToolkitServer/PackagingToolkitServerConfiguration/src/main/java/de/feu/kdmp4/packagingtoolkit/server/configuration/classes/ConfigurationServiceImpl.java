package de.feu.kdmp4.packagingtoolkit.server.configuration.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationService;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * An implementation of {@link de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationService} with Spring 
 * annotations.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationServiceImpl implements ConfigurationService {
	/**
	 * A reference to a object of {@link de.feu.kdmp4.packagingtoolkit.server.
	 * facades.OperationsFacade}, that 
	 * contains the atomar operations for configuration. 
	 */
	private ConfigurationOperations configurationOperations;
	
	@Override
	public void configureRuleOntology(final String fileName, final byte[] fileContent) {
		final ServerConfigurationData serverConfigurationData = ConfigurationModelFactory.
				createEmptyServerConfigurationData();
		final WorkingDirectory workingDirectory = getWorkingDirectory();
		final File ruleOntologyFile = workingDirectory.saveFileContentInConfigurationDirectory(fileContent, fileName);
		final String ruleOntologyPath = ruleOntologyFile.getAbsolutePath();
		final String ruleOntologyName = PackagingToolkitFileUtils.extractFileName(ruleOntologyPath);
		serverConfigurationData.setConfiguredRuleOntologyName(ruleOntologyName);
		configurationOperations.configureServer(serverConfigurationData);
	}
	
	@Override
	public void configureBaseOntology(final String fileName, final byte[] fileContent) {
		final ServerConfigurationData serverConfigurationData = ConfigurationModelFactory.
				createEmptyServerConfigurationData();
		final WorkingDirectory workingDirectory = getWorkingDirectory();
		final File baseOntologyFile = workingDirectory.saveFileContentInConfigurationDirectory(fileContent, fileName);
		final String baseOntologyPath = baseOntologyFile.getAbsolutePath();
		final String baseOntologyName = PackagingToolkitFileUtils.extractFileName(baseOntologyPath);
		serverConfigurationData.setConfiguredBaseOntologyName(baseOntologyName);
		configurationOperations.configureServer(serverConfigurationData);
	}
	
	@Override
	public void configurePremisOntology(final String fileName, final byte[] fileContent) {
		final ServerConfigurationData serverConfigurationData = ConfigurationModelFactory.
				createEmptyServerConfigurationData();
		final WorkingDirectory workingDirectory = getWorkingDirectory();
		final File premisFile = workingDirectory.saveFileContentInConfigurationDirectory(fileContent, fileName);
		final String premisOntologyPath = premisFile.getAbsolutePath();
		final String premisOntologyName = PackagingToolkitFileUtils.extractFileName(premisOntologyPath);
		serverConfigurationData.setConfiguredPremisOntologyName(premisOntologyName);
		configurationOperations.configureServer(serverConfigurationData);
	}
	
	@Override
	public void configureSkosOntology(final String fileName, final byte[] fileContent) {
		final ServerConfigurationData serverConfigurationData = ConfigurationModelFactory.
				createEmptyServerConfigurationData();
		final WorkingDirectory workingDirectory = getWorkingDirectory();
		final File skosFile = workingDirectory.saveFileContentInConfigurationDirectory(fileContent, fileName);
		final String skosOntologyPath = skosFile.getAbsolutePath();
		final String skosOntologyName = PackagingToolkitFileUtils.extractFileName(skosOntologyPath);
		serverConfigurationData.setConfiguredSkosOntologyName(skosOntologyName);
		configurationOperations.configureServer(serverConfigurationData);
	}
	
	@Override
	public void configureServer(final ServerConfigurationDataResponse serverConfigurationDataResponse) {
		// Read the configuration data before they are changed.
		final ServerConfigurationData  oldServerConfigurationData = configurationOperations.readServerConfiguration();
		final ServerConfigurationData serverConfigurationData = convertToServerConfigurationData(serverConfigurationDataResponse);
		configurationOperations.configureServer(serverConfigurationData);
		deleteOldOntologyFiles(oldServerConfigurationData, serverConfigurationData);
	}

	/**
	 * Converts a response object to an object of type {@link de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData}
	 * for further processing on the server.
	 * @param serverConfigurationDataResponse The response object.
	 * @return The data contained in serverConfigurationDataResponse converted to an object 
	 * of type {@link de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData}.
	 */
	private ServerConfigurationData convertToServerConfigurationData(final ServerConfigurationDataResponse serverConfigurationDataResponse) {
		final String workingDirectoryPath = serverConfigurationDataResponse.getWorkingDirectoryPath();
		final String ruleOntologyPath = serverConfigurationDataResponse.getConfiguredRuleOntologyPath();
		final String baseOntologyPath = serverConfigurationDataResponse.getConfiguredBaseOntologyPath();
		final WorkingDirectory workingDirectory = ServerModelFactory.createWorkingDirectory(workingDirectoryPath);
		
		final ServerConfigurationData serverConfigurationData = ConfigurationModelFactory.
				createServerConfigurationData(baseOntologyPath, null, 
											  ruleOntologyPath, null, workingDirectory);
		serverConfigurationData.setConfiguredBaseOntologyName(baseOntologyPath);
		serverConfigurationData.setConfiguredRuleOntologyName(ruleOntologyPath);
		return serverConfigurationData;
	}
	
	/**
	 * Deletes the old ontology files from the configuration directory if there are new ones.
	 * @param oldServerConfigurationData The configuration data that can reference to old ontology files.
	 * @param newServerConfigurationData Contains in every case the paths to the new ontology files. Is compared with
	 * oldServerConfigurationData and if there are any differences, these old ontology files are removed.
	 */
	private void deleteOldOntologyFiles(final ServerConfigurationData oldServerConfigurationData, final ServerConfigurationData newServerConfigurationData) {
		final String oldBaseOntology = oldServerConfigurationData.getConfiguredBaseOntologyName();
		final String oldRuleOntology = oldServerConfigurationData.getConfiguredRuleOntologyName();
		final String oldSkosOntology = oldServerConfigurationData.getConfiguredSkosOntologyName();
		final String oldPremisOntology = oldServerConfigurationData.getConfiguredPremisOntologyName();
		final String newBaseOntology = newServerConfigurationData.getConfiguredBaseOntologyName();
		final String newRuleOntology = newServerConfigurationData.getConfiguredRuleOntologyName();
		final String newSkosOntology = newServerConfigurationData.getConfiguredSkosOntologyName();
		final String newPremisOntology = newServerConfigurationData.getConfiguredPremisOntologyName();
		configurationOperations.deleteOldOntologyFiles(oldBaseOntology, newBaseOntology, oldRuleOntology, newRuleOntology, newSkosOntology, 
				oldSkosOntology, newPremisOntology, oldPremisOntology);
	}
	
	@Override 
	public ServerConfigurationDataResponse readServerConfiguration() {
		final ServerConfigurationData configurationData = configurationOperations.readServerConfiguration();
		final String workingDirectoryPath = configurationData.getWorkingDirectoryPath();
		final String configuredRuleOntology = configurationData.getConfiguredRuleOntologyName();
		final String configuredBaseOntology = configurationData.getConfiguredBaseOntologyName();
		final String configuredSkosOntology = configurationData.getConfiguredSkosOntologyName();
		final String configuredPremisOntology = configurationData.getConfiguredPremisOntologyName();
		
		return ResponseModelFactory.getServerConfigurationData(configuredBaseOntology,
				configuredRuleOntology,	workingDirectoryPath,  configuredPremisOntology, configuredSkosOntology);
	}
	
	@Override
	public WorkingDirectory getWorkingDirectory() {
		final ServerConfigurationData configurationData = configurationOperations.readServerConfiguration();
		final String workingDirectoryPath = configurationData.getWorkingDirectoryPath();
		return ServerModelFactory.createWorkingDirectory(workingDirectoryPath);
	}

	@Override
	public void setConfigurationOperations(ConfigurationOperations configurationOperations) {
		this.configurationOperations = configurationOperations;
	}

	@Override
	public MetadataMappingMap readMetadataMappingConfiguration() {
		final ServerConfigurationData configurationData = configurationOperations.readServerConfiguration();
		final WorkingDirectory workingDirectory = configurationData.getWorkingDirectory();
		return configurationOperations.readMetadataMappingConfiguration(workingDirectory);
	}
}
