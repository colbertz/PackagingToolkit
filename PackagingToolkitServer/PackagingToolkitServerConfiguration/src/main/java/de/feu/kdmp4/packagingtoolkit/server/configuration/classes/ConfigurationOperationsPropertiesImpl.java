package de.feu.kdmp4.packagingtoolkit.server.configuration.classes;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import de.feu.kdmp4.packagingtoolkit.exceptions.ConfigurationException;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * An implementation of {@link de.feu.kdmp4.packagingtoolkit.server.configuration.
 * interfaces.ConfigurationOperations} with property files.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationOperationsPropertiesImpl implements ConfigurationOperations {
	/**
	 * The path of the configuration file.
	 */
	private static final String PATH_CONFIGURATION_FILE = "config.properties";
	/**
	 * The property in the property file that contains the name of the 
	 * base ontology. The base ontology lies in the configuration directory
	 * of the server.
	 */
	private static final String PROPERTY_BASE_ONTOLOGY = "baseOntology";
	/**
	 * Contains the information about the mapping of meta data elements to properties of classes
	 * in the ontology.
	 */
	private static final String PATH_MAPPING_CONFIGURATION_FILE = "digitalObjectsMapping.properties";
	/**
	 * The property in the property file that contains the name of the 
	 * Premis ontology. The Premis ontology lies in the configuration directory
	 * of the server.
	 */
	private static final String PROPERTY_PREMIS_ONTOLOGY = "premisOntology";
	/**
	 * The property in the property file that contains the name of the 
	 * SKOS ontology. The SKOS ontology lies in the configuration directory
	 * of the server.
	 */
	private static final String PROPERTY_SKOS_ONTOLOGY = "skosOntology";
	/**
	 * The property in the property file that contains the name of the 
	 * base ontology class. This is the class whose subclasses contain the meta 
	 * information that has to be entered by the user.
	 */
	private static final String PROPERTY_BASE_ONTOLOGY_CLASS = "baseOntologyClass";
	/**
	 * The property in the property file that contains the name of the 
	 * class that is the starting point for the SKOS ontology in the base ontology.
	 */
	private static final String PROPERTY_MAIN_SKOS_CLASS = "mainSkosClass";
	
	/**
	 * The property in the property file that contains the name of the 
	 * rule ontology. The rule ontology lies in the configuration directory
	 * of the server.
	 */
	private static final String PROPERTY_RULE_ONTOLOGY = "ruleOntology";
	/**
	 * The property for the working directory in the configuration file.
	 */
	private static final String PROPERTY_WORKING_DIRECTORY = "workingDirectory";
	
	/**
	 * The working directory read from the configuration file. 
	 */
	private WorkingDirectory workingDirectory;
	
	@Override
	public MetadataMappingMap readMetadataMappingConfiguration(WorkingDirectory workingDirectory) {
		return readMetadataMapping(workingDirectory);
	}
	
	/**
	 * Reads the mapping of meta data elements from the configuration file.
	 * @return The mapping meta data elements to properties.
	 */
	private MetadataMappingMap readMetadataMapping(WorkingDirectory workingDirectory) {
		final MetadataMappingMap metadataMappingMap = ServerModelFactory.createEmptyMetadataMappingMap();
		final String configPath = getPathOfMappingFile(workingDirectory);
		final File propertiesFile = new File(configPath);
		checkPropertyFileForReading(propertiesFile);
		
		try (BufferedInputStream bufferedInputStream = new BufferedInputStream(
				new FileInputStream(propertiesFile))) {
			final Properties properties = new Properties();
			properties.load(bufferedInputStream);
			
			final Set<Entry<Object, Object>> entrySet = properties.entrySet();
			for (final Entry<Object, Object> entry: entrySet) {
				final String metadataElementName = (String)entry.getKey();
				String value = (String)entry.getValue();
				if (!value.contains(StringUtils.SHARP)) {
					value = StringUtils.replaceLast(value, StringUtils.SLASH.charAt(0), StringUtils.SHARP.charAt(0));
				}
				final Iri iriOfProperty = ServerModelFactory.createIri(value);
				metadataMappingMap.addMetadataMapping(metadataElementName, iriOfProperty);
			}
		} catch (IOException ex) {
			ConfigurationException exception = ConfigurationException.
					generalIOException(ex, PATH_MAPPING_CONFIGURATION_FILE);
			throw exception;
		}
		
		return metadataMappingMap;
	}
	
	@Override
	public void configureServer(final ServerConfigurationData serverConfigurationData) {
		configureServerWithPropertyFile(serverConfigurationData);
		configureBaseOntology(serverConfigurationData);
		configureRuleOntology(serverConfigurationData);
	}
	
	/**
	 * Sets the new configuration data in the property file of the server.
	 * @param serverConfigurationData The configuration data that should be set.
	 * @throws ConfigurationException if the file is not writable or if there is any IOException.
	 */
	private void configureServerWithPropertyFile(final ServerConfigurationData serverConfigurationData) {
		final String configPath = getPathOfConfigurationFile();
		final File propertiesFile = new File(configPath);
		checkPropertyFileForWriting(propertiesFile);
		
		try (FileInputStream fileInputStream = new FileInputStream(propertiesFile);) {
			final Properties properties = new Properties();
			// Load the old properties.
			properties.load(fileInputStream);
			changePropertyValues(properties, serverConfigurationData);
			
			try (FileOutputStream fileOutputStream = new FileOutputStream(propertiesFile)) {
				properties.store(fileOutputStream, "");
			}
		}catch (IOException ex) {
			ConfigurationException exception = ConfigurationException.
					generalIOException(ex, PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	/**
	 * Returns the path of the configuration file. Is important for the tests. The path of the configuration file for testing
	 * purposes can be mocked with the help from this method.
	 * @return The path of the configuration file.
	 */
	@Override   
	public String getPathOfConfigurationFile() {
		return PATH_CONFIGURATION_FILE;
	}
	
	/**
	 * Sets the values of the property file.
	 * @param properties The properties that have to be changed. They have been received from the client.
	 * @param serverConfigurationData The configuration data. They are compared with the data stored in
	 * properties. If these data are new, they are saved in properties.
	 */
	private void changePropertyValues(final Properties properties, final ServerConfigurationData serverConfigurationData) {
		// Read the properties received from the client.
		final String workingDirectoryPath = serverConfigurationData.getWorkingDirectoryPath();
		final String baseOntology = serverConfigurationData.getConfiguredBaseOntologyName();
		final String ruleOntology = serverConfigurationData.getConfiguredRuleOntologyName();
		final String baseOntologyClass = serverConfigurationData.getBaseOntologyClass();
		
		changeWorkingDirectory(properties, workingDirectoryPath);
		changeBaseOntology(properties, baseOntology);
		changeRuleOntology(properties, ruleOntology);
		changeBaseOntologyClass(properties, baseOntologyClass);
	}
	
	/**
	 * Changes the path of the working directory in the property file.
	 * @param properties The properties with the configuration data.
	 * @param workingDirectoryPath The working directory path.
	 */
	private void changeWorkingDirectory(final Properties properties, final String workingDirectoryPath) {
		if (StringValidator.isNotNullOrEmpty(workingDirectoryPath)) {
			properties.setProperty(PROPERTY_WORKING_DIRECTORY, workingDirectoryPath);
			// use the new working directory as value for the attribute.
			workingDirectory = new WorkingDirectoryImpl(workingDirectoryPath);
		}
	}
	
	/**
	 * Changes the path of the base ontology in the property file
	 * @param properties The properties with the configuration data.
	 * @param baseOntologyPath The path of the base ontology.
	 */
	private void changeBaseOntology(final Properties properties, final String baseOntologyPath) {
		if (StringValidator.isNotNullOrEmpty(baseOntologyPath)) {
			properties.setProperty(PROPERTY_BASE_ONTOLOGY, baseOntologyPath);
		}
	}

	/**
	 * Changes the path of the rule ontology in the property file
	 * @param properties The properties with the configuration data.
	 * @param ruleOntologyPath The path of the rule ontology.
	 */
	private void changeRuleOntology(final Properties properties, final String ruleOntologyPath) {
		if (StringValidator.isNotNullOrEmpty(ruleOntologyPath)) {
			properties.setProperty(PROPERTY_RULE_ONTOLOGY, ruleOntologyPath);
		}
	}
	
	/**
	 * Changes the path of the base ontology class in the property file
	 * @param properties The properties with the configuration data.
	 * @param baseOntologyClass The name of the base ontology class.
	 */
	private void changeBaseOntologyClass(final Properties properties, final String baseOntologyClass) {
		if (StringValidator.isNotNullOrEmpty(baseOntologyClass)) {
			properties.setProperty(PROPERTY_BASE_ONTOLOGY_CLASS, baseOntologyClass);
		}
	}
	
	/**
	 * Configures the rule ontology and uploads the file on the server.
	 * @param serverConfigurationData Contains the path to the rule ontology.
	 * @throws ConfigurationException if the file cannot be written or if there is any IOException.
	 */
	private void configureRuleOntology(final ServerConfigurationData serverConfigurationData) {
		final byte[] fileContent = serverConfigurationData.getRuleOntologyContent();
		if (fileContent != null) {
			final String ontologyPath = serverConfigurationData.getRuleOntologyPath();
			PackagingToolkitFileUtils.writeByteArrayToFile(ontologyPath, fileContent);
		}
	}
	
	/**
	 * Configures the base ontology and uploads the file on the server.
	 * @param serverConfigurationData Contains the path to the base ontology.
	 * @throws ConfigurationException if the file cannot be written or if there is any IOException.
	 */
	private void configureBaseOntology(final ServerConfigurationData serverConfigurationData) {
		final byte[] fileContent = serverConfigurationData.getBaseOntologyContent();
		if (fileContent != null) {
			final String ontologyPath = serverConfigurationData.getBaseOntologyPath();
			PackagingToolkitFileUtils.writeByteArrayToFile(ontologyPath, fileContent);
		}
	}
	
	@Override
	public void deleteOldOntologyFiles(final String oldBaseOntology, final String newBaseOntology, 
			final  String oldRuleOntology, final String newRuleOntology, final String newSkosOntology, 
			final String oldSkosOntology, final String newPremisOntology, final String oldPremisOntology) {
		final File configDirectory = workingDirectory.getConfigurationDirectory();
		PackagingToolkitFileUtils.deleteOldFileIfChanged(oldRuleOntology, newRuleOntology, configDirectory);
		PackagingToolkitFileUtils.deleteOldFileIfChanged(oldBaseOntology, newBaseOntology, configDirectory);
		PackagingToolkitFileUtils.deleteOldFileIfChanged(oldSkosOntology, newSkosOntology, configDirectory);
		PackagingToolkitFileUtils.deleteOldFileIfChanged(oldPremisOntology, newPremisOntology, configDirectory);
	}
	
	@Override 
	public ServerConfigurationData readServerConfiguration() {
		final String configPath = getPathOfConfigurationFile();
		final File propertiesFile = new File(configPath);
		checkPropertyFileForReading(propertiesFile);
		final ServerConfigurationData serverConfigurationData = readServerProperties(propertiesFile); 
		return serverConfigurationData;
	}
	
	/**
	 * Reads the configuration of a configuration file on the server.
	 * @param propertiesFile The file that contains the configuration.
	 * @return An object with the read configuration data.
	 */
	private ServerConfigurationData readServerProperties(final File propertiesFile) {
		try (BufferedInputStream bufferedInputStream = new BufferedInputStream(
				new FileInputStream(propertiesFile))) {
			final Properties properties = new Properties();
			properties.load(bufferedInputStream);
			// Reading of the properties from the file.
			final String baseOntologieName = properties.getProperty(PROPERTY_BASE_ONTOLOGY);
			final String ruleOntologieName = properties.getProperty(PROPERTY_RULE_ONTOLOGY);
			final String workingDirectoryPath = properties.getProperty(PROPERTY_WORKING_DIRECTORY);
			final String baseOntologyClass = properties.getProperty(PROPERTY_BASE_ONTOLOGY_CLASS);
			final String premisOntologyName = properties.getProperty(PROPERTY_PREMIS_ONTOLOGY);
			final String skosOntologyName = properties.getProperty(PROPERTY_SKOS_ONTOLOGY);
			final String mainSkosClass = properties.getProperty(PROPERTY_MAIN_SKOS_CLASS);
			
			final WorkingDirectory workingDirectory = ServerModelFactory.
					createWorkingDirectory(workingDirectoryPath);
			
			// Add the values of the properties to the object for sending.
			final ServerConfigurationData serverConfigurationData = ConfigurationModelFactory.
					createServerConfigurationData(baseOntologieName, null, 
												  ruleOntologieName, null, 
												  workingDirectory);
			serverConfigurationData.setBaseOntologyClass(baseOntologyClass);
			serverConfigurationData.setConfiguredPremisOntologyName(premisOntologyName);
			serverConfigurationData.setConfiguredSkosOntologyName(skosOntologyName);
			serverConfigurationData.setMainSkosClass(mainSkosClass);
			
			return serverConfigurationData;
		} catch (IOException ex) {
			ConfigurationException exception = ConfigurationException.
					generalIOException(ex, PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	/**
	 * Checks if the properties file can be read.
	 * @param propertiesFile The property file that should be checked.
	 * @throws ConfigurationException if the file does not exist or it is not
	 * readable.
	 */
	private void checkPropertyFileForReading(final File propertiesFile) {
		if (!propertiesFile.exists()) {
			final ConfigurationException exception = ConfigurationException.
				doesNotExistException(PATH_CONFIGURATION_FILE);
			throw exception;
		}
		
		if (!propertiesFile.canRead()) {
			final ConfigurationException exception = ConfigurationException.
					notReadableException(PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	/**
	 * Checks if the properties file can be written.
	 * @param propertiesFile The property file that should be checked.
	 * @throws ConfigurationException if the file is not writable.
	 */
	private void checkPropertyFileForWriting(final File propertiesFile) {
		if (!propertiesFile.canWrite()) {
			final ConfigurationException exception = ConfigurationException.
					notWritableException(PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	/**
	 * Gets the base ontology as file object. 
	 * @return The base ontology. Does not contain any data of the file. 
	 */
	@Override
	public File getBaseOntology() {
		final File configurationDirectory = workingDirectory.getConfigurationDirectory();
		final ServerConfigurationData configurationData = readServerConfiguration();
		final String baseOntologyName = configurationData.getConfiguredBaseOntologyName();
		return new File(configurationDirectory, baseOntologyName);
	}

	@Override
	public String getPathOfMappingFile(WorkingDirectory workingDirectory) {
		final File configurationDirectory = workingDirectory.getConfigurationDirectory();
		final File mappingConfigurationFile = new File(configurationDirectory, PATH_MAPPING_CONFIGURATION_FILE);
		return mappingConfigurationFile.getAbsolutePath();
	}

	@Override
	public String readSkosMainClass() {
		final ServerConfigurationData serverConfigurationData = readServerConfiguration();
		return serverConfigurationData.getMainSkosClass();
	}
}