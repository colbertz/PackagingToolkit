package de.feu.kdmp4.packagingtoolkit.server.configuration.facades;

import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationService;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * Contains the methods the module Storage uses for accessing the methods of the module Configuration.
 * @author Christopher Olbertz
 *
 */
public class StorageConfigurationFacade {
	/**
	 * Contains the business logic.
	 */
	private ConfigurationService configurationService;
	
	/**
	 * Determines the working directory from the configuration file.
	 * @return The working directory.
	 */
	public WorkingDirectory getWorkingDirectory() {
		return configurationService.getWorkingDirectory();
	}
	
	/**
	 * Sets the object that contains the business logic.
	 * @param configurationOperations The object that contains the business logic.
	 */
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
