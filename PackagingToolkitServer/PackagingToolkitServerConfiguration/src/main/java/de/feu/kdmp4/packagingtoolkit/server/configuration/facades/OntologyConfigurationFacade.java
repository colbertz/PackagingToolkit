package de.feu.kdmp4.packagingtoolkit.server.configuration.facades;

import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;

/**
 * Contains the methods the module Ontology uses for accessing the methods of the module Configuration.
 * @author Christopher Olbertz
 *
 */
public class OntologyConfigurationFacade {
	/**
	 * Contains the operations layer.
	 */
	private ConfigurationOperations configurationOperations;
	
	/**
	 * Reads the configuration data of the server.
	 * @return The configuration data of the server.
	 */
	public ServerConfigurationData readServerConfiguration() {
		return configurationOperations.readServerConfiguration();
	}
	
	/**
	 * Reads the string that describes the main class for SKOS. This class determines the point where
	 * a taxonomy described by SKOS is starting. 
	 * @return The iri of the class as given in the configuration file.
	 */
	public String readSkosMainClass() {
		return configurationOperations.readSkosMainClass();
	}
	
	/**
	 * Sets the object that contains the method of the operations layer.
	 * @param configurationOperations The object that contains the method of the operations layer.
	 */
	public void setConfigurationOperations(ConfigurationOperations configurationOperations) {
		this.configurationOperations = configurationOperations;
	}
}
