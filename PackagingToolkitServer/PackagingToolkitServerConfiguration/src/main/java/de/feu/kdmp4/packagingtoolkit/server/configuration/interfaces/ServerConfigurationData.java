package de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * Encapsulates the configuration data of the server.
 * @author Christopher Olbetz
 *
 */
public interface ServerConfigurationData {
	/**
	 * Returns the path of the working directory on the server. The working directory contains subdirectories the server is working with like
	 * the temporary and the configuration directory.
	 * @return  The path of the working directory on the server.
	 */
	String getWorkingDirectoryPath();
	/**
	 * Returns the working directory as object with all information. The working directory contains subdirectories the server is
	 *  working with like the temporary and the configuration directory.
	 * @return The working directory.
	 */
	WorkingDirectory getWorkingDirectory();
	/**
	 * The name of the rule ontology file as its configured in the configuration file.
	 * @return The name of the rule ontology file.
	 */
	String getConfiguredRuleOntologyName();
	/**
	 * The name of the base ontology file as its configured in the configuration file.
	 * @return The name of the base ontology file.
	 */
	String getConfiguredBaseOntologyName();
	/**
	 * The path of the temporary directory that is contained in the working directory. 
	 * @return
	 */
	String getTemporaryDirectory();
	/**
	 * Sets a working directory object. The working directory contains subdirectories the server is working with like
	 * the temporary and the configuration directory.
	 * @param workingDirectory The working directory.
	 */
	void setWorkingDirectory(WorkingDirectory workingDirectory);
	/**
	 * Set the name of the rule ontology like it is configured on the server. The rule ontology is contained
	 * in the configuration subdirectory of the working directory.
	 * @param configuredRuleOntologyPath The name of the rule ontology.
	 */
	void setConfiguredRuleOntologyName(String configuredRuleOntologyPath);
	/**
	 * Set the name of the base ontology like it is configured on the server. The base ontology is contained
	 * in the configuration subdirectory of the working directory.
	 * @param configuredRuleOntologyPath The name of the base ontology.
	 */
	void setConfiguredBaseOntologyName(String configuredBaseOntologyPath);
	/**
	 * Set the name of the Premis ontology like it is configured on the server. The Premis ontology is contained
	 * in the configuration subdirectory of the working directory.
	 * @param configuredRuleOntologyPath The name of the Premis ontology.
	 */
	void setConfiguredPremisOntologyName(String configuredPremisOntologyPath);
	/**
	 * Set the name of the SKOS ontology like it is configured on the server. The SKOS ontology is contained
	 * in the configuration subdirectory of the working directory.
	 * @param configuredRuleOntologyPath The name of the SKOS ontology.
	 */
	void setConfiguredSkosOntologyName(String configuredSkosOntologyPath);
	/**
	 * Returns the name of the base ontology class. This is the class of the base ontology the extraction of the 
	 * classes starts with. In the most cases it is the class Information_Package.
	 * @return The name of the base ontology class
	 */
	String getBaseOntologyClass();
	/**
	 * Sets the name of the base ontology class. This is the class of the base ontology the extraction of the 
	 * classes starts with. In the most cases it is the class Information_Package.
	 * @param baseOntologyClass The name of the base ontology class
	 */
	void setBaseOntologyClass(String baseOntologyClass);
	/**
	 * Return the base ontology as File object.
	 * @return The base ontology as File object.
	 */
	File getBaseOntologyFile();
	/**
	 * Return the Premis ontology as File object.
	 * @return The Premis  ontology as File object.
	 */
	File getPremisOntologyFile();
	/**
	 * Return the SKOS ontology as File object.
	 * @return The SKOS ontology as File object.
	 */
	File getSkosOntologyFile();
	/**
	 * The name of the SKOS ontology file as its configured in the configuration file.
	 * @return The name of the SKOS ontology file.
	 */
	String getConfiguredSkosOntologyName();
	/**
	 * Gets the content of the rule ontology file and saves it as file in the configuation directory.
	 * @param fileContent The content of the rule ontology file. 
	 */
	void setRuleOntologyContent(byte[] fileContent);
	/**
	 * Gets the content of the Premis ontology file that is saved in the configuration subdirectory of the
	 * working directory of the server.
	 * @return The content of the Premis ontology file 
	 */
	byte[] getPremisOntologyContent();
	/**
	 * Gets the content of the Premis ontology file and saves it as file in the configuation directory.
	 * @param fileContent The content of the Premis ontology file. 
	 */
	void setPremisOntologyContent(byte[] fileContent);
	/**
	 * Returns the path of the Premis ontology in the configuration directory.
	 * @return The path of the Premis ontology.
	 */
	String getPremisOntologyPath();
	/**
	 * The name of the Premis ontology file as its configured in the configuration file.
	 * @return The name of the Premis ontology file.
	 */
	String getConfiguredPremisOntologyName();
	/**
	 * Gets the content of the rule ontology file that is saved in the configuration subdirectory of the
	 * working directory of the server.
	 * @return The content of the rule ontology file 
	 */
	byte[] getRuleOntologyContent();
	/**
	 * Gets the content of the base ontology file that is saved in the configuration subdirectory of the
	 * working directory of the server.
	 * @return The content of the base ontology file 
	 */
	byte[] getBaseOntologyContent();
	/**
	 * Gets the content of the SKOS ontology file and saves it as file in the configuation directory.
	 * @param fileContent The content of the SKOS ontology file. 
	 */
	void setSkosOntologyContent(byte[] skosOntologyContent);
	/**
	 * Gets the content of the SKOS ontology file that is saved in the configuration subdirectory of the
	 * working directory of the server.
	 * @return The content of the SKOS ontology file 
	 */
	byte[] getSkosOntologyContent();
	/**
	 * Gets the content of the base ontology file and saves it as file in the configuation directory.
	 * @param fileContent The content of the base ontology file. 
	 */
	void setBaseOntologyContent(byte[] baseOntologyContent);
	/**
	 * Returns the path of the SKOS ontology in the configuration directory.
	 * @return The path of the SKOS ontology.
	 */
	String getSkosOntologyPath();
	/**
	 * Returns the path of the base ontology in the configuration directory.
	 * @return The path of the base ontology.
	 */
	String getBaseOntologyPath();
	/**
	 * Returns the path of the rule ontology in the configuration directory.
	 * @return The path of the rule ontology.
	 */
	String getRuleOntologyPath();
	/**
	 * Sets the iri of the class that is the starting point for the SKOS otology.
	 * @param mainSkosClass The iri of the class that is the starting point for the SKOS otology.
	 */
	void setMainSkosClass(String mainSkosClass);
	/**
	 * Returns the iri of the class that is the starting point for the SKOS otology.
	 * @return The iri of the class that is the starting point for the SKOS otology.
	 */
	String getMainSkosClass();
}