
/**
 * Contains the facade classes this module provides for accessing the logic of this module.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.configuration.facades;