package de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;


/**
 * Interface for the operations methods for the configuration of the server.
 * @author Christopher Olbertz
 *
 */
public interface ConfigurationOperations {
	/**
	 * Reads the configuration data of the server.
	 * @return The configuration data of the server.
	 */
	ServerConfigurationData readServerConfiguration();
	/**
	 * Configures the server.
	 * @param serverConfigurationData Contain the configuration data that should be written into the configuration
	 * file.
	 */
	void configureServer(final ServerConfigurationData serverConfigurationData);
	/**
	 * Checks if new ontology files have been uploaded and deletes the old one from
	 * the configuration directory. Compares the old and the new names and deletes
	 * the old files if the names are not equal.
	 * @param oldBaseOntology The name of the old base ontology.
	 * @param newBaseOntology The name the new base ontology.
	 * @param oldRuleOntology The name of the old rule ontology.
	 * @param newRuleOntology The name of the new base ontology.
	 * @param newSkosOntology The name of the new SKOS ontology.
	 * @param oldSkosOntology The name of the old SKOS ontology.
	 * @param newPremisOntology The name of the new Premis ontology.
	 * @param oldPremisOntology The name of the old Premis ontology.
	 */
	void deleteOldOntologyFiles(final String oldBaseOntology, final String newBaseOntology, 
			final  String oldRuleOntology, final String newRuleOntology, final String newSkosOntology, 
			final String oldSkosOntology, final String newPremisOntology, final String oldPremisOntology);
	/**
	 * Gets the base ontology as a file object. The base ontology is in the
	 * configuration directory of the server that lies in the work directory.
	 * @return The base ontology.
	 */
	File getBaseOntology();
	/**
	 * Determines the path of the configuration file of the server.
	 * @return The path of the configuration file of the server.
	 */
	String getPathOfConfigurationFile();
	/**
	 * Reads the mapping of meta data elements to properties from the ontology from the 
	 * configuration file. 
	 * @param workingDirectory The working directory of the server. The mapping configuration file is 
	 * expected in the configuration directory of the server.
	 * @return A map that contains the mapping configuration.
	 */
	MetadataMappingMap readMetadataMappingConfiguration(WorkingDirectory workingDirectory);
	/**
	 * Determines the path of the mapping file of the server.
	 * @param workingDirectory The working directory of the server. The mapping configuration file is 
	 * expected in the configuration directory of the server.
	 * @return The path of the mapping file of the server.
	 */
	String getPathOfMappingFile(WorkingDirectory workingDirectory);
	/**
	 * Reads the string that describes the main class for SKOS. This class determines the point where
	 * a taxonomy described by SKOS is starting. 
	 * @return The iri of the class as given in the configuration file.
	 */
	String readSkosMainClass();
}
