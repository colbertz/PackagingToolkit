package de.feu.kdmp4.packagingtoolkit.server.configuration.classes;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationService;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Contains the method for communicating with the client for configuring purposes. 
 * @author Christopher Olbertz
 *
 */
@RestController
@RequestMapping(PathConstants.SERVER_CONFIGURATION_INTERFACE + "/**")
public class ConfigurationCommunication {
	/**
	 * The object that contains the business logic of this module.
	 */
	@Autowired
	private ConfigurationService configurationService;
	
	/**
	 * This method is called from the client to read the current configuration data
	 * of the server.
	 * @return The configuration data read from the configuration file. 
	 */
	@RequestMapping(value = PathConstants.SERVER_GET_CONFIGURATION_DATA, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<ServerConfigurationDataResponse> readConfigurationData() { 
		final ServerConfigurationDataResponse configurationData = configurationService.readServerConfiguration();
		return new ResponseEntity<ServerConfigurationDataResponse>(configurationData, HttpStatus.OK);
	}
	
	/**
	 * Is used for testing if the server is available. 
	 * @return A string if the server can send an answer.
	 */
	@RequestMapping(value = PathConstants.SERVER_PING, method = RequestMethod.GET)
	public ResponseEntity<String> receivePing() { 
		return new ResponseEntity<String>(PackagingToolkitConstants.I_AM_HERE, HttpStatus.OK);
	}
	
	/**
	 * Is called by the client to configure the working directory of the server. 
	 * @param workingDirectory The new working directory.
	 */
	@RequestMapping(value = PathConstants.SERVER_CONFIGURE_SERVER, method = RequestMethod.POST, 
		consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> configureServer(@RequestBody final ServerConfigurationDataResponse serverConfigurationData) {
		configurationService.configureServer(serverConfigurationData);
		return ResponseEntity.status(HttpStatus.OK).build(); 
	}
	
	/**
	 * Is called if the user wants to upload the base ontology.
	 * @param fileInputStream The content of the file.
	 * @param fileMetaData The meta data of the file. Contains e.g. the file name.
	 * @return A response with code 200 if the process was successful. If it was not 
	 * successful the response contains a ConfigurationException.
	 */
	@PostMapping(PathConstants.SERVER_UPLOAD_BASE_ONTOLOGY)
	public ResponseEntity<String> uploadBaseOntology(@RequestParam(ParamConstants.PARAM_FILE) MultipartFile file) {
		try {
			byte[] fileData = file.getBytes();
			configurationService.configureBaseOntology(file.getOriginalFilename(), fileData);
			return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.OK);
		} catch (IOException e) {
			return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Is called if the user wants to upload the rule ontology.
	 * @param fileInputStream The content of the file.
	 * @param fileMetaData The meta data of the file. Contains e.g. the file name.
	 * @return A response with code 200 if the process was successful. If it was not 
	 * successful the response contains a ConfigurationException.
	 */
	@PostMapping(PathConstants.SERVER_UPLOAD_RULE_ONTOLOGY)
	public ResponseEntity<String> uploadRuleOntology(@RequestParam(ParamConstants.PARAM_FILE) MultipartFile file) {
		try {
			byte[] fileData = file.getBytes();
			configurationService.configureRuleOntology(file.getOriginalFilename(), fileData);
			return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.OK);
		} catch (IOException e) {
			return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
