package de.feu.kdmp4.packagingtoolkit.server.configuration.facades;

import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;

/**
 * Contains the methods the module Serialization uses for accessing the methods of the module Configuration.
 * @author Christopher Olbertz
 *
 */
public class SerializationConfigurationFacade {
	/**
	 * Contains the operations layer.
	 */
	private ConfigurationOperations configurationOperations;
	
	/**
	 * Reads the configuration data of the server.
	 * @return The configuration data of the server.
	 */
	public ServerConfigurationData readServerConfiguration() {
		return configurationOperations.readServerConfiguration();
	}
	
	/**
	 * Sets the object that contains the method of the operations layer.
	 * @param configurationOperations The object that contains the method of the operations layer.
	 */
	public void setConfigurationOperations(ConfigurationOperations configurationOperations) {
		this.configurationOperations = configurationOperations;
	}
}
