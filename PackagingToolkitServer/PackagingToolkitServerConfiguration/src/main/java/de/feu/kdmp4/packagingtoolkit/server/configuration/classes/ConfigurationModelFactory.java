package de.feu.kdmp4.packagingtoolkit.server.configuration.classes;

import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * This factory creates the model objects for the configuration module. It encapsulates the constructors of 
 * {@ de.feu.kdmp4.packagingtoolkit.server.configuration.classes.ServerConfigurationDataImpl}. So the 
 * application is independant from the implementation because it only works with the interface 
 * {@ de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData}.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationModelFactory {
	/**
	 * Creates an object of {@ de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData}
	 *  with the given configuration data. 
	 * @param configuredBaseOntologyPath The path of the base ontology on the server.
	 * @param baseOntologyInputStream Contains the data of the base ontology that is uploaded to the server.
	 * @param configuredRuleOntologyPath The path of the rule ontology on the server.
	 * @param ruleOntologyInputStream Contains the data of the rule ontology that is uploaded to the server.
	 * @param workingDirectory The path of the working directory on the server.
	 * @return An object that contains the given data.
	 */
	public static final ServerConfigurationData createServerConfigurationData(
			final String configuredBaseOntologyPath, final byte[] baseOntologyContent, 
			final String configuredRuleOntologyPath, final byte[] ruleOntologyContent, 
			final WorkingDirectory workingDirectory) {
		return new ServerConfigurationDataImpl(
				configuredBaseOntologyPath, baseOntologyContent,
				configuredRuleOntologyPath, ruleOntologyContent,
				workingDirectory);
	}
	
	/**
	 * Creates an empty object of {@ de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData}.
	 * @return An empty object.
	 */
	public static final ServerConfigurationData createEmptyServerConfigurationData() {
		return new ServerConfigurationDataImpl();
	}
}
