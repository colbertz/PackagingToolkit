package de.feu.kdmp4.packagingtoolkit.server.views.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage.ViewException;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.ViewDatabaseFacade;

/**
 * The class for the communication related to the views on an ontology. 
 * @author Christopher Olbertz
 *
 */
@RestController
@RequestMapping(PathConstants.SERVER_VIEWS_INTERFACE)
public class ViewsCommunication {
	/**
	 * The interface for communicating with the database module.
	 */
	@Autowired
	private ViewDatabaseFacade viewDatabaseFacade;
	
	/**
	 * This method is called from the client to read the current configuration data
	 * of the server.
	 * @return The configuration data read from the configuration file. 
	 */
	@RequestMapping(value = PathConstants.SERVER_GET_ALL_VIEWS, method = RequestMethod.GET,
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<ViewListResponse> getAllViews() { 
		final ViewListResponse viewList = viewDatabaseFacade.getAllViews();
		return new ResponseEntity<ViewListResponse>(viewList, HttpStatus.OK);
	}
	
	/**
	 * Saves a new view in the database. 
	 * @param view The view that should be saved.
	 * @return A OK message if the view could be saved.
	 */
	@RequestMapping(value = PathConstants.SERVER_SAVE_NEW_VIEW, method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> saveNewView(@RequestBody ViewResponse view) {
		try {
			viewDatabaseFacade.saveNewView(view);
		} catch (ViewException viewException) {
			return new ResponseEntity<String>(viewException.getMessage(), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	/**
	 * Is called if the user wants to delete a view on the client. The client sends the id of the view to 
	 * the server.
	 * @param viewId The id of the view to delete.
	 */
	@RequestMapping(value = PathConstants.SERVER_DELETE_VIEW, method = RequestMethod.DELETE)
	public void deleteView(@PathVariable(ParamConstants.PARAM_VIEW_ID) int viewId) {
		viewDatabaseFacade.deleteView(viewId);
	}
}
