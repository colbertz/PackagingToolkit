package de.feu.kdmp4.packagingtoolkit.server.session.classes;

import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.junit.MockitoRule;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.SessionService;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;

public class SessionServiceTest {
	// ***************** Constants *****************
	/**
	 * The size of the queue.
	 */
	private static final int SIZE = 3;
	/**
	 * The name of the field informationPackageQueue of the object that is tested.
	 * Is used for access via reflections.
	 */
	private static final String FIELD_INFORMATION_PACKAGE_QUEUE = "informationPackageQueue";
	/**
	 * The name of the field queueTimer of the object that is tested.
	 * Is used for access via reflections.
	 */
	private static final String FIELD_QUEUE_TIMER = "queueTimer";
	/**
	 * The name of the field queueThread of the object that is tested.
	 * Is used for access via reflections.
	 */
	private static final String FIELD_QUEUE_THREAD = "queueThread"; 
	
	// ***************** Attributes ****************
	private BasicInformationPackage firstInformationPackage;
	private BasicInformationPackage fourthInformationPackage;
	private BasicInformationPackage secondInformationPackage;
	private BasicInformationPackage thirdInformationPackage;
	
	@Rule
	public TemporaryFolder workingDirectoryRule = new TemporaryFolder();
	private SessionService sessionService;
	@Rule
	public MockitoRule mockitoRule;
	
	// *************** Setup and tear down *********
	/**
	 * The information packages are created and three information packages are put
	 * into the session.
	 */
	@Before
	public void setup() {
	}
	
	// *****************************************************************************************************
	// *********** testCreateNewSubmissionInformationPackage()
	// *****************************************************************************************************
	@Test
	public void testCreateNewSubmissionInformationPackage() {
		sessionService = new SessionServiceImpl();
		sessionService.initialize();
		String expectedTitle = "the title of the information package";
		PackageResponse actualPackageResponse = sessionService.createNewSubmissionInformationPackage(expectedTitle);
		
		assertThat(actualPackageResponse, is(notNullValue()));
		String actualUuid = actualPackageResponse.getUuid();
		assertThat(actualUuid, not(isEmptyOrNullString()));
		String actualTitle = actualPackageResponse.getTitle();
		assertThat(actualTitle, is(equalTo(expectedTitle)));
		String actualPackageType = actualPackageResponse.getPackageType();
		assertThat(actualPackageType, is(equalTo(PackageType.SIP.toString())));
	}
	
	// *****************************************************************************************************
	// *********** testCreateNewSubmissionInformationUnit()
	// *****************************************************************************************************
	@Test
	public void testCreateNewSubmissionInformationUnit() {
		String expectedTitle = "the title of the information package";
		sessionService = new SessionServiceImpl();
		sessionService.initialize();
		PackageResponse actualPackageResponse = sessionService.createNewSubmissionInformationUnit(expectedTitle);
		
		assertThat(actualPackageResponse, is(notNullValue()));
		String actualUuid = actualPackageResponse.getUuid();
		assertThat(actualUuid, not(isEmptyOrNullString()));
		String actualTitle = actualPackageResponse.getTitle();
		assertThat(actualTitle, is(equalTo(expectedTitle)));
		String actualPackageType = actualPackageResponse.getPackageType();
		assertThat(actualPackageType, is(equalTo(PackageType.SIU.toString())));
	}
	
	// *****************************************************************************************************
	// *********** testInformationPackageExists_ResultTrue
	// *****************************************************************************************************
	/**
	 * Checks the method for evaluating if an information package with a given uuid exists in the session. The information
	 * package has to exist for this test.
	 * @throws IOException
	 */
	@Test
	public void testInformationPackageExists_ResultTrue() throws IOException {
		Uuid testUuid = prepareTest_testInformationPackageExists();
		
		boolean informationPackageExists = sessionService.informationPackageExists(testUuid);
		assertTrue(informationPackageExists);
	}
	
	/**
	 * Prepares the session with three information packages and returns the uuid of an information package
	 * that exists in the session.
	 * @return The uuid of an information package that exists in the session.
	 */
	private Uuid prepareTest_testInformationPackageExists() {
		sessionService = new SessionServiceImpl();
		sessionService.initialize();
		sessionService.createNewSubmissionInformationPackage("a");
		PackageResponse packageResponse = sessionService.createNewSubmissionInformationPackage("b");
		sessionService.createNewSubmissionInformationUnit("c");
		Uuid uuid = new Uuid(packageResponse.getUuid());
		return uuid;
	}
	
	// *****************************************************************************************************
	// *********** testInformationPackageExists_ResultFalse
	// *****************************************************************************************************
	/**
	 * Checks the method for evaluating if an information package with a given uuid exists in the session. The information
	 * package does not exist for this test.
	 * @throws IOException
	 */
	/*
	 * TODO Der Test schlaegt fehl, weil eine Exception geworfen wird. 
	 * Frage: Was ist besser: Die SessionException beibehalten oder einfach ueberall im Falle eines nicht existierenden
	 * Informationspakets false zurueckgeben?
	 * Brauche ich die Methode informationPackageExists() ueberhaupt noch?
	 */
	@Test
	public void testInformationPackageExists_ResultFalse() throws IOException {
		Uuid testUuid = prepareTest_testInformationPackageNotExists();
		
		boolean informationPackageExists = sessionService.informationPackageExists(testUuid);
		assertFalse(informationPackageExists);
	}
	
	/**
	 * Prepares the session with three information packages and returns the uuid of an information package
	 * that does not exist in the session.
	 * @return The uuid of an information package that does not exist in the session.
	 */
	private Uuid prepareTest_testInformationPackageNotExists() {
		sessionService = new SessionServiceImpl();
		sessionService.initialize();
		sessionService.createNewSubmissionInformationPackage("a");
		sessionService.createNewSubmissionInformationPackage("b");
		sessionService.createNewSubmissionInformationUnit("c");
		Uuid uuid = new Uuid();
		return uuid;
	}
	
	// *****************************************************************************************************
	// *********** testAddReferenceToInformationPackage
	// *****************************************************************************************************
	/**
	 * Tests the method addReferenceToInformationPackage().
	 */
	@Test
	public void testAddReferenceToInformationPackage() {
		String expectedFileName = "myFile.txt";
		Uuid uuidOfInformationPackage = prepareTest_testAddReferenceToInformationPackage();
		Uuid uuidOfFile = new Uuid();
		sessionService.addLocalFileReferenceToInformationPackage(expectedFileName, uuidOfInformationPackage, uuidOfFile);
		
		ReferenceCollection referenceList = sessionService.getAllReferencesOfInformationPackage(uuidOfInformationPackage);
		int actualReferenceCount = referenceList.getReferencesCount();
		assertThat(actualReferenceCount, is(equalTo(1)));
		
		
	}
	
	/**
	 * Prepares the session with three information packages and returns the uuid of an information package
	 * that exists in the session.
	 * @return The uuid of an information package that exists in the session.
	 */
	private Uuid prepareTest_testAddReferenceToInformationPackage() {
		sessionService = new SessionServiceImpl();
		sessionService.initialize();
		sessionService.createNewSubmissionInformationPackage("a");
		PackageResponse packageResponse = sessionService.createNewSubmissionInformationPackage("b");
		sessionService.createNewSubmissionInformationPackage("c");
		Uuid uuid = new Uuid(packageResponse.getUuid());
		return uuid;
	}
	
	// *****************************************************************************************************
	// *********** testRemoveInformationPackageFromSession()
	// *****************************************************************************************************
	/**
	 * Tests the method removeInformationPackageFromSession. As preparation for the test some information packages are
	 * created and stored in the session. One of these information packages is removed. 
	 */
	@Test
	public void testRemoveInformationPackageFromSession() {
		Uuid uuidOfInformationPackage = prepareTest_testRemoveInformationPackageFromSession();
		sessionService.removeInformationPackageFromSession(uuidOfInformationPackage);
		
		boolean informationPackageExists = sessionService.informationPackageExists(uuidOfInformationPackage);
		assertFalse(informationPackageExists);
	}
	
	/**
	 * Prepares the session with three information packages and returns the uuid of an information package
	 * that exists in the session.
	 * @return The uuid of an information package that exists in the session. It is deleted during the test.
	 */
	private Uuid prepareTest_testRemoveInformationPackageFromSession() {
		sessionService = new SessionServiceImpl();
		sessionService.initialize();
		sessionService.createNewSubmissionInformationPackage("a");
		PackageResponse packageResponse = sessionService.createNewSubmissionInformationPackage("b");
		sessionService.createNewSubmissionInformationPackage("c");
		Uuid uuid = new Uuid(packageResponse.getUuid());
		return uuid;
	}
}
