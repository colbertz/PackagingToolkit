package de.feu.kdmp4.packagingtoolkit.server.session.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.AbstractQueueTimer;

public class QueueTimerTest {
	
	@Before
	public void setup() throws InterruptedException {

	}
	
	// ********************************************
	// ************ testIsEmpty_resultTrue() 
	// ********************************************
	@Test
	public void testIsEmpty_resultTrue() {
		AbstractQueueTimer emptyQueueTimer = new QueueTimerImpl();
		boolean empty = emptyQueueTimer.isEmpty();
		assertTrue(empty);
	}

	// ********************************************
	// ************ testIsEmpty_resultFalse() 
	// ********************************************
	@Test
	public void testIsEmpty_resultFalse() throws InterruptedException {
		AbstractQueueTimer queueTimer = prepareTest_testIsNotEmpty_resultFalse_queueTimer();
		boolean empty = queueTimer.isEmpty();
		assertFalse(empty);
	}
	
	/**
	 * Prepares a queue timer that is not empty.
	 * @return The queue timer for the test.
	 * @throws InterruptedException
	 */
	private AbstractQueueTimer prepareTest_testIsNotEmpty_resultFalse_queueTimer() throws InterruptedException  {
		AbstractQueueTimer queueTimer = new QueueTimerImpl(1);
		 
		Uuid firstUuid = new Uuid();
		Uuid secondUuid = new Uuid();
		
		Date firstDate = new Date();
		queueTimer.put(firstUuid, firstDate);
		Date secondDate = new Date();
		queueTimer.put(secondUuid, secondDate);
		
		return queueTimer;
	}
	
	// ********************************************
	// ************ testIsNotEmpty_resultTrue() 
	// ********************************************
	/**
	 * Tests the method isNotEmpty() with a not empty queue.
	 * @throws InterruptedException
	 */
	@Test
	public void testIsNotEmpty_resultTrue() throws InterruptedException {
		AbstractQueueTimer queueTimer = prepareTest_testIsNotEmpty_queueTimer();
		boolean empty = queueTimer.isNotEmpty();
		assertTrue(empty);
	}
	
	/**
	 * Prepares a queue timer that is not empty.
	 * @return The queue timer for the test.
	 * @throws InterruptedException
	 */
	private AbstractQueueTimer prepareTest_testIsNotEmpty_queueTimer() throws InterruptedException  {
		AbstractQueueTimer queueTimer = new QueueTimerImpl(1);
		 
		Uuid firstUuid = new Uuid();
		Uuid secondUuid = new Uuid();
		
		Date firstDate = new Date();
		queueTimer.put(firstUuid, firstDate);
		Date secondDate = new Date();
		queueTimer.put(secondUuid, secondDate);
		
		return queueTimer;
	}
	
	// ********************************************
	// ************ testIsNotEmpty_resultFalse() 
	// ********************************************
	@Test
	public void testIsNotEmpty_resultFalse() {
		AbstractQueueTimer emptyQueueTimer = new QueueTimerImpl();
		boolean notEmpty = emptyQueueTimer.isNotEmpty();
		assertFalse(notEmpty);
	}
	
	// ********************************************
	// ************ testRemove_elementRemoved()
	// ********************************************
	/**
	 * Tests the method remove(). In this test, there is one element removed because of the time out
	 * mechanism.
	 * @throws InterruptedException
	 */
	@Test
	public void testRemove_elementRemoved() throws InterruptedException {
		Uuid uuidForTest = new Uuid();
		AbstractQueueTimer queueTimer = prepareTest_testRemove_elementRemoved_queueTimer(uuidForTest);
		queueTimer.removeIfNecessary();
		
		boolean notEmpty = queueTimer.isNotEmpty();
		assertTrue(notEmpty);
		
		int size = queueTimer.getSize();
		assertThat(size, is(equalTo(2)));
	}
	
	/**
	 * Prepares a queue timer that is not empty.
	 * @return The queue timer for the test.
	 * @throws InterruptedException
	 */
	private AbstractQueueTimer prepareTest_testRemove_elementRemoved_queueTimer(Uuid secondUuid) throws InterruptedException  {
		AbstractQueueTimer queueTimer = new QueueTimerImpl(1);
		 
		Uuid firstUuid = new Uuid();
		Uuid thirdUuid = new Uuid();
		
		Date firstDate = new Date();
		queueTimer.put(firstUuid, firstDate);
		Thread.sleep(2000);
		Date secondDate = new Date();
		queueTimer.put(secondUuid, secondDate);
		Thread.sleep(3000);
		Date thirdDate = new Date();
		queueTimer.put(thirdUuid, thirdDate);
		Thread.sleep(2000);
		
		return queueTimer;
	}
	
	// ********************************************
	// ************ testRemove_elementNotRemoved()
	// ********************************************
	/**
	 * Tests the method remove(). In this test, there is no element removed because the time limit is 
	 * too low.
	 * @throws InterruptedException
	 */
	@Test
	public void testRemove_elementNotRemoved() throws InterruptedException {
		Uuid uuidForTest = new Uuid();
		AbstractQueueTimer queueTimer = prepareTest_testRemove_elementNotRemoved_queueTimer(uuidForTest);
		queueTimer.removeIfNecessary();
		
		boolean notEmpty = queueTimer.isNotEmpty();
		assertTrue(notEmpty);
		
		int size = queueTimer.getSize();
		assertThat(size, is(equalTo(3)));
	}
	
	/**
	 * Prepares a queue timer that is not empty. The times are low enough that no information package has to be 
	 * temoved during the test.
	 * @return The queue timer for the test.
	 * @throws InterruptedException
	 */
	private AbstractQueueTimer prepareTest_testRemove_elementNotRemoved_queueTimer(Uuid secondUuid) throws InterruptedException  {
		AbstractQueueTimer queueTimer = new QueueTimerImpl(5);
		 
		Uuid firstUuid = new Uuid();
		Uuid thirdUuid = new Uuid();
		
		Date firstDate = new Date();
		queueTimer.put(firstUuid, firstDate);
		Thread.sleep(1000);
		Date secondDate = new Date();
		queueTimer.put(secondUuid, secondDate);
		Thread.sleep(1000);
		Date thirdDate = new Date();
		queueTimer.put(thirdUuid, thirdDate);
		Thread.sleep(1000);
		
		return queueTimer;
	}
	
	// ********************************************
	// ************ testRemoveEmptyQueue()
	// ********************************************
	public void testRemoveEmptyQueue() {
		AbstractQueueTimer emptyQueueTimer = new QueueTimerImpl();
		Uuid uuid = emptyQueueTimer.removeIfNecessary();
		assertNull(uuid);
	}
	
	// ********************************************
	// ************ testGetFirstKey()
	// ********************************************
	@Test
	public void testGetFirstKey() {
		Uuid firstUuid = new Uuid();
		Uuid secondUuid = new Uuid();
		AbstractQueueTimer queueTimer = prepareTest_testGetFirstKey_queueTimer(firstUuid, secondUuid);
		Uuid key = queueTimer.getFirstKey();
		
		if (firstUuid.compareTo(secondUuid) < 0) {
			assertThat(key, is(equalTo(firstUuid)));
		} else {
			assertThat(key, is(equalTo(secondUuid)));
		}
		
	}
	
	/**
	 * Prepares a queue timer that is not empty.
	 * @return The queue timer for the test.
	 * @throws InterruptedException
	 */
	private AbstractQueueTimer prepareTest_testGetFirstKey_queueTimer(Uuid firstUuid, Uuid secondUuid) {
		AbstractQueueTimer queueTimer = new QueueTimerImpl(1);
		
		Date firstDate = new Date();
		queueTimer.put(firstUuid, firstDate);
		Date secondDate = new Date();
		queueTimer.put(secondUuid, secondDate);
		
		return queueTimer;
	}
	
	// ********************************************
	// ************ testGetFirstValue()
	// ********************************************
	@Test
	public void testGetFirstValue() {
		Date dateForTest = new Date();
		AbstractQueueTimer queueTimer = prepareTest_testGetFirstValue_queueTimer(dateForTest);
		Date date = queueTimer.getFirstValue();
		assertThat(date, is(equalTo(dateForTest)));
	}
	
	/**
	 * Prepares a queue timer that is not empty.
	 * @return The queue timer for the test.
	 * @throws InterruptedException
	 */
	private AbstractQueueTimer prepareTest_testGetFirstValue_queueTimer(Date firstDate) {
		AbstractQueueTimer queueTimer = new QueueTimerImpl(1);
		 
		Uuid firstUuid = new Uuid();
		Uuid secondUuid = new Uuid();
		
		queueTimer.put(firstUuid, firstDate);
		Date secondDate = new Date();
		queueTimer.put(secondUuid, secondDate);
		
		return queueTimer;
	}
	
	// ********************************************
	// ************ testGetFirstKeyEmpty()
	// ********************************************
	@Test
	public void testGetFirstKeyEmpty() {
		AbstractQueueTimer emptyQueueTimer = new QueueTimerImpl();
		Uuid key = emptyQueueTimer.getFirstKey();
		assertNull(key);
	}
	
	// ********************************************
	// ************ testGetFirstValueEmpty()
	// ********************************************
	@Test
	public void testGetFirstValueEmpty() {
		AbstractQueueTimer emptyQueueTimer = new QueueTimerImpl();
		Date date = emptyQueueTimer.getFirstValue();
		assertNull(date);
	}
}
