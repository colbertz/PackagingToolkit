package de.feu.kdmp4.packagingtoolkit.server.session.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.InformationPackageQueue;
import de.feu.kdmp4.packagingtoolkit.server.session.model.SubmissionInformationPackageImpl;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

public class InformationPackageQueueTest {
	// ********** Attributes ************* 
	private BasicInformationPackage firstInformationPackage;
	private BasicInformationPackage fourthInformationPackage;
	private InformationPackageQueue informationPackageQueue;
	private BasicInformationPackage secondInformationPackage;
	private BasicInformationPackage thirdInformationPackage;

	// ********* Setup and tear down **********
	@Before
	public void setup() {
		informationPackageQueue = new InformationPackageQueueImpl();
	}
	
	/**
	 * Prepares some information packages for the tests. 
	 */
	private void prepareInformationPackagesForTests() {
		firstInformationPackage = new SubmissionInformationPackageImpl("abc", new Uuid(), DateTimeUtils.getNow(), DateTimeUtils.getNow());
		secondInformationPackage = new SubmissionInformationPackageImpl("def", new Uuid(), DateTimeUtils.getNow(), DateTimeUtils.getNow());
		thirdInformationPackage = new SubmissionInformationPackageImpl("ghi", new Uuid(), DateTimeUtils.getNow(), DateTimeUtils.getNow());
		fourthInformationPackage = new SubmissionInformationPackageImpl("jkl", new Uuid(), DateTimeUtils.getNow(), DateTimeUtils.getNow());
	}
	
	/**
	 * Puts three information packages in the information package queue.
	 */
	private void prepareInformationPackageQueueForTests() {
		prepareInformationPackagesForTests();
		informationPackageQueue.put(firstInformationPackage);
		informationPackageQueue.put(secondInformationPackage);
		informationPackageQueue.put(thirdInformationPackage);
	}
	
	// ********** Tests methods *************
	/**
	 * Tests the method getSize() with three information packages.
	 */
	@Test
	public void testGetSize_Result3() {
		prepareInformationPackagesForTests();
		prepareInformationPackageQueueForTests();
		
		int size = informationPackageQueue.getSize();
		assertThat(size, is(equalTo(3)));
	}
	
	/**
	 * Tests the method getSize() with zero information packages.
	 */
	@Test
	public void testGetSize_Result0() {
		int size = informationPackageQueue.getSize();
		assertThat(size, is(equalTo(0)));
	}
	
	/**
	 * Tests the method contains(). The information package that is used for the test is actually contained
	 * in the queue.
	 */
	@Test
	public void testContainsTrue() {
		prepareInformationPackagesForTests();
		prepareInformationPackageQueueForTests();
		
		Uuid uuidOfInformationPackage = secondInformationPackage.getUuid();
		boolean contains = informationPackageQueue.contains(uuidOfInformationPackage);
		assertThat(contains, is(true));
	}
	
	/**
	 * Tests the method contains(). The information package that is used for the test is actually not contained
	 * in the queue.
	 */
	@Test
	public void testContainsFalse() {
		prepareInformationPackageQueueForTests();
		boolean contains = informationPackageQueue.contains(fourthInformationPackage.getUuid());
		assertThat(contains, is(false));
	}
	
	@Test
	public void testGet() {
		prepareInformationPackageQueueForTests();
		BasicInformationPackage informationPackage = informationPackageQueue.
				get(secondInformationPackage.getUuid());
		
		assertThat(informationPackage, is(notNullValue()));
		assertThat(informationPackage.getTitle(), is(equalTo(secondInformationPackage.getTitle())));
	}
	
	@Test
	public void testPut() {
		prepareInformationPackageQueueForTests(); 
		informationPackageQueue.put(fourthInformationPackage);
		int size = informationPackageQueue.getSize();
		assertThat(size, is(equalTo(4)));
		
		BasicInformationPackage informationPackage = informationPackageQueue.get(fourthInformationPackage.getUuid());
		assertThat(informationPackage.getTitle(), is(equalTo(fourthInformationPackage.getTitle())));
	}
	
	@Test
	public void testRemove() {
		prepareInformationPackageQueueForTests(); 
		informationPackageQueue.remove(secondInformationPackage.getUuid());
		int size = informationPackageQueue.getSize();
		assertThat(size, is(equalTo(2)));
		
		BasicInformationPackage informationPackage = informationPackageQueue.get(firstInformationPackage.getUuid());
		assertThat(informationPackage, is(notNullValue()));
		assertThat(informationPackage.getTitle(), is(equalTo(firstInformationPackage.getTitle())));
		
		informationPackage = informationPackageQueue.get(thirdInformationPackage.getUuid());
		assertThat(informationPackage, is(notNullValue()));
		assertThat(informationPackage.getTitle(), is(equalTo(thirdInformationPackage.getTitle())));
	}
	
	@Test
	public void testHasCreationDate_resultTrue() {
		final BasicInformationPackage informationPackage = new SubmissionInformationPackageImpl("test", DateTimeUtils.getNow(), DateTimeUtils.getNow());
		final boolean hasCreationDate = informationPackage.hasCreationDate();
		assertTrue(hasCreationDate);
	}
	
	@Test
	public void testHasCreationDate_resultFalse() {
		final BasicInformationPackage informationPackage = new SubmissionInformationPackageImpl("test", null, DateTimeUtils.getNow());
		final boolean hasCreationDate = informationPackage.hasCreationDate();
		assertFalse(hasCreationDate);
	}
}
