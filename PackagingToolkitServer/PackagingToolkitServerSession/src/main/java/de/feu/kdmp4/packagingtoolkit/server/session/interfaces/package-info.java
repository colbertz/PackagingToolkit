/**
 * Contains the interfaces for the classes that have to do with the synchronization and
 * the session handling.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.session.interfaces;