package de.feu.kdmp4.packagingtoolkit.server.session.model.classes;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.UuidsOfFilesAndTheirStatements;

public class UuidsOfFilesAndTheirMetadataHashMapImpl implements UuidsOfFilesAndTheirStatements {
	/**
	 * A map that contains the data of this datastructure.
	 */
	private Map<Uuid, TripleStatementCollection> uuidsAndTheMetadataMap;
	
	/**
	 * Creates a new object and initializes the map.
	 */
	public UuidsOfFilesAndTheirMetadataHashMapImpl() {
		uuidsAndTheMetadataMap = new HashMap<>();
	}
	
	@Override
	public void addMetadata(final Uuid uuidOfFile, final TripleStatementCollection tripleStatements) {
		uuidsAndTheMetadataMap.put(uuidOfFile, tripleStatements);
	}
	
	@Override
	public TripleStatementCollection getStatementsOfFile(final Uuid uuidOfFile) {
		return uuidsAndTheMetadataMap.get(uuidOfFile);
	}

	@Override
	public Set<Entry<Uuid, TripleStatementCollection>> getUuidsAndProperties() {
		return Collections.unmodifiableSet(uuidsAndTheMetadataMap.entrySet());
	}
	
	@Override
	public Set<Uuid> getUuidsOfFiles() {
		return Collections.unmodifiableSet(uuidsAndTheMetadataMap.keySet());
	}
}
