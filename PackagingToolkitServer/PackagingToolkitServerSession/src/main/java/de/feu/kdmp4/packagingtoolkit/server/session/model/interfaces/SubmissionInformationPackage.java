package de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces;

/**
 * A submission information package. If it is not virtual it can contain only
 * one digital object.
 * @author Christopher Olbertz
 *
 */
public interface SubmissionInformationPackage extends BasicInformationPackage {
}
