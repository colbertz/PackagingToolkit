/**
 * Contains the classes that create object for the module Session.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.session.factories;