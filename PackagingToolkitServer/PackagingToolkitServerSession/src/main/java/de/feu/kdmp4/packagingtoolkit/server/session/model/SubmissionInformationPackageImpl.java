package de.feu.kdmp4.packagingtoolkit.server.session.model;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage.DigitalObjectException;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.SubmissionInformationPackage;

/**
 * The implementation of a submission information package. A SIP can only contain one digital object
 * at the same time.
 * @author Christopher Olbertz
 *
 */
public class SubmissionInformationPackageImpl extends BasicInformationPackageImpl 
										    implements SubmissionInformationPackage {
	/**
	 * A reference to the digital object that is contained in this SIP.
	 */
	private Reference reference;
	
	/**
	 * Creates a new SIP.
	 * @param title The title of the SIP.
	 * @param uuid The uuid that identifies the SIP.
	 */
	public SubmissionInformationPackageImpl(final String title, final Uuid uuid, final LocalDateTime creationDate, final LocalDateTime lastModificationDate) {
		super(title, uuid, creationDate, lastModificationDate);
	}
	
	public SubmissionInformationPackageImpl(final String title, final LocalDateTime creationDate, final LocalDateTime lastModificationDate) {
		super(title, creationDate, lastModificationDate);
	}
	
	/**
	 * Sets a reference in the submission information package. Because there is only one 
	 * digital object allowed in a SIP, an exception is thrown if there is already a 
	 * digital object in this SIP.
	 * @param reference The reference to a digital object to set.
	 * @throws DigitalObjectException Is thrown if the digital object is null or if
	 * there is already a digital object in the SIP.
	 */
	private void setReference(final Reference reference) {
		this.reference = reference;				
	}
	
	@Override
	public PackageType getPackageType() {
		return PackageType.SIP;
	}

	@Override
	public void addReference(final Reference reference) {
		super.addReference(reference);
		setReference(reference);
	}

	@Override
	public ReferenceCollection getReferences() {
		final List<Reference> referenceList = new ArrayList<>();
		referenceList.add(reference);
		final ReferenceCollection references = ServerModelFactory.createReferenceList(referenceList);				
		return references;
	}
	
	@Override
	public TripleStatementCollection toTripleStatements() {
		final TripleStatementCollection tripleStatements = super.toTripleStatements();
		final TripleStatement referenceAsStatement = reference.asTripleStatement(getUuid()); 
		tripleStatements.addTripleStatement(referenceAsStatement);
		return tripleStatements;
	}

	@Override
	public TripleStatementCollection toStatements() {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		if (containsReferences()) {
			if (reference.isLocalFileReference()) {
				final Uuid uuidOfFile = reference.getUuidOfFile();
				final TripleStatement tripleStatement = RdfElementFactory.createStorageFileStatement(getUuid(), uuidOfFile);
				tripleStatements.addTripleStatement(tripleStatement);
				tripleStatements.addStatements(getStatementsOfAllFiles());
			} else {
				final String urlOfFile = reference.getUrl();
				final TripleStatement tripleStatement = RdfElementFactory.createRemoteFileStatement(getUuid(), urlOfFile);
				tripleStatements.addTripleStatement(tripleStatement);
			}
		}
		
		return tripleStatements;
	}


	@Override
	public boolean containsReferences() {
		if (reference != null) {
			return true;
		} else {
			return false;
		}
	}
}
