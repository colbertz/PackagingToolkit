package de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.OaisEntity;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;

/**
 * Represents a digital object that is stored in the storage directory of the server.
 * @author Christopher Olbertz
 *
 */
public interface DigitalObject extends OaisEntity, Comparable<DigitalObject> {
	/**
	 * Gets the filename of the file in the storage directory.
	 * @return The filename of the file in the storage directory.
	 */
	public String getFilename();
	/**
	 * Gets the reference of the file. It is used by the information packages that
	 * point to this digital object.
	 * @return The reference of the file.
	 */
	Reference getReference();
}