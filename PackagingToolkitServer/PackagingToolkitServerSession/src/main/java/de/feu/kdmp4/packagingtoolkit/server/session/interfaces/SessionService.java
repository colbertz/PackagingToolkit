package de.feu.kdmp4.packagingtoolkit.server.session.interfaces;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.SessionDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ObjectPropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;
import de.feu.kdmp4.server.triplestore.facades.SessionTripleStoreFacade;

/**
 * The service for administrating the session handling.
 * @author Christopher Olbertz
 *
 */
public interface SessionService {
	/**
	 * Removes an information package from the session.
	 * @param uuid The uuid of the information package to remove.
	 * @return The removed information package.
	 */
	public BasicInformationPackage removeInformationPackageFromSession(final Uuid uuid);
	/**
	 * Updates an information package in the session. The information package itself
	 * is not modified but only its timestamp in the session.
	 * @param uuid The uuid of the information package.
	 */
	void updateQueue(final Uuid uuid);
	/**
	 * Changes the timeout. If this time is over, unused packages are removed from
	 * the session.
	 * @param waitingTimeInSeconds The time a packages has to be unused before it is
	 * deleted from the session.
	 */
	void changeTimeout(final int waitingTimeInSeconds);
	/**
	 * Mainly for testing purposes. The time a package has to be unused before it is
	 * deleted from the session can be changed.
	 * @param waitingTimeInSeconds The time a package has to be unused before it is deleted
	 * from the session.
	 */
	void changeWaitingTime(final int waitingTimeInSeconds);
	/**
	 * Checks if there is an information package with a given uuid in the
	 * session.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return True if there is an information package with this uuid in
	 * the session, false otherwise.
	 */
	boolean informationPackageExists(final Uuid uuidOfInformationPackage);
	/**
	 * Saves an information package, converts it to triples and saves them in the triple store. Then, the 
	 * information package is removed from the session.
	 * @param uuidOfInformationPackage The uuid of the information package we want to save.
	 */
	void saveInformationPackageAndRemoveFromSession(final Uuid uuidOfInformationPackage);
	/**
	 * Initializes the service object and prepares the session.
	 */
	void initialize();
	/**
	 * Adds an individual to an information package.
	 * @param uuidOfInformationPackage The uuid of the information package the individual should be added to.
	 * @param individual The individual that should be added.
	 */
	void addIndividualToInformationPackage(final Uuid uuidOfInformationPackage, final OntologyIndividual individual);
	/**
	 * Creates a new submission information package and adds it to the session. A SIP can contain several references. 
	 * @param title The title of the information package.
	 * @return A response object for the newly created SIP.
	 */
	PackageResponse createNewSubmissionInformationPackage(final String title);
	/**
	 * Creates a new submission information unit and adds it to the session. A SIU can contain several references. 
	 * @param title The title of the information package.
	 * @return A response object for the newly created SIU.
	 */
	PackageResponse createNewSubmissionInformationUnit(final String title);
	/**
	 * Adds a reference to a ressource in the internet to the information package
	 * @param url The url the reference points to.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 */
	void addRemoteFileReferenceToInformationPackage(final String url, final Uuid uuidOfInformationPackage);
	/**
	 * Determines all references assigned to an information package. The result contains the references to
	 * remote and to local files.
	 * @param uuidOfInformationPackage The uuid of the information package we are interested in.
	 * @return All references assigned to this information package.
	 */
	ReferenceCollection getAllReferencesOfInformationPackage(final Uuid uuidOfInformationPackage);
	/**
	 * Adds a reference to a file saved in the storage directory of the server.
	 * @param filename The name of the file the reference is pointing to.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @param uuidOfDigitalObject The uuid of the digital object.
	 */
	void addLocalFileReferenceToInformationPackage(final String filename, final Uuid uuidOfInformationPackage, 
			final Uuid uuidOfDigitalObject);
	/**
	 * Sets the object that contains the method for accessing the module TripleStore.
	 * @param sessionTripleStoreFacade The object that contains the method for accessing the module TripleStore.
	 */
	void setSessionTripleStoreFacade(final SessionTripleStoreFacade sessionTripleStoreFacade);
	/**
	 * Sets the object that contains the method for accessing the module Database.
	 * @param sessionDatabaseFacade The object that contains the method for accessing the module Database.
	 */
	void setSessionDatabaseFacade(final SessionDatabaseFacade sessionDatabaseFacade);
	/**
	 * Edits an individual in an information package. The existing individual is replaced by the new one.
	 * @param uuidOfInformationPackage The uuid of the information package that contains the individual.
	 * @param individual The individual that should replace another one with the same uuid.
	 */
	void editIndividualInInformationPackage(final Uuid uuidOfInformationPackage, final OntologyIndividual individual);
	/**
	 * Loads an information package in the session for modifications.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 */
	void loadInformationPackage(final Uuid uuidOfInformationPackage);
	/**
	 * Sets the object that contain all information about the ontologies.
	 * @param ontologyCache The ontology cache object.
	 */
	void setOntologyCache(final OntologyCache ontologyCache);
	/**
	 * Sets the factory for creating properties.
	 * @param propertyFactory The factory object.
	 */
	void setPropertyFactory(final PropertyFactory propertyFactory);
	/**
	 * Adds some meta data to this information package.
	 * @param tripleStatements Describe the meta data of the file.
	 * @param uuidOfInformationPackage The uuid of the information package we are working with.
	 * @param uuidOfFile The uuid of the file the meta data belong to. 
	 */
	void addMetadataToInformationPackage(TripleStatementCollection tripleStatements, Uuid uuidOfInformationPackage,
			Uuid uuidOfFile);
	/**
	 * Assigns several individuals to an information package. 
	 * @param uuidOfInformationPackage The uuid of the information package that should reference to 
	 * the individuals.
	 * @param ontologyIndividuals The individuals that should be assigned to the information package. 
	 */
	void assignIndividualsToInformationPackage(Uuid uuidOfInformationPackage,
			OntologyIndividualCollection ontologyIndividuals);
	/**
	 * Assigns several individuals to an information package. 
	 * @param uuidOfInformationPackage The uuid of the information package that should reference to 
	 * the individuals.
	 * @param uuidsOfIndividuals The uuids of the individuals that should be assigned to the information package. 
	 */
	public void assignIndividualsToInformationPackage(Uuid uuidOfInformationPackage, UuidList uuidsOfIndividuals);
	/**
	 * Removes the reference to an individual from the information package. These are uuids of individuals that
	 * are saved in the triple store. 
	 * @param uuidOfInformationPackage The uuid of the information package we want to remove a reference to an 
	 * individual from.
	 * @param uuidsOfIndividuals The uuids of the individuals that should be removed from the information package.
	 */
	void unassignIndividualsFromInformationPackage(Uuid uuidOfInformationPackage, UuidList uuidsOfIndividuals);
	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param irisOfIndividuals The iris of the individuals from the taxonomy that should be 
	 * assigned to the information package. 
	 * @param iriOfInformationPackage The iri of the information package the individuals should be assigned to. 
	 */
	void assignTaxonomyIndividualsToInformationPackage(IriCollection irisOfIndividuals,
			Iri iriOfInformationPackage);
	/**
	 * Finds the iris of all taxonomy individuals that are assigned to an certain 
	 * information package. 
	 * @param uuidOfInformationPackage The uuid of the information package whose
	 * taxonomy individuals we want to determine.
	 * @return The iris of all taxonomy individuals that are assigned to the 
	 * information package with the uuid uuidOfInformationPackage.
	 */
	IriCollection findAllIrisOfAssignedTaxonomyIndividuals(Uuid uuidOfInformationPackage);
	void setObjectPropertyFactory(ObjectPropertyFactory objectPropertyFactory);
}
