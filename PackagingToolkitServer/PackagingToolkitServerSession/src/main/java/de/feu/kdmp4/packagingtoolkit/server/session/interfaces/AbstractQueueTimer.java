package de.feu.kdmp4.packagingtoolkit.server.session.interfaces;

import java.util.Date;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.session.exceptions.SessionException;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

/**
 * An abstract class for the queue timer. The timer removes objects that are not used anymore
 * after a certain time.
 * @author Christopher Olbertz
 *
 */
public abstract class AbstractQueueTimer {
	/**
	 * The default number of seconds that have to be passed before an unused information package is
	 * removed from the thread. Can be changed by the constructor.
	 */
	private static final int SECONDS_TO_REMOVE_PACKAGE_FROM_QUEUE = 600;
	/**
	 * The number of seconds that have to be passed before an unused information package is
	 * removed from the thread.
	 */
	private int secondsToRemove;
	
	/**
	 * Sets the number of seconds that have to be passed before an unused information package is
	 * removed from the thread to the default number of 600.
	 */
	public AbstractQueueTimer() {
		secondsToRemove = SECONDS_TO_REMOVE_PACKAGE_FROM_QUEUE;
	}
	
	/**
	 * Gets a number of seconds. These are the seconds an information package has to be unused before
	 * it is remove from the thread.
	 * @param secondsToRemove The number of seconds for the time out.
	 */
	public AbstractQueueTimer(final int secondsToRemove) {
		if (secondsToRemove <= 0) {
			SessionException exception = SessionException.secondsInvalidException(secondsToRemove);
			throw exception;
		}
		
		this.secondsToRemove = secondsToRemove;
	}
	
	/**
	 * Returns the key of the first entry of the data structure.
	 * @return The key of the first entry. 
	 */
	public abstract Uuid getFirstKey();
	/**
	 * Returns the value of the first entry of the data structure.
	 * @return The value of the first entry. 
	 */
	public abstract Date getFirstValue();
	/**
	 * Checks if the queue timer contains any information packages.
	 * @return True if the queue timer is empty, false otherwise.
	 */
	public abstract boolean isEmpty();
	/**
	 * Checks if the queue timer contains any information packages.
	 * @return True if the queue timer is not empty, false otherwise.
	 */
	public abstract boolean isNotEmpty();
	/**
	 * Puts a new key-value pair in the data structure. The key is an uuid and the value is a date.
	 * @param uuid The key object.
	 * @param date The value object.
	 */
	public abstract void put(final Uuid uuid, final Date date);
	/**
	 * Checks if there is an information package whose time is over and that has 
	 * to be removed from the session. 
	 * @return The uuid of the information package to remove. Null if there is
	 * no information package that has to be removed.
	 */
	public Uuid removeIfNecessary() {
		if (isNotEmpty()) {
			final Date firstDateInQueue = getFirstValue();
			final Date now = new Date();
			final long difference = DateTimeUtils.getDiffInMilliseconds(
					firstDateInQueue.getTime(), now.getTime());
			final long differenceInSeconds = DateTimeUtils.convertMillisecondsToSeconds(difference);
			if (differenceInSeconds > secondsToRemove) {
				final Uuid uuid = getFirstKey();
				remove(uuid);
				return uuid;
			} else {
				return null;
			}
		}
		return null;
	}
	
	/**
	 * Mainly for testing purposes. The time the threads waits before it looks for expired
	 * information packages in the session.
	 * @param seconds The time the thread waits.
	 */
	public void setTimeOut(final int seconds) {
		if (secondsToRemove <= 0) {
			final SessionException exception = SessionException.secondsInvalidException(seconds);
			throw exception;
		}
		this.secondsToRemove = seconds;
	}
	
	/**
	 * Removes an information package from the session.
	 * @param uuid The uuid of the information package that should be removed from the session.
	 */
	public abstract void remove(final Uuid uuid);
	
	public abstract int getSize();
}
