package de.feu.kdmp4.packagingtoolkit.server.session.classes;

import static de.feu.kdmp4.packagingtoolkit.server.utils.ServerLogUtil.logInfoInformationPackageCreated;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.SessionDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.factories.ExchangeModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ObjectPropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.session.factories.InformationPackageFactory;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.SessionService;
import de.feu.kdmp4.packagingtoolkit.server.session.model.SubmissionInformationPackageImpl;
import de.feu.kdmp4.packagingtoolkit.server.session.model.SubmissionInformationUnitImpl;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;
import de.feu.kdmp4.server.triplestore.facades.SessionTripleStoreFacade;

public class SessionServiceImpl implements SessionService {
	/**
	 * Represents the session and contains the information packages that are currently
	 * in the session
	 */
	private QueueThread queueThread;
	/**
	 * A reference to the fassade that the session module uses for access the methods
	 * of the triplestore module.
	 */
	private SessionTripleStoreFacade sessionTripleStoreFacade;
	/**
	 * A reference to the fassade that the session module uses for access the methods
	 * of the database module.
	 */
	private SessionDatabaseFacade sessionDatabaseFacade;
	/**
	 * Contains the objects that have been read from the base ontology. 
	 */
	private OntologyCache ontologyCache;
	/**
	 * Creates the objects that represent the properties that have been declared in the
	 * base ontology.
	 */
	private PropertyFactory propertyFactory;
	
	private ObjectPropertyFactory objectPropertyFactory;

	/**
	 * Intializes the business logic and starts the thread. 
	 */
	@PostConstruct
	@Override
	public void initialize() {
		queueThread = new QueueThread();
		queueThread.start();
	}

	/**
	 * Only for testing purposes and for this reason the method is package public. So the test classes
	 * can access it.
	 * @param uuid The uuid of the information package to find.
	 * @return The found information package.
	 */
	BasicInformationPackage getInformationPackage(final Uuid uuid) {
		return queueThread.getInformationPackage(uuid);
	}

	@Override
	public boolean informationPackageExists(final Uuid uuidOfInformationPackage) {
		return queueThread.containsInformationPackage(uuidOfInformationPackage);
	}
	
	@Override
	public void loadInformationPackage(final Uuid uuidOfInformationPackage) {
		final Optional<InformationPackageExchange> optionalWithInformationPackageExchange = sessionDatabaseFacade.findInformationPackageByUuid(uuidOfInformationPackage.toString()); 
		if (optionalWithInformationPackageExchange.isPresent()) {
			final InformationPackageExchange informationPackageExchange = optionalWithInformationPackageExchange.get(); 
			BasicInformationPackage informationPackage = null;
			final LocalDateTime creationDate = informationPackageExchange.getCreationDate();
			final LocalDateTime lastModificationDate = informationPackageExchange.getLastModifiedDate();
			
			if (informationPackageExchange.isSubmissionInformationPackage()) {
				informationPackage = new SubmissionInformationPackageImpl(informationPackageExchange.getTitle(), uuidOfInformationPackage, creationDate, lastModificationDate);
			} else {
				informationPackage = new SubmissionInformationUnitImpl(informationPackageExchange.getTitle(), uuidOfInformationPackage, creationDate, lastModificationDate);
			}
			
			informationPackage.setSerializationFormat(informationPackageExchange.getSerializationFormat());
			// Load information from the triple store und put them into the information package.
			final TripleStatementCollection tripleStatementsOfInformationPackage = sessionTripleStoreFacade.findTriplesOfInformationPackage(uuidOfInformationPackage);
			removeTaxonomyIndividualStatements(tripleStatementsOfInformationPackage);
			addReferences(informationPackage, tripleStatementsOfInformationPackage);
			addIndividuals(informationPackage, tripleStatementsOfInformationPackage);
			putInformationPackage(informationPackage);
		}
	}
	
	/**
	 * Removes all statements that describe the relationship between an information package and a
	 * taxonomy individual, because these statements are not necessary for the current step. 
	 * @param tripleStatements The collection we want to remove the taxonomy individual statements
	 * from. 
	 */
	private void removeTaxonomyIndividualStatements(final TripleStatementCollection tripleStatements) {
		for (int i = 0; i < tripleStatements.getTripleStatementsCount(); i++) {
			final TripleStatement tripleStatement = tripleStatements.getTripleStatement(i);
			final TriplePredicate triplePredicate = tripleStatement.getPredicate();
			if (triplePredicate.isTaxonomyIndividualStatement()) {
				tripleStatements.removeTripleStatement(tripleStatement);
			}
		}
	}
	
	/**
	 * Adds individuals to an information package. The individuals are already coded as RDF triples because they are 
	 * saved in the triple store if the information package is saved.
	 * @param informationPackage The information package the individuals are assigned to. 
	 * @param tripleStatementsOfInformationPackage  Represent the individuals that are saved with the information 
	 * package.
	 */
	private void addIndividuals(final BasicInformationPackage informationPackage, 
								final TripleStatementCollection tripleStatementsOfInformationPackage) {
		final TripleStatementCollection individualStatements = tripleStatementsOfInformationPackage.getIndividualStatements();
		
		for (int i = 0; i < individualStatements.getTripleStatementsCount(); i++) {
			final TripleStatement tripleStatement = individualStatements.getTripleStatement(i);
			final Iri iriOfIndividual = tripleStatement.getSubject().getIri();
			final Uuid uuidOfIndividual = new Uuid(iriOfIndividual.getLocalName().getLocalName());
			
			final TripleStatementCollection propertyStatements = sessionTripleStoreFacade.findTriplesByIndividual(uuidOfIndividual);
			final OntologyIndividual ontologyIndividual = createIndividualFromStatements(uuidOfIndividual, propertyStatements);
			
			informationPackage.addOntologyIndividual(ontologyIndividual);
		}
	}
	
	/**
	 * Extracts the properties of an individual from a collection with rdf statements. The properties are added to an individual. 
	 * @param uuidOfIndividual The uuid of the individual whose properties we want to extract from triple statements.
	 * @param propertyStatements The statements that describe the properties of the individual.
	 * @return The individual that has been created from the statements. 
	 */
	private OntologyIndividual createIndividualFromStatements(final Uuid uuidOfIndividual, 
															  final TripleStatementCollection propertyStatements) {
		OntologyIndividual ontologyIndividual = null;
		
		if (uuidOfIndividual != null) {
			ontologyIndividual = new OntologyIndividualImpl(uuidOfIndividual.toString());
		} else {
			ontologyIndividual = new OntologyIndividualImpl();
		}
		ontologyIndividual.setUuid(uuidOfIndividual);
		
		for (int j = 0; j < propertyStatements.getTripleStatementsCount(); j++) {
				final TripleStatement statementOfProperty = propertyStatements.getTripleStatement(j);
				final TriplePredicate predicateOfProperty = statementOfProperty.getPredicate();
				final TripleObject objectOfProperty = statementOfProperty.getObject();
				
				if (predicateOfProperty.isTypePredicate()) {
					determineClassOfIndividual(predicateOfProperty, objectOfProperty, ontologyIndividual);
				} else if (predicateOfProperty.isLiteralPredicate()) {
					determineDatatypeOfLiteralStatement(predicateOfProperty, objectOfProperty, ontologyIndividual);
				} else if (predicateOfProperty.isObjectPredicate()) {
					final OntologyObjectProperty ontologyObjectProperty = createObjectProperty(statementOfProperty);
					ontologyIndividual.addObjectProperty(ontologyObjectProperty);
				}
			}
		
		return ontologyIndividual;
	}
	
	private OntologyObjectProperty createObjectProperty(final TripleStatement tripleStatement) {
		final TripleSubject tripleSubject = tripleStatement.getSubject();
		final TriplePredicate triplePredicate = tripleStatement.getPredicate();
		final TripleObject tripleObject = tripleStatement.getObject();
		
		final Iri iriOfSubject = ServerModelFactory.createIri(tripleSubject.toString());
		final String propertyDomain = sessionTripleStoreFacade.findOntologyClassOfIndividual(iriOfSubject);
		final Iri iriOfObject = ServerModelFactory.createIri(tripleObject.toString());
		final String propertyRange = sessionTripleStoreFacade.findOntologyClassOfIndividual(iriOfObject);
		
		final String propertyIri = triplePredicate.toString();
		final String label = triplePredicate.toString();
		
		final OntologyClassesMap ontologyClassesMap = ontologyCache.getOntologyClassesMap();
		final OntologyObjectProperty ontologyObjectProperty = objectPropertyFactory.getObjectProperty(propertyIri, label, 
				propertyRange, propertyDomain, ontologyClassesMap);
		
		final IriCollection irisOfValues = ServerModelFactory.createEmptyIriCollection();
		irisOfValues.addIri(iriOfObject);
		ontologyObjectProperty.setIrisOfValues(irisOfValues);
		return ontologyObjectProperty;
	}
	
	/**
	 * Determines the datatype of a statement that represents a property with a literal as object. This is important 
	 * because the information of the datatype is needed for loading the individual in the gui. Every datatype is
	 * represented by a specific component in the user interface. The information about the datatype is
	 * saved with the property in the triple store and can be loaded. 
	 * @param predicateOfProperty The predicate of the statement that represents the property.
	 * @param objectOfProperty The object of the statement that represents the property.
	 * @param ontologyIndividual The individual that contains the property.
	 */
	private void determineDatatypeOfLiteralStatement(TriplePredicate predicateOfProperty, TripleObject objectOfProperty, OntologyIndividual ontologyIndividual) {
		final Object value = objectOfProperty.getValue();
		final String iriOfProperty = predicateOfProperty.getIri().toString();
		final Optional<OntologyDatatype> optionalWithDatatype = sessionTripleStoreFacade.determineDatatypeOfProperty(iriOfProperty);
		if (optionalWithDatatype.isPresent()) {
			final OntologyDatatype datatype = optionalWithDatatype.get();
			final String propertyRange = datatype.getIri();

			final OntologyDatatypeProperty datatypeProperty = propertyFactory.getProperty(propertyRange, iriOfProperty, iriOfProperty);
			datatypeProperty.setPropertyValue(value);
			ontologyIndividual.addDatatypeProperty(datatypeProperty);
		}
	}
	
	/**
	 * Determines the class of the individual as described in the statements and enters it in the individual. 
	 * @param predicateOfIndividual The type predicate. 
	 * @param objectOfIndividual Describes the class of this individual.
	 * @param ontologyIndividual The individual whose class should be determined.
	 */
	private void determineClassOfIndividual(final TriplePredicate predicateOfIndividual, final TripleObject objectOfIndividual, 
			final OntologyIndividual ontologyIndividual) {
		if (predicateOfIndividual.isTypePredicate()) {
			final String iriOfClass = objectOfIndividual.getIri().toString();
			final OntologyClass ontologyClass = ontologyCache.getClassByIri(iriOfClass);
			ontologyIndividual.setOntologyClass(ontologyClass);
		}
	}
	
	/**
	 * Reads the triples about the references from the triple store and adds the references to the information package.
	 * @param informationPackage
	 * @param tripleStatementsOfInformationPackage
	 */
	private void addReferences(final BasicInformationPackage informationPackage, 
			final TripleStatementCollection tripleStatementsOfInformationPackage) {
		while (tripleStatementsOfInformationPackage.hasNextReferenceStatement()) {
			final TripleStatement referenceStatement = tripleStatementsOfInformationPackage.getNextReferenceStatement();
			final TripleObject tripleObject = referenceStatement.getObject();
			final TriplePredicate referencePredicate = referenceStatement.getPredicate();
			
			if (referencePredicate.isLocalFilePredicate()) {
				final Uuid uuidOfFile = new Uuid(tripleObject.getValue().toString());
				final Reference reference = sessionDatabaseFacade.findReferenceByUuid(uuidOfFile.toString());
				informationPackage.addReference(reference);
			} else {
				final String url = tripleObject.getLocalName().toString();
				final Reference reference = ServerModelFactory.createRemoteReference(url);
				informationPackage.addReference(reference);
			}
		}
	}
	
	/**
	 * Converts the individuals of an information package to statements for the triple store.  If the individuals are already
	 * saved in the triple store they are removed because they are modified if they already exist.
	 * @param informationPackage The information package that contains the individuals.
	 * @return A collection with the statements for the individuals of informationPackage.
	 */
	private TripleStatementCollection convertIndividualsToTriples(final BasicInformationPackage informationPackage) {
		final OntologyIndividualCollection ontologyIndividuals = informationPackage.getOntologyIndividualList();
		final TripleStatementCollection tripleStatements =  RdfElementFactory.createEmptyTripleStatementCollection();
		
		for (int i = 0; i < ontologyIndividuals.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = ontologyIndividuals.getIndividual(i);
			final Optional<TripleStatementCollection> optionalWithTripleStatements = ontologyIndividual.toTripleStatements();
				if (optionalWithTripleStatements.isPresent()) {
					tripleStatements.addStatements(optionalWithTripleStatements.get());
				}
				final Uuid uuidOfIndividual = ontologyIndividual.getUuid();
				sessionTripleStoreFacade.deleteAllTriplesOfIndividualIfExists(uuidOfIndividual);
			}			
		
		return tripleStatements;
	}
	
	@Override
	public void saveInformationPackageAndRemoveFromSession(final Uuid uuidOfInformationPackage) {
		if (informationPackageExists(uuidOfInformationPackage)) {
			final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
			final TripleStatementCollection tripleStatements = convertIndividualsToTriples(informationPackage);
			final TripleStatementCollection tripleStatementsOfUuids = informationPackage.getStatementsOfAssignedIndividuals();
			tripleStatements.addStatements(tripleStatementsOfUuids);
			final TripleStatementCollection tripleStatementsOfTaxonomyIndividuals = informationPackage.getStatementsOfTaxonomyIndividuals();
			tripleStatements.addStatements(tripleStatementsOfTaxonomyIndividuals);
			final TripleStatementCollection tripleStatementsOfReferences = informationPackage.toStatements();
			tripleStatements.addStatements(tripleStatementsOfReferences);
			sessionTripleStoreFacade.unassignAllIndividualsFromInformationPackage(uuidOfInformationPackage);
			sessionTripleStoreFacade.deleteAllTaxomonyIndividualsOfInformationPackage(uuidOfInformationPackage);
			sessionTripleStoreFacade.saveTriples(tripleStatements); 
			InformationPackageExchange informationPackageExchange = null;
			
			if (informationPackage.hasCreationDate()) {
				informationPackageExchange = ExchangeModelFactory.createInformationPackageExchange(
						informationPackage.getTitle(), informationPackage.getUuid().toString(), informationPackage.getPackageType(), 
						informationPackage.getCreationDate(), DateTimeUtils.getNow(), informationPackage.getSerializationFormat());
			} else {
				informationPackageExchange = ExchangeModelFactory.createInformationPackageExchange(
						informationPackage.getTitle(), informationPackage.getUuid().toString(), informationPackage.getPackageType(), 
						DateTimeUtils.getNow(), DateTimeUtils.getNow(), informationPackage.getSerializationFormat());
			}
			
			if (informationPackage.containsReferences()) {
				final ReferenceCollection references = informationPackage.getReferences(); 
				for (int i = 0; i < references.getReferencesCount(); i++) {
					final Reference reference = references.getReference(i);
					if (reference.isLocalFileReference()) {
						informationPackageExchange.addUuidOfFile(reference.getUuidOfFile().toString());
					}
				}
			}
			
			sessionDatabaseFacade.saveInformationPackage(informationPackageExchange);
			removeInformationPackageFromSession(uuidOfInformationPackage);
		}
	}
	
	@Override
	public void addIndividualToInformationPackage(final Uuid uuidOfInformationPackage, final OntologyIndividual individual) {
		if (informationPackageExists(uuidOfInformationPackage)) {
			final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
			if (informationPackage.containsIndividual(individual)) {
				informationPackage.removeIndividual(individual);
			} 
			informationPackage.addOntologyIndividual(individual);
		}
	}
	
	@Override
	public void editIndividualInInformationPackage(final Uuid uuidOfInformationPackage, final OntologyIndividual individual) {
		if (informationPackageExists(uuidOfInformationPackage)) {
			final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
			informationPackage.replaceOntologyIndividual(individual);
		}
	}
	
	@Override
	public PackageResponse createNewSubmissionInformationPackage(final String title) {
		final BasicInformationPackage informationPackage = InformationPackageFactory.createSubmissionInformationPackage(title);
		logInfoInformationPackageCreated(informationPackage.getTitle(), informationPackage.getUuid());
		informationPackage.setSerializationFormat(SerializationFormat.OAIORE);
		
		putInformationPackage(informationPackage);
		PackageResponse packageResponse = getPackageResponse(informationPackage); 
		return packageResponse;
	}
	
	@Override
	public PackageResponse createNewSubmissionInformationUnit(final String title) {
		final BasicInformationPackage informationPackage = InformationPackageFactory.createSubmissionInformationUnit(title);
		logInfoInformationPackageCreated(informationPackage.getTitle(), informationPackage.getUuid());
		informationPackage.setSerializationFormat(SerializationFormat.OAIORE);
		
		putInformationPackage(informationPackage);
		final PackageResponse packageResponse = getPackageResponse(informationPackage); 
		return packageResponse;
	}
	
	/**
	 * Converts an information package into a response object for being send via http. This method is not in the
	 * class {@link de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory} because the interface
	 * {@link de.feu.kdmp4.packagingtoolkit.server.models.interfaces.BasicInformationPackage} is 
	 * encapsulated in the session module and should not be known in the rest of the server component.
	 * @param informationPackage The information package to convert into a response.
	 * @return The response object.
	 */
	private PackageResponse getPackageResponse(final BasicInformationPackage informationPackage) {
		final String title = informationPackage.getTitle();
		final PackageType packageType = informationPackage.getPackageType();
		final Uuid uuid = informationPackage.getUuid();
		final UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(uuid);
		
		return new PackageResponse(packageType.toString(), uuidResponse, title);
	}

	/**
	 * Puts an information package in the session.
	 * @param informationPackage The information package to put in the queue.
	 */
	private void putInformationPackage(final BasicInformationPackage informationPackage) {
		queueThread.updateQueue(informationPackage);
	}

	@Override
	public BasicInformationPackage removeInformationPackageFromSession(final Uuid uuid) {
		final BasicInformationPackage informationPackage = getInformationPackage(uuid);
		queueThread.removeFromSession(uuid);
		return informationPackage;
	}

	@Override
	public void addLocalFileReferenceToInformationPackage(final String filename, final Uuid uuidOfInformationPackage,
			Uuid uuidOfDigitalObject) { 
		final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
		
		final Reference referenceToDigitalObject = ServerModelFactory.createLocalFileReference(filename, uuidOfDigitalObject);
		informationPackage.addReference(referenceToDigitalObject);
		
		queueThread.updateQueue(informationPackage);
	}
	
	@Override
	public void addRemoteFileReferenceToInformationPackage(final String url, final Uuid uuid) {
		final BasicInformationPackage informationPackage = getInformationPackage(uuid);
		final Reference referenceToDigitalObject = ServerModelFactory.createRemoteReference(url);
		informationPackage.addReference(referenceToDigitalObject);
	}
	
	@Override
	public IriCollection findAllIrisOfAssignedTaxonomyIndividuals(final Uuid uuidOfInformationPackage) {
		final TripleStatementCollection tripleStatements = sessionTripleStoreFacade.findAllTriplesOfAssignedTaxonomyIndividuals(
				uuidOfInformationPackage);
		final IriCollection iris = ServerModelFactory.createEmptyIriCollection();
		
		for (int i = 0; i < tripleStatements.getTripleStatementsCount(); i++) {
			final TripleStatement tripleStatement = tripleStatements.getTripleStatement(i);
			final Iri iri = tripleStatement.getSubject().getIri();
			iris.addIri(iri);
		}
		
		return iris;
	}
	
	@Override
	public void updateQueue(final Uuid uuid) {
		final BasicInformationPackage informationPackage = getInformationPackage(uuid);
		queueThread.updateQueue(informationPackage);
	}
	
	@Override
	public ReferenceCollection getAllReferencesOfInformationPackage(final Uuid uuid) {
		final BasicInformationPackage informationPackage = getInformationPackage(uuid);
		return informationPackage.getReferences();
	}
	
	@Override
	public void changeTimeout(final int seconds) {
		queueThread.changeTimeout(seconds);
	}
	
	@Override
	public void changeWaitingTime(final int waitingTimeInSeconds) {
		queueThread.changeWaitingTime(waitingTimeInSeconds);
	}
	
	@Override
	public void setSessionTripleStoreFacade(final SessionTripleStoreFacade sessionTripleStoreFacade) {
		this.sessionTripleStoreFacade = sessionTripleStoreFacade;
	}
	
	@Override
	public void setSessionDatabaseFacade(final SessionDatabaseFacade sessionDatabaseFacade) {
		this.sessionDatabaseFacade = sessionDatabaseFacade;
	}
	
	@Override
	public void setOntologyCache(final OntologyCache ontologyCache) {
		this.ontologyCache = ontologyCache;
	}
	
	@Override
	public void setPropertyFactory(final PropertyFactory propertyFactory) {
		this.propertyFactory = propertyFactory;
	}

	@Override
	public void addMetadataToInformationPackage(final TripleStatementCollection tripleStatements, final Uuid uuidOfInformationPackage, 
			final Uuid uuidOfFile) {
		final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
		informationPackage.addMetadataProperty(tripleStatements, uuidOfFile);
	}

	@Override
	public void assignIndividualsToInformationPackage(final Uuid uuidOfInformationPackage,
			final OntologyIndividualCollection ontologyIndividuals) {
		for (int i = 0; i < ontologyIndividuals.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = ontologyIndividuals.getIndividual(i);
			addIndividualToInformationPackage(uuidOfInformationPackage, ontologyIndividual);
		}
	}

	@Override
	public void assignIndividualsToInformationPackage(final Uuid uuidOfInformationPackage, final UuidList uuidsOfIndividuals) {
		if (informationPackageExists(uuidOfInformationPackage)) {
			final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
			for (int i = 0; i < uuidsOfIndividuals.getUuidCount(); i++) {
				final Uuid uuid = uuidsOfIndividuals.getUuidAt(i);
				informationPackage.addAssignedUuid(uuid);
			}
		}
	}
	
	@Override
	public void unassignIndividualsFromInformationPackage(final Uuid uuidOfInformationPackage, final UuidList uuidsOfIndividuals) {
		if (informationPackageExists(uuidOfInformationPackage)) {
			final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
			for (int i = 0; i < uuidsOfIndividuals.getUuidCount(); i++) {
				final Uuid uuid = uuidsOfIndividuals.getUuidAt(i);
				informationPackage.removeAssignedUuid(uuid);
			}
		}
	}

	@Override
	public void assignTaxonomyIndividualsToInformationPackage(IriCollection irisOfIndividuals,
			Iri iriOfInformationPackage) {
		final String uuidOfInformationPackageAsString = iriOfInformationPackage.getLocalName().toString();
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(uuidOfInformationPackageAsString);
		final BasicInformationPackage informationPackage = getInformationPackage(uuidOfInformationPackage);
		informationPackage.addTaxonomyIndividuals(irisOfIndividuals);
	}
	
	@Override
	public void setObjectPropertyFactory(ObjectPropertyFactory objectPropertyFactory) {
		this.objectPropertyFactory = objectPropertyFactory;
	}
}
 