package de.feu.kdmp4.packagingtoolkit.server.session.exceptions;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class SessionException extends ProgrammingException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8886160205821208866L;

	private SessionException(String message) {
		super(message);
	}
	
	public static SessionException dateMayNotBeNullException(
			final String theClass, 
			final String theMethod) {
		String message = I18nExceptionUtil.getDateMayNotBeNullString();
		return new SessionException(message);
	}
	
	public static SessionException informationPackageNotAvailableException(
			final Uuid uuid) {
		String message = I18nExceptionUtil.getInformationPackageNotAvailableString();
		message = String.format(message, uuid);
		return new SessionException(message);
	}

	public static SessionException secondsInvalidException(final int seconds) {
		String message = I18nExceptionUtil.getSecondsInvalidString();
		message = String.format(message, seconds);
		return new SessionException(message);
	}
	
	public static SessionException digitalObjectNotContainedInSIP(final String uuidOfDigitalObject,
			final String uuidOfSIP) {
		String message = I18nExceptionUtil.getDigitalObjectNotContainedInSIPString(uuidOfDigitalObject, uuidOfSIP);
		return new SessionException(message);
	}
	
	/**
	 * Returns the additional information of this exception.
	 * @return The additional information of this exception.
	 */
	@Override
	public String getAdditionalInformation() {
		if (additionalInformation != null) {
			return additionalInformation.toString();
		} else {
			return StringUtils.EMPTY_STRING;
		}
	}

	@Override
	public String getMessage() {
		String message = super.getMessage();
		return String.format(message, getAdditionalInformation());
	}
}