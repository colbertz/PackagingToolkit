package de.feu.kdmp4.packagingtoolkit.server.session.model.classes;

import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitFileException;
import de.feu.kdmp4.packagingtoolkit.models.classes.OaisEntityImpl;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class DigitalObjectImpl extends OaisEntityImpl implements DigitalObject {
	/**
	 * The name of the digital object.
	 */
	private String filename;
	/**
	 * the reference of the file. It is used by the information packages that
	 * point to this digital object.
	 */
	private Reference reference;
	
	/**
	 * Constructs a new digital object only with a filename. A new uuid is being created.
	 * @param filename The name of the file the digital object is pointing to. 
	 */
	public DigitalObjectImpl(final String filename) {
		this(null, filename);
	}
	
	// TODO Die Referenz muss wohl noch im Konstruktor gesetzt werden.
	/**
	 * Constructs a new digital object with an existing uuid.
	 * @param uuid The uuid of the file.
	 * @param filename The name of the file.
	 */
	public DigitalObjectImpl(final Uuid uuid, final String filename) {
		super(uuid);
		if (!StringValidator.isNotNullOrEmpty(filename)) {
			PackagingToolkitFileException exception = PackagingToolkitFileException.
					filenameMayNotBeEmptyException();
			throw exception;
		}
		
		this.filename = filename;
	}

	/**
	 * Compares two digital objects. Compared are the filenames.
	 * @param digitalObject The digital object we want to compare with this 
	 * digital object.
	 * @return 0 if the digital objects are equal, a value less than 0 if this digital object
	 * is less than the other one and a value greater than 0 if this digital object is
	 * greater than the other one.
	 */
	@Override
	public int compareTo(final DigitalObject digitalObject) {
		return filename.compareTo(digitalObject.getFilename());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitalObjectImpl other = (DigitalObjectImpl) obj;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return filename;
	}
	
	@Override
	public String getFilename() {
		return filename;
	}
	
	@Override
	public Reference getReference() {
		return reference;
	}
}
