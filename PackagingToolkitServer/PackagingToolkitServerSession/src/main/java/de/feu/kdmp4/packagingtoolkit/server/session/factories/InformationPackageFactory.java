package de.feu.kdmp4.packagingtoolkit.server.session.factories;

import de.feu.kdmp4.packagingtoolkit.server.models.classes.DigitalObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.session.model.SubmissionInformationPackageImpl;
import de.feu.kdmp4.packagingtoolkit.server.session.model.SubmissionInformationUnitImpl;
import de.feu.kdmp4.packagingtoolkit.server.session.model.classes.UuidsOfFilesAndTheirMetadataHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.SubmissionInformationPackage;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.SubmissionInformationUnit;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.UuidsOfFilesAndTheirStatements;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

/**
 * A factory class for creating information packages. For every type of information
 * package, there is an own creation method. This class is the only place where 
 * it is allowed to call the constructor of the classes that represent information
 * packages.  
 * @author Christopher Olbertz
 *
 */
public abstract class InformationPackageFactory {
	/**
	 * Creates a new archival information package. 
	 * @param title The title of the new sip.
	 * @return The newly created sip.
	 */
	public static final SubmissionInformationPackage createSubmissionInformationPackage(
																			  final String title) {
		return new SubmissionInformationPackageImpl(title, DateTimeUtils.getNow(), DateTimeUtils.getNow());
	}
	
	/**
	 * Creates a new archival information unit. 
	 * @param title The title of the new siu.
	 * @return The newly created siu.
	 */
	public static final SubmissionInformationUnit createSubmissionInformationUnit(
			final String title) {
		return new SubmissionInformationUnitImpl(title, DateTimeUtils.getNow(), DateTimeUtils.getNow());
	}
	
	/**
	 * Creates a new digital object.
	 * @param filename The name of the file contained in the digital object.
	 * @return The new digital object.
	 */
	public static DigitalObject createDigitalObject(final String filename, final String md5Checksum) {
		return new DigitalObjectImpl(filename, md5Checksum);
	}
	
	/**
	 * Creates an empty map that contains the uuids of files and the properties that contain the
	 * meta data values of the file.
	 * @return The newly created map.
	 */
	public static UuidsOfFilesAndTheirStatements createEmptyUuidsOfFilesAndTheirMetadataMap() {
		return new UuidsOfFilesAndTheirMetadataHashMapImpl();
	}
}
 