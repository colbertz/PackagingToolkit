package de.feu.kdmp4.packagingtoolkit.server.session.classes;

import java.util.concurrent.ConcurrentHashMap; 

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.session.exceptions.SessionException;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.InformationPackageQueue;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;
import static de.feu.kdmp4.packagingtoolkit.server.utils.ServerLogUtil.*;

public class InformationPackageQueueImpl implements InformationPackageQueue {
	/**
	 * Contains the information packages saved in the session. The information packages are 
	 * identified by their uuid.
	 */
	private ConcurrentHashMap<Uuid, BasicInformationPackage> informationPackageQueue;
	
	/**
	 * Constructs a new queue for the administration of the information packages in the session.
	 */
	public InformationPackageQueueImpl() {
		informationPackageQueue = new ConcurrentHashMap<>();
	}

	@Override
	public boolean contains(final Uuid uuid) {
		return informationPackageQueue.containsKey(uuid);
	}

	@Override
	public BasicInformationPackage get(final Uuid uuid) {
		if (!contains(uuid)) {
			final SessionException exception = SessionException.
					informationPackageNotAvailableException(uuid);
			throw exception;
		}
		return informationPackageQueue.get(uuid);
	}
	
	/**
	 * Logs the current state of the queue that represents the session. Logs first the number of information packages in
	 * the session and then the uuid of every information package.
	 */
	private void logQueueContent() {
		logInfoSessionState(informationPackageQueue.size());
		informationPackageQueue.forEach((uuid, informationPackage) -> {
			logInfoInformationPackageInSession(uuid);
		});
	}

	@Override
	public void put(final BasicInformationPackage informationPackage) {
		informationPackageQueue.put(informationPackage.getUuid(), informationPackage);
		logQueueContent();
	}

	@Override
	public void remove(final Uuid uuid) {
		informationPackageQueue.remove(uuid);
	}

	@Override
	public int getSize() {
		return informationPackageQueue.size();
	}
}
