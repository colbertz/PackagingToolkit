package de.feu.kdmp4.packagingtoolkit.server.session.facade;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.SessionService;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;

/**
 * Contains the methods that the module Packaging can use for accessing the module Session.
 * @author Christopher Olbertz
 *
 */
public class PackagingSessionFacade {
	/**
	 * A reference to the business logic of the module Session.
	 */
	private SessionService sessionService;
	
	/**
	 * Adds a reference to a file saved in the storage directory of the server.
	 * @param filename The name of the file the reference is pointing to.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @param uuidOfDigitalObject The uuid of the digital object.
	 */
	public void addLocalFileReferenceToInformationPackage(final String filename, final Uuid uuid, final Uuid uuidOfDigitalObject) {
		sessionService.addLocalFileReferenceToInformationPackage(filename, uuid, uuidOfDigitalObject);
	}
	
	/**
	 * Adds a reference to a ressource in the internet to the information package
	 * @param url The url the reference points to.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 */
	public void addRemoteFileReferenceToInformationPackage(final String url, final Uuid uuidOfInformationPackage) {
		sessionService.addRemoteFileReferenceToInformationPackage(url, uuidOfInformationPackage);
	}
	
	/**
	 * Adds some meta data to this information package.
	 * @param properties Contains the properties that represent the meta data values of the file.
	 * @param uuidOfInformationPackage The uuid of the information package we are working with.
	 * @param uuidOfFile The uuid of the file the meta data belong to. 
	 */
	public void addMetadataToInformationPackage(final TripleStatementCollection tripleStatements, final Uuid uuidOfInformationPackage, final Uuid uuidOfFile) {
		sessionService.addMetadataToInformationPackage(tripleStatements, uuidOfInformationPackage, uuidOfFile);
	}

	/**
	 * Removes an information package from the session.
	 * @param uuid The uuid of the information package to remove.
	 * @return The removed information package.
	 */
	public BasicInformationPackage removeInformationPackageFromSession(final Uuid uuid) {
		return sessionService.removeInformationPackageFromSession(uuid);
	}
	
	/**
	 * Adds an individual to an information package.
	 * @param uuidOfInformationPackage The uuid of the information package the individual should be added to.
	 * @param individual The individual that should be added.
	 */
	public void addIndividualToInformationPackage(final Uuid uuidOfInformationPackage, final OntologyIndividual individual) {
		sessionService.addIndividualToInformationPackage(uuidOfInformationPackage, individual);
	}
	
	/**
	 * Edits an individual in an information package. The existing individual is replaced by the new one.
	 * @param uuidOfInformationPackage The uuid of the information package that contains the individual.
	 * @param individual The individual that should replace another one with the same uuid.
	 */
	public void editIndividualInInformationPackage(final Uuid uuidOfInformationPackage, final OntologyIndividual individual) {
		sessionService.editIndividualInInformationPackage(uuidOfInformationPackage, individual);
	}
	
	/**
	 * Creates a new submission information package and adds it to the session. A SIP can contain several references. 
	 * @param title The title of the information package.
	 * @return A response object for the newly created SIP.
	 */
	public PackageResponse createNewSubmissionInformationPackage(final String title) {
		return sessionService.createNewSubmissionInformationPackage(title);
	}
	
	/**
	 * Creates a new submission information unit and adds it to the session. A SIU can contain several references. 
	 * @param title The title of the information package.
	 * @return A response object for the newly created SIU.
	 */
	public PackageResponse createNewSubmissionInformationUnit(final String title) {
		return sessionService.createNewSubmissionInformationUnit(title);
	}
	
	/**
	 * Loads an information package in the session for modifications.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 */
	public void loadInformationPackage(final Uuid uuidOfInformationPackage) {
		sessionService.loadInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Saves an information package, converts it to triples and saves them in the triple store. Then, the 
	 * information package is removed from the session.
	 * @param uuidOfInformationPackage The uuid of the information package we want to save.
	 */
	public void saveInformationPackageAndRemoveFromSession(final Uuid uuidOfInformationPackage) {
		sessionService.saveInformationPackageAndRemoveFromSession(uuidOfInformationPackage);
	}
	
	/**
	 * Checks if an information package exists in the session.
	 * @param uuidOfInformationPackage The uuid of the information package we want to check. 
	 * @return True if the information package exists in the session, false otherwise.
	 */
	public boolean informationPackageExistsInSession(final Uuid uuidOfInformationPackage) {
		return sessionService.informationPackageExists(uuidOfInformationPackage);
	}
	
	/**
	 * Sets a reference to the object that contains the business logic of the module Session.
	 * @param sessionService A reference to the object that contains the business logic of the module Session.
	 */
	public void setSessionService(final SessionService sessionService) {
		this.sessionService = sessionService;
	}

	/**
	 * Assigns several individuals to an information package. 
	 * @param uuidOfInformationPackage The uuid of the information package that should reference to 
	 * the individuals.
	 * @param uuidsOfIndividuals The uuids of the individuals that should be assigned to the information package. 
	 */
	public void assignIndividualsToInformationPackage(final Uuid uuidOfInformationPackage, final UuidList uuidsOfIndividuals) {
		sessionService.assignIndividualsToInformationPackage(uuidOfInformationPackage, uuidsOfIndividuals);
	}
	
	/**
	 * Removes the reference to an individual from the information package. These are uuids of individuals that
	 * are saved in the triple store. 
	 * @param uuidOfInformationPackage The uuid of the information package we want to remove a reference to an 
	 * individual from.
	 * @param uuidsOfIndividuals The uuids of the individuals that should be removed from the information package.
	 */
	public void unassignIndividualsFromInformationPackage(final Uuid uuidOfInformationPackage, final UuidList uuidsOfIndividuals) {
		sessionService.unassignIndividualsFromInformationPackage(uuidOfInformationPackage, uuidsOfIndividuals);
	}

	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param irisOfIndividuals The iris of the individuals from the taxonomy that should be 
	 * assigned to the information package. 
	 * @param iriOfInformationPackage The iri of the information package the individuals should be assigned to. 
	 */
	public void assignTaxonomyIndividualsToInformationPackage(final IriCollection irisOfIndividuals, 
			final Iri iriOfInformationPackage) {
		sessionService.assignTaxonomyIndividualsToInformationPackage(irisOfIndividuals, iriOfInformationPackage);
	}
	
	/**
	 * Finds the iris of all taxonomy individuals that are assigned to an certain 
	 * information package. 
	 * @param uuidOfInformationPackage The uuid of the information package whose
	 * taxonomy individuals we want to determine.
	 * @return The iris of all taxonomy individuals that are assigned to the 
	 * information package with the uuid uuidOfInformationPackage.
	 */
	public IriCollection findAllIrisOfAssignedTaxonomyIndividuals(final Uuid uuidOfInformationPackage) {
		return sessionService.findAllIrisOfAssignedTaxonomyIndividuals(uuidOfInformationPackage);
	}
}
