package de.feu.kdmp4.packagingtoolkit.server.session.classes;

import java.util.Date;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.session.exceptions.SessionException;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.AbstractQueueTimer;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.InformationPackageQueue;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;
import static de.feu.kdmp4.packagingtoolkit.server.utils.ServerLogUtil.*;

public class QueueThread extends Thread {
	/**
	 * The time in seconds the thread waits before it checks the
	 * session for unused information packages.
	 */
	private int waitingTimeInSeconds;
	/**
	 * True if the thread is running.
	 */
	private boolean running;
	/**
	 * A reference to the data structure that administrates the information packages.
	 */
	private InformationPackageQueue informationPackageQueue;
	/**
	 * A reference to the timer that coordinates the time out mechanism.
	 */
	private AbstractQueueTimer queueTimer;
	/**
	 * The default time in seconds a information package has to be unused before
	 * it is removed from the session.
	 */
	private static final int SECONDS_TIMEOUT = 600;
	/**
	 * The default time in seconds the thread waits before it checks the
	 * session for unused information packages.
	 */
	private static final int WAITING_TIME_IN_SECONDS = 10;
	/**
	 * The number of milliseconds a second contains.
	 */
	private static final int ONE_SECOND_IN_MILLISECONDS = 1000;
	
	/**
	 * Creates a new thread with default values.
	 */
	public QueueThread() {
		this(WAITING_TIME_IN_SECONDS, SECONDS_TIMEOUT);	
	}
	
	/**
	 * Creates a new thread.
	 * @param waitingTimeInSeconds The time in seconds the thread waits before it checks the
	 * session for unused information packages.
	 * @param timeoutInSeconds The time an information package may be unused before it is deleted 
	 * from the sessin.
	 */
	public QueueThread(final int waitingTimeInSeconds, 
			final int timeoutInSeconds) {
		informationPackageQueue = new InformationPackageQueueImpl();
		queueTimer = new QueueTimerImpl(timeoutInSeconds);
		
		if (waitingTimeInSeconds <= 0) {
			final SessionException exception = SessionException.secondsInvalidException(waitingTimeInSeconds);
			throw exception;
		}
		
		this.waitingTimeInSeconds = waitingTimeInSeconds;
		running = true;
	}
	
	/**
	 * Checks if a object with a given key is contained in the thread queue.
	 * @param uuid The uuid to look for.
	 * @return True, if the thread queue contains the key, false otherwise. 
	 */
	public boolean containsKey(final Uuid uuid) {
		return informationPackageQueue.contains(uuid);
	}
	
	/**
	 * Removes an information package with a given uuid from the session.
	 * @param uuid The uuid of the information package to remove.
	 */
	public void removeFromSession(final Uuid uuid) {
		if (uuid != null) {
			informationPackageQueue.remove(uuid);
			queueTimer.remove(uuid);
		} 
	}

	/**
	 * Finished the thread and removes all information packages from the session. 
	 * Information packages that have not been saved are lost.  
	 */
	public void shutdown() {
		running = false;
		this.informationPackageQueue = null;
	}
	
	/**
	 * Updates the queue. Is called after an operation on an aip has been 
	 * performed. The aip is inserted into the queue.
	 * @param archivalInformationPackage The aip to insert in the queue.
	 */
	public void updateQueue(final BasicInformationPackage informationPackage) {
		informationPackageQueue.put(informationPackage);
		queueTimer.put(informationPackage.getUuid(), new Date());
		logInfoInformationPackagePutInSession(informationPackage.getUuid());
	}
	
	/**
	 * Counts information packages in the session.
	 * @return The number of information packages in the session. 
	 */
	public int getInformationPackageCount() {
		return informationPackageQueue.getSize();
	}

	/**
	 * Is called while the thread is running. Waits for a given time and checks after the waiting 
	 * time if there are information packages in the session that have been expired. If there are any
	 * expired information packages found, they are removed from the session.
	 */
	@Override
	public void run() {
		while(running) {
			try {
				sleep(waitingTimeInSeconds * ONE_SECOND_IN_MILLISECONDS);
				if(queueTimer.isNotEmpty()) {
					final Uuid uuid = queueTimer.removeIfNecessary();
					removeFromSession(uuid);
				}
			} catch (InterruptedException e) {
				running = false;
			}
		}
	}
	
	/**
	 * Determines an information package with the help of its uuid.
	 * @param uuid The uuid of the information package we are interested in. 
	 * @return The found information package.
	 */
	public BasicInformationPackage getInformationPackage(final Uuid uuid) {
		return informationPackageQueue.get(uuid);
	}
	
	/**
	 * Mainly for testing purposes. The time a package has to be unused before it is
	 * deleted from the session can be changed.
	 * @param waitingTimeInSeconds The time a package has to be unused before it is deleted
	 * from the session.
	 */
	void changeTimeout(final int seconds) {
		queueTimer.setTimeOut(seconds);
	}
	
	/**
	 * Mainly for testing purposes. The time the threads waits before it looks for expired
	 * information packages in the session.
	 * @param waitingTimeInSeconds The time the thread waits.
	 */
	void changeWaitingTime(final int waitingTimeInSeconds) {
		this.waitingTimeInSeconds = waitingTimeInSeconds;
	}
	
	/**
	 * Checks if an information package exists in the thread.
	 * @param uuidOfInformationPackage The uuid of the information package we want to check. 
	 * @return True if the information packag exists, false otherwise. 
	 */
	public boolean containsInformationPackage(final Uuid uuidOfInformationPackage) {
		return informationPackageQueue.contains(uuidOfInformationPackage);
	}
}