package de.feu.kdmp4.packagingtoolkit.server.session.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;

/**
 * An interface for a queue that contains some information packages.
 * @author Christopher Olbertz
 *
 */
public interface InformationPackageQueue {
	/**
	 * Checks if the queue contains an information package with a certain uuid.
	 * @param uuid The uuid of the package to look for.
	 * @return True, if there is an information package with uuid, false otherwise.
	 */
	public abstract boolean contains(final Uuid uuid);
	/**
	 * Gets an information package with a certain uuid;
	 * @param uuid The uuid to look for.
	 * @return The found information package or null, if there is no information
	 * package with this uuid.
	 */
	public abstract BasicInformationPackage get(final Uuid uuid);
	/**
	 * Gets the number of the elements in the queue.
	 * @return The size of the queue.
	 */
	public abstract int getSize();
	/**
	 * Puts a information package in the queue.
	 * @param informationPackage The information package to put in the queue.
	 */
	public abstract void put(final BasicInformationPackage informationPackage);
	/**
	 * Removes an information package from the queue.
	 * @param uuid The uuid of the information package to remove.
	 */
	public abstract void remove(final Uuid uuid);
}
