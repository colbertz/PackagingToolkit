/**
 * Contains the classes that contain the access methods for other modules that want to access
 * the module Session.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.session.facade;