package de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OaisEntity;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

/**
 * The super interface of all information packages. 
 * @author Christopher Olbertz
 *
 */
public interface BasicInformationPackage extends OaisEntity {
	/**
	 * Returns uuid of the information package that identifies the information package
	 * uniquely.
	 * @return The uuid of the information package.
	 */
	Uuid getUuid();
	/**
	 * Returns the title of the information package.
	 * @return The title of the information package.
	 */
	String getTitle();
	/**
	 * Returns the package type of the information package.
	 * @return The package type of the information package.
	 */
	PackageType getPackageType();
	/**
	 * Returns the serialization format of the information package.
	 * @return The serialization format of the information package.
	 */
	SerializationFormat getSerializationFormat();
	/**
	 * Adds several individuals to the information package. The individuals contain the data the user has
	 * entered.
	 * @param individualList The collection with the individuals that should be assigned to the information
	 * package.
	 */
	void addOntologyIndividuals(final OntologyIndividualCollection individualList);
	/**
	 * Creates a collection with all individuals that are assigned to the information package.
	 * @return All individuals that are assigned to the information package.
	 */
	OntologyIndividualCollection getOntologyIndividualList();
	/**
	 * Adds an individual to the information package. 
	 * @param individual The individual that should be assigned to the information package.
	 */
	void addOntologyIndividual(final OntologyIndividual individual);
	/**
	 * Adds a new reference to the information package. The references can point to a remote file or to
	 * a local file that is saved in the storage directory.
	 * @param reference The reference that should be added.
	 */
	void addReference(final Reference reference);
	/**
	 * Gets the references of the information package. The references can point to a remote file or to
	 * a local file that is saved in the storage directory.
	 * @return A collection with the references of this information package.
	 */
	ReferenceCollection getReferences();
	/**
	 * Converts the information package in statements that can be saved in the triple store.
	 * @return The information package in a representation as triple statements.
	 */
	TripleStatementCollection toTripleStatements();
	/**
	 * Sets the serialization format of the information package.
	 * @param serializationFormat The serialization format of the information package.
	 */
	void setSerializationFormat(final SerializationFormat serializationFormat);
	/**
	 * Replaces an individual in this information package by another one. The uuid of the 
	 * replaced individual and the new one have to be equal. 
	 * @param ontologyIndividual The individual that replaces the individual with the same uuid.
	 */
	void replaceOntologyIndividual(final OntologyIndividual ontologyIndividual);
	/**
	 * Creates triple statements with the information contained in this information package. At the 
	 * moment this is the information about the references this information package has.
	 * @return A list with the statements describing the references of this information package.
	 */
	TripleStatementCollection toStatements();
	/**
	 * Determines if the information package contains any references to local files or remote files.
	 * @return True if the information package contains any references, false otherwise.
	 */
	boolean containsReferences();
	/**
	 * Adds properties that contains meta data values to the information package.
	 * @param tripleStatements The statements that describe the meta data of this file.
	 * @param uuidOfFile The uuid of the file the meta data belong to. 
	 */
	void addMetadataProperty(final TripleStatementCollection tripleStatements, final Uuid uuidOfFile);
	/**
	 * Determines if the creation date of the information package has been set. If the creation date is null, the information
	 * package has not been saved in the database yet.
	 * @return True if the information package has a creation date, false otherwise.
	 */
	boolean hasCreationDate();
	LocalDateTime getCreationDate();
	LocalDateTime getLastModificationDate();
	/**
	 * Checks if an individual is already assigned to this information package.
	 * @param individual The individual we want to check.
	 * @return True if individual is contained false otherwise.
	 */
	boolean containsIndividual(OntologyIndividual individual);
	/**
	 * Removes an individual from the information package. 
	 * @param individual The individual we want to remove. 
	 */
	void removeIndividual(OntologyIndividual individual);
	/**
	 * Adds an uuid of an individual to the information package. This individual is saved
	 * in the triple store and the methods adds a reference to this information package.
	 * @param uuidOfIndividual The uuid of the individual that should be assigned to the
	 * information package. 
	 */
	void addAssignedUuid(Uuid uuidOfIndividual);
	/**
	 * Removes a reference to an individual that is saved in the triple store from
	 * the information package.
	 * @param uuidOfIndividual The uuid of the individuals that should not longer be
	 * assigned to this information package.
	 */
	void removeAssignedUuid(Uuid uuidOfIndividual);
	/**
	 * Creates a list that contains statements for the individuals that are assigned
	 * to this information package. The statements are aggregatedBy-statements with the
	 * uuid of the individual as subject and the uuid of the information package as object. 
	 * @return The statements that describe the assigned ontology individuals. 
	 */
	TripleStatementCollection getStatementsOfAssignedIndividuals();
	/**
	 * Assigns some individuals from a taxonomy to the information package.
	 * @param irisOfSelectedTaxonomyIndividuals The iris of the individuals that should be
	 * assigned to the information package.
	 */
	void addTaxonomyIndividuals(IriCollection irisOfSelectedTaxonomyIndividuals);
	/**
	 * Creates an iri for the information package. The namespace of the iri is the default namespace
	 * for the PackagingToolkit and the local name is the uuid of the information package.
	 * @return
	 */
	Iri getIri();
	/**
	 * Creates statements that describe the assignement of taxonomy individuals to this information
	 * package.
	 * @return The statements created. 
	 */
	TripleStatementCollection getStatementsOfTaxonomyIndividuals();
}
