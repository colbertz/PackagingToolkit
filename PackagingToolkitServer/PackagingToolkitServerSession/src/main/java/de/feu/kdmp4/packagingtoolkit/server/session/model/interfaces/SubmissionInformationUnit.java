package de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;

public interface SubmissionInformationUnit extends BasicInformationPackage{
	/**
	 * Determines a list with all references of this SIU.
	 */
	ReferenceCollection getReferences();
}
