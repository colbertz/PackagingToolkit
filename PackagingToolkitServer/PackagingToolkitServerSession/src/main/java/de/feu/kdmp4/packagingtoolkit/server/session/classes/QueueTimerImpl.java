package de.feu.kdmp4.packagingtoolkit.server.session.classes;

import java.util.Date;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.session.interfaces.AbstractQueueTimer;

/**
 * This implementation uses a TreeMap for realizing the queue
 * timer. 
 * @author Christopher Olbertz
 *
 */
@Component
public class QueueTimerImpl extends AbstractQueueTimer {
	/**
	 * A TreeMap that represents the queue time. 
	 */
	private TreeMap<Uuid, Date> queueTimer = new TreeMap<>();
	
	/**
	 * Creates a new object.
	 */
	public QueueTimerImpl() {
		super();
	}
	
	/**
	 * Constructs a new object.
	 * @param secondsToRemove The number of seconds the queue should wait before
	 * it removes unused information packages.
	 */
	public QueueTimerImpl(final int secondsToRemove) {
		super(secondsToRemove);
	}
	
	/**
	 * Mainly for testing purposes. The time a package has to be unused before it is
	 * deleted from the session can be changed.
	 * @param waitingTimeInSeconds The time a package has to be unused before it is deleted
	 * from the session.
	 */
	public void changeTimeOut(final int seconds) {
		setTimeOut(seconds);
	}
	
	@Override
	public boolean isEmpty() {
		return queueTimer.isEmpty();
	}

	@Override
	public boolean isNotEmpty() {
		return !queueTimer.isEmpty();
	}
	
	@Override
	public void remove(final Uuid uuid) {	
		queueTimer.remove(uuid);
	}

	@Override
	public Uuid getFirstKey() {
		try {
			return queueTimer.firstKey();
		} catch (NoSuchElementException ex) {
			return null;
		}
	}

	@Override
	public Date getFirstValue() {
		final Entry<Uuid, Date> firstEntry = queueTimer.firstEntry();
		if (firstEntry == null) {
			return null;
		} else {
			return firstEntry.getValue();
		}
	}
	
	@Override
	public int getSize() {
		return queueTimer.size();
	}

	@Override
	public void put(final Uuid uuid, final Date date) {
		queueTimer.put(uuid, date);
	}
}
