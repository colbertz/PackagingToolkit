package de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces;

import java.util.Map.Entry;
import java.util.Set;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;

/**
 * This map contains the uuids of the file that are assigned to an information package and the 
 * statements that describe their meta data in the triple store.
 * @author Christopher Olbertz
 *
 */
public interface UuidsOfFilesAndTheirStatements {
	/**
	 * Adds an element to the map.
	 * @param uuidOfFile The uuid of the file whose meta data we want to save.
	 * @param tripleStatements The statements that represent the meta data of this file.
	 */
	void addMetadata(final Uuid uuidOfFile, final TripleStatementCollection tripleStatements);
	/**
	 * Gets the properties that are assigned to a certain file.
	 * @param uuidOfFile The uuid of the file whose meta data we want to receive.
	 * @return The statements that represent the meta data of the file with the uuid
	 * uuidOfFile.
	 */
	TripleStatementCollection getStatementsOfFile(final Uuid uuidOfFile);
	/**
	 * Creates a set with all entries in the map. 
	 * @return Contains all entries in the map. The set is unmodifiable.
	 */
	Set<Entry<Uuid, TripleStatementCollection>> getUuidsAndProperties();
	/**
	 * Writes all uuids contained in the map in a set.
	 * @return A set with all uuids.
	 */
	Set<Uuid> getUuidsOfFiles();
}
