package de.feu.kdmp4.packagingtoolkit.server.session.model;

import static de.feu.kdmp4.packagingtoolkit.server.utils.ServerLogUtil.logInfoReferenceAdded;

import java.time.LocalDateTime;
import java.util.Map.Entry;
import java.util.Set;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage.InformationPackageException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.OaisEntityImpl;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.OntologyClassIndividualsHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.session.factories.InformationPackageFactory;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.BasicInformationPackage;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.UuidsOfFilesAndTheirStatements;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * An abstract base class for information packages with the elements that all 
 * information packages have. 
 * @author Christopher Olbertz
 *
 */
public abstract class BasicInformationPackageImpl extends OaisEntityImpl 
												  implements BasicInformationPackage {
	/**
	 * Date and time the information package was created.
	 */
	private LocalDateTime creationDate;
	/**
	 * Contains the individuals and their classes that are assigned to this information package.
	 */
	private OntologyClassIndividualsHashMapImpl ontologyClassIndividuals;
	/**
	 * Date and time the information package was modified the last time. 
	 */
	private LocalDateTime lastModificationDate;
	/**
	 * The serialization format of this information package.
	 */
	private SerializationFormat serializationFormat;
	/**
	 * Contains the uuids of the files that are assigned to this information package
	 * and the meta data values of these files as properties.
	 */
	private UuidsOfFilesAndTheirStatements uuidsOfFilesAndTheirMetadataMap;
	/**
	 * The title of this information package.
	 */
	private String title;
	/**
	 * Contains the uuids of the individuals that are assigned to this information package. These
	 * individuals already are saved in the triple store and therefore it is only necessary to
	 * 
	 */
	private UuidList uuidsOfAssignedIndividuals;
	/**
	 * Contains the iris of the individuals in the taxonomy that have been selected by the 
	 * user and assigned to the information package. 
	 */
	private IriCollection irisOfSelectedTaxonomyIndividuals;
	
	/**
	 * A constructor that does not get an uuid for the information package. The
	 * uuid is created automatically.
	 * @param title The title of the information package.
	 * @param virtual True, if the information package is virtual, false otherwise. 
	 * @param creationDate The date and time the information package has been created.
	 * @param lastModificationDate The date and time the information package was modified the last time.
	 * @throws InformationPackageTitleMayNotBeNullException Is thrown if the title
	 * parameter is empty.
	 */
	public BasicInformationPackageImpl(final String title, final LocalDateTime creationDate, final LocalDateTime lastModificationDate) {
		this(title, null, creationDate, lastModificationDate); 
	}
	
	/**
	 * Constructs an information package.
	 * @param title The title of the information package. May not be empty.
	 * @param virtual True, if the information package is virtual, false 
	 * otherwise.
	 * @param uuid The uuid of the information package. May not be null.
	 * @param creationDate The date and time the information package has been created.
	 * @param lastModificationDate The date and time the information package was modified the last time.
	 * @throws InformationPackageTitleMayNotBeNullException Is thrown if the title
	 * parameter is empty.
	 */
	public BasicInformationPackageImpl(final String title, final Uuid uuid, final LocalDateTime creationDate, final LocalDateTime lastModificationDate) {
		super(uuid);
		if (StringValidator.isNotNullOrEmpty(title)) {
			this.title = title;
		} else {
			InformationPackageException exception = InformationPackageException.titleMayNotBeEmptyException();
			throw exception;
		}
		ontologyClassIndividuals = new OntologyClassIndividualsHashMapImpl();
		this.creationDate = creationDate;
		this.lastModificationDate = lastModificationDate;
		uuidsOfAssignedIndividuals = PackagingToolkitModelFactory.createEmptyUuidList();
		uuidsOfFilesAndTheirMetadataMap = InformationPackageFactory.createEmptyUuidsOfFilesAndTheirMetadataMap();
		irisOfSelectedTaxonomyIndividuals = ServerModelFactory.createEmptyIriCollection();
	}
	
	@Override
	public void addTaxonomyIndividuals(final IriCollection irisOfSelectedTaxonomyIndividuals) {
		for (int i = 0; i < irisOfSelectedTaxonomyIndividuals.getIrisCount(); i++) {
			final Iri iri = irisOfSelectedTaxonomyIndividuals.getIri(i);
			
			if (this.irisOfSelectedTaxonomyIndividuals.doesNotContainIri(iri)) {
				this.irisOfSelectedTaxonomyIndividuals.addIri(iri);
			}
		}
	}
	
	@Override
	public boolean containsIndividual(final OntologyIndividual individual) {
		if (ontologyClassIndividuals.containsIndividual(individual)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void removeIndividual(final OntologyIndividual individual) {
		final String className = individual.getOntologyClass().getFullName();
		final OntologyIndividualCollection individualList = ontologyClassIndividuals.getOntologyIndividuals(className);
		individualList.removeIndividual(individual);
	}
	
	@Override
	public void addMetadataProperty(final TripleStatementCollection tripleStatements, final Uuid uuidOfFile) {
		uuidsOfFilesAndTheirMetadataMap.addMetadata(uuidOfFile, tripleStatements);
	}
	
	/**
	 * Should be called by the overriding methods in the sub classes for ensuring that the message is
	 * logged in every case. This method does not add the reference to the information package but only
	 * starts the logging.
	 * @param reference The reference that is added to the information package and used for the log message.
	 */
	public void addReference(final Reference reference) {
		logInfoReferenceAdded(getUuid(), reference);
	}

	@Override
	public void addOntologyIndividuals(final OntologyIndividualCollection individualList) {
		for (int i = 0; i< individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
			ontologyClassIndividuals.addOrUpdateIndividual(ontologyIndividual);
		}
	}
	
	@Override
	public void addOntologyIndividual(final OntologyIndividual individual) {
		if (individual != null) {
			ontologyClassIndividuals.addOrUpdateIndividual(individual);
			individual.setUuidOfInformationPackage(getUuid());
		}
	}
	
	@Override
	public void replaceOntologyIndividual(final OntologyIndividual ontologyIndividual) {
		final String className = ontologyIndividual.getOntologyClass().getFullName();
		final OntologyIndividualCollection individualList = ontologyClassIndividuals.getOntologyIndividuals(className);
		individualList.replaceIndividual(ontologyIndividual);
	}
	
	@Override
	public boolean hasCreationDate() {
		if (creationDate == null) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Returns the type of the information package. Every subclass has to override this
	 * method and to return its package type. This method is used if querying the hierarchy
	 * with instance of is not suitable. For example, this method is used for creation of the names
	 * of the zip files. 
	 * @return The type of the information package.
	 */
	@Override
	public abstract PackageType getPackageType();
	
	@Override
	public abstract TripleStatementCollection toStatements();
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public SerializationFormat getSerializationFormat() {
		return serializationFormat;
	}
	
	@Override
	public void setSerializationFormat(final SerializationFormat serializationFormat) {
		this.serializationFormat = serializationFormat;
	}

	@Override
	public OntologyIndividualCollection getOntologyIndividualList() {
		OntologyIndividualCollection  ontologyIndividualList = ontologyClassIndividuals.toOntologyIndividualList();
		return ontologyIndividualList;
	}
	
	@Override
	// DELETE_ME Kann wahrscheinlich raus!
	public TripleStatementCollection toTripleStatements() {
		TripleStatementCollection tripleStatements = getOntologyIndividualList().toStatements();
		
		for (int i = 0; i < uuidsOfAssignedIndividuals.getSize(); i++) {
			final Uuid uuid = uuidsOfAssignedIndividuals.getUuidAt(i);
			final TripleSubject individual = RdfElementFactory.createTripleSubject(uuid.toString());
			final TripleObject uuidOfInformationPackage = RdfElementFactory.createTripleObject(uuid);
			final TripleStatement tripleStatement = RdfElementFactory.createAggregatedByStatement(individual, uuidOfInformationPackage);
			tripleStatements.addTripleStatement(tripleStatement);
		}
		
		tripleStatements.addStatements(getStatementsOfAllFiles());
		return tripleStatements;
	}
	
	@Override
	public TripleStatementCollection getStatementsOfAssignedIndividuals() {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		for (int i = 0; i < uuidsOfAssignedIndividuals.getSize(); i++) {
			final Uuid uuid = uuidsOfAssignedIndividuals.getUuidAt(i);
			final TripleSubject individual = RdfElementFactory.createTripleSubject(uuid.toString());
			final TripleObject uuidOfInformationPackage = RdfElementFactory.createTripleObject(getUuid());
			final TripleStatement tripleStatement = RdfElementFactory.createAggregatedByStatement(individual, uuidOfInformationPackage);
			tripleStatements.addTripleStatement(tripleStatement);
		}
		
		return tripleStatements;
	}
	
	@Override
	public TripleStatementCollection getStatementsOfTaxonomyIndividuals() {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		for (int i = 0; i < irisOfSelectedTaxonomyIndividuals.getIrisCount(); i++) {
			final Iri iriOfTaxonomyIndividual = irisOfSelectedTaxonomyIndividuals.getIri(i);
			final TripleStatement tripleStatement = RdfElementFactory.createTaxonomyIndividualAssignedStatement(
					iriOfTaxonomyIndividual, getIri());
			tripleStatements.addTripleStatement(tripleStatement);
		}
		
		return tripleStatements;
	}
	
	/**
	 * Creates a collection with all statements that describe the files of this information package.
	 * @return A collection with all statements that describe the files of this information package.
	 */
	protected TripleStatementCollection getStatementsOfAllFiles() {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		final Set<Entry<Uuid, TripleStatementCollection>> metadataSet = uuidsOfFilesAndTheirMetadataMap.getUuidsAndProperties();
		
		for (final Entry<Uuid, TripleStatementCollection> entry: metadataSet) {
			tripleStatements.addStatements(entry.getValue());
		}
		
		return tripleStatements;
	}	
	
	@Override
	public LocalDateTime getCreationDate() {
		return creationDate;
	}
	
	@Override
	public LocalDateTime getLastModificationDate() {
		return lastModificationDate;
	}
	
	@Override
	public void addAssignedUuid(final Uuid uuidOfIndividual) {
		if (uuidsOfAssignedIndividuals.notContainsUuid(uuidOfIndividual)) {
			uuidsOfAssignedIndividuals.addUuidToList(uuidOfIndividual);
		}
	}
	
	@Override
	public void removeAssignedUuid(final Uuid uuidOfIndividual) {
		if (uuidsOfAssignedIndividuals.containsUuid(uuidOfIndividual)) {
			uuidsOfAssignedIndividuals.removeUuid(uuidOfIndividual);
		}
		ontologyClassIndividuals.removeIndividualByUuid(uuidOfIndividual);
	}
	
	@Override
	public Iri getIri() {
		final Namespace namespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
		final LocalName localName = ServerModelFactory.createLocalName(getUuid().toString());
		final Iri iriOfInformationPackage = ServerModelFactory.createIri(namespace, localName);
		return iriOfInformationPackage;
	}
}