package de.feu.kdmp4.packagingtoolkit.server.session.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.session.model.interfaces.SubmissionInformationUnit;

/**
 * Represents an archival information unit that can contain multiple digital objects.
 * @author Christopher Olbertz
 *
 */
public class SubmissionInformationUnitImpl extends BasicInformationPackageImpl 
									 implements SubmissionInformationUnit {
	private Map<Uuid, Reference> references;
	
	public SubmissionInformationUnitImpl(final String title, final Uuid uuid, final LocalDateTime creationDate, final LocalDateTime lastModificationDate) {
		super(title, uuid, creationDate, lastModificationDate);
		references = new HashMap<>();
	}

	public SubmissionInformationUnitImpl(final String title, final LocalDateTime creationDate, final LocalDateTime lastModificationDate) {
		super(title, creationDate, lastModificationDate);
		references = new HashMap<>();
	}
	
	@Override
	public void addReference(final Reference reference) {
		super.addReference(reference);
		references.put(reference.getUuidOfFile(), reference);
	}
	
	@Override
	public PackageType getPackageType() {
		return PackageType.SIU;
	}
	
	@Override
	public ReferenceCollection getReferences() {
		final Collection<Reference> referenceCollection = references.values();
		final ReferenceCollection referenceList = ServerModelFactory.createReferenceList(referenceCollection);
		return referenceList;
	}
	
	@Override
	public TripleStatementCollection toTripleStatements() {
		final TripleStatementCollection tripleStatements = super.toTripleStatements();
		final Collection<Reference> referenceCollection = references.values();
		
		for (final Reference reference: referenceCollection) {
			final TripleStatement referenceAsStatement = reference.asTripleStatement(getUuid());
			tripleStatements.addTripleStatement(referenceAsStatement);
		}
		
		return tripleStatements;
	}
	
	@Override
	public TripleStatementCollection toStatements() {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		final Collection<Reference> referenceCollection = references.values();
		
		for (final Reference reference: referenceCollection) {
			if (reference.isLocalFileReference()) {
				final Uuid uuidOfFile = reference.getUuidOfFile();
				final TripleStatement tripleStatement = RdfElementFactory.createStorageFileStatement(getUuid(), uuidOfFile);
				tripleStatements.addTripleStatement(tripleStatement);
			} else {
				final String urlOfFile = reference.getUrl();
				final TripleStatement tripleStatement = RdfElementFactory.createRemoteFileStatement(getUuid(), urlOfFile);
				tripleStatements.addTripleStatement(tripleStatement);
			}
		}
		
		return tripleStatements;
	}

	@Override
	public boolean containsReferences() {
		if (references == null) {
			return false;
		} else if (references.size() == 0) {
			return false;
		} else {
			return true;
		}
	}
}
