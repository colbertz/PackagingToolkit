package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.feu.kdmp4.packagingtoolkit.server.configuration.FactoryConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.MockedOperationsConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@ActiveProfiles("propertiesTest")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
@Category(SpringTests.class)
public class OntologyShortPropertyTest implements ApplicationContextAware{
	private ApplicationContext applicationContext;
	private static final String PROPERTY_ID = "datatypeProperty#byteProperty";
	private static final String PROPERTY_LABEL = "byteProperty";
	private static final String PROPERTY_RANGE = "byte";
	
	private OntologyShortProperty ontologyShortProperty;
	
	private ExpectedException thrownException = ExpectedException.none();
	
	@Before
	public void setUp() {
		ontologyShortProperty = new OntologyShortPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsShort() {
		assertFalse(ontologyShortProperty.isBooleanProperty());
		assertFalse(ontologyShortProperty.isBooleanProperty());
		assertFalse(ontologyShortProperty.isDateProperty());
		assertFalse(ontologyShortProperty.isDateTimeProperty());
		assertFalse(ontologyShortProperty.isDoubleProperty());
		assertFalse(ontologyShortProperty.isDurationProperty());
		assertFalse(ontologyShortProperty.isFloatProperty());
		assertFalse(ontologyShortProperty.isIntegerProperty());
		assertFalse(ontologyShortProperty.isLongProperty());
		assertTrue(ontologyShortProperty.isShortProperty());
		assertFalse(ontologyShortProperty.isStringProperty());
		assertFalse(ontologyShortProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		ontologyShortProperty.setPropertyValue(5);
		int value = ontologyShortProperty.getPropertyValue();
		assertEquals(5, value);
	}

	@Test
	public void testUnsignedShortValue5() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(5);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedShortValueMaxValueMinus1() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(Short.MAX_VALUE * 2);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedShortValueMaxValue() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(Short.MAX_VALUE * 2 + 1);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testUnsignedShortValue0() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(0);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testUnsignedShortValueNegative() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(-1);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testUnsignedShortMaxValuePlus2() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(Short.MAX_VALUE * 2 + 2);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}

	@Test
	public void testSignedShortMaxValue() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Integer valueAsInteger = new Integer(Short.MAX_VALUE);
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(valueAsInteger);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testSignedShortMaxValuePlus1() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Integer valueAsInteger = new Integer(Short.MAX_VALUE + 1) ;
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(valueAsInteger);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testSignedShortValue0() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Integer valueAsInteger = new Integer(0);
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(valueAsInteger);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testSignedShortMinValue() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Integer valueAsInteger = new Integer(Short.MIN_VALUE);
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(valueAsInteger);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testSignedShortValueMinValueMinus1() {
		OntologyShortProperty unsigned = new OntologyShortPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Integer valueAsInteger = new Integer(Short.MIN_VALUE - 1);
		RestrictionValue<Integer> value = new RestrictionValue<Integer>(valueAsInteger);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Rule
	public ExpectedException getThrownException() {
		return thrownException;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
