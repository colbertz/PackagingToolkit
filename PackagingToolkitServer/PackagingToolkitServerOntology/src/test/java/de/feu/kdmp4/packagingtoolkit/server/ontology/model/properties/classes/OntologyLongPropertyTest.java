package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.configuration.FactoryConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.MockedOperationsConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@ActiveProfiles("propertiesTest")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
@Category(SpringTests.class)
public class OntologyLongPropertyTest implements ApplicationContextAware{
	private ApplicationContext applicationContext;
	private static final String PROPERTY_ID = "datatypeProperty#longProperty";
	private static final String PROPERTY_LABEL = "longProperty";
	private static final String PROPERTY_RANGE = "long";
	
	private OntologyLongProperty ontologyLongProperty;
	
	private ExpectedException thrownException = ExpectedException.none();
	
	@Before
	public void setUp() {
		ontologyLongProperty = new OntologyLongPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsLong() {
		assertFalse(ontologyLongProperty.isBooleanProperty());
		assertFalse(ontologyLongProperty.isByteProperty());
		assertFalse(ontologyLongProperty.isDateProperty());
		assertFalse(ontologyLongProperty.isDateTimeProperty());
		assertFalse(ontologyLongProperty.isDoubleProperty());
		assertFalse(ontologyLongProperty.isDurationProperty());
		assertFalse(ontologyLongProperty.isFloatProperty());
		assertFalse(ontologyLongProperty.isIntegerProperty());
		assertTrue(ontologyLongProperty.isLongProperty());
		assertFalse(ontologyLongProperty.isShortProperty());
		assertFalse(ontologyLongProperty.isStringProperty());
		assertFalse(ontologyLongProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		BigInteger expectedValue = BigInteger.valueOf(5);
		ontologyLongProperty.setPropertyValue(expectedValue);
		BigInteger value = ontologyLongProperty.getPropertyValue();
		assertEquals(expectedValue, value);
	}

	@Test
	public void testUnsignedLongValue5() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(5);
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedLongMaxValueMinus1() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);

		BigInteger bigIntegerValue = BigInteger.valueOf(Long.MAX_VALUE).multiply(BigInteger.valueOf(2));
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedLongMaxValue() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(Long.MAX_VALUE);
		bigIntegerValue.multiply(BigInteger.valueOf(2));
		bigIntegerValue = bigIntegerValue.add(BigInteger.valueOf(1));
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testUnsignedLongValue0() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(0);
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testUnsignedLongValueNegative() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(-1);
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testUnsignedLongMaxValuePlus2() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(Long.MAX_VALUE);
		bigIntegerValue = bigIntegerValue.multiply(BigInteger.valueOf(2));
		bigIntegerValue = bigIntegerValue.add(BigInteger.valueOf(2));
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}

	@Test
	public void testSignedLongMaxValue() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(Long.MAX_VALUE);
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testSignedLongValuePlus1() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.valueOf(1));
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testSignedLongValue0() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(0);
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testSignedLongValueNegative() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(Long.MIN_VALUE);
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testSignedLongValueMinValueMinus1() {
		OntologyLongProperty unsigned = new OntologyLongPropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		BigInteger bigIntegerValue = BigInteger.valueOf(Long.MIN_VALUE ).subtract(BigInteger.valueOf(1));
		RestrictionValue<BigInteger> value = new RestrictionValue<BigInteger>(bigIntegerValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Rule
	public ExpectedException getThrownException() {
		return thrownException;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
