/**
 * 
 */
/**
 * @author christopher
 * Contains classes that extends the original classes and provide some changes for the tests. In the most cases these
 * changes are related to initialization of some attributes that are given by Spring in the real application. These classes
 * may not change the behavior of the original classes.
 */
package de.feu.kdmp4.packagingtoolkit.server.ontology.override;