package de.feu.kdmp4.packagingtoolkit.server.ontology.testapi;

import de.feu.kdmp4.packagingtoolit.server.ontology.model.interfaces.TaxonomyIndividualMap;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Creates some testdate for the tests of the taxonomy and the related classes.
 * @author Christopher Olbertz
 *
 */
public class TaxonomyTestApi {
	/**
	 * Creates an individual with a random uuid as localname.
	 * @return The individual.
	 */
	public static TaxonomyIndividual createTaxonomyIndividual() {
		final Iri iriOfIndividual = new IriImpl(new Uuid());
		final TaxonomyIndividual taxonomyIndividual = new TaxonomyIndividualImpl(iriOfIndividual, iriOfIndividual.getLocalName().toString(), iriOfIndividual.getNamespace(),
				iriOfIndividual);
		return taxonomyIndividual;
	}
	
	/**
	 * Adds three individuals as narrowers to the individual.
	 * @param taxonomyIndividual The individual we want to add three narrowers to.
	 * @return The second of the three narrowers.
	 */
	public static TaxonomyIndividual addThreeNarrowersToTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual ) {
		final TaxonomyIndividual narrower1 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower1);
		
		final TaxonomyIndividual narrower2 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower2);
		
		final TaxonomyIndividual narrower3 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower3);
		
		return narrower2;
	}
	
	/**
	 * Adds one individual as narrower to the individual.
	 * @param taxonomyIndividual The individual we want to add a narrower to.
	 * @return The narrower that has been added.
	 */
	public static TaxonomyIndividual addOneNarrowerToTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual ) {
		final TaxonomyIndividual narrower = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower);
		
		return narrower;
	}
	
	/**
	 * Adds three individuals to the map.
	 * @param taxonomyIndividualMap The map we want to add three individuals to.
	 * @return The second of the three individuals.
	 */
	public static TaxonomyIndividual addThreeIndividualsToTaxonomyIndividualMap(final TaxonomyIndividualMap 
			taxonomyIndividualMap) {
		final TaxonomyIndividual taxonomyIndividual1 = createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual2 = createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual3 = createTaxonomyIndividual();
		taxonomyIndividualMap.addTaxonomyIndividual(taxonomyIndividual1);
		taxonomyIndividualMap.addTaxonomyIndividual(taxonomyIndividual2);
		taxonomyIndividualMap.addTaxonomyIndividual(taxonomyIndividual3);
		
		return taxonomyIndividual2;
	}
	
	/**
	 * Creates a taxonomy with only a root individual.
	 * @return The new taxonomy.
	 */
	public static Taxonomy createTaxonomy() {
		final TaxonomyIndividual rootIndividual = createTaxonomyIndividual();
		final String title = rootIndividual.getIri().getLocalName().toString();
		final Namespace namespace = ServerModelFactory.createNamespace(title);
		final Taxonomy taxonomy = new TaxonomyImpl(rootIndividual,  title,  namespace);
		return taxonomy;
	}
	
	/**
	 * Creates a collection with three taxonomies for the tests.
	 * @return The second taxonomy in the collection.
	 */
	public static Taxonomy createTaxonomyCollectionWithThreeTaxonomies(final TaxonomyCollection taxonomyCollection) {
		final Taxonomy taxonomy1 = createTaxonomy();
		final Taxonomy taxonomy2 = createTaxonomy();
		final Taxonomy taxonomy3 = createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy1);
		taxonomyCollection.addTaxonomy(taxonomy2);
		taxonomyCollection.addTaxonomy(taxonomy3);
		
		return taxonomy2;
	}

	/**
	 * Creates a collection with three taxonomy individuals for the tests.
	 * @return The second taxonomy individual in the collection.
	 */
	public static TaxonomyIndividual createTaxonomyIndividualCollectionWithThreeIndividuals(
			TaxonomyIndividualCollection taxonomyIndividualCollection) {
		final Namespace namespace = new NamespaceImpl("http://mynamespace");
		final TaxonomyIndividual taxonomyIndividual1 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label1", namespace,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual2 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label2", namespace,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual3 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label3", namespace,
				new IriImpl(new Uuid()));
		
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual1);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual2);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual3);
		
		return taxonomyIndividual2;
	}
	
	/**
	 * Creates a collection with ten taxonomy individuals for the tests.
	 * @return The second taxonomy individual in the collection.
	 */
	public static TaxonomyIndividual createTaxonomyIndividualCollectionWithTenIndividuals(
			TaxonomyIndividualCollection taxonomyIndividualCollection) {
		final Namespace namespace1 = new NamespaceImpl("http://mynamespace");
		final Namespace namespace2 = new NamespaceImpl("http://the-other-namespace");
		final TaxonomyIndividual taxonomyIndividual1 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label1", namespace2,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual2 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label2", namespace1,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual3 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label3", namespace2,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual4 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label4", namespace2,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual5 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label5", namespace2,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual6 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label6", namespace1,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual7 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label7", namespace2,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual8 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label8", namespace1,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual9 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label9", namespace1,
				new IriImpl(new Uuid()));
		final TaxonomyIndividual taxonomyIndividual10 = new TaxonomyIndividualImpl(new IriImpl(new Uuid()), "label10", namespace2,
				new IriImpl(new Uuid()));
		
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual1);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual2);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual3);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual4);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual5);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual6);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual7);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual8);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual9);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual10);
		
		return taxonomyIndividual4;
	}
	
	/**
	 * Compares the values of a taxonomy individual with the values of a response object.
	 * @param taxonomyIndividual The individual that contains the value the response has to contain.
	 * @param taxonomyIndividualResponse The response object we want to check.
	 * @return True if the both objects contain the same values, false otherwise.
	 */
	public static boolean areValuesEqual(final TaxonomyIndividual taxonomyIndividual, final TaxonomyIndividualResponse taxonomyIndividualResponse) {
		final String expectedIri = taxonomyIndividual.getIri().toString();
		final String actualIri = taxonomyIndividualResponse.getIri();
		if (StringUtils.areStringsUnequal(expectedIri, actualIri)) {
			return false;
		}
		
		final String expectedPreferredLabel = taxonomyIndividual.getPreferredLabel();
		final String actualPreferredLabel = taxonomyIndividualResponse.getPreferredLabel();
		if (StringUtils.areStringsUnequal(expectedPreferredLabel, actualPreferredLabel)) {
			return false;
		}
		
		final int expectedNarrowerCount = taxonomyIndividual.getNarrowerCount();
		final int actualNarrowerCount = taxonomyIndividualResponse.getNarrowers().getNarrowersCount(); 
		if (expectedNarrowerCount != actualNarrowerCount) {
			return false;
		}
		
		for (int i = 0; i < actualNarrowerCount; i++) {
			final TaxonomyIndividual narrower = taxonomyIndividual.getNextNarrower().get();
			final TaxonomyIndividualResponse narrowerResponse = taxonomyIndividualResponse.getNarrowers().getIndividuals().get(i);
			if (areValuesEqual(narrower, narrowerResponse) == false) {
				return false;
			}
		}
		
		return true;
	}
}
