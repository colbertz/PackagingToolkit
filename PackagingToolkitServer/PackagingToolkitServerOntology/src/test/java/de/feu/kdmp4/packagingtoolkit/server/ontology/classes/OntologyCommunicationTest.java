package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.glassfish.jersey.client.ClientResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.server.ontology.TestConfiguration;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {TestConfiguration.class})
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = {TestConfiguration.class})
@EnableAutoConfiguration
@ComponentScan
@Category(SpringTests.class)
public class OntologyCommunicationTest {
	private static final String PATH_ONTOLOGY = "src" + File.separator + "main" + 
			File.separator + "resources" + File.separator + "KDMP4-OAISrdf.owl";

	
	private File baseOntologyFile;
	@LocalServerPort
    private int port;
	private File configFile;
    private TestRestTemplate restTemplate = new TestRestTemplate();
	
	/**
	 * Writes a configuration file with test data because they are needed for the tests.
	 */
	@Before
	public void setUp() {
		baseOntologyFile = new File(PATH_ONTOLOGY);
	}
	
	/*private ServerConfigurationData readTestConfigurationFile(File propertiesFile) {
		try (BufferedInputStream bufferedInputStream = new BufferedInputStream(
				new FileInputStream(propertiesFile))) {
			Properties properties = new Properties();
			properties.load(bufferedInputStream);
			// Reading of the properties from the file.
			String baseOntologiePath = properties.getProperty(PROPERTY_BASE_ONTOLOGY);
			String ruleOntologiePath = properties.getProperty(PROPERTY_RULE_ONTOLOGY);
			String workingDirectoryPath = properties.getProperty(PROPERTY_WORKING_DIRECTORY);
			String baseOntologyClass = properties.getProperty(PROPERTY_BASE_ONTOLOGY_CLASS);
			
			WorkingDirectory workingDirectory = ServerModelFactory.
					createWorkingDirectory(workingDirectoryPath);
			
			// Add the values of the properties to the object for sending.
			ServerConfigurationData serverConfigurationData = ConfigurationModelFactory.
					createServerConfigurationData(baseOntologiePath, null, 
												  ruleOntologiePath, null, 
												  workingDirectory);
			serverConfigurationData.setBaseOntologyClass(baseOntologyClass);
			
			return serverConfigurationData;
		} catch (IOException ex) {
			fail();
			return null;
		}
	}*/
	
	@After
	public void tearDown() {
	}
	
	/*@Test
	public void testReadConfigurationData() {
		String url = "http://localhost:" + port + PathConstants.SERVER_CLIENT_INTERFACE + "/" + PathConstants.SERVER_GET_CONFIGURATION_DATA;
		ResponseEntity<ServerConfigurationDataResponse> response = restTemplate.getForEntity(url, ServerConfigurationDataResponse.class);
		assertTrue(response.getStatusCode().is2xxSuccessful());
		ServerConfigurationDataResponse serverConfigurationData = response.getBody();
		// Write the received data in local variables.
		String workingDirectoryPath = serverConfigurationData.getWorkingDirectoryPath();
		String baseOntologyPath = serverConfigurationData.getConfiguredBaseOntologyPath();
		String ruleOntologyPath = serverConfigurationData.getConfiguredRuleOntologyPath();
		// The sending of the base ontology class is not implemented yet. 
		//String baseOntologyClass = serverConfigurationData.getBaseOntologyClass();
		// Asserts.
		assertThat(workingDirectoryPath, is(equalTo(WORKING_DIRECTORY)));
		assertThat(baseOntologyPath, is(equalTo(TEST_BASE_ONTOLOGY)));
		assertThat(ruleOntologyPath, is(equalTo(TEST_RULE_ONTOLOGY)));
		//assertThat(baseOntologyClass, is(equalTo(TEST_BASE_ONTOLOGY_CLASS)));
	}*/
	
	@Test 
	public void testGetHierarchy() {
		String url = "http://localhost:" + port + PathConstants.SERVER_CLIENT_INTERFACE + "/" + PathConstants.SERVER_READ_HIERARCHY;
		StringListResponse classNames = ResponseModelFactory.getStringListResponse();
		classNames.addString("Information_Package");
		
		HttpEntity<StringListResponse> classNamesEntity = new 
				HttpEntity<StringListResponse>(classNames);
		
		ResponseEntity<String> response = restTemplate.postForEntity(url, classNamesEntity, String.class);
		assertTrue(response.getStatusCode().is2xxSuccessful());
		
		/*OntologyClass ontologyClass = ontologyOperations.getHierarchy("Information_Package");
		assertThat(ontologyClass.getSubclassCount(), is(equalTo(2)));
		assertThat(ontologyClass.getLabel().contains("Information_Package"), is(equalTo(true)));
		OntologyClass contentInformation = ontologyClass.getSubClassAt(0);
		assertThat(contentInformation.getLabel().contains("Content_Information"), is(equalTo(true)));
		assertThat(contentInformation.getSubclassCount(), is(equalTo(2)));
		OntologyClass dataObject = contentInformation.getSubClassAt(1);
		assertThat(dataObject.getLabel().contains("Data_Object"), is(equalTo(true)));
		assertThat(dataObject.getSubclassCount(), is(equalTo(2)));
		OntologyClass digitalObject = dataObject.getSubClassAt(1);
		assertThat(digitalObject.getLabel().contains("Digital_Object"), is(equalTo(true)));
		assertThat(digitalObject.getSubclassCount(), is(equalTo(0)));
		OntologyClass physicalObject = dataObject.getSubClassAt(0);
		assertThat(physicalObject.getLabel().contains("Physical_Object"), is(equalTo(true)));
		assertThat(physicalObject.getSubclassCount(), is(equalTo(0)));
		OntologyClass representationInformation = contentInformation.getSubClassAt(0);
		assertThat(representationInformation.getLabel().contains("Representation_Information"), is(equalTo(true)));
		assertThat(representationInformation.getSubclassCount(), is(equalTo(3)));
		OntologyClass otherRepresentationInformation = representationInformation.getSubClassAt(2);
		assertThat(otherRepresentationInformation.getLabel().contains("Other_Representation_Information"), is(equalTo(true)));
		assertThat(otherRepresentationInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass semanticInformation = representationInformation.getSubClassAt(1);
		assertThat(semanticInformation.getLabel().contains("Semantic_Information"), is(equalTo(true)));
		assertThat(semanticInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass structureInformation = representationInformation.getSubClassAt(0);
		assertThat(structureInformation.getLabel().contains("Structure_Information"), is(equalTo(true)));
		assertThat(structureInformation.getSubclassCount(), is(equalTo(0)));
		// 7.2. PreservationDescriptionInformation.
		OntologyClass preservationDescriptionInformation = ontologyClass.getSubClassAt(1);
		assertThat(preservationDescriptionInformation.getLabel().contains("Preservation_Description_Information"), is(equalTo(true)));
		assertThat(preservationDescriptionInformation.getSubclassCount(), is(equalTo(5)));
		OntologyClass accessRightsInformation = preservationDescriptionInformation.getSubClassAt(0);
		assertThat(accessRightsInformation.getLabel().contains("Access_Rights_Information"), is(equalTo(true)));
		assertThat(accessRightsInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass contextInformation = preservationDescriptionInformation.getSubClassAt(2);
		assertThat(contextInformation.getLabel().contains("Context_Information"), is(equalTo(true)));
		assertThat(contextInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass fixityInformation = preservationDescriptionInformation.getSubClassAt(3);
		assertThat(fixityInformation.getLabel().contains("Fixity_Information"), is(equalTo(true)));
		assertThat(fixityInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass provenanceInformation = preservationDescriptionInformation.getSubClassAt(4);
		assertThat(provenanceInformation.getLabel().contains("Provenance_Information"), is(equalTo(true)));
		assertThat(provenanceInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass referenceInformation = preservationDescriptionInformation.getSubClassAt(1);
		assertThat(referenceInformation.getLabel().contains("Reference_Information"), is(equalTo(true)));
		assertThat(referenceInformation.getSubclassCount(), is(equalTo(0)));*/
	}
}