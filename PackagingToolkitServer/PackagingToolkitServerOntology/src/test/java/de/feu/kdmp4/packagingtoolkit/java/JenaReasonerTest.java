package de.feu.kdmp4.packagingtoolkit.java;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerFactory;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.shared.JenaException;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Ignore;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;

public class JenaReasonerTest {

	@Test
	@Ignore
	public void testReasoner() {
		String NS = "urn:x-hp-jena:eg/";

		// Build a trivial example data set
		Model rdfsExample = ModelFactory.createDefaultModel();
		Property p = rdfsExample.createProperty(NS, "p");
		Property q = rdfsExample.createProperty(NS, "x");
		rdfsExample.add(p, RDFS.subPropertyOf, q);
		rdfsExample.createResource(NS+"a").addProperty(p, "foo");
		
		Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
		
		InfModel inf = ModelFactory.createInfModel(reasoner, rdfsExample);
		
		Resource a = inf.getResource(NS+"a");
		ValidityReport validityReport = inf.validate();
		
		if (validityReport.isValid()) {
			System.out.println("Laeuft");
		} else {
		    System.out.println("Conflicts");
		    for (Iterator i = validityReport.getReports(); i.hasNext(); ) {
		        System.out.println(" - " + i.next());
		    }
		}
		
		System.out.println("Statement: " + a.getProperty(q));
	}

	@Test
	//@Ignore
	public void testOwlReasoner() {
		File fileWithOntology = new File("/home/christopher/Dokumente/Studium/Masterarbeit/workspace/PackagingToolkit/PackagingToolkitServer/PackagingToolkitServerOntology/src/main/resources/KDMP4-OAISrdf.owl");
		
		System.out.println("********************");
		
		OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		final String ontologyPath = fileWithOntology.getAbsolutePath();
		try (InputStream inputStream = FileManager.get().open(ontologyPath)){
			ontModel.read(inputStream, null);
		} catch (JenaException jex) {
			jex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		OntClass person = ontModel.getOntClass("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#Person");
		Individual sascha = ontModel.createIndividual(person);
		
		DatatypeProperty hasEarnings = ontModel.getDatatypeProperty("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasEarnings");
		DatatypeProperty hasOrcId = ontModel.getDatatypeProperty("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#has_ORCID");
		Literal literal = ontModel.createTypedLiteral("a");
		Literal literalOrcId = ontModel.createTypedLiteral("X");
		sascha.addLiteral(hasEarnings, literal);
		sascha.addLiteral(hasOrcId, literalOrcId);
		
		ontModel.createIndividual(sascha);
		//InfModel inf = ModelFactory.createRDFSModel(ontModel);  // [1]
		Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
		
		InfModel inf = ModelFactory.createInfModel(reasoner, ontModel);
		
		ValidityReport validityReport = inf.validate();
		
		if (validityReport.isValid()) {
			System.out.println("Laeuft");
		} else {
		    System.out.println("Conflicts");
		    for (Iterator i = validityReport.getReports(); i.hasNext(); ) {
		        System.out.println(" - " + i.next());
		    }
		}
		
		
		/*String NS = "urn:x-hp-jena:eg/";
		OntModel m = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		m.read( "http://www.eswc2006.org/technologies/ontology" );

		DatatypeProperty subDeadline = m.getDatatypeProperty( NS + "hasSubmissionDeadline" );
		DatatypeProperty notifyDeadline = m.getDatatypeProperty( NS + "hasNotificationDeadline" );
		DatatypeProperty cameraDeadline = m.getDatatypeProperty( NS + "hasCameraReadyDeadline" );

		DatatypeProperty deadline = m.createDatatypeProperty( NS + "deadline" );
		deadline.addDomain( m.getOntClass( NS + "Call" ) );
		deadline.addRange( XSD.dateTime );

		deadline.addSubProperty( subDeadline );
		deadline.addSubProperty( notifyDeadline );
		deadline.addSubProperty( cameraDeadline );*/
	}
	
}
