package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

public class OntologyClassIndividualsHashMapTest {

	// ****************************************************
	// testAddIndividual()
	// ****************************************************
	/**
	 * Tests the method addIndividual(). Three individuals are added to the map and the result
	 * is checked after each insertion.
	 */
	@Test
	public void testAddIndividual() {
		List<OntologyIndividual> individuals = prepareTest_testAddIndividual_ThreeIndividuals();
		OntologyClassIndividualsMap ontologyClassIndividualsMap = new OntologyClassIndividualsHashMapImpl();
		checkInsertIndividual(individuals.get(0), ontologyClassIndividualsMap);
		checkInsertIndividual(individuals.get(1), ontologyClassIndividualsMap);
		checkInsertIndividual(individuals.get(2), ontologyClassIndividualsMap);
	}
	
	/**
	 * Creates a list with three individuals. These individuals are added to the map in the test.
	 * @return The individuals that are added to the test.
	 */
	private List<OntologyIndividual> prepareTest_testAddIndividual_ThreeIndividuals() {
		List<OntologyIndividual> individuals = new ArrayList<>();
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		
		OntologyClass ontologyClass1 = new OntologyClassImpl(ServerModelFactory.createNamespace("a"), ServerModelFactory.createLocalName("b"));
		OntologyClass ontologyClass2 = new OntologyClassImpl(ServerModelFactory.createNamespace("x"), ServerModelFactory.createLocalName("y"));
		
		OntologyIndividual individual1 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual2 = new OntologyIndividualImpl(ontologyClass2, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual3 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		
		individuals.add(individual1);
		individuals.add(individual2);
		individuals.add(individual3);
		
		return individuals;
	}
	
	/**
	 * Inserts an individual to the map and checks the result: The individual has to be contained in the 
	 * map. 
	 * @param ontologyIndividual The individual for the test.
	 * @param ontologyClassIndividualsMap The map that contains the individuals for the test.
	 */
	private void checkInsertIndividual(OntologyIndividual ontologyIndividual, OntologyClassIndividualsMap ontologyClassIndividualsMap) {
		ontologyClassIndividualsMap.addOrUpdateIndividual(ontologyIndividual);
		
		OntologyClass actualOntologyClass = ontologyIndividual.getOntologyClass();
		String className = actualOntologyClass.getFullName();
		OntologyIndividualCollection actualIndividuals = ontologyClassIndividualsMap.getOntologyIndividuals(className);
		List<OntologyIndividual> actualIndividualList = actualIndividuals.toList();
		assertThat(ontologyIndividual, isIn(actualIndividualList));
	}
	
	// ****************************************************
	// testAddIndividuals()
	// ****************************************************
	@Test
	/**
	 * Tests the method addIndividuals(). The map is initialized with one individual. Then a list with three individuals is
	 * inserted in the map and the result is checked.
	 */
	public void testAddIndividuals() {
		OntologyIndividualCollection ontologyIndividualList = prepareTest_testAddIndividuals_ThreeIndividuals();
		OntologyClassIndividualsMap ontologyClassIndividualsMap = prepareTest_testAddIndividuals_MapWithOneIndividual();
		
		ontologyClassIndividualsMap.addIndividuals("a#b", ontologyIndividualList);
		checkResult_testAddIndividuals(ontologyClassIndividualsMap);
	}
	
	/**
	 * Creates a list with three individuals. These individuals are added to the map in the test.
	 * @return The individuals that are added to the test.
	 */
	private OntologyIndividualCollection prepareTest_testAddIndividuals_ThreeIndividuals() {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		OntologyIndividualCollection individuals = new OntologyIndividualCollectionImpl();
		
		OntologyClass ontologyClass1 = new OntologyClassImpl(ServerModelFactory.createNamespace("a"), ServerModelFactory.createLocalName("b"));
		
		OntologyIndividual individual1 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual2 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual3 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		
		individuals.addIndividual(individual1);
		individuals.addIndividual(individual2);
		individuals.addIndividual(individual3);
		
		return individuals;
	}
	
	/**
	 * Initializes the map with one individual.
	 * @return The map with one individual.
	 */
	private OntologyClassIndividualsMap prepareTest_testAddIndividuals_MapWithOneIndividual() {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		OntologyClassIndividualsMap ontologyClassIndividualsMap = new OntologyClassIndividualsHashMapImpl();
		OntologyClass ontologyClass = new OntologyClassImpl(ServerModelFactory.createNamespace("a"), ServerModelFactory.createLocalName("b"));
		OntologyIndividual individual = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		ontologyClassIndividualsMap.addOrUpdateIndividual(individual);
		
		return ontologyClassIndividualsMap;
	}
	
	/**
	 * Checks the result: The three individuals has to be contained in the map. 
	 * @param ontologyClassIndividualsMap The map with the individuals inserted during the test.
	 */
	private void checkResult_testAddIndividuals(OntologyClassIndividualsMap ontologyClassIndividualsMap) {
		OntologyIndividualCollection actualIndividuals = ontologyClassIndividualsMap.getOntologyIndividuals("a#b");
		List<OntologyIndividual> actualIndividualList = actualIndividuals.toList();
		
		for (int i = 0; i < actualIndividualList.size(); i++) {
			OntologyIndividual ontologyIndividual = actualIndividualList.get(i);
			assertThat(ontologyIndividual, isIn(actualIndividualList));
		}
	}
	
	// ****************************************************
	// testGetOntologyIndividuals()
	// ****************************************************
	@Test
	public void testGetOntologyIndividuals() {
		OntologyClassIndividualsMap ontologyClassIndividualsMap = prepareTest_testGetOntologyIndividuals_ThreeIndividualsInMap();
		OntologyIndividualCollection ontologyIndividualList = ontologyClassIndividualsMap.getOntologyIndividuals("a#b");
		int actualSize = ontologyIndividualList.getIndiviualsCount();
		assertThat(actualSize, is(equalTo(2)));
	}
	
	/**
	 * Creates a list with three individuals. These individuals are added to the map in the test.
	 * @return The map with the individuals for the test.
	 */
	private OntologyClassIndividualsMap prepareTest_testGetOntologyIndividuals_ThreeIndividualsInMap() {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		OntologyClassIndividualsMap ontologyClassIndividualsMap = new OntologyClassIndividualsHashMapImpl();
		
		OntologyClass ontologyClass1 = new OntologyClassImpl(ServerModelFactory.createNamespace("a"), ServerModelFactory.createLocalName("b"));
		OntologyClass ontologyClass2 = new OntologyClassImpl(ServerModelFactory.createNamespace("x"), ServerModelFactory.createLocalName("y"));
		
		OntologyIndividual individual1 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual2 = new OntologyIndividualImpl(ontologyClass2, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual3 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		
		ontologyClassIndividualsMap.addOrUpdateIndividual(individual1);
		ontologyClassIndividualsMap.addOrUpdateIndividual(individual2);
		ontologyClassIndividualsMap.addOrUpdateIndividual(individual3);
		
		return ontologyClassIndividualsMap;
	}
	
	// ****************************************************
	// testToOntologyIndividualList()
	// ****************************************************
	/**
	 * Tests the method toOntologyIndividualList().
	 */
	@Test
	public void testToOntologyIndividualList() {
		OntologyIndividualCollection expectedOntologyIndividuals = new OntologyIndividualCollectionImpl();
		OntologyClassIndividualsMap ontologyClassIndividualsMap = prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(expectedOntologyIndividuals);
		
		OntologyIndividualCollection actualOntologyIndividuals = ontologyClassIndividualsMap.toOntologyIndividualList();
		
		checkResults_testToOntologyIndividualList(expectedOntologyIndividuals, actualOntologyIndividuals);		
	}
	
	/**
	 * Creates a list with three individuals. These individuals are added to the map in the test.
	 * @return The map with the individuals for the test.
	 */
	private OntologyClassIndividualsMap prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(OntologyIndividualCollection ontologyIndividualList) {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		OntologyClassIndividualsMap ontologyClassIndividualsMap = new OntologyClassIndividualsHashMapImpl();
		
		OntologyClass ontologyClass1 = new OntologyClassImpl(ServerModelFactory.createNamespace("a"), ServerModelFactory.createLocalName("b"));
		OntologyClass ontologyClass2 = new OntologyClassImpl(ServerModelFactory.createNamespace("x"), ServerModelFactory.createLocalName("y"));
		
		OntologyIndividual individual1 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual2 = new OntologyIndividualImpl(ontologyClass2, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual individual3 = new OntologyIndividualImpl(ontologyClass1, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		
		ontologyClassIndividualsMap.addOrUpdateIndividual(individual1);
		ontologyClassIndividualsMap.addOrUpdateIndividual(individual2);
		ontologyClassIndividualsMap.addOrUpdateIndividual(individual3);
		
		ontologyIndividualList.addIndividual(individual1);
		ontologyIndividualList.addIndividual(individual2);
		ontologyIndividualList.addIndividual(individual3);
		
		return ontologyClassIndividualsMap;
	}
	
	/**
	 * Compares the sizes of the two lists and their individuals.
	 * @param expectedOntologyIndividuals The expected individuals that were created during initialization.
	 * @param actualOntologyIndividuals The actual individuals that are the result of the test.
	 */
	private void checkResults_testToOntologyIndividualList(OntologyIndividualCollection expectedOntologyIndividuals, OntologyIndividualCollection actualOntologyIndividuals) {
		int actualCount = actualOntologyIndividuals.getIndiviualsCount();
		int expectedCount = expectedOntologyIndividuals.getIndiviualsCount();
		
		List<OntologyIndividual> expectedOntologyIndividualList = expectedOntologyIndividuals.toList();
		List<OntologyIndividual> actualOntologyIndividualList = actualOntologyIndividuals.toList();
		
		assertThat(actualCount, is(equalTo(expectedCount)));
		for (int i = 0; i < expectedOntologyIndividualList.size(); i++) {
			OntologyIndividual expectedOntologyIndividual = expectedOntologyIndividualList.get(i);
			assertThat(expectedOntologyIndividual, isIn(actualOntologyIndividualList));
		}
	}
	
	@Test
	public void testFindIndividualByUuid_individualExistsInMap() {
		final OntologyIndividualCollection expectedOntologyIndividuals = new OntologyIndividualCollectionImpl();
		final OntologyClassIndividualsMap underTest = prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(expectedOntologyIndividuals);
		final Uuid uuidOfExpectionIndividual = expectedOntologyIndividuals.getIndividual(1).getUuid();
		final OntologyIndividual actualIndividual = underTest.findIndividualByUuid(uuidOfExpectionIndividual);
		final Uuid actualUuid = actualIndividual.getUuid();
		
		assertThat(actualUuid, is(equalTo(uuidOfExpectionIndividual)));
	}
	
	@Test
	public void testFindIndividualByUuid_individualDoesNotExistInMap() {
		final OntologyIndividualCollection expectedOntologyIndividuals = new OntologyIndividualCollectionImpl();
		final OntologyClassIndividualsMap underTest = prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(expectedOntologyIndividuals);
		final Uuid uuidOfExpectionIndividual = new Uuid();
		final OntologyIndividual actualIndividual = underTest.findIndividualByUuid(uuidOfExpectionIndividual);
		
		assertNull(actualIndividual);
	}
	
	@Test
	public void testRemoveIndividualByUuid_individualExistsInMap() {
		final OntologyIndividualCollection expectedOntologyIndividuals = new OntologyIndividualCollectionImpl();
		final OntologyClassIndividualsMap underTest = prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(expectedOntologyIndividuals);
		final OntologyIndividual ontologyIndividual = expectedOntologyIndividuals.getIndividual(1);
		underTest.removeIndividualByUuid(ontologyIndividual.getUuid());
		
		assertThat(underTest.getEntryCount(), is(equalTo(2)));
		final boolean individualExistsAfterDeletion = underTest.containsIndividual(ontologyIndividual);
		assertFalse(individualExistsAfterDeletion);
	}
	
	/*@Test
	public void testRemoveIndividualByUuid_individualDoesNotExistInMap() {
		final OntologyIndividualList expectedOntologyIndividuals = new OntologyIndividualListImpl();
		final OntologyClassIndividualsMap underTest = prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(expectedOntologyIndividuals);
		final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl();
		underTest.removeIndividualByUuid(ontologyIndividual.getUuid());
		
		assertThat(underTest.getEntryCount(), is(equalTo(3)));
		final boolean individualExistsAfterDeletion = underTest.containsIndividual(ontologyIndividual);
		assertTrue(individualExistsAfterDeletion);
	}*/
	
	@Test
	public void testFindClassByIndividualUuid_individualExistsInMap() {
		final OntologyIndividualCollection expectedOntologyIndividuals = new OntologyIndividualCollectionImpl();
		final OntologyClassIndividualsMap underTest = prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(expectedOntologyIndividuals);
		final OntologyIndividual ontologyIndividual = expectedOntologyIndividuals.getIndividual(1);
		final OntologyClass actualOntologyClass = underTest.findClassByIndividualUuid(ontologyIndividual.getUuid());
		final OntologyClass expectedOntologyClass = ontologyIndividual.getOntologyClass();
		
		assertThat(actualOntologyClass, is(equalTo(expectedOntologyClass)));
	}
	
	@Test
	public void testFindClassByIndividualUuid_individualDoesNotExistInMap() {
		final OntologyIndividualCollection expectedOntologyIndividuals = new OntologyIndividualCollectionImpl();
		final OntologyClassIndividualsMap underTest = prepareTest_testToOntologyIndividualList_ThreeIndividualsInMap(expectedOntologyIndividuals);
		final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl();
		final OntologyClass actualOntologyClass = underTest.findClassByIndividualUuid(ontologyIndividual.getUuid());
		
		assertNull(actualOntologyClass);
	}
}
