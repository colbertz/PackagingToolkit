package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.feu.kdmp4.packagingtoolkit.server.configuration.FactoryConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.MockedOperationsConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@ActiveProfiles("propertiesTest")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
@Category(SpringTests.class)
public class OntologyStringPropertyTest {
	private static final String PROPERTY_ID = "datatypeProperty#byteProperty";
	private static final String PROPERTY_LABEL = "byteProperty";
	private static final String PROPERTY_RANGE = "byte";
	
	private OntologyStringProperty ontologyStringProperty;
	
	@Before
	public void setUp() {
		ontologyStringProperty = prepareStringProperty();
	}
	
	@Test
	public void testIsString() {
		assertFalse(ontologyStringProperty.isBooleanProperty());
		assertFalse(ontologyStringProperty.isByteProperty());
		assertFalse(ontologyStringProperty.isDateProperty());
		assertFalse(ontologyStringProperty.isDateTimeProperty());
		assertFalse(ontologyStringProperty.isDoubleProperty());
		assertFalse(ontologyStringProperty.isDurationProperty());
		assertFalse(ontologyStringProperty.isFloatProperty());
		assertFalse(ontologyStringProperty.isIntegerProperty());
		assertFalse(ontologyStringProperty.isLongProperty());
		assertFalse(ontologyStringProperty.isShortProperty());
		assertTrue(ontologyStringProperty.isStringProperty());
		assertFalse(ontologyStringProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		String value = ontologyStringProperty.getPropertyValue();
		assertEquals("abc", value);
	}
	
	private OntologyStringProperty prepareStringProperty() {
		OntologyStringProperty ontologyStringProperty = new OntologyStringPropertyImpl(PROPERTY_ID,  PROPERTY_LABEL, PROPERTY_RANGE);
		ontologyStringProperty.setPropertyValue("abc");
		return ontologyStringProperty;
	}
	
	@Test 
	public void testCompareToEqual() {
		OntologyStringProperty anOtherProperty = prepareStringProperty();
		int areTheyEqual = anOtherProperty.compareTo(ontologyStringProperty);
		assertEquals(0, areTheyEqual);
	}

	@Test 
	public void testCompareToNotEqual() {
		OntologyStringProperty anOtherProperty = prepareStringProperty();
		anOtherProperty.setPropertyValue("xxx");
		int areTheyEqual = anOtherProperty.compareTo(ontologyStringProperty);
		assertNotEquals(0, areTheyEqual);
	}
}
