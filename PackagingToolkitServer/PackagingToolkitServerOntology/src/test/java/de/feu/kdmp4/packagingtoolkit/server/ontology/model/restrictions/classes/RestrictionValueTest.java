package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.RestrictionCollection;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class RestrictionValueTest {
	private RestrictionValue<Integer> restrictionValue;
	
	@Before
	public void setUp() throws Exception {
		restrictionValue = new RestrictionValue<Integer>(5);
	}

	@Test
	public void testCompare1() {
		RestrictionValue<Integer> theOtherRestrictionValue = new RestrictionValue<Integer>(5);
		int compare = restrictionValue.compare(theOtherRestrictionValue);
		assertThat(compare, is(equalTo(0)));
	}
	
	@Test
	public void testCompare2() {
		RestrictionValue<Integer> theOtherRestrictionValue = new RestrictionValue<Integer>(6);
		int compare = restrictionValue.compare(theOtherRestrictionValue);
		boolean compareBoolean = compare < 0;
		assertTrue(compareBoolean);
	}	
	
	@Test
	public void testCompare3() {
		RestrictionValue<Integer> theOtherRestrictionValue = new RestrictionValue<Integer>(3);
		int compare = restrictionValue.compare(theOtherRestrictionValue);
		boolean compareBoolean = compare > 0;
		assertTrue(compareBoolean);
	}	
}
