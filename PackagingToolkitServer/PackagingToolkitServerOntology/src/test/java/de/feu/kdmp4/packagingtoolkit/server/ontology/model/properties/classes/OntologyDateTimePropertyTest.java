package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;

public class OntologyDateTimePropertyTest {
	private static final String PROPERTY_ID = "datatypeProperty#dateTimeProperty";
	private static final String PROPERTY_LABEL = "dateTimeProperty";
	private static final String PROPERTY_RANGE = "dateTime";
	
	private OntologyDateTimeProperty ontologyDateTimeProperty;
	
	@Before
	public void setUp() {
		ontologyDateTimeProperty = new OntologyDateTimePropertyImpl(PROPERTY_ID, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsDateTime() {
		assertFalse(ontologyDateTimeProperty.isBooleanProperty());
		assertFalse(ontologyDateTimeProperty.isByteProperty());
		assertFalse(ontologyDateTimeProperty.isDateProperty());
		assertTrue(ontologyDateTimeProperty.isDateTimeProperty());
		assertFalse(ontologyDateTimeProperty.isDoubleProperty());
		assertFalse(ontologyDateTimeProperty.isDurationProperty());
		assertFalse(ontologyDateTimeProperty.isFloatProperty());
		assertFalse(ontologyDateTimeProperty.isIntegerProperty());
		assertFalse(ontologyDateTimeProperty.isLongProperty());
		assertFalse(ontologyDateTimeProperty.isShortProperty());
		assertFalse(ontologyDateTimeProperty.isStringProperty());
		assertFalse(ontologyDateTimeProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		LocalDateTime expectedDateTime = LocalDateTime.now();
		ontologyDateTimeProperty.setPropertyValue(expectedDateTime);
		LocalDateTime actualDateTime = ontologyDateTimeProperty.getPropertyValue();
		assertEquals(expectedDateTime, actualDateTime);
	}
}
