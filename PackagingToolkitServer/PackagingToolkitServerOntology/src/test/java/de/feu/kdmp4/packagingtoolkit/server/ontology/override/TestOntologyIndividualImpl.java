package de.feu.kdmp4.packagingtoolkit.server.ontology.override;

import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;

// DELETE_ME Was wollte ich hiermit? Brauche ich das noch?
public class TestOntologyIndividualImpl extends OntologyIndividualImpl {
	private static final long serialVersionUID = -3626585210571587277L;

	public TestOntologyIndividualImpl(OntologyClass ontologyClass) {
		super(ontologyClass, PackagingToolkitModelFactory.getDefaultInformationPackageUuid(), new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl());
	}
	
	@Override
	public void initialize() {
		//setOntologyModelFactory(new OntologyModelFactory());
		super.initialize();
	}
}
