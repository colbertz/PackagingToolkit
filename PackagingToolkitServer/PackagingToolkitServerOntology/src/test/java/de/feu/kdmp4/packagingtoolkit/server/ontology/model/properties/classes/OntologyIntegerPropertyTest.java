package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.configuration.FactoryConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.MockedOperationsConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@ActiveProfiles("propertiesTest")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
@Category(SpringTests.class)
public class OntologyIntegerPropertyTest implements ApplicationContextAware{
	private ApplicationContext applicationContext;
	private static final String PROPERTY_ID = "datatypeProperty#integerProperty";
	private static final String PROPERTY_LABEL = "integerProperty";
	private static final String PROPERTY_RANGE = "integer";
	
	private OntologyIntegerProperty ontologyIntegerProperty;
	
	private ExpectedException thrownException = ExpectedException.none();
	
	@Before
	public void setUp() {
		ontologyIntegerProperty = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.BOTH , false, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsInteger() {
		assertFalse(ontologyIntegerProperty.isBooleanProperty());
		assertFalse(ontologyIntegerProperty.isByteProperty());
		assertFalse(ontologyIntegerProperty.isDateProperty());
		assertFalse(ontologyIntegerProperty.isDateTimeProperty());
		assertFalse(ontologyIntegerProperty.isDoubleProperty());
		assertFalse(ontologyIntegerProperty.isDurationProperty());
		assertFalse(ontologyIntegerProperty.isFloatProperty());
		assertTrue(ontologyIntegerProperty.isIntegerProperty());
		assertFalse(ontologyIntegerProperty.isLongProperty());
		assertFalse(ontologyIntegerProperty.isShortProperty());
		assertFalse(ontologyIntegerProperty.isStringProperty());
		assertFalse(ontologyIntegerProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		ontologyIntegerProperty.setPropertyValue(5l);
		long value = ontologyIntegerProperty.getPropertyValue();
		assertEquals(5l, value);
	}

	@Test
	public void testNonPositiveInteger0() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.MINUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(0l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testNonPositiveIntegerCorrect() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.MINUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(-1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testNonPositiveIntegerIncorrect() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.MINUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testNegativeInteger0() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.MINUS_SIGN, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(0l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testNegativeIntegerCorrect1() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.MINUS_SIGN, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(-1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testNegativeIntegerCorrect2() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.MINUS_SIGN, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(-2l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testNegativeIntegerIncorrect() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, false, Sign.MINUS_SIGN, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}

	@Test
	public void testNonNegativeInteger0() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(0l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testNonNegativeInteger1() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testNonNegativeIntegerIncorrect() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(-1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testUnsignedIntegerValue0() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(0l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedIntegerValue1() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedIntegerMaxValue() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		long maxValue = Integer.MAX_VALUE * 2l + 1;
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(maxValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedIntegerMaxValuePlus1() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		long maxValue = Integer.MAX_VALUE * 2l + 2;
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(maxValue);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testUnsignedIntegerIncorrectValue() {
		OntologyIntegerProperty unsigned = new OntologyIntegerPropertyImpl(PROPERTY_ID, true, Sign.PLUS_SIGN, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Long> value = new RestrictionValue<Long>(-1l);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Rule
	public ExpectedException getThrownException() {
		return thrownException;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
