package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class DatatypePropertyListTest {
	private DatatypePropertyCollection datatypePropertyList;
	private List<OntologyDatatypeProperty> expectedList;
	
	@Before
	public void setUp() {
		expectedList = new ArrayList<>();
		datatypePropertyList = new DatatypePropertyCollectionImpl();
		OntologyDatatypeProperty booleanProperty = new OntologyBooleanPropertyImpl();
		OntologyDatatypeProperty doubleProperty = new OntologyDoublePropertyImpl();
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl();
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		datatypePropertyList.addDatatypeProperty(booleanProperty);
		datatypePropertyList.addDatatypeProperty(doubleProperty);
		datatypePropertyList.addDatatypeProperty(stringProperty);
		datatypePropertyList.addDatatypeProperty(shortProperty);
		
		expectedList.add(booleanProperty);
		expectedList.add(doubleProperty);
		expectedList.add(stringProperty);
		expectedList.add(shortProperty);
	}
	
	@Test
	public void testAddDatatypeProperty() {
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		expectedList.add(shortProperty);
		int expectedSize = expectedList.size();
		
		datatypePropertyList.addDatatypeProperty(shortProperty);
		int actualSize = datatypePropertyList.getPropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			OntologyDatatypeProperty expectedDatatypeProperty = expectedList.get(i);
			OntologyDatatypeProperty actualDatatypeProperty = datatypePropertyList.getDatatypeProperty(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}
	
	@Test
	public void testGetDatatypeProperty() {
		OntologyDatatypeProperty expectedDatatypeProperty = expectedList.get(2);
		OntologyDatatypeProperty actualDatatypeProperty = datatypePropertyList.getDatatypeProperty(2);
		int expectedSize = expectedList.size();
		int actualSize = datatypePropertyList.getPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
	}
	
	@Test
	public void testGetPropertiesCount() {
		int expectedSize = expectedList.size();
		int actualSize = datatypePropertyList.getPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
}
