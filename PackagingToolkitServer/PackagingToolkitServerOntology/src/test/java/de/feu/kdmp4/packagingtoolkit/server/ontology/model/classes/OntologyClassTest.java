package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.server.models.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class OntologyClassTest {
	private OntologyClass ontologyClass;
	private List<OntologyDatatypeProperty> expectedDatatypePropertyList;
	private List<OntologyObjectProperty> expectedObjectPropertyList;
	private List<OntologyClass> expectedSubclasses;
	
	@Before
	public void setUp() {
		expectedDatatypePropertyList = new ArrayList<>();
		expectedObjectPropertyList = new ArrayList<>();
		expectedSubclasses = new ArrayList<>();
		
		DatatypePropertyCollection datatypePropertyList = prepareDatatypeProperties();
		ObjectPropertyCollection objectPropertyList = prepareObjectProperties();
		
		ontologyClass = new OntologyClassImpl(datatypePropertyList, objectPropertyList, new NamespaceImpl("myNamespace"), new LocalNameImpl("myClass"));
		prepareSubclasses();
	}
	
	private DatatypePropertyCollection prepareDatatypeProperties() {
		DatatypePropertyCollection datatypePropertyList = new DatatypePropertyCollectionImpl();
		OntologyDatatypeProperty booleanProperty = new OntologyBooleanPropertyImpl();
		OntologyDatatypeProperty doubleProperty = new OntologyDoublePropertyImpl();
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl();
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		datatypePropertyList.addDatatypeProperty(booleanProperty);
		datatypePropertyList.addDatatypeProperty(doubleProperty);
		datatypePropertyList.addDatatypeProperty(stringProperty);
		datatypePropertyList.addDatatypeProperty(shortProperty);
		
		expectedDatatypePropertyList.add(booleanProperty);
		expectedDatatypePropertyList.add(doubleProperty);
		expectedDatatypePropertyList.add(stringProperty);
		expectedDatatypePropertyList.add(shortProperty);
		
		return datatypePropertyList;
	}
	
	private ObjectPropertyCollection prepareObjectProperties() {
		OntologyObjectProperty objectProperty1 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty2 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty3 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty4 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);

		expectedObjectPropertyList.add(objectProperty1);
		expectedObjectPropertyList.add(objectProperty2);
		expectedObjectPropertyList.add(objectProperty3);
		expectedObjectPropertyList.add(objectProperty4);
		
		ObjectPropertyCollection objectPropertyList = new ObjectPropertyCollectionImpl();
		objectPropertyList.addObjectProperty(objectProperty1);
		objectPropertyList.addObjectProperty(objectProperty2);
		objectPropertyList.addObjectProperty(objectProperty3);
		objectPropertyList.addObjectProperty(objectProperty4);
		
		return objectPropertyList;
	}
	
	private void prepareSubclasses() {
		OntologyClass subclass1 = new OntologyClassImpl(new NamespaceImpl("myNamespace"), new LocalNameImpl("class1"));
		OntologyClass subclass2 = new OntologyClassImpl(new NamespaceImpl("myNamespace"), new LocalNameImpl("class2"));
		OntologyClass subclass3 = new OntologyClassImpl(new NamespaceImpl("myNamespace"), new LocalNameImpl("class3"));
		OntologyClass subclass4 = new OntologyClassImpl(new NamespaceImpl("myNamespace"), new LocalNameImpl("class4"));
		ontologyClass.addSubClass(subclass1);
		ontologyClass.addSubClass(subclass2);
		ontologyClass.addSubClass(subclass3);
		ontologyClass.addSubClass(subclass4);
		
		expectedSubclasses.add(subclass1);
		expectedSubclasses.add(subclass2);
		expectedSubclasses.add(subclass3);
		expectedSubclasses.add(subclass4);
	}
	
	@Test
	public void testAddDatatypeProperty() {
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		expectedDatatypePropertyList.add(shortProperty);
		int expectedSize = expectedDatatypePropertyList.size();
		
		ontologyClass.addDatatypeProperty(shortProperty);
		int actualSize = ontologyClass.getDatatypePropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			OntologyDatatypeProperty expectedDatatypeProperty = expectedDatatypePropertyList.get(i);
			OntologyDatatypeProperty actualDatatypeProperty = ontologyClass.getDatatypeProperty(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}
	
	@Test
	public void testGetDatatypeProperty() {
		OntologyDatatypeProperty expectedDatatypeProperty = expectedDatatypePropertyList.get(2);
		OntologyDatatypeProperty actualDatatypeProperty = ontologyClass.getDatatypeProperty(2);
		int expectedSize = expectedDatatypePropertyList.size();
		int actualSize = ontologyClass.getDatatypePropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
	}

	@Test
	public void testGetDatatypePropertiesCount() {
		int expectedSize = expectedDatatypePropertyList.size();
		int actualSize = ontologyClass.getDatatypePropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	@Test
	public void testAddObjectPropery() {
		OntologyObjectProperty objectProperty = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		expectedObjectPropertyList.add(objectProperty);
		int expectedSize = expectedObjectPropertyList.size();
		
		ontologyClass.addObjectProperty(objectProperty);
		int actualSize = ontologyClass.getObjectPropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			OntologyObjectProperty expectedDatatypeProperty = expectedObjectPropertyList.get(i);
			OntologyObjectProperty actualDatatypeProperty = ontologyClass.getObjectProperty(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}
	
	@Test
	public void testGetObjectProperty() {
		OntologyObjectProperty expectedObjectProperty = expectedObjectPropertyList.get(2);
		OntologyObjectProperty actualObjectProperty = ontologyClass.getObjectProperty(2);
		int expectedSize = expectedObjectPropertyList.size();
		int actualSize = ontologyClass.getObjectPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualObjectProperty, is(equalTo(expectedObjectProperty)));
	}
	
	@Test
	public void testGetObjectPropertiesCount() {
		int expectedSize = expectedObjectPropertyList.size();
		int actualSize = ontologyClass.getObjectPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	/**
	 * Creates a list with four elements. The four elements in the list are the four elements in expectedDatatypePropertyList. This
	 * list is added to the data property list in ontologyClass. For comparing reasons, the same four elements are add to
	 * expectedDatatypePropertyList. So, expectedDatatypePropertyList has also eight elements. 
	 */
	@Test
	public void testAddDatatypeProperties() {
		// Prepare the test data.
		int expectedSize = expectedDatatypePropertyList.size();
		final DatatypePropertyCollection datatypePropertyList = new DatatypePropertyCollectionImpl();
		
		/* After this loop expectedDatatypePropertyList has eight elements and datatypePropertyList has four elements.
		 *  datatypePropertyList is added to the datatype properties of the class.
		 */
		for (int i = 0; i < expectedSize; i++) {
			final OntologyDatatypeProperty expectedDatatypeProperty = expectedDatatypePropertyList.get(i);
			datatypePropertyList.addDatatypeProperty(expectedDatatypeProperty);
			expectedDatatypePropertyList.add(expectedDatatypeProperty);
		}

		// Check the result of the loop.
		assertThat(expectedDatatypePropertyList.size(), is(equalTo(8)));
		assertThat(datatypePropertyList.getPropertiesCount(), is(equalTo(4)));
		
		// Test the method.
		ontologyClass.addDatatypeProperties(datatypePropertyList);
		
		// Check the results.
		final int actualSize = ontologyClass.getDatatypePropertiesCount();
		expectedSize = expectedDatatypePropertyList.size();
		assertThat(actualSize, is(equalTo(expectedSize)));
		
		for (int i = 0; i < expectedSize; i++) {
			final OntologyDatatypeProperty expectedDatatypeProperty = expectedDatatypePropertyList.get(i);
			final OntologyDatatypeProperty actualDatatypeProperty = ontologyClass.getDatatypeProperty(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}		
	}
	
	@Test
	public void testAddSubclass() {
		OntologyClass subclass5 = new OntologyClassImpl(new NamespaceImpl("myNamespace"), new LocalNameImpl("class5"));
		ontologyClass.addSubClass(subclass5);
		int expectedSize = expectedSubclasses.size() + 1;
		int actualSize = ontologyClass.getSubclassCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		OntologyClass actualSubclass = ontologyClass.getSubClassAt(expectedSize - 1);
		assertThat(actualSubclass, is(equalTo(subclass5)));
	}
	
	@Test
	public void testGetSubclassCount() {
		int expectedSize = expectedSubclasses.size();
		int actualSize = ontologyClass.getSubclassCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	@Test
	public void testHasSubclasses() {
		boolean hasSubclasses = ontologyClass.hasSubclasses();
		assertTrue(hasSubclasses);
	}
	
	@Test
	public void testHasNoSubclasses() {
		OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("namespace"), new LocalNameImpl("localname"));
		boolean hasSubclasses = ontologyClass.hasSubclasses();
		assertFalse(hasSubclasses);
	}
	
	@Test
	public void testHasDatatypeProperties() {
		boolean hasDatatypeProperties = ontologyClass.hasDatatypeProperties();
		assertTrue(hasDatatypeProperties);
	}
	
	@Test
	public void testHasNoDatatypeProperties() {
		OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("namespace"), new LocalNameImpl("localname"));
		boolean hasDatatypeProperties = ontologyClass.hasDatatypeProperties();
		assertFalse(hasDatatypeProperties);
	}
	
	@Test
	public void testHasObjectProperties() {
		boolean hasObjectProperties = ontologyClass.hasObjectProperties();
		assertTrue(hasObjectProperties);
	}
	
	@Test
	public void testHasNoObjectProperties() {
		OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("namespace"), new LocalNameImpl("localname"));
		boolean hasObjectProperties = ontologyClass.hasObjectProperties();
		assertFalse(hasObjectProperties);
	}
	
	@Test
	public void testIndexOfSubclass() {
		int actualIndex = ontologyClass.indexOfSubclass("class2");
		assertThat(actualIndex, is(equalTo(1)));
	}
	
	@Test
	public void testClassnameContains() {
		boolean classnameContained = ontologyClass.classnameContains("my");
		assertTrue(classnameContained);
	}
	
	@Test
	public void testClassnameNotContained() {
		boolean classnameContained = ontologyClass.classnameContains("xxx");
		assertFalse(classnameContained);
	}
	
	@Test
	public void testGetNamespace() {
		Namespace actualNamespace = ontologyClass.getNamespace();
		assertThat(actualNamespace.toString(), is(equalTo("myNamespace")));
	}
	
	@Test
	public void testGetLocalname() {
		LocalName actualLocalname = ontologyClass.getLocalName();
		assertThat(actualLocalname.toString(), is(equalTo("myClass")));
	}
	
	@Test
	public void testGetFullName() {
		String actualLocalname = ontologyClass.getFullName();
		assertThat(actualLocalname, is(equalTo("myNamespace#myClass")));
	}
}
