package de.feu.kdmp4.packagingtoolkit.server.configuration;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.ConfigurationOperationsPropertiesImpl;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;

/**
 * Spring configuration class for the operation classes.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class MockedOperationsConfiguration {
	/**
	 * Configures an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.
	 * ConfigurationOperations}.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.
	 * ConfigurationOperations}.
	 */
	//@Primary
	@Bean(name = "configurationOperations")
	public ConfigurationOperations getConfigurationOperations() {
		return Mockito.mock(ConfigurationOperationsPropertiesImpl.class);
	}
	
	/**
	 * Configures an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.
	 * SerializationOntologyOperations}.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.
	 * SerializationOntologyOperations}.
	 */
	/*@Bean(name = "serializationOntologyOperations")
	public SerializationOntologyOperations getSerializationOntologyOperations() {
		return new SerializationOntologyOperationsOwlApiImpl();
	}*/
	
	/**
	 * Configures an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.
	 * OntologyOperations}.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.classes.
	 * OntologyOperationsJenaImpl}.
	 */
	//@Primary
	@Profile("propertiesTest")
	@Bean(name = "ontologyOperations")
	public OntologyOperations getOntologyOperations() {
		return Mockito.mock(OntologyOperations.class);
	}
	
	/**
	 * Configures an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.
	 * StorageOperations}.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.
	 * StorageOperations}.
	 */
	/*@Bean(name = "storageOperations")
	public StorageOperations getStorageOperations() {
		return new StorageOperationsImpl();
	}*'/
	
	/**
	 * Configures an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.
	 * TripleStoreOperations}.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.serialization.interfaces.
	 * TripleStoreOperations}.
	 */
	/*@Bean(name = "tripleStoreOperations")
	public TripleStoreOperations getTripleStoreOperations() {
		return new TripleStoreOperationsRdf4JImpl();
	}*/
}