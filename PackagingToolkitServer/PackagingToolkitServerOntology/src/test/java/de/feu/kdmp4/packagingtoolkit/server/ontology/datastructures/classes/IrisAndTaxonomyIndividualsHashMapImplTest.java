package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;

public class IrisAndTaxonomyIndividualsHashMapImplTest {
	/**
	 * Tests adding a new individual to the map. The iri does not exist in the map yet. 
	 */
	@Test
	public void testAddIndividual_collectionDoesNotExistYet() {
		final IrisAndTaxonomyIndividualsHashMapImpl map = new IrisAndTaxonomyIndividualsHashMapImpl();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Iri iri = new IriImpl(new Uuid());
		map.addTaxonomyIndividal(iri, taxonomyIndividual);
		
		final Optional<TaxonomyIndividualCollection> optionalWithTaxonomyIndividualCollection = map.getTaxonomyIndividuals(iri);
		final TaxonomyIndividualCollection taxonomyIndividualCollection = optionalWithTaxonomyIndividualCollection.get();
		assertTrue(optionalWithTaxonomyIndividualCollection.isPresent());
		assertThat(taxonomyIndividualCollection.getTaxonomyIndividualCount(), is(equalTo(1)));
	}
	
	/**
	 * Tests adding a new individual to the map. The iri already exists in the map. 
	 */
	@Test
	public void testAddIndividual_collectionExists() {
		final IrisAndTaxonomyIndividualsHashMapImpl map = new IrisAndTaxonomyIndividualsHashMapImpl();
		final Iri iri = new IriImpl(new Uuid());
		final TaxonomyIndividual taxonomyIndividual1 = TaxonomyTestApi.createTaxonomyIndividual();
		map.addTaxonomyIndividal(iri, taxonomyIndividual1);
		final TaxonomyIndividual taxonomyIndividual2 = TaxonomyTestApi.createTaxonomyIndividual();
		map.addTaxonomyIndividal(iri, taxonomyIndividual2);
		
		final Optional<TaxonomyIndividualCollection> optionalWithTaxonomyIndividualCollection = map.getTaxonomyIndividuals(iri);
		final TaxonomyIndividualCollection taxonomyIndividualCollection = optionalWithTaxonomyIndividualCollection.get();
		assertTrue(optionalWithTaxonomyIndividualCollection.isPresent());
		assertThat(taxonomyIndividualCollection.getTaxonomyIndividualCount(), is(equalTo(2)));
	}
}
