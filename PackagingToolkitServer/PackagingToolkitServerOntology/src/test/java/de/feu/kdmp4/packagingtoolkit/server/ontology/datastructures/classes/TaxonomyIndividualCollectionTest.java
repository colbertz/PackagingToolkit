package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;

public class TaxonomyIndividualCollectionTest {
	
	/**
	 * Adds three taxonomy individuals to the collection and tests after every insertion if the process was sucessful.
	 */
	@Test
	public void testAddTaxonomyIndividual() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = new TaxonomyIndividualCollectionImpl();
		assertThat(taxonomyIndividualCollection.getTaxonomyIndividualCount(), is(equalTo(0)));
		
		final TaxonomyIndividual taxonomyIndividual1 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual1);
		assertThat(taxonomyIndividualCollection.getTaxonomyIndividualCount(), is(equalTo(1)));
		final Optional<TaxonomyIndividual> optionalWithExpectedTaxonomyIndividual1 = taxonomyIndividualCollection.getTaxonomyIndividual(0);
		assertTrue(optionalWithExpectedTaxonomyIndividual1.isPresent());
		final TaxonomyIndividual expectedTaxonomyIndividual1 = optionalWithExpectedTaxonomyIndividual1.get();
		assertThat(expectedTaxonomyIndividual1, is(equalTo(taxonomyIndividual1)));
		
		final TaxonomyIndividual taxonomyIndividual2 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual2);
		assertThat(taxonomyIndividualCollection.getTaxonomyIndividualCount(), is(equalTo(2)));
		final Optional<TaxonomyIndividual> optionalWithExpectedTaxonomyIndividual2 = taxonomyIndividualCollection.getTaxonomyIndividual(1);
		assertTrue(optionalWithExpectedTaxonomyIndividual2.isPresent());
		final TaxonomyIndividual expectedTaxonomyIndividual2 = optionalWithExpectedTaxonomyIndividual2.get();
		assertThat(expectedTaxonomyIndividual2, is(equalTo(taxonomyIndividual2)));
		
		final TaxonomyIndividual taxonomyIndividual3 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual3);
		assertThat(taxonomyIndividualCollection.getTaxonomyIndividualCount(), is(equalTo(3)));
		final Optional<TaxonomyIndividual> optionalWithExpectedTaxonomyIndividual3 = taxonomyIndividualCollection.getTaxonomyIndividual(2);
		assertTrue(optionalWithExpectedTaxonomyIndividual3.isPresent());
		final TaxonomyIndividual expectedTaxonomyIndividual3 = optionalWithExpectedTaxonomyIndividual3.get();
		assertThat(expectedTaxonomyIndividual3, is(equalTo(taxonomyIndividual3)));
	}
	
	/**
	 * Tests if the second taxonomy individual is determined from the collection correctly.
	 */
	@Test
	public void testGetTaxonomyIndividual_resultTaxonomyIndividualFound() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
		final TaxonomyIndividual expectedTaxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividualCollection);
		final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = taxonomyIndividualCollection.getTaxonomyIndividual(1);
		assertTrue(optionalWithTaxonomyIndividual.isPresent());
		final TaxonomyIndividual actualTaxonomyIndividual = optionalWithTaxonomyIndividual.get();
		assertThat(actualTaxonomyIndividual, is(equalTo(expectedTaxonomyIndividual)));
	}
	
	/**
	 * Tests if correct exception is thrown if the index does not exist in the list.
	 */
	@Test(expected = ListException.class)
	public void testGetTaxonomyIndividual_resultTaxonomyIndividualNotFound() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividualCollection);
		taxonomyIndividualCollection.getTaxonomyIndividual(11);
	}
	
	/**
	 * Tests the two methods hasNextIndividualInTaxonomy() and nextIndividualInTaxonomy. The api class creates a collection with ten elements.
	 * Six elements are assigned to the scheme we are looking for. The test checks for every determined individual if it is assigned to the scheme.
	 */
	@Test
	public void testNextIndividualInTaxonomy() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividualCollectionWithTenIndividuals(taxonomyIndividualCollection);
		final Namespace namespace = taxonomyIndividual.getInScheme();
		final Taxonomy taxonomy = new TaxonomyImpl(taxonomyIndividual, "bla", namespace);
		int counter = 0;
		
		while (taxonomyIndividualCollection.hasNextIndividualInTaxonomy(taxonomy)) {
			final Optional<TaxonomyIndividual> optionalWithIindividual = taxonomyIndividualCollection.nextIndividualInTaxonomy(taxonomy);
			final TaxonomyIndividual individual = optionalWithIindividual.get();
			
			assertThat(individual.getInScheme(), is(equalTo(namespace)));
			counter++;
		}
		
		assertThat(counter, is(equalTo(6)));
	}
	
	/**
	 * Tests the two methods hasNextIndividualInTaxonomy() and nextIndividualInTaxonomy. The api class creates a collection with three elements.
	 * There are not elements with the desired namespace. 
	 */
	@Test
	public void testNextIndividualInTaxonomy_noIndividualsFound() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividualCollection);
		final Namespace namespace  = new NamespaceImpl("http://the-other-namespace");
		final Taxonomy taxonomy = new TaxonomyImpl(taxonomyIndividual, "bla", namespace);
		int counter = 0;
		
		while (taxonomyIndividualCollection.hasNextIndividualInTaxonomy(taxonomy)) {
			final Optional<TaxonomyIndividual> optionalWithIindividual = taxonomyIndividualCollection.nextIndividualInTaxonomy(taxonomy);
			final TaxonomyIndividual individual = optionalWithIindividual.get();
			
			assertThat(individual.getInScheme(), is(equalTo(namespace)));
			counter++;
		}
		
		assertThat(counter, is(equalTo(0)));
	}
	
	@Test
	public void testRemoveIndividualsOfTaxonomy() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividualCollectionWithTenIndividuals(taxonomyIndividualCollection);
		final Namespace namespace = taxonomyIndividual.getInScheme();
		final Taxonomy taxonomy = new TaxonomyImpl(taxonomyIndividual, "abc", namespace);
		taxonomyIndividualCollection.removeIndividualsOfTaxonomy(taxonomy);
		final int sizeOfCollection = taxonomyIndividualCollection.getTaxonomyIndividualCount();
		assertThat(sizeOfCollection, is(equalTo(4)));
	}
}
