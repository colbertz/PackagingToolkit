package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@Category(NonSpringTests.class)
public class OntologyDatePropertyTest {
	private static final String PROPERTY_ID = "datatypeProperty#dateProperty";
	private static final String PROPERTY_LABEL = "dateProperty";
	private static final String PROPERTY_RANGE = "date";
	
	private OntologyDateProperty ontologyDateProperty;
	
	@Before
	public void setUp() {
		ontologyDateProperty = new OntologyDatePropertyImpl(PROPERTY_ID, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsDate() {
		assertFalse(ontologyDateProperty.isBooleanProperty());
		assertFalse(ontologyDateProperty.isByteProperty());
		assertTrue(ontologyDateProperty.isDateProperty());
		assertFalse(ontologyDateProperty.isDateTimeProperty());
		assertFalse(ontologyDateProperty.isDoubleProperty());
		assertFalse(ontologyDateProperty.isDurationProperty());
		assertFalse(ontologyDateProperty.isFloatProperty());
		assertFalse(ontologyDateProperty.isIntegerProperty());
		assertFalse(ontologyDateProperty.isLongProperty());
		assertFalse(ontologyDateProperty.isShortProperty());
		assertFalse(ontologyDateProperty.isStringProperty());
		assertFalse(ontologyDateProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		LocalDate expectedDate = LocalDate.now();
		ontologyDateProperty.setPropertyValue(expectedDate);
		LocalDate actualDate = ontologyDateProperty.getPropertyValue();
		assertEquals(expectedDate, actualDate);
	}

}
