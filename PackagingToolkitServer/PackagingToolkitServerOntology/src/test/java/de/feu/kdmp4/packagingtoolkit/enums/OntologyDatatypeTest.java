package de.feu.kdmp4.packagingtoolkit.enums;

import static org.junit.Assert.*;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;

public class OntologyDatatypeTest {

	@Test
	public void testIsBoolean() {
		String propertyRange = "Boolean";
		boolean isBoolean = OntologyDatatype.isBoolean(propertyRange);
		assertTrue(isBoolean);
	}

	@Test
	public void testIsByte() {
		String propertyRange = "Byte";
		boolean isByte = OntologyDatatype.isByte(propertyRange);
		assertTrue(isByte);
	}
	
	@Test
	public void testIsDate() {
		String propertyRange = "Date";
		boolean isDate = OntologyDatatype.isDate(propertyRange);
		assertTrue(isDate);
	}
	
	@Test
	public void testIsDateTime() {
		String propertyRange = "DateTime";
		boolean isDateTime = OntologyDatatype.isDateTime(propertyRange);
		assertTrue(isDateTime);
	}
	
	@Test
	public void testIsDouble() {
		String propertyRange = "Double";
		boolean isDouble = OntologyDatatype.isDouble(propertyRange);
		assertTrue(isDouble);
	}
	
	@Test
	public void testIsDuration() {
		String propertyRange = "Duration";
		boolean isDuration = OntologyDatatype.isDuration(propertyRange);
		assertTrue(isDuration);
	}
	
	@Test
	public void testIsFloat() {
		String propertyRange = "Float";
		boolean isFloat = OntologyDatatype.isFloat(propertyRange);
		assertTrue(isFloat);
	}
	
	@Test
	public void testIsInteger() {
		String propertyRange = "Integer";
		boolean isInteger = OntologyDatatype.isInteger(propertyRange);
		assertTrue(isInteger);
	}
	
	@Test
	public void testIsLong() {
		String propertyRange = "Long";
		boolean isLong = OntologyDatatype.isLong(propertyRange);
		assertTrue(isLong);
	}
	
	@Test
	public void testIsShort() {
		String propertyRange = "Short";
		boolean isShort = OntologyDatatype.isShort(propertyRange);
		assertTrue(isShort);
	}
	
	@Test
	public void testIsString() {
		String propertyRange = "String";
		boolean isString = OntologyDatatype.isString(propertyRange);
		assertTrue(isString);
	}
	
	@Test
	public void testIsTime() {
		String propertyRange = "Time";
		boolean isTime = OntologyDatatype.isTime(propertyRange);
		assertTrue(isTime);
	}
}
