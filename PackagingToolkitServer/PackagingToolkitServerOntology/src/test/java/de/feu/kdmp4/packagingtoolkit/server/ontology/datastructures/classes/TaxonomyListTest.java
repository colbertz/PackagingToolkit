package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import org.junit.Test;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;

public class TaxonomyListTest {
	/**
	 * Adds three taxonomies to the collection and tests after every insertion if the process was sucessful.
	 */
	@Test
	public void testAddTaxonomy() {
		final TaxonomyCollection taxonomyCollection = new TaxonomyListImpl();
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(0)));
		
		final Taxonomy taxonomy1 = TaxonomyTestApi.createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy1);
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(1)));
		final Optional<Taxonomy> optionalWithExpectedTaxonomy1 = taxonomyCollection.getTaxonomy(0);
		assertTrue(optionalWithExpectedTaxonomy1.isPresent());
		final Taxonomy expectedTaxonomy1 = optionalWithExpectedTaxonomy1.get();
		assertThat(expectedTaxonomy1, is(equalTo(taxonomy1)));
		
		final Taxonomy taxonomy2 = TaxonomyTestApi.createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy2);
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(2)));
		final Optional<Taxonomy> optionalWithExpectedTaxonomy2 = taxonomyCollection.getTaxonomy(1);
		assertTrue(optionalWithExpectedTaxonomy2.isPresent());
		final Taxonomy expectedTaxonomy2 = optionalWithExpectedTaxonomy2.get();
		assertThat(expectedTaxonomy2, is(equalTo(taxonomy2)));
		
		final Taxonomy taxonomy3 = TaxonomyTestApi.createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy3);
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(3)));
		final Optional<Taxonomy> optionalWithExpectedTaxonomy3 = taxonomyCollection.getTaxonomy(2);
		assertTrue(optionalWithExpectedTaxonomy3.isPresent());
		final Taxonomy expectedTaxonomy3 = optionalWithExpectedTaxonomy3.get();
		assertThat(expectedTaxonomy3, is(equalTo(taxonomy3)));
	}
	
	/**
	 * Tests if the second taxonomy is determined from the collection correctly.
	 */
	@Test
	public void testGetTaxonomy_resultTaxonomyFound() {
		final TaxonomyCollection taxonomyCollection = OntologyModelFactory.createEmptyTaxonomyCollection();
		final Taxonomy expectedTaxonomy = TaxonomyTestApi.createTaxonomyCollectionWithThreeTaxonomies(taxonomyCollection);
		final Optional<Taxonomy> optionalWithTaxonomy = taxonomyCollection.getTaxonomy(1);
		assertTrue(optionalWithTaxonomy.isPresent());
		final Taxonomy actualTaxonomy = optionalWithTaxonomy.get();
		assertThat(actualTaxonomy, is(equalTo(expectedTaxonomy)));
	}
	
	/**
	 * Tests if correct exception is thrown if the index does not exist in the list.
	 */
	@Test(expected = ListException.class)
	public void testGetTaxonomy_resultTaxonomyNotFound() {
		final TaxonomyCollection taxonomyCollection = OntologyModelFactory.createEmptyTaxonomyCollection();
		TaxonomyTestApi.createTaxonomyCollectionWithThreeTaxonomies(taxonomyCollection);
		taxonomyCollection.getTaxonomy(11);
	}
	
	/**
	 * Tests if the second taxonomy is determined from the collection correctly by its namespace.
	 */
	@Test
	public void testFindTaxonomyByNamespace_resultTaxonomyFound() {
		final TaxonomyCollection taxonomyCollection = OntologyModelFactory.createEmptyTaxonomyCollection();
		final Taxonomy expectedTaxonomy = TaxonomyTestApi.createTaxonomyCollectionWithThreeTaxonomies(taxonomyCollection);
		final Namespace namespace = expectedTaxonomy.getNamespace();
		final Optional<Taxonomy> optionalWithTaxonomy = taxonomyCollection.findTaxonomyByNamespace(namespace);
		assertTrue(optionalWithTaxonomy.isPresent());
		final Taxonomy actualTaxonomy = optionalWithTaxonomy.get();
		assertThat(actualTaxonomy, is(equalTo(expectedTaxonomy)));
	}
	
	/**
	 * Tests if an empty optional is determined if the namespace does not exist in the list.
	 */
	@Test
	public void testFindTaxonomyByNamespace_resultTaxonomyNotFound() {
		final TaxonomyCollection taxonomyCollection = OntologyModelFactory.createEmptyTaxonomyCollection();
		final Taxonomy taxonomy = TaxonomyTestApi.createTaxonomy();
		final Namespace namespace = taxonomy.getNamespace();
		TaxonomyTestApi.createTaxonomyCollectionWithThreeTaxonomies(taxonomyCollection);
		final Optional<Taxonomy> optionalWithTaxonomy = taxonomyCollection.findTaxonomyByNamespace(namespace);
		assertFalse(optionalWithTaxonomy.isPresent());
	}
}
