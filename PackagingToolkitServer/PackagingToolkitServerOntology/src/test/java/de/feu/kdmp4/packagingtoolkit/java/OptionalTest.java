package de.feu.kdmp4.packagingtoolkit.java;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;

/**
 * These test are experiments with the new Optional class.
 * @author Christopher Olbertz
 *
 */
public class OptionalTest {
	private static final String INIT_STRING = "Answer";
	
	private Optional<String> myOptionalString;
	
	@Before
	public void setUp() {
		myOptionalString = Optional.of(INIT_STRING);
	}
	
	/**
	 * The get() method of Optional returns the value that was saved into the
	 * Optional.
	 */
	@Test
	public void testNonEmptyOptional() {
		String stringOfOptional = myOptionalString.get();
		assertThat(stringOfOptional, is(equalTo(INIT_STRING)));
	}

	/**
	 * An Optional saves the given string value.
	 */
	@Test
	public void testOfNullableWithNotNull() {
		Optional<String> notNullOptional = Optional.ofNullable(INIT_STRING); 
		String stringOfOptional = notNullOptional.get();
		assertThat(stringOfOptional, is(equalTo(INIT_STRING)));
	}

	/**
	 * If the method Optional.ofNullable receives a null value an exception is thrown.
	 */
	@Test(expected = NoSuchElementException.class)
	public void testOfNullableWithNull() {
		String nullString = null;
		Optional<String> notNullOptional = Optional.ofNullable(nullString); 
		notNullOptional.get();
	}
	
	/**
	 * Creates an empty Optional object. It has no value.
	 */
	@Test
	public void whenCreatesEmptyOptional_thenCorrect() {
	    Optional<String> empty = Optional.empty();
	    assertFalse(empty.isPresent());
	}
	
	/**
	 * An Optional is created with the of() method. It contains a value.
	 */
	@Test
	public void givenNonNull_whenCreatesNonNullable_thenCorrect() {
	    String name = "stl";
	    Optional.of(name);
	}
	
	/**
	 * The value that is given with the of() method is ready for use.
	 */
	@Test
	public void givenNonNull_whenCreatesOptional_thenCorrect() {
	    Optional<String> opt = Optional.of(INIT_STRING);
	    assertEquals("Optional[" + INIT_STRING +"]", opt.toString());
	}
	
	/**
	 * We cannot give null as argument for of. Then we get a NullPointerException.
	 */
	@Test(expected = NullPointerException.class)
	public void givenNull_whenThrowsErrorOnCreate_thenCorrect() {
	    String name = null;
	     Optional.of(name);
	}
	
	/**
	 * If we except that there are null values given as string, the Optional can be 
	 * created with the ofNullable() method.
	 */
	@Test
	public void givenNonNull_whenCreatesNullable_thenCorrect() {
	    Optional<String> opt = Optional.ofNullable(INIT_STRING);
	    assertEquals("Optional[" + INIT_STRING +"]", opt.toString());
	}
	
	/**
	 * If we work with ofNullable() we can give a null value to the Optional, but
	 * we get an empty Optional object, not a null object.
	 */
	@Test
	public void givenNull_whenCreatesNullable_thenCorrect() {
	    Optional<String> opt = Optional.ofNullable(null);
	    assertEquals("Optional.empty", opt.toString());
	}

	/**
	 * We can check if a value is present in the Optional. 
	 */
	@Test
	public void givenOptional_whenIsPresentWorks_thenCorrect() {
	    Optional<String> opt = Optional.of(INIT_STRING);
	    assertTrue(opt.isPresent());
	}
	
	/**
	 * Now we give a null value as argument. isPresent() returns false.
	 */
	@Test
	public void givenOptional_whenIsPresentWorks_thenCorrect_withNullValue() {
	    Optional<String> opt = Optional.ofNullable(null);
	    assertFalse(opt.isPresent());
	}
	
	/**
	 * We want to perform an action on an Optional if there is a value. With Java 8 there 
	 * is no if / else necessary.
	 */
	@Test
	public void givenOptional_whenIfPresentWorks_thenCorrect() {
	    Optional<String> opt = Optional.of(INIT_STRING);
	 
	    opt.ifPresent(name -> System.out.println(name.length()));
	}
	
	/**
	 * We can provide an other value if there is no value present in the Optional.
	 */
	@Test
	public void whenOrElseWorks_thenCorrect() {
	    String nullName = null;
	    Optional<String> testOptional = Optional.ofNullable(nullName);
	    testOptional.orElse("john");
	    String name = Optional.ofNullable(nullName).orElse("john");
	    assertEquals("john", name);
	}
	
	/**
	 * With orElseGet() we can provide a functional supplier interface. Its return value
	 * is the value that is returned as value of the Optional.
	 */
	@Test
	public void whenOrElseGetWorks_thenCorrect() {
	    String nullName = null;
	    String name = Optional.ofNullable(nullName).orElseGet(() -> "john");
	    assertEquals("john", name);
	}
	
	/**
	 * It is also possible to throw an exception if the value is not present.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void whenOrElseThrowWorks_thenCorrect() {
	    String nullName = null;
	    Optional.ofNullable(nullName).orElseThrow(IllegalArgumentException::new);
	}
}
