package de.feu.kdmp4.packagingtoolkit.server.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.ConfigurationOperationsPropertiesImpl;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyOperationsJenaImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;

/**
 * Spring configuration class for the operation classes.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class OperationsConfiguration {
	/**
	 * Configures an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.
	 * ConfigurationOperations}.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.
	 * ConfigurationOperations}.
	 */
	@Bean(name = "configurationOperations")
	public ConfigurationOperations getConfigurationOperations() {
		return new ConfigurationOperationsPropertiesImpl();
	}
	
	/**
	 * Configures an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.
	 * OntologyOperations}.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.classes.
	 * OntologyOperationsJenaImpl}.
	 */
	@Bean(name = "ontologyOperationsTest")
	public OntologyOperations getOntologyOperations() {
		return new OntologyOperationsJenaImpl();
	}
}