package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.DataStructureFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

public class OntologyClassesHashMapTest {
	private static final String NAMESPACE = "abc";
	private static final String CLASSNAME_1 = "A";
	private static final String CLASSNAME_2 = "B";
	private static final String CLASSNAME_3 = "C";
	private static final String CLASSNAME_4 = "D";
	
	private static final String PROPERTY_1 = "X";
	private static final String PROPERTY_2 = "Y";
	private static final String PROPERTY_3 = "Z";
	
	private OntologyClass ontologyClass1;
	private OntologyClass ontologyClass2;
	private OntologyClass ontologyClass3;
	private OntologyClass ontologyClass4;
	private OntologyClassesMap ontologyClassesMap;
	
	private OntologyProperty property1;
	private OntologyProperty property2;
	private OntologyProperty property3;
	
	private OntologyModelFactory ontologyModelFactory;
	private DataStructureFactory dataStructureFactory;
	private PropertyFactory propertyFactory;
	
	@Before
	public void setUp() {
		ontologyModelFactory = new OntologyModelFactory();
		dataStructureFactory = new DataStructureFactory();
		propertyFactory = new PropertyFactory();
		
		ontologyClass1 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_1));
		ontologyClass2 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_2));
		ontologyClass3 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_3));
		ontologyClass4 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_4));
		ontologyClassesMap = dataStructureFactory.createOntologyClassesMap();
		property1 = propertyFactory.getProperty("boolean", PROPERTY_1, PROPERTY_1);
		property1.setPropertyValue(true);
		property2 = propertyFactory.getProperty("string", PROPERTY_2, PROPERTY_2);
		property2.setPropertyValue("bla");
		property3 = propertyFactory.getProperty("string", PROPERTY_3, PROPERTY_3);
		property3.setPropertyValue("blub");
	}
	
	@Test
	public void testAddClass() {
		ontologyClassesMap.addClass(ontologyClass1);
		assertThat(ontologyClassesMap.getOntologyClassesCount(), is(equalTo(1)));
		OntologyClass firstClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_1);
		assertThat(firstClass, is(equalTo(ontologyClass1)));
		
		ontologyClassesMap.addClass(ontologyClass2);
		assertThat(ontologyClassesMap.getOntologyClassesCount(), is(equalTo(2)));
		OntologyClass secondClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_2);
		assertThat(secondClass, is(equalTo(ontologyClass2)));
		
		ontologyClassesMap.addClass(ontologyClass3);
		assertThat(ontologyClassesMap.getOntologyClassesCount(), is(equalTo(3)));
		OntologyClass thirdClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_3);
		assertThat(thirdClass, is(equalTo(ontologyClass3)));
	}
	
	@Test
	public void testGetOntologyClassesCount() {
		ontologyClassesMap.addClass(ontologyClass1);
		ontologyClassesMap.addClass(ontologyClass2);
		ontologyClassesMap.addClass(ontologyClass3);
		ontologyClassesMap.addClass(ontologyClass4);
		int size = ontologyClassesMap.getOntologyClassesCount();
		assertThat(size, is(equalTo(4)));
	}
	
	@Test
	public void testGetOntologyClass() {
		ontologyClassesMap.addClass(ontologyClass1);
		ontologyClassesMap.addClass(ontologyClass2);
		ontologyClassesMap.addClass(ontologyClass3);
		ontologyClassesMap.addClass(ontologyClass4);
		
		OntologyClass firstClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_1);
		assertThat(firstClass, is(equalTo(ontologyClass1)));
		
		OntologyClass secondClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_2);
		assertThat(secondClass, is(equalTo(ontologyClass2)));
		
		OntologyClass thirdClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_3);
		assertThat(thirdClass, is(equalTo(ontologyClass3)));
		
		OntologyClass fourthClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_4);
		assertThat(fourthClass, is(equalTo(ontologyClass4)));
	}

	@Test
	public void testAddProperty() {
		ontologyClassesMap.addProperty(NAMESPACE + "#" + CLASSNAME_1, property1);
		ontologyClassesMap.addProperty(NAMESPACE + "#" + CLASSNAME_2, property2);
		ontologyClassesMap.addProperty(NAMESPACE + "#" + CLASSNAME_2, property3);
		
		OntologyClass firstClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_1);
		assertThat(firstClass.getDatatypePropertiesCount(), is(equalTo(1)));
		OntologyProperty firstProperty = firstClass.getDatatypeProperty(0);
		assertThat(firstProperty, is(equalTo(property1)));
		
		OntologyClass secondClass = ontologyClassesMap.getOntologyClass(NAMESPACE + "#" + CLASSNAME_2);
		assertThat(secondClass.getDatatypePropertiesCount(), is(equalTo(2)));
		OntologyProperty secondProperty = secondClass.getDatatypeProperty(0);
		assertThat(secondProperty, is(equalTo(property2)));
		OntologyProperty thirdProperty = secondClass.getDatatypeProperty(1);
		assertThat(thirdProperty, is(equalTo(property3)));
	}
}
