package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class ObjectPropertyListTest {
	private ObjectPropertyCollection objectPropertyList;
	private List<OntologyObjectProperty> expectedList;
	
	@Before
	public void setUp() {
		expectedList = new ArrayList<>();
		objectPropertyList = new ObjectPropertyCollectionImpl();
		
		OntologyClass ontologyClass = new OntologyClassImpl(ServerModelFactory.createNamespace("myNamespace"), ServerModelFactory.createLocalName("myClass"));
		OntologyObjectProperty objectProperty1 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty2 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty3 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty4 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		objectPropertyList.addObjectProperty(objectProperty1);
		objectPropertyList.addObjectProperty(objectProperty2);
		objectPropertyList.addObjectProperty(objectProperty3);
		objectPropertyList.addObjectProperty(objectProperty4);
		
		expectedList.add(objectProperty1);
		expectedList.add(objectProperty2);
		expectedList.add(objectProperty3);
		expectedList.add(objectProperty4);
	}
	
	@Test
	public void testAddObjectPropery() {
		OntologyClass ontologyClass = new OntologyClassImpl(ServerModelFactory.createNamespace("myNamespace"), ServerModelFactory.createLocalName("myClass"));
		OntologyObjectProperty objectProperty = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		expectedList.add(objectProperty);
		int expectedSize = expectedList.size();
		
		objectPropertyList.addObjectProperty(objectProperty);
		int actualSize = objectPropertyList.getPropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			OntologyObjectProperty expectedDatatypeProperty = expectedList.get(i);
			OntologyObjectProperty actualDatatypeProperty = objectPropertyList.getObjectProperty(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}
	
	@Test
	public void testGetObjectProperty() {
		OntologyObjectProperty expectedObjectProperty = expectedList.get(2);
		OntologyObjectProperty actualObjectProperty = objectPropertyList.getObjectProperty(2);
		int expectedSize = expectedList.size();
		int actualSize = objectPropertyList.getPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualObjectProperty, is(equalTo(expectedObjectProperty)));
	}
	
	@Test
	public void testGetObjectPropertiesCount() {
		int expectedSize = expectedList.size();
		int actualSize = objectPropertyList.getPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
}
