package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;

public class TaxonomyTest {
	/**
	 * Tests if two top level concepts can be added to the taxonomy.
	 */
	@Test
	public void testAddTopConcept() {
		final Taxonomy taxonomy = TaxonomyTestApi.createTaxonomy();
		final TaxonomyIndividual topLevelIndividual1 = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual topLevelIndividual2 = TaxonomyTestApi.createTaxonomyIndividual();
		
		taxonomy.addTopConceptToTaxonomy(topLevelIndividual1);
		final TaxonomyIndividual rootIndividual = taxonomy.getRootIndividual();
		boolean containsTopLevel = rootIndividual.containsNarrower(topLevelIndividual1);
		assertTrue(containsTopLevel);
		assertThat(rootIndividual.getNarrowerCount(), is(equalTo(1)));
		
		taxonomy.addTopConceptToTaxonomy(topLevelIndividual2);
		containsTopLevel = rootIndividual.containsNarrower(topLevelIndividual2);
		assertTrue(containsTopLevel);
		assertThat(rootIndividual.getNarrowerCount(), is(equalTo(2)));
	}
	
	@Test
	public void testBuildTaxonomy() {
		final Taxonomy taxonomy = TaxonomyTestApi.createTaxonomy();
		final TaxonomyIndividual topLevelIndividual1 = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual topLevelIndividual2 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomy.addTopConceptToTaxonomy(topLevelIndividual1);
		taxonomy.addTopConceptToTaxonomy(topLevelIndividual2);
		
		final TaxonomyIndividual taxonomyIndividual1 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual1, topLevelIndividual1.getIri());
		final TaxonomyIndividual taxonomyIndividual2 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual2, topLevelIndividual1.getIri());
		
		final TaxonomyIndividual taxonomyIndividual3 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual3, topLevelIndividual2.getIri());
 
		final TaxonomyIndividual taxonomyIndividual4 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual4, taxonomyIndividual2.getIri());
		
		final TaxonomyIndividual taxonomyIndividual5 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual5, taxonomyIndividual4.getIri());
		
		assertThat(taxonomy.countTaxonomyIndividuals(), is(equalTo(7)));
		assertThat(taxonomyIndividual5.getNarrowerCount(), is(equalTo(0)));
		assertThat(taxonomyIndividual3.getNarrowerCount(), is(equalTo(0)));
		
		assertThat(taxonomyIndividual4.getNarrowerCount(), is(equalTo(1)));
		final boolean Individual4ContainsIndividual5 = taxonomyIndividual4.containsNarrower(taxonomyIndividual5);
		assertTrue(Individual4ContainsIndividual5);
		
		assertThat(taxonomyIndividual2.getNarrowerCount(), is(equalTo(1)));
		final boolean individual2ContainsIndividual4 = taxonomyIndividual2.containsNarrower(taxonomyIndividual4);
		assertTrue(individual2ContainsIndividual4);
		
		assertThat(topLevelIndividual1.getNarrowerCount(), is(equalTo(2)));
		final boolean topIndividual1ContainsIndividual1 = topLevelIndividual1.containsNarrower(taxonomyIndividual1);
		assertTrue(topIndividual1ContainsIndividual1);
		final boolean topIndividual1ContainsIndividual2 = topLevelIndividual1.containsNarrower(taxonomyIndividual2);
		assertTrue(topIndividual1ContainsIndividual2);
	}
	
	/**
	 * Tests the ability of the taxonomy to build a correct tree even if an individual is inserted whose parent
	 * is not in the taxonomy yet. The test created one top level individual and three other individuals.
	 * taxonomyIndividual1 contains taxonomyIndividual2 as narrower, taxonomyIndividual2 contains
	 * taxonomyIndividual3 as narrower. But the parent individuals do not exist when the individuals
	 * are inserted.
	 */
	@Test
	public void testBuildTaxonomy_withNotExistingBroader() {
		final Taxonomy taxonomy = TaxonomyTestApi.createTaxonomy();
		final TaxonomyIndividual topLevelIndividual1 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomy.addTopConceptToTaxonomy(topLevelIndividual1);
		final TaxonomyIndividual taxonomyIndividual1 = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual2 = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual3 = TaxonomyTestApi.createTaxonomyIndividual();
		
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual3, taxonomyIndividual2.getIri());
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual2, taxonomyIndividual1.getIri());
		taxonomy.addIndividualToTaxonomy(taxonomyIndividual1, topLevelIndividual1.getIri());
		
		assertThat(topLevelIndividual1.getNarrowerCount(), is(equalTo(1)));
		final boolean topLevelContainsIndividual1 = topLevelIndividual1.containsNarrower(taxonomyIndividual1);
		assertTrue(topLevelContainsIndividual1);
		
		assertThat(taxonomyIndividual1.getNarrowerCount(), is(equalTo(1)));
		final boolean individual1ContainsIndividual2 = taxonomyIndividual1.containsNarrower(taxonomyIndividual2);
		assertTrue(individual1ContainsIndividual2);
		assertThat(taxonomyIndividual2.getNarrowerCount(), is(equalTo(1)));
		final boolean individual2ContainsIndividual3 = taxonomyIndividual2.containsNarrower(taxonomyIndividual3);
		assertTrue(individual2ContainsIndividual3);
		assertThat(taxonomyIndividual3.getNarrowerCount(), is(equalTo(0)));
	}
}
