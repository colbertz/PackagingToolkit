package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.DataStructureFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

//@ActiveProfiles("propertiesTest")
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
//@Category(SpringTests.class)
public class OntologyPropertiesHashMapTest {
	private static final String PROPERTY_1 = "X";
	private static final String PROPERTY_2 = "Y";
	private static final String PROPERTY_3 = "Z";
	private static final String PROPERTY_4 = "ZZ";
	
	private OntologyPropertiesMap ontologyPropertiesMap;
	
	private OntologyProperty property1;
	private OntologyProperty property2;
	private OntologyProperty property3;
	private OntologyProperty property4;

	//@Autowired
	private DataStructureFactory dataStructureFactory;
	//@Autowired
	private PropertyFactory propertyFactory;
	
	@Before
	public void setUp() {
		ontologyPropertiesMap = dataStructureFactory.createOntologyPropertiesMap();
		property1 = propertyFactory.getProperty("boolean", PROPERTY_1, PROPERTY_1);
		property1.setPropertyValue(true);
		property2 = propertyFactory.getProperty("string", PROPERTY_2, PROPERTY_2);
		property2.setPropertyValue("bla");
		property3 = propertyFactory.getProperty("string", PROPERTY_3, PROPERTY_3);
		property3.setPropertyValue("blub");
		property4 = propertyFactory.getProperty("integer", PROPERTY_4, PROPERTY_4);
		property4.setPropertyValue(4);
	}
	
	@Test
	public void testAddProperty() {
		ontologyPropertiesMap.addProperty(property1);
		assertThat(ontologyPropertiesMap.getPropertiesCount(), is(equalTo(1)));
		OntologyProperty firstProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_1);
		assertThat(firstProperty, is(equalTo(property1)));
		
		ontologyPropertiesMap.addProperty(property2);
		assertThat(ontologyPropertiesMap.getPropertiesCount(), is(equalTo(2)));
		OntologyProperty secondProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_2);
		assertThat(secondProperty, is(equalTo(property2)));
		
		ontologyPropertiesMap.addProperty(property3);
		assertThat(ontologyPropertiesMap.getPropertiesCount(), is(equalTo(3)));
		OntologyProperty thirdProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_3);
		assertThat(thirdProperty, is(equalTo(property3)));
		
		ontologyPropertiesMap.addProperty(property4);
		assertThat(ontologyPropertiesMap.getPropertiesCount(), is(equalTo(4)));
		OntologyProperty fourthProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_4);
		assertThat(fourthProperty, is(equalTo(property4)));
	}
	
	@Test
	public void testGetOntologyClassesCount() {
		ontologyPropertiesMap.addProperty(property1);
		ontologyPropertiesMap.addProperty(property2);
		ontologyPropertiesMap.addProperty(property3);
		ontologyPropertiesMap.addProperty(property4);
		int size = ontologyPropertiesMap.getPropertiesCount();
		assertThat(size, is(equalTo(4)));
	}
	
	@Test
	public void testGetOntologyProperty() {
		ontologyPropertiesMap.addProperty(property1);
		ontologyPropertiesMap.addProperty(property2);
		ontologyPropertiesMap.addProperty(property3);
		ontologyPropertiesMap.addProperty(property4);
		
		OntologyProperty firstProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_1);
		assertThat(firstProperty, is(equalTo(property1)));
		
		OntologyProperty secondProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_2);
		assertThat(secondProperty, is(equalTo(property2)));
		
		OntologyProperty thirdProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_3);
		assertThat(thirdProperty, is(equalTo(property3)));
		
		OntologyProperty fourthProperty = ontologyPropertiesMap.cloneOntologyProperty(PROPERTY_4);
		assertThat(fourthProperty, is(equalTo(property4)));
	}
}
