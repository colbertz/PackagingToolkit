package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TriplePredicateImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleSubjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class OntologyIndividualImplTest {
	private OntologyClass ontologyClass;
	private OntologyIndividual ontologyIndividual;
	private List<OntologyDatatypeProperty> expectedDatatypePropertyList;
	private List<OntologyObjectProperty> expectedObjectPropertyList;
	
	@Before
	public void setUp() {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		expectedDatatypePropertyList = new ArrayList<>();
		expectedObjectPropertyList = new ArrayList<>();
		
		DatatypePropertyCollection datatypePropertyList = prepareDatatypeProperties();
		ObjectPropertyCollection objectPropertyList = prepareObjectProperties();
		ontologyClass = new OntologyClassImpl(datatypePropertyList, objectPropertyList, new NamespaceImpl("myNamespace"), new LocalNameImpl("myClass"));
		ontologyIndividual = new OntologyIndividualImpl(ontologyClass, datatypePropertyList, objectPropertyList, uuidOfInformationPackage);
	}
	
	private DatatypePropertyCollection prepareDatatypeProperties() {
		DatatypePropertyCollection datatypePropertyList = new DatatypePropertyCollectionImpl();
		OntologyDatatypeProperty booleanProperty = new OntologyBooleanPropertyImpl();
		OntologyDatatypeProperty doubleProperty = new OntologyDoublePropertyImpl();
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl();
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		datatypePropertyList.addDatatypeProperty(booleanProperty);
		datatypePropertyList.addDatatypeProperty(doubleProperty);
		datatypePropertyList.addDatatypeProperty(stringProperty);
		datatypePropertyList.addDatatypeProperty(shortProperty);
		
		expectedDatatypePropertyList.add(booleanProperty);
		expectedDatatypePropertyList.add(doubleProperty);
		expectedDatatypePropertyList.add(stringProperty);
		expectedDatatypePropertyList.add(shortProperty);
		
		return datatypePropertyList;
	}
	
	private ObjectPropertyCollection prepareObjectProperties() {
		OntologyObjectProperty objectProperty1 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty2 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty3 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		OntologyObjectProperty objectProperty4 = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);

		expectedObjectPropertyList.add(objectProperty1);
		expectedObjectPropertyList.add(objectProperty2);
		expectedObjectPropertyList.add(objectProperty3);
		expectedObjectPropertyList.add(objectProperty4);
		
		ObjectPropertyCollection objectPropertyList = new ObjectPropertyCollectionImpl();
		objectPropertyList.addObjectProperty(objectProperty1);
		objectPropertyList.addObjectProperty(objectProperty2);
		objectPropertyList.addObjectProperty(objectProperty3);
		objectPropertyList.addObjectProperty(objectProperty4);
		
		return objectPropertyList;
	}
	
	@Test
	public void testAddDatatypeProperty() {
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		expectedDatatypePropertyList.add(shortProperty);
		int expectedSize = expectedDatatypePropertyList.size();
		
		ontologyIndividual.addDatatypeProperty(shortProperty);
		int actualSize = ontologyIndividual.getDatatypePropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			OntologyDatatypeProperty expectedDatatypeProperty = expectedDatatypePropertyList.get(i);
			OntologyDatatypeProperty actualDatatypeProperty = ontologyIndividual.getDatatypePropertyAt(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}

	@Test
	public void testGetDatatypeProperty() {
		OntologyDatatypeProperty expectedDatatypeProperty = expectedDatatypePropertyList.get(2);
		OntologyDatatypeProperty actualDatatypeProperty = ontologyIndividual.getDatatypePropertyAt(2);
		int expectedSize = expectedDatatypePropertyList.size();
		int actualSize = ontologyIndividual.getDatatypePropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
	}

	@Test
	public void testGetDatatypePropertiesCount() {
		int expectedSize = expectedDatatypePropertyList.size();
		int actualSize = ontologyIndividual.getDatatypePropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	@Test
	public void testAddObjectPropery() {
		OntologyObjectProperty objectProperty = new OntologyObjectPropertyImpl("abc", "def", ontologyClass, "myNamespace#myClass", ontologyClass);
		expectedObjectPropertyList.add(objectProperty);
		int expectedSize = expectedObjectPropertyList.size();
		
		ontologyIndividual.addObjectProperty(objectProperty);
		int actualSize = ontologyIndividual.getObjectPropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			OntologyObjectProperty expectedDatatypeProperty = expectedObjectPropertyList.get(i);
			OntologyObjectProperty actualDatatypeProperty = ontologyIndividual.getObjectPropertyAt(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}
	
	@Test
	public void testGetObjectProperty() {
		OntologyObjectProperty expectedObjectProperty = expectedObjectPropertyList.get(2);
		OntologyObjectProperty actualObjectProperty = ontologyIndividual.getObjectPropertyAt(2);
		int expectedSize = expectedObjectPropertyList.size();
		int actualSize = ontologyIndividual.getObjectPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualObjectProperty, is(equalTo(expectedObjectProperty)));
	}
	
	@Test
	public void testGetObjectPropertiesCount() {
		int expectedSize = expectedObjectPropertyList.size();
		int actualSize = ontologyIndividual.getObjectPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	// ******************************************************
	// *******testContainsTripleStatement() ********************
	// ******************************************************
	@Test
	public void testContainsTripleStatement_contained() {
		TripleStatement tripleStatement1 = RdfElementFactory.createRemoteFileStatement(new Uuid(), "aFile1");
		TripleStatement tripleStatement2 = RdfElementFactory.createRemoteFileStatement(new Uuid(), "aFile2");
		TripleStatement tripleStatement3 = RdfElementFactory.createRemoteFileStatement(new Uuid(), "aFile3");
		TripleStatementCollection tripleStatements = new TripleStatementCollectionImpl();
		tripleStatements.addTripleStatement(tripleStatement1);
		tripleStatements.addTripleStatement(tripleStatement2);
		tripleStatements.addTripleStatement(tripleStatement3);
		
		boolean statementContained = tripleStatements.containsTripleStatement(tripleStatement2);
		assertTrue(statementContained);
	}
	
	@Test
	public void testContainsTripleStatement_notContained() {
		TripleStatement tripleStatement1 = RdfElementFactory.createRemoteFileStatement(new Uuid(), "aFile1");
		TripleStatement tripleStatement2 = RdfElementFactory.createRemoteFileStatement(new Uuid(), "aFile2");
		TripleStatement tripleStatement3 = RdfElementFactory.createRemoteFileStatement(new Uuid(), "aFile3");
		TripleStatementCollection tripleStatements = new TripleStatementCollectionImpl();
		tripleStatements.addTripleStatement(tripleStatement1);
		tripleStatements.addTripleStatement(tripleStatement3);
		
		boolean statementContained = tripleStatements.containsTripleStatement(tripleStatement2);
		assertFalse(statementContained);
	}
	
	// ******************************************************
	// ************testToTripleStatements() *******************
	// ******************************************************
	/**
	 * Creates an individual and creates triple statements out of this individual.
	 */
	@Test
	public void testToTripleStatements() {
		Uuid uuidOfInformationPackage = new Uuid();
		DatatypePropertyCollection datatypeProperties = prepareTest_testToTripleStatements_datatypeProperties();
		ObjectPropertyCollection objectProperties = prepareTest_testToTripleStatements_objectProperties();
		
		OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, datatypeProperties, objectProperties, uuidOfInformationPackage);
		Optional<TripleStatementCollection> optionalWithActualTripleStatements = ontologyIndividual.toTripleStatements();
		TripleStatementCollection actualTripleStatements = optionalWithActualTripleStatements.get(); 
		checkResults_testToTripleStatements(actualTripleStatements, ontologyIndividual);
	}
	
	/**
	 * Prepares two datatype properties for the individual for the test.
	 * @return A collection with an integer and a string property.
	 */
	private DatatypePropertyCollection prepareTest_testToTripleStatements_datatypeProperties() {
		DatatypePropertyCollection datatypeProperties = new DatatypePropertyCollectionImpl();
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl("http://test.de/property#hasInteger", false, Sign.BOTH, true, "hasInteger", "http://www.w3.org/2001/XMLSchema#integer");
		integerProperty.setPropertyValue(45l);
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl("http://test.de/property#hasString", "hasString", "http://www.w3.org/2001/XMLSchema#string");
		stringProperty.setPropertyValue("A string property");
		
		datatypeProperties.addDatatypeProperty(integerProperty);
		datatypeProperties.addDatatypeProperty(integerProperty);
		
		return datatypeProperties;
	}
	
	/**
	 * Prepares three object properties for the individual for the test.
	 * @return A collection with three object properties.
	 */
	private ObjectPropertyCollection prepareTest_testToTripleStatements_objectProperties() {
		ObjectPropertyCollection objectProperties = new ObjectPropertyCollectionImpl();
		
		DatatypePropertyCollection datatypeProperties = prepareTest_testToTripleStatements_datatypeProperties();
		OntologyClass range = new OntologyClassImpl(datatypeProperties, objectProperties, new NamespaceImpl("http://test.de"), new LocalNameImpl("class1"));
		OntologyClass propertyDomain = new OntologyClassImpl(datatypeProperties, objectProperties, new NamespaceImpl("http://test.de"), new LocalNameImpl("class2"));
		
		OntologyObjectProperty ontologyObjectProperty1 = new OntologyObjectPropertyImpl("http://test.de/property", "testproperty1", range, "http://test.de#class1", propertyDomain);
		OntologyObjectProperty ontologyObjectProperty2 = new OntologyObjectPropertyImpl("http://test.de/property", "testproperty2", range, "http://test.de#class1", propertyDomain);
		OntologyObjectProperty ontologyObjectProperty3 = new OntologyObjectPropertyImpl("http://test.de/property", "testproperty3", range, "http://test.de#class1", propertyDomain);
		
		objectProperties.addObjectProperty(ontologyObjectProperty1);
		objectProperties.addObjectProperty(ontologyObjectProperty2);
		objectProperties.addObjectProperty(ontologyObjectProperty3);
		
		return objectProperties;
	}
	
	/**
	 * Checks the results of the tests. Creates the statement we are expecting and checks if they are contained in the 
	 * resulting collection.
	 * @param actualTripleStatements The collection that contains the results of the test. 
	 * @param ontologyIndividual The individual that was used during the test.
	 */
	private void checkResults_testToTripleStatements(TripleStatementCollection actualTripleStatements, OntologyIndividual ontologyIndividual ) {
		final int expectedSize = 7;
		Uuid uuidOfInformationPackage = ontologyIndividual.getUuidOfInformationPackage();
		assertThat(actualTripleStatements.getTripleStatementsCount(), is(equalTo(expectedSize)));
		
		TripleSubject tripleSubjectProperty1 = new TripleSubjectImpl(ontologyIndividual.getUuid());
		TripleObject tripleObjectProperty1 = new TripleObjectImpl(uuidOfInformationPackage);
		TripleStatement aggregatedByStatement = RdfElementFactory.createAggregatedByStatement(tripleSubjectProperty1, tripleObjectProperty1);
		
		TripleSubject integerPropertySubject = new TripleSubjectImpl("hasInteger");
		TriplePredicate integerPropertyPredicate = new TriplePredicateImpl(ontologyIndividual.getUuid(), new NamespaceImpl("http://test.de/property"), new LocalNameImpl("hasInteger"));
		TripleObject integerPropertyObject = new TripleObjectImpl(45l);
		TripleStatement statementIntegerProperty = new TripleStatementImpl(integerPropertySubject, integerPropertyPredicate, integerPropertyObject);
		
		TripleSubject stringPropertySubject = new TripleSubjectImpl("hasString");
		TriplePredicate stringPropertyPredicate = new TriplePredicateImpl(ontologyIndividual.getUuid(), new NamespaceImpl("http://test.de/property"), new LocalNameImpl("hasString"));
		TripleObject stringPropertyObject = new TripleObjectImpl("A string property");
		TripleStatement statementStringProperty = new TripleStatementImpl(stringPropertySubject, stringPropertyPredicate, stringPropertyObject);
		
		OntologyObjectProperty ontologyObjectProperty1 = ontologyIndividual.getObjectPropertyAt(0);
		TripleSubject objectProperty1Subject = new TripleSubjectImpl(ontologyIndividual.getUuid());
		TriplePredicate objectProperty1Predicate = new TriplePredicateImpl("http://test.de/property#testproperty1");
		TripleObject objectProperty1Object = new TripleObjectImpl(ontologyObjectProperty1);
		TripleStatement objectProperty1Statement = new TripleStatementImpl(objectProperty1Subject, objectProperty1Predicate, objectProperty1Object);
		
		OntologyObjectProperty ontologyObjectProperty2 = ontologyIndividual.getObjectPropertyAt(1);
		TripleSubject objectProperty2Subject = new TripleSubjectImpl(ontologyIndividual.getUuid());
		TriplePredicate objectProperty2Predicate = new TriplePredicateImpl("http://test.de/property#testproperty2");
		TripleObject objectProperty2Object = new TripleObjectImpl(ontologyObjectProperty2);
		TripleStatement objectProperty2Statement = new TripleStatementImpl(objectProperty2Subject, objectProperty2Predicate, objectProperty2Object);
		
		OntologyObjectProperty ontologyObjectProperty3 = ontologyIndividual.getObjectPropertyAt(2);
		TripleSubject objectProperty3Subject = new TripleSubjectImpl(ontologyIndividual.getUuid());
		TriplePredicate objectProperty3Predicate = new TriplePredicateImpl("http://test.de/property#testproperty3");
		TripleObject objectProperty3Object = new TripleObjectImpl(ontologyObjectProperty3);
		TripleStatement objectProperty3Statement = new TripleStatementImpl(objectProperty3Subject, objectProperty3Predicate, objectProperty3Object);
		
		TripleSubject typeSubject = new TripleSubjectImpl(ontologyIndividual.getUuid());
		TripleObject typeObject = new TripleObjectImpl(ontologyClass.getNamespace(), ontologyClass.getLocalName());
		TripleStatement typeStatement = RdfElementFactory.createTypeStatement(typeSubject, typeObject);
		
		boolean containsAggregatedBy = actualTripleStatements.containsTripleStatement(aggregatedByStatement);
		assertTrue(containsAggregatedBy);
		
		boolean containsIntegerProperty = actualTripleStatements.containsTripleStatement(statementIntegerProperty);
		assertTrue(containsIntegerProperty);
		
		boolean containsStringProperty = actualTripleStatements.containsTripleStatement(statementStringProperty);
		assertTrue(containsStringProperty);
		
		boolean containsObjectProperty1 = actualTripleStatements.containsTripleStatement(objectProperty1Statement);
		assertTrue(containsObjectProperty1);
		
		boolean containsObjectProperty2 = actualTripleStatements.containsTripleStatement(objectProperty2Statement);
		assertTrue(containsObjectProperty2);
		
		boolean containsObjectProperty3 = actualTripleStatements.containsTripleStatement(objectProperty3Statement);
		assertTrue(containsObjectProperty3);
		
		boolean containsTypeStatement = actualTripleStatements.containsTripleStatement(typeStatement);
		assertTrue(containsTypeStatement);
	}
	
	@Test
	public void testIsSkosClass_conceptScheme_resultTrue() {
		final OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("http://www.w3.org/2004/02/skos/core"), new LocalNameImpl("ConceptScheme"));
		final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, new IriImpl("http://mynamespace#mylocalname"));
		
		final boolean isSkos = ontologyIndividual.isSkosIndividual();
		assertTrue(isSkos);
	}
	
	@Test
	public void testIsSkosClass_concept_resultTrue() {
		final OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("http://www.w3.org/2004/02/skos/core"), new LocalNameImpl("Concept"));
		final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, new IriImpl("http://mynamespace#mylocalname"));
		
		final boolean isSkos = ontologyIndividual.isSkosIndividual();
		assertTrue(isSkos);
	}
	
	@Test
	public void testIsSkosClass_resultFalse() {
		final OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("http://www.w3.org/2004/02/skos/core"), new LocalNameImpl("NotAConcept"));
		final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, new IriImpl("http://mynamespace#mylocalname"));
		
		final boolean isSkos = ontologyIndividual.isSkosIndividual();
		assertFalse(isSkos);
	}
}
