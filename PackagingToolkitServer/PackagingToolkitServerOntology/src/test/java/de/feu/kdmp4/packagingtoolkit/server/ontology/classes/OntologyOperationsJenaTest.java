package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDatePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDateTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDurationPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyFloatPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyLongPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.override.TestOntologyIndividualImpl;

//@ActiveProfiles("ontologyOperationsTest")
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, OperationsConfiguration.class})
//@Category(SpringTests.class)
public class OntologyOperationsJenaTest {
	private static final String PATH_ONTOLOGY = "src" + File.separator + "test" + 
				File.separator + "resources" + File.separator + "testOntology.owl";
	private static final String NAMESPACE_TEST_ONTOLOGY = "http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP";
	private static final String ONTOLOGY_CLASS_DATA_OBJECT = "Data_Object";
	private static final String ONTOLOGY_CLASS_REPRESENTATION_INFORMATION = "Representation_Information";
	private static final String ONTOLOGY_CLASS_SEMANTIC_INFORMATION = "Semantic_Information";
	private static final String ONTOLOGY_BOOLEAN_PROPERTY = "hasBooleanProperty";
	private static final String ONTOLOGY_BYTE_PROPERTY = "hasByteProperty";
	private static final String ONTOLOGY_DATE_PROPERTY = "hasDateProperty";
	private static final String ONTOLOGY_DATETIME_PROPERTY = "hasDateTimeProperty";
	private static final String ONTOLOGY_DURATION_PROPERTY = "hasDurationProperty";
	private static final String ONTOLOGY_DOUBLE_PROPERTY = "hasDoubleProperty";
	private static final String ONTOLOGY_FLOAT_PROPERTY = "hasFloatProperty";
	private static final String ONTOLOGY_INTEGER_PROPERTY = "hasIntegerProperty";
	private static final String ONTOLOGY_LONG_PROPERTY = "hasLongProperty";
	private static final String ONTOLOGY_NEGATIVE_INTEGER_PROPERTY = "hasNegativeIntProperty";
	private static final String ONTOLOGY_POSITIVE_INTEGER_PROPERTY = "hasPositiveIntProperty";
	private static final String ONTOLOGY_NON_NEGATIVE_INTEGER_PROPERTY = "hasNonNegativeIntProperty";
	private static final String ONTOLOGY_NON_POSITIVE_INTEGER_PROPERTY = "hasNonPositiveIntProperty";
	private static final String ONTOLOGY_SHORT_PROPERTY = "hasShortProperty";
	private static final String ONTOLOGY_STRING_PROPERTY = "hasStringProperty";
	private static final String ONTOLOGY_TIME_PROPERTY = "hasTimeProperty";
	private static final String ONTOLOGY_UNSIGNED_INTEGER_PROPERTY = "hasUnsignedIntProperty";
	private static final String ONTOLOGY_BYTE_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_BYTE_PROPERTY;
	private static final String ONTOLOGY_BOOLEAN_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_BOOLEAN_PROPERTY;
	private static final String ONTOLOGY_DATE_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_DATE_PROPERTY;
	private static final String ONTOLOGY_DATETIME_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_DATETIME_PROPERTY;
	private static final String ONTOLOGY_DURATION_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_DURATION_PROPERTY;
	private static final String ONTOLOGY_DOUBLE_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_DOUBLE_PROPERTY;
	private static final String ONTOLOGY_FLOAT_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_FLOAT_PROPERTY;
	private static final String ONTOLOGY_INTEGER_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_INTEGER_PROPERTY;
	private static final String ONTOLOGY_LONG_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_LONG_PROPERTY;
	private static final String ONTOLOGY_POSITIVE_INTEGER_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_POSITIVE_INTEGER_PROPERTY;
	private static final String ONTOLOGY_NON_POSITIVE_INTEGER_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_NON_POSITIVE_INTEGER_PROPERTY;
	private static final String ONTOLOGY_NEGATIVE_INTEGER_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_NEGATIVE_INTEGER_PROPERTY;
	private static final String ONTOLOGY_NON_NEGATIVE_INTEGER_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_NON_NEGATIVE_INTEGER_PROPERTY;
	private static final String ONTOLOGY_STRING_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_STRING_PROPERTY;
	private static final String ONTOLOGY_SHORT_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_SHORT_PROPERTY;
	private static final String ONTOLOGY_TIME_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_TIME_PROPERTY;
	private static final String ONTOLOGY_UNSIGNED_INT_PROPERTY_WITH_NAMESPACE = NAMESPACE_TEST_ONTOLOGY + "#" + ONTOLOGY_UNSIGNED_INTEGER_PROPERTY;
	private static final String XSD_DATATYPE_BOOLEAN = "http://www.w3.org/2001/XMLSchema#boolean";
	private static final String XSD_DATATYPE_BYTE = "http://www.w3.org/2001/XMLSchema#byte";
	private static final String XSD_DATATYPE_DATE = "http://www.w3.org/2001/XMLSchema#date";
	private static final String XSD_DATATYPE_DATETIME = "http://www.w3.org/2001/XMLSchema#datetime";
	private static final String XSD_DATATYPE_DURATION = "http://www.w3.org/2001/XMLSchema#duration";
	private static final String XSD_DATATYPE_DOUBLE = "http://www.w3.org/2001/XMLSchema#double";
	private static final String XSD_DATATYPE_FLOAT = "http://www.w3.org/2001/XMLSchema#float";
	private static final String XSD_DATATYPE_INTEGER = "http://www.w3.org/2001/XMLSchema#integer";
	private static final String XSD_DATATYPE_POSITIVE_INTEGER = "http://www.w3.org/2001/XMLSchema#positiveInteger";
	private static final String XSD_DATATYPE_NON_POSITIVE_INTEGER = "http://www.w3.org/2001/XMLSchema#nonPositiveInteger";
	private static final String XSD_DATATYPE_NEGATIVE_INTEGER = "http://www.w3.org/2001/XMLSchema#negativeInteger";
	private static final String XSD_DATATYPE_NON_NEGATIVE_INTEGER = "http://www.w3.org/2001/XMLSchema#nonNegativeInteger";
	private static final String XSD_DATATYPE_LONG = "http://www.w3.org/2001/XMLSchema#long";
	private static final String XSD_DATATYPE_SHORT = "http://www.w3.org/2001/XMLSchema#short";
	private static final String XSD_DATATYPE_STRING = "http://www.w3.org/2001/XMLSchema#string";
	private static final String XSD_DATATYPE_TIME = "http://www.w3.org/2001/XMLSchema#time";
	private static final String XSD_DATATYPE_UNSIGNED_INTEGER = "http://www.w3.org/2001/XMLSchema#unsignedint";
	
	//@Autowired
	//@Spy
	private OntologyOperations ontologyOperations;
	
	
	@Before
	public void setUp() {
		
	}
	
	/**
	 * Prepares the OntologyOperations objects. The file with the ontology is injected.
	 */
	private void prepareAllTests_ontologyOperations() {
		ontologyOperations = new OntologyOperationsJenaImpl();
		File ontologyFile = new File(PATH_ONTOLOGY);
		ontologyOperations.startBaseOntologyProcessing(ontologyFile);
		//ontologyOperations.setBaseOntologyFile(ontologyFile);
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeBoolean()_resultSuccessful
	// ******************************************
	/**
	 * Tests the validation of an individual with a boolean property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeBoolean_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeBoolean_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a boolean property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeBoolean_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_DATA_OBJECT);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty booleanProperty = new OntologyBooleanPropertyImpl(ONTOLOGY_BOOLEAN_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_BOOLEAN_PROPERTY, XSD_DATATYPE_BOOLEAN);		
		booleanProperty.setPropertyValue(true);
		ontologyIndividual.addDatatypeProperty(booleanProperty);
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeSignedByte_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a boolean property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeSignedByte_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeSignedByte_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a byte property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeSignedByte_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_DATA_OBJECT);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty byteProperty = new OntologyBytePropertyImpl(ONTOLOGY_BYTE_PROPERTY_WITH_NAMESPACE, false, 
				ONTOLOGY_BYTE_PROPERTY, XSD_DATATYPE_BYTE);		
		ontologyIndividual.addDatatypeProperty(byteProperty);
		byteProperty.setPropertyValue(50);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeSignedByte_resultUnsuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a boolean property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test(expected = OntologyReasonerException.class)
	public void testValidateIndividual_withDatatypeSignedByte_resultUnsuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeSignedByte_resultUnsuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a byte property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeSignedByte_resultUnsuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_DATA_OBJECT);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty byteProperty = new OntologyBytePropertyImpl(ONTOLOGY_BYTE_PROPERTY_WITH_NAMESPACE, false, 
				ONTOLOGY_BYTE_PROPERTY, XSD_DATATYPE_BYTE);		
		ontologyIndividual.addDatatypeProperty(byteProperty);
		OntologyDatatypeProperty booleanProperty = new OntologyBooleanPropertyImpl( NAMESPACE_TEST_ONTOLOGY + "#blablub", "bla", XSD_DATATYPE_BOOLEAN);
		byteProperty.setPropertyValue(129);
		booleanProperty.setPropertyValue(true);
		ontologyIndividual.addDatatypeProperty(booleanProperty);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeDate_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a date property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeDate_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeDate_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a date property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeDate_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_DATA_OBJECT);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty dateProperty = new OntologyDatePropertyImpl(ONTOLOGY_DATE_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_DATE_PROPERTY, XSD_DATATYPE_DATE);		
		ontologyIndividual.addDatatypeProperty(dateProperty);
		dateProperty.setPropertyValue(LocalDate.now());
		
		return ontologyIndividual;
	}

	// ******************************************
	// testValidateIndividual_withDatatypeDateTime_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a datetime property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeDateTime_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeDateTime_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a datetime property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeDateTime_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_DATA_OBJECT);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty dateTimeProperty = new OntologyDateTimePropertyImpl(ONTOLOGY_DATETIME_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_DATETIME_PROPERTY, XSD_DATATYPE_DATETIME);		
		ontologyIndividual.addDatatypeProperty(dateTimeProperty);
		dateTimeProperty.setPropertyValue(LocalDateTime.now());
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeDuration_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a duration property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeDuration_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeDuration_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a duration property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeDuration_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty dateTimeProperty = new OntologyDurationPropertyImpl(ONTOLOGY_DURATION_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_DURATION_PROPERTY, XSD_DATATYPE_DURATION);		
		ontologyIndividual.addDatatypeProperty(dateTimeProperty);
		dateTimeProperty.setPropertyValue("P1Y2M3DT4H5M6S");
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeDouble_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a double property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeDouble_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeDouble_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a double property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeDouble_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_DATA_OBJECT);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty doubleProperty = new OntologyDoublePropertyImpl(ONTOLOGY_DOUBLE_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_DOUBLE_PROPERTY, XSD_DATATYPE_DOUBLE);		
		ontologyIndividual.addDatatypeProperty(doubleProperty);
		doubleProperty.setPropertyValue(-10.5);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeFloat_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a float property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeFloat_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeFloat_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a float property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeFloat_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty floatProperty = new OntologyFloatPropertyImpl(ONTOLOGY_FLOAT_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_FLOAT_PROPERTY, XSD_DATATYPE_FLOAT);		
		ontologyIndividual.addDatatypeProperty(floatProperty);
		floatProperty.setPropertyValue(-10.5f);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeInteger_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with an integer property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeSignedInteger_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeSignedInteger_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates an integer property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeSignedInteger_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl(ONTOLOGY_INTEGER_PROPERTY_WITH_NAMESPACE, false, 
				Sign.BOTH, true, ONTOLOGY_INTEGER_PROPERTY, XSD_DATATYPE_INTEGER);		
		ontologyIndividual.addDatatypeProperty(integerProperty);
		integerProperty.setPropertyValue(-10l);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeString_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a string property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeString_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeString_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a string property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeString_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_SEMANTIC_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl(ONTOLOGY_STRING_PROPERTY_WITH_NAMESPACE, ONTOLOGY_STRING_PROPERTY, 
				XSD_DATATYPE_STRING);		
		ontologyIndividual.addDatatypeProperty(stringProperty);
		stringProperty.setPropertyValue("abc");
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeUnsignedInt_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with an unsigned integer property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeUnsignedInt_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeUnsignedInt_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates an unsigned integer property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeUnsignedInt_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl(ONTOLOGY_UNSIGNED_INT_PROPERTY_WITH_NAMESPACE, true, 
				Sign.BOTH, true, ONTOLOGY_UNSIGNED_INTEGER_PROPERTY, XSD_DATATYPE_UNSIGNED_INTEGER);		
		ontologyIndividual.addDatatypeProperty(integerProperty);
		integerProperty.setPropertyValue(10l);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypePositiveInteger_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a positive integer property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypePositiveInteger_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypePositiveInteger_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a positive integer property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypePositiveInteger_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl(ONTOLOGY_POSITIVE_INTEGER_PROPERTY_WITH_NAMESPACE, false, 
				Sign.PLUS_SIGN, false, ONTOLOGY_POSITIVE_INTEGER_PROPERTY, XSD_DATATYPE_POSITIVE_INTEGER);		
		ontologyIndividual.addDatatypeProperty(integerProperty);
		integerProperty.setPropertyValue(1l);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeNonPositiveInteger_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a non positive integer property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeNonPositiveInteger_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeNonPositiveInteger_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a non positive integer property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeNonPositiveInteger_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl(ONTOLOGY_NON_POSITIVE_INTEGER_PROPERTY_WITH_NAMESPACE, true, 
				Sign.MINUS_SIGN, true, ONTOLOGY_NON_POSITIVE_INTEGER_PROPERTY, XSD_DATATYPE_NON_POSITIVE_INTEGER);		
		ontologyIndividual.addDatatypeProperty(integerProperty);
		integerProperty.setPropertyValue(-6l);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeNegativeInteger_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a negative integer property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeNegativeInteger_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeNegativeInteger_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a negative integer property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeNegativeInteger_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl(ONTOLOGY_NEGATIVE_INTEGER_PROPERTY_WITH_NAMESPACE, false, 
				Sign.MINUS_SIGN, false, ONTOLOGY_NEGATIVE_INTEGER_PROPERTY, XSD_DATATYPE_NEGATIVE_INTEGER);		
		ontologyIndividual.addDatatypeProperty(integerProperty);
		integerProperty.setPropertyValue(-1l);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeNonNegativeInteger_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a non negative integer property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeNonNegativeInteger_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeNonNegativeInteger_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a non negative integer property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeNonNegativeInteger_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_REPRESENTATION_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl(ONTOLOGY_NON_NEGATIVE_INTEGER_PROPERTY_WITH_NAMESPACE, true, 
				Sign.PLUS_SIGN, true, ONTOLOGY_NON_NEGATIVE_INTEGER_PROPERTY, XSD_DATATYPE_NON_NEGATIVE_INTEGER);		
		ontologyIndividual.addDatatypeProperty(integerProperty);
		integerProperty.setPropertyValue(4l);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeLong_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a long property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeLong_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeLong_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a long property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeLong_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName( ONTOLOGY_CLASS_SEMANTIC_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty longProperty = new OntologyLongPropertyImpl(ONTOLOGY_LONG_PROPERTY_WITH_NAMESPACE, false, ONTOLOGY_LONG_PROPERTY, 
				XSD_DATATYPE_LONG);		
		ontologyIndividual.addDatatypeProperty(longProperty);
		BigInteger longValue = BigInteger.valueOf(500l);
		longProperty.setPropertyValue(longValue);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeShort_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a short property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeShort_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeShort_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a short property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeShort_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_SEMANTIC_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl(ONTOLOGY_SHORT_PROPERTY_WITH_NAMESPACE, false, ONTOLOGY_SHORT_PROPERTY, 
				XSD_DATATYPE_SHORT);		
		ontologyIndividual.addDatatypeProperty(shortProperty);
		shortProperty.setPropertyValue(50);
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withDatatypeTime_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with a time property. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withDatatypeTime_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withDatatypeTime_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a time property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withDatatypeTime_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_SEMANTIC_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty timeProperty = new OntologyTimePropertyImpl(ONTOLOGY_TIME_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_TIME_PROPERTY, XSD_DATATYPE_TIME);		
		ontologyIndividual.addDatatypeProperty(timeProperty);
		timeProperty.setPropertyValue(LocalTime.now());
		
		return ontologyIndividual;
	}
	
	// ******************************************
	// testValidateIndividual_withThreeProperties_resultSuccessful()
	// ******************************************
	/**
	 * Tests the validation of an individual with three properties. The validation is successful. This means that no
	 * exception is thrown during the test and therefore no assert is needed.
	 */
	@Test
	public void testValidateIndividual_withThreeProperties_resultSuccessful() {
		prepareAllTests_ontologyOperations();
		OntologyIndividual ontologyIndividual = prepareTest_testValidateIndividual_withThreeProperties_resultSuccessful_Individual();
		
		ontologyOperations.validateIndividual(ontologyIndividual);
	}
	
	/**
	 * Prepares the individual for the test. Creates a time property and saves it in the individual.
	 * @return The indiviudual.
	 */
	private OntologyIndividual prepareTest_testValidateIndividual_withThreeProperties_resultSuccessful_Individual() {
		Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_TEST_ONTOLOGY);
		LocalName localName = ServerModelFactory.createLocalName(ONTOLOGY_CLASS_SEMANTIC_INFORMATION);
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		OntologyIndividual ontologyIndividual = new TestOntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty timeProperty = new OntologyTimePropertyImpl(ONTOLOGY_TIME_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_TIME_PROPERTY, XSD_DATATYPE_TIME);		
		timeProperty.setPropertyValue(LocalTime.now());
		
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl(ONTOLOGY_SHORT_PROPERTY_WITH_NAMESPACE, false, 
				ONTOLOGY_SHORT_PROPERTY, XSD_DATATYPE_SHORT);		
		ontologyIndividual.addDatatypeProperty(shortProperty);
		shortProperty.setPropertyValue(50);
		
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl(ONTOLOGY_STRING_PROPERTY_WITH_NAMESPACE, 
				ONTOLOGY_STRING_PROPERTY, XSD_DATATYPE_STRING);		
		ontologyIndividual.addDatatypeProperty(stringProperty);
		stringProperty.setPropertyValue("bla");

		ontologyIndividual.addDatatypeProperty(timeProperty);
		ontologyIndividual.addDatatypeProperty(shortProperty);
		ontologyIndividual.addDatatypeProperty(stringProperty);
		return ontologyIndividual;
	}
	
	
	@Test
	@Ignore
	public void testGetHierarchy() {
		File file = new File(PATH_ONTOLOGY);
		boolean fileExists = file.exists();
		
		assertThat(fileExists, is(equalTo(true)));
		
		ontologyOperations.startBaseOntologyProcessing(file);
		OntologyClass ontologyClass = ontologyOperations.getHierarchy("Information_Package");
		assertThat(ontologyClass.getSubclassCount(), is(equalTo(2)));
		assertThat(ontologyClass.getLabel().contains("Information_Package"), is(equalTo(true)));
		// 6. the subtree of PackageInformation.
		//OntologyClass packagingInformation = ontologyClass.getSubClassAt(5);
		//assertThat(packagingInformation.getLabel().contains("Packaging_Information"), is(equalTo(true)));
		// TODO Hier ist Nothing mit drin.
		//assertThat(packagingInformation.getSubclassCount(), is(equalTo(1)));
		// 7. The subtree of InformationPackage.
		//OntologyClass informationPackage = ontologyClass.getSubClassAt(0);
		//assertThat(informationPackage.getLabel().contains("Information_Package"), is(equalTo(true)));
		//assertThat(informationPackage.getSubclassCount(), is(equalTo(2)));
		// 7.2 ContextInformation
		OntologyClass contentInformation = ontologyClass.getSubClassAt(0);
		assertThat(contentInformation.getLabel().contains("Content_Information"), is(equalTo(true)));
		assertThat(contentInformation.getSubclassCount(), is(equalTo(2)));
		OntologyClass dataObject = contentInformation.getSubClassAt(1);
		assertThat(dataObject.getLabel().contains("Data_Object"), is(equalTo(true)));
		assertThat(dataObject.getSubclassCount(), is(equalTo(2)));
		OntologyClass digitalObject = dataObject.getSubClassAt(1);
		assertThat(digitalObject.getLabel().contains("Digital_Object"), is(equalTo(true)));
		// TODO Nothing mit dabei.
		assertThat(digitalObject.getSubclassCount(), is(equalTo(0)));
		OntologyClass physicalObject = dataObject.getSubClassAt(0);
		assertThat(physicalObject.getLabel().contains("Physical_Object"), is(equalTo(true)));
		// TODO Mit Nothing.
		assertThat(physicalObject.getSubclassCount(), is(equalTo(0)));
		OntologyClass representationInformation = contentInformation.getSubClassAt(0);
		assertThat(representationInformation.getLabel().contains("Representation_Information"), is(equalTo(true)));
		assertThat(representationInformation.getSubclassCount(), is(equalTo(3)));
		OntologyClass otherRepresentationInformation = representationInformation.getSubClassAt(2);
		assertThat(otherRepresentationInformation.getLabel().contains("Other_Representation_Information"), is(equalTo(true)));
		assertThat(otherRepresentationInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass semanticInformation = representationInformation.getSubClassAt(1);
		assertThat(semanticInformation.getLabel().contains("Semantic_Information"), is(equalTo(true)));
		assertThat(semanticInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass structureInformation = representationInformation.getSubClassAt(0);
		assertThat(structureInformation.getLabel().contains("Structure_Information"), is(equalTo(true)));
		assertThat(structureInformation.getSubclassCount(), is(equalTo(0)));
		// 7.2. PreservationDescriptionInformation.
		OntologyClass preservationDescriptionInformation = ontologyClass.getSubClassAt(1);
		assertThat(preservationDescriptionInformation.getLabel().contains("Preservation_Description_Information"), is(equalTo(true)));
		assertThat(preservationDescriptionInformation.getSubclassCount(), is(equalTo(5)));
		OntologyClass accessRightsInformation = preservationDescriptionInformation.getSubClassAt(0);
		assertThat(accessRightsInformation.getLabel().contains("Access_Rights_Information"), is(equalTo(true)));
		assertThat(accessRightsInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass contextInformation = preservationDescriptionInformation.getSubClassAt(2);
		assertThat(contextInformation.getLabel().contains("Context_Information"), is(equalTo(true)));
		assertThat(contextInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass fixityInformation = preservationDescriptionInformation.getSubClassAt(3);
		assertThat(fixityInformation.getLabel().contains("Fixity_Information"), is(equalTo(true)));
		assertThat(fixityInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass provenanceInformation = preservationDescriptionInformation.getSubClassAt(4);
		assertThat(provenanceInformation.getLabel().contains("Provenance_Information"), is(equalTo(true)));
		assertThat(provenanceInformation.getSubclassCount(), is(equalTo(0)));
		OntologyClass referenceInformation = preservationDescriptionInformation.getSubClassAt(1);
		assertThat(referenceInformation.getLabel().contains("Reference_Information"), is(equalTo(true)));
		assertThat(referenceInformation.getSubclassCount(), is(equalTo(0)));
	}

}
