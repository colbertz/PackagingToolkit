package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class OntologyIndividualListTest {
	private OntologyIndividualCollection ontologyIndividualList;
	private List<OntologyIndividual> expectedList;
	
	@Before
	public void setUp() {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		expectedList = new ArrayList<>();
		ontologyIndividualList = new OntologyIndividualCollectionImpl();
		OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("myNamespace"), new LocalNameImpl("myClass"));
		DatatypePropertyCollection datatypeProperties = new DatatypePropertyCollectionImpl();
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl();
		integerProperty.setPropertyValue(45);
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl();
		stringProperty.setPropertyValue("Hallo");
		datatypeProperties.addDatatypeProperty(stringProperty);
		datatypeProperties.addDatatypeProperty(integerProperty);

		OntologyIndividual ontologyIndividual1 = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual ontologyIndividual2 = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				datatypeProperties, new ObjectPropertyCollectionImpl());
		OntologyIndividual ontologyIndividual3 = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		OntologyIndividual ontologyIndividual4 = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		
		ontologyIndividualList.addIndividual(ontologyIndividual1);
		ontologyIndividualList.addIndividual(ontologyIndividual2);
		ontologyIndividualList.addIndividual(ontologyIndividual3);
		ontologyIndividualList.addIndividual(ontologyIndividual4);
		
		expectedList.add(ontologyIndividual1);
		expectedList.add(ontologyIndividual2);
		expectedList.add(ontologyIndividual3);
		expectedList.add(ontologyIndividual4);
	}
	
	@Test
	public void testToList() {
		List<OntologyIndividual> actualIndividuals = ontologyIndividualList.toList();
		assertThat(actualIndividuals, is(equalTo(expectedList)));
	}
	
	@Test
	public void testAddIndividual() {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		OntologyClass ontologyClass = new OntologyClassImpl(new NamespaceImpl("myNamespace"), new LocalNameImpl("myClass"));
		OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		expectedList.add(ontologyIndividual);
		int expectedSize = expectedList.size();
		
		ontologyIndividualList.addIndividual(ontologyIndividual);
		int actualSize = ontologyIndividualList.getIndiviualsCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			OntologyIndividual expectedDatatypeProperty = expectedList.get(i);
			OntologyIndividual actualDatatypeProperty = ontologyIndividualList.getIndividual(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}
	
	@Test
	public void testGetOntologyIndividual() {
		OntologyIndividual expectedOntologyIndividual = expectedList.get(2);
		OntologyIndividual actualOntologyIndividual = ontologyIndividualList.getIndividual(2);
		int expectedSize = expectedList.size();
		int actualSize = ontologyIndividualList.getIndiviualsCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualOntologyIndividual, is(equalTo(expectedOntologyIndividual)));
	}
	
	@Test
	public void testGetObjectPropertiesCount() {
		int expectedSize = expectedList.size();
		int actualSize = ontologyIndividualList.getIndiviualsCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	/**
	 * Tests if an individual in the list can be replaced by another one.
	 */
	@Test
	public void testReplaceIndividual() {
		OntologyIndividual newOntologyIndividual = prepareTest_testReplaceIndividual_newOntologyIndividual();
		
		ontologyIndividualList.replaceIndividual(newOntologyIndividual);
		
		checkResults_testReplaceIndividual(newOntologyIndividual);
	}
	
	/**
	 * Prepares an ontology that should replace an individual with the same uuid in the collection.
	 * @return The new individual.
	 */
	private OntologyIndividual prepareTest_testReplaceIndividual_newOntologyIndividual() {
		OntologyIndividual individualToReplace = ontologyIndividualList.getIndividual(1);
		DatatypePropertyCollection datatypeProperties = new DatatypePropertyCollectionImpl();
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl();
		integerProperty.setPropertyValue(99);
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl();
		stringProperty.setPropertyValue("Hallo Welt");
		datatypeProperties.addDatatypeProperty(stringProperty);
		datatypeProperties.addDatatypeProperty(integerProperty);
		DatatypePropertyCollection propertyList = new DatatypePropertyCollectionImpl();
		propertyList.addDatatypeProperty(stringProperty);
		propertyList.addDatatypeProperty(integerProperty);
		OntologyIndividual newOntologyIndividual = new OntologyIndividualImpl(individualToReplace.getOntologyClass(), propertyList, individualToReplace.getUuid(), 
				new Uuid());
		return newOntologyIndividual;
	}
	
	/**
	 * Check if the new individual has replaced the one with the same uuid in the collection.
	 * @param newOntologyIndividual The individual that should have replaced the other one.
	 */
	private void checkResults_testReplaceIndividual(OntologyIndividual newOntologyIndividual) {
		OntologyIndividual replacedIndividual = ontologyIndividualList.getIndividual(1);
		assertThat(replacedIndividual.getUuid(), is(equalTo(newOntologyIndividual.getUuid())));
		OntologyDatatypeProperty stringProperty = replacedIndividual.getDatatypePropertyAt(0);
		OntologyDatatypeProperty integerProperty = replacedIndividual.getDatatypePropertyAt(1);
		assertThat(stringProperty.getPropertyValue(), is(equalTo("Hallo Welt")));
		assertThat(integerProperty.getPropertyValue(), is(equalTo(99l)));
	}
	
	@Test
	public void testContainsOntologyIndividual_resultTrue() {
		final OntologyIndividual secondOntologyIndividual = ontologyIndividualList.getIndividual(1);
		final boolean individualContained = ontologyIndividualList.containsIndividual(secondOntologyIndividual);
		assertTrue(individualContained);
	}
	
	@Test
	public void testContainsOntologyIndividual_resultFalse() {
		final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(new OntologyClassImpl(new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl()), new Uuid(), 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl());
		final boolean individualContained = ontologyIndividualList.containsIndividual(ontologyIndividual);
		assertFalse(individualContained);
	}
	
	/**
	 * Tests the method doWithEveryIndividualAndReturnNewList by append the String abc to the preferred label of every individual. 
	 * The test checks if abchas been appended to every label.
	 * <br />
	 * This test checks if the usage of an UnaryOperator is working.
	 */
	@Test
	public void testDoWithEveryIndividualAndModifyCollection_appendAbcToPreferredLabel() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = new TaxonomyIndividualCollectionImpl(); 
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividualCollection);
		final Consumer<TaxonomyIndividual> operator = (taxonomyIndividual) -> {
			String preferredLabel = taxonomyIndividual.getPreferredLabel();
			preferredLabel = preferredLabel + "abc";
			taxonomyIndividual.setPreferredLabel(preferredLabel);
		};
		
		taxonomyIndividualCollection.doWithEveryIndividual(operator);
		
		for (int i = 0; i < taxonomyIndividualCollection.getTaxonomyIndividualCount(); i++) {
			final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = taxonomyIndividualCollection.getTaxonomyIndividual(i);
			final TaxonomyIndividual taxonomyIndividual = optionalWithTaxonomyIndividual.get();
			assertThat(taxonomyIndividual.getPreferredLabel(), endsWith("abc"));
		}
	}
	
	@Test
	public void testHasNext() {
		final OntologyIndividualCollection ontologyIndividuals = new OntologyIndividualCollectionImpl();
		final OntologyIndividual ontologyIndividual1 = new OntologyIndividualImpl();
		final OntologyIndividual ontologyIndividual2 = new OntologyIndividualImpl();
		
		ontologyIndividuals.addIndividual(ontologyIndividual1);
		ontologyIndividuals.addIndividual(ontologyIndividual2);
		
		assertTrue(ontologyIndividuals.hasNextOntologyIndividual());
		ontologyIndividuals.getNextOntologyIndividual();
		assertTrue(ontologyIndividuals.hasNextOntologyIndividual());
		ontologyIndividuals.getNextOntologyIndividual();
		assertFalse(ontologyIndividuals.hasNextOntologyIndividual());
	}
	
	@Test
	public void testGetNext() {
		final OntologyIndividualCollection ontologyIndividuals = new OntologyIndividualCollectionImpl();
		final OntologyIndividual ontologyIndividual1 = new OntologyIndividualImpl();
		final OntologyIndividual ontologyIndividual2 = new OntologyIndividualImpl();
		
		ontologyIndividuals.addIndividual(ontologyIndividual1);
		ontologyIndividuals.addIndividual(ontologyIndividual2);
		
		assertTrue(ontologyIndividuals.hasNextOntologyIndividual());
		assertNotNull(ontologyIndividuals.getNextOntologyIndividual());
		assertTrue(ontologyIndividuals.hasNextOntologyIndividual());
		assertNotNull(ontologyIndividuals.getNextOntologyIndividual());
		ontologyIndividuals.getNextOntologyIndividual();
		assertFalse(ontologyIndividuals.hasNextOntologyIndividual());
		assertNull(ontologyIndividuals.getNextOntologyIndividual());
	}
}
