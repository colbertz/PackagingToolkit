package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.models.classes.OntologyDurationImpl;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class OntologyDurationPropertyTest {
	private static final String PROPERTY_ID = "datatypeProperty#durationProperty";
	private static final String PROPERTY_LABEL = "durationProperty";
	private static final String PROPERTY_RANGE = "duration";
	
	private OntologyDurationProperty ontologyDurationProperty;
	
	@Before
	public void setUp() {
		ontologyDurationProperty = new OntologyDurationPropertyImpl(PROPERTY_ID, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsDuration() {
		assertFalse(ontologyDurationProperty.isBooleanProperty());
		assertFalse(ontologyDurationProperty.isByteProperty());
		assertFalse(ontologyDurationProperty.isDateProperty());
		assertFalse(ontologyDurationProperty.isDateTimeProperty());
		assertFalse(ontologyDurationProperty.isDoubleProperty());
		assertTrue(ontologyDurationProperty.isDurationProperty());
		assertFalse(ontologyDurationProperty.isFloatProperty());
		assertFalse(ontologyDurationProperty.isIntegerProperty());
		assertFalse(ontologyDurationProperty.isLongProperty());
		assertFalse(ontologyDurationProperty.isShortProperty());
		assertFalse(ontologyDurationProperty.isStringProperty());
		assertFalse(ontologyDurationProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		OntologyDuration expectedDuration = new OntologyDurationImpl(1, 2, 3, 4, 5, 6);
		ontologyDurationProperty.setPropertyValue(expectedDuration);
		OntologyDuration actualDuration = ontologyDurationProperty.getPropertyValue();
		assertEquals(expectedDuration, actualDuration);
	}
}
