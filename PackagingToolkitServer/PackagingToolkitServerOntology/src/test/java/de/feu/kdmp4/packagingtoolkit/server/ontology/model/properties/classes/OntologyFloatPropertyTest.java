package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.feu.kdmp4.packagingtoolkit.server.configuration.FactoryConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.MockedOperationsConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.MinInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@ActiveProfiles("propertiesTest")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
@Category(SpringTests.class)
public class OntologyFloatPropertyTest implements ApplicationContextAware{
	private ApplicationContext applicationContext;
	private static final String PROPERTY_ID = "datatypeProperty#floatProperty";
	private static final String PROPERTY_LABEL = "floatProperty";
	private static final String PROPERTY_RANGE = "float";
	
	private OntologyFloatProperty ontologyFloatProperty;
	
	@Before
	public void setUp() {
		ontologyFloatProperty = new OntologyFloatPropertyImpl(PROPERTY_ID, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsFloat() {
		assertFalse(ontologyFloatProperty.isBooleanProperty());
		assertFalse(ontologyFloatProperty.isByteProperty());
		assertFalse(ontologyFloatProperty.isDateProperty());
		assertFalse(ontologyFloatProperty.isDateTimeProperty());
		assertFalse(ontologyFloatProperty.isDoubleProperty());
		assertFalse(ontologyFloatProperty.isDurationProperty());
		assertTrue(ontologyFloatProperty.isFloatProperty());
		assertFalse(ontologyFloatProperty.isIntegerProperty());
		assertFalse(ontologyFloatProperty.isLongProperty());
		assertFalse(ontologyFloatProperty.isShortProperty());
		assertFalse(ontologyFloatProperty.isStringProperty());
		assertFalse(ontologyFloatProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		Float expectedFloat = 4.4f;
		ontologyFloatProperty.setPropertyValue(expectedFloat);
		Float actualFloat = ontologyFloatProperty.getPropertyValue();
		assertEquals(expectedFloat, actualFloat);
	}
	
	@Test
	public void testMinInclusivePositive1() {
		OntologyFloatProperty ontologyFloatProperty = new OntologyFloatPropertyImpl(PROPERTY_ID,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyFloatProperty);
		String beanName = ontologyFloatProperty.getClass().getSimpleName();
		beanFactory.configureBean(ontologyFloatProperty, beanName);
		
		ontologyFloatProperty.addMinInclusiveRestriction(5.5f);
		
		RestrictionValue<Float> restrictionValue = new RestrictionValue<>(5.5f);
		boolean satisfied = ontologyFloatProperty.areRestrictionsSatisfied(restrictionValue);
		assertTrue(satisfied);
	}

	@Test
	public void testMinInclusivePositive2() {
		OntologyFloatProperty ontologyFloatProperty = new OntologyFloatPropertyImpl(PROPERTY_ID,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyFloatProperty);
		String beanName = ontologyFloatProperty.getClass().getSimpleName();
		beanFactory.configureBean(ontologyFloatProperty, beanName);
		
		ontologyFloatProperty.addMinInclusiveRestriction(5.5f);
		
		RestrictionValue<Float> restrictionValue = new RestrictionValue<>(6.5f);
		boolean satisfied = ontologyFloatProperty.areRestrictionsSatisfied(restrictionValue);
		assertTrue(satisfied);
	}
	
	@Test
	public void testMinInclusiveNegative() {
		OntologyFloatProperty ontologyFloatProperty = new OntologyFloatPropertyImpl(PROPERTY_ID,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyFloatProperty);
		String beanName = ontologyFloatProperty.getClass().getSimpleName();
		beanFactory.configureBean(ontologyFloatProperty, beanName);
		
		ontologyFloatProperty.addMinInclusiveRestriction(5.5f);
		
		RestrictionValue<Float> restrictionValue = new RestrictionValue<>(5.4f);
		boolean satisfied = ontologyFloatProperty.areRestrictionsSatisfied(restrictionValue);
		assertFalse(satisfied);
	}
	
	@Test
	public void testMaxInclusivePositive1() {
		OntologyFloatProperty ontologyFloatProperty = new OntologyFloatPropertyImpl(PROPERTY_ID,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyFloatProperty);
		String beanName = ontologyFloatProperty.getClass().getSimpleName();
		beanFactory.configureBean(ontologyFloatProperty, beanName);
		
		ontologyFloatProperty.addMaxInclusiveRestriction(5.5f);
		
		RestrictionValue<Float> restrictionValue = new RestrictionValue<>(5.5f);
		boolean satisfied = ontologyFloatProperty.areRestrictionsSatisfied(restrictionValue);
		assertTrue(satisfied);
	}

	@Test
	public void testMaxInclusivePositive2() {
		OntologyFloatProperty ontologyFloatProperty = new OntologyFloatPropertyImpl(PROPERTY_ID,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyFloatProperty);
		String beanName = ontologyFloatProperty.getClass().getSimpleName();
		beanFactory.configureBean(ontologyFloatProperty, beanName);
		
		ontologyFloatProperty.addMaxInclusiveRestriction(5.5f);
		
		RestrictionValue<Float> restrictionValue = new RestrictionValue<>(5.4f);
		boolean satisfied = ontologyFloatProperty.areRestrictionsSatisfied(restrictionValue);
		assertTrue(satisfied);
	}
	
	@Test
	public void testMaxInclusiveNegative() {
		OntologyFloatProperty ontologyFloatProperty = new OntologyFloatPropertyImpl(PROPERTY_ID,  PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyFloatProperty);
		String beanName = ontologyFloatProperty.getClass().getSimpleName();
		beanFactory.configureBean(ontologyFloatProperty, beanName);
		
		ontologyFloatProperty.addMaxInclusiveRestriction(5.5f);
		
		RestrictionValue<Float> restrictionValue = new RestrictionValue<>(5.6f);
		boolean satisfied = ontologyFloatProperty.areRestrictionsSatisfied(restrictionValue);
		assertFalse(satisfied);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
