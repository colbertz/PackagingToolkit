package de.feu.kdmp4.packagingtoolkit.server.factories;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.models.classes.OntologyDurationImpl;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.TextInLanguageResponse;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TextInLanguageImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDatePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDateTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDurationPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyFloatPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyLongPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;;

public class ServerResponseFactoryTest {
	private ServerResponseFactory serverResponseFactory;
	
	@Before
	public void setUp() {
		serverResponseFactory = new ServerResponseFactory();
	}
	
	@Test
	public void testCreateDatatypePropertyListResponse() {
		DatatypePropertyCollection datatypePropertyList = setupDatatypePropertyList();
		DatatypePropertyListResponse  datatypePropertyListResponse = serverResponseFactory.toResponse(datatypePropertyList);
		
		// Check the result.
		assertNotNull(datatypePropertyListResponse);
		int actualSize = datatypePropertyListResponse.getOntologyPropertyCount();
		int expectedSize = datatypePropertyList.getPropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		// Check the boolean data property.
		DatatypePropertyResponse property = datatypePropertyListResponse.getOntologyPropertyAt(0);
		OntologyDatatypeResponse datatype =  property.getDatatype();
		assertThat(datatype, is(equalTo(OntologyDatatypeResponse.BOOLEAN)));
		
		// Check the integer data property.
		property = datatypePropertyListResponse.getOntologyPropertyAt(1);
		datatype =  property.getDatatype();
		assertThat(datatype, is(equalTo(OntologyDatatypeResponse.INTEGER)));
		
		// Check the short data property.
		property = datatypePropertyListResponse.getOntologyPropertyAt(2);
		datatype =  property.getDatatype();
		assertThat(datatype, is(equalTo(OntologyDatatypeResponse.SHORT)));
		
		// Check the string data property.
		property = datatypePropertyListResponse.getOntologyPropertyAt(3);
		datatype =  property.getDatatype();
		assertThat(datatype, is(equalTo(OntologyDatatypeResponse.STRING)));		
	}
	
	private DatatypePropertyCollection setupDatatypePropertyList() {
		DatatypePropertyCollection datatypePropertyList = new DatatypePropertyCollectionImpl();
		OntologyDatatypeProperty booleanProperty = new OntologyBooleanPropertyImpl();
		OntologyDatatypeProperty integerProperty = new OntologyIntegerPropertyImpl();
		OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl();
		
		datatypePropertyList.addDatatypeProperty(booleanProperty);
		datatypePropertyList.addDatatypeProperty(integerProperty);
		datatypePropertyList.addDatatypeProperty(shortProperty);
		datatypePropertyList.addDatatypeProperty(stringProperty);
		
		return datatypePropertyList;
	}
	
	@Test
	public void testToBooleanPropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "boolean";
		OntologyBooleanProperty booleanProperty = new OntologyBooleanPropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(booleanProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsBoolean();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.BOOLEAN;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(false)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToBooleanPropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "boolean";
		boolean expectedValue = true;
		OntologyBooleanProperty booleanProperty = new OntologyBooleanPropertyImpl(expectedId, expectedLabel, expectedRange);
		booleanProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(booleanProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsBoolean();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.BOOLEAN;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToBytePropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "boolean";
		OntologyByteProperty byteProperty = new OntologyBytePropertyImpl(expectedId, true, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(byteProperty);
		short expectedValue = 0;
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsByte();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.BYTE;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToBytePropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "byte";
		short expectedValue = 4;
		
		OntologyByteProperty byteProperty = new OntologyBytePropertyImpl(expectedId, true, expectedLabel, expectedRange);
		byteProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(byteProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsByte();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.BYTE;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDatePropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Date";
		OntologyDateProperty dateProperty = new OntologyDatePropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(dateProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDate();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DATE;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(nullValue()));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDatePropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "byte";
		LocalDate expectedValue = LocalDate.now();
		
		OntologyDateProperty dateProperty = new OntologyDatePropertyImpl(expectedId, expectedLabel, expectedRange);
		dateProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(dateProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDate();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DATE;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDateTimePropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "DateTime";
		OntologyDateTimeProperty dateTimeProperty = new OntologyDateTimePropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(dateTimeProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDateTime();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DATETIME;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(nullValue()));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDateTimePropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "DateTime";
		LocalDateTime expectedValue = LocalDateTime.now();
		// Delete the nano seconds because because of them the test can fail. They are not necessary.
		int nanos = expectedValue.getNano();
		expectedValue = expectedValue.minusNanos(nanos);
		
		OntologyDateTimeProperty dateTimeProperty = new OntologyDateTimePropertyImpl(expectedId, expectedLabel, expectedRange);
		dateTimeProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(dateTimeProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDateTime();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DATETIME;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDoublePropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Double";
		OntologyDoubleProperty doubleProperty = new OntologyDoublePropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(doubleProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDateTime();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DOUBLE;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(nullValue()));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDoublePropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Double";
		Double expectedValue = 0.5;
		
		OntologyDoubleProperty doubleProperty = new OntologyDoublePropertyImpl(expectedId, expectedLabel, expectedRange);
		doubleProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(doubleProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDouble();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DOUBLE;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDurationPropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Duration";
		OntologyDurationProperty durationProperty = new OntologyDurationPropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(durationProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDuration();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DURATION;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(nullValue()));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToDurationPropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Duration";
		OntologyDuration expectedValue = new OntologyDurationImpl(1, 2, 3, 4, 5, 6);
		
		OntologyDurationProperty doubleProperty = new OntologyDurationPropertyImpl(expectedId, expectedLabel, expectedRange);
		doubleProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(doubleProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDuration();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.DURATION;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToFloatPropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Float";
		OntologyFloatProperty floatProperty = new OntologyFloatPropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(floatProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsDateTime();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.FLOAT;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(nullValue()));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToFloatPropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Float";
		Float expectedValue = 0.5f;
		
		OntologyFloatProperty floatProperty = new OntologyFloatPropertyImpl(expectedId, expectedLabel, expectedRange);
		floatProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(floatProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsFloat();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.FLOAT;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToIntegerPropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Integer";
		OntologyIntegerProperty integerProperty = new OntologyIntegerPropertyImpl(expectedId, true, Sign.BOTH, true, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(integerProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsInteger();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.INTEGER;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(0L));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToIntegerPropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Integer";
		Long expectedValue = 100L;
		
		OntologyIntegerProperty integerProperty = new OntologyIntegerPropertyImpl(expectedId, true, Sign.BOTH, true, expectedLabel, expectedRange);
		integerProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(integerProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsInteger();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.INTEGER;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToLongPropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Long";
		BigInteger expectedValue = BigInteger.valueOf(0L);
		OntologyLongProperty longProperty = new OntologyLongPropertyImpl(expectedId, true, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(longProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsLong();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.LONG;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToLongPropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Long";
		BigInteger expectedValue = BigInteger.valueOf(1000L);
		
		OntologyLongProperty integerProperty = new OntologyLongPropertyImpl(expectedId, true, expectedLabel, expectedRange);
		integerProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(integerProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsLong();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.LONG;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToShortPropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Short";

		OntologyShortProperty shortProperty = new OntologyShortPropertyImpl(expectedId, true, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(shortProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsShort();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.SHORT;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(0)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToShortPropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Short";
		Integer expectedValue = 5;
		
		OntologyShortProperty shortProperty = new OntologyShortPropertyImpl(expectedId, true, expectedLabel, expectedRange);
		shortProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(shortProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsShort();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.SHORT;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToStringPropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "String";

		OntologyStringProperty stringProperty = new OntologyStringPropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(stringProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsString();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.STRING;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo("")));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToStringPropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "String";
		String expectedValue = "qwertz";
		
		OntologyStringProperty stringProperty = new OntologyStringPropertyImpl(expectedId, expectedLabel, expectedRange);
		stringProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(stringProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsString();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.STRING;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToTimePropertyResponseWithoutValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Time";
		OntologyTimeProperty dateTimeProperty = new OntologyTimePropertyImpl(expectedId, expectedLabel, expectedRange);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(dateTimeProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsTime();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.TIME;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(nullValue()));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToTimePropertyResponseWithValue() {
		String expectedLabel = "abc";
		String expectedId = "xyz";
		String expectedRange = "Time";
		LocalTime expectedValue = LocalTime.now();
		
		OntologyTimeProperty dateTimeProperty = new OntologyTimePropertyImpl(expectedId, expectedLabel, expectedRange);
		dateTimeProperty.setPropertyValue(expectedValue);
		DatatypePropertyResponse datatypePropertyResponse = serverResponseFactory.toResponse(dateTimeProperty);
		
		// Check the results.
		String actualLabel = datatypePropertyResponse.getLabel();
		String actualId = datatypePropertyResponse.getPropertyName();
		Object actualValue = datatypePropertyResponse.getValueAsTime();
		OntologyDatatypeResponse actualDatatype = datatypePropertyResponse.getDatatype();
		OntologyDatatypeResponse expectedDatatype = OntologyDatatypeResponse.TIME;
		
		assertThat(actualLabel, is(equalTo(expectedLabel)));
		assertThat(actualId, is(equalTo(expectedId)));
		assertThat(actualValue, is(equalTo(expectedValue)));
		assertThat(actualDatatype, is(equalTo(expectedDatatype)));
	}
	
	@Test
	public void testToTextInLanguageResponse() {
		final String expectedText = "A Text";
		TextInLanguage textInLanguage = new TextInLanguageImpl(expectedText, OntologyLabelLanguage.GERMAN);
		TextInLanguageResponse actualTextInLanguage = ServerResponseFactory.toResponse(textInLanguage);
		assertThat(actualTextInLanguage.getText(), is(equalTo(expectedText)));
		assertThat(actualTextInLanguage.getLanguage(), is(equalTo("de")));
	}
	
	@Test
	public void testFromTaxonomyIndividual() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		
		final TaxonomyIndividualResponse taxonomyIndividualResponse = ServerResponseFactory.fromTaxonomyIndividual(taxonomyIndividual);
		final boolean valuesEqual = TaxonomyTestApi.areValuesEqual(taxonomyIndividual, taxonomyIndividualResponse);
		assertTrue(valuesEqual);
	}
}
