package de.feu.kdmp4.packagingtoolkit.testcategories;

/**
 * A category for all tests that use Spring.
 * @author Christopher Olbertz
 *
 */
public interface SpringTests {

}
