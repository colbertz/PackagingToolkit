package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.RestrictionCollection;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class RestrictionListTest {
	private RestrictionCollection restrictionList;
	private Restriction restricition1;
	private Restriction restricition2;
	
	@Before
	public void setUp() throws Exception {
		restrictionList = new RestrictionCollectionImpl();
		RestrictionValue<Integer> restrictionValue1 = new RestrictionValue<Integer>(5);
		restricition1 = new MaxInclusiveImpl(restrictionValue1);
		restrictionList.addRestriction(restricition1);
		RestrictionValue<Integer> restrictionValue2 = new RestrictionValue<Integer>(3);
		restricition2 = new MaxInclusiveImpl(restrictionValue2);
		restrictionList.addRestriction(restricition2);
	}

	@Test
	public void testGetRestriction() {
		Restriction actualRestriction1 = restrictionList.getRestriction(0);
		Restriction actualRestriction2 = restrictionList.getRestriction(1);
		assertThat(actualRestriction1, is(equalTo(restricition1)));
		assertThat(actualRestriction2, is(equalTo(restricition2)));
	}
	
	@Test
	public void testAddRestriction() {
		int expectedRestrictionCount = restrictionList.getRestrictionsCount();
		expectedRestrictionCount++;
		
		RestrictionValue<Integer> restrictionValue3 = new RestrictionValue<Integer>(30);
		restrictionList.addRestriction(new MaxInclusiveImpl(restrictionValue3));
		int actualRestrictionCount = restrictionList.getRestrictionsCount();
		assertThat(actualRestrictionCount, is(equalTo(expectedRestrictionCount)));
	}	
	
	
}
