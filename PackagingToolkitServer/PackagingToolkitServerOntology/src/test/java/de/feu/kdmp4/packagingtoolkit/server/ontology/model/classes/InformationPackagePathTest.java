package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.InformationPackagePath;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

// DELETE_ME Wird wahrscheinlich nicht mehr benoetigt.
@Category(NonSpringTests.class)
public class InformationPackagePathTest {
	private InformationPackagePath informationPackagePath;
	
	@Before
	public void setUp() {
		informationPackagePath = new InformationPackagePathImpl();
		informationPackagePath.addPathComponent("a");
		informationPackagePath.addPathComponent("b");
		informationPackagePath.addPathComponent("c");
		assertThat(informationPackagePath.getComponentCount(), is(equalTo(3)));
	}
	
	@Test
	public void testAddComponent() {
		informationPackagePath.addPathComponent("d");
		String expectedPath = "a/b/c/d/";
		assertThat(informationPackagePath.getComponentCount(), is(equalTo(4)));
		assertThat(informationPackagePath.toString(), is(equalTo(expectedPath)));
	}

	@Test
	public void testDeleteLastPath() {
		informationPackagePath.deleteLastPathComponent();
		String expectedPath = "a/b/";
		assertThat(informationPackagePath.getComponentCount(), is(equalTo(2)));
		assertThat(informationPackagePath.toString(), is(equalTo(expectedPath)));
	}
	
	@Test
	public void testToString() {
		String actualPath = informationPackagePath.toString();
		String expectedPath = "a/b/c/";
		assertThat(actualPath, is(equalTo(expectedPath)));
	}
}
