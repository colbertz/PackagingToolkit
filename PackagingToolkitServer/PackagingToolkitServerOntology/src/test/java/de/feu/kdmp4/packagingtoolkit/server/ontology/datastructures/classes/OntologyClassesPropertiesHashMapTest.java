package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.DataStructureFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

//@ActiveProfiles("propertiesTest")
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
//@Category(SpringTests.class)
public class OntologyClassesPropertiesHashMapTest {
	private static final String NAMESPACE = "abc";
	private static final String CLASSNAME_1 = "A";
	private static final String CLASSNAME_2 = "B";
	private static final String CLASSNAME_3 = "C";
	private static final String CLASSNAME_4 = "D";
	
	private static final String PROPERTY_1 = "X";
	private static final String PROPERTY_2 = "Y";
	private static final String PROPERTY_3 = "Z";
	private static final String PROPERTY_4 = "ZZ";
	
	private OntologyClass ontologyClass1;
	private OntologyClass ontologyClass2;
	private OntologyClass ontologyClass3;
	private OntologyClass ontologyClass4;
	private OntologyClassesPropertiesMap ontologyClassesPropertiesMap;
	
	private OntologyDatatypeProperty property1;
	private OntologyDatatypeProperty property2;
	private OntologyDatatypeProperty property3;
	private OntologyDatatypeProperty property4;
	
	//@Autowired
	private DataStructureFactory dataStructureFactory;
	//@Autowired
	private OntologyModelFactory ontologyModelFactory;
	//@Autowired
	private PropertyFactory propertyFactory;
	
	@Before
	public void setUp() {
		ontologyClass1 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_1));
		ontologyClass2 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_2));
		ontologyClass3 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_3));
		ontologyClass4 = ontologyModelFactory.createOntologyClass(ServerModelFactory.createNamespace(NAMESPACE), ServerModelFactory.createLocalName(CLASSNAME_4));
		ontologyClassesPropertiesMap = dataStructureFactory.createOntologyClassesPropertiesMap();
		
		property1 = propertyFactory.getProperty("boolean", PROPERTY_1, PROPERTY_1);
		property1.setPropertyValue(true);
		property2 = propertyFactory.getProperty("string", PROPERTY_2, PROPERTY_2);
		property2.setPropertyValue("bla");
		property3 = propertyFactory.getProperty("string", PROPERTY_3, PROPERTY_3);
		property3.setPropertyValue("blub");
		property4 = propertyFactory.getProperty("integer", PROPERTY_4, PROPERTY_4);
		property4.setPropertyValue(4);
		
		ontologyClass1.addDatatypeProperty(property1);
		ontologyClass1.addDatatypeProperty(property2);
		ontologyClassesPropertiesMap.addClass(ontologyClass1);
		//ontologyClassesPropertiesMap.addProperty(NAMESPACE + "#" + CLASSNAME_1, ontologyProperty);
	}
	
	@Test
	public void testAddClass() {
		ontologyClassesPropertiesMap.addClass(ontologyClass1);
		assertThat(ontologyClassesPropertiesMap.getDatatypePropertyListsCount(), is(equalTo(1)));
		DatatypePropertyCollection firstPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_1);
	
		OntologyProperty ontologyProperty1 = firstPropertyList.getDatatypeProperty(0);
		assertThat(ontologyProperty1, is(equalTo(property1)));
		OntologyProperty ontologyProperty2 = firstPropertyList.getDatatypeProperty(1);
		assertThat(ontologyProperty2, is(equalTo(property2)));
		
		ontologyClassesPropertiesMap.addClass(ontologyClass2);
		assertThat(ontologyClassesPropertiesMap.getDatatypePropertyListsCount(), is(equalTo(2)));
		DatatypePropertyCollection secondPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_2);
		boolean propertyListEmpty = secondPropertyList.isEmpty();
		assertTrue(propertyListEmpty);
	}
	
	@Test
	public void testGetOntologyClassesCount() {
		ontologyClassesPropertiesMap.addClass(ontologyClass1);
		ontologyClassesPropertiesMap.addClass(ontologyClass2);
		ontologyClassesPropertiesMap.addClass(ontologyClass3);
		ontologyClassesPropertiesMap.addClass(ontologyClass4);
		int size = ontologyClassesPropertiesMap.getDatatypePropertyListsCount();
		assertThat(size, is(equalTo(4)));
	}
	
	@Test
	public void testGetProperties() {
		ontologyClassesPropertiesMap.addClass(ontologyClass1);
		ontologyClassesPropertiesMap.addClass(ontologyClass2);
		ontologyClassesPropertiesMap.addClass(ontologyClass3);
		ontologyClassesPropertiesMap.addClass(ontologyClass4);
		
		DatatypePropertyCollection firstPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_1);
		OntologyProperty ontologyProperty1 = firstPropertyList.getDatatypeProperty(0);
		assertThat(ontologyProperty1, is(equalTo(property1)));
		OntologyProperty ontologyProperty2 = firstPropertyList.getDatatypeProperty(1);
		assertThat(ontologyProperty2, is(equalTo(property2)));
		
		DatatypePropertyCollection secondPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_2);
		boolean propertyListEmpty = secondPropertyList.isEmpty();
		assertTrue(propertyListEmpty);
		
		DatatypePropertyCollection thirdPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_3);
		propertyListEmpty = thirdPropertyList.isEmpty();
		assertTrue(propertyListEmpty);
		
		DatatypePropertyCollection fourthPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_4);
		propertyListEmpty = fourthPropertyList.isEmpty();
		assertTrue(propertyListEmpty);
	}

	@Test
	public void testAddProperty() {
		ontologyClassesPropertiesMap.addProperty(NAMESPACE + "#" + CLASSNAME_1, property3);
		ontologyClassesPropertiesMap.addProperty(NAMESPACE + "#" + CLASSNAME_2, property3);
		ontologyClassesPropertiesMap.addProperty(NAMESPACE + "#" + CLASSNAME_2, property4);
		
		DatatypePropertyCollection firstPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_1);
		OntologyProperty ontologyProperty1 = firstPropertyList.getDatatypeProperty(0);
		assertThat(ontologyProperty1, is(equalTo(property1)));
		OntologyProperty ontologyProperty2 = firstPropertyList.getDatatypeProperty(1);
		assertThat(ontologyProperty2, is(equalTo(property2)));
		OntologyProperty ontologyProperty3 = firstPropertyList.getDatatypeProperty(2);
		assertThat(ontologyProperty3, is(equalTo(property3)));
		
		DatatypePropertyCollection secondPropertyList = ontologyClassesPropertiesMap.getDatatypeProperties(NAMESPACE + "#" + CLASSNAME_2);
		OntologyProperty ontologyProperty4 = secondPropertyList.getDatatypeProperty(0);
		assertThat(ontologyProperty4, is(equalTo(property3)));
		OntologyProperty ontologyProperty5 = secondPropertyList.getDatatypeProperty(1);
		assertThat(ontologyProperty5, is(equalTo(property4)));
	}
}
