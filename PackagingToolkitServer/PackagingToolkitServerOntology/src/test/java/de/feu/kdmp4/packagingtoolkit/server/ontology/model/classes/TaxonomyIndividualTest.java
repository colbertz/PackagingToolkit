package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;

public class TaxonomyIndividualTest {
	/**
	 * Tests the construction of an individual. The iri has to be the iri that has been provided to the constructor and
	 * there may not be any narrowers in this individual.
	 */
	@Test
	public void testConstructor() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Iri iriOfIndividual = taxonomyIndividual.getIri();
		
		assertNotNull(taxonomyIndividual);
		assertThat(taxonomyIndividual.getIri(), is(equalTo(iriOfIndividual)));
		assertThat(taxonomyIndividual.getNarrowerCount(), is(equalTo(0)));
	}
	
	/**
	 * Tests the method for adding narrowers. Three narrowers are added to this individual and
	 * after adding a narrower, the test checks if the number of narrowers has been increased and
	 * if the narrower is contained in the individual. 
	 */
	@Test
	public void testAddNarrower() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual narrower1 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower1);
		assertThat(taxonomyIndividual.getNarrowerCount(), is(equalTo(1)));
		assertTrue(taxonomyIndividual.containsNarrower(narrower1));
		
		final TaxonomyIndividual narrower2 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower2);
		assertThat(taxonomyIndividual.getNarrowerCount(), is(equalTo(2)));
		assertTrue(taxonomyIndividual.containsNarrower(narrower2));
		
		final TaxonomyIndividual narrower3 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower3);
		assertThat(taxonomyIndividual.getNarrowerCount(), is(equalTo(3)));
		assertTrue(taxonomyIndividual.containsNarrower(narrower3));
	}
	
	/**
	 * Tests the method that checks if an individual is narrower. In this test the individual is actually a narrower.
	 */
	@Test
	public void testContainsNarrower_resultTrue() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual narrower = TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		final boolean containsNarrower = taxonomyIndividual.containsNarrower(narrower);
		assertTrue(containsNarrower);
	}
	
	/**
	 * Tests the method that checks if an individual is narrower. We create an individual that is not a narrower and
	 * expect the result false.
	 */
	@Test
	public void testContainsNarrower_resultFalse() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		final TaxonomyIndividual narrower = TaxonomyTestApi.createTaxonomyIndividual();
		final boolean containsNarrower = taxonomyIndividual.containsNarrower(narrower);
		assertFalse(containsNarrower);
	}
	
	/**
	 * Tests the method that looks for a narrower by its iri. In this case the narrower is found.
	 */
	@Test
	public void testFindNarrower_resultFound() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual narrower = TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		final Optional<TaxonomyIndividual> optionalWithNarrower = taxonomyIndividual.findNarrower(narrower.getIri());
		
		assertTrue(optionalWithNarrower.isPresent());
		final TaxonomyIndividual actualNarrower = optionalWithNarrower.get();
		assertThat(actualNarrower, is(equalTo(narrower)));
	}

	/**
	 * Tests the method that looks for a narrower by its iri. In this case the narrower is not found.
	 */
	@Test
	public void testFindNarrower_resultNotFound() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		final TaxonomyIndividual narrower = TaxonomyTestApi.createTaxonomyIndividual();
		final Optional<TaxonomyIndividual> optionalWithNarrower = taxonomyIndividual.findNarrower(narrower.getIri());
		
		assertFalse(optionalWithNarrower.isPresent());
	}

	/**
	 * Tests the method that looks for a narrower by its iri. In this case the individual does not have any narrowers.
	 */
	@Test
	public void testFindNarrower_resultNoNarrowers() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final TaxonomyIndividual narrower = TaxonomyTestApi.createTaxonomyIndividual();
		final Optional<TaxonomyIndividual> optionalWithNarrower = taxonomyIndividual.findNarrower(narrower.getIri());
		
		assertFalse(optionalWithNarrower.isPresent());
	}
	
	/**
	 * Tests the method that checks if an individual is assigned to a taxonomy. In this case the individual is
	 * assigned to the taxonomy.
	 */
	@Test
	public void testIsInTaxonomy_resultTrue() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final String iriOfNamespace = taxonomyIndividual.getInScheme().toString();
		final Namespace scheme = new NamespaceImpl(iriOfNamespace);
		final Taxonomy taxonomy = new TaxonomyImpl(taxonomyIndividual, "A Title", scheme);
		final boolean assignedToTaxonomy = taxonomyIndividual.isInTaxonomy(taxonomy);
		assertTrue(assignedToTaxonomy);
	}
	
	/**
	 * Tests the method that checks if an individual is assigned to a taxonomy. In this case the individual is
	 * not assigned to the taxonomy.
	 */
	@Test
	public void testIsInTaxonomy_resultFalse() {
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Namespace scheme = new NamespaceImpl("http://anamespace");
		final Taxonomy taxonomy = new TaxonomyImpl(taxonomyIndividual, "A Title", scheme);
		final boolean assignedToTaxonomy = taxonomyIndividual.isInTaxonomy(taxonomy);
		assertFalse(assignedToTaxonomy);
	}
	
	/**
	 * Tests the check for narrowers with an individual with three narrowers. 
	 */
	@Test
	public void testContainsNarrowers_threeNarrowers_resultTrue() {
		final TaxonomyIndividual taxonomyIndividual = new TaxonomyIndividualImpl(new IriImpl("http://mynamespace#abc"), "abc", new NamespaceImpl("http://mynamespace"));
		TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		assertTrue(taxonomyIndividual.containsNarrowers());
	}
	
	/**
	 * Tests the check for narrowers with an individual with only one narrower. 
	 */
	@Test
	public void testContainsNarrowers_oneNarrower_resultTrue() {
		final TaxonomyIndividual taxonomyIndividual = new TaxonomyIndividualImpl(new IriImpl("http://mynamespace#abc"), "abc", new NamespaceImpl("http://mynamespace"));
		TaxonomyTestApi.addOneNarrowerToTaxonomyIndividual(taxonomyIndividual);
		assertTrue(taxonomyIndividual.containsNarrowers());
	}
	
	/**
	 * Tests the check for narrowers with an individual that does not contain any narrowers.
	 */
	@Test
	public void testContainsNarrowers_resultFalse() {
		final TaxonomyIndividual taxonomyIndividual = new TaxonomyIndividualImpl(new IriImpl("http://mynamespace#abc"), "abc", new NamespaceImpl("http://mynamespace"));
		assertFalse(taxonomyIndividual.containsNarrowers());
	}
	
	/**
	 * Tests if the individual can recognize that there is another narrower.
	 */
	@Test
	public void testHasNextNarrower_resultTrue() {
		final TaxonomyIndividual taxonomyIndividual  = new TaxonomyIndividualImpl(new IriImpl("http://mynamespace#abc"), "abc", new NamespaceImpl("http://mynamespace"));
		TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		assertTrue(taxonomyIndividual.hasNextNarrower());
	}
	
	/**
	 * Tests if the individual can recognize that there is not another narrower with an empty list of narrowers.
	 */
	@Test
	public void testHasNextNarrower_resultFalse() {
		final TaxonomyIndividual taxonomyIndividual  = new TaxonomyIndividualImpl(new IriImpl("http://mynamespace#abc"), "abc", new NamespaceImpl("http://mynamespace"));
		assertFalse(taxonomyIndividual.hasNextNarrower());
	}

	/**
	 * Tests if the next narrower can be determined. The individual is initialized with three narrowers. Then we try to determine the three narrowers. 
	 * Every time we determine a narrower, we check if there is a next narrower and if the optional with the next narrower is not empty. The fourth 
	 * time we check if there is not a next narrower and the optional with the next narrower has to be empty.
	 */
	@Test
	public void testGetNextNarrower() {
		final TaxonomyIndividual taxonomyIndividual  = new TaxonomyIndividualImpl(new IriImpl("http://mynamespace#abc"), "abc", new NamespaceImpl("http://mynamespace"));
		TaxonomyTestApi.addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual);
		
		assertTrue(taxonomyIndividual.hasNextNarrower());
		Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = taxonomyIndividual.getNextNarrower();
		assertTrue(optionalWithTaxonomyIndividual.isPresent());
		
		assertTrue(taxonomyIndividual.hasNextNarrower());
		optionalWithTaxonomyIndividual = taxonomyIndividual.getNextNarrower();
		assertTrue(optionalWithTaxonomyIndividual.isPresent());
		
		assertTrue(taxonomyIndividual.hasNextNarrower());
		optionalWithTaxonomyIndividual = taxonomyIndividual.getNextNarrower();
		assertTrue(optionalWithTaxonomyIndividual.isPresent());
		
		assertFalse(taxonomyIndividual.hasNextNarrower());
		optionalWithTaxonomyIndividual = taxonomyIndividual.getNextNarrower();
		assertFalse(optionalWithTaxonomyIndividual.isPresent());
	}
	
	/**
	 * Tests if the next narrower can be determined. The individual does not contain any narrowers.  We check if there is not a 
	 * next narrower and the optional with the next narrower has to be empty.
	 */
	@Test
	public void testGetNextNarrower_noNarrowers() {
		final TaxonomyIndividual taxonomyIndividual  = new TaxonomyIndividualImpl(new IriImpl("http://mynamespace#abc"), "abc", new NamespaceImpl("http://mynamespace"));
		
		assertFalse(taxonomyIndividual.hasNextNarrower());
		Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = taxonomyIndividual.getNextNarrower();
		assertFalse(optionalWithTaxonomyIndividual.isPresent());
	}
}
