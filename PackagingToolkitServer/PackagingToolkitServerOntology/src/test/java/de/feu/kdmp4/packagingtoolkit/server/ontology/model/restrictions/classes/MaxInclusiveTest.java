package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class MaxInclusiveTest {
	private RestrictionValue<Integer> restrictionValue;
	private Restriction restriction;
	
	@Before
	public void setUp() throws Exception {
		restrictionValue = new RestrictionValue<Integer>(5);
		restriction = new MaxInclusiveImpl(restrictionValue);
	}

	@Test(expected = RestrictionNotSatisfiedException.class)
	public void testCheckRestriction1() {
		RestrictionValue<Integer> compareValue = new RestrictionValue<>(30);
		restriction.checkRestriction(compareValue);
	}	
	
	@Test
	public void testCheckRestriction2() {
		RestrictionValue<Integer> compareValue = new RestrictionValue<>(5);
		restriction.checkRestriction(compareValue);
	}
	
	@Test
	public void testCheckRestriction3() {
		RestrictionValue<Integer> compareValue = new RestrictionValue<>(-5);
		restriction.checkRestriction(compareValue);
	}	
}
