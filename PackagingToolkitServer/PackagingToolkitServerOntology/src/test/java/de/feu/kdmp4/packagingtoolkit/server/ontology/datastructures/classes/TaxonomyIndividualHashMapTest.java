package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import de.feu.kdmp4.packagingtoolit.server.ontology.model.interfaces.TaxonomyIndividualMap;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyIndividualHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.testapi.TaxonomyTestApi;

public class TaxonomyIndividualHashMapTest {
	/**
	 * Tests if an individual can be added to the map.
	 */
	@Test
	public void testAddIndividual() {
		final TaxonomyIndividualMap taxonomyIndividualMap = new TaxonomyIndividualHashMapImpl();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Iri iriOfIndividual = taxonomyIndividual.getIri();
		
		taxonomyIndividualMap.addTaxonomyIndividual(taxonomyIndividual);
		final boolean containsIndividual = taxonomyIndividualMap.containsTaxonomyIndividual(iriOfIndividual);
		assertTrue(containsIndividual);
		final Optional<TaxonomyIndividual> optionalWithFoundTaxonomyIndividual = taxonomyIndividualMap.getTaxonomyIndividual(iriOfIndividual);
		assertTrue(optionalWithFoundTaxonomyIndividual.isPresent());
		final TaxonomyIndividual foundTaxonomyIndividual = optionalWithFoundTaxonomyIndividual.get();
		assertThat(foundTaxonomyIndividual, is(equalTo(taxonomyIndividual)));
	}
	
	/**
	 * Tests if an individual can be added to the map with another iri than its own.
	 */
	@Test
	public void testAddIndividual_withAnotherIri() {
		final TaxonomyIndividualMap taxonomyIndividualMap = new TaxonomyIndividualHashMapImpl();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Iri iri = new IriImpl(new Uuid());
		
		taxonomyIndividualMap.addTaxonomyIndividual(iri, taxonomyIndividual);
		final boolean containsIndividual = taxonomyIndividualMap.containsTaxonomyIndividual(iri);
		assertTrue(containsIndividual);
		final Optional<TaxonomyIndividual> optionalWithFoundTaxonomyIndividual = taxonomyIndividualMap.getTaxonomyIndividual(iri);
		assertTrue(optionalWithFoundTaxonomyIndividual.isPresent());
		final TaxonomyIndividual foundTaxonomyIndividual = optionalWithFoundTaxonomyIndividual.get();
		assertThat(foundTaxonomyIndividual, is(equalTo(taxonomyIndividual)));
	}
	
	/**
	 * Tests if an individual is determined as contained in the map.
	 */
	@Test
	public void testContainsIndividual_resultTrue() {
		final TaxonomyIndividualMap taxonomyIndividualMap = new TaxonomyIndividualHashMapImpl();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.addThreeIndividualsToTaxonomyIndividualMap(taxonomyIndividualMap);
		final Iri  iriOfIndividual = taxonomyIndividual.getIri();
		
		final boolean individualContained = taxonomyIndividualMap.containsTaxonomyIndividual(iriOfIndividual);
		assertTrue(individualContained);
	}
	
	/**
	 * Tests if an individual is determined as not contained in the map.
	 */
	@Test
	public void testContainsIndividual_resultFalse() {
		final TaxonomyIndividualMap taxonomyIndividualMap = new TaxonomyIndividualHashMapImpl();
		TaxonomyTestApi.addThreeIndividualsToTaxonomyIndividualMap(taxonomyIndividualMap);
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Iri  iriOfIndividual = taxonomyIndividual.getIri();
		
		final boolean individualContained = taxonomyIndividualMap.containsTaxonomyIndividual(iriOfIndividual);
		assertFalse(individualContained);
	}
	
	/**
	 * Tests if an individual can be get from the map.
	 */
	@Test
	public void testGetIndividual_resultAnIndividual() {
		final TaxonomyIndividualMap taxonomyIndividualMap = new TaxonomyIndividualHashMapImpl();
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.addThreeIndividualsToTaxonomyIndividualMap(taxonomyIndividualMap);
		final Iri  iriOfIndividual = taxonomyIndividual.getIri();
		
		final Optional<TaxonomyIndividual> optionalWithFoundTaxonomyIndividual = taxonomyIndividualMap.getTaxonomyIndividual(iriOfIndividual);
		assertTrue(optionalWithFoundTaxonomyIndividual.isPresent());
		final TaxonomyIndividual foundTaxonomyIndividual = optionalWithFoundTaxonomyIndividual.get();
		assertThat(foundTaxonomyIndividual, is(equalTo(taxonomyIndividual)));
	}
	
	/**
	 * Tests if we get an empty optional if an individual can not be found in the map.
	 */
	@Test
	public void testGetIndividual_resultEmptyOptional() {
		final TaxonomyIndividualMap taxonomyIndividualMap = new TaxonomyIndividualHashMapImpl();
		TaxonomyTestApi.addThreeIndividualsToTaxonomyIndividualMap(taxonomyIndividualMap);
		final TaxonomyIndividual taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Iri  iriOfIndividual = taxonomyIndividual.getIri();
		
		final Optional<TaxonomyIndividual> optionalWithFoundTaxonomyIndividual = taxonomyIndividualMap.getTaxonomyIndividual(iriOfIndividual);
		assertFalse(optionalWithFoundTaxonomyIndividual.isPresent());
	}
}
