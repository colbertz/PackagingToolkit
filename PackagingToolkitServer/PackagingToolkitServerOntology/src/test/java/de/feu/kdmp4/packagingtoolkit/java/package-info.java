/**
 * This package contains some test for evaluating some features of the programming lanuage Java. 
 * The classes are not testing real features of PackagingToolkit. They are some precise experiments
 * for learning the work with some features of java. 
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.java;