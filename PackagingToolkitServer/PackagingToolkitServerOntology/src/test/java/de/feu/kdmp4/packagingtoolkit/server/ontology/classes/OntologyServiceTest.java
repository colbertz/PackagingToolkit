package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TriplePredicateImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleSubjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.server.triplestore.facades.OntologyTripleStoreFacade;

public class OntologyServiceTest {
	private OntologyService ontologyService;
	
	@Before
	public void setUp() {
		ontologyService = new OntologyServiceImpl();
	}

	//****************************************************
	// testGetIndiviualsByOntologyClassAndInformationPackage
	//****************************************************
	@Test
	public void testGetIndiviualsByOntologyClassAndInformationPackage() {
		final Uuid uuidOfInformationPackage = new Uuid();
		final LocalName localName = ServerModelFactory.createLocalName("aClass");
		Iri iriOfClass = ServerModelFactory.createIri(RdfElementFactory.getPackagingToolkitDefaultNamespace(), localName);
		prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_mockOntologyTripleStoreFacade(iriOfClass.toString(), uuidOfInformationPackage);
		prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_mockOntologyCache(iriOfClass.toString());
		
		IndividualListResponse individualListResponse = ontologyService.getIndiviualsByOntologyClassAndInformationPackage(
				iriOfClass.toString(), uuidOfInformationPackage.toString());
		
		int actualSize = individualListResponse.getIndividualCount();
		int expectedSize = 6;
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	/**
	 * Creates three property statement for every individual in the test.
	 * @param uuidsOfIndividuals The uuids of the individuals for the test.
	 * @return The statements that describe the individuals.
	 */
	private TripleStatementCollection prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_statementTripleList(
			List<Uuid> uuidsOfIndividuals) {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		
		for (Uuid uuid: uuidsOfIndividuals) {
			char propertyName = 'a';
			TripleSubject tripleSubject1 = new TripleSubjectImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace() + 
					PackagingToolkitConstants.NAMESPACE_SEPARATOR + uuid.toString());
			TriplePredicate triplePredicate1 = new TriplePredicateImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace() + 
					PackagingToolkitConstants.NAMESPACE_SEPARATOR + propertyName);
			TripleObject tripleObject1 = new TripleObjectImpl((int)propertyName++);
			tripleStatementList.addTripleStatement(new TripleStatementImpl(tripleSubject1, triplePredicate1, tripleObject1));
			
			TripleSubject tripleSubject2 = new TripleSubjectImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace() + 
					PackagingToolkitConstants.NAMESPACE_SEPARATOR + uuid.toString());
			TriplePredicate triplePredicate2 = new TriplePredicateImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace() + 
					PackagingToolkitConstants.NAMESPACE_SEPARATOR + propertyName);
			TripleObject tripleObject2 = new TripleObjectImpl((int)propertyName++);
			tripleStatementList.addTripleStatement(new TripleStatementImpl(tripleSubject2, triplePredicate2, tripleObject2));
			
			TripleSubject tripleSubject3 = new TripleSubjectImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace() + 
					PackagingToolkitConstants.NAMESPACE_SEPARATOR + uuid.toString());
			TriplePredicate triplePredicate3 = new TriplePredicateImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace() + 
					PackagingToolkitConstants.NAMESPACE_SEPARATOR + propertyName);
			TripleObject tripleObject3 = new TripleObjectImpl((int)propertyName++);
			tripleStatementList.addTripleStatement(new TripleStatementImpl(tripleSubject3, triplePredicate3, tripleObject3));
		}
		
		return tripleStatementList;
	}
	
	private void prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_mockOntologyTripleStoreFacade(String iriOfClass,
			Uuid uuidOfInformationPackage) {
		List<Uuid> uuidsOfIndividuals = new ArrayList<>();
		uuidsOfIndividuals.add(new Uuid());
		uuidsOfIndividuals.add(new Uuid());
		uuidsOfIndividuals.add(new Uuid());
		
		TripleStatementCollection tripleStatementList = prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_statementTripleList(uuidsOfIndividuals);
		OntologyTripleStoreFacade ontologyTripleStoreFacade = mock(OntologyTripleStoreFacade.class);
		when(ontologyTripleStoreFacade.findTriplesByInformationPackageAndClass(uuidOfInformationPackage, iriOfClass)).thenReturn(tripleStatementList);
		
		ontologyService.setOntologyTripleStoreFacade(ontologyTripleStoreFacade);
		ontologyService.setOntologyModelFactory(new OntologyModelFactory());
	}
	
	private void prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_mockOntologyCache(String className) {
		OntologyCache ontologyCache = mock(OntologyCache.class);
		OntologyIndividualCollection ontologyIndividualList = prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_individualList(className);
		DatatypePropertyCollection  datatypePropertyList = new DatatypePropertyCollectionImpl();
		
		for (char c = 'a'; c <= 'c'; c++) {
			String propertyIri = RdfElementFactory.getPackagingToolkitDefaultNamespace() + PackagingToolkitConstants.NAMESPACE_SEPARATOR + c;
					
			OntologyDatatypeProperty datatypeProperty = new OntologyStringPropertyImpl(propertyIri, "", "");
			datatypePropertyList.addDatatypeProperty(datatypeProperty);
		}

		Namespace namespace = ServerModelFactory.createNamespace(RdfElementFactory.getPackagingToolkitDefaultNamespace().toString());
		LocalName localName = ServerModelFactory.createLocalName(className);
		OntologyClass ontologyClass = new OntologyClassImpl(datatypePropertyList, null, namespace, localName);
		
		when(ontologyCache.getIndividualsByOntologyClass(className)).thenReturn(ontologyIndividualList);
		when(ontologyCache.getClassByIri(className)).thenReturn(ontologyClass);
		
		ontologyService.setOntologyCache(ontologyCache);
	}
	
	/**
	 * Creates a list with three individuals. The individuals do not contain any properties. They are not necessary for the test. This list
	 * contains the individuals that are predefined in the base ontology.
	 * @return A list with the individuals for the test.
	 */
	private OntologyIndividualCollection prepareTest_testGetIndiviualsByOntologyClassAndInformationPackage_individualList(String classname) {
		OntologyIndividualCollection ontologyIndividualList = new OntologyIndividualCollectionImpl();
		Namespace namespace = ServerModelFactory.createNamespace(RdfElementFactory.getPackagingToolkitDefaultNamespace().toString());
		LocalName localName = ServerModelFactory.createLocalName("aClass");
		
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		Uuid uuidOfInformationPackage = new Uuid();
		
		ontologyIndividualList.addIndividual(new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl()));
		ontologyIndividualList.addIndividual(new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl()));
		ontologyIndividualList.addIndividual(new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				new DatatypePropertyCollectionImpl(), new ObjectPropertyCollectionImpl()));
		
		return ontologyIndividualList;
	}

}
