package de.feu.kdmp4.packagingtoolkit.server.factories;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class AbstractFactory  implements ApplicationContextAware{
	private ApplicationContext applicationContext;
	
	protected void initBean(Object myBean) {
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(myBean);
		beanFactory.configureBean(myBean, myBean.getClass().getSimpleName());
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
