package de.feu.kdmp4.packagingtoolkit.testcategories;

/**
 * A category for the tests that do not use Spring.
 * @author Christopher Olbertz
 *
 */
public interface NonSpringTests {

}
