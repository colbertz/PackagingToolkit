package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.shared.JenaException;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;

public class TaxonomyOperationsTest {
	private TaxonomyIndividual  crossComputingTools;
	private TaxonomyIndividual  discreteMathematics;
	private TaxonomyIndividual documentTypes;
	private TaxonomyIndividual generalReferences;
	private TaxonomyIndividual mathematicsOfComputing;
	private TaxonomyIndividual probalisticRepresentations;
	private TaxonomyIndividual hardware;
	private TaxonomyIndividual software;
	private TaxonomyIndividual probabilityStatistics;
	private Taxonomy taxonomyAnother;
	private TaxonomyIndividual taxonomyAnotherRoot;
	private TaxonomyIndividual castle;
	private TaxonomyIndividual church;
	private TaxonomyIndividual cathedral;
	private TaxonomyIndividual moatedCastle;
	
	@Before
	public void setUp() {
		final Namespace namespace1 = new NamespaceImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml");
		final Namespace namespace2 = new NamespaceImpl("http://totem.semedica.com/taxonomy/AnotherTaxonomy");
		taxonomyAnotherRoot = new TaxonomyIndividualImpl(new IriImpl("http://totem.semedica.com/taxonomy/AnotherTaxonomy"), "Yet another taxonomie", namespace2);
		taxonomyAnother = new TaxonomyImpl(taxonomyAnotherRoot, "Yet another taxonomie", new NamespaceImpl("http://totem.semedica.com/taxonomy/AnotherTaxonomie"));
		church = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#333333"), "Church", namespace2); 
		castle = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#444444"), "Castle", namespace2);
		cathedral = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#55555555"), "Cathedral", namespace2);
		moatedCastle = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#66666666"), "Moated Castle", namespace2);
		
		taxonomyAnother.addTopConceptToTaxonomy(church);
		taxonomyAnother.addTopConceptToTaxonomy(castle);
		church.addNarrower(cathedral);
		castle.addNarrower(moatedCastle);
		 
		crossComputingTools = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10011123"), "Cross-computing tools and techniques", namespace1);
		discreteMathematics = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10003624"), "Discrete mathematics", namespace1);
		documentTypes = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10011122"), "Document types", namespace1);
		generalReferences = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10002944"), "General and reference", namespace1);
		mathematicsOfComputing = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10002950"), "Mathematics of computing", namespace1);
		probabilityStatistics = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10003648"), "Probability and statistics", namespace1);
		probalisticRepresentations = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10003649"), "Probabilistic representations", namespace1);
		hardware = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#1111111"), "Hardware", namespace1);
		software = new TaxonomyIndividualImpl(new IriImpl("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#2222222"), "Software", namespace1);		
	}
	
	@Test
	public void testAddIndividualToTaxonomy_conceptScheme() {
		final TaxonomyOperations taxonomyOperations = new TaxonomyOperationsJenaImpl();
		
		final OntModel ontModel = readSkosOntology();
		ontModel.add(readSkosTestOntology());
		final ExtendedIterator<Individual> individualIterator = ontModel.listIndividuals();
		
		while(individualIterator.hasNext()) {
			 try {
				 final Individual individual = individualIterator.next();
				 taxonomyOperations.addIndividualToTaxonomy(individual);
			 } catch (Exception ex)  {
				 ex.printStackTrace();
			 }
		}
		
		final Optional<Taxonomy> optionalWithTaxonomy2 = taxonomyOperations.getTaxonomy(0);
		final Taxonomy taxonomy2 = optionalWithTaxonomy2.get();
		final int taxonomy2IndividualsCount = taxonomy2.countTaxonomyIndividuals();
		assertThat(taxonomy2IndividualsCount, is(equalTo(4)));
		
		final TaxonomyIndividual taxonomy2Root = taxonomy2.getRootIndividual();
		assertTrue(taxonomy2Root.containsNarrower(castle));
		assertTrue(taxonomy2Root.containsNarrower(church));
		
		assertTrue(taxonomy2Root.containsNarrower(castle));
		assertTrue(taxonomy2Root.containsNarrower(church));
		
		final Optional<TaxonomyIndividual> optionalWithActualCastle = taxonomy2Root.findNarrower(castle.getIri());
		final TaxonomyIndividual actualCastle = optionalWithActualCastle.get();		
		assertThat(actualCastle, is(equalTo(castle)));
		assertThat(actualCastle.getNarrowerCount(), is(equalTo(1)));
		
		final Optional<TaxonomyIndividual> optionalWithActualChurch = taxonomy2Root.findNarrower(church.getIri());
		final TaxonomyIndividual actualChurch = optionalWithActualChurch.get();		
		assertThat(actualChurch, is(equalTo(church)));
		assertThat(actualChurch.getNarrowerCount(), is(equalTo(1)));
		
		final Optional<TaxonomyIndividual> optionalWithActualCathedral = church.findNarrower(cathedral.getIri());
		final TaxonomyIndividual actualCathedral = optionalWithActualCathedral.get();		
		assertThat(actualCathedral, is(equalTo(cathedral)));
		assertThat(actualCathedral.getNarrowerCount(), is(equalTo(0)));
		
		final Optional<TaxonomyIndividual> optionalWithActualMoatedCastle = castle.findNarrower(moatedCastle.getIri());
		final TaxonomyIndividual actualMoatedCastle = optionalWithActualMoatedCastle.get();		
		assertThat(actualMoatedCastle, is(equalTo(moatedCastle)));
		assertThat(actualMoatedCastle.getNarrowerCount(), is(equalTo(0)));		
		
		final int taxonomyCount = taxonomyOperations.getTaxonomyCount();
		assertThat(taxonomyCount, is(equalTo(2)));
		final Optional<Taxonomy> optionalWithTaxonomy1 = taxonomyOperations.getTaxonomy(1);
		final Taxonomy taxonomy1 = optionalWithTaxonomy1.get();
		final int taxonomy1IndividualsCount = taxonomy1.countTaxonomyIndividuals();
		assertThat(taxonomy1IndividualsCount, is(equalTo(9)));
		final TaxonomyIndividual taxonomy1Root = taxonomy1.getRootIndividual();
		assertTrue(taxonomy1Root.containsNarrower(generalReferences));
		assertTrue(taxonomy1Root.containsNarrower(mathematicsOfComputing));
		
		final Optional<TaxonomyIndividual> optionalWithGeneralReferences = taxonomy1Root.findNarrower(generalReferences.getIri());
		final TaxonomyIndividual actualGeneralReferences = optionalWithGeneralReferences.get();
		final Optional<TaxonomyIndividual> optionalWithActualDocumentTypes = actualGeneralReferences.findNarrower(documentTypes.getIri());
		assertTrue(optionalWithActualDocumentTypes.isPresent());
		final TaxonomyIndividual actualDocumentTypes = optionalWithActualDocumentTypes.get();
		assertThat(actualDocumentTypes, is(equalTo(documentTypes)));
		
		final Optional<TaxonomyIndividual> optionalWithActualMathematicsOfComputing = taxonomy1Root.findNarrower(mathematicsOfComputing.getIri());
		assertTrue(optionalWithActualMathematicsOfComputing.isPresent());
		final TaxonomyIndividual actualMathematicsOfComputing = optionalWithActualMathematicsOfComputing.get();
		assertThat(actualMathematicsOfComputing, is(equalTo(mathematicsOfComputing)));
		
		final Optional<TaxonomyIndividual> optionalWithActualCrossComputing = actualGeneralReferences.findNarrower(crossComputingTools.getIri());
		final TaxonomyIndividual actualCrossComputing = optionalWithActualCrossComputing.get();		
		assertThat(actualCrossComputing, is(equalTo(crossComputingTools)));
		
		final Optional<TaxonomyIndividual> optionalWithActualDiscreteMathematics = actualMathematicsOfComputing.findNarrower(discreteMathematics.getIri());
		final TaxonomyIndividual actualDiscreteMathematics = optionalWithActualDiscreteMathematics.get();		
		assertThat(actualDiscreteMathematics, is(equalTo(discreteMathematics)));
		assertThat(actualDiscreteMathematics.getNarrowerCount(), is(equalTo(2)));
		
		final Optional<TaxonomyIndividual> optionalWithActualSoftware = actualDiscreteMathematics.findNarrower(software.getIri());
		final TaxonomyIndividual actualSoftware = optionalWithActualSoftware.get();		
		assertThat(actualSoftware, is(equalTo(software)));
		assertThat(actualSoftware.getNarrowerCount(), is(equalTo(0)));

		final Optional<TaxonomyIndividual> optionalWithActualHardware = actualDiscreteMathematics.findNarrower(hardware.getIri());
		final TaxonomyIndividual actualHardware = optionalWithActualHardware.get();		
		assertThat(actualHardware, is(equalTo(hardware)));
		assertThat(actualHardware.getNarrowerCount(), is(equalTo(0)));
		
		final Optional<TaxonomyIndividual> optionalWithActualProbabilityStatistics = actualMathematicsOfComputing.findNarrower(probabilityStatistics.getIri());
		final TaxonomyIndividual actualProbabilityStatistics = optionalWithActualProbabilityStatistics.get();		
		assertThat(actualProbabilityStatistics, is(equalTo(probabilityStatistics)));
		assertThat(actualProbabilityStatistics.getNarrowerCount(), is(equalTo(1)));
		
		final Optional<TaxonomyIndividual> optionalWithActualProbalisticRepresentations = actualProbabilityStatistics.findNarrower(probalisticRepresentations.getIri());
		final TaxonomyIndividual actualProbalisticRepresentations = optionalWithActualProbalisticRepresentations.get();		
		assertThat(actualProbalisticRepresentations, is(equalTo(probalisticRepresentations)));
		assertThat(actualProbalisticRepresentations.getNarrowerCount(), is(equalTo(0)));
		

	}
	
	private OntModel readSkosTestOntology() {
	    OntModel model = ModelFactory.createOntologyModel();
	    File fileWithOntology = new File("/home/christopher/packagingtoolkit/PackagingToolkitServer/PackagingToolkitServerApplication/work/config/skosOntology.owl");

		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		final String ontologyPath = fileWithOntology.getAbsolutePath();
		try (InputStream inputStream = FileManager.get().open(ontologyPath)){
			model.read(inputStream, null);
			return model;
		} catch (JenaException jex) {
			jex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	private OntModel readSkosOntology() {
	    OntModel model = ModelFactory.createOntologyModel();
	    File fileWithOntology = new File("/home/christopher/packagingtoolkit/PackagingToolkitServer/PackagingToolkitServerOntology/src/test/resources/skosTest.owl");

		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		final String ontologyPath = fileWithOntology.getAbsolutePath();
		try (InputStream inputStream = FileManager.get().open(ontologyPath)){
			model.read(inputStream, null);
			return model;
		} catch (JenaException jex) {
			jex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
