package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class OntologyTimePropertyTest {
	private static final String PROPERTY_ID = "datatypeProperty#timeProperty";
	private static final String PROPERTY_LABEL = "timeProperty";
	private static final String PROPERTY_RANGE = "time";
	
	private OntologyTimeProperty ontologyTimeProperty;
	
	@Before
	public void setUp() {
		ontologyTimeProperty = new OntologyTimePropertyImpl(PROPERTY_ID, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsTime() {
		assertFalse(ontologyTimeProperty.isBooleanProperty());
		assertFalse(ontologyTimeProperty.isByteProperty());
		assertFalse(ontologyTimeProperty.isDateProperty());
		assertFalse(ontologyTimeProperty.isDateTimeProperty());
		assertFalse(ontologyTimeProperty.isDoubleProperty());
		assertFalse(ontologyTimeProperty.isDurationProperty());
		assertFalse(ontologyTimeProperty.isFloatProperty());
		assertFalse(ontologyTimeProperty.isIntegerProperty());
		assertFalse(ontologyTimeProperty.isLongProperty());
		assertFalse(ontologyTimeProperty.isShortProperty());
		assertFalse(ontologyTimeProperty.isStringProperty());
		assertTrue(ontologyTimeProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		LocalTime expectedTime = LocalTime.now();
		ontologyTimeProperty.setPropertyValue(expectedTime);
		LocalTime actualTime = ontologyTimeProperty.getPropertyValue();
		assertEquals(expectedTime, actualTime);
	}
}
