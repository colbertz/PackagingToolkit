package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ OntologyBooleanPropertyTest.class, OntologyBytePropertyTest.class, OntologyDatePropertyTest.class,
		OntologyDateTimePropertyTest.class, OntologyDurationPropertyTest.class, OntologyFloatPropertyTest.class,
		OntologyIntegerPropertyTest.class, OntologyLongPropertyTest.class, OntologyObjectPropertyTest.class,
		OntologyShortPropertyTest.class, OntologyStringPropertyTest.class, OntologyTimePropertyTest.class })
public class AllPropertyTests {

}
