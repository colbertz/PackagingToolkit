package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class MinInclusiveTest {
	private RestrictionValue<Integer> restrictionValue;
	private Restriction restriction;
	
	@Before
	public void setUp() throws Exception {
		restrictionValue = new RestrictionValue<Integer>(5);
		restriction = new MinInclusiveImpl(restrictionValue);
	}

	@Test(expected = RestrictionNotSatisfiedException.class)
	public void testCheckRestriction1() {
		RestrictionValue<Integer> compareValue = new RestrictionValue<>(5);
		restriction.checkRestriction(compareValue);
	}	
	
	@Test
	public void testCheckRestriction2() {
		RestrictionValue<Integer> compareValue = new RestrictionValue<>(5);
		restriction.checkRestriction(compareValue);
	}
	
	@Test
	public void testCheckRestriction3() {
		RestrictionValue<Integer> compareValue = new RestrictionValue<>(30);
		restriction.checkRestriction(compareValue);
	}	
}
