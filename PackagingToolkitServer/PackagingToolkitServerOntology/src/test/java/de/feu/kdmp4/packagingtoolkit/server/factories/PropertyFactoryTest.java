package de.feu.kdmp4.packagingtoolkit.server.factories;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.feu.kdmp4.packagingtoolkit.server.configuration.FactoryConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.MockedOperationsConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
@Category(SpringTests.class)
public class PropertyFactoryTest {
	private static final int UNSIGNED_BYTE_MAX = 255;
	private static final long UNSIGNED_INT_MAX = 4294967295L;
	private static final BigInteger UNSIGNED_LONG_MAX = new BigInteger("18446744073709551615");
	private static final int UNSIGNED_SHORT_MAX = 65535;
	@Autowired
	private PropertyFactory propertyFactory;
	
	@Before
	public void setUp() throws Exception {
	}  

	@Test
	public void testCreateByteProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#byte";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
		assertTrue(ontologyProperty instanceof OntologyByteProperty);
				
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertTrue(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateBooleanProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#boolean";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);

		assertTrue(ontologyProperty instanceof OntologyBooleanProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertTrue(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateDateProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#date";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyDateProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertTrue(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateDateTimeProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#dateTime";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyDateTimeProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertTrue(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateDoubleProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#double";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyDoubleProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertTrue(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateDurationProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#duration";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyDurationProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertTrue(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateFloatProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#float";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyFloatProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertTrue(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateIntegerProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#integer";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyIntegerProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertTrue(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateLongProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#long";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyLongProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertTrue(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateNegativeIntegerProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#negativeInteger";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyIntegerProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertTrue(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		RestrictionValue<Long> restrictionValue1 = new RestrictionValue<Long>(3l);
		RestrictionValue<Long> restrictionValue2 = new RestrictionValue<Long>(-3l);
		RestrictionValue<Long> restrictionValue3 = new RestrictionValue<Long>(0l);
		long minValue = Integer.MIN_VALUE;
		RestrictionValue<Long> restrictionValue4 = new RestrictionValue<Long>(minValue);
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
	}
	
	@Test
	public void testCreateNonPositiveIntegerProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#nonPositiveInteger";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyIntegerProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertTrue(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		RestrictionValue<Long> restrictionValue1 = new RestrictionValue<Long>(300l);
		RestrictionValue<Long> restrictionValue2 = new RestrictionValue<Long>(-300l);
		RestrictionValue<Long> restrictionValue3 = new RestrictionValue<Long>(0l);
		long minValue = Integer.MIN_VALUE;
		RestrictionValue<Long> restrictionValue4 = new RestrictionValue<Long>(minValue);
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
	}
	
	@Test
	public void testCreateNonNegativeIntegerProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#nonNegativeInteger";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyIntegerProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertTrue(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		RestrictionValue<Long> restrictionValue1 = new RestrictionValue<Long>(3l);
		RestrictionValue<Long> restrictionValue2 = new RestrictionValue<Long>(-3l);
		RestrictionValue<Long> restrictionValue3 = new RestrictionValue<Long>(0l);
		long maxValue = Integer.MAX_VALUE;
		RestrictionValue<Long> restrictionValue4 = new RestrictionValue<Long>(maxValue);
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
	}
	
	@Test
	public void testCreatePositiveIntegerProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#positiveInteger";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyIntegerProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertTrue(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		RestrictionValue<Long> restrictionValue1 = new RestrictionValue<Long>(300l);
		RestrictionValue<Long> restrictionValue2 = new RestrictionValue<Long>(-300l);
		RestrictionValue<Long> restrictionValue3 = new RestrictionValue<Long>(0l);
		long maxValue = Integer.MAX_VALUE;
		RestrictionValue<Long> restrictionValue4 = new RestrictionValue<Long>(maxValue);
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
	}
	
	@Test
	public void testCreateShortProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#short";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyShortProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertTrue(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateStringProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#string";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyStringProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertTrue(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateTimeProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#time";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyTimeProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertTrue(ontologyProperty.isTimeProperty());
	}
	
	@Test
	public void testCreateUnsignedIntegerProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#unsignedInt";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyIntegerProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertTrue(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		RestrictionValue<Long> restrictionValue1 = new RestrictionValue<Long>(3l);
		RestrictionValue<Long> restrictionValue2 = new RestrictionValue<Long>(-3l);
		RestrictionValue<Long> restrictionValue3 = new RestrictionValue<Long>(0l);
		long maxValue = UNSIGNED_INT_MAX;
		long tooHighValue = UNSIGNED_INT_MAX + 1;
		RestrictionValue<Long> restrictionValue4 = new RestrictionValue<Long>(maxValue);
		RestrictionValue<Long> restrictionValue5 = new RestrictionValue<Long>(tooHighValue);
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue5));
	}
	
	@Test
	public void testCreateUnsignedLongProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#unsignedLong";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyLongProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertTrue(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		BigInteger value1 = new BigInteger("3");
		BigInteger value2 = new BigInteger("-300");
		BigInteger value3 = new BigInteger("0");
		RestrictionValue<BigInteger> restrictionValue1 = new RestrictionValue<BigInteger>(value1);
		RestrictionValue<BigInteger> restrictionValue2 = new RestrictionValue<BigInteger>(value2);
		RestrictionValue<BigInteger> restrictionValue3 = new RestrictionValue<BigInteger>(value3);
		RestrictionValue<BigInteger> restrictionValue4 = new RestrictionValue<BigInteger>(UNSIGNED_LONG_MAX);
		BigInteger value5 = UNSIGNED_LONG_MAX.add(new BigInteger("1"));
		RestrictionValue<BigInteger> restrictionValue5 = new RestrictionValue<BigInteger>(value5);
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue5));
	}
	
	@Test
	public void testCreateUnsignedShortProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#unsignedShort";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyShortProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertFalse(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertTrue(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		RestrictionValue<Integer> restrictionValue1 = new RestrictionValue<Integer>(3);
		RestrictionValue<Integer> restrictionValue2 = new RestrictionValue<Integer>(-3);
		RestrictionValue<Integer> restrictionValue3 = new RestrictionValue<Integer>(0);
		int maxValue = UNSIGNED_SHORT_MAX;
		int tooHighValue = UNSIGNED_SHORT_MAX + 1;
		RestrictionValue<Integer> restrictionValue4 = new RestrictionValue<Integer>(maxValue);
		RestrictionValue<Integer> restrictionValue5 = new RestrictionValue<Integer>(tooHighValue);
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue5));
	}
	
	@Test
	public void testCreateUnsignedByteProperty() {
		String propertyRange = "http://www.w3.org/2001/XMLSchema#unsignedByte";
		String propertyId = "abc";
		String label = "xyz";
		OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(propertyRange, propertyId, label);
				
		assertTrue(ontologyProperty instanceof OntologyByteProperty);
		assertThat(ontologyProperty.getPropertyId(), is(equalTo(propertyId)));
		assertThat(ontologyProperty.getLabel(), is(equalTo(label)));
		assertTrue(ontologyProperty.isByteProperty());
		assertFalse(ontologyProperty.isBooleanProperty());
		assertFalse(ontologyProperty.isDateProperty());
		assertFalse(ontologyProperty.isDateTimeProperty());
		assertFalse(ontologyProperty.isDoubleProperty());
		assertFalse(ontologyProperty.isDurationProperty());
		assertFalse(ontologyProperty.isFloatProperty());
		assertFalse(ontologyProperty.isIntegerProperty());
		assertFalse(ontologyProperty.isLongProperty());
		assertFalse(ontologyProperty.isObjectProperty());
		assertFalse(ontologyProperty.isShortProperty());
		assertFalse(ontologyProperty.isStringProperty());
		assertFalse(ontologyProperty.isTimeProperty());
		
		RestrictionValue<Integer> restrictionValue1 = new RestrictionValue<Integer>(3);
		RestrictionValue<Integer> restrictionValue2 = new RestrictionValue<Integer>(-3);
		RestrictionValue<Integer> restrictionValue3 = new RestrictionValue<Integer>(0);
		int maxValue = UNSIGNED_BYTE_MAX;
		int tooHighValue = UNSIGNED_BYTE_MAX + 1;
		RestrictionValue<Integer> restrictionValue4 = new RestrictionValue<Integer>(maxValue);
		RestrictionValue<Integer> restrictionValue5 = new RestrictionValue<Integer>(tooHighValue);
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue1));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue2));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue3));
		assertTrue(ontologyProperty.areRestrictionsSatisfied(restrictionValue4));
		assertFalse(ontologyProperty.areRestrictionsSatisfied(restrictionValue5));
	}
}
