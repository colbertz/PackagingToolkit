package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;

@Category(NonSpringTests.class)
public class OntologyObjectPropertyTest {
	private static final String PROPERTY_ID = "ontologyProperty#objectProperty";
	private static final String PROPERTY_LABEL = "objectProperty";
	private static final String PROPERTY_RANGE = "object";
	
	private OntologyObjectProperty ontologyObjectProperty;
	private OntologyIndividualCollection expectedIndividuals;
	
	@Before
	public void setUp() {
		OntologyClass ontologyClass = prepareOntologyClass();
		ontologyObjectProperty = prepareObjectProperty(ontologyClass);
		expectedIndividuals = prepareSomeIndividuals(ontologyClass, 2);
		ontologyObjectProperty.setPropertyValue(expectedIndividuals);
	}
	
	@Test
	public void testGetValue() {
		OntologyIndividualCollection actualIndividuals = ontologyObjectProperty.getPropertyValue();
		assertEquals(expectedIndividuals, actualIndividuals);
	}
	
	@Test
	public void testAddIndividual() {
		OntologyClass ontologyClass = prepareOntologyClass();
		OntologyObjectProperty objectProperty = prepareObjectProperty(ontologyClass);
		int individualCount = 3;
		OntologyIndividualCollection individualList = prepareSomeIndividuals(ontologyClass, individualCount);
		
		for (int i = 0; i < individualCount; i++) {
			OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
			objectProperty.addOntologyIndividual(ontologyIndividual);
		}
		
		OntologyIndividualCollection actualIndividuals = objectProperty.getPropertyValue();
		assertEquals(individualList, actualIndividuals);
	}
	
	/**
	 * Creates an object property of a certain class.
	 * @param ontologyClass The class the created object property has as range.
	 * @return The new object property.
	 */
	private OntologyObjectProperty prepareObjectProperty(OntologyClass ontologyClass) {
		OntologyObjectProperty ontologyObjectProperty = new OntologyObjectPropertyImpl(PROPERTY_ID, PROPERTY_LABEL, 
				ontologyClass, PROPERTY_RANGE, ontologyClass);
		return ontologyObjectProperty;
	}
	
	/**
	 * Prepares an ontology class for testing with the namespace abc and the local name def.
	 * @return The new ontology class.
	 */
	private OntologyClass prepareOntologyClass() {
		return new OntologyClassImpl(new NamespaceImpl("abc"), new LocalNameImpl("def"));
	}

	@Test
	public void testSetPropertyValue() {
		OntologyClass ontologyClass = prepareOntologyClass();
		int expectedIndividualCount = 2; 
		OntologyIndividualCollection expectedIndividuals = prepareSomeIndividuals(ontologyClass, expectedIndividualCount);
		
		ontologyObjectProperty.setPropertyValue(expectedIndividuals);
		
		OntologyIndividualCollection actualIndividuals = ontologyObjectProperty.getPropertyValue();
		assertEquals(expectedIndividuals, actualIndividuals);
	}
	
	/**
	 * Creates a list with some individuals.
	 * @param individualsOntologyClass The class the individuals in the list belong to.
	 * @param individualCount The number of individual that should be created and put into the list.
	 * @return The list with individuals.
	 */
	private OntologyIndividualCollection prepareSomeIndividuals(OntologyClass individualsOntologyClass,
					int individualCount) {
		Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		OntologyIndividualCollection individuals = new OntologyIndividualCollectionImpl();
		
		for (int i = 0; i < individualCount; i++) {
			OntologyIndividual individual = new OntologyIndividualImpl(individualsOntologyClass, uuidOfInformationPackage, new DatatypePropertyCollectionImpl(),
					new ObjectPropertyCollectionImpl());
			individuals.addIndividual(individual);
		}
		return individuals;
	}
}
