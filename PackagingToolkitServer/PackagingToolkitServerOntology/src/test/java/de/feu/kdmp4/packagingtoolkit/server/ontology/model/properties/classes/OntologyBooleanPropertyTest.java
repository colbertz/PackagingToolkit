package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.testcategories.NonSpringTests;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@Category(NonSpringTests.class)
public class OntologyBooleanPropertyTest {
	private static final String PROPERTY_ID = "datatypeProperty#booleanProperty";
	private static final String PROPERTY_LABEL = "booleanProperty";
	private static final String PROPERTY_RANGE = "boolean";
	
	private OntologyBooleanProperty ontologyBooleanProperty;
	
	@Before
	public void setUp() {
		ontologyBooleanProperty = new OntologyBooleanPropertyImpl(PROPERTY_ID, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsBoolean() {
		assertTrue(ontologyBooleanProperty.isBooleanProperty());
		assertFalse(ontologyBooleanProperty.isByteProperty());
		assertFalse(ontologyBooleanProperty.isDateProperty());
		assertFalse(ontologyBooleanProperty.isDateTimeProperty());
		assertFalse(ontologyBooleanProperty.isDoubleProperty());
		assertFalse(ontologyBooleanProperty.isDurationProperty());
		assertFalse(ontologyBooleanProperty.isFloatProperty());
		assertFalse(ontologyBooleanProperty.isIntegerProperty());
		assertFalse(ontologyBooleanProperty.isLongProperty());
		assertFalse(ontologyBooleanProperty.isShortProperty());
		assertFalse(ontologyBooleanProperty.isStringProperty());
		assertFalse(ontologyBooleanProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		ontologyBooleanProperty.setPropertyValue(true);
		boolean value = ontologyBooleanProperty.getPropertyValue();
		assertTrue(value);
	}

}
