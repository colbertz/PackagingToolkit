package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.configuration.FactoryConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.configuration.MockedOperationsConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.testcategories.SpringTests;

@ActiveProfiles("propertiesTest")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FactoryConfiguration.class, OntologyConfiguration.class, MockedOperationsConfiguration.class})
@Category(SpringTests.class)
public class OntologyBytePropertyTest implements ApplicationContextAware{
	private ApplicationContext applicationContext;
	private static final String PROPERTY_ID = "datatypeProperty#byteProperty";
	private static final String PROPERTY_LABEL = "byteProperty";
	private static final String PROPERTY_RANGE = "byte";
	
	private OntologyByteProperty ontologyByteProperty;
	
	private ExpectedException thrownException = ExpectedException.none();
	
	@Before
	public void setUp() {
		ontologyByteProperty = new OntologyBytePropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
	}
	
	@Test
	public void testIsByte() {
		assertFalse(ontologyByteProperty.isBooleanProperty());
		assertTrue(ontologyByteProperty.isByteProperty());
		assertFalse(ontologyByteProperty.isDateProperty());
		assertFalse(ontologyByteProperty.isDateTimeProperty());
		assertFalse(ontologyByteProperty.isDoubleProperty());
		assertFalse(ontologyByteProperty.isDurationProperty());
		assertFalse(ontologyByteProperty.isFloatProperty());
		assertFalse(ontologyByteProperty.isShortProperty());
		assertFalse(ontologyByteProperty.isLongProperty());
		assertFalse(ontologyByteProperty.isShortProperty());
		assertFalse(ontologyByteProperty.isStringProperty());
		assertFalse(ontologyByteProperty.isTimeProperty());
	}
	
	@Test
	public void testGetValue() {
		ontologyByteProperty.setPropertyValue(5);
		int value = ontologyByteProperty.getPropertyValue();
		assertEquals(5, value);
	}

	@Test
	public void testUnsignedByteValue5() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Short> value = new RestrictionValue<Short>((short)5);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedByteValue254() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Short> value = new RestrictionValue<Short>((short)(Byte.MAX_VALUE * 2));
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testUnsignedByteValue255() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Short> value = new RestrictionValue<Short>((short)(Byte.MAX_VALUE * 2 + 1));
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testUnsignedByteValue0() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Short> value = new RestrictionValue<Short>((short) 0);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testUnsignedByteValueNegative() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Short> value = new RestrictionValue<Short>((short) -1);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testUnsignedByteValue256() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, true, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		RestrictionValue<Short> value = new RestrictionValue<Short>((short)(Byte.MAX_VALUE * 2 + 2));
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}

	@Test
	public void testSignedByteValue127() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Short valueAsShort = new Short(Byte.MAX_VALUE);
		RestrictionValue<Short> value = new RestrictionValue<Short>(valueAsShort);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}

	@Test
	public void testSignedByteValue128() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Short valueAsShort = new Short((short)(Byte.MAX_VALUE + 1) );
		RestrictionValue<Short> value = new RestrictionValue<Short>(valueAsShort);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Test
	public void testSignedByteValue0() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Short valueAsShort = new Short((short) 0);
		RestrictionValue<Short> value = new RestrictionValue<Short>(valueAsShort);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testSignedByteValueNegative() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Short valueAsShort = new Short(Byte.MIN_VALUE);
		RestrictionValue<Short> value = new RestrictionValue<Short>(valueAsShort);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertTrue(satisfied);
	}
	
	@Test
	public void testSignedByteValueBelowMinValue() {
		OntologyByteProperty unsigned = new OntologyBytePropertyImpl(PROPERTY_ID, false, PROPERTY_LABEL, PROPERTY_RANGE);
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(unsigned);
		String beanName = unsigned.getClass().getSimpleName();
		beanFactory.configureBean(unsigned, beanName);
		
		Short valueAsShort = new Short((short)(Byte.MIN_VALUE - 1));
		RestrictionValue<Short> value = new RestrictionValue<Short>(valueAsShort);
		boolean satisfied = unsigned.areRestrictionsSatisfied(value);
		assertFalse(satisfied);
	}
	
	@Rule
	public ExpectedException getThrownException() {
		return thrownException;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
