package de.feu.kdmp4.packagingtoolkit.server.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.DataStructureFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.RestrictionFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;

@Configuration
@Profile({"propertiesTest", "ontologyOperationsTest"})
public class FactoryConfiguration {
	@Bean(name = "dataStructureFactory")
	public DataStructureFactory getDataStructureFactory() {
		return new DataStructureFactory();
	}
	
	@Bean(name = "ontologyModelFactory")
	public OntologyModelFactory getOntologyModelFactory() {
		OntologyModelFactory ontologyModelFactory = new OntologyModelFactory();
		return ontologyModelFactory;
	}
	
	@Bean(name = "propertyFactory")
	public PropertyFactory getPropertyFactory() {
		return new PropertyFactory();
	}
	
	@Bean(name = "restrictionFactory")
	public RestrictionFactory getRestrictionFactory() {
		return new RestrictionFactory();
	}
	
	@Bean(name = "serverResponseFactory")
	public ServerResponseFactory getServerResponseFactory() {
		return new ServerResponseFactory();
	}
}
