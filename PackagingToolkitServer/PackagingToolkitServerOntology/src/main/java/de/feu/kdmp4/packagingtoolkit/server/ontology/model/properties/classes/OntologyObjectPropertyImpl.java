package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;

public class OntologyObjectPropertyImpl extends OntologyPropertyImpl
										implements OntologyObjectProperty {

	private static final long serialVersionUID = 5950765286646166012L;
	/**
	 * The range of the property.
	 */
	private OntologyClass range;
	/**
	 * Contains the iris of the individuals that are values of this
	 * property.
	 */
	private IriCollection irisOfValues;
	
	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param label The label that represents the property.
	 * @param range The range as specified in the ontology.
	 * @param propertyDomain The domain as specified in the ontology.
	 * @param propertyRange The range of the property as class object.  
	 */
	public OntologyObjectPropertyImpl(final String propertyId, final String label, final OntologyClass range, 
			final String propertyRange, final OntologyClass propertyDomain) {
		super(propertyId, null, label, propertyRange);
		this.range = range;
		if (propertyDomain != null) {
			addPropertyDomain(propertyDomain.getFullName());
		}
		
		setPropertyValue(OntologyModelFactory.createEmptyOntologyIndividualList());
		irisOfValues = ServerModelFactory.createEmptyIriCollection();
	}

	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param label The label that represents the property.
	 * @param propertyRange The range as specified in the ontology.
	 * @param propertyDomain The domain as specified in the ontology.
	 */
	public OntologyObjectPropertyImpl(final String propertyId, final String label, final String propertyRange, 
			final String propertyDomain) {
		super(propertyId, null, label, propertyRange);
		addPropertyDomain(propertyDomain);
		setPropertyValue(OntologyModelFactory.createEmptyOntologyIndividualList());
		irisOfValues = ServerModelFactory.createEmptyIriCollection();
	}
	
	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param label The label that represents the property.
	 * @param propertyRange The range as specified in the ontology.
	 */
	public OntologyObjectPropertyImpl(final String propertyId, final String label, final String propertyRange) {
		super(propertyId, null, label, propertyRange);
		setPropertyValue(OntologyModelFactory.createEmptyOntologyIndividualList());
		irisOfValues = ServerModelFactory.createEmptyIriCollection();
	}
	
	@Override
	public void addOntologyIndividual(final OntologyIndividual ontologyIndividual) {
		OntologyIndividualCollection ontologyIndividualList = (OntologyIndividualCollection)super.getValue();
		ontologyIndividualList.addIndividual(ontologyIndividual);
	}
	
	@Override
	public void setPropertyValue(final Object ontologyIndividuals) {
		if (ontologyIndividuals instanceof OntologyIndividualCollection) {
			super.setValue(ontologyIndividuals);
			final OntologyIndividualCollection ontologyIndividualList = (OntologyIndividualCollection)ontologyIndividuals;
			for (int i = 0; i < ontologyIndividualList.getIndiviualsCount(); i++) {
				final OntologyIndividual ontologyIndividual = ontologyIndividualList.getIndividual(i);
				if (ontologyIndividual.getIri() != null) {
					irisOfValues.addIri(ontologyIndividual.getIri());
				}
			}
		}
	}
	
	@Override
	public void setIrisOfValues(final IriCollection iris) {
		this.irisOfValues = iris;
	}
	
	@Override
	public boolean isObjectProperty() {
		return true;
	}

	@Override
	public OntologyIndividualCollection getPropertyValue() {
		return (OntologyIndividualCollection)getValue();
	}
	
	@Override
	public OntologyClass getRange() {
		return range;
	}
}
