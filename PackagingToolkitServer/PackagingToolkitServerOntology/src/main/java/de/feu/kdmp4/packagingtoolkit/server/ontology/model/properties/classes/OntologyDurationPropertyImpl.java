package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;

public class OntologyDurationPropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDurationProperty {
	private static final long serialVersionUID = 1748033123603000410L;

	/**
	 * Creates a new property.
	 */
	public OntologyDurationPropertyImpl() {
	}
	
	/**
	 * Constructs a new property with a value.
	 * @param propertyId The iri of the property.
	 * @param value The value of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDurationPropertyImpl(final String propertyId, 
			final OntologyDuration value, final String label, final String range) {
		super(propertyId, value, label, range);
	}

	/**
	 * Constructs a new property without a value.
	 * @param propertyId The iri of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDurationPropertyImpl(final String propertyId, final String label, final String propertyRange) {
		super(propertyId, null, label, propertyRange);
	}
	
	@Override
	public OntologyDuration getPropertyValue() {
		return (OntologyDuration) getValue();
	}

	@Override
	public boolean isDurationProperty() {
		return true;
	}
}
