/**
 * Contains the implementation classes of the Ontology module.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;