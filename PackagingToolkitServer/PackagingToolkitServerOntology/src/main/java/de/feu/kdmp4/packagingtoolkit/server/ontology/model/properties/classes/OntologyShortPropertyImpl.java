package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.PropertyRangeException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;

public class OntologyShortPropertyImpl extends OntologyDatatypePropertyImpl implements OntologyShortProperty {
	private static final int DEFAULT_VALUE = 0;
	private static final long serialVersionUID = 106001928244230493L;
	private static final int SIGNED_MAX = Short.MAX_VALUE;
	private static final int SIGNED_MIN = Short.MIN_VALUE;
	private static final int UNSIGNED_MAX = 65535;
	private static final int UNSIGNED_MIN = 0;

	/**
	 * True if the value is unsigned. Unsigned means that there are only positive
	 * values possible and the value ranged is doubled. 
	 */
	private boolean unsigned;

	/**
	 * Creates a new property.
	 */
	public OntologyShortPropertyImpl() {
	}
	
	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param unsigned True if the value may be unsigned, false otherwise.
	 * @param defaultLabel The label that represents the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyShortPropertyImpl(final String propertyId, final boolean unsigned, final String defaultLabel, final String range) {
		super(propertyId, DEFAULT_VALUE, defaultLabel, range);
		this.unsigned = unsigned;
	}

	@Override
	public boolean isUnsigned() {
		return unsigned;
	}

	@Override
	public void setPropertyValue(final int value) {
		super.setValue(value);
	}

	/**
	 * Initializes the restrictions like specified in the constructor. 
	 */
	@PostConstruct
	public void initialize() {
		if (unsigned) {
			addMaxInclusiveRestriction(UNSIGNED_MAX);
			addMinInclusiveRestriction(UNSIGNED_MIN);
		} else {
			addMaxInclusiveRestriction(SIGNED_MAX);
			addMinInclusiveRestriction(SIGNED_MIN);
		}
	}

	@Override
	public void addMaxInclusiveRestriction(final int value) {
		if (!checkShortRange(value)) {
			final PropertyRangeException exception = PropertyRangeException.valueNotInShortRange(value);
			throw exception;
		}
		final RestrictionValue<Integer> restrictionValue = new RestrictionValue<>(value);
		final MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public void addMinInclusiveRestriction(final int value) {
		if (!checkShortRange(value)) {
			final PropertyRangeException exception = PropertyRangeException.valueNotInShortRange(value);
			throw exception;
		}
		final RestrictionValue<Integer> restrictionValue = new RestrictionValue<>(value);
		final MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	private boolean checkShortRange(final int value) {
		if (unsigned) {
			if (value >= UNSIGNED_MIN && value <= UNSIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		} else {
			if (value >= SIGNED_MIN && value <= SIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Override
	public Integer getPropertyValue() {
		return (Integer) getValue();
	}

	@Override
	public boolean isShortProperty() {
		return true;
	}
}
