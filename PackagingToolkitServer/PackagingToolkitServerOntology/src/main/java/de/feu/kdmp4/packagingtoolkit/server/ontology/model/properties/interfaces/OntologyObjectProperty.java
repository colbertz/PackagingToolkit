package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

/**
 * Represents object properties in an ontology. The value of an object property is a list with 
 * individual of a class that is the range of this property. 
 * @author Christopher Olbertz
 *
 */
public interface OntologyObjectProperty extends OntologyProperty {
	/**
	 * Gets the class that is the range of this property. All individuals in the value of this
	 * property are idnviduals of this class.
	 * @return The range of this property.
	 */
	OntologyClass getRange();
	/**
	 * Adds an individual to the value of this property.
	 * @param ontologyIndividual The individual that is added to the list with individuals of this
	 * property.
	 */
	void addOntologyIndividual(final OntologyIndividual ontologyIndividual);
	/**
	 * Returns the value of this object property. The value is a list with the individuals of this 
	 * object property.
	 * @return A list with the individuals of this property.
	 */
	OntologyIndividualCollection getPropertyValue();
	/**
	 * Sets the iris of the individuals that are the values of this object properties. 
	 * This method can be used if there are no OntologyIndividual objects available for
	 * the value of this property.
	 * @param iris The iris that represent the values of this property.
	 */
	void setIrisOfValues(IriCollection iris);
}
