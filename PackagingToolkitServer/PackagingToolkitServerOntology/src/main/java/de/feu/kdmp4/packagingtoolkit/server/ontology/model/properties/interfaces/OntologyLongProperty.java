package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import java.math.BigInteger;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyLongPropertyImpl;

/**
 * An ontology property with a long value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyLongProperty extends OntologyDatatypeProperty {
	/**
	 * Adds a max inclusive restriction.
	 * @param value The value that may not be overstepped.
	 */
	void addMaxInclusiveRestriction(final BigInteger value);
	/**
	 * Adds a min inclusive restriction.
	 * @param value The value that may not be fallen below.
	 */
	void addMinInclusiveRestriction(final BigInteger value);
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public BigInteger getPropertyValue();
	
	/**
	 * Creates a unsigned long property.
	 * @param propertyId The iri of the property.
	 * @param label The label of the property. 
	 * @param range The range as specified in the ontology.
	 * @return The newly created property.
	 */
	public static OntologyLongProperty createUnsigned(final String propertyId, final String label, final String range) {
		return new OntologyLongPropertyImpl(propertyId, true, label, range); 
	}
	
	/**
	 * Creates a signed long property.
	 * @param propertyId The iri of the property.
	 * @param label The label of the property. 
	 * @param range The range as specified in the ontology.
	 * @return The newly created property.
	 */
	public static OntologyLongProperty createSigned(final String propertyId, final String label, final String range) {
		return new OntologyLongPropertyImpl(propertyId, false, label, range); 
	}
	
	/**
	 * Determines if the property is unsigned.
	 * @return True if the property is unsigned, false otherwise. 
	 */
	boolean isUnsigned();
}
