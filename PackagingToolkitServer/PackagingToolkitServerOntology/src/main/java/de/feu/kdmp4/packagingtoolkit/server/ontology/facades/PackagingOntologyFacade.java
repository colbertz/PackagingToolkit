package de.feu.kdmp4.packagingtoolkit.server.ontology.facades;

import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

/**
 * The facade the Packaging Module uses for accessing the methods of the Ontology module.
 * @author Christopher Olbertz
 *
 */
public class PackagingOntologyFacade {
	/**
	 * Contains a reference to the operations of the Ontology module.
	 */
	private OntologyOperations ontologyOperations;
	/**
	 * Contains a reference to the business logic of the Ontology module.
	 */
	private OntologyService ontologyService;
	/**
	 * Contains a reference to the object that contains all data of the ontologies. 
	 */
	private OntologyCache ontologyCache;
	
	/**
	 * Validates an individual with the help of the reasoner.
	 * @param ontologyIndividual The individual that should be validated.
	 */
	public void validateIndividuals(final OntologyIndividualCollection individuals) {
		ontologyOperations.validateIndividuals(individuals);
	}

	/**
	 * Finds the individuals of an information package.
	 * @param uuidOfInformationPackageAsString The uuid of the information package.
	 * @return The found individuals.
	 */
	public OntologyIndividualCollection findIndiviualsByInformationPackage(final String uuidOfInformationPackageAsString) {
		return ontologyService.findIndiviualsByInformationPackage(uuidOfInformationPackageAsString);
	}
	
	/**
	 * Validates an individual with the help of the reasoner.
	 * @param ontologyIndividual The individual that should be validated.
	 * @throws OntologyReasonerException if the individual contains values that could not be verified by the reasoner.
	 */
	public void validateIndividual(final OntologyIndividual ontologyIndividual) {
		ontologyOperations.validateIndividual(ontologyIndividual);
	}

	/**
	 * Returns a class by its iri.
	 * @param iri The iri of the class we are looking for.
	 * @return The found class.
	 */
	public OntologyClass findOntologyClassByIri(final String iri) {
		return ontologyCache.getClassByIri(iri);
	}
	
	/**
	 * Looks for individuals that are predefined in the base ontology by their uuids.
	 * @param ontologyIndividualList The list contains empty individuals. They only contain the uuids. The method 
	 * looks for the individuals and writes their values in the corresponding object in the list.
	 * @return A collection that contains values for the individuals that could be filled. The individuals whose 
	 * uuids could not be found in the ontology do not contain any values.
	 */
	public OntologyIndividualCollection findIndividualsByUuids(final OntologyIndividualCollection ontologyIndividuals) {
		return ontologyService.findIndividualByUuidsFromOntologies(ontologyIndividuals);
	}
	
	/**
	 * Sets the reference to the operations of the Ontology module.
	 * @param ontologyOperations A reference to the operations of the Ontology module.
	 */
	public void setOntologyOperations(final OntologyOperations ontologyOperations) {
		this.ontologyOperations = ontologyOperations;
	}
	
	/**
	 * Sets a reference to the object that caches the values of the ontologies.
	 * @param ontologyCache A reference to the object that caches the values of the ontologies.
	 */
	public void setOntologyCache(final OntologyCache ontologyCache) {
		this.ontologyCache = ontologyCache;
	}
	
	/**
	 * Sets a reference to the business logic of this module.
	 * @param ontologyService A reference to the business logic.
	 */
	public void setOntologyService(final OntologyService ontologyService) {
		this.ontologyService = ontologyService;
	}
	
	/**
	 * Determines individuals from the ontology with the help from their uuids. 
	 * @param uuidsOfIndividuals The uuids of the individuals we are looking for. 
	 * @return Contains the individuals that were found with the help from the uuids.
	 */
	public OntologyIndividualCollection findIndividualByUuidsFromOntologies(final UuidList uuidsOfIndividuals) {
		return ontologyService.findIndividualByUuidsFromOntologies(uuidsOfIndividuals);
	}
}
