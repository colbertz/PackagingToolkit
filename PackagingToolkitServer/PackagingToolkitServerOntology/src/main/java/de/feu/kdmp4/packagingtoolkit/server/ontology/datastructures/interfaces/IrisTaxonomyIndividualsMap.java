package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;

/**
 * Contains iris as key and a collection with taxonomy individiuals as value. 
 * @author Christopher Olbertz
 *
 */
public interface IrisTaxonomyIndividualsMap {
	/**
	 * Adds an idividual to the map. 
	 * @param iri The iri the individual is assigned to.
	 * @param taxonomyIndividual The individual that should be added to the map.
	 */
	void addTaxonomyIndividal(Iri iri, TaxonomyIndividual taxonomyIndividual);
	/**
	 * Determines the individuals that are assigned to a certain iri in the map.
	 * @param iri The iri whose individuals we are looking for.
	 * @return The individuals that are assigned to iri.
	 */
	Optional<TaxonomyIndividualCollection> getTaxonomyIndividuals(Iri iri);

}
