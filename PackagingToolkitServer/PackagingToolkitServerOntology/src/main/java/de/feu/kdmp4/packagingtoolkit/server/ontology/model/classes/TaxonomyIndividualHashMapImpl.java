package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolit.server.ontology.model.interfaces.TaxonomyIndividualMap;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;

public class TaxonomyIndividualHashMapImpl implements TaxonomyIndividualMap {
	/**
	 * Contains the individuals in a taxonomy as values and iris as key.
	 */
	private Map<Iri, TaxonomyIndividual> taxonomyIndividualMap;
	
	/**
	 * Creates a new object and initializes a HashMap.
	 */
	public TaxonomyIndividualHashMapImpl() {
		taxonomyIndividualMap = new HashMap<>();
	}
	
	@Override
	public void addTaxonomyIndividual(final Iri iriOfIndividual, final TaxonomyIndividual taxonomyIndividual) {
		if (iriOfIndividual != null) {
			taxonomyIndividualMap.put(iriOfIndividual, taxonomyIndividual);
		}
	}
	
	@Override
	public void addTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual) {
		final Iri iriOfIndividual = taxonomyIndividual.getIri();
		if (iriOfIndividual != null) {
			taxonomyIndividualMap.put(iriOfIndividual, taxonomyIndividual);
		}
	}
	
	@Override
	public Optional<TaxonomyIndividual> getTaxonomyIndividual(final Iri iriOfIndividual) {
		final TaxonomyIndividual taxonomyIndividual = taxonomyIndividualMap.get(iriOfIndividual);
		return OntologyOptionalFactory.createOptionalWithTaxonomyIndividual(taxonomyIndividual);
	}
	
	@Override
	public boolean containsTaxonomyIndividual(final Iri iri) {
		return taxonomyIndividualMap.containsKey(iri);
	}

	@Override
	public int countTaxonomyIndividuals() {
		return taxonomyIndividualMap.size();
	}
}
