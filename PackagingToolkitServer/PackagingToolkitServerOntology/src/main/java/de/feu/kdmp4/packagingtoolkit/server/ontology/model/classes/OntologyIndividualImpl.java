package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.util.Optional;

import org.apache.jena.vocabulary.SKOS;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TriplePredicateImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleSubjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class OntologyIndividualImpl implements OntologyIndividual {
	/**
	 * 
	 */
	private static final long serialVersionUID = -457859572134184882L;
	/**
	 * The class in the ontology the individual belongs to.
	 */
	transient private OntologyClass ontologyClass;
	/**
	 * The iri of the individual. If the individual has been read from the base ontology, its iri from the base ontology is used.
	 * If the individual has been defined by the user, the uuid and the default namespace of the PackagingToolkit is 
	 * used as namespace. 
	 */
	private Iri iri;
	
	/**
	 * Contains the datatype properties of this individual. A datatype property is related to a simple literal.
	 */
	transient private DatatypePropertyCollection datatypeProperties;
	/**
	 * Contains the object properties of this individual. A datatype property is related to an individual of another
	 * class.
	 */
	transient private ObjectPropertyCollection objectProperties;
	/**
	 * The uuid that identifies the individual itself.
	 */
	private Uuid uuidOfIndividual;
	/**
	 * The uuid that identifies the information package this individual belongs to.
	 */
	private Uuid uuidOfInformationPackage;

	/**
	 * This constructor is used by frameworks.
	 */
	public OntologyIndividualImpl() {
		initialize();
	}
	
	/**
	 * Constructs a new individual.
	 * @param ontologyClass The class in the ontology the individual belongs to.
	 * @param uuidOfIndividual The uuid of the individual if some has been already created.
	 */
	public OntologyIndividualImpl(final OntologyClass ontologyClass, final Uuid uuidOfIndividual) {
		this.ontologyClass = ontologyClass;
		this.uuidOfIndividual = uuidOfIndividual;
		iri = ServerModelFactory.createIri(uuidOfIndividual);
		initialize();
	}
	
	/**
	 * Constructs a new individual.
	 * @param uuidOfIndividual The uuid of the individual if some has been already created.
	 */
	public OntologyIndividualImpl(final String uuidOfIndividual) {
		this.uuidOfIndividual = new Uuid(uuidOfIndividual);
		iri = ServerModelFactory.createIri(this.uuidOfIndividual);
		initialize();
	}
	
	@Override
	public boolean isSkosIndividual() {
		final String localNameOfOntologyClass = ontologyClass.getLocalName().toString(); 
		 if(localNameOfOntologyClass.equals(SKOS.ConceptScheme.getLocalName())) {
			 return true;
		 } else if(localNameOfOntologyClass.equals(SKOS.Concept.getLocalName())) {
			 return true;
		 } else {
			 return false;
		 }
	}
	
	/**
	 * Constructs a new individual.
	 * @param ontologyClass The class in the ontology the individual belongs to.
	 * @param uuidOfIndividual The uuid of the individual if some has been already created.
	 */
	public OntologyIndividualImpl(final OntologyClass ontologyClass, final Iri iriOfIndividual) {
		this.ontologyClass = ontologyClass;
		this.iri = iriOfIndividual;
		tryToUseLocalNameAsUuid(iriOfIndividual);
		initialize();
	}
	
	/**
	 * Gets an iri and try to convert the local name into an uuid. If this is successful, the local name is
	 * used as uuid. If not, a random uuid is used.
	 * @param iri The iri that contains eventually an iri.
	 */
	private void tryToUseLocalNameAsUuid(final Iri iri) {
		final LocalName localName = iri.getLocalName();
		try {
			uuidOfIndividual = new Uuid(localName.toString());
		} catch (IllegalArgumentException ex) {
			uuidOfIndividual = new Uuid();
		}
	}
	
	/**
	 * Constructs a new individual.
	 * @param ontologyClass The class in the ontology the individual belongs to.
	 * @param uuidOfInformationPackage The uuid of the information package the individual belongs to.
	 * @param uuidOfIndividual The uuid of the individual if some has been already created.
	 */
	public OntologyIndividualImpl(final OntologyClass ontologyClass, final Uuid uuidOfInformationPackage, 
			final Uuid uuidOfIndividual, final DatatypePropertyCollection datatypeProperties,
			final ObjectPropertyCollection objectProperties) {
		this.ontologyClass = ontologyClass;
		this.objectProperties = objectProperties;
		this.datatypeProperties = datatypeProperties;
		this.uuidOfIndividual = uuidOfIndividual;
		this.uuidOfInformationPackage = uuidOfInformationPackage;
		initialize();
	}
	
	/**
	 * Constructs a new individual.
	 * @param ontologyClass The class in the ontology the individual belongs to.
	 * @param uuidOfInformationPackage The uuid of the information package the individual belongs to.
	 */
	public OntologyIndividualImpl(final OntologyClass ontologyClass, final Uuid uuidOfInformationPackage, final DatatypePropertyCollection datatypeProperties,
			final ObjectPropertyCollection objectProperties) {
		this.ontologyClass = ontologyClass;
		this.objectProperties = objectProperties;
		this.datatypeProperties = datatypeProperties;
		initialize();
	}
	
	@Override
	public boolean hasProperties() {
		if(objectProperties == null && datatypeProperties == null) {
			return false;
		} else if (objectProperties.getPropertiesCount() == 0 && datatypeProperties.getPropertiesCount() == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Initializes a new object. Is called by frameworks if they create objects by theirself.
	 */
	//@PostConstruct
	public void initialize() {
		if (uuidOfIndividual == null) {
			this.uuidOfIndividual = new Uuid();
		}
		if (iri == null) {
			iri = new IriImpl(uuidOfIndividual);
		}
		this.datatypeProperties = new DatatypePropertyCollectionImpl();
		this.objectProperties = new ObjectPropertyCollectionImpl();
	}
	
	/**
	 * Constructs a new individual.
	 * @param ontologyClass The class in the ontology the individual belongs to.
	 * @param propertyList The datatype properties this individual contains values for.
	 * @param uuidOfInformationPackage The uuid of the information package the individual belongs to.
	 */
	public OntologyIndividualImpl(final OntologyClass ontologyClass, final DatatypePropertyCollection propertyList,final  Uuid uuid, 
			final Uuid uuidOfInformationPackage) {
		this.datatypeProperties = propertyList;
		this.ontologyClass = ontologyClass;
		this.uuidOfIndividual = uuid;
	}
	
	/**
	 * Constructs a new individual.
	 * @param ontologyClass The class in the ontology the individual belongs to.
	 * @param datatypeProperties The datatype properties this individual contains values for.
	 * @param objectProperties The object properties this individual contains values for. 
	 * @param uuidOfInformationPackage The uuid of the information package the individual belongs to.
	 */
	public OntologyIndividualImpl(final OntologyClass ontologyClass, final DatatypePropertyCollection datatypeProperties, 
			final ObjectPropertyCollection objectProperties, final Uuid uuidOfInformationPackage) {
		this.datatypeProperties = datatypeProperties;
		this.ontologyClass = ontologyClass;
		this.objectProperties = objectProperties;
		this.uuidOfIndividual = new Uuid();
	}
	
	@Override
	public Optional<TripleStatementCollection> toTripleStatements() {
		if (uuidOfInformationPackage != null) {
			final TripleStatementCollection tripleStatementList = createDatatypePropertiesStatements();
			final TripleStatementCollection objectPropertiesStatementList = createObjectPropertiesStatements();
			tripleStatementList.addStatements(objectPropertiesStatementList);
			tripleStatementList.addTripleStatement(createAggregatedByStatement());
			tripleStatementList.addTripleStatement(createTypeStatement());
			final Optional<TripleStatementCollection> optionalWithStatements = ServerOptionalFactory.createOptionalWithTripleStatements(tripleStatementList);
			return optionalWithStatements;
		} else {
			return ServerOptionalFactory.createEmptyOptionalWithTripleStatements();
		}
	}
	
	/**
	 * Creates a type statement that defines the relationship between the individual and its class in the ontology.
	 * @return The type statement.
	 */
	private TripleStatement createTypeStatement() {
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual.toString());
		final String iriOfClass = ontologyClass.getFullName();
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(iriOfClass);
		final TripleStatement tripleStatement = RdfElementFactory.createTypeStatement(tripleSubject, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a aggregatedBy statement that contains the information to which information package this individual
	 * belongs. The subject of the statement is the individual itself and the object is  uuidOfInformationPackage.
	 * @return The aggregatedBy statement.
	 */
	private TripleStatement createAggregatedByStatement() {		
		final TripleSubject subjectWithIndividual = RdfElementFactory.createTripleSubject(iri.toString());
		if (uuidOfInformationPackage != null) {
			final TripleObject objectWithInformationPackage = RdfElementFactory.createTripleObject(uuidOfInformationPackage);
			final TripleStatement aggregatedByStatement = RdfElementFactory.createAggregatedByStatement(subjectWithIndividual, 
					objectWithInformationPackage);
			
			return aggregatedByStatement;
		}
		return null;
	}
	
	/**
	 * Creates a list with statements. Every statement represents one datatype property the individual assigns a value to. 
	 * @return A list with the statements.
	 */
	private TripleStatementCollection createDatatypePropertiesStatements() {
		final TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		
		for (int i = 0; i < datatypeProperties.getPropertiesCount(); i++) {
			final OntologyDatatypeProperty datatypeProperty = datatypeProperties.getDatatypeProperty(i);
			final TripleStatementCollection tripleStatementsForProperty = createStatementsForProperty(datatypeProperty);
			tripleStatementList.addStatements(tripleStatementsForProperty);
		}
		
		return tripleStatementList;
	}
	
	/**
	 * Creates a list with statements. Every statement represents one object property the individual assigns a value to. 
	 * @return A list with the statements.
	 */
	private TripleStatementCollection createObjectPropertiesStatements() {
		final TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		
		for (int i = 0; i < objectProperties.getPropertiesCount(); i++) {
			final OntologyObjectProperty objectProperty = objectProperties.getObjectProperty(i);
			final TripleStatementCollection tripleStatementsForProperty = createStatementsForProperty(objectProperty);
			tripleStatementList.addStatements(tripleStatementsForProperty);
		}
		
		return tripleStatementList;
	}
	
	// REFACTORIZE_ME Die Methode ist viel zu lang. 
	private TripleStatementCollection createStatementsForProperty(final OntologyProperty ontologyProperty) {
		final TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		final String iriOfProperty = ontologyProperty.getPropertyId();
		final String namespaceAsString = StringUtils.extractNamespaceFromIri(iriOfProperty); 
		final Namespace namespace = ServerModelFactory.createNamespace(namespaceAsString);
		final String localNameAsString = StringUtils.extractLocalNameFromIri(iriOfProperty); 
		final LocalName localName = ServerModelFactory.createLocalName(localNameAsString);
		final Uuid uuidOfProperty = ontologyProperty.getUuid();
		final TripleSubject tripleSubject = new TripleSubjectImpl(uuidOfIndividual);
		// The predicate is the property.
		final TriplePredicate  triplePredicate = new TriplePredicateImpl(uuidOfProperty, namespace, localName);
		
		if (ontologyProperty.isObjectProperty()) {
			final OntologyObjectProperty objectProperty = (OntologyObjectProperty)ontologyProperty;
			final OntologyIndividualCollection individualList = objectProperty.getPropertyValue();
			triplePredicate.setPredicateType(PredicateType.OBJECT_PROPERTY);
			
			for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
				final OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
				final String iriOfRange = objectProperty.getPropertyRange();
				final Uuid uuidOfIndividual = ontologyIndividual.getUuid();
				final TripleObject tripleObject = new TripleObjectImpl(uuidOfIndividual.toString());
				final TripleStatement tripleStatement = new TripleStatementImpl(tripleSubject, triplePredicate, tripleObject);
				tripleStatementList.addTripleStatement(tripleStatement);
				final TripleStatement hasObjectStatement = RdfElementFactory.createHasObjectStatement(tripleStatement, iriOfRange);
				tripleStatementList.addTripleStatement(hasObjectStatement);
			}
		} else {
			final Object value = ontologyProperty.getPropertyValue();
			triplePredicate.setPredicateType(PredicateType.LITERAL);
			if (value != null) {
				final TripleObject tripleObject = new TripleObjectImpl(value);
				final TripleStatement tripleStatement = new TripleStatementImpl(tripleSubject, triplePredicate, tripleObject);
				tripleStatementList.addTripleStatement(tripleStatement);
				TripleStatement hasDatatypeStatement = RdfElementFactory.createHasDatatypeStatement(tripleStatement);
				tripleStatementList.addTripleStatement(hasDatatypeStatement);
			}
		}
		
		return tripleStatementList;
	}
	
	@Override
	public OntologyIndividual clone() throws CloneNotSupportedException {
		// TODO Hier werden die Properties noch nicht geklont.
		final OntologyIndividual clonedOntologyIndividual = new OntologyIndividualImpl(getOntologyClass(), uuidOfInformationPackage, datatypeProperties,
				objectProperties);
		return clonedOntologyIndividual;
	}
	
	@Override
	public void addDatatypeProperty(final OntologyDatatypeProperty ontologyProperty) {
		datatypeProperties.addDatatypeProperty(ontologyProperty);
	}
	
	@Override
	public void addObjectProperty(final OntologyObjectProperty ontologyProperty) {
		objectProperties.addObjectProperty(ontologyProperty);
	}
	
	@Override
	public OntologyDatatypeProperty getDatatypePropertyAt(final int index) {
		return datatypeProperties.getDatatypeProperty(index);
	}
	
	@Override
	public int getDatatypePropertiesCount() {
		return datatypeProperties.getPropertiesCount();
	}
	
	@Override
	public OntologyObjectProperty getObjectPropertyAt(final int index) {
		return objectProperties.getObjectProperty(index);
	}

	@Override
	public int getObjectPropertiesCount() {
		return objectProperties.getPropertiesCount();
	}
	
	@Override
	public OntologyClass getOntologyClass() {
		return ontologyClass;
	}
	
	@Override
	public void setOntologyClass(final OntologyClass ontologyClass) {
		this.ontologyClass = ontologyClass;
	}
	
	@Override
	public Uuid getUuid() {
		return uuidOfIndividual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuidOfIndividual == null) ? 0 : uuidOfIndividual.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyIndividualImpl other = (OntologyIndividualImpl) obj;
		if (uuidOfIndividual == null) {
			if (other.uuidOfIndividual != null)
				return false;
		} else if (!uuidOfIndividual.equals(other.uuidOfIndividual))
			return false;
		return true;
	}

	@Override
	public Namespace getNamespace() {
		return ontologyClass.getNamespace();
	}
	
	@Override
	public void setUuid(final String uuid) {
		this.uuidOfIndividual = new Uuid(uuid);
		iri = ServerModelFactory.createIri(uuidOfIndividual);
	}
	
	@Override
	public void setUuid(final Uuid uuid) {
		this.uuidOfIndividual = uuid;
	}
	
	@Override
	public void setUuidOfInformationPackage(final String uuid) {
		this.uuidOfInformationPackage = new Uuid(uuid);
	}
	
	@Override
	public void setUuidOfInformationPackage(final Uuid uuid) {
		this.uuidOfInformationPackage = uuid;
	}
	
	@Override
	public Uuid getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}

	@Override
	public void setDatatypeProperty(final DatatypePropertyCollection datatypePropertyList) {
		this.datatypeProperties = datatypePropertyList;
	}

	@Override
	public void setObjectProperty(final ObjectPropertyCollection objectPropertyList) {
		this.objectProperties = objectPropertyList;
	}

	@Override
	public boolean hasObjectProperty(final String iriOfObjectProperty) {
		for (int i = 0; i < objectProperties.getPropertiesCount(); i++) {
			final OntologyObjectProperty currentProperty = objectProperties.getObjectProperty(i);
			final String currentPropertyIri = currentProperty.getPropertyId();
			if (StringUtils.areStringsEqual(currentPropertyIri, iriOfObjectProperty)) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public void addIndividualToObjectProperty(OntologyObjectProperty objectProperty,
			final OntologyIndividual ontologyIndividual) {
		final String iriOfObjectProperty = objectProperty.getPropertyId();
		if (hasObjectProperty(iriOfObjectProperty)) {
			objectProperty = getObjectProperty(iriOfObjectProperty);
			objectProperty.addOntologyIndividual(ontologyIndividual);
		} else {
			objectProperty.addOntologyIndividual(ontologyIndividual);
			addObjectProperty(objectProperty);
		}
	}
	
	@Override
	public OntologyObjectProperty getObjectProperty(final String iriOfObjectProperty) {
		for (int i = 0; i < objectProperties.getPropertiesCount(); i++) {
			final OntologyObjectProperty currentProperty = objectProperties.getObjectProperty(i);
			final String currentPropertyIri = currentProperty.getPropertyId();
			if (StringUtils.areStringsEqual(currentPropertyIri, iriOfObjectProperty)) {
				return currentProperty;
			}
		}
		
		return null;
	}
	
	@Override
	public Iri getIri() {
		return iri;
	}
	
	@Override
	public void setIri(Iri iri) {
		this.iri = iri;
		tryToUseLocalNameAsUuid(iri);
	}
	
	@Override
	public String toString() {
		return iri.toString();
	}
}
