package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;

public interface TaxonomyIndividual {
	/**
	 * Adds an individual as narrower to this individual. A narrower is an individual that
	 * follows this one in the taxonomy and specifies a concept.
	 * @param taxonomyIndividual The individual that as added as narrower.
	 */
	void addNarrower(TaxonomyIndividual taxonomyIndividual);
	/**
	 * Determines the iri of this individual.
	 * @return The iri of this individual.
	 */
	Iri getIri();
	/**
	 * Determines the number of narrowers of this individual.
	 * @return The number of narrowers of this individual.
	 */
	int getNarrowerCount();
	/**
	 * Checks if a given individual is a narrower of this individual.
	 * @param narrower The individual we want to check if its is a narrower.
	 * @return True if narrower is a narrower, false otherwise.
	 */
	boolean containsNarrower(TaxonomyIndividual narrower);
	/**
	 * Finds a narrower of this individual by its iri. 
	 * @param iriOfNarrower The iri of the narrower we are looking for.
	 * @return The found narrower or an empty optional if the narrower was not found or
	 * the individual does not have any narrowers.
	 */
	Optional<TaxonomyIndividual> findNarrower(Iri iriOfNarrower);
	/**
	 * Determines the text that was defined as preferred label in the ontology.
	 * @return The preferred label.
	 */
	String getPreferredLabel();
	/**
	 * Determines the scheme this individual is assigned to. 
	 * @return The scheme this individual is assigned to.
	 */
	Namespace getInScheme();
	/**
	 * Checks if this individual is assigned to a certain taxonomy.
	 * @param taxonomy The taxonomy we want to check if the individual is assigned to.
	 * @return True if the individual is assigned to taxonomy, else otherwise. 
	 */
	boolean isInTaxonomy(Taxonomy taxonomy);
	/**
	 * Checks if this individual has any narrowers.
	 * @return True if the individual has any narrowers, false otherwise.
	 */
	boolean containsNarrowers();
	/**
	 * Determines the next narrower if there is any.
	 * @return The next narrower or an empty optional if there is no next narrower.
	 */
	Optional<TaxonomyIndividual> getNextNarrower();
	/**
	 * Determines if there is any next narrower in this individual.
	 * @return True if there is a next narrower, false otherwise.
	 */
	boolean hasNextNarrower();
	Iri getIriOfBroader();
	void setPreferredLabel(String preferredLabel);
}
