package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.PropertyRangeException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;

public class OntologyBytePropertyImpl extends OntologyDatatypePropertyImpl 
									  implements OntologyByteProperty {
	private static final long serialVersionUID = 106001928244230493L;
	private static final short SIGNED_MAX = 127;
	private static final short SIGNED_MIN = -128;
	private static final short UNSIGNED_MAX = 255;
	private static final short UNSIGNED_MIN = 0;
	
	private boolean unsigned;
	
	public OntologyBytePropertyImpl() { }
	
	public OntologyBytePropertyImpl(String propertyId, boolean unsigned, 
			String defaultLabel, String range) {
		super(propertyId, (short)0, defaultLabel, range);
		this.unsigned = unsigned;
	}

	@PostConstruct
	public void initialize() {
		super.initialize();
		if (unsigned) {
			addMaxInclusiveRestriction(UNSIGNED_MAX);
			addMinInclusiveRestriction(UNSIGNED_MIN);
		} else {
			addMaxInclusiveRestriction(SIGNED_MAX);
			addMinInclusiveRestriction(SIGNED_MIN);
		}
	}
	
	@Override
	public void addMaxInclusiveRestriction(final short value) {
		if (!checkByteRange(value)) {
			PropertyRangeException exception = PropertyRangeException.valueNotInByteRange(value);
			throw exception;
		}
		RestrictionValue<Short> restrictionValue = new RestrictionValue<>(value);
		MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}
	
	@Override
	public void addMinInclusiveRestriction(final short value) {
		if (!checkByteRange(value)) {
			PropertyRangeException exception = PropertyRangeException.valueNotInByteRange(value);
			throw exception;
		}
		RestrictionValue<Short> restrictionValue = new RestrictionValue<>(value);
		MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public Short getPropertyValue() {
		return (Short)getValue();
	}
	
	@Override
	public boolean isUnsigned() {
		return unsigned;
	}

	@Override
	public boolean isByteProperty() {
		return true;
	}
	
	private boolean checkByteRange(final short value) {
		if (unsigned) {
			if (value >= UNSIGNED_MIN && value <= UNSIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		} else {
			if (value >= SIGNED_MIN && value <= SIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		}
	}
}
