package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBytePropertyImpl;

/**
 * An ontology property with a byte value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyByteProperty extends OntologyDatatypeProperty {
	/**
	 * Adds a max inclusive restriction.
	 * @param value The value that may not be overstepped.
	 */
	void addMaxInclusiveRestriction(final short value);
	/**
	 * Adds a min inclusive restriction.
	 * @param value The value that may not be fallen below.
	 */
	void addMinInclusiveRestriction(final short value);
	
	/**
	 * Creates an unsigned property value.
	 * @param propertyId The iri of the property. 
	 * @param label The label of the property.
	 * @param range The range as specified in the ontology.
	 * @return The created byte property. 
	 */
	public static OntologyByteProperty createUnsigned(final String propertyId, final String label, final String range) {
		return new OntologyBytePropertyImpl(propertyId, true, label, range); 
	}

	/**
	 * Creates a signed property value.
	 * @param propertyId The iri of the property. 
	 * @param label The label of the property.
	 * @param range The range as specified in the ontology.
	 * @return The created byte property. 
	 */
	public static OntologyByteProperty createSigned(final String propertyId, final String label, final String range) {
		return new OntologyBytePropertyImpl(propertyId, false, label, range); 
	}
	
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public Short getPropertyValue();
	/**
	 * Determines if the property is unsigned.
	 * @return True if the property is unsigned, false otherwise. 
	 */
	boolean isUnsigned();
}
