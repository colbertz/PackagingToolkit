package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;

/**
 * An interface for a map that contains annotation properties as value and their iris an key.
 * @author Christopher Olbertz
 *
 */
public interface IrisAndTheirAnnotationPropertiesMap {
	/**
	 * Adds an annotation property to the map. The iri of the annotation property is automatically used
	 * as key.
	 * @param ontologyAnnotationProperty The annotation property that is added to the map.
	 */
	void addAnnotationProperty(final OntologyAnnotationProperty ontologyAnnotationProperty);
	/**
	 * Determines an annotation property that is saved in the map by its iri.
	 * @param iri The iri of the annotation property we are looking for.
	 * @return The found annotation property.
	 */
	Optional<OntologyAnnotationProperty> getAnnotationProperty(final String iri);
}
