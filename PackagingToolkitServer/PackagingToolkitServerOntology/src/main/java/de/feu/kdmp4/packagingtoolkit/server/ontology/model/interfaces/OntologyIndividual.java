package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import java.io.Serializable;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;

/**
 * Represents an individual in an ontology.
 * @author Christopher Olbertz
 *
 */
public interface OntologyIndividual extends Serializable, Cloneable {
	/**
	 * Creates a copy of the individual.
	 * @return A new individual object with the values of this individual.
	 * @throws CloneNotSupportedException
	 */
	OntologyIndividual clone() throws CloneNotSupportedException;
	/**
	 * Determines the class this individual is assigned to.
	 * @return The class this individual is assigned to.
	 */
	OntologyClass getOntologyClass();
	/**
	 * Adds a datatype property to this individual.
	 * @param ontologyProperty The property we want to add to the individual.
	 */
	void addDatatypeProperty(final OntologyDatatypeProperty ontologyProperty);
	/**
	 * Determines a datatype property at a given index.
	 * @param index The index we are looking at.
	 * @return The found property.
	 */
	OntologyDatatypeProperty getDatatypePropertyAt(final int index);
	/**
	 * Adds an object property to this individual.
	 * @param ontologyProperty The property we want to add to the individual.
	 */
	void addObjectProperty(final OntologyObjectProperty ontologyProperty);
	/**
	 * Determines an object property at a given index.
	 * @param index The index we are looking at.
	 * @return The found property.
	 */
	OntologyObjectProperty getObjectPropertyAt(final int index);
	/**
	 * Checks if there are object properties in this class.
	 * @return True if this class has object properties, false otherwise.
	 */
	boolean hasObjectProperty(final String iriOfObjectProperty);
	/**
	 * Adds a new individual to an object property of this individual.
	 * @param objectProperty The object property we want to add the individual to.
	 * @param ontologyIndividual The individual that should be added.
	 */
	void addIndividualToObjectProperty(final OntologyObjectProperty objectProperty, final OntologyIndividual ontologyIndividual);
	/**
	 * Determines the uuid of this individual.
	 * @return The uuid of this individual.
	 */
	Uuid getUuid();
	/**
	 * Counts the datatype properties in this class.
	 * @return The number of datatype properties.
	 */
	int getDatatypePropertiesCount();
	/**
	 * Counts the object properties in this class.
	 * @return The number of object properties.
	 */
	int getObjectPropertiesCount();
	/**
	 * Converts the individual to a list with statements that are used for modelling in RDF. The list contains
	 * a statement for every property the individual contains and exactly one aggregatedBy statement that
	 * describes to which information package the individual belongs. 
	 * @return The list with the statements.
	 */
	Optional<TripleStatementCollection> toTripleStatements();
	/**
	 * Returns the namespace of this class. The namespace is the part in the iri before the # symbol.
	 * @return The namespace of this class.
	 */
	Namespace getNamespace();
	/**
	 * Sets the class this individual is assigned to. 
	 * @param ontologyClass The class of this individual. 
	 */
	void setOntologyClass(final OntologyClass ontologyClass);
	/**
	 * Sets the uuid of this individual. 
	 * @param uuid The uuid of this individual.
	 */
	void setUuid(final String uuid);
	/**
	 * Sets the uuid of the information package this individual belongs to. 
	 * @param uuid The uuid of the information package this individual belongs to.
	 */
	void setUuidOfInformationPackage(final String uuid);
	/**
	 * Sets the uuid of the information package this individual belongs to. 
	 * @param uuid The uuid of the information package this individual belongs to.
	 */
	void setUuidOfInformationPackage(final Uuid uuid);
	/**
	 * Gets the uuid of the information package this individual belongs to. 
	 * @return The uuid of the information package this individual belongs to.
	 */
	Uuid getUuidOfInformationPackage();
	/**
	 * Sets a list of datatype properties in this individual.
	 * @param datatypeProperties The datatype properties that belong to this individual. 
	 */
	void setDatatypeProperty(final DatatypePropertyCollection datatypeProperties);
	/**
	 * Sets a list of object properties in this individual.
	 * @param objectProperties The object properties that belong to this individual. 
	 */
	void setObjectProperty(final ObjectPropertyCollection objectProperties);
	/**
	 * Determines an object property with a certain iri. 
	 * @param iriOfObjectProperty The iri of the object property we are looking for. 
	 * @return The found object property. 
	 */
	OntologyObjectProperty getObjectProperty(final String iriOfObjectProperty);
	/**
	 * Sets the uuid of this individual. 
	 * @param uuid The uuid of this individual.
	 */
	void setUuid(Uuid uuid);
	/**
	 * Checks if there are properties in this class.
	 * @return True if this class has properties, false otherwise.
	 */
	boolean hasProperties();
	Iri getIri();
	void setIri(Iri iri);
	/**
	 * Determines if this individual is an individual either of SKOS concept or of SKOS concept scheme.
	 * @return True if this individual is an individual of a SKOS class, false otherwise.
	 */
	boolean isSkosIndividual();
}
