package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

public class OntologyDateTimePropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDateTimeProperty {
	private static final long serialVersionUID = 1748033123603000410L;

	/**
	 * Creates a new property.
	 */
	public OntologyDateTimePropertyImpl() {
	}
	
	/**
	 * Constructs a new property with a value.
	 * @param propertyId The iri of the property.
	 * @param value The value of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDateTimePropertyImpl(final String propertyId, final LocalDateTime value, final String defaultLabel, final String range) {
		super(propertyId, value, defaultLabel, range);
	}

	/**
	 * Constructs a new property without a value.
	 * @param propertyId The iri of the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDateTimePropertyImpl(final String propertyId, final String range) {
		super(propertyId, null, null, range);
	}

	/**
	 * Constructs a new property without a value.
	 * @param propertyId The iri of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDateTimePropertyImpl(final String propertyId, final String label, final String range) {
		super(propertyId, null, label, range);
	}
	
	@Override
	public LocalDateTime getPropertyValue() {
		return (LocalDateTime) getValue();
	}

	@Override
	public boolean isDateTimeProperty() {
		return true;
	}
	
	@Override
	public String toString() {
		return DateTimeUtils.getLocalDateTimeAsOntologyString(getPropertyValue());
	}
}
