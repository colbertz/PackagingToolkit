package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

/**
 * A collection with properties. The collection can contain datatype and object properties.
 * @author Christopher Olbertz
 *
 */
public interface PropertyCollection {
	/**
	 * Gets a property at a given index of the list.
	 * @param index The index.
	 * @return The property found at the index.
	 */
	OntologyProperty getProperty(final int index);
	/**
	 * Adds a property to the list.
	 * @param property The property that should be appended to the list.
	 */
	void addProperty(final OntologyProperty property);
	/**
	 * Determines the number of properties saved in the collection.
	 * @return The number of properties in the collection.
	 */
	int getPropertiesCount();
	/**
	 * Determines if the collection contains any properties.
	 * @return True if the collection is empty, false otherwise.
	 */
	boolean isEmpty();
}
