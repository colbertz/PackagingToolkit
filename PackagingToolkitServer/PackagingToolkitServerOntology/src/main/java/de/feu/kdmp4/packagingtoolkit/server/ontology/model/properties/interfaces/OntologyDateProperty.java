package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import java.time.LocalDate;

public interface OntologyDateProperty extends OntologyDatatypeProperty {
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public LocalDate getPropertyValue();
}
