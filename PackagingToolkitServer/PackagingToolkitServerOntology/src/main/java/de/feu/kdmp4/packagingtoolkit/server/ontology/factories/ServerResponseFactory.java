package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyResponse;
import de.feu.kdmp4.packagingtoolkit.response.TextInLanguageListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TextInLanguageResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguageCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;

/**
 * Creates the response objects out of the model objects and vice versa. The response objects are used for
 * sending between server, mediator and clients. This class only works with the model objects of the server.
 * @author Christopher Olbertz
 *
 */
public class ServerResponseFactory {
	/**
	 * Creates an reponse object out of an object of {@linkde.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty}.
	 * @param datatypePropertyList The property list that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public static final DatatypePropertyListResponse toResponse(final DatatypePropertyCollection datatypePropertyList) {
		final DatatypePropertyListResponse listResponse = ResponseModelFactory.getOntologyPropertyListResponse();
		
		for (int i = 0; i < datatypePropertyList.getPropertiesCount(); i++) {
			final OntologyDatatypeProperty ontologyProperty = datatypePropertyList.getDatatypeProperty(i);
			final DatatypePropertyResponse ontologyPropertyResponse = toResponse(ontologyProperty); 
			listResponse.addDatatypePropertyToList(ontologyPropertyResponse);
		}
		return listResponse;
	}
	
	/**
	 * Creates an reponse object out of an object for references.
	 * @param references The collection with the reference that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public static final ReferenceListResponse toResponse(final ReferenceCollection references, final UuidResponse uuidOfInformationPackage) {
		final ReferenceListResponse referencesResponse = ResponseModelFactory.createEmptyReferenceListResponse();
		
		for (int i = 0; i < references.getReferencesCount(); i++) {
			final Reference reference = references.getReference(i);
			final ReferenceResponse referenceResponse = toResponse(reference, uuidOfInformationPackage);
			referencesResponse.addReferenceToList(referenceResponse);
		}
		return referencesResponse;
	}
	
	/**
	 * Creates an reponse object out of an object a reference.
	 * @param references The reference that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public static final ReferenceResponse toResponse(final Reference reference, final UuidResponse uuidOfInformationPackage) {
		final ReferenceResponse referenceResponse = ResponseModelFactory.getReferenceResponse();
		referenceResponse.setUrl(reference.getUrl());
		final String uuidOfFileAsString = reference.getUuidOfFile().toString();
		final UuidResponse uuidOfFile = ResponseModelFactory.getUuidResponse(uuidOfFileAsString);
		referenceResponse.setUuidOfFile(uuidOfFile);
		referenceResponse.setUuidOfInformationPackage(uuidOfInformationPackage);
		return referenceResponse;
	}
	
	/**
	 * Creates an reponse object out of an object of {import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage}.
	 * @param textInLanguage The object that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public static final TextInLanguageResponse toResponse(final TextInLanguage textInLanguage) {
		final TextInLanguageResponse textInLanguageResponse = new TextInLanguageResponse();
		textInLanguageResponse.setLanguage(textInLanguage.getLanguage().getLanguageCode());
		textInLanguageResponse.setText(textInLanguage.getLabelText());
		
		return textInLanguageResponse;
	}
	
	/**
	 * Creates an reponse object out of an object of {import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguageList}.
	 * @param textInLanguage The object that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public static final TextInLanguageListResponse toResponse(final TextInLanguageCollection textsInLanguage) {
		final TextInLanguageListResponse textInLanguageListResponse = new TextInLanguageListResponse();
		
		for (int i = 0; i < textsInLanguage.getTextsInLanguageCount(); i++) {
			TextInLanguage textInLanguage = textsInLanguage.getTextInLanguage(i);
			TextInLanguageResponse textInLanguageResponse = toResponse(textInLanguage);
			textInLanguageListResponse.addTextInLanguage(textInLanguageResponse);
		}
		
		return textInLanguageListResponse;
	}
	
	/**
	 * Creates an reponse object out of an object of {@link de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyListResponse}.
	 * @param objectPropertyList The property list that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public static final ObjectPropertyListResponse toResponse(final ObjectPropertyCollection objectPropertyList) {
		final ObjectPropertyListResponse listResponse = ResponseModelFactory.getObjectPropertyListResponse();
		
		for (int i = 0; i < objectPropertyList.getPropertiesCount(); i++) {
			final OntologyObjectProperty ontologyProperty = objectPropertyList.getObjectProperty(i);
			final ObjectPropertyResponse ontologyPropertyResponse = toResponse(ontologyProperty); 
			listResponse.addObjectPropertyToList(ontologyPropertyResponse);
		}
		return listResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a datatype property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	public static final DatatypePropertyResponse toResponse(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		DatatypePropertyResponse propertyResponse = null;
		
		if (ontologyDatatypeProperty instanceof OntologyBooleanProperty) {
			propertyResponse = fromOntologyBooleanProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyByteProperty) {
			propertyResponse = fromOntologyByteProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDateProperty) {
			propertyResponse = fromOntologyDateProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDateTimeProperty) {
			propertyResponse = fromOntologyDateTimeProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDoubleProperty) {
			propertyResponse = fromOntologyDoubleProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDurationProperty) {
			propertyResponse = fromOntologyDurationProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyFloatProperty) {
			propertyResponse = fromOntologyFloatProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyIntegerProperty) {
			propertyResponse = fromOntologyIntegerProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyLongProperty) {
			propertyResponse = fromOntologyLongProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyShortProperty) {
			propertyResponse = fromOntologyShortProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyStringProperty) {
			propertyResponse = fromOntologyStringProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyTimeProperty) {
			propertyResponse = fromOntologyTimeProperty(ontologyDatatypeProperty);
		}
		
		propertyResponse.setLabel(ontologyDatatypeProperty.getLabel());
		propertyResponse.setPropertyName(ontologyDatatypeProperty.getPropertyId());
		
		final TextInLanguageListResponse labelTextsResponse = new TextInLanguageListResponse();
		for (int i = 0; i < ontologyDatatypeProperty.getLabelCount(); i++) {
			final TextInLanguage label = ontologyDatatypeProperty.getLabelAt(i);
			final TextInLanguageResponse textInLanguageResponse = toResponse(label);
			labelTextsResponse.addTextInLanguage(textInLanguageResponse);
		}
		propertyResponse.setLabelTexts(labelTextsResponse);
		
		return propertyResponse;		
	} 
	
	/**
	 * Creates an object for sending via http with the values of an object property.
	 * @param ontologyObjectProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	public static final ObjectPropertyResponse toResponse(final OntologyObjectProperty ontologyObjectProperty) {
		final ObjectPropertyResponse objectPropertyResponse = new ObjectPropertyResponse();

		objectPropertyResponse.setLabel(ontologyObjectProperty.getLabel());
		objectPropertyResponse.setPropertyName(ontologyObjectProperty.getPropertyId());
		final OntologyClass range = ontologyObjectProperty.getRange();
		if (range != null) {
			objectPropertyResponse.setRange(range.getFullName());
		}
		for (int i = 0; i < ontologyObjectProperty.getPropertyDomainsCount(); i++) {
			final String domain = ontologyObjectProperty.getPropertyDomainAt(i);
			objectPropertyResponse.addDomain(domain);
		}
		
		final int individualCount = ontologyObjectProperty.getPropertyValue().getIndiviualsCount();
		for (int i = 0; i < individualCount; i++) {
			final OntologyIndividual ontologyIndividual = ontologyObjectProperty.getPropertyValue().getIndividual(i);
			final Iri iriOfIndividual = ontologyIndividual.getIri();
			final String namespace = iriOfIndividual.getNamespace().toString();
			final String localName = iriOfIndividual.getLocalName().toString();
			final IriResponse iriOfIndividualResponse = new IriResponse(namespace, localName);
			objectPropertyResponse.getValue().addIriToList(iriOfIndividualResponse);
		}
		
		final TextInLanguageListResponse labelTextsResponse = new TextInLanguageListResponse();
		for (int i = 0; i < ontologyObjectProperty.getLabelCount(); i++) {
			final TextInLanguage label = ontologyObjectProperty.getLabelAt(i);
			final TextInLanguageResponse textInLanguageResponse = toResponse(label);
			labelTextsResponse.addTextInLanguage(textInLanguageResponse);
		}
		objectPropertyResponse.setLabelTexts(labelTextsResponse);
		
		return objectPropertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a boolean property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyBooleanProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyBooleanProperty ontologyBooleanProperty = (OntologyBooleanProperty)ontologyDatatypeProperty;
		final Object value = ontologyBooleanProperty.getPropertyValue();
		if (value != null) {
			final Boolean booleanValue = (Boolean)value;
			propertyResponse.setValue(Boolean.toString(booleanValue));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.BOOLEAN);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a byte property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyByteProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyByteProperty ontologyByteProperty = (OntologyByteProperty)ontologyDatatypeProperty;
		final Object value = ontologyByteProperty.getPropertyValue();
		if (value != null) {
			final Short shortValue = (Short)ontologyByteProperty.getPropertyValue();
			propertyResponse.setValue(Short.toString(shortValue));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.BYTE);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a date property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyDateProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDateProperty ontologyDateProperty = (OntologyDateProperty)ontologyDatatypeProperty;
		final LocalDate value = (LocalDate)ontologyDateProperty.getPropertyValue();		
		if (value != null) {
			final long epochDay = value.toEpochDay();
			propertyResponse.setValue(Long.toString(epochDay));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DATE);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a datetime property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyDateTimeProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDateTimeProperty ontologyDateTimeProperty = (OntologyDateTimeProperty)ontologyDatatypeProperty;
		final LocalDateTime value = (LocalDateTime)ontologyDateTimeProperty.getPropertyValue();		
		if (value != null) {
			final long epochSecond = value.toEpochSecond(ZoneOffset.UTC);
			propertyResponse.setValue(Long.toString(epochSecond));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DATETIME);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a double property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyDoubleProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDoubleProperty ontologyDoubleProperty = (OntologyDoubleProperty)ontologyDatatypeProperty;
		final Double value = (Double)ontologyDoubleProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Double.toString(value));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DOUBLE);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a duration property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyDurationProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDurationProperty ontologyDurationProperty = (OntologyDurationProperty)ontologyDatatypeProperty;
		final OntologyDuration value = (OntologyDuration)ontologyDurationProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(value.toString());
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DURATION);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a float property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyFloatProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyFloatProperty ontologyFloatProperty = (OntologyFloatProperty)ontologyDatatypeProperty;
		final Float value = (Float)ontologyFloatProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Float.toString(value));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.FLOAT);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of an integer property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyIntegerProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyIntegerProperty ontologyIntegerProperty = (OntologyIntegerProperty)ontologyDatatypeProperty;
		final Long value = (Long)ontologyIntegerProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Long.toString(value));
		}
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.INTEGER);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a long property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyLongProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyLongProperty ontologyLongProperty = (OntologyLongProperty)ontologyDatatypeProperty;
		final BigInteger value = (BigInteger)ontologyLongProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(value.toString());
		}
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.LONG);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a short property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyShortProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyShortProperty ontologyShortProperty = (OntologyShortProperty)ontologyDatatypeProperty;
		final Integer value = (Integer)ontologyShortProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Integer.toString(value));
		}
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.SHORT);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a string property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyStringProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyStringProperty ontologyStringProperty = (OntologyStringProperty)ontologyDatatypeProperty;
		final String value = (String)ontologyStringProperty.getPropertyValue();
		propertyResponse.setValue(value);
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.STRING);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a time property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	private static DatatypePropertyResponse fromOntologyTimeProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyTimeProperty ontologyTimeProperty = (OntologyTimeProperty)ontologyDatatypeProperty;
		final LocalTime value = (LocalTime)ontologyTimeProperty.getPropertyValue();		
		if (value != null) {
			final long nanoOfDay = value.toNanoOfDay();
			propertyResponse.setValue(Long.toString(nanoOfDay));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.TIME);
		return propertyResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of an object property.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	public static ObjectPropertyResponse fromObjectProperty(final OntologyObjectProperty ontologyObjectProperty) {
		final ObjectPropertyResponse propertyResponse = ResponseModelFactory.getObjectPropertyResponse();
		propertyResponse.setPropertyName(ontologyObjectProperty.getPropertyId());
		propertyResponse.setLabel(ontologyObjectProperty.getLabel());
		final OntologyClass range = ontologyObjectProperty.getRange();
		propertyResponse.setRange(range.getFullName());
		return propertyResponse;		
	}
	
	/**
	 * Creates an object for sending via http with the values of an individual.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	public static IndividualResponse fromOntologyIndividual(final OntologyIndividual ontologyIndividual) {
		final IndividualResponse individualResponse = ResponseModelFactory.getIndividualResponse(ontologyIndividual.getUuid(), 
				ontologyIndividual.getUuidOfInformationPackage());
		int datatypePropertiesCount = ontologyIndividual.getDatatypePropertiesCount();
		
		for (int i = 0; i < datatypePropertiesCount; i++) {
			final OntologyDatatypeProperty ontologyProperty = ontologyIndividual.getDatatypePropertyAt(i);
			final DatatypePropertyResponse propertyResponse = toResponse(ontologyProperty);
			individualResponse.addOntologyProperty(propertyResponse);
		}
		
		int objectPropertiesCount = ontologyIndividual.getObjectPropertiesCount();
		for (int i = 0; i < objectPropertiesCount; i++) {
			final OntologyObjectProperty ontologyProperty = ontologyIndividual.getObjectPropertyAt(i);
			final ObjectPropertyResponse propertyResponse = toResponse(ontologyProperty);
			individualResponse.addObjectProperty(propertyResponse);
		}
		
		final OntologyClass ontologyClass = ontologyIndividual.getOntologyClass();
		final String classIri = ontologyClass.getFullName();
		individualResponse.setClassIri(classIri);
		final Iri iriOfIndividual = ontologyIndividual.getIri();
		final IriResponse  iriOfIndividualResponse = new IriResponse(iriOfIndividual.getNamespace().toString(), iriOfIndividual.getLocalName().toString());
		individualResponse.setIriOfIndividual(iriOfIndividualResponse);
		
		return individualResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a collection with individuals.
	 * @param ontologyDatatypeProperty The property whose values should be used.
	 * @return The object for sending via http.
	 */
	public static IndividualListResponse fromOntologyIndividualList(final OntologyIndividualCollection individualList) {
		final IndividualListResponse individualListResponse = ResponseModelFactory.getIndividualListResponse();
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
			final IndividualResponse individualResponse = fromOntologyIndividual(ontologyIndividual);
			individualListResponse.addIndividualToList(individualResponse);
		}
		
		return individualListResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of an individuals in a taxonomy.
	 * @param taxonomyIndividual The property whose values should be used.
	 * @return The object for sending via http.
	 */
	// TO_TEST
	public static TaxonomyIndividualResponse fromTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual)  {
		final TaxonomyIndividualResponse taxonomyIndividualResponse = ResponseModelFactory.createTaxonomyIndividualResponse();
		final Iri iri = taxonomyIndividual.getIri();
		taxonomyIndividualResponse.setIri(iri.toString());
		taxonomyIndividualResponse.setPreferredLabel(taxonomyIndividual.getPreferredLabel());
		
		while (taxonomyIndividual.hasNextNarrower()) {
			final Optional<TaxonomyIndividual> optionalWithNarrower = taxonomyIndividual.getNextNarrower();
			if (optionalWithNarrower.isPresent()) {
				final TaxonomyIndividual narrower = optionalWithNarrower.get();
				final TaxonomyIndividualResponse narrowerResoponse = fromTaxonomyIndividual(narrower);
				taxonomyIndividualResponse.addNarrower(narrowerResoponse);
			}
		}
		
		return taxonomyIndividualResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a collection with individuals in a taxonomy.
	 * @param taxonomyIndividualCollection The collection that contains the individuals that should be transformed into
	 * response objects.
	 * @return The object for sending via http.
	 */
	public static TaxonomyIndividualListResponse fromTaxonomyIndividualList(final TaxonomyIndividualCollection taxonomyIndividualCollection) {
		final TaxonomyIndividualListResponse taxonomyIndividualListResponse = ResponseModelFactory.createEmptyTaxonomyIndividualListResponse();
		for (int i = 0; i < taxonomyIndividualCollection.getTaxonomyIndividualCount(); i++) {
			final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = taxonomyIndividualCollection.getTaxonomyIndividual(i);
			final TaxonomyIndividual taxonomyIndividual = optionalWithTaxonomyIndividual.get();
			final TaxonomyIndividualResponse taxonomyIndividualResponse = fromTaxonomyIndividual(taxonomyIndividual);
			taxonomyIndividualListResponse.addTaxonomyIndividual(taxonomyIndividualResponse);
		}
		
		return taxonomyIndividualListResponse;
	}
	
	/**
	 * Creates an object for sending via http with the values of a taxonomy.
	 * @param taxonomy The taxonomy that should be transformed into a response object.
	 * @return The object for sending via http.
	 */
	public static TaxonomyResponse fromTaxonomy(final Taxonomy taxonomy) {
		final TaxonomyResponse taxonomyResponse = ResponseModelFactory.createTaxonomyResponse();
		final TaxonomyIndividual rootIndividual = taxonomy.getRootIndividual();
		final TaxonomyIndividualResponse rootIndividualResponse = fromTaxonomyIndividual(rootIndividual);
		taxonomyResponse.setRootIndividual(rootIndividualResponse);
		taxonomyResponse.setNamespace(taxonomy.getNamespace().toString());
		taxonomyResponse.setTitle(taxonomy.getTitle());
		
		return taxonomyResponse;
	}
}