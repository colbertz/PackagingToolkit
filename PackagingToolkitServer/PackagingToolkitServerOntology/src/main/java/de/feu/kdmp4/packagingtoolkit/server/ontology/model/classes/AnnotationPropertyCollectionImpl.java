package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;


import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.AnnotationPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;

/**
 * Implements the collection for annotation properties as list.
 * @author Christopher Olbertz
 *
 */
public class AnnotationPropertyCollectionImpl extends AbstractList implements AnnotationPropertyCollection {
	@Override
	public void addAnnotationProperty(final OntologyAnnotationProperty ontologyAnnotationProperty ) {
		super.add(ontologyAnnotationProperty);
	}
	
	@Override
	public OntologyAnnotationProperty getAnnotationPropertyAt(final int index) {
		return (OntologyAnnotationProperty)super.getElement(index);
	}
	
	@Override
	public int getAnnotationPropertyCount() {
		return super.getSize();
	}
}
