package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Represents a datatype property of string in an ontology. 
 * @author Christopher Olbertz
 *
 */
public class OntologyStringPropertyImpl extends OntologyDatatypePropertyImpl 
										implements OntologyStringProperty, 
												   Comparable<OntologyStringProperty> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 782577707127219080L;
	private static final String STRING_DEFAULT_VALUE = StringUtils.EMPTY_STRING;
	
	public OntologyStringPropertyImpl() {
	}
	
	/**
	 * Gets only a label for this property. The default value is an empty string.
	 * @param label The label.
	 */
	public OntologyStringPropertyImpl(final String propertyId, final String label, final String propertyRange) {
		super(propertyId, STRING_DEFAULT_VALUE, label, propertyRange);
	}
	
	@Override
	public String getPropertyValue() {
		return (String)getValue();
	}
	
	@Override
	public int compareTo(final OntologyStringProperty ontologyProperty) {
		String theStringValue = getPropertyValue();
		return theStringValue.compareTo((String)ontologyProperty.getPropertyValue());
	}
	
	@Override
	public boolean isStringProperty() {
		return true;
	}
}
