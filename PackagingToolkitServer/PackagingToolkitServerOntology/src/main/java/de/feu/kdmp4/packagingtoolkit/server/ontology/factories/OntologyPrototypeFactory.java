package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import de.feu.kdmp4.packagingtoolkit.server.ontology.OntologyConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

/**
 * Contains static methods for creating protoype beans with the help of the Spring
 * application context. Is used if you want to get objects from factories managed by 
 * Spring on places in the code you cannot use the Spring methods that are normally
 * used, for example in the middle of a method. I did not find an other possibility to
 * achieve this but this class.
 * @author Christopher Olbertz
 *
 */
public class OntologyPrototypeFactory {
	private static AnnotationConfigApplicationContext applicationContext;
	
	static {
		applicationContext = new AnnotationConfigApplicationContext(OntologyConfiguration.class);
	}
	
	public static OntologyClass createNewOntologyClass() {
		return applicationContext.getBean(OntologyClass.class);
	}
	
	public static DatatypePropertyCollection createEmptyDatatypePropertyList() {
		return applicationContext.getBean(DatatypePropertyCollection.class);
	}
	
	public static ObjectPropertyCollection createEmptyObjectPropertyList() {
		return applicationContext.getBean(ObjectPropertyCollection.class);
	}
	
	public static OntologyIndividualCollection createEmptyIndividualList() {
		return applicationContext.getBean(OntologyIndividualCollection.class);
	}
	
	public static OntologyProperty cloneProperty(OntologyProperty ontologyProperty) {
		PropertyFactory propertyFactory = applicationContext.getBean(PropertyFactory.class);
		return propertyFactory.cloneOntologyProperty(ontologyProperty);
	}
}
