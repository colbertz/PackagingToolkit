package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import java.io.File;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.OntologyConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.OntologyUuidIndividualHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.IrisAndTheirAnnotationPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyUuidIndividualMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.AnnotationPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.server.triplestore.facades.OntologyTripleStoreFacade;

public class OntologyCache {
	private OntologyConfigurationFacade ontologyConfigurationFacade;
	/**
	 * The root class of the ontology class tree. Its the initial point
	 * for the whole ontology. Based on this object, the whole class
	 * hierarchy of the ontology is stored in the memory. So it is 
	 * not necessary to analyse the ontology every time the classes
	 * in the ontology are used.
	 */
	private OntologyClass mainClass;
	/**
	 * The root class of the Premis ontology.
	 */
	private OntologyClass premisMainClass;
	/**
	 * The root class of the SKOS ontology.
	 */
	private OntologyClass skosMainClass;
	/**
	 * A map that contains the classes of the ontology.
	 */
	private OntologyClassesMap ontologyClassesMap;
	/**
	 * This map contains the classes as key and the properties of these classes as values.
	 */
	private OntologyClassesPropertiesMap ontologyClassesPropertiesMap;
	/**
	 * Contains the annotation property that were extracted from the ontologies. 
	 */
	private IrisAndTheirAnnotationPropertiesMap irisAndTheirAnnotationPropertiesMap;
	/**
	 * This map contains the classes as key and the individuals of these classes as values.
	 */
	private OntologyClassIndividualsMap ontologyClassIndividualsMap;
	/**
	 * This map contains the uuid in the way that searching with the help of their uuids is possible.
	 */
	private OntologyUuidIndividualMap ontologyUuidIndividualMap;
	/**
	 * The operations object that contains the access methods to the operations on
	 * ontologies.
	 */
	private OntologyOperations ontologyOperations;
	/**
	 * Constructs objects that implements the datastructure interfaces.
	 */
	private DataStructureFactory dataStructureFactory;
	/**
	 * A map with the properties found in the ontology.
	 */
	private OntologyPropertiesMap ontologyPropertiesMap;
	// DELETE_ME Wird derzeit nicht benutzt.
	private OntologyTripleStoreFacade ontologyTripleStoreFacade;
	/**
	 * Contains the individuals that have been identified as individuals related to SKOS.
	 */
	private OntologyIndividualCollection skosIndividuals;
	
	/**
	 * Reads and analyzes the ontology files and initializes the datastructures. 
	 */
	@PostConstruct
	public void initialize() {
		initializeNecessaryObjects();
		processOntologyFile();
		processPremisOntology();
		processSkosOntology();
		premisMainClass = ontologyOperations.getPremisHierarchy();
		skosMainClass = ontologyOperations.getSkosHierarchy();
		determineClassHierarchy();
		//extractAnnotationProperties();
		putClassesInMap();
		extractProperties();
		extractIndividuals();
	}
	
	/**
	 * Creates the objects that are necessary for this class.
	 */
	private void initializeNecessaryObjects() {
		ontologyClassesMap = dataStructureFactory.createOntologyClassesMap();
		ontologyClassesPropertiesMap = dataStructureFactory.createOntologyClassesPropertiesMap();
		ontologyPropertiesMap = dataStructureFactory.createOntologyPropertiesMap();
		ontologyClassIndividualsMap = dataStructureFactory.createOntologyClassIndividualsMap();		
		irisAndTheirAnnotationPropertiesMap = dataStructureFactory.createEmptyIrisAndTheirAnnotationPropertiesMap();
		skosIndividuals = OntologyModelFactory.createEmptyOntologyIndividualList();
	}
	
	/**
	 * Reads the configuration data of the server and determines the base ontology file. Then the base ontology file is processed.
	 */
	private void processOntologyFile() {
		final ServerConfigurationData serverConfigurationData = ontologyConfigurationFacade.readServerConfiguration();
		final File baseOntologyFile = serverConfigurationData.getBaseOntologyFile();
		
		ontologyOperations.startBaseOntologyProcessing(baseOntologyFile);
	}
	
	/**
	 * Analyzes the Premis ontology.
	 */
	private void processPremisOntology() {
		final ServerConfigurationData serverConfigurationData = ontologyConfigurationFacade.readServerConfiguration();
		final File premisOntologyFile = serverConfigurationData.getPremisOntologyFile();
		
		ontologyOperations.startPremisOntologyProcessing(premisOntologyFile);
	}
	
	/**
	 * Analyzes the SKOS ontology.
	 */
	private void processSkosOntology() {
		final ServerConfigurationData serverConfigurationData = ontologyConfigurationFacade.readServerConfiguration();
		final File skosOntologyFile = serverConfigurationData.getSkosOntologyFile();
		
		ontologyOperations.startSkosOntologyProcessing(skosOntologyFile);
	}
	
	/**
	 * Determines the class hierarchy of the configured information package class. The class hierarchy
	 * is returned in an ontology class.
	 * @return The ontology classes whose subclasses are the determined subclasses.
	 */
	private OntologyClass determineClassHierarchy() {
		final ServerConfigurationData serverConfigurationData = ontologyConfigurationFacade.readServerConfiguration();
		final String informationPackageClassName = serverConfigurationData.getBaseOntologyClass();
		mainClass = ontologyOperations.getHierarchy(informationPackageClassName);
		return mainClass;
	}
	
	/**
	 * Extracts the datatype and objects properties from the base ontology.
	 */
	private void extractProperties() {
		final DatatypePropertyCollection datatypeProperties = ontologyOperations.extractDatatypeProperties();
		final ObjectPropertyCollection objectProperties = ontologyOperations.extractObjectProperties(ontologyClassesMap);

		for (int i = 0; i < datatypeProperties.getPropertiesCount(); i++) {
			final OntologyProperty property = datatypeProperties.getDatatypeProperty(i);
			for (int j = 0; j < property.getPropertyDomainsCount(); j++) {
				final String domain = property.getPropertyDomainAt(j);
				ontologyClassesPropertiesMap.addProperty(domain, property);
			}
			
			ontologyPropertiesMap.addProperty(property);
		}
		
		for (int i = 0; i < objectProperties.getPropertiesCount(); i++) {
			final OntologyProperty ontologyProperty = objectProperties.getObjectProperty(i);
			
			for (int j = 0; j < ontologyProperty.getPropertyDomainsCount(); j++) {
				final String domain = ontologyProperty.getPropertyDomainAt(j);
				ontologyClassesPropertiesMap.addProperty(domain, ontologyProperty);
			}
			
			ontologyPropertiesMap.addProperty(ontologyProperty);
		}
	}
	
	/**
	 * Extracts the annotation properties from the ontologies (only SKOS at the moment) and puts them into the map.
	 */
	private void extractAnnotationProperties() {
		final AnnotationPropertyCollection annotationProperties = ontologyOperations.extractAnnotationProperties();
		
		for (int i = 0; i < annotationProperties.getAnnotationPropertyCount(); i++) {
			final OntologyAnnotationProperty ontologyAnnotationProperty = annotationProperties.getAnnotationPropertyAt(i);
			irisAndTheirAnnotationPropertiesMap.addAnnotationProperty(ontologyAnnotationProperty);
		}
	}
	
	/**
	 * Extracts the individuals from the base ontology. 
	 */
	private void extractIndividuals() {
		ontologyUuidIndividualMap = new OntologyUuidIndividualHashMapImpl();
		final OntologyIndividualCollection individualList = ontologyOperations.getIndividualsFromOntology(ontologyClassesMap);
		
		for(int i = 0; i < individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
			if (ontologyIndividual.isSkosIndividual()) {
				skosIndividuals.addIndividual(ontologyIndividual);
				ontologyClassIndividualsMap.addOrUpdateIndividual(ontologyIndividual);
				ontologyUuidIndividualMap.addIndividual(ontologyIndividual);
			} else {
				ontologyClassIndividualsMap.addOrUpdateIndividual(ontologyIndividual);
				ontologyUuidIndividualMap.addIndividual(ontologyIndividual);
			}
		}
	}
	
	/**
	 * Returns a class that contains the properties of the class. The desired class is determined by its iri.
	 * @param classIri The iri of the class we are interested in. 
	 * @return The class that contains all properties.
	 */
	public OntologyClass getClassWithProperties(final String classIri) {
		final OntologyClass ontologyClass = ontologyClassesMap.getOntologyClass(classIri);
		if (ontologyClass != null) {
			if (ontologyClass.getDatatypePropertiesCount() == 0) {
				final DatatypePropertyCollection propertyList = ontologyClassesPropertiesMap.getDatatypeProperties(classIri);
				ontologyClass.addDatatypeProperties(propertyList);
			}
			
			if (ontologyClass.getObjectPropertiesCount() == 0) {
				final ObjectPropertyCollection objectProperties = ontologyClassesPropertiesMap.getObjectProperties(classIri);
				ontologyClass.addObjectProperties(objectProperties);
			}
		}
			
		return ontologyClass;
	}
	
	public OntologyClass getMainClass() {
		return mainClass;
	}
	
	public OntologyClass getPremisMainClass() {
		return premisMainClass;
	}
	
	public OntologyClass getSkosMainClass() {
		return skosMainClass;
	}
	
	public OntologyClass getClassByIri(String classname) {
		return ontologyClassesMap.getOntologyClass(classname);
	}
	
	public OntologyIndividualCollection getIndividualsByOntologyClass(final String className) {
		return ontologyClassIndividualsMap.getOntologyIndividuals(className);
	}
	
	public OntologyClass getOntologyClassByPropertyRange(String propertyRange) {
		OntologyClass ontologyClass = ontologyClassesMap.getOntologyClass(propertyRange); 
		return ontologyClass;
	}
	
	// DELETE_ME Wird nicht aufgerufen?
	public void addObjectProperty(String propertyRange, OntologyObjectProperty ontologyObjectProperty) {
		ontologyClassesMap.addProperty(propertyRange, ontologyObjectProperty);
		ontologyPropertiesMap.addProperty(ontologyObjectProperty);
		ontologyClassesPropertiesMap.addProperty(propertyRange, ontologyObjectProperty);
	}
	
	public void addDatatypeProperty(String className, OntologyDatatypeProperty ontologyDatatypeProperty) {
		ontologyClassesMap.addProperty(className, ontologyDatatypeProperty);
		ontologyPropertiesMap.addProperty(ontologyDatatypeProperty);
		ontologyClassesPropertiesMap.addProperty(className, ontologyDatatypeProperty);
	}
	
	public OntologyIndividual findOntologyIndividualByUuid(Uuid uuidOfIndividual) {
		return ontologyUuidIndividualMap.getIndividual(uuidOfIndividual);
	}
	
	public OntologyProperty cloneOntologyProperty(String propertyName) {
		OntologyProperty ontologyProperty = ontologyPropertiesMap.cloneOntologyProperty(propertyName);
		return ontologyProperty;
	}
	
	public void setOntologyOperations(OntologyOperations ontologyOperations) {
		this.ontologyOperations = ontologyOperations;
	}
	
	public void setDataStructureFactory(DataStructureFactory dataStructureFactory) {
		this.dataStructureFactory = dataStructureFactory;
	}
	
	public void setOntologyConfigurationFacade(OntologyConfigurationFacade ontologyConfigurationFacade) {
		this.ontologyConfigurationFacade = ontologyConfigurationFacade;
	}
	
	/**
	 * Runs through the tree of ontology classes and puts their names as keys
	 * in the map.
	 */
	private void putClassesInMap() {
		traverseOntologyTree(mainClass);
		traverseOntologyTree(premisMainClass);
		//traverseOntologyTree(skosMainClass);
	}
	
	/**
	 * Traverses recursivly through the tree of ontology classes and puts the classes in
	 * the map. At this particular time, the classes do not have any properties
	 * in their lists.
	 * @param parentClass The parent class for the recursion step.
	 */
	private void traverseOntologyTree(OntologyClass parentClass) {
		for (int i = 0; i < parentClass.getSubclassCount(); i++) {
			OntologyClass ontologyClass = parentClass.getSubClassAt(i);
			traverseOntologyTree(ontologyClass);
			ontologyClassesMap.addClass(ontologyClass);
		}
	}
	
	public void setOntologyTripleStoreFacade(OntologyTripleStoreFacade ontologyTripleStoreFacade) {
		this.ontologyTripleStoreFacade = ontologyTripleStoreFacade;
	}
	
	public boolean hasNextSkosIndividual() {
		return skosIndividuals.hasNextOntologyIndividual();
	}
	
	public OntologyIndividual getNextSkosIndividual() {
		return skosIndividuals.getNextOntologyIndividual();
	}
	
	public OntologyClassesMap getOntologyClassesMap() {
		return ontologyClassesMap;
	}
}
