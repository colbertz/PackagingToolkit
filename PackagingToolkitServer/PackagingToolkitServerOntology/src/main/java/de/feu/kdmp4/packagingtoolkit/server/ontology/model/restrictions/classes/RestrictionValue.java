package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

/**
 * Represents values that are used in restrictions. The class encapsulates the 
 * values that are comparable. A value can be an object of different datatypes.
 * This class hides the real datatype. This class exists in order that the
 * methods of AbstractRestriction are more flexible.
 * @author Christopher Olbertz
 *
 */
public class RestrictionValue<T extends Comparable<T>> {
	/**
	 * The value that is used in a restriction.
	 */
	private T value;

	/**
	 * Constructs a new restriction.
	 */
	public RestrictionValue() {
	}
	
	/**
	 * Constructs a new restriction with a value that is checked.
	 * @param value The value the restriction is checked against.
	 */
	public RestrictionValue(final T value) {
		this.value = value;
	}
	
	/**
	 * Compares two values.
	 * @param restrictionValue The value we want to compare with.
	 * @return True if the two values are equal, false otherwise.
	 */
	public int compare(final RestrictionValue<T> restrictionValue) {
		return value.compareTo(restrictionValue.getValue());
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
	/**
	 * Returns the value of this restriction.
	 * @return The value of this restriction.
	 */
	public T getValue() {
		return value;
	}
}