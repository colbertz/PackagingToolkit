package de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces;

import java.io.File;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ObjectPropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.AnnotationPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

/**
 * Contains methods for working with ontologies. The methods only work with 
 * Decorator objects so that the implementation can be replaced with an implementation
 * with an other framework without modifying the rest of the program.
 * @author Christopher Olbertz
 *
 */
public interface OntologyOperations {
	/**
	 * Determines the class hierarchy in the ontology stored for
	 * processing starting with the class Thing.
	 * @return The root class of the ontology.
	 */
	public OntologyClass getHierarchy();
    /**
     * Determines the label of an OWL class. If there is no label, 
     * the methods returns the URI of the class.
     * @param owlClass The OWL class whose label is determined.
     * @return The label or the URI of the class if there is no label.
     */
	// DELETE_ME Kann ich vielleicht noch brauchen.
	//public String getLabelFor(@Nonnull final OWLClass clazz);
	/**
	 * Extracts the object property from the base ontology.
	 * @return The list with the datatype properties.
	 */
	DatatypePropertyCollection extractDatatypeProperties();
	/**
	 * Determines the class hierarchy in the ontology stored for
	 * processing starting with the given class.
	 * @param ontologyClass The name of the class to start with. It has not to be the class Thing.
	 * @return The root class of the ontology.
	 */
	OntologyClass getHierarchy(final String informationPackageClassName);
	/**
	 * Starts the processing of the ontology. Has to be called as the first method before another
	 * method of this class. It is not done by the constructor because administration by Spring is
	 * not possible if the constructor gets a parameter.
	 * @param fileWithOntology The file with the ontology.
	 */
	void startBaseOntologyProcessing(final File fileWithOntology);
	/**
	 * Saves the ontology on the hard disk. The original ontology file is updated with the 
	 * modifications.
	 */
	void saveOntology();
	/**
	 * Gets a collection with all individual in the base ontology.
	 * @param ontologyClassesMap Contains all classes in the base ontology.
	 * @return The individuals found in the base ontology.
	 */
	OntologyIndividualCollection getIndividualsFromOntology(final OntologyClassesMap ontologyClassesMap);
	/**
	 * Extracts the object property from the base ontology.
	 * @return The list with the object properties.
	 */
	ObjectPropertyCollection extractObjectProperties(final OntologyClassesMap ontologyClassesMap);
	/**
	 * Sets a reference to the factory that creates the model objects.
	 * @param ontologyModelFactory A reference to the factory that creates the model objects.
	 */
	void setOntologyModelFactory(final OntologyModelFactory ontologyModelFactory);
	/**
	 * Sets a reference to the factory that creates the properties.
	 * @param ontologyModelFactory A reference to the factory that creates the properties.
	 */
	void setPropertyFactory(final PropertyFactory propertyFactory);
	/**
	 * Validates an individual with the help of the reasoner.
	 * @param ontologyIndividual The individual that should be validated.
	 * @throws OntologyReasonerException if the individual contains values that could not be verified by the reasoner.
	 */
	void validateIndividual(final OntologyIndividual ontologyIndividual);
	/**
	 * Sets the file for the base ontology.
	 * @param ontologyFile The file for the base ontology.
	 */
	void setBaseOntologyFile(final File ontologyFile);
	/**
	 * Validates multiple individuals with the help of a reasoner. 
	 * @param individuals The individuals that should be validated.
	 */
	void validateIndividuals(final OntologyIndividualCollection individuals);
	/**
	 * Finds a property with the help of its iri. 
	 * @param iri The iri of the property we are looking for. 
	 * @param ontologyClassesMap Contains all classes in the ontologies. 
	 * @return Contains the found property if any was found.
	 */
	Optional<OntologyProperty> findPropertyByIri(final String iri, final OntologyClassesMap ontologyClassesMap);
	/**
	 * Sets a reference to the factory that creates the object properties.
	 * @param ontologyModelFactory A reference to the factory that creates the object properties.
	 */
	void setObjectPropertyFactory(final ObjectPropertyFactory objectPropertyFactory);
	/**
	 * Analyzes the SKOS ontology.
	 * @param fileWithSkosOntology The file that contains the SKOS ontology.
	 */
	void startSkosOntologyProcessing(final File fileWithSkosOntology);
	/**
	 * Analyzes the Premis ontology.
	 * @param fileWithPremisOntology The file that contains the Premis ontology.
	 */
	void startPremisOntologyProcessing(final File fileWithPremisOntology);
	/**
	 * Determines the hierarchy of the Premis ontology.
	 * @return The root class of the Premis ontology. It contains the whole hierarchy as sub classes.
	 */
	OntologyClass getPremisHierarchy();
	// DELETE_ME Brauche ich wahrscheinlich nicht mehr, weil SKOS keine Hierarchie in dem Sinne hat.
	/**
	 * Determines the hierarchy of the SKOS ontology.
	 * @return The root class of the SKOS ontology. It contains the whole hierarchy as sub classes.
	 */
	OntologyClass getSkosHierarchy();
	/**
	 * Extracts the annotation properties out of the ontologies.
	 * @return The found annotation properties.
	 */
	AnnotationPropertyCollection extractAnnotationProperties();
	/**
	 * Sets a reference to the object that contains the logic for processing taxonomies.
	 * @param taxonomyOperations A reference to the object that contains the logic for processing taxonomies.
	 */
	void setTaxonomyOperations(TaxonomyOperations taxonomyOperations);
}
