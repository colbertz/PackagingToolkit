package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;

/**
 * An ontology property with a short value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyShortProperty extends OntologyDatatypeProperty {
	/**
	 * Adds a max inclusive restriction.
	 * @param value The value that may not be overstepped.
	 */
	void addMaxInclusiveRestriction(final int value);
	/**
	 * Adds a min inclusive restriction.
	 * @param value The value that may not be fallen below.
	 */
	void addMinInclusiveRestriction(final int value);
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public Integer getPropertyValue();
	
	/**
	 * Creates a unsigned property value.
	 * @param propertyId The iri of the property. 
	 * @param label The label of the property.
	 * @param range The range as specified in the ontology.
	 * @return The created byte property. 
	 */
	public static OntologyShortProperty createUnsigned(final String propertyId, final String label, final String range) {
		return new OntologyShortPropertyImpl(propertyId, true, label, range); 
	}
	
	/**
	 * Creates a signed property value.
	 * @param propertyId The iri of the property. 
	 * @param label The label of the property.
	 * @param range The range as specified in the ontology.
	 * @return The created byte property. 
	 */
	public static OntologyShortProperty createSigned(final String propertyId, final String label, final String range) {
		return new OntologyShortPropertyImpl(propertyId, false, label, range); 
	}
	void setPropertyValue(int value);
	boolean isUnsigned();
}