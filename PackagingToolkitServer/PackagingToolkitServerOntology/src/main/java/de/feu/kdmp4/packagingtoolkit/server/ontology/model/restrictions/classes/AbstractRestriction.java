package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;

/**
 * An abstract superclass for restrictions.
 * @author Christopher Olbertz
 *
 */
public abstract class AbstractRestriction implements Restriction {
}