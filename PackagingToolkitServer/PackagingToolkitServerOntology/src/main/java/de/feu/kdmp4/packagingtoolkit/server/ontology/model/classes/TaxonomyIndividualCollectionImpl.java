package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;

public class TaxonomyIndividualCollectionImpl extends AbstractList implements TaxonomyIndividualCollection {
	private IndividualOfTaxonomyIterator individualOfTaxonomyIterator;
	
	@Override
	public void addTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual) {
		super.add(taxonomyIndividual);
	}
	
	@Override
	public Optional<TaxonomyIndividual> getTaxonomyIndividual(final int index) {
		TaxonomyIndividual taxonomyIndividual = (TaxonomyIndividual)super.getElement(index);
		return OntologyOptionalFactory.createOptionalWithTaxonomyIndividual(taxonomyIndividual);
	}
	
	@Override
	public int getTaxonomyIndividualCount() {
		return super.getSize();
	}
	
	@Override
	public Optional<TaxonomyIndividual> nextIndividualInTaxonomy(final Taxonomy taxonomy) {
		if (individualOfTaxonomyIterator == null) {
			individualOfTaxonomyIterator = new IndividualOfTaxonomyIterator(taxonomy);
		}
		
		if (individualOfTaxonomyIterator.hasNext()) {
			final TaxonomyIndividual taxonomyIndividual = individualOfTaxonomyIterator.next();
			return OntologyOptionalFactory.createOptionalWithTaxonomyIndividual(taxonomyIndividual);
		}
		 
		return OntologyOptionalFactory.createEmptyOptionalWithTaxonomyIndividual();
	}
	
	@Override
	public void  doWithEveryIndividual(Consumer<TaxonomyIndividual> consumer) {
		for (int i = 0; i < getSize(); i++) {
			final TaxonomyIndividual taxonomyIndividual = (TaxonomyIndividual)getElement(i);
			consumer.accept(taxonomyIndividual);
		}
	}
	
	@Override
	public boolean hasNextIndividualInTaxonomy(final Taxonomy taxonomy) {
		if (individualOfTaxonomyIterator == null) {
			individualOfTaxonomyIterator = new IndividualOfTaxonomyIterator(taxonomy);
		}
		
		return individualOfTaxonomyIterator.hasNext();
	}
	
	
	/**
	 * Iterates through the individuals of a certain taxonomy.
	 * @author Christopher Olbertz
	 *
	 */
	private class IndividualOfTaxonomyIterator implements Iterator<TaxonomyIndividual> {
		/**
		 * The current individual. This is returned by the next call of the
		 * method next().
		 */
		private TaxonomyIndividual currentIndividual;
		/**
		 * Counts the statements that have been already processed. 
		 */
		private int counter;
		/**
		 * The taxonomy we are looking for.  
		 */
		private Taxonomy taxonomy;
		
		public IndividualOfTaxonomyIterator(final Taxonomy taxonomy) {
			counter = 0;
			this.taxonomy = taxonomy;
		}
		
		@Override
		public boolean hasNext() {
			int size = getSize();
			if (counter < size) {
				for (int i = counter; i < size; i++) {
					final TaxonomyIndividual taxonomyIndividual = (TaxonomyIndividual)getElement(i);
					if (taxonomyIndividual.isInTaxonomy(taxonomy)) {
						currentIndividual = taxonomyIndividual;
						counter = i;
						return true;
					}
				}
				individualOfTaxonomyIterator = null;
				return false;
			} else {
				individualOfTaxonomyIterator = null;
				return false;
			}
		}

		@Override
		public TaxonomyIndividual next() {
			if (hasNext()) {
				counter++;
				return currentIndividual;
			} else {
				return null;
			}
		}
	}

	@Override
	public void removeIndividualsOfTaxonomy(final Taxonomy taxonomy) {
		for (int i = getSize() - 1; i >= 0; i--) {
			final TaxonomyIndividual taxonomyIndividual = (TaxonomyIndividual)getElement(i);
			if (taxonomyIndividual.isInTaxonomy(taxonomy)) {
				super.remove(taxonomyIndividual);
			}
		}
	}
}
