package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;

public class OntologyDoublePropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDoubleProperty {
	private static final double DEFAULT_VALUE = 0.0;
	private static final long serialVersionUID = -6417668866515143729L;

	
	public OntologyDoublePropertyImpl() {
	}
	
	/**
	 * Constructs a new property without a value.
	 * @param propertyId The iri of the property.
	 * @param defaultLabel The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDoublePropertyImpl(final String propertyId, final String defaultLabel, final String range) {
		super(propertyId, DEFAULT_VALUE, defaultLabel, range);
	}

	/**
	 * Constructs a new property with a value.
	 * @param propertyId The iri of the property.
	 * @param value The value of the property.
	 * @param defaultLabel The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDoublePropertyImpl(final String propertyId, final double value, final String defaultLabel, final String range) {
		super(propertyId, value, defaultLabel, range);
	}

	@Override
	public Double getPropertyValue() {
		return (Double) getValue();
	}

	@Override
	public boolean isDoubleProperty() {
		return true;
	}
	
	@Override
	public void setPropertyValue(final Object value) {
		String valueAsString = value.toString();
		if (valueAsString.contains(DECIMAL_SEPARATOR_DE)) {
			valueAsString = valueAsString.replaceAll(DECIMAL_SEPARATOR_DE, DECIMAL_SEPARATOR_EN);
		}
		super.setPropertyValue(valueAsString);
	}
}