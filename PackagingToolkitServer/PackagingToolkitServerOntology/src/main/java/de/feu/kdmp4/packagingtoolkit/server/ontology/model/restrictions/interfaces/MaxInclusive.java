package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces;

/**
 * Defines a max inclusive restriction. This means that a value has to be below or equal the value
 * of this restriction.
 * @author Christopher Olbertz
 *
 */
public interface MaxInclusive extends Restriction {
}
