package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;


/**
 * Encapsulates the creation of objects of {@link java.util.Optional}. The reason for this class is to remove the need
 * of knowing how Optionals are created. 
 * @author Christopher Olbertz
 *
 */
public class OntologyOptionalFactory {
	/**
	 * Creates an optional with a datatype property in an ontology.
	 * @param datatypeProperty The datatype property that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyDatatypeProperty> createOntologyDatatypePropertyOptional(
			final OntologyDatatypeProperty datatypeProperty) {
		return Optional.of(datatypeProperty);
	}
	
	/**
	 * Creates an empty optional for a datatype property.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyDatatypeProperty> createEmptyOntologyDatatypePropertyOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a property in an ontology. It is not important if it is a datatype or a object
	 * property
	 * @param property The property that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyProperty> createOntologyPropertyOptional(
			final OntologyProperty property) {
		return Optional.of(property);
	}
	
	/**
	 * Creates an empty optional for a property.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyProperty> createEmptyOntologyPropertyOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an empty optional for an annotation property.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyAnnotationProperty> createEmptyOntologyAnnoationPropertyOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a map with a collection with taxonomy individuals.
	 * @param taxonomyIndividuals The collection that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TaxonomyIndividualCollection> createTaxonomyIndividualCollectionOptional(
			final TaxonomyIndividualCollection taxonomyIndividuals) {
		if (taxonomyIndividuals == null) {
			return createEmptyTaxonomyIndividualCollectionOptional();
		} else {
			return Optional.of(taxonomyIndividuals);
		}
	}
	
	/**
	 * Creates an empty optional with a map with a collection with taxonomy individuals.
	 * @return The created empty Optional.
	 */
	public final static Optional<TaxonomyIndividualCollection> createEmptyTaxonomyIndividualCollectionOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an annotation property. 
	 * @param property The property that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyAnnotationProperty> createOntologyAnnotationPropertyOptional(
			final OntologyAnnotationProperty ontologyAnnotationProperty) {
		return Optional.of(ontologyAnnotationProperty);
	}

	/**
	 * Creates an optional with a taxonomy.
	 * @param iri The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<Taxonomy> createOptionalWithTaxonomy(final Taxonomy taxonomy) {
		if (taxonomy != null) {
			return Optional.of(taxonomy);
		}
		return createEmptyOptionalWithTaxonomy();
	}
	
	/**
	 * Creates an empty optional for a taxonomy.
	 * @return The created empty Optional.
	 */
	public final static Optional<Taxonomy> createEmptyOptionalWithTaxonomy() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an individual in a taxonomy.
	 * @param iri The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TaxonomyIndividual> createOptionalWithTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual) {
		if (taxonomyIndividual != null) {
			return Optional.of(taxonomyIndividual);
		}
		return createEmptyOptionalWithTaxonomyIndividual();
	}
	
	/**
	 * Creates an empty optional for an individual in a taxonomy.
	 * @return The created empty Optional.
	 */
	public final static Optional<TaxonomyIndividual> createEmptyOptionalWithTaxonomyIndividual() {
		return Optional.empty();
	}
}
