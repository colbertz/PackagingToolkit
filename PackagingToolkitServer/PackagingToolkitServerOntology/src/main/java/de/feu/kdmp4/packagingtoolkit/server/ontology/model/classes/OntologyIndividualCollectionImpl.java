package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

public class OntologyIndividualCollectionImpl extends AbstractList implements OntologyIndividualCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;
	private OntologyIndividualIterator ontologyIndividualIterator;
	
	@Override
	public void addIndividual(final OntologyIndividual individual) {
			super.add(individual);
	}
	
	@Override
	public OntologyIndividual getIndividual(final int index) {
		return (OntologyIndividual)super.getElement(index);
	}
	
	@Override
	public OntologyIndividual getIndividualByUuid(final Uuid uuid) {
		OntologyIndividual individual = null;
		int i = 0;
		boolean found = false;
		
		while (found == false && i < getIndiviualsCount()) {
			individual = getIndividual(i);
			Uuid individualUuid = individual.getUuid();
			
			if (individualUuid.equals(uuid)) {
				found = true;
			}
			
			i++;
		}
				
		return individual;
	}
	
	
	@Override
	public OntologyIndividualCollection clone() throws CloneNotSupportedException {
		final OntologyIndividualCollection ontologyIndividualList = OntologyModelFactory.createEmptyOntologyIndividualList();
		
		for (int i = 0; i < getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = getIndividual(i).clone();
			ontologyIndividualList.addIndividual(ontologyIndividual);
		}
		
		return ontologyIndividualList;
	}
	
	@Override
	public int getIndiviualsCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}

	@Override
	public boolean isNotEmpty() {
		return !super.isEmpty();
	}

	@Override
	public void removeIndividual(final int index) {
		final OntologyIndividual individual = getIndividual(index);
		super.remove(individual);
	}
	
	@Override
	public void removeIndividual(final OntologyIndividual ontologyIndividual) {
		super.remove(ontologyIndividual);
	}

	@Override
	public void appendIndividualList(final OntologyIndividualCollection individualList) {
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual individual = individualList.getIndividual(i);
			addIndividual(individual);
		}
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public List<OntologyIndividual> toList() {
		final List<OntologyIndividual> ontologyIndiduals = new ArrayList<>();
		
		for (int i = 0; i < getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = getIndividual(i);
			ontologyIndiduals.add(ontologyIndividual);
		}
		
		return ontologyIndiduals;
	}
	
	@Override
	public String toString() {
		final StringBuffer stringBuffer = new StringBuffer();
		
		for (int i = 0; i < getIndiviualsCount(); i++) {
			OntologyIndividual ontologyIndividual = getIndividual(i);
			stringBuffer.append(ontologyIndividual.toString()). append(PackagingToolkitConstants.LINE_FEED);
		}
		
		return stringBuffer.toString();
	}
	
	@Override
	public TripleStatementCollection toStatements() {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		for (int i = 0; i < getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = getIndividual(i);
			final Optional<TripleStatementCollection> optionalWithTripleStatements = ontologyIndividual.toTripleStatements();
			if (optionalWithTripleStatements.isPresent()) {
				final TripleStatementCollection individualsTripleStatements = optionalWithTripleStatements.get();
				tripleStatements.addStatements(individualsTripleStatements);	
			}
		}
		
		return tripleStatements;
	}
	
	@Override
	public OntologyIndividualCollection getIndividualsOfInformationPackage(final Uuid uuidOfInformationPackage) {
		final OntologyIndividualCollection individualList = new OntologyIndividualCollectionImpl();
		final UuidOfInformationPackageIterator iterator = new UuidOfInformationPackageIterator(uuidOfInformationPackage);
		
		while (iterator.hasNext()) {
			final OntologyIndividual nextIndividual = iterator.next();
			individualList.addIndividual(nextIndividual);
		}
		
		return individualList;
	}
	
	@Override
	public boolean hasNextOntologyIndividual() {
		if (ontologyIndividualIterator == null) {
			ontologyIndividualIterator = new OntologyIndividualIterator();
		}
		return ontologyIndividualIterator.hasNext();
	}
	
	@Override
	public OntologyIndividual getNextOntologyIndividual() {
		if (ontologyIndividualIterator == null) {
			ontologyIndividualIterator = new OntologyIndividualIterator();
		}
		return ontologyIndividualIterator.next();
	}
	
	/**
	 * Iterates trough the individuals and looks for all individuals that belong to a certain information package.
	 * @author Christopher Olbertz 
	 *
	 */
	private class UuidOfInformationPackageIterator implements Iterator<OntologyIndividual> {
		/**
		 * The individual that is returned by the next call of next(). null if there is not another individual
		 * that belongs to the desired information package.
		 */
		private OntologyIndividual currentIndividual;
		/**
		 * Counts the individuals. Is necessary to determine if the end of the list is reached.
		 */
		private int counter;
		/**
		 * The uuid of the information package whose individuals we are looking for. 
		 */
		private Uuid uuidOfInformationPackage;
		
		/**
		 * Creates an iterator that looks for individuals contained in the information package with a certain uuid.
		 * @param uuidOfInformationPackage The uuid of the information package.
		 */
		public UuidOfInformationPackageIterator(final Uuid uuidOfInformationPackage) {
			counter = 0;
			this.uuidOfInformationPackage = uuidOfInformationPackage;
		}
		
		/**
		 * Checks if there is any other indiviual that belongs to this information package.
		 * @return True if there is another individual, false otherwise.-
		 */
		@Override
		public boolean hasNext() {
			int size = getIndiviualsCount();
			if (counter < size) {
				for (int i = counter; i < size; i++) {
					OntologyIndividual individual = getIndividual(i);
					Uuid uuidOfIndividual = individual.getUuid();
					if (uuidOfIndividual.equals(uuidOfInformationPackage)) {
						currentIndividual = individual;
						counter = i + 1;
						return true;
					}
				}
			}
			return false;
		}

		/**
		 * Returns the next individual assigned to the information package with the given uuid.
		 * @return The next individual.
		 */
		@Override
		public OntologyIndividual next() {
			if (hasNext()) {
				return currentIndividual;
			} else {
				return null;
			}
		}
	}

	@Override
	public void replaceIndividual(final OntologyIndividual newOntologyIndividual) {
		Uuid uuidOfNewIndividual = newOntologyIndividual.getUuid();
		
		int counter = 0;
		boolean individualFound = false;
		
		while(counter < getIndiviualsCount() && individualFound == false) {
			final OntologyIndividual ontologyIndividual = getIndividual(counter);
			if (ontologyIndividual.getUuid().equals(uuidOfNewIndividual)) {
				remove(counter);
				addObjectAt(counter, newOntologyIndividual);
				individualFound = true;
			}
			counter++;
		}
	}

	@Override
	public boolean containsIndividual(final OntologyIndividual ontologyIndividual) {
		for (int i = 0; i < getIndiviualsCount(); i++) {
			final OntologyIndividual currentIndividual = getIndividual(i);
			if (currentIndividual.equals(ontologyIndividual)) {
				return true;
			}
		} 
		
		return false;
	}
	
	/**
	 * Iterates through all individuals in this collection.
	 * @author Christopher Olbertz
	 *
	 */
	private class OntologyIndividualIterator implements Iterator<OntologyIndividual> {
		private OntologyIndividual currentOntologyIndividual;
		private int counter;
		
		@Override
		public boolean hasNext() {
			if (counter < getIndiviualsCount()) {
				currentOntologyIndividual = getIndividual(counter);
				return true;
			} else {
				return false;
			}
		}

		@Override
		public OntologyIndividual next() {
			if (hasNext())  {
				counter++;
				return currentOntologyIndividual;
			}
			else {
				return null;
			}
		}
	}
}
