package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

/**
 * Represents an property in an ontology that is a String.
 * @author Christopher Olbertz
 *
 */
public interface OntologyStringProperty extends OntologyDatatypeProperty {
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public String getPropertyValue();
	/**
	 * Compares the values of two properties.
	 * @param ontologyProperty The property we want to compare with.
	 * @return True if the both property have the same value, false otherwise.
	 */
	int compareTo(OntologyStringProperty ontologyProperty);
}
