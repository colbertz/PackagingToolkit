package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;

/**
 * A collection that contains objects of 
 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty}. 
 * @author Christopher Olbertz
 *
 */
public interface AnnotationPropertyCollection {
	/**
	 * Adds an annotation property.
	 * @param ontologyAnnotationProperty The annotation property that should be added to the collection.
	 */
	void addAnnotationProperty(final OntologyAnnotationProperty ontologyAnnotationProperty);
	/**
	 * Returns an annotation property from the collection at the given index.
	 * @param index The index of the element we are looking for.
	 * @return The desired annotation property.
	 */
	OntologyAnnotationProperty getAnnotationPropertyAt(final int index);
	/**
	 * Determines the number of annotation properties in the collection.
	 * @return The number of annotation properties. 
	 */
	int getAnnotationPropertyCount();

}
