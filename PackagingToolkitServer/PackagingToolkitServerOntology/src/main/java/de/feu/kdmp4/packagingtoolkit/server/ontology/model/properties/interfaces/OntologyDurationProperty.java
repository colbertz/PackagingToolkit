package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;

/**
 * An ontology property with a duration value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyDurationProperty extends OntologyDatatypeProperty {
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public OntologyDuration getPropertyValue();
}
