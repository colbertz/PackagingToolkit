package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.IrisAndTheirAnnotationPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;

public class IrisAndTheirAnnotationPropertiesHashMapImpl implements IrisAndTheirAnnotationPropertiesMap{
	/**
	 * Contains the iri of the annotation properties as keys and the objects as values.
	 */
	private Map<String, OntologyAnnotationProperty> annotationPropertyMap;
	
	public IrisAndTheirAnnotationPropertiesHashMapImpl() {
		annotationPropertyMap = new HashMap<>();
	}
	
	@Override
	public void addAnnotationProperty(final OntologyAnnotationProperty ontologyAnnotationProperty) {
		final String iri = ontologyAnnotationProperty.getIri();
		annotationPropertyMap.put(iri, ontologyAnnotationProperty);
	}
	
	@Override
	public Optional<OntologyAnnotationProperty> getAnnotationProperty(final String iri) {
		final OntologyAnnotationProperty ontologyAnnotationProperty = annotationPropertyMap.get(iri);
		return OntologyOptionalFactory.createOntologyAnnotationPropertyOptional(ontologyAnnotationProperty);
	}
}
