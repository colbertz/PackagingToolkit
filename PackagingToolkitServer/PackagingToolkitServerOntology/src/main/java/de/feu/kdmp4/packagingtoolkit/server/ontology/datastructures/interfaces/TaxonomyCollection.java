package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;

/**
 * A collection that contains taxonomies.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomyCollection {
	/**
	 * Adds a taxonomy to the collection.
	 * @param taxonomy The taxonomy that should be added.
	 */
	void addTaxonomy(Taxonomy taxonomy);
	/**
	 * Determines a taxonomy in the collection at a certain position.
	 * @param index The position we are looking at.
	 * @return The found taxonomy or an empty optional.
	 */
	Optional<Taxonomy> getTaxonomy(int index);
	/**
	 * Determines the number of taxonomies in this collection.
	 * @return The number of taxonomies in this collection.
	 */
	int getTaxonomyCount();
	/**
	 * Finds a taxonomy in the collection by it namespace.
	 * @param namespace The namespace of the taxonomy we are looking for.
	 * @return The found taxonomy or an empty optional.
	 */
	Optional<Taxonomy> findTaxonomyByNamespace(Namespace namespace);

}
