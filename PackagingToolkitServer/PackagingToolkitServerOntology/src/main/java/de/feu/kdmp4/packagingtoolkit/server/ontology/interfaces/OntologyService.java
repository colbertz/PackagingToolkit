package de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces;

 import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.OntologyConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.server.triplestore.facades.OntologyTripleStoreFacade;

public interface OntologyService {
	/**
	 * Looks in the hierarchy of the classes for the classes with the names 
	 * in the given string list. 
	 * @param stringListResponse Contains the names of the classes we are
	 * looking for.
	 * @return Contains the result classes with their sub tree.
	 */
	OntologyClassResponse getHierarchy(final StringListResponse stringListResponse);
	/**
	 * Determines the class hierarchy in the Premis ontology. 
	 * @return A super class that contains the classes of the Premis ontology as sub classes.
	 */
	OntologyClassResponse getPremisHierarchy();
	/**
	 * Determines the properties of a class. 
	 * @param classIri The iri of the class whose properties we are interested in.
	 * @return A class that contains the found properties
	 */
	OntologyClassResponse getPropertiesOfClass(final String classIri);
	/**
	 * Looks for individuals that are predefined in the base ontology by their uuids.
	 * @param ontologyIndividualList The list contains empty individuals. They only contain the uuids. The method 
	 * looks for the individuals and writes their values in the corresponding object in the list.
	 * @return A collection that contains values for the individuals that could be filled. The individuals whose 
	 * uuids could not be found in the ontology do not contain any values.
	 */
	OntologyIndividualCollection findIndividualByUuidsFromOntologies(final OntologyIndividualCollection ontologyIndividualList);
	/**
	 * Sets a reference to the factory object that creates response objects for sending via http.
	 * @param serverResponseFactory A reference to the factory object.
	 */
	void setServerResponseFactory(final ServerResponseFactory serverResponseFactory);
	/**
	 * Sets a reference to the cache that contains all data from the ontologies.
	 * @param ontologyCache The cache object.
	 */
	void setOntologyCache(final OntologyCache ontologyCache);
	/**
	 * Sets a reference to the object that contains all methods from the TripleStore module the Ontology module 
	 * is allowed to access.
	 * @param ontologyTripleStoreFacade The facade object.
	 */
	void setOntologyTripleStoreFacade(final OntologyTripleStoreFacade ontologyTripleStoreFacade);
	/**
	 * Finds all individuals of a class that are in a specific information package.
	 * @param iriOfClass The iri of the class whose individuals we want to get.
	 * @param uuidOfInformationPackage The uuid of the information package whose individuals we want to get.  
	 * @return The found individuals.
	 */
	IndividualListResponse getIndiviualsByOntologyClassAndInformationPackage(final String iriOfClass,
			final String uuidOfInformationPackage);
	/**
	 * Sets a reference to the factory that creates the model objects that are related to the ontologies.
	 * @param ontologyModelFactory The factory object.
	 */
	void setOntologyModelFactory(final OntologyModelFactory ontologyModelFactory);
	/**
	 * Finds the individuals of an information package.
	 * @param uuidOfInformationPackageAsString The uuid of the information package.
	 * @return The found individuals.
	 */
	OntologyIndividualCollection findIndiviualsByInformationPackage(final String uuidOfInformationPackageAsString);
	/**
	 * Reads the individuals that are already defined in the base ontology.
	 * @param iriOfClass The iri of the class whose individuals we are looking for.
	 * @return The predefined individuals of the class with the iri iriOfClass.
	 */
	IndividualListResponse findPredefinedIndividualsByClass(final String iriOfClass);
	/**
	 * Looks for the individuals of a certain class that are saved on the server. 
	 * @param iri The iri of the class.
	 * @return The individuals of the class with this iri.
	 */
	IndividualListResponse findIndividualsByClass(final Iri iriOfClass);
	/**
	 * Determines the hierarchy of the classes in the base ontology starting with the 
	 * Information_Package class.
	 * @return The hierarchy of the classes in the base ontology.
	 */
	OntologyClassResponse getHierarchy();
	/**
	 * Determines the hierarchy of the classes in the SKOS ontology.
	 * @return The hierarchy of the classes in the SKOS ontology.
	 */
	OntologyClassResponse getSkosHierarchy();
	/**
	 * Finds all individuals of a certain class. 
	 * @param iriOfClass The iri of the class whose individuals we are looking for.
	 * @return The individuals found in the triple store.
	 */
	IndividualListResponse getIndiviualsByOntologyClass(final String iriOfClass);
	/**
	 * Determines the class that is the starting class for taxonomies from the base ontology.
	 * @return The class that marks the starting point for the taxonomies.
	 */
	OntologyClassResponse getTaxonomyStartClass();
	/**
	 * Sets a reference to the class that contains the interface to the configuration module.
	 * @param ontologyConfigurationFacade A reference to the class that contains the interface to the configuration module.
	 */
	void setOntologyConfigurationFacade(OntologyConfigurationFacade ontologyConfigurationFacade);
	/**
	 * Forces the initialization of the ontology cache. Can be used if a new base ontology has been uploaded.
	 */
	void updateOntologyCache();
	/**
	 * Determines individuals from the ontology with the help from their uuids. 
	 * @param uuidsOfIndividuals The uuids of the individuals we are looking for. 
	 * @return Contains the individuals that were found with the help from the uuids.
	 */
	OntologyIndividualCollection findIndividualByUuidsFromOntologies(UuidList uuidsOfIndividuals);
}
