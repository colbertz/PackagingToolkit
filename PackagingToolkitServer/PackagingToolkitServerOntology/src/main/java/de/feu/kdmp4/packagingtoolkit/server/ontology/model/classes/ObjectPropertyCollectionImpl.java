package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.io.Serializable;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;

/**
 * Implements the interface {@linkplain de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection}
 * with a list.
 * @author Christopher Olbertz
 *
 */
public class ObjectPropertyCollectionImpl extends AbstractList implements ObjectPropertyCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;

	public ObjectPropertyCollectionImpl() {
	}
	
	@Override
	public void addObjectProperty(final OntologyObjectProperty property) {
			super.add(property);
	}
	
	@Override
	public OntologyObjectProperty getObjectProperty(final int index) {
		return (OntologyObjectProperty)super.getElement(index);
	}
	
	@Override
	public int getPropertiesCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}
}
