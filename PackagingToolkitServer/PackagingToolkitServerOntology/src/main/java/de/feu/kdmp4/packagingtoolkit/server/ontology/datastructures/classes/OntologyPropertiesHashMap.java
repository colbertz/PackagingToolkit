package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

public class OntologyPropertiesHashMap implements OntologyPropertiesMap {
	/**
	 * Contains the iri of the property as key and the property object itself as value. 
	 */
	private Map<String, OntologyProperty> propertiesMap;
	
	public OntologyPropertiesHashMap() {
		propertiesMap = new HashMap<>();
	}
	
	@Override
	public void addProperty(final OntologyProperty ontologyProperty) {
		final String propertyName = ontologyProperty.getPropertyId();
		propertiesMap.put(propertyName, ontologyProperty);
	}
	
	@Override
	public OntologyProperty cloneOntologyProperty(final String propertyName) {
		final OntologyProperty ontologyProperty = propertiesMap.get(propertyName);
		return ontologyProperty.cloneProperty();
	}
	
	@Override
	public int getPropertiesCount() {
		return propertiesMap.size();
	}
}
