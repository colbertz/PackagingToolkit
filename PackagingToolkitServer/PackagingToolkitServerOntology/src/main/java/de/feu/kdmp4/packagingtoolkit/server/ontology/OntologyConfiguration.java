package de.feu.kdmp4.packagingtoolkit.server.ontology;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDatePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDateTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDurationPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyFloatPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyLongPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.MaxInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.MinInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.RestrictionCollection;

//@Configuration
public class OntologyConfiguration {
	@Bean(name = "datatypePropertyList")
	@Scope("prototype")
	public DatatypePropertyCollection getDatatypePropertyList() {
		return new DatatypePropertyCollectionImpl();
	}
	
	@Bean(name = "objectPropertyList")
	@Scope("prototype")
	public ObjectPropertyCollection getObjectPropertyList() {
		return new ObjectPropertyCollectionImpl();
	}
	
	@Bean(name = "OntologyBooleanPropertyImpl")
	@Scope("prototype")
	public OntologyBooleanProperty getOntologyBooleanProperty() {
		return new OntologyBooleanPropertyImpl();
	}
	
	@Bean(name = "OntologyBytePropertyImpl")
	@Scope("prototype")
	public OntologyByteProperty getOntologyByteProperty() {
		return new OntologyBytePropertyImpl();
	}
	
	@Bean(name = "OntologyClassImpl")
	@Scope("prototype")
	public OntologyClass getOntologyClass(DatatypePropertyCollection datatypePropertyList, ObjectPropertyCollection objectPropertyList) {
		return new OntologyClassImpl(datatypePropertyList, objectPropertyList);
	}
	
	@Bean(name = "ontologyCache")
	public OntologyCache getOntologyCache() {
		return new OntologyCache();
	}
	
	@Bean(name = "OntologyDatePropertyImpl")
	@Scope("prototype")
	public OntologyDateProperty getOntologyDateProperty() {
		return new OntologyDatePropertyImpl();
	}
	
	@Bean(name = "OntologyDateTimePropertyImpl")
	@Scope("prototype")
	public OntologyDateTimeProperty getOntologyDateTimeProperty() {
		return new OntologyDateTimePropertyImpl();
	}
	
	@Bean(name = "OntologyDoublePropertyImpl")
	@Scope("prototype")
	public OntologyDoubleProperty getOntologyDoubleProperty() {
		return new OntologyDoublePropertyImpl();
	}
	
	@Bean(name = "OntologyDurationPropertyImpl")
	@Scope("prototype")
	public OntologyDurationProperty getOntologyDurationProperty() {
		return new OntologyDurationPropertyImpl();
	}
	
	@Bean(name = "OntologyFloatPropertyImpl")
	@Scope("prototype")
	public OntologyFloatProperty getOntologyFloatProperty() {
		return new OntologyFloatPropertyImpl();
	}
	
	@Bean(name = "OntologyIntegerPropertyImpl")
	@Scope("prototype")
	public OntologyIntegerProperty getOntologyIntegerProperty() {
		return new OntologyIntegerPropertyImpl();
	}
	
	@Bean(name = "OntologyLongPropertyImpl")
	@Scope("prototype")
	public OntologyLongProperty getOntologyLongProperty() {
		return new OntologyLongPropertyImpl();
	}
	
	@Bean(name = "MaxInclusiveImpl")
	@Scope("prototype")
	public MaxInclusive getMaxInclusive() {
		return new MaxInclusiveImpl();
	}
	
	@Bean(name = "MinInclusiveImpl")
	@Scope("prototype")
	public MinInclusive getMinInclusive() {
		return new MinInclusiveImpl();
	}
	
	@Bean(name = "restrictionList")
	@Scope("prototype")
	public RestrictionCollection getRestrictionList() {
		return new RestrictionCollectionImpl();
	}
	
	@Bean(name = "OntologyShortPropertyImpl")
	@Scope("prototype")
	public OntologyShortProperty getOntologyShortProperty() {
		return new OntologyShortPropertyImpl();
	}
	
	@Bean(name = "OntologyStringPropertyImpl")
	@Scope("prototype")
	public OntologyStringProperty getOntologyStringProperty() {
		return new OntologyStringPropertyImpl();
	}
	
	@Bean(name = "OntologyTimePropertyImpl")
	@Scope("prototype")
	public OntologyTimeProperty getOntologyTimeProperty() {
		return new OntologyTimePropertyImpl();
	}
}
