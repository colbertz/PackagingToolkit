package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;

public class OntologyFloatPropertyImpl extends OntologyDatatypePropertyImpl implements OntologyFloatProperty {
	private static final float DEFAULT_VALUE = 0.0f;
	private static final long serialVersionUID = -6417668866515143729L;

	/**
	 * Creates a new property.
	 */
	public OntologyFloatPropertyImpl() {
	}
	
	/**
	 * Constructs a new property without a value.
	 * @param propertyId The iri of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyFloatPropertyImpl(final String propertyId, final String defaultLabel, final String range) {
		super(propertyId, DEFAULT_VALUE, defaultLabel, range);
	}

	/**
	 * Constructs a new property with a value.
	 * @param propertyId The iri of the property.
	 * @param value The value of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyFloatPropertyImpl(final String propertyId, final double value, final String defaultLabel, final String range) {
		super(propertyId, value, defaultLabel, range);
	}
	
	@PostConstruct
	public void initialize() {
		addMaxInclusiveRestriction(Float.MAX_VALUE);
		addMinInclusiveRestriction(Float.MIN_VALUE);
	}

	@Override
	public void addMaxInclusiveRestriction(final float value) {
		final RestrictionValue<Float> restrictionValue = new RestrictionValue<>(value);
		final MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public void addMinInclusiveRestriction(final float value) {
		final RestrictionValue<Float> restrictionValue = new RestrictionValue<>(value);
		final MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public Float getPropertyValue() {
		return (Float) getValue();
	}

	@Override
	public boolean isFloatProperty() {
		return true;
	}
	
	@Override
	public void setPropertyValue(final Object value) {
		String valueAsString = value.toString();
		if (valueAsString.contains(DECIMAL_SEPARATOR_DE)) {
			valueAsString = valueAsString.replaceAll(DECIMAL_SEPARATOR_DE, DECIMAL_SEPARATOR_EN);
		}
		super.setPropertyValue(valueAsString);
	}
}
