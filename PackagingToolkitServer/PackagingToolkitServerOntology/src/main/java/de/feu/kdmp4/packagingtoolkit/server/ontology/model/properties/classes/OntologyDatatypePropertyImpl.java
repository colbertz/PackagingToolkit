package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.RestrictionFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.RestrictionCollection;

public abstract class OntologyDatatypePropertyImpl extends OntologyPropertyImpl implements OntologyDatatypeProperty {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7678084367113086933L;
	/**
	 * The restrictions that are valid for this property.
	 */
	@Autowired
	private RestrictionCollection restrictionList;
	@Autowired
	protected RestrictionFactory restrictionFactory;
	
	public OntologyDatatypePropertyImpl() {
	}
	
	public OntologyDatatypePropertyImpl(String propertyId, Object value, 
			String label, String range) {
		super(propertyId, value, label, range);
		
	}
	
	@PostConstruct
	public void initialize() {
		restrictionList = restrictionFactory.createRestrictionList();
	}
	
	@Override
	public void checkRestrictions(RestrictionValue value) {
		int i = 0;
		
		while (i < restrictionList.getRestrictionsCount()) {
			Restriction restriction = restrictionList.getRestriction(i);
			restriction.checkRestriction(value);
			i++;
		}
	}
	
	@Override
	public boolean areRestrictionsSatisfied(RestrictionValue value) {
		boolean isValid = true;
		int i = 0;
		
		while (i < restrictionList.getRestrictionsCount() && isValid == true) {
			Restriction restriction = restrictionList.getRestriction(i);
			isValid = restriction.isSatisfied(value);
			i++;
		}
		
		return isValid;
	}
	
	@Override
	public Restriction getRestrictionAt(int index) {
		return restrictionList.getRestriction(index);
	}
	
	@Override
	public void setPropertyValue(final Object value) {
		if (value != null) {
			String valueAsString = value.toString();
			
			if (isBooleanProperty()) { 
				Boolean inputAsBoolean = Boolean.parseBoolean(valueAsString);
				setValue(inputAsBoolean);
			} else if (isByteProperty()) { 
				Short inputAsShort = Short.parseShort(valueAsString);
				setValue(inputAsShort);
			} else if (isDateProperty()) { 
				if (value instanceof LocalDate) {
					LocalDate inputAsLocalDate = (LocalDate)value;
					setValue(inputAsLocalDate);
				} else {
					String inputAsString = value.toString();
					LocalDate inputAsLocalDate = LocalDate.parse(inputAsString);
					setValue(inputAsLocalDate);
				}
			} else if (isDateTimeProperty()) {
				if (value instanceof LocalDateTime) {
					LocalDateTime inputAsLocalDateTime = (LocalDateTime)value;
					setValue(inputAsLocalDateTime);
				} else {
					String inputAsString = value.toString();
					LocalDateTime inputAsLocalDateTime = LocalDateTime.parse(inputAsString);
					setValue(inputAsLocalDateTime);
				}
			} else if (isDoubleProperty()){
				Double inputAsDouble = Double.parseDouble(valueAsString);
				setValue(inputAsDouble);
			} else if (isDurationProperty()) {
				OntologyDuration valueAsDuration = OntologyDuration.fromString(valueAsString);
				setValue(valueAsDuration);
			} else if (isIntegerProperty()) {
				Long inputAsLong = Long.parseLong(valueAsString);
				setValue(inputAsLong);
			} else if (isFloatProperty()){
				Float inputAsFloat = Float.parseFloat(valueAsString);
				setValue(inputAsFloat);
			} else if (isLongProperty()){
				BigInteger inputAsBigIntager = new BigInteger(valueAsString);
				setValue(inputAsBigIntager);
			} else if (isShortProperty()){
				Integer inputAsShort = Integer.parseInt(valueAsString);
				setValue(inputAsShort);
			} else if (isStringProperty()){
				setValue(valueAsString);
			} else if (isTimeProperty()) { 
				if (value instanceof LocalTime) {
					LocalTime inputAsLocalTime = (LocalTime)value;
					setValue(inputAsLocalTime);
				} else {
					String inputAsString = value.toString();
					LocalTime inputAsLocalTime = LocalTime.parse(inputAsString);
					setValue(inputAsLocalTime);
				}
			}
		}
	}
	
	@Override
	public boolean isBooleanProperty() {
		return false;
	}
	
	@Override
	public boolean isByteProperty() {
		return false;
	}

	@Override
	public boolean isDateProperty() {
		return false;
	}

	@Override
	public boolean isDateTimeProperty() {
		return false;
	}
	
	@Override
	public boolean isDoubleProperty() {
		return false;
	}
	
	@Override
	public boolean isDurationProperty() {
		return false;
	}
	
	@Override
	public boolean isFloatProperty() {
		return false;
	}

	@Override
	public boolean isIntegerProperty() {
		return false;
	}
	
	@Override
	public boolean isLongProperty() {
		return false;
	}
	
	@Override
	public boolean isShortProperty() {
		return false;
	}
	
	@Override
	public boolean isStringProperty() {
		return false;
	}
	
	@Override
	public boolean isTimeProperty() {
		return false;
	}
	
	// ********* Protected methods **********
	/**
	 * Adds a restriction to the property. Is only be used by the subclasses.
	 * Every subclass defines add methods for every restriction type. This
	 * methods call the method addRestriction(). Therefore, the subclasses
	 * do not need access to restrictionList. 
	 * @param restriction The restriction to add. 
	 */
	protected void addRestriction(Restriction restriction) {
		restrictionList.addRestriction(restriction);
	}
}
