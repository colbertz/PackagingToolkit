package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.RestrictionCollection;

public class RestrictionCollectionImpl extends AbstractList implements RestrictionCollection {
	@Override
	public Restriction getRestriction(final int index) {
		return (Restriction)super.getElement(index);
	}
	
	@Override
	public void addRestriction(final Restriction restriction) {
		super.add(restriction);
	}

	@Override
	public int getRestrictionsCount() {
		return super.getSize();
	}
}
