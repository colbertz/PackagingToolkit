package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;

/**
 * A taxonomy is a scheme for classification of objects. It is a hierarchical classification. The taxonomy contains
 * individuals and starts with a root individual. This individual contains so called narrowers. These are individuals
 * that follow their parent individual in the taxonomy.  
 * @author Christopher Olbertz
 *
 */
public interface Taxonomy {
	/**
	 * Adds a new individual to the taxonomy. The individual is not a top level individual but has 
	 * an other individual as broader. For adding a top level individual please call the method
	 * addTopLevelIndividual().
	 * @param taxonomyIndividual The individual we want to add to the taxonomy.
	 * @param iriOfBroader The iri of the broader of the individual. 
	 */
	void addIndividualToTaxonomy(TaxonomyIndividual taxonomyIndividual, Iri iriOfBroader);
	/**
	 * Adds an individual as a top level concept to the taxonomy. This means that this individual does not
	 * have any broaders. For adding another individual, plese call the method addIndividualToTaxonomy().
	 * @param topLevelIndividual The individual that should be added as top level individual.
	 */
	void addTopConceptToTaxonomy(TaxonomyIndividual topLevelIndividual);
	/**
	 * Returns the individual the taxonomy starts with. This individual contains the top level concepts as
	 * narrower.
	 * @return The individual the taxonomy starts with.
	 */
	TaxonomyIndividual getRootIndividual();
	/**
	 * Counts the individuals in the taxonomy on all levels.
	 * @return The number of individuals in the taxonomy.
	 */
	int countTaxonomyIndividuals();
	String getTitle();
	Namespace getNamespace();
}
