package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyUuidIndividualMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;

public class OntologyUuidIndividualHashMapImpl implements OntologyUuidIndividualMap {
	/**
	 * Contains the uuid of an individual as key and the individual object itself as value. 
	 */
	private Map<Uuid, OntologyIndividual> ontologyIndividuals;
	
	public OntologyUuidIndividualHashMapImpl() {
		ontologyIndividuals = new HashMap<>();
	}
	
	@Override
	public void addIndividual(final OntologyIndividual ontologyIndividual) {
		if (ontologyIndividual != null && ontologyIndividual.getUuid() != null) {
			ontologyIndividuals.put(ontologyIndividual.getUuid(), ontologyIndividual);
		}
	}
	
	@Override
	public OntologyIndividual getIndividual(final Uuid uuidOfIndividual) {
		return ontologyIndividuals.get(uuidOfIndividual);
	}
}
