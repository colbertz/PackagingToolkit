package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;

/**
 * Represents a class in a owl file. It can contain subclasses. This
 * class is a tool for building a tree describing an owl file.
 * @author Christopher Olbertz
 */
public interface OntologyClass  extends OntologyElement {
	/**
	 * Adds a OntologyClass to the list of subclasses. 
	 * @param subClass The subclass of this class. May not be null.
	 */
	public void addSubClass(final OntologyClass subClass);

	/**
	 * Returns a subclass at a given index.
	 * @param index The index.
	 * @return The found subclass.
	 */
	public OntologyClass getSubClassAt(final int index);
	/**
	 * Gets the number of subclasses.
	 * @return The number of subclasses.
	 */
	public int getSubclassCount();
	/**
	 * Checks, if the class has any subclasses. This is the case if die count of subclasses
	 * is zero or if the only subclass is the class Nothing.
	 * @return True, if the class has subclasses expect of Nothing, false otherwise.
	 */
	public boolean hasSubclasses();
	/**
	 * Returns the index of an class in the list of subclasses. 
	 * @param subclassName The name of the class to look for.
	 * @return The index of the class in the list of subclasses.
	 */
	public int indexOfSubclass(final String subclassName);
	/**
	 * Checks if the name of the class contains a certain text.
	 * @param text The text.
	 * @return True if the name of the class contains the text, false otherwise.
	 */
	boolean classnameContains(final String text);
	/**
	 * Checks if the class is the ontology class Nothing.
	 * @return True, if the class ist the class Nothing, false otherwise.
	 */
	public boolean isNothing();
	
	/**
	 * Checks if the class is the ontology class Thing.
	 * @return True, if the class ist the class Thing, false otherwise.
	 */
	public boolean isThing();
	/**
	 * Adds a new datatype property to the class.
	 * @param datatypeProperty The property that should be added.
	 */
	void addDatatypeProperty(final OntologyDatatypeProperty datatypeProperty);
	/**
	 * Determines a datatype property at a given index. 
	 * @param index The index of the position we want to look.
	 * @return The found property.
	 */
	OntologyDatatypeProperty getDatatypeProperty(final int index);
	/**
	 * Counts the datatype properties in this class.
	 * @return The number of datatype properties.
	 */
	int getDatatypePropertiesCount();
	/**
	 * Counts the object properties in this class.
	 * @return The number of object properties.
	 */
	int getObjectPropertiesCount();
	/**
	 * Returns the namespace of this class. The namespace is the part in the iri before the # symbol.
	 * @return The namespace of this class.
	 */
	Namespace getNamespace();
	/**
	 * Returns the local name of this class. The local name  is the part in the iri after the # symbol.
	 * @return The local name of this class.
	 */
	LocalName getLocalName();
	/**
	 * Returns the full iri of the class. It consists of the namespace and the local name, separated by a # symbol.
	 * @return The full iri of the class.
	 */
	String getFullName();
	/**
	 * Creates a response object for sending via http with the values of a class.
	 * @return The object used for sending the data of the class via http.
	 */
	OntologyClassResponse toResponse();
	/**
	 * Adds several datatype properties to the class.
	 * @param datatypeProperties Contains the datatype properties that should be added to the class.
	 */
	public void addDatatypeProperties(final DatatypePropertyCollection datatypeProperties);
	/**
	 * Checks if there are datatype properties in this class.
	 * @return True if this class has datatype properties, false otherwise.
	 */
	boolean hasDatatypeProperties();
	/**
	 * Checks if there are object properties in this class.
	 * @return True if this class has object properties, false otherwise.
	 */
	boolean hasObjectProperties();
	/**
	 * Adds several object properties to the class.
	 * @param objectProperties Contains the object properties that should be added to the class.
	 */
	void addObjectProperties(final ObjectPropertyCollection objectProperties);
	/**
	 * Determines a datatype property with a given iri. 
	 * @param propertyName The iri of the property we are looking for.
	 * @return The found property.
	 */
	OntologyDatatypeProperty getDatatypeProperty(final String propertyName);
	/**
	 * Determines an object property with a given iri. 
	 * @param propertyName The iri of the property we are looking for.
	 * @return The found property.
	 */
	OntologyObjectProperty getObjectProperty(final String propertyName);
	/**
	 * Determines an object property at a given index. 
	 * @param index The index of the position we want to look.
	 * @return The found property.
	 */
	OntologyObjectProperty getObjectProperty(final int index);
	/**
	 * Adds a new object  property to the class.
	 * @param ontologyObjectProperty The property that should be added.
	 */
	void addObjectProperty(OntologyObjectProperty ontologyObjectProperty);
	/**
	 * Sets a new namespace in this class.
	 * @param namespace The new namespace.
	 */
	void setNamespace(final Namespace namespace);
}
