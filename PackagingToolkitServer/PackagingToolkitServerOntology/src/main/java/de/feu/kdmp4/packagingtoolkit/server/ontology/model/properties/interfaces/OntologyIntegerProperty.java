package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import de.feu.kdmp4.packagingtoolkit.enums.Sign;

/**
 * An ontology property with an integer value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyIntegerProperty extends OntologyDatatypeProperty {
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	Long getPropertyValue();
	/**
	 * Adds a max inclusive restriction.
	 * @param value The value that may not be overstepped.
	 */
	void addMaxInclusiveRestriction(long value);
	/**
	 * Adds a min inclusive restriction.
	 * @param value The value that may not be fallen below.
	 */
	void addMinInclusiveRestriction(long value);
	/**
	 * Determines if the property is unsigned.
	 * @return True if the property is unsigned, false otherwise. 
	 */
	boolean isUnsigned();
	/**
	 * Checks if the value may be equal zero.
	 * @return  True if the value may be equal zero, false otherwise.
	 */
	boolean isInclusiveZero();
	/**
	 * Determines how the sign is handled.
	 * @return The information about the sign.
	 */
	Sign getSign();
}
