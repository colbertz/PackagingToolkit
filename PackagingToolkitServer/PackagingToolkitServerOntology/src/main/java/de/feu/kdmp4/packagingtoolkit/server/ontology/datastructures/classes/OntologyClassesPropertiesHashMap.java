package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyPrototypeFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;


/**
 * Contains the information which properties the ontology classes have. In order to
 * store these information, this class contains a map. The key of the map are the names
 * of the ontology classes. The values are objects of {@link de.feu.kdmp4.packagingtoolkit.
 * PropertyCollection.ontology.model.interfaces.PropertyList}. They contain the properties of the ontology
 * class
 * @author Christopher Olbertz
 *
 */
public class OntologyClassesPropertiesHashMap implements OntologyClassesPropertiesMap {
	/**
	 * Contains the iri of a class as key and the datatype properties of the class as value.
	 */
	private Map<String, DatatypePropertyCollection> datatypePropertiesMap;
	/**
	 * Contains the iri of a class as key and the object properties of the class as value.
	 */
	private Map<String, ObjectPropertyCollection> objectPropertiesMap;
	
	public OntologyClassesPropertiesHashMap() {
		datatypePropertiesMap = new HashMap<>();
		objectPropertiesMap = new HashMap<>();
	}
	
	@Override
	public void addClass(final OntologyClass ontologyClass) {
		addDatatypeProperties(ontologyClass);
		addObjectProperties(ontologyClass);
	}
	
	/**
	 * Inserts the datatype properties of a class in the map. 
	 * @param ontologyClass The class whose datatype properties should be inserted in the map.
	 */
	private void addDatatypeProperties(final OntologyClass ontologyClass) {
		final String className = ontologyClass.getFullName();
		final DatatypePropertyCollection datatypeProperties = OntologyPrototypeFactory.createEmptyDatatypePropertyList(); 
		
		if (ontologyClass.hasDatatypeProperties()) {
			for (int i = 0; i < ontologyClass.getDatatypePropertiesCount(); i++) {
				OntologyDatatypeProperty ontologyProperty = ontologyClass.getDatatypeProperty(i);
				datatypeProperties.addDatatypeProperty(ontologyProperty);
			}
		}
		datatypePropertiesMap.put(className, datatypeProperties);
	}
	
	/**
	 * Inserts the object properties of a class in the map. 
	 * @param ontologyClass The class whose object properties should be inserted in the map.
	 */
	private void addObjectProperties(final OntologyClass ontologyClass) {
		final String className = ontologyClass.getFullName();
		final ObjectPropertyCollection objectProperties = OntologyPrototypeFactory.createEmptyObjectPropertyList();
		
		if (ontologyClass.hasObjectProperties()) {
			for (int i = 0; i < ontologyClass.getObjectPropertiesCount(); i++) {
				OntologyObjectProperty ontologyProperty = ontologyClass.getObjectProperty(i);
				objectProperties.addObjectProperty(ontologyProperty);
			}
		}
		objectPropertiesMap.put(className, objectProperties);
	}
	
	@Override
	public void addProperty(final String className, final OntologyProperty ontologyProperty) {
		if (ontologyProperty instanceof OntologyDatatypeProperty) {
			DatatypePropertyCollection datatypePropertyList = datatypePropertiesMap.get(className); 
			if (datatypePropertyList == null) {
				// TODO Hier noch das new wegschaffen. Gab Probleme wegen applicationContext in OntologyPrototypeFactory.
				datatypePropertyList = new DatatypePropertyCollectionImpl();
			}
			
			final OntologyDatatypeProperty ontologyDatatypeProperty = (OntologyDatatypeProperty)ontologyProperty;
			datatypePropertyList.addDatatypeProperty(ontologyDatatypeProperty);
			datatypePropertiesMap.put(className, datatypePropertyList);
		} else if (ontologyProperty instanceof OntologyObjectProperty) {
			ObjectPropertyCollection objectProperties = objectPropertiesMap.get(className);
			if (objectProperties == null) {
				objectProperties = new ObjectPropertyCollectionImpl();
			}
			
			final OntologyObjectProperty ontologyObjectProperty = (OntologyObjectProperty)ontologyProperty;
			objectProperties.addObjectProperty(ontologyObjectProperty);
			objectPropertiesMap.put(className, objectProperties);
		}
	}

	@Override
	public DatatypePropertyCollection getDatatypeProperties(final String className) {
		return datatypePropertiesMap.get(className);
	}
	
	@Override
	public ObjectPropertyCollection getObjectProperties(final String className) {
		return objectPropertiesMap.get(className);
	}
	
	@Override
	public int getDatatypePropertyListsCount() {
		return datatypePropertiesMap.size();
	}
	
	@Override
	public int getObjectPropertyListsCount() {
		return objectPropertiesMap.size();
	}
}
