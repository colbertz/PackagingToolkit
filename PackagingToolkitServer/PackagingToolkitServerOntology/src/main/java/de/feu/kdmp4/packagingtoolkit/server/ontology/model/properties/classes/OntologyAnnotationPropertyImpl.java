package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;

public class OntologyAnnotationPropertyImpl extends OntologyPropertyImpl implements OntologyAnnotationProperty {
	private static final long serialVersionUID = 1652899829611391413L;

	public OntologyAnnotationPropertyImpl(final Namespace namespace, final LocalName localName) {
		super(namespace, localName);
	}
	
	@Override
	public void setPropertyValue(final Object value) {
		super.setValue(value);
	}

	@Override
	public String getPropertyValue() {
		return getValue().toString();
	}

	@Override
	public String getIri() {
		return getNamespace() + PackagingToolkitConstants.NAMESPACE_SEPARATOR + getLocalName();
	}
}
