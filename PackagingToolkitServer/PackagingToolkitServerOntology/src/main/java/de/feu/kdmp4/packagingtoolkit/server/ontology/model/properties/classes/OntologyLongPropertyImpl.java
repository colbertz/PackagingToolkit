package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import java.math.BigInteger;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.PropertyRangeException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;

public class OntologyLongPropertyImpl extends OntologyDatatypePropertyImpl 
									  implements OntologyLongProperty {
	private static final long DEFAULT_VALUE = 0L;
	private static final long serialVersionUID = 106001928244230493L;
	private static final BigInteger SIGNED_MAX = new BigInteger(String.valueOf(Long.MAX_VALUE));
	private static final BigInteger SIGNED_MIN = new BigInteger(String.valueOf(Long.MIN_VALUE));
	private static final BigInteger UNSIGNED_MAX = BigInteger.valueOf(Long.MAX_VALUE).multiply(BigInteger.valueOf(2));
	private static final BigInteger UNSIGNED_MIN = new BigInteger("0");

	/**
	 * True if the value is unsigned. Unsigned means that there are only positive
	 * values possible and the value ranged is doubled. 
	 */
	private boolean unsigned;

	/**
	 * Creates a new property.
	 */
	public OntologyLongPropertyImpl() {
	}
	
	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param unsigned True if the value may be unsigned, false otherwise.
	 * @param defaultLabel The label that represents the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyLongPropertyImpl(final String propertyId, final boolean unsigned, final String defaultLabel, final String range) {
		super(propertyId, BigInteger.valueOf(DEFAULT_VALUE), defaultLabel, range);
		this.unsigned = unsigned;
	}
	
	/**
	 * Initializes the restrictions like specified in the constructor. 
	 */
	@PostConstruct
	public void initialize() {
		if (unsigned) {
			addMaxInclusiveRestriction(UNSIGNED_MAX);
			addMinInclusiveRestriction(UNSIGNED_MIN);
		} else {
			addMaxInclusiveRestriction(SIGNED_MAX);
			addMinInclusiveRestriction(SIGNED_MIN);
		}		
	}

	@Override
	public void addMaxInclusiveRestriction(final BigInteger value) {
		if (!checkLongRange(value)) {
			final PropertyRangeException exception = PropertyRangeException.valueNotInLongRange(value);
			throw exception;
		}
		final RestrictionValue<BigInteger> restrictionValue = new RestrictionValue<>(value);
		final MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public void addMinInclusiveRestriction(final BigInteger value) {
		if (!checkLongRange(value)) {
			final PropertyRangeException exception = PropertyRangeException.valueNotInLongRange(value);
			throw exception;
		}
		final RestrictionValue<BigInteger> restrictionValue = new RestrictionValue<>(value);
		final MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public boolean isUnsigned() {
		return unsigned;
	}
	
	@Override
	public BigInteger getPropertyValue() {
		return (BigInteger) getValue();
	}

	@Override
	public boolean isLongProperty() {
		return true;
	}

	/**
	 * Checks if a given value is in the range of long. It considers if the
	 * property is signed or unsigned.
	 * 
	 * @param value
	 *            The value to check.
	 * @return True if the value is in the range of long, false otherwise.
	 */
	private boolean checkLongRange(final BigInteger value) {
		if (unsigned) {
			int compareMin = value.compareTo(UNSIGNED_MIN);
			int compareMax = value.compareTo(UNSIGNED_MAX);
			if (compareMin != -1 && compareMax != 1) {
				return true;
			} else {
				return false;
			}
		} else {
			int compareMin = value.compareTo(SIGNED_MIN);
			int compareMax = value.compareTo(SIGNED_MAX);
			if (compareMin != -1 && compareMax != 1) {
				return true;
			} else {
				return false;
			}
		}
	}
}