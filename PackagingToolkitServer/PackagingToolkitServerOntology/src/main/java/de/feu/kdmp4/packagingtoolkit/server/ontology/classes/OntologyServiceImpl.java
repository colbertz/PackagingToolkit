package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.OntologyConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;
import de.feu.kdmp4.server.triplestore.facades.OntologyTripleStoreFacade;

/**
 * A service class for the access to 
 * {@link de.feu.kdmp4.packagingtoolkit.SerializationOntologyOperations.operations.interfaces.OntologyOperations}. 
 * @author Christopher Olbertz
 *
 */ 
public class OntologyServiceImpl implements OntologyService {
	/**
	 * A reference to the object that contains the cache with the objects from the ontologies.
	 */
	private OntologyCache ontologyCache;
	// DELETE_ME
	private ServerResponseFactory serverResponseFactory;
	/**
	 * Creates model objects for ontologies.
	 */
	private OntologyModelFactory ontologyModelFactory;
	/**
	 * Contains the methods of the TripleStore module the Ontology module is allowed to call. 
	 */
	private OntologyTripleStoreFacade ontologyTripleStoreFacade; 
	
	private OntologyConfigurationFacade ontologyConfigurationFacade;
	
	@Override
	public OntologyClassResponse getHierarchy() {
		OntologyClass rootClass = ontologyCache.getMainClass();
		OntologyClassResponse ontologyClassResponse = rootClass.toResponse();
		return ontologyClassResponse;
	}
	
	@Override
	public void updateOntologyCache() {
		ontologyCache.initialize();
	}
	
	@Override
	public OntologyClassResponse getHierarchy(final StringListResponse stringListResponse) {
		final OntologyClass rootClass = ontologyCache.getMainClass();
		final Namespace namespace = rootClass.getNamespace();
		final LocalName localName = rootClass.getLocalName();
		final OntologyClassResponse ontologyClassResponse = ResponseModelFactory.getOntologyClassResponse(namespace.toString(), localName.toString());
		
		for (int i = 0; i < stringListResponse.getStringCount(); i++) {
			final String classname = stringListResponse.getStringAtIndex(i);
			final OntologyClass foundClass = ontologyCache.getClassByIri(classname);
			ontologyClassResponse.addSubclass(foundClass.toResponse());
		}
		
		return ontologyClassResponse;
	}
	
	@Override
	public OntologyClassResponse getPremisHierarchy() {
		final OntologyClass rootClass = ontologyCache.getPremisMainClass();
		final Namespace namespace = rootClass.getNamespace();
		final LocalName localName = rootClass.getLocalName();
		final OntologyClassResponse ontologyClassResponse = ResponseModelFactory.getOntologyClassResponse(namespace.toString(), localName.toString());
		
		for (int i = 0; i < rootClass.getSubclassCount(); i++) {
			final OntologyClass foundClass = rootClass.getSubClassAt(i);
			ontologyClassResponse.addSubclass(foundClass.toResponse());
		}
		
		return ontologyClassResponse;
	}

	@Override
	public OntologyClassResponse getSkosHierarchy() {
		final OntologyClass rootClass = ontologyCache.getSkosMainClass();
		final Namespace namespace = rootClass.getNamespace();
		final LocalName localName = rootClass.getLocalName();
		final OntologyClassResponse ontologyClassResponse = ResponseModelFactory.getOntologyClassResponse(namespace.toString(), localName.toString());
		
		for (int i = 0; i < rootClass.getSubclassCount(); i++) {
			final OntologyClass foundClass = rootClass.getSubClassAt(i);
			ontologyClassResponse.addSubclass(foundClass.toResponse());
		}
		
		return ontologyClassResponse;
	}
	
	@Override
	public OntologyClassResponse getPropertiesOfClass(final String classname) {
		final OntologyClass ontologyClass = ontologyCache.getClassWithProperties(classname);
		return ontologyClass.toResponse();
	}

	@Override
	public OntologyIndividualCollection findIndiviualsByInformationPackage(final String 
			uuidOfInformationPackageAsString) {
		return requestIndividualsFromTripleStore(null, uuidOfInformationPackageAsString);
	}
	
	/**
	 * Finds statements from the triple store either by a class or by an information package. The statements are
	 * sorted by the subject.
	 * @param iriOfClass The iri of the class whose statements we are looking for. 
	 * @param uuidOfInformationPackageAsString The uuid of the information package whose statements we are looking for. 
	 * @return The found statements.
	 */
	private TripleStatementCollection findTriplesByClassOrInformationPackageSortBySubject(final String iriOfClass, 
			final String uuidOfInformationPackageAsString) {
		TripleStatementCollection individualTriples = null;
		
		if (StringValidator.isNotNullOrEmpty(uuidOfInformationPackageAsString)) {
			final Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getUuid(uuidOfInformationPackageAsString);
			individualTriples = ontologyTripleStoreFacade.findTriplesByInformationPackageAndClass(
						uuidOfInformationPackage, iriOfClass);
		} else {
			Iri classIri = ServerModelFactory.createIri(iriOfClass);
			individualTriples = ontologyTripleStoreFacade.findTriplesByClass(classIri);
		}
		
		individualTriples = individualTriples.sortBySubject();
		return individualTriples;
	}
	
	/**
	 * Requests the triples with the individuals from the triple store and converts them into individuals.
	 * @param iriOfClass The iri of the class the desired individuals belong to. Null if the class is not relevant
	 * for the search.
	 * @param uuidOfInformationPackageAsString The uuid of the information package the individuals 
	 * belong to.
	 * @return The found individuals.
	 */
	private OntologyIndividualCollection requestIndividualsFromTripleStore(final String iriOfClass, 
			final String uuidOfInformationPackageAsString) {
		final OntologyIndividualCollection individualList = new OntologyIndividualCollectionImpl();
		final TripleStatementCollection individualTriples = findTriplesByClassOrInformationPackageSortBySubject(iriOfClass, uuidOfInformationPackageAsString);
		boolean anotherIndividual = true;
		int statementCounter = 0;
		
		if (individualTriples.getTripleStatementsCount() > 0) {
			while (anotherIndividual) {
				final TripleStatement tripleStatement = individualTriples.getTripleStatement(statementCounter);
				final Iri iriOfIndividual = tripleStatement.getSubject().getIri();
				final Uuid uuidOfIndividual = new Uuid(iriOfIndividual.getLocalName().toString());
				String iriOfOntologyClass = null;
				
				if(StringValidator.isNullOrEmpty(iriOfClass)) {
					iriOfOntologyClass = ontologyTripleStoreFacade.findOntologyClassOfIndividual(iriOfIndividual);
				} else {
					iriOfOntologyClass = iriOfClass;
				}
				
				final OntologyClass ontologyClass = ontologyCache.getClassWithProperties(iriOfOntologyClass);
				final OntologyIndividual ontologyIndividual = ontologyModelFactory.createOntologyIndividual(ontologyClass, 
						null, uuidOfIndividual);
				
				while (individualTriples.hasNextStatementOfIndividual(iriOfIndividual.toString())) {
					final TripleStatement statementOfIndividual = individualTriples.getNextStatementOfIndividual(
							iriOfIndividual.toString());
					
					final TriplePredicate triplePredicateWithProperty = statementOfIndividual.getPredicate();
					if (triplePredicateWithProperty.isLiteralPredicate()) {
						createDatatypeProperty(statementOfIndividual, ontologyClass, ontologyIndividual);
					} else if (triplePredicateWithProperty.isObjectPredicate()) {
						createObjectProperty(statementOfIndividual, ontologyClass, ontologyIndividual);
					}
				}
				if (ontologyIndividual.hasProperties()) {
					individualList.addIndividual(ontologyIndividual);
				}
				statementCounter++;
				
				if (statementCounter == individualTriples.getTripleStatementsCount() - 1) {
					anotherIndividual = false;
				}
			}
		}
		return individualList;
	}
	
	/**
	 * Creates a datatype property with the values found in a statement from the triple store. 
	 * @param statementOfIndividual The statement we want to convert into a literal property.
	 * @param ontologyClass The class the property references to. 
	 * @param ontologyIndividual The class the property references to. The individual the property is added to.
	 */
	private void createDatatypeProperty(final TripleStatement statementOfIndividual, final OntologyClass ontologyClass, 
			final OntologyIndividual ontologyIndividual) {
		final TriplePredicate triplePredicateWithProperty = statementOfIndividual.getPredicate();
		final Iri iriOfProperty = triplePredicateWithProperty.getIri();
		final TripleObject tripleObjectWithValue = statementOfIndividual.getObject();
		
		final OntologyDatatypeProperty datatypeProperty = ontologyClass.getDatatypeProperty(iriOfProperty.toString());
		final OntologyDatatypeProperty copyOfDatatypeProperty = ontologyModelFactory.copyOntologyDatatypeProperty(datatypeProperty);
	
		final Object value = tripleObjectWithValue.getValue();
		copyOfDatatypeProperty.setPropertyValue(value);
		ontologyIndividual.addDatatypeProperty(copyOfDatatypeProperty);
	}
	
	/**
	 * Creates an object property from statements from the triple store. 
	 * @param statementOfIndividual The statement that describes an object property.
	 * @param ontologyClass The class the property references to. 
	 * @param ontologyIndividual The individual the property is added to.
	 */
	private void createObjectProperty(final TripleStatement statementOfIndividual, final OntologyClass ontologyClass, 
			final OntologyIndividual ontologyIndividual) {
		final TripleObject tripleObjectWithValue = statementOfIndividual.getObject();
		final TriplePredicate triplePredicate = statementOfIndividual.getPredicate();
		
		final OntologyClass propertyDomain = determineObjectPropertyDomain(statementOfIndividual);
		final OntologyClass propertyRange = determineObjectPropertyRange(statementOfIndividual);
		final OntologyIndividual valueIndividual = determineObjectPropertyValue(statementOfIndividual, propertyRange);
		
		final OntologyObjectProperty objectProperty = createObjectProperty(triplePredicate, propertyDomain, propertyRange);
		addIndividualToObjectProperty(objectProperty, valueIndividual, ontologyIndividual);
	}
	
	/**
	 * Determines the domain class of an object property. 
	 * @param tripleStatement The subject contains the individual that has the type of the domain class.
	 * @return The class that is the domain of the object property described by tripleStatement.
	 */
	private OntologyClass determineObjectPropertyDomain(final TripleStatement tripleStatement) {
		final TripleSubject tripleSubject = tripleStatement.getSubject();
		final String domainClassIri = ontologyTripleStoreFacade.findOntologyClassOfIndividual(tripleSubject.getIri());
		final OntologyClass domainClass = ontologyCache.getClassByIri(domainClassIri);
		return domainClass;
	}
	
	/**
	 * Determines the range class of an object property. 
	 * @param tripleStatement The subject contains the individual that has the type of the range class.
	 * @return The class that is the range of the object property described by tripleStatement.
	 */
	private OntologyClass determineObjectPropertyRange(final TripleStatement tripleStatement) {
		final TripleObject tripleObject = tripleStatement.getObject();
		
		final String iriOfRange = ontologyTripleStoreFacade.findOntologyClassOfIndividual(tripleObject.getIri());
		final Iri range = ServerModelFactory.createIri(iriOfRange);
		final OntologyClass propertyRange = ontologyCache.getClassByIri(range.toString());
		return propertyRange;
	}
	
	/**
	 * Determines the value of an object property. The value of an object property is an individual.
	 * @param tripleStatement The subject contains the individual that has the type of the range class.
	 * @param ontologyClass The class of the individual that is the value of this object property.
	 * @return The individual that 
	 */
	private OntologyIndividual determineObjectPropertyValue(final TripleStatement tripleStatement, 
			final OntologyClass ontologyClass) {
		final TripleObject tripleObject = tripleStatement.getObject();
		final LocalName localName = tripleObject.getLocalName();
		final Uuid uuidOfObjectIndividual = new Uuid(localName.toString());
		final OntologyIndividual propertyIndividual = ontologyModelFactory.createOntologyIndividual(ontologyClass, null,
				uuidOfObjectIndividual); 
		return propertyIndividual;
	}
	
	/**
	 * Creates an object property. It does not contain any values yet.
	 * @param triplePredicate The predicate of the statement that describes the the object property.
	 * @param propertyDomain The domain of the property.
	 * @param propertyRange The range of the property.
	 * @return The created object property.
	 */
	private OntologyObjectProperty createObjectProperty(final TriplePredicate triplePredicate, final OntologyClass propertyDomain,
			final OntologyClass propertyRange) {
		final Iri iriOfProperty = triplePredicate.getIri();
		final String propertyId = iriOfProperty.toString();
		final LocalName localName = iriOfProperty.getLocalName();
		
		final OntologyObjectProperty objectProperty = new OntologyObjectPropertyImpl(propertyId, localName.toString(), 
				propertyRange, propertyRange.getFullName(), propertyDomain);
		return objectProperty;
	}
	
	/**
	 * Adds an individual as value for an object property to another individual. 
	 * @param objectProperty The object property the individual is a value of. 
	 * @param valueIndividual This individual is a value of objectProperty.
	 * @param propertyIndividual This individual contains objectProperty.
	 */
	private void addIndividualToObjectProperty(final OntologyObjectProperty objectProperty,
			final OntologyIndividual valueIndividual, final OntologyIndividual propertyIndividual) {
		final String uuidOfObjectIndividual = valueIndividual.getUuid().toString();
		valueIndividual.setUuid(uuidOfObjectIndividual);
		propertyIndividual.addIndividualToObjectProperty(objectProperty, valueIndividual);
	}
	
	@Override
	public IndividualListResponse getIndiviualsByOntologyClassAndInformationPackage(final String iriOfClass, 
			final String uuidOfInformationPackageAsString) {
		final OntologyIndividualCollection individualList = requestIndividualsFromTripleStore(iriOfClass, uuidOfInformationPackageAsString);
		final IndividualListResponse individualListResponse = ServerResponseFactory.fromOntologyIndividualList(individualList);
		
		return individualListResponse;
	}
	
	@Override
	public IndividualListResponse findIndividualsByClass(final Iri iriOfClass) {
		final OntologyIndividualCollection individualList = requestIndividualsFromTripleStore(iriOfClass.toString(), null);
		final IndividualListResponse individualListResponse = ServerResponseFactory.fromOntologyIndividualList(individualList);
		
		return individualListResponse;
	}
	
	@Override
	public IndividualListResponse getIndiviualsByOntologyClass(final String iriOfClass) {
		OntologyIndividualCollection individualList = requestIndividualsFromTripleStore(iriOfClass, null);
		IndividualListResponse individualListResponse = ServerResponseFactory.fromOntologyIndividualList(individualList);
		
		return individualListResponse;
	}
	
	@Override
	public IndividualListResponse findPredefinedIndividualsByClass(final String iriOfClass) {
		IndividualListResponse individualListResponse = null;
		final OntologyIndividualCollection individualList = ontologyCache.getIndividualsByOntologyClass(iriOfClass);
		
		if (individualList.getIndiviualsCount() > 0) {
			individualListResponse = ServerResponseFactory.fromOntologyIndividualList(individualList);
		} else {
			individualListResponse = ResponseModelFactory.getIndividualListResponse();
		}
		
		return individualListResponse;
	}

	@Override
	public void setOntologyCache(final OntologyCache ontologyCache) {
		this.ontologyCache = ontologyCache;
	}

	@Override
	public void setServerResponseFactory(final ServerResponseFactory serverResponseFactory) {
		this.serverResponseFactory = serverResponseFactory;
	}
	
	@Override
	public void setOntologyTripleStoreFacade(final OntologyTripleStoreFacade ontologyTripleStoreFacade) {
		this.ontologyTripleStoreFacade = ontologyTripleStoreFacade;
	}
	
	@Override
	public void setOntologyModelFactory(final OntologyModelFactory ontologyModelFactory) {
		this.ontologyModelFactory = ontologyModelFactory;
	}

	@Override
	public OntologyIndividualCollection findIndividualByUuidsFromOntologies(final OntologyIndividualCollection ontologyIndividualList) {
		for (int i = 0; i < ontologyIndividualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = ontologyIndividualList.getIndividual(i);
			final Uuid uuidOfIndividual = ontologyIndividual.getUuid();
			final OntologyIndividual ontologyIndividualFromMap = ontologyCache.findOntologyIndividualByUuid(uuidOfIndividual);
			if (ontologyIndividualFromMap != null) {
				ontologyIndividual.setOntologyClass(ontologyIndividualFromMap.getOntologyClass());
				ontologyIndividual.setUuidOfInformationPackage(ontologyIndividualFromMap.getUuidOfInformationPackage());
				
				for (int j = 0; j < ontologyIndividual.getDatatypePropertiesCount(); j++) {
					final OntologyDatatypeProperty ontologyProperty = ontologyIndividual.getDatatypePropertyAt(j);
					ontologyIndividual.addDatatypeProperty(ontologyProperty);
				}
				
				for (int j = 0; j < ontologyIndividual.getObjectPropertiesCount(); j++) {
					final OntologyObjectProperty ontologyProperty = ontologyIndividual.getObjectPropertyAt(j);
					ontologyIndividual.addObjectProperty(ontologyProperty);
				}
			}
		}

		return ontologyIndividualList;
	}
	
	@Override
	public OntologyIndividualCollection findIndividualByUuidsFromOntologies(final UuidList uuidsOfIndividuals) {
		final OntologyIndividualCollection ontologyIndividualList = OntologyModelFactory.createEmptyOntologyIndividualList();
		
		for (int i = 0; i < uuidsOfIndividuals.getUuidCount(); i++) {
			final Uuid uuidOfIndividual = uuidsOfIndividuals.getUuidAt(i);
			final OntologyIndividual ontologyIndividual = ontologyCache.findOntologyIndividualByUuid(uuidOfIndividual);
			ontologyIndividualList.addIndividual(ontologyIndividual);
		}

		return ontologyIndividualList;
	}
	
	/*@Override
	public void saveSelectedTaxonomyIndividuals(final TaxonomyIndividualListResponse taxonomyIndividualsResponse)  {
		for (final TaxonomyIndividualResponse taxonomyIndividual: taxonomyIndividualsResponse.getIndividuals()) {
			RdfElementFactory
		}
		
		
		ontologyTripleStoreFacade.
	}*/
	
	@Override
	public OntologyClassResponse getTaxonomyStartClass() {
		final String configuredSkosMainClass = ontologyConfigurationFacade.readSkosMainClass();
		final OntologyClass skosMainClass =	ontologyCache.getClassByIri(configuredSkosMainClass);
		if (skosMainClass == null) {
			return ResponseModelFactory.getOntologyClassResponse(StringUtils.EMPTY_STRING, StringUtils.EMPTY_STRING);
		}
		final OntologyClassResponse skosMainClassResponse = skosMainClass.toResponse();
		
		return skosMainClassResponse;
	}
	
	@Override
	public void setOntologyConfigurationFacade(OntologyConfigurationFacade ontologyConfigurationFacade) {
		this.ontologyConfigurationFacade = ontologyConfigurationFacade;
	}
}
