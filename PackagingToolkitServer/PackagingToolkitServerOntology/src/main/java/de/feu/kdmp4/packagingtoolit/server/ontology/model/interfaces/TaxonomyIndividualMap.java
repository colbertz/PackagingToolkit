package de.feu.kdmp4.packagingtoolit.server.ontology.model.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;

/**
 * Contains individuals in a taxonomy that are identified by their iri.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomyIndividualMap {
	/**
	 * Puts an individual in the map. The iri of the individual is used as key. 
	 * @param taxonomyIndividual The individual that should be put in the map. 
	 */
	void addTaxonomyIndividual(TaxonomyIndividual taxonomyIndividual);
	/**
	 * Determines an individual in the map by its iri. 
	 * @param iriOfIndividual The iri of the individual we are looking for. 
	 * @return The found individual or an empty optional.
	 */
	Optional<TaxonomyIndividual> getTaxonomyIndividual(Iri iriOfIndividual);
	/**
	 * Checks if the map contains an individual with the certain iri.
	 * @param iri The iri of the individual we are looking for.
	 * @return True if the map contains an individual with this iri, false otherwise.
	 */
	boolean containsTaxonomyIndividual(Iri iri);
	/**
	 * Counts the individuals in the map.
	 * @return The number of individuals in the map.
	 */
	int countTaxonomyIndividuals();
	/**
	 * Adds an individual to the map but as key another iri than the iri of this individual is used.
	 * @param iril The iri that is used as key.
	 * @param taxonomyIndividual The individual that should be put in the map. 
	 */
	void addTaxonomyIndividual(Iri iril, TaxonomyIndividual taxonomyIndividual);
}
 