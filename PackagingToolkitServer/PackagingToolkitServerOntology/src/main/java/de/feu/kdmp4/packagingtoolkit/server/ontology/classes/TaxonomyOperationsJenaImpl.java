package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import java.util.Optional;

import org.apache.jena.graph.Triple;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.SKOS;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class TaxonomyOperationsJenaImpl implements TaxonomyOperations {
	private static final String TITLE = "title";
	private TaxonomyCollection taxonomies;
	/**
	 * Contains individuals that have been extracted but there is no suitable taxonomy available at the moment.
	 * These individuals have to be checked after another taxonomy has been created. 
	 */
	private TaxonomyIndividualCollection individualsWithoutTaxonomy;
	
	public TaxonomyOperationsJenaImpl() {
		taxonomies = OntologyModelFactory.createEmptyTaxonomyCollection();
		individualsWithoutTaxonomy = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
	}
	
	public void addIndividualsToTaxonomy(final OntologyIndividualCollection ontologyIndividualList) {
		
	}
	
	@Override
	public void addIndividualToTaxonomy(final Object objectWithIndividual) { 
		if (objectWithIndividual instanceof Individual) {
			Individual individual = (Individual)objectWithIndividual;
			if (isSkosConceptScheme(individual)) {
				createTaxonomy(individual);
			} else if (isSkosConcept(individual))  {
				addIndividualToTaxonomy(individual);
			}
		}
	}
	
	/**
	 * Creates a new taxonomy.
	 * @param individual The root individual for the new taxonomy.
	 */
	private void createTaxonomy(final Individual individual) {
		final String localNameAsString =  individual.getLocalName();
		final String namespaceAsString = individual.getNameSpace();
		final String namespaceForTaxonomy = namespaceAsString + localNameAsString;
		final Namespace namespace = ServerModelFactory.createNamespace(namespaceForTaxonomy);
		final Iri iri = ServerModelFactory.createIri(namespaceForTaxonomy);
		final String title = determineTaxonomyTitle(individual); 
		
		final TaxonomyIndividual rootIndividual = OntologyModelFactory.createRootTaxonomyIndividual(iri, title, namespace);
		final Taxonomy taxonomy = OntologyModelFactory.createTaxonomy(rootIndividual, title, namespace);
		taxonomies.addTaxonomy(taxonomy);
		
		while (individualsWithoutTaxonomy.hasNextIndividualInTaxonomy(taxonomy)) {
			final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = individualsWithoutTaxonomy.nextIndividualInTaxonomy(taxonomy);
			final TaxonomyIndividual taxonomyIndividual = optionalWithTaxonomyIndividual.get();
			if (taxonomyIndividual.getIriOfBroader() != null) {
				taxonomy.addIndividualToTaxonomy(taxonomyIndividual, taxonomyIndividual.getIriOfBroader());
			} else {
				taxonomy.addTopConceptToTaxonomy(taxonomyIndividual);
			}
		}
		
		individualsWithoutTaxonomy.removeIndividualsOfTaxonomy(taxonomy);
	}
	
	/**
	 * Determines the title for a taxonomy from the root individual. The title is in a dc:title element.
	 * @param individual The individual that contains the title.
	 * @return The title extracted from the individual.
	 */
	private String determineTaxonomyTitle(final Individual individual) {
		String title = null;
		final StmtIterator propertyIterator = individual.listProperties();
		
		while(propertyIterator.hasNext() && title == null) {
			final Statement propertyStatement = propertyIterator.next();
			final Property property = propertyStatement.getPredicate();
			final String localName = property.getLocalName();
			System.out.println(localName);
			if (localName.contains(TITLE)) {
				final Triple triple = propertyStatement.asTriple();
				title = triple.getObject().getLiteralValue().toString();
			}
		}
		
		return title;
	}
	
	/**
	 * Creates an individiual for a taxonomy with the data of an individual extracted from the ontology
	 * by Jena and adds to a taxonomy.
	 * @param individual The Jena individual.
	 */
	private void addIndividualToTaxonomy(final Individual individual) {
		final StmtIterator propertyIterator = individual.listProperties();
		boolean isTopConcept = false;
		String preferredLabel = null;
		String inScheme = null;
		String broader = null;
		String topConceptOf = null;
		
		while(propertyIterator.hasNext()) {
			final Statement propertyStatement = propertyIterator.next();
			final Property property = propertyStatement.getPredicate();
			
			if (isTopConceptProperty(property)) {
				isTopConcept = true;
			} else if (isPrefLabelProperty(property)) {
				final Triple triple = propertyStatement.asTriple();
				preferredLabel = triple.getObject().getLiteralValue().toString();
			} else if (isInSchemeProperty(property)) {
				final Triple triple = propertyStatement.asTriple();
				inScheme = triple.getObject().toString();
			} else if (isBroaderProperty(property)) {
				final Triple triple = propertyStatement.asTriple();
				broader = triple.getObject().toString();
			} else if (isTopConceptOfProperty(property)) {
				final Triple triple = propertyStatement.asTriple();
				topConceptOf = triple.getObject().toString();
			}
		}
		
		final Iri iri = createIriFromIndividual(individual);
		final Namespace namespace = ServerModelFactory.createNamespace(inScheme);
		
		TaxonomyIndividual taxonomyIndividual = null;
		if (isTopConcept) {
			taxonomyIndividual = OntologyModelFactory.createRootTaxonomyIndividual(iri, preferredLabel, namespace);
		} else {
			final Iri iriOfBroader = ServerModelFactory.createIri(broader);
			taxonomyIndividual = OntologyModelFactory.createTaxonomyIndividual(iri, preferredLabel, namespace, iriOfBroader);
		}
		 
		final Optional<Taxonomy> optionalWithTaxonomy = taxonomies.findTaxonomyByNamespace(namespace);
		if (optionalWithTaxonomy.isPresent()) {
			final Taxonomy taxonomy = optionalWithTaxonomy.get();
			if (isTopConcept) {
				taxonomy.addTopConceptToTaxonomy(taxonomyIndividual);
			} else {
				final Iri iriOfBroader = ServerModelFactory.createIri(broader);
				taxonomy.addIndividualToTaxonomy(taxonomyIndividual, iriOfBroader);
				if (StringValidator.isNotNullOrEmpty(topConceptOf)) {
					
				}
			}
		} else {
			individualsWithoutTaxonomy.addTaxonomyIndividual(taxonomyIndividual);
		}
	}
	
	/**
	 * Reads the iri of an individual from the individual object.
	 * @param individual The individual whose iri we want to extract. 
	 * @return The extracted iri. 
	 */
	private Iri createIriFromIndividual(final Individual individual) {
		final String iriAsString = individual.getNameSpace();
		//final Namespace namespace = ServerModelFactory.createNamespace(individual.getNameSpace());
		//final LocalName localName = ServerModelFactory.createLocalName(individual.getLocalName());
		final Iri iri = ServerModelFactory.createIri(iriAsString);
		return iri;
	}
	
	/**
	 * Checks if a property represents a top concept of SKOS.
	 * @param property The property we want to check.
	 * @return True if the property represents a top concept, false otherwise.
	 */
	private boolean isTopConceptProperty(final Property property) {
		if (property.getLocalName().equals(SKOS.topConceptOf.getLocalName())) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Checks if a property represents a prefLabel of SKOS.
	 * @param property The property we want to check.
	 * @return True if the property represents a prefLabel, false otherwise.
	 */
	private boolean isPrefLabelProperty(final Property property) {
		if (property.getLocalName().equals(SKOS.prefLabel.getLocalName())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if a property represents a broader of SKOS.
	 * @param property The property we want to check.
	 * @return True if the property represents a  broader, false otherwise.
	 */
	private boolean isBroaderProperty(final Property property) {
		if (property.getLocalName().equals(SKOS.broader.getLocalName())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if a property represents a topConceptOf of SKOS.
	 * @param property The property we want to check.
	 * @return True if the property represents a  topConceptOf, false otherwise.
	 */
	private boolean isTopConceptOfProperty(final Property property) {
		if (property.getLocalName().equals(SKOS.topConceptOf.getLocalName())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if a property represents a inScheme of SKOS.
	 * @param property The property we want to check.
	 * @return True if the property represents a inScheme, false otherwise.
	 */
	private boolean isInSchemeProperty(final Property property) {
		if (property.getLocalName().equals(SKOS.inScheme.getLocalName())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Determines if a statement contains a SKOS concept
	 * @param objectWithStatement The object we want to check. 
	 * @return True if the statement is related to SKOS concept.
	 */
	private boolean isSkosConcept(final Individual individual) {
		final OntClass ontClass = individual.getOntClass();
		
		 if(ontClass.getLocalName().equals(SKOS.Concept.getLocalName())) {
			 return true;
		 }
		
		return false;
	}
	
	/**
	 * Determines if a statement contains a SKOS concept scheme.
	 * @param objectWithStatement The object we want to check. 
	 * @return True if the statement is related to SKOS concept scheme.
	 */
	private boolean isSkosConceptScheme(final Individual individual) {
		final OntClass ontClass = individual.getOntClass();
		
		 if(ontClass.getLocalName().equals(SKOS.ConceptScheme.getLocalName())) {
			 return true;
		 }
		
		return false;
	}
	
	@Override
	public int getTaxonomyCount() {
		return taxonomies.getTaxonomyCount();
	}
	
	@Override
	public Optional<Taxonomy> getTaxonomy(final int index) {
		return taxonomies.getTaxonomy(index);
	}
}
