package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.util.Iterator;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;

public class TaxonomyIndividualImpl implements TaxonomyIndividual {
	/**
	 * The iri of this individual. 
	 */
	private Iri iri;
	/**
	 * The label that should be displayed to the user.
	 */
	private String preferredLabel;
	/**
	 * The narrowers of this individual. Narrowers are the individuals that follow this one in the
	 * taxonomy.
	 */
	private TaxonomyIndividualCollection narrowers;
	/**
	 * The namespace of the scheme the individual is assigned to.
	 */
	private Namespace inScheme;
	/**
	 * The iri of the broader of this individual. This is the parent individual in the taxonomy.
	 */
	private Iri iriOfBroader;
	/**
	 * Iterates through all narrowers of this individual.
	 */
	private NarrowerIterator  narrowerIterator;
	
	/**
	 * Creates a new object with an iri and an empty narrower collection.
	 */
	public TaxonomyIndividualImpl(final Iri iri, final String preferredLabel, final Namespace inScheme, final Iri iriOfBroader) {
		narrowers = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
		this.iri = iri;
		this.inScheme = inScheme;
		this.iriOfBroader = iriOfBroader;
		this.preferredLabel = preferredLabel;
	}
	
	/**
	 * Creates a new object with an iri and an empty narrower collection. This constructor is used for individuals that are 
	 * root individual of an taxonomy or individuals that represent a top concept. In both cases there is no broader and therefore
	 * the individual does not get any iri for a broader.
	 */
	public TaxonomyIndividualImpl(final Iri iri, final String preferredLabel, final Namespace inScheme) {
		this(iri, preferredLabel, inScheme, null);
	}
	
	@Override
	public void addNarrower(final TaxonomyIndividual taxonomyIndividual) {
		narrowers.addTaxonomyIndividual(taxonomyIndividual);
	}
	
	@Override
	public int getNarrowerCount() {
		return narrowers.getTaxonomyIndividualCount();
	}
	
	@Override
	public Iri getIri() {
		return iri;
	}

	@Override
	public boolean containsNarrower(final TaxonomyIndividual narrower) {
		for (int i = 0; i < narrowers.getTaxonomyIndividualCount(); i++) {
			final Optional<TaxonomyIndividual> optionalWithNarrower = narrowers.getTaxonomyIndividual(i); 
			final TaxonomyIndividual otherNarrower = optionalWithNarrower.get();
			if (otherNarrower.equals(narrower)) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public Optional<TaxonomyIndividual> findNarrower(final Iri iriOfNarrower) {
		for (int i = 0; i < narrowers.getTaxonomyIndividualCount(); i++) {
			final Optional<TaxonomyIndividual> optionalWithNarrower = narrowers.getTaxonomyIndividual(i); 
			final TaxonomyIndividual otherNarrower = optionalWithNarrower.get();
			if (otherNarrower.getIri().equals(iriOfNarrower)) {
				return optionalWithNarrower;
			}
		}
		
		return OntologyOptionalFactory.createEmptyOptionalWithTaxonomyIndividual();
	}
	
	@Override
	public boolean hasNextNarrower() {
		if (narrowerIterator == null) {
			narrowerIterator = new  NarrowerIterator();
		}
		
		final boolean hasNext = narrowerIterator.hasNext();
		
		if (hasNext == false) {
			narrowerIterator = null;
		}
		return hasNext;
	}
	
	@Override
	public Optional<TaxonomyIndividual> getNextNarrower() {
		if (narrowerIterator == null) {
			return OntologyOptionalFactory.createEmptyOptionalWithTaxonomyIndividual();
		}
		return narrowerIterator.next();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iri == null) ? 0 : iri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxonomyIndividualImpl other = (TaxonomyIndividualImpl) obj;
		if (iri == null) {
			if (other.iri != null)
				return false;
		} else if (!iri.equals(other.iri))
			return false;
		return true;
	}
	
	@Override
	public String getPreferredLabel() {
		return preferredLabel;
	}
	
	@Override
	public Namespace getInScheme() {
		return inScheme;
	}
	
	@Override
	public Iri getIriOfBroader() {
		return iriOfBroader;
	}
	
	@Override
	public boolean isInTaxonomy(final Taxonomy taxonomy) {
		final Namespace namespaceOfTaxonomy = taxonomy.getNamespace();
		if (namespaceOfTaxonomy.equals(inScheme)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString( ) {
		return preferredLabel + ": " + iri.toString();
	}
	
	@Override
	public void setPreferredLabel(String preferredLabel) {
		this.preferredLabel = preferredLabel;
	}
	
	@Override
	public boolean containsNarrowers() {
		if (narrowers.getTaxonomyIndividualCount() == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Iterates through all narrowers.
	 * @author Christopher Olbertz
	 *
	 */
	private class NarrowerIterator implements Iterator<Optional<TaxonomyIndividual>> {
		private int counter;
		private TaxonomyIndividual currentTaxonomyIndividual;
		
		public NarrowerIterator() {
			counter = 0;
			if (containsNarrowers()) {
				currentTaxonomyIndividual = narrowers.getTaxonomyIndividual(0).get();
			} else {
				currentTaxonomyIndividual = null;
			}
		}
		
		@Override
		public boolean hasNext() {
			if (currentTaxonomyIndividual == null) {
				counter = 0;
				return false;
			} else {
				if (counter < getNarrowerCount()) {
					return true;
				}
			}
			
			counter = 0;
			return false;
		}

		@Override
		public Optional<TaxonomyIndividual> next() {
			if (hasNext()) {
				final Optional<TaxonomyIndividual> taxonomyIndividual = narrowers.getTaxonomyIndividual(counter);
				counter++;
				return taxonomyIndividual;
			} else {
				return OntologyOptionalFactory.createEmptyOptionalWithTaxonomyIndividual();
			}
		}
	}
}
