package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.io.Serializable;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.PropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

public class PropertyCollectionImpl extends AbstractList implements PropertyCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;

	@Override
	public void addProperty(final OntologyProperty property) {
		super.add(property);
	}
	
	@Override
	public OntologyProperty getProperty(final int index) {
		return (OntologyProperty)super.getElement(index);
	}
	
	@Override
	public int getPropertiesCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}
}
