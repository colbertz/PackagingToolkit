package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

/**
 * A collection that contains individuals in a taxonomy.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomyIndividualCollection {
	/**
	 * Adds an individual to the collection.
	 * @param taxonomyIndividual The individual that should be added.
	 */
	void addTaxonomyIndividual(TaxonomyIndividual taxonomyIndividual);
	/**
	 * Determines an individual in the collection at a certain position.
	 * @param index The position we are looking at.
	 * @return The found individual or an empty optional.
	 */
	Optional<TaxonomyIndividual> getTaxonomyIndividual(int index);
	/**
	 * Determines the number of individuals in this collection.
	 * @return The number of individuals in this collection.
	 */
	int getTaxonomyIndividualCount();
	/**
	 * Determines the next individual in the collection that is assigned to a certain taxonomy. 
	 * @param taxonomy The taxonomy we want to determine the next individual of.
	 * @return The next individual that is assigned to taxonomy or an empty optional if there is no
	 * next individual.
	 */
	Optional<TaxonomyIndividual> nextIndividualInTaxonomy(Taxonomy taxonomy);
	/**
	 * Checks if there are any other individuals that are contained in this collection.
	 * @param taxonomy The taxonomy we want to determine the next individual of.
	 * @return True if there is an other individuals that is contained in this collection.
	 */
	boolean hasNextIndividualInTaxonomy(Taxonomy taxonomy);
	/**
	 * Removes all individuals that are assigned to a certain taxonomy from the collection.
	 * @param taxonomy The taxonomy whose individuals we want to remove.
	 */
	void removeIndividualsOfTaxonomy(Taxonomy taxonomy);
	/**
	 * Processes an action on every individual in this collection. The modified individuals are written in a new collection
	 * object. This means that the original individuals are not modified.
	 * @param operator Contains the operation that should be done on every individual.
	 * @return A new collection object with the modifications.
	 */
	void doWithEveryIndividual(Consumer<TaxonomyIndividual> consumer);
}