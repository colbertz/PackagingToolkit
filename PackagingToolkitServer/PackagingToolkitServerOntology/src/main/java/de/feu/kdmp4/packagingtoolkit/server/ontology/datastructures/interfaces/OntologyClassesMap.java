package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

/**
 * Contains the iris of classes of the ontologies as key and the objects as value. 
 * @author Christopher Olbertz
 *
 */
public interface OntologyClassesMap {
	/**
	 * Adds a new class to the map.
	 * @param ontologyClass The class we want to add to the map.
	 */
	public void addClass(final OntologyClass ontologyClass);
	/**
	 * Gets the a class from the map by its iri. 
	 * @param classIri The iri of the class. This is the key in the map.
	 * @return The class object that has been found. 
	 */
	OntologyClass getOntologyClass(final String classIri);
	/**
	 * Adds a property to a class in the map. The class is identified by its iri. 
	 * @param classIri The iri of the class and the key for the map.
	 * @param ontologyProperty The property we want to add to the class.  
	 */
	void addProperty(final String classIri, final OntologyProperty ontologyProperty);
	/**
	 * Determines the number of classes in the map.
	 * @return The number of classes in the map.
	 */
	int getOntologyClassesCount();
}
