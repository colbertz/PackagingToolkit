package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.TextInLanguageListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TextInLanguageResponse;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.validators.ListValidator;

/**
 * An implementation of {@see de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass}. 
 * @author Christopher Olbertz
 *
 */
public class OntologyClassImpl extends OntologyElementImpl implements OntologyClass, Serializable {
	private static final long serialVersionUID = -4248591790948061732L;
	
	/**
	 * The namespace of the class in the ontology.
	 */
	private Namespace namespace;
	/**
	 * The local name of the class in the ontology.
	 */
	private LocalName localName;
	/**
	 * Contains the sub classes of this class as defined in the ontology.
	 */
	private List<OntologyClass> subclasses;
	/**
	 * Contains the datatype properties as defined in the ontology.
	 */
	private DatatypePropertyCollection datatypePropertyList;
	/**
	 * Contains the object properties as defined in the ontology.
	 */
	private ObjectPropertyCollection objectPropertyList;
	
	/**
	 * Creates a new ontology class. The subclasses are initialized as empty collection.
	 * @param datatypePropertyList The datatype of the class that should be created.
	 * @param objectPropertyList The object of the class that should be created.
	 */
	public OntologyClassImpl(final DatatypePropertyCollection datatypePropertyList, final ObjectPropertyCollection objectPropertyList) {
		this.datatypePropertyList = datatypePropertyList;
		this.objectPropertyList = objectPropertyList;
		subclasses = new ArrayList<>();
	}
	
	/**
	 * Creates a new ontology class. The subclasses are initialized as empty collection.
	 * @param datatypePropertyList The datatype of the class that should be created.
	 * @param objectPropertyList The object of the class that should be created.
	 * @param namespace The namespace of the class.
	 * @param localName The local name of the class.
	 */
	public OntologyClassImpl(final DatatypePropertyCollection datatypePropertyList, final ObjectPropertyCollection objectPropertyList,
			final Namespace namespace, final LocalName localName) {
		this.datatypePropertyList = datatypePropertyList;
		this.objectPropertyList = objectPropertyList;
		subclasses = new ArrayList<>();
		this.namespace = namespace;
		this.localName = localName;
	}
	
	/**
	 * Constructs an ontology class. The classLabel may not be null or empty. The list
	 * with the subclasses is instantiated as an array list.
	 * @param namespace The namespace of the class.
	 * @param localName The local name of the class.
	 */
	public OntologyClassImpl(final Namespace namespace, final LocalName localname) {
		this.namespace = namespace;
		this.localName = localname;
		subclasses = new ArrayList<>();
		datatypePropertyList = new DatatypePropertyCollectionImpl();
		objectPropertyList = new ObjectPropertyCollectionImpl();
	}
	
	@Override  
	public void addDatatypeProperties(final DatatypePropertyCollection propertyList) {
		if (propertyList != null) {
			for (int i = 0; i < propertyList.getPropertiesCount(); i++) {
				final OntologyDatatypeProperty property = propertyList.getDatatypeProperty(i);
				this.datatypePropertyList.addDatatypeProperty(property);
			}
		}
	}
	
	@Override  
	public void addObjectProperties(final ObjectPropertyCollection propertyList) {
		if (propertyList != null) {
			for (int i = 0; i < propertyList.getPropertiesCount(); i++) {
				final OntologyObjectProperty property = propertyList.getObjectProperty(i);
				this.objectPropertyList.addObjectProperty(property);
			}
		}
	}
	
	@Override
	public void addDatatypeProperty(final OntologyDatatypeProperty ontologyProperty) {
		datatypePropertyList.addDatatypeProperty(ontologyProperty);
	}
	
	@Override
	public void addObjectProperty(final OntologyObjectProperty ontologyObjectProperty) {
		objectPropertyList.addObjectProperty(ontologyObjectProperty);
	}
	
	@Override
	public OntologyObjectProperty getObjectProperty(final int index) {
		return objectPropertyList.getObjectProperty(index);
	}
	
	@Override
	public OntologyDatatypeProperty getDatatypeProperty(final int index) {
		return datatypePropertyList.getDatatypeProperty(index);
	}
	
	@Override 
	public OntologyDatatypeProperty getDatatypeProperty(final String propertyName) {
		OntologyDatatypeProperty ontologyProperty = null;
		int i = 0;
		boolean found = false;
		
		while (found == false && i < datatypePropertyList.getPropertiesCount()) {
			ontologyProperty = datatypePropertyList.getDatatypeProperty(i);
			
			if (ontologyProperty.getPropertyId().equals(propertyName)) {
				found = true;
			}
			i++;
		}
		
		return ontologyProperty;
	}
	
	@Override 
	public OntologyObjectProperty getObjectProperty(final String propertyName) {
		OntologyObjectProperty ontologyProperty = null;
		int i = 0;
		boolean found = false;
		
		while (found == false && i < objectPropertyList.getPropertiesCount()) {
			ontologyProperty = objectPropertyList.getObjectProperty(i);
			
			if (ontologyProperty.getPropertyId().equals(propertyName)) {
				found = true;
			}
			i++;
		}
		
		return ontologyProperty;
	}
	
	@Override
	public int getDatatypePropertiesCount() {
		return datatypePropertyList.getPropertiesCount();
	}
	
	@Override
	public int getObjectPropertiesCount() {
		return objectPropertyList.getPropertiesCount();
	}
	
	@Override
	public void addSubClass(final OntologyClass subClass) {
		assert subClass != null: "subClass may not be null!";
		subclasses.add(subClass);
	}
	
	@Override
	public OntologyClass getSubClassAt(final int index) {
		assert ListValidator.isIndexInRange(index, subclasses): "The index " + index +
			" is invalid!";
		return subclasses.get(index);
	}
	
	@Override
	public Namespace getNamespace(){
		return namespace;
	}
	
	@Override
	public LocalName getLocalName() {
		return localName;
	}

	@Override
	public int getSubclassCount() {
		return subclasses.size();
	}
	
	@Override
	public boolean hasSubclasses() {
		// There are no subclasses.
		if (subclasses.size() == 0) {
			return false;
		} else if (subclasses.size() == 1){
			// There is one subclass and this subclass is Nothing.
			if (subclasses.get(0).isNothing()) {
				return false;
			} else {
				// There is one subclass that is not Nothing.
				return true;
			}
 		} else {
 			// There are multiple subclasses.
			return true;
		}
	}
	
	@Override 
	public int indexOfSubclass(final String subclassName) {
		for (int i = 0; i <= subclasses.size(); i++) {
			final OntologyClass ontologyClass = subclasses.get(i);
			if (ontologyClass.classnameContains(subclassName)) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public String toString() {
		return getLocalName().toString();
	}   
	
	@Override
	public boolean classnameContains(final String text) {
		return localName.toString().contains(text);
	}
	
	@Override
	public String getLabel() {
		return localName.toString();
	}

	@Override
	public boolean isNothing() {
		return localName.toString().contains("Nothing");
	}
	
	@Override
	public boolean isThing() {
		return localName.toString().contains("Thing");
	}
	
	@Override
	public String getFullName() {
		return namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localName;
	}
	
	@Override
	public OntologyClassResponse toResponse() {
		final OntologyClassResponse ontologyClassResponse = ResponseModelFactory.getOntologyClassResponse(namespace.toString(), localName.toString());
		
		if (datatypePropertyList != null && !datatypePropertyList.isEmpty()) {
			final DatatypePropertyListResponse propertyListResponse = ServerResponseFactory.toResponse(datatypePropertyList); 
			ontologyClassResponse.setProperties(propertyListResponse);
		}
		
		if (objectPropertyList != null && !objectPropertyList.isEmpty()) {
			final ObjectPropertyListResponse objectPropertyListResponse = ServerResponseFactory.toResponse(objectPropertyList); 
			ontologyClassResponse.setObjectProperties(objectPropertyListResponse);
		}
		
		if (subclasses != null && !subclasses.isEmpty()) {
			for (int i = 0; i < subclasses.size(); i++) {
				final OntologyClass subclass = subclasses.get(i);
				final OntologyClassResponse subclassResponse = subclass.toResponse();
				ontologyClassResponse.addSubclass(subclassResponse);
			}
		}
		
		final TextInLanguageListResponse labelTextsResponse = new TextInLanguageListResponse();
		for (int i = 0; i < this.getLabelCount(); i++) {
			final TextInLanguage label = this.getLabelAt(i);
			final TextInLanguageResponse textInLanguageResponse = ServerResponseFactory.toResponse(label);
			labelTextsResponse.addTextInLanguage(textInLanguageResponse);
		}
		ontologyClassResponse.setLabelTexts(labelTextsResponse);
		
		return ontologyClassResponse;
	}

	@Override
	public boolean hasDatatypeProperties() {
		if (datatypePropertyList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public boolean hasObjectProperties() {
		if (objectPropertyList == null || objectPropertyList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localName == null) ? 0 : localName.hashCode());
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyClassImpl other = (OntologyClassImpl) obj;
		if (localName == null) {
			if (other.localName != null)
				return false;
		} else if (!localName.equals(other.localName))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		return true;
	}

	@Override
	public void setNamespace(final Namespace namespace) {
		this.namespace = namespace;
	}
}

