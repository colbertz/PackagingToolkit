package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolit.server.ontology.model.interfaces.TaxonomyIndividualMap;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.IrisTaxonomyIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;

public class TaxonomyImpl implements Taxonomy {
	/**
	 * This is the individual the taxonomy starts with. 
	 */
	private TaxonomyIndividual rootIndividual;
	/**
	 * Contains the individuals in the taxonomy in a map. This makes it easier to find a certain individual.
	 */
	private TaxonomyIndividualMap taxonomyIndividualMap;
	/**
	 * A title for the taxonomy that can for example be displayed in the user interface.
	 */
	private String title;
	/**
	 * A namespace for the taxonomy. The taxonomy can be identified uniquely by this namespace.
	 */
	private Namespace namespace;
	
	/**
	 * Contains the individuals that have been added to the map but there broaders have not been present in the
	 * map. If an individual is added to the taxonomy, this map is searched for the new individual. If it is found the
	 * narrower relationship is established.
	 */
	private IrisTaxonomyIndividualsMap individualsWithNotExistingBroaders;
	
	public TaxonomyImpl(final TaxonomyIndividual rootIndividual, final String title, final Namespace namespace) {
		this.rootIndividual = rootIndividual;
		this.title = title;
		this.namespace = namespace;
		taxonomyIndividualMap = OntologyModelFactory.createTaxonomyIndividualMap();
		individualsWithNotExistingBroaders = OntologyModelFactory.createIrisTaxonomyIndividualsMap();
	}
	
	@Override
	public void addIndividualToTaxonomy(final TaxonomyIndividual taxonomyIndividual, final Iri iriOfBroader) {
		taxonomyIndividualMap.addTaxonomyIndividual(taxonomyIndividual);
		final Optional<TaxonomyIndividual> optionalWithBroader = taxonomyIndividualMap.getTaxonomyIndividual(iriOfBroader);
		
		if (optionalWithBroader.isPresent()) {
			final TaxonomyIndividual broader = optionalWithBroader.get();
			broader.addNarrower(taxonomyIndividual);
		} else {
			individualsWithNotExistingBroaders.addTaxonomyIndividal(iriOfBroader, taxonomyIndividual);
		}
		
		final Iri iriOfIndividual = taxonomyIndividual.getIri();
		final Optional<TaxonomyIndividualCollection> optionalWithIndivididualsOfBroader = individualsWithNotExistingBroaders.getTaxonomyIndividuals(iriOfIndividual);
		if (optionalWithIndivididualsOfBroader.isPresent()) {
			final TaxonomyIndividualCollection taxonomyIndividualCollection = optionalWithIndivididualsOfBroader.get();
			for (int i = 0; i < taxonomyIndividualCollection.getTaxonomyIndividualCount(); i++) {
				final Optional<TaxonomyIndividual> optionalWithIndividual = taxonomyIndividualCollection.getTaxonomyIndividual(i);
				if (optionalWithIndividual.isPresent()) {
					final TaxonomyIndividual broader = optionalWithIndividual.get();
					taxonomyIndividual.addNarrower(broader);
				}
			}
		}
	}
	
	@Override
	public void addTopConceptToTaxonomy(final TaxonomyIndividual topLevelIndividual) {
		taxonomyIndividualMap.addTaxonomyIndividual(topLevelIndividual);
		rootIndividual.addNarrower(topLevelIndividual);
		
		final Iri iriOfIndividual = topLevelIndividual.getIri();
		final Optional<TaxonomyIndividualCollection> optionalWithIndivididualsOfBroader = individualsWithNotExistingBroaders.getTaxonomyIndividuals(iriOfIndividual);
		if (optionalWithIndivididualsOfBroader.isPresent()) {
			final TaxonomyIndividualCollection taxonomyIndividualCollection = optionalWithIndivididualsOfBroader.get();
			for (int i = 0; i < taxonomyIndividualCollection.getTaxonomyIndividualCount(); i++) {
				final Optional<TaxonomyIndividual> optionalWithIndividual = taxonomyIndividualCollection.getTaxonomyIndividual(i);
				if (optionalWithIndividual.isPresent()) {
					final TaxonomyIndividual broader = optionalWithIndividual.get();
					topLevelIndividual.addNarrower(broader);
				}
			}
		}
	}
	
	@Override
	public TaxonomyIndividual getRootIndividual() {
		return rootIndividual;
	}
	
	@Override
	public int countTaxonomyIndividuals() {
		return taxonomyIndividualMap.countTaxonomyIndividuals();
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public Namespace getNamespace() {
		return namespace;
	}
}
