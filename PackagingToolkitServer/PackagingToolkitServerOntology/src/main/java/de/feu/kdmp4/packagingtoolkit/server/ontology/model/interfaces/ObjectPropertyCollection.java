package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;

/**
 * Contains a collection with object properties.
 * @author Christopher Olbertz
 *
 */
public interface ObjectPropertyCollection {
	/**
	 * Adds a new object property to the collection.
	 * @param property The property that should be added.
	 */
	void addObjectProperty(final OntologyObjectProperty property);
	/**
	 * Determines a property at a given position in the collection.
	 * @param index The index we want looking at.
	 * @return The found property.
	 */
	OntologyObjectProperty getObjectProperty(final int index);
	/**
	 * Determines the number of properties in the collection.
	 * @return The number of properties.
	 */
	int getPropertiesCount();
	/**
	 * Determines if there are any objects in the collection.
	 * @return True if the collection is empty, false otherwise.
	 */
	boolean isEmpty();
}
