package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import java.io.Serializable;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyElement;

/**
 * Represents a property in an ontology. A property has a value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyProperty extends OntologyElement, Serializable, Cloneable {
	/**
	 * Determines the range as specified in the ontology.
	 * @return The range as specified in the ontology.
	 */
	String getPropertyRange();
	/**
	 * Creates a copy of the property.
	 * @return The copy of the property.
	 */
	OntologyProperty cloneProperty();
	/**
	 * Determines if the property is an object property.
	 * @return True if the property is an object property, false otherwise.
	 */
	boolean isObjectProperty();
	/**
	 * Returns the iri of the property.
	 * @return The iri of the property.
	 */
	String getPropertyId();
	/**
	 * Checks if the property has a value.
	 * @return True if the value of the property is not equal null.
	 */
	boolean hasValue();
	/**
	 * Sets a new value in the property. 
	 * @param value The new value. May be null. 
	 */
	void setPropertyValue(final Object value);
	/**
	 * Gets the property of the value.
	 * @return The property of the value.
	 */
	Object getPropertyValue();
	/**
	 * The namespace of the property. The namespace is the part before the # symbol.
	 * @return The namespace of the property.
	 */
	Namespace getNamespace();
	/**
	 * Gets the uuid that identifies the property uniquely.
	 * @return The uuid. 
	 */
	Uuid getUuid();
	/**
	 * Returns the domain of the property at a given position.
	 * @param index The position in the list with domains.
	 * @return The domain at the given position.
	 */
	String getPropertyDomainAt(int index);
	/**
	 * Counts the domains of the property.
	 * @return The number of domains.
	 */
	int getPropertyDomainsCount();
	/**
	 * Adds a domain to the property. 
	 * @param propertyDomain The property that should be added.
	 */
	void addPropertyDomain(String propertyDomain);
	/**
	 * The local name of the property. The local name is the part before the # symbol.
	 * @return The local name of the property.
	 */
	LocalName getLocalName();
}
