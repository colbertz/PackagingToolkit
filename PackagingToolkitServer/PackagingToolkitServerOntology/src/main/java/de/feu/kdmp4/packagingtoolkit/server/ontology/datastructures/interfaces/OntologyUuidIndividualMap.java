package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;

/**
 * Contains individuals. Their keys are their uuids.
 * @author Christopher Olbertz
 *
 */
public interface OntologyUuidIndividualMap {
	/**
	 * Adds an individual in the map. 
	 * @param ontologyIndividual The individual to add to the map. Its uuid is used as its key.
	 */
	void addIndividual(final OntologyIndividual ontologyIndividual);
	/**
	 * Gets an individual from the map with the help of the uuid.
	 * @param uuidOfIndividual The uuid of the individual we are looking for.
	 * @return The individual with the uuid uuidOfIndividual.
	 */
	OntologyIndividual getIndividual(final Uuid uuidOfIndividual);
}
