package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

/**
 * Represents a property with the datatype double.
 * @author Christopher Olbertz
 *
 */
public interface OntologyDoubleProperty extends OntologyDatatypeProperty {

}
