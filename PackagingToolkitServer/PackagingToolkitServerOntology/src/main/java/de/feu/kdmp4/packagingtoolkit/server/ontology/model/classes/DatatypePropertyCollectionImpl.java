package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.io.Serializable;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;

public class DatatypePropertyCollectionImpl extends AbstractList  implements DatatypePropertyCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;

	public DatatypePropertyCollectionImpl() {
	}
	
	@Override
	public void addDatatypeProperty(final OntologyDatatypeProperty property) {
		super.add(property);
	}
	
	@Override
	public OntologyDatatypeProperty getDatatypeProperty(final int index) {
		return (OntologyDatatypeProperty)super.getElement(index);
	}
	 
	@Override
	public int getPropertiesCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}

	@Override
	public void addDatatypeProperties(final DatatypePropertyCollection datatypeProperties) {
		super.addCollection(datatypeProperties); 
	}
}
