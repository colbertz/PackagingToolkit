package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

/**
 * Represents an annotation property.
 * @author Christopher Olbertz
 *
 */
public interface OntologyAnnotationProperty {
	/**
	 * Returns the iri of the property.
	 * @return The iri of the property.
	 */
	String getIri();
}
