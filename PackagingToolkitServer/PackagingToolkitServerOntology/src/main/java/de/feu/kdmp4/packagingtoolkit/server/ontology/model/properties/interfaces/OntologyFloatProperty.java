package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;


public interface OntologyFloatProperty extends OntologyDatatypeProperty {
	/**
	 * Adds a max inclusive restriction.
	 * @param value The value that may not be overstepped.
	 */
	void addMaxInclusiveRestriction(final float value);
	/**
	 * Adds a min inclusive restriction.
	 * @param value The value that may not be fallen below.
	 */
	void addMinInclusiveRestriction(final float value);
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	Float getPropertyValue();
}
