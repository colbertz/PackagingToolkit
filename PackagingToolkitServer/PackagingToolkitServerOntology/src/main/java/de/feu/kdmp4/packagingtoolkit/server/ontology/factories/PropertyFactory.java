package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDatePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDateTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyDurationPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyFloatPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyLongPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyLongProperty;
//import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * A factory for ontology properties.
 * @author Christopher Olbertz
 *
 */
public class PropertyFactory extends AbstractFactory {
	/**
	 * Clones an ontology property and write its values in a new ontology property object.
	 * @param ontologyProperty The ontology property that has be cloned.
	 * @return A new ontology property object that is identical to the parameter object.
	 */
	public OntologyProperty cloneOntologyProperty(final OntologyProperty originalProperty) {
		String propertyRange = originalProperty.getPropertyRange();
		String label = originalProperty.getLabel();
		String propertyId = originalProperty.getPropertyId();
		OntologyProperty copy = getProperty(propertyRange, propertyId, label);
		super.initBean(copy);
		return copy;
	}
	
	/**
	 * Copies a datatype property and write its values in a new datatype property object.
	 * @param ontologyProperty The datatype property that should be copied.
	 * @return The copy of ontologyProperty  
	 */
	public OntologyDatatypeProperty copyOntologyDatatypeProperty(final OntologyDatatypeProperty ontologyProperty) {
		final String propertyIri = ontologyProperty.getPropertyId();
		final String label = ontologyProperty.getLabel();

		final Object value = ontologyProperty.getPropertyValue();
		final String propertyRange = ontologyProperty.getPropertyRange();
		OntologyDatatypeProperty datatypeProperty = null;
		
		if (ontologyProperty.isBooleanProperty()) {
			datatypeProperty = new OntologyBooleanPropertyImpl(propertyIri, label, propertyRange);
		} else if (ontologyProperty.isByteProperty()) {
			OntologyByteProperty ontologyByteProperty = (OntologyByteProperty)ontologyProperty;
			datatypeProperty = new OntologyBytePropertyImpl(propertyIri, ontologyByteProperty.isUnsigned(), label, propertyRange);
		} else if (ontologyProperty.isDateProperty()) {
			datatypeProperty = new OntologyDatePropertyImpl(propertyIri, label, propertyRange);
		} else if (ontologyProperty.isDateTimeProperty()) {
			datatypeProperty = new OntologyDateTimePropertyImpl(propertyIri, label, propertyRange);
		} else if (ontologyProperty.isDoubleProperty()) {
			datatypeProperty = new OntologyDoublePropertyImpl(propertyIri, label, propertyRange);
		} else if (ontologyProperty.isDurationProperty()) {
			datatypeProperty = new OntologyDurationPropertyImpl(propertyIri, label, propertyRange);
		} else if (ontologyProperty.isFloatProperty()) {
			datatypeProperty = new OntologyFloatPropertyImpl(propertyIri, label, propertyRange);
		} else if (ontologyProperty.isIntegerProperty()) {
			OntologyIntegerProperty ontologyIntegerProperty = (OntologyIntegerProperty)ontologyProperty;
			datatypeProperty = new OntologyIntegerPropertyImpl(propertyIri, ontologyIntegerProperty.isUnsigned(), 
					ontologyIntegerProperty.getSign(), ontologyIntegerProperty.isInclusiveZero(), label, propertyRange);
		} else if (ontologyProperty.isLongProperty()) {
			OntologyLongProperty ontologyLongProperty = (OntologyLongProperty)ontologyProperty;
			datatypeProperty = new OntologyLongPropertyImpl(propertyIri, ontologyLongProperty.isUnsigned(), label, propertyRange);
		} else if (ontologyProperty.isShortProperty()) {
			OntologyShortProperty ontologyShortProperty = (OntologyShortProperty)ontologyProperty;
			datatypeProperty = new OntologyShortPropertyImpl(propertyIri, ontologyShortProperty.isUnsigned(), label, propertyRange);
		} else if (ontologyProperty.isStringProperty()) {
			datatypeProperty = new OntologyStringPropertyImpl(propertyIri, label, propertyRange);
		} else  {
			datatypeProperty = new OntologyTimePropertyImpl(propertyIri, label, propertyRange);
		}
		
		datatypeProperty.setPropertyValue(value);
		return datatypeProperty;
	}
	
	/**
	 * Creates an datatype property out of an response object that has been send from the client. The object is registered with Spring.
	 * @param ontologyDatatype The response object.
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property. 
	 * @param unsigned True if the property is unsigned, false otherwise. It can be false if there is no
	 * signed or unsigned with this datatype.
	 * @param sign The sign of the property.
	 * @param inclusiveZero True if the property range contains zero, false otherwise.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created ontology property object.
	 */
	private OntologyProperty getDatatypeProperty(final OntologyDatatypeResponse ontologyDatatype, 
			final String propertyId, final String label, final String value, final boolean unsigned, final Sign sign,
			boolean inclusiveZero, String propertyRange) {
		OntologyDatatypeProperty ontologyDatatypeProperty = null;
		
		switch(ontologyDatatype) {
		case BOOLEAN:			
			ontologyDatatypeProperty = createBooleanProperty(propertyId, label, value, propertyRange);
			break;
		case BYTE:
			ontologyDatatypeProperty = createByteProperty(propertyRange, propertyId, label, value);
			break;
		case DATE:
			ontologyDatatypeProperty = createDateProperty(propertyId, label, value);
			break;
		case DATETIME:
			ontologyDatatypeProperty = createDateTimeProperty(propertyId, label, value);
			break;
		case DOUBLE:
			ontologyDatatypeProperty = createDoubleProperty(propertyId, label, value, propertyRange);
			break;
		case DURATION:
			ontologyDatatypeProperty = createDurationProperty(propertyId, label, value, propertyRange);
			break;
		case FLOAT: 
			ontologyDatatypeProperty = createFloatProperty(propertyRange, propertyId, label, value);
			break;
		case INTEGER:
			ontologyDatatypeProperty = createIntegerProperty(propertyRange, propertyId, label, unsigned, sign, inclusiveZero, value);
			break;
		case LONG:
			ontologyDatatypeProperty = createLongProperty(propertyId, label, value, unsigned, propertyRange);
			break;
		case SHORT:
			ontologyDatatypeProperty = createShortProperty(propertyId, label, value, unsigned, propertyRange);
			break;
		case STRING:
			ontologyDatatypeProperty = createStringProperty(propertyId, label, value, propertyRange);
			break;
		case TIME:
			ontologyDatatypeProperty = createTimeProperty(propertyId, label, value);
			break;
		} 
		super.initBean(ontologyDatatypeProperty);
		
		return ontologyDatatypeProperty;
	}
	
	/**
	 * Creates a new ontology property object out of an response object that has been send by the client.
	 * @param propertyResponse The response object.
	 * @return The newly created ontology property object.
	 */
	public OntologyProperty getProperty(final DatatypePropertyResponse propertyResponse) {
		final OntologyDatatypeResponse datatype = propertyResponse.getDatatype();
		OntologyProperty ontologyProperty = null;
		final String propertyId = propertyResponse.getPropertyName();
		final String label = propertyResponse.getLabel();
		final Sign sign = propertyResponse.getSign();
		final boolean unsigned = propertyResponse.isUnsigned();
		final boolean inclusiveZero = propertyResponse.isInclusiveZero();
		final String propertyRange = "";
		final String value = propertyResponse.getValue();

		ontologyProperty = getDatatypeProperty(datatype, propertyId, label, value, unsigned, sign, inclusiveZero, propertyRange);
		return ontologyProperty;
	}

	/**
	 * Creates a new property object of the datatype boolean. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype boolean.
	 */
	private OntologyBooleanProperty createBooleanProperty(final String propertyId,
			   final String label, final String value, final String propertyRange) {
		final OntologyBooleanProperty ontologyProperty = new OntologyBooleanPropertyImpl(propertyId, label, propertyRange);
		if (StringValidator.isNotNullOrEmpty(value)) {
			final Boolean booleanValue = Boolean.valueOf(value);
			ontologyProperty.setPropertyValue(booleanValue);
		}
		
		return ontologyProperty;
	}

	/**
	 * Creates a new property object of the datatype byte. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype byte.
	 */
	private OntologyByteProperty createByteProperty(final String propertyRange, final String propertyId,
			   final String label, final String value) {
		OntologyByteProperty ontologyProperty = null; 
		
		if (OntologyDatatype.isUnsignedByte(propertyRange)) {
			ontologyProperty = OntologyByteProperty.createUnsigned(propertyId, label, propertyRange);
		} else {
			ontologyProperty = OntologyByteProperty.createSigned(propertyId, label, propertyRange);
		}
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final Short byteValue = Short.valueOf(value);
			ontologyProperty.setPropertyValue(byteValue);
		} else {
			ontologyProperty.setPropertyValue((short)0);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype date. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype date.
	 */
	private OntologyDateProperty createDateProperty(final String propertyId,
			   final String label,
			   final String value) {
		final OntologyDateProperty ontologyProperty = new OntologyDatePropertyImpl(propertyId, label);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final LocalDate dateValue = DateTimeUtils.getValueAsDate(value);
			ontologyProperty.setPropertyValue(dateValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype double. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype double.
	 */
	private OntologyDoubleProperty createDoubleProperty(final String propertyId,
			   final String label,
			   final String value, final String propertyRange) {
		final OntologyDoubleProperty ontologyProperty = new OntologyDoublePropertyImpl(propertyId, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final Double doubleValue = Double.valueOf(value);
			ontologyProperty.setPropertyValue(doubleValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype datetime. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype datetime.
	 */
	private OntologyDateTimeProperty createDateTimeProperty(final String propertyId,
			   final String label,
			   final String value) {
		final OntologyDateTimeProperty ontologyProperty = new OntologyDateTimePropertyImpl(propertyId, label);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final LocalDateTime dateTimeValue = DateTimeUtils.getValueAsDateTime(value);
			ontologyProperty.setPropertyValue(dateTimeValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype duration. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype duration.
	 */
	private OntologyDatatypeProperty createDurationProperty(final String propertyId, final  String label, final String value,
			final String propertyRange) {
		final OntologyDatatypeProperty ontologyProperty = new OntologyDurationPropertyImpl(propertyId, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final OntologyDuration durationValue = OntologyDuration.fromString(value);
			ontologyProperty.setPropertyValue(durationValue);
		}
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype float. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype float.
	 */
	private OntologyFloatProperty createFloatProperty(final String propertyRange, 
			   final String propertyId,
			   final String label,
			   final String value) {
		final OntologyFloatProperty ontologyProperty = new OntologyFloatPropertyImpl(propertyId, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final Float floatValue = Float.valueOf(value);
			ontologyProperty.setPropertyValue(floatValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype integer. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype integer.
	 */
	private OntologyIntegerProperty createIntegerProperty(final String propertyRange, 
			   final String propertyId,
			   final String label,
			   final boolean unsigned,
			   final Sign sign,
			   final boolean inclusiveZero,
			   final String value) {
		OntologyIntegerProperty ontologyProperty = null;
		
		if (OntologyDatatype.isNonNegativeInteger(propertyRange)) {
			ontologyProperty = createOntologyIntegerProperty(propertyId, label, 
					true, Sign.PLUS_SIGN, true, propertyRange);
		} else if (OntologyDatatype.isNegativeInteger(propertyRange)) {
			ontologyProperty = createOntologyIntegerProperty(propertyId, label, 
					false, Sign.MINUS_SIGN, false, propertyRange);
		} else if (OntologyDatatype.isNonPositiveInteger(propertyRange)) {
			ontologyProperty = createOntologyIntegerProperty(propertyId, label, 
					false, Sign.MINUS_SIGN, true, propertyRange);
		} else if (OntologyDatatype.isPositiveInteger(propertyRange)) {
			ontologyProperty = createOntologyIntegerProperty(propertyId, label, 
					false, Sign.PLUS_SIGN, false, propertyRange);
		} else if (OntologyDatatype.isUnsignedInt(propertyRange)) {
			ontologyProperty = createOntologyIntegerProperty(propertyId, label, 
					true, Sign.MINUS_SIGN, false, propertyRange);
		} else {
			ontologyProperty = createOntologyIntegerProperty(propertyId, label, 
					false, Sign.BOTH, false, propertyRange);
		}
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final Long integerValue = Long.valueOf(value);
			ontologyProperty.setPropertyValue(integerValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype long. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype long.
	 */
	private OntologyDatatypeProperty createLongProperty(final String propertyId, final String label, 
			final String value, final boolean unsigned, final String propertyRange) {
		final OntologyDatatypeProperty ontologyProperty = new OntologyLongPropertyImpl(propertyId, unsigned, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			final BigInteger longValue = new BigInteger(value);
			ontologyProperty.setPropertyValue(longValue);
		}
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype short. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype short.
	 */
	private OntologyDatatypeProperty createShortProperty(final String propertyId, final String label, 
			final String value, final boolean unsigned, final String propertyRange) {
		final OntologyDatatypeProperty ontologyProperty = new OntologyShortPropertyImpl(propertyId, unsigned, label, propertyRange);
		if (StringValidator.isNotNullOrEmpty(value)) {
			final Integer shortValue = Integer.valueOf(value);
			ontologyProperty.setPropertyValue(shortValue);
		}
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype string. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype string.
	 */
	private static OntologyDatatypeProperty createStringProperty(final String propertyId, 
			final String label, 
			final String value, 
			final String propertyRange) {
		final OntologyDatatypeProperty ontologyProperty = new OntologyStringPropertyImpl(propertyId, label, propertyRange);
		ontologyProperty.setPropertyValue(value);
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype time. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype time.
	 */
	private OntologyDatatypeProperty createTimeProperty(final String propertyId, final String label, 
			final String value) {
		OntologyDatatypeProperty ontologyProperty = new OntologyTimePropertyImpl(propertyId, label);
		if (StringValidator.isNotNullOrEmpty(value)) {
			final LocalTime timeValue = DateTimeUtils.getValueAsTime(value);
			ontologyProperty.setPropertyValue(timeValue);
		}
		return ontologyProperty;
	}

	/**
	 * Creates a datatype property object. The object is registered with Spring.
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created datatype property object.
	 */
	public OntologyDatatypeProperty getProperty(final String propertyRange, 
											   final String propertyId,
											   final String label) {		
		boolean unsigned = OntologyDatatype.isUnsigned(propertyRange);
		OntologyDatatypeProperty ontologyDatatypeProperty = null;
		
		if (OntologyDatatype.isBoolean(propertyRange)) {
			ontologyDatatypeProperty = new OntologyBooleanPropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isByte(propertyRange)) {
			ontologyDatatypeProperty = createByteProperty(propertyRange, propertyId, label, null);
		} else if (OntologyDatatype.isDateTime(propertyRange)) {
			ontologyDatatypeProperty = new OntologyDateTimePropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isDate(propertyRange)) {
			ontologyDatatypeProperty = new OntologyDatePropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isDouble(propertyRange)) {
			ontologyDatatypeProperty = new OntologyDoublePropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isDuration(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyDurationPropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isFloat(propertyRange)) {
			ontologyDatatypeProperty =  createFloatProperty(propertyRange, propertyId, label, null);
		} else if (OntologyDatatype.isInteger(propertyRange)) {
			ontologyDatatypeProperty =  createIntegerProperty(propertyRange, propertyId, label, unsigned, Sign.BOTH, true, null);
		} else if (OntologyDatatype.isLong(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyLongPropertyImpl(propertyId, unsigned, label, propertyRange);
		} else if (OntologyDatatype.isShort(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyShortPropertyImpl(propertyId, unsigned, label, propertyRange);
		} else if (OntologyDatatype.isString(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyStringPropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isTime(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyTimePropertyImpl(propertyId, label, propertyRange);
		}  else {
			ontologyDatatypeProperty =  new OntologyStringPropertyImpl(propertyId, label, propertyRange);
		}
		super.initBean(ontologyDatatypeProperty);
		return ontologyDatatypeProperty;
	}
	
	/**
	 * Creates a new integeger property.
	 * @param propertyId The iri of the property.
	 * @param label The label that should be displayed.
	 * @param unsigned True if the integer value is unsigned.
	 * @param sign Contains the information how to tread the sign.
	 * @param inclusiveZero True if the value may contain zero.
	 * @param propertyRange The range of the property as specified in the ontology.
	 * @return The newly created integer property.
	 */
	public final OntologyIntegerProperty createOntologyIntegerProperty(final String propertyId, 
			final String label, final boolean unsigned, final Sign sign, final boolean inclusiveZero, final String propertyRange) {
		return new OntologyIntegerPropertyImpl(propertyId, unsigned, sign, inclusiveZero, label, propertyRange);
	}
}
