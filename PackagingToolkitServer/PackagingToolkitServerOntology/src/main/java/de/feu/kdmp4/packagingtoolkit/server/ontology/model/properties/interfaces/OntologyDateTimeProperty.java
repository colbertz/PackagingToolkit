package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import java.time.LocalDateTime;

public interface OntologyDateTimeProperty extends OntologyDatatypeProperty {
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public LocalDateTime getPropertyValue();
}
