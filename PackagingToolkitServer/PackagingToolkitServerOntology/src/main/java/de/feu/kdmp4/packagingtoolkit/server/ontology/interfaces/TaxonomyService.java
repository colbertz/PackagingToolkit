package de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces;

import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;

/**
 * Contains the methods with the business logic for the 
 * @author christopher
 *
 */
public interface TaxonomyService {
	/**
	 * Determines all taxonomies that are referenced in the base ontology and defined with the help of SKOS.
	 * @return Contains all taxonomies with all individuals ready for being send via http.
	 */
	TaxonomyListResponse getAllTaxonomies();
	/**
	 * Sets a reference to an object with the operations for processing taxonomies.
	 * @param taxonomyOperations The reference to an object with the operations for processing taxonomies. 
	 */
	void setTaxonomyOperations(TaxonomyOperations taxonomyOperations);
}
