package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.InformationPackageIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

// TODO Diese Klasse koennte unnoetig sein.
public class InformationPackageIndividualsHashMapImpl implements InformationPackageIndividualsMap {
	private Map<Uuid, OntologyIndividualCollection> individualMap;
	
	public InformationPackageIndividualsHashMapImpl() {
		individualMap = new HashMap<>();
	}
	
	@Override
	public void addIndividual(OntologyIndividual ontologyIndividual) {
		Uuid uuid = ontologyIndividual.getUuid();
		OntologyIndividualCollection individualList = individualMap.get(uuid);
		
		if (individualList == null) {
			individualList = OntologyModelFactory.createEmptyOntologyIndividualList();
		}
		
		individualList.addIndividual(ontologyIndividual);
	}
	
	@Override
	public void addIndividuals(Uuid uuid, OntologyIndividualCollection individualList) {
		OntologyIndividualCollection individualListOfInformationPackage = individualMap.get(uuid);
		
		if (individualListOfInformationPackage == null) {
			individualListOfInformationPackage = OntologyModelFactory.createEmptyOntologyIndividualList();
		}
		individualListOfInformationPackage.appendIndividualList(individualList);
	}

	@Override
	public OntologyIndividualCollection getOntologyIndividuals(Uuid uuid) {
		OntologyIndividualCollection individualList = individualMap.get(uuid);
		return individualList;
	}
}
