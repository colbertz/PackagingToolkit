package de.feu.kdmp4.packagingtoolkit.server.ontology;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.springframework.context.annotation.Bean;

import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.ConfigurationOperationsPropertiesImpl;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyOperationsJenaImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyService;

// DELETE_ME
//@Configuration
public class TestConfiguration {
	private static final String CONFIG_FILE_PATH = "config.properties";
	/**
	 * The property in the property file that contains the name of the 
	 * base ontology. The base ontology lies in the configuration directory
	 * of the server.
	 */
	private static final String PROPERTY_BASE_ONTOLOGY = "baseOntology";
	/**
	 * The property in the property file that contains the name of the 
	 * base ontology class. This is the class whose subclasses contain the meta 
	 * information that has to be entered by the user.
	 */
	private static final String PROPERTY_BASE_ONTOLOGY_CLASS = "baseOntologyClass";
	/**
	 * The property in the property file that contains the name of the 
	 * rule ontology. The rule ontology lies in the configuration directory
	 * of the server.
	 */
	private static final String PROPERTY_RULE_ONTOLOGY = "ruleOntology";
	/**
	 * The property for the working directory in the configuration file.
	 */
	private static final String PROPERTY_WORKING_DIRECTORY = "workingDirectory";
	private static final String TEST_BASE_ONTOLOGY = "baseOntology.owl";
	private static final String TEST_BASE_ONTOLOGY_CLASS = "Information_Package";
	private static final String TEST_RULE_ONTOLOGY = "ruleOntology.owl";
	private static final String TEST_WORKING_DIRECTORY_PATH = "work";
	
	@Bean(name = "ontologyService")
	public OntologyService getOntologyService() {
		return new OntologyServiceImpl();
	}
	
	@Bean(name = "ontologyOperations")
	public OntologyOperations getOntologyOperations() {
		return new OntologyOperationsJenaImpl();
	}
	
	@Bean(name = "ontologyCache")
	public OntologyCache getOntologyCache() {
		File configFile = new File(CONFIG_FILE_PATH);
		writeTestConfiguration(configFile);
		return new OntologyCache();
	}
	
	@Bean(name = "configurationOperations")
	public ConfigurationOperations getConfigurationOperations() {
		return new ConfigurationOperationsPropertiesImpl();
	}
	
	private void writeTestConfiguration(File propertiesFile) {
		try {
			propertiesFile.createNewFile();
		}catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		
		try (FileInputStream fileInputStream = new FileInputStream(propertiesFile);) {
			Properties properties = new Properties();
			// Load the old properties.
			properties.load(fileInputStream); 
			properties.setProperty(PROPERTY_WORKING_DIRECTORY, TEST_WORKING_DIRECTORY_PATH);
			properties.setProperty(PROPERTY_BASE_ONTOLOGY, TEST_BASE_ONTOLOGY);
			properties.setProperty(PROPERTY_RULE_ONTOLOGY, TEST_RULE_ONTOLOGY);
			properties.setProperty(PROPERTY_BASE_ONTOLOGY_CLASS, TEST_BASE_ONTOLOGY_CLASS);
			try (FileOutputStream fileOutputStream = new FileOutputStream(propertiesFile)) {
				properties.store(fileOutputStream, "");
			}
		}catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	/*@Bean(name = "jerseyConfig")
	public JerseyConfig getJerseyConfig() {
		return new JerseyConfig();
	}*/
	
	/*@Bean
	public ConfigurationCommunication getConfigurationCommunication() {
		return new ConfigurationCommunication();
	}*/
}
