package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;


import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyAnnotationPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;

/**
 * Creates annotation property objects.
 * @author Christopher Olbertz
 *
 */
public class AnnotationPropertyFactory {
	/**
	 * The constructor is private because there are only static methods in this class.
	 */
	private AnnotationPropertyFactory() {
	}
	
	/**
	 * Creates an object for an annotation property.
	 * @param namespace The namespace of the property. This is the of the iri before the # symbol.
	 * @param localName The local name of the property. This is the of the iri after the # symbol.
	 * @return An annotation property with the given namspace and local name;
	 */
	public static final OntologyAnnotationProperty createAnnotationProperty(final Namespace namespace, final LocalName localName) {
		final OntologyAnnotationProperty annotationProperty = new OntologyAnnotationPropertyImpl(namespace, localName);
		return annotationProperty;
	}
}
