package de.feu.kdmp4.packagingtoolkit.server.ontology.facades;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyService;

// DELETE_ME Wird anscheinend nicht benoetigt.
public class SessionOntologyFacade {
	private OntologyService ontologyService;
	
	public void setOntologyService(OntologyService ontologyService) {
		this.ontologyService = ontologyService;
	}
}
