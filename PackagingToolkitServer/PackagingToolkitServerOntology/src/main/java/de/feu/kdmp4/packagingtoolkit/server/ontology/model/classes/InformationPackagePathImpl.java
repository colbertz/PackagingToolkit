package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import java.util.ArrayList;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.InformationPackagePath;

// DELETE_ME Das scheine ich hier nicht zu brauchen.
public class InformationPackagePathImpl implements InformationPackagePath {
	private static final String SEPARATOR = "/";
	
	private List<String> pathComponents;
	
	
	public InformationPackagePathImpl() {
		pathComponents = new ArrayList<>();
	}
	
	@Override
	public void addPathComponent(String pathComponent) {
		pathComponents.add(pathComponent);
	}
	
	@Override
	public void deleteLastPathComponent() {
		pathComponents.remove(pathComponents.size() - 1);
	}
	
	@Override
	public int getComponentCount() {
		return pathComponents.size();
	}
	
	@Override
	public String toString() {
		String result = "";
		for (String aString: pathComponents) {
			result = result + aString + SEPARATOR;
		}
		
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pathComponents == null) ? 0 : pathComponents.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InformationPackagePathImpl other = (InformationPackagePathImpl) obj;
		if (pathComponents == null) {
			if (other.pathComponents != null)
				return false;
		} else if (!pathComponents.equals(other.pathComponents))
			return false;
		return true;
	}
}
