package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Iterator;
import java.util.Optional;

import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.xsd.impl.XSDByteType;
import org.apache.jena.datatypes.xsd.impl.XSDDateTimeType;
import org.apache.jena.datatypes.xsd.impl.XSDDateType;
import org.apache.jena.datatypes.xsd.impl.XSDDurationType;
import org.apache.jena.datatypes.xsd.impl.XSDTimeType;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.ConversionException;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.FunctionalProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.InverseFunctionalProperty;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.reasoner.ValidityReport.Report;
import org.apache.jena.shared.JenaException;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.SKOS;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.AnnotationPropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ObjectPropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.AnnotationPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyElement;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyAnnotationProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.utils.LogUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

	
/**
 * Contains the implementation of 
 * {@link de.feu.kdmp4.packagingtoolkit.SerializationOntologyOperations.operations.interfaces.OntologyOperations}
 * with Apache Jena. Further information be found here: 
 * {@linkplain https://jena.apache.org}.
 * @author Christopher Olbertz
 *
 */
public class OntologyOperationsJenaImpl implements OntologyOperations {
	/**
	 * Contains the ontology for processing with Jena.
	 */
	private OntModel ontModel;
	/**
	 * Contains the Premis ontology for processing with Jena.
	 */
	private OntModel premisModel;
	/**
	 * Contains the SKOS ontology for processing with Jena.
	 */
	private OntModel skosModel;
	/**
	 * The file that contains the ontology.
	 */
	private File ontologyFile;
	/**
	 * The factory that creates properties for the ontology.
	 */
	private PropertyFactory propertyFactory;
	/**
	 * The factory that creates model objects. 
	 */
	private OntologyModelFactory ontologyModelFactory;
	/**
	 * The factory that creates object properties. 
	 */
	private ObjectPropertyFactory objectPropertyFactory;
	/**
	 * Contains the business logic for processing the taxonomies.
	 */
	private TaxonomyOperations taxonomyOperations;
	
	@Override
	public OntologyClass getHierarchy() {
		// TODO Hier noch als Namespace z.B. die Ontology IRI eintragen.
		return this.getHierarchy("Thing");
	}
	
	@Override
	public OntologyClass getHierarchy(final String informationPackageClassName) {
		final OntClass informationPackageClass = searchClass(informationPackageClassName);
		final LocalName rootLocalName = ServerModelFactory.createLocalName(informationPackageClass.getLocalName());
		final String namespaceAsString = informationPackageClass.getNameSpace();
		final Namespace namespace = ServerModelFactory.createNamespace( StringUtils.trimLastSharp(namespaceAsString));
		final OntologyClass rootClass = ontologyModelFactory.createOntologyClass(namespace, rootLocalName);
		
		determineLabels(rootClass, informationPackageClass);
		addSubclasses(rootClass, informationPackageClass);
		
		return rootClass;
		
	}

	@Override
	public OntologyClass getPremisHierarchy() {
		final Namespace namespace = ServerModelFactory.createNamespace("Premis");
		final LocalName localName = ServerModelFactory.createLocalName("Premis");
		final OntologyClass premisRootClass = ontologyModelFactory.createOntologyClass(namespace, localName);
		final ExtendedIterator<OntClass> iterator = premisModel.listHierarchyRootClasses();
		//final OntClass ontClass = null;
		boolean hasNext = true;

		/*
		 * The method iterator.hasNext can throw a ConversionException. If thrown, the for loop is cancelled. To avoid this,
		 * we can use this construction. hasNext() is not called in the loop itself but its result is saved in the local variable
		 * hasNext and this variable is used for the loop.  
		 */
		while (hasNext) {
			try {
				hasNext = iterator.hasNext();
				if (hasNext) {
					final OntClass nextClass = iterator.next();
					addSubclasses(premisRootClass, nextClass);
				}
			} catch (ConversionException ex) {
				/* Nothing is done because we have to catch this exception only
					 for avoiding the cancellation of the loop. */
			}
		}
		
		return premisRootClass;
	}
	
	@Override
	public OntologyClass getSkosHierarchy() {
		Namespace namespace = ServerModelFactory.createNamespace("Skos");
		LocalName localName = ServerModelFactory.createLocalName("Skos");
		final OntologyClass skosRootClass = ontologyModelFactory.createOntologyClass(namespace, localName);
		final ExtendedIterator<OntClass> iterator = skosModel.listHierarchyRootClasses();
		//final OntClass ontClass = null;
		boolean hasNext = true;

		/*
		 * The method iterator.hasNext can throw a ConversionException. If thrown, the for loop is cancelled. To avoid this,
		 * we can use this construction. hasNext() is not called in the loop itself but its result is saved in the local variable
		 * hasNext and this variable is used for the loop.  
		 */
		while (hasNext) {
			try {
				hasNext = iterator.hasNext();
				if (hasNext) {
					final OntClass nextClass = iterator.next();
					addSubclasses(skosRootClass, nextClass);
					
					String localNameAsString = nextClass.getLocalName();
					String namespaceAsString = nextClass.getNameSpace();
					
					if (StringValidator.isNotNullOrEmpty(namespaceAsString) && StringValidator.isNotNullOrEmpty(localNameAsString)) {
						if (namespaceAsString.endsWith("#")) {
							namespaceAsString = StringUtils.deleteLastCharacter(namespaceAsString);
						}
						
						namespace = ServerModelFactory.createNamespace(namespaceAsString);
						localName = ServerModelFactory.createLocalName(localNameAsString);
						OntologyClass subClass = ontologyModelFactory.createOntologyClass(namespace, localName);
						skosRootClass.addSubClass(subClass);
					}
				}
			} catch (ConversionException ex) {
				/* Nothing is done because we have to catch this exception only
					 for avoiding the cancellation of the loop. */
			}
		}
		
		return skosRootClass;
	}
	
	/**
	 * Determines the subclasses of an OntClass object and adds them to an OntologyClass object.
	 * @param rootClass The class the subclasses are added to.
	 * @param informationPackageClass The class that is dependant from Jena and whose subclasses are extracted.
	 * @return The class that is independant from Jena with its subclasses extracted from the Jena class.
	 */
	private OntologyClass addSubclasses(final OntologyClass rootClass, final OntClass informationPackageClass) {
		
		final ExtendedIterator<OntClass> iterator = informationPackageClass.listSubClasses();
		
		while(iterator.hasNext()) {
			final OntClass ontClass = iterator.next();
			rootClass.addSubClass(getHierarchy(ontClass, rootClass));
			determineLabels(rootClass, ontClass);
		}
		return rootClass;
	}
	
	/**
	 * Looks for an class with a given name. 
	 * @param classname The name of the class to look for.
	 * @return The found class with the given name.
	 * @throws InformationPackageClassNotFoundException Is thrown if the information
	 * package class is not found in the ontology. In this case, it is not possible
	 * to work with this ontology.
	 */
	private OntClass searchClass(final String classname) {
		final ExtendedIterator<OntClass> iterator = ontModel.listHierarchyRootClasses();
		OntClass ontClass = null;
		
		while (iterator.hasNext() && ontClass == null) {
			final OntClass nextClass = iterator.next();
			ontClass = lookForClass(classname, nextClass);
			System.out.println(nextClass);
		}
		
		return ontClass;
	}
	
	@Override
	public void startBaseOntologyProcessing(final File fileWithOntology) {
		/* Creation of a specification for OWL models that are stored 
		 * in memory and do no additional entailment reasoning.
		 */
		ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		final String ontologyPath = fileWithOntology.getAbsolutePath();
		try (InputStream inputStream = FileManager.get().open(ontologyPath)){
			ontModel.read(inputStream, null);
			//final Resource skosConcept = SKOS.Concept;
			//Resource skosResource = SKOS.Concept;
			//ontModel.add(SKOS.Concept, RDFS.Class, SKOS.Concept);			
			this.ontologyFile = fileWithOntology;
		} catch (JenaException jex) {
			jex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void startPremisOntologyProcessing(final File fileWithPremisOntology) {
		premisModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		final String ontologyPath = fileWithPremisOntology.getAbsolutePath();
		try (InputStream inputStream = FileManager.get().open(ontologyPath)){
			premisModel.read(inputStream, null);
			//this.ontologyFile = fileWithPremisOntology;
		} catch (JenaException jex) {
			jex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void startSkosOntologyProcessing(final File fileWithSkosOntology) {
		skosModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		final String ontologyPath = fileWithSkosOntology.getAbsolutePath();
		try (InputStream inputStream = FileManager.get().open(ontologyPath)){
			skosModel.read(inputStream, null);
			ontModel.add(skosModel);
			//this.ontologyFile = fileWithSkosOntology;
		} catch (JenaException jex) {
			jex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Converts an individual object that is independent from Jena to an object of the Jena Framework. 
	 * @param ontologyIndividual The individual that we want to convert.
	 * @param ontModel The ont model that contains the base ontology.
	 * @return The individual as Jena class.
	 */
	private Individual convertIndividual(final OntologyIndividual ontologyIndividual, final OntModel ontModel)  {
		final OntologyClass ontologyClass = ontologyIndividual.getOntologyClass();
		final String ontologyClassName = ontologyClass.getFullName();
		final OntClass jenaOntclass = ontModel.getOntClass(ontologyClassName); 
		final Individual jenaIndividual = ontModel.createIndividual(jenaOntclass);
		
		for (int i = 0; i < ontologyIndividual.getDatatypePropertiesCount(); i++) {
			final OntologyDatatypeProperty ontologyDatatypeProperty = ontologyIndividual.getDatatypePropertyAt(i);
			final String datatypePropertyUri = ontologyDatatypeProperty.toString();
			final DatatypeProperty datatypeProperty = ontModel.createDatatypeProperty(datatypePropertyUri);
			final Literal literal = createLiteral(ontologyDatatypeProperty, ontModel);
			if (literal != null) {
				jenaIndividual.addProperty(datatypeProperty, literal);
			}
		}

		LogUtils.logDebug("Individual converted to Jena: " + jenaIndividual);
		return jenaIndividual;
	}
	
	/**
	 * Creates a literal with the value of a property that is independend from Jena.
	 * @param ontologyDatatypeProperty The property contains the datatype and the value of the literal. 
	 * @param ontModel The model needed for creating the literal objects.
	 * @return The created literal.
	 */
	private Literal createLiteral(final OntologyDatatypeProperty ontologyDatatypeProperty, final OntModel ontModel) {
		Literal literal = null;
		final Object propertyValue = ontologyDatatypeProperty.getPropertyValue();
		
		if (propertyValue != null) {
			if (ontologyDatatypeProperty.isBooleanProperty()) {
				final Boolean booleanValue = (Boolean)propertyValue;
				literal = ontModel.createTypedLiteral(booleanValue);
			} else if (ontologyDatatypeProperty.isByteProperty()) {
				final Short byteValue = (Short)propertyValue;
				final RDFDatatype byteType = new XSDByteType("byte");
				//if (byteType.isValidValue(byteValue)) {
					literal = ontModel.createTypedLiteral(byteValue, byteType);
				/*} else {
					literal = ontModel.createTypedLiteral(byteType);
				}*/
			} else if (ontologyDatatypeProperty.isDateProperty()) {
				final LocalDate localDateValue = (LocalDate)propertyValue; 
				final RDFDatatype dateType = new XSDDateType("date");
				if(dateType.isValidValue(localDateValue)) {
					literal = ontModel.createTypedLiteral(localDateValue, dateType);
				}
			} else if (ontologyDatatypeProperty.isDateTimeProperty()) {
				final LocalDateTime localDateTimeValue = (LocalDateTime)propertyValue; 
				final RDFDatatype dateTimeType = new XSDDateTimeType("dateTime");
				if(dateTimeType.isValidValue(localDateTimeValue)) {
					literal = ontModel.createTypedLiteral(localDateTimeValue, dateTimeType);
				}
			} else if (ontologyDatatypeProperty.isDoubleProperty()) {
				final Double doubleValue = (Double)propertyValue;
				literal = ontModel.createTypedLiteral(doubleValue);
			} else if (ontologyDatatypeProperty.isDurationProperty()) {
				final OntologyDuration ontologyDuration = (OntologyDuration)propertyValue;
				final String durationValueAsString = ontologyDuration.toString();
				final RDFDatatype durationType = new XSDDurationType();
				if(durationType.isValidValue(durationValueAsString)) {
					literal = ontModel.createTypedLiteral(durationValueAsString, durationType);
				}
			} else if (ontologyDatatypeProperty.isFloatProperty()) {
				final Float floatValue = (Float)propertyValue;
				literal = ontModel.createTypedLiteral(floatValue);
			} else if (ontologyDatatypeProperty.isIntegerProperty()) {
				final Long integerValue = (Long)propertyValue;
				literal = ontModel.createTypedLiteral(integerValue);
			} else if (ontologyDatatypeProperty.isLongProperty()) {
				final BigInteger longValue = (BigInteger)propertyValue;
				literal = ontModel.createTypedLiteral(longValue);
			} else if (ontologyDatatypeProperty.isShortProperty()) {
				final Integer shortValue = (Integer)propertyValue;
				literal = ontModel.createTypedLiteral(shortValue);
			} else if (ontologyDatatypeProperty.isStringProperty()) {
				final String stringValue = (String)propertyValue;
				literal = ontModel.createTypedLiteral(stringValue);
			} else {
				if (propertyValue != null) {
					final LocalTime localTimeValue = (LocalTime)propertyValue;
					final RDFDatatype timeType = new XSDTimeType("time");
					if (timeType.isValidValue(localTimeValue)) {
						literal = ontModel.createTypedLiteral(localTimeValue, timeType);
					}
				} 
			}
		} 
		
		LogUtils.logDebug("Literal created: " + literal);
		return literal;
	}
	
	/**
	 * Validates an OntModel object with the help of a reasoner. 
	 */
	/* TODO Vorsicht: Das Individual sollte nicht fest in das OntModel eingetragen werden. Am besten lade ich
	 * die Ontologie neu und schicke das OntModel spaeter zum Teufel. Oder ich verwende das originale Ontmodel und
	 * entferne das Individuum nach dem validieren noch einmal. Ich koennte auch fuer jedes IP ein eigenes Ontmodel
	 * speichern. Das muesste dann in die Session rein. Das ist aber insofern problematisch, dass ich die Jena-Klassen
	 * ja nicht ausserhalb dieser Klasse OntologyOperationsJenaImpl bekannt machen moechte. Die validierten Individuals
	 * muss ich natuerlich im IP eintragen. 
	 * 
	 * Am schlauesten waere vermutlich wirklich: neu laden, das Individuum in die Ontologie eintragen und das OntModel
	 * anschliessend verwerfen.
	 */
	@Override
	// REFACTORIZE_ME
	public void validateIndividual(final OntologyIndividual ontologyIndividual) {
		final OntModel ontModelForReasoning = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);

		// TODO Doppelter Code, auch in startOntologyProcessing().
		final String ontologyPath = ontologyFile.getAbsolutePath();
		try (InputStream inputStream = FileManager.get().open(ontologyPath)){
			ontModelForReasoning.read(inputStream, null);
		} catch (JenaException jex) {
			jex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final Individual individual = convertIndividual(ontologyIndividual, ontModelForReasoning);
		ontModelForReasoning.createIndividual(individual);
		//Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
		final InfModel infModel = ModelFactory.createRDFSModel(ontModelForReasoning);
		
		ValidityReport validityReport = infModel.validate();
		
		if (!validityReport.isValid()) {
			final StringList reasonerMessages = PackagingToolkitModelFactory.getStringList();
			final Iterator<Report> iterator = validityReport.getReports();
			while (iterator.hasNext()) {
				final String message = iterator.next().toString();
				reasonerMessages.addStringToList(message);
		    }
			
			LogUtils.logDebug("Reasoner Validation failed: " + reasonerMessages);
			throw OntologyReasonerException.createIndividualInvalidException(reasonerMessages);
		}
	}
	
	@Override
	public void validateIndividuals(final OntologyIndividualCollection individuals) {
		for (int i = 0; i < individuals.getIndiviualsCount(); i++) {
			OntologyIndividual ontologyIndividual = individuals.getIndividual(i);
			validateIndividual(ontologyIndividual);
		}
	}

	/**
     * Determines the class hierarchy for the given ontology from this class down. 
     * Makes no attempt to deal sensibly with multiple inheritance. 
     * @param ontClass The Jena class that has been created with the information from the ontology.
     * @param ontologyClass The values from ontClass are transfered into this object. It contains the
     * hierarchy so far.
	 * @return The top class of the hierarchy. It contains subclasses and provides
	 * access to the entire hierarchy. 
     */
    private OntologyClass getHierarchy(final OntClass ontClass, 
    								   final OntologyClass ontologyClass)  {
    	final String localNameAsString = ontClass.getLocalName();
    	String namespaceAsString = ontClass.getNameSpace();
    	namespaceAsString = StringUtils.trimLastSharp(namespaceAsString);
    	
		Namespace namespace = ServerModelFactory.createNamespace(namespaceAsString);
		LocalName localName = ServerModelFactory.createLocalName(localNameAsString);
		
    	final OntologyClass childOntologyClass = ontologyModelFactory.createOntologyClass
							(namespace, localName);
    	LogUtils.logDebug("Class found in ontology: " + childOntologyClass.toString());
    	for (Iterator<OntClass> iterator = ontClass.listSubClasses(); 
	    			iterator.hasNext();) {
	    		final OntClass child = iterator.next();
	    		childOntologyClass.addSubClass(getHierarchy(child, childOntologyClass));
	    	}

    	determineLabels(childOntologyClass, ontClass);
	    return childOntologyClass;
    }
    
    /**
     * Looks recursivly for a class with a given name in the ontology.
     * @param classname The classname to look for.
     * @param ontClass The ontClass object this recursion step starts with.
     * @return The found ontology class or null, if no class with the given
     * name was found. 
     */
    private OntClass lookForClass(final String classname, final OntClass ontClass) {
		final String childLocalName = ontClass.getLocalName();
    	System.out.println(childLocalName);
		
		if (childLocalName != null) {
	    	if (childLocalName.equals(classname)) {
	    		return ontClass;
			}
	    	
	    	for (final Iterator<OntClass> iterator = ontClass.listSubClasses(); 
	    			iterator.hasNext();) {
	    		
	    		try {
		    		if (ontClass.hasSubClass()) {
		    			final OntClass child = iterator.next();
		    			lookForClass(classname, child);
		    		}
	    		} catch (ConversionException ex) {
	    			LogUtils.logError("Conversion error in OntologyOperationsJenaImpl.lookForClass()");
	    		}
	    	}
		}
    	
    	return null;
    }
    
	@Override
	public ObjectPropertyCollection extractObjectProperties(OntologyClassesMap ontologyClassesMap) {
		final ExtendedIterator<ObjectProperty> iterator = ontModel.listObjectProperties(); 
		ObjectPropertyCollection objectPropertyList = ontologyModelFactory.createEmptyObjectPropertyList();
		
		while (iterator.hasNext()) {
			ObjectProperty objectProperty = iterator.next();
			/*
			 * Super properties do not have a domain and a range. They are 
			 *  are skipped while the properties are processed.
			 */
			if (objectProperty.getDomain() != null && objectProperty.getRange() != null)  {
				final String propertyId = objectProperty.getNameSpace() + objectProperty.getLocalName();
				final String propertyRange = objectProperty.getRange().toString();
				final String propertyDomain = objectProperty.getDomain().toString();
				// The the label in the default language.
				final String defaultLabel = objectProperty.getLabel(null);
				OntologyObjectProperty ontologyObjectProperty = objectPropertyFactory.getObjectProperty(propertyId, 
						defaultLabel, propertyRange, propertyDomain, ontologyClassesMap);
				determineLabels(ontologyObjectProperty, objectProperty);
				if (ontologyObjectProperty != null) {
					//ontologyCache.addObjectProperty(propertyRange, ontologyObjectProperty);
					objectPropertyList.addObjectProperty(ontologyObjectProperty);
				}
			}
		}
		
		LogUtils.logDebug("Extracted ObjectProperties : " + objectPropertyList.toString());
		return objectPropertyList;
	}
	
	// REFACTORIZE_ME
	@Override
	public Optional<OntologyProperty> findPropertyByIri(String iri, OntologyClassesMap ontologyClassesMap) {
		final ExtendedIterator<DatatypeProperty> iterator = ontModel.listDatatypeProperties();

		while(iterator.hasNext()) {
			DatatypeProperty datatypeProperty = iterator.next(); 
			
			/*
			 * Super properties do not have a domain and a range. They are 
			 *  are skipped while the properties are processed.
			 */
			if (datatypeProperty.getDomain() != null && datatypeProperty.getRange() != null)  {
				final String className = datatypeProperty.getDomain().toString(); 
				final String propertyRange = datatypeProperty.getRange().toString();
				//String label = null;
				
				/*if (language != null) {
					label = datatypeProperty.getLabel(language.getLanguageCode());
				} else {
					label = datatypeProperty.getLabel(null);
				}*/

				
				final String propertyId = datatypeProperty.getNameSpace() + datatypeProperty.getLocalName();
				// The the label in the default language.
				final String defaultLabel = datatypeProperty.getLabel(null); 
				final OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(
						propertyRange, propertyId, defaultLabel);
				
				// Look for the label text in each language.
				for (final OntologyLabelLanguage language: OntologyLabelLanguage.values()) {
					if (!language.equals(OntologyLabelLanguage.DEFAULT)) {
						final String languageCode = language.getLanguageCode();
						final String label = datatypeProperty.getLabel(languageCode);
						// Is there a label in the desired language?
						if (label != null) {
							//ontologyProperty.addLabel(label, language);
						}
					}
				}
				
				// Add the property to a class.
				if (ontologyProperty != null) {
					if (StringUtils.areStringsEqual(iri, propertyId)) {
						return OntologyOptionalFactory.createOntologyPropertyOptional(ontologyProperty);
					}
				}
			}
		}
		
		final ExtendedIterator<ObjectProperty> objectPropertyIterator = ontModel.listObjectProperties();
		while(objectPropertyIterator.hasNext()) {
			ObjectProperty objectProperty = objectPropertyIterator.next();
			if (objectProperty.getDomain() != null && objectProperty.getRange() != null) {
				final String className = objectProperty.getDomain().toString(); 
				final String propertyRange = objectProperty.getRange().toString();
				final String defaultLabel = objectProperty.getLabel(null); 
				final String propertyDomain = objectProperty.getDomain().toString();
				
				final String propertyId = objectProperty.getNameSpace() + objectProperty.getLocalName();
				final OntologyObjectProperty ontologyObjectProperty = objectPropertyFactory.getObjectProperty(propertyId, defaultLabel, propertyRange, 
						propertyDomain, ontologyClassesMap);
				
				// Add the property to a class.
				if (ontologyObjectProperty != null) {
					if (StringUtils.areStringsEqual(iri, propertyId)) {
						LogUtils.logDebug("ObjectProperty with iri " + iri + " has been found!");
						return OntologyOptionalFactory.createOntologyPropertyOptional(ontologyObjectProperty);
					}
				}
			}
		}
		
		LogUtils.logDebug("ObjectProperty with iri " + iri + " has not been found!");
		return OntologyOptionalFactory.createEmptyOntologyPropertyOptional();
	}
	
	private AnnotationPropertyCollection extractAnnotationProperties(final OntModel ontModel) {
		final ExtendedIterator<AnnotationProperty> iterator = ontModel.listAnnotationProperties();
		final AnnotationPropertyCollection annotationPropertyCollection = ontologyModelFactory.createEmptyAnnotationPropertyCollection();
		
		while (iterator.hasNext()) {
			final AnnotationProperty annotationProperty = iterator.next();
			LocalName localName = ServerModelFactory.createLocalName(annotationProperty.getLocalName()); 
			Namespace namespace = ServerModelFactory.createNamespace(annotationProperty.getNameSpace());
			
			OntologyAnnotationProperty ontologyAnnotationProperty = AnnotationPropertyFactory.createAnnotationProperty(namespace, localName);
			annotationPropertyCollection.addAnnotationProperty(ontologyAnnotationProperty);
		}
		
		return annotationPropertyCollection;
	}
	
	private DatatypePropertyCollection extractDatatypeProperties(final OntModel ontModel) {
		final ExtendedIterator<DatatypeProperty> iterator = ontModel.listDatatypeProperties();
		DatatypePropertyCollection datatypePropertyList = ontologyModelFactory.createEmptyDatatypePropertyList();
		
		while (iterator.hasNext()) {
			DatatypeProperty datatypeProperty = iterator.next(); 
			
			/*
			 * Super properties do not have a domain and a range. They are 
			 *  are skipped while the properties are processed.
			 */
			if (datatypeProperty.getDomain() != null && datatypeProperty.getRange() != null)  {
				final String className = datatypeProperty.getDomain().toString(); 
				final String propertyRange = datatypeProperty.getRange().toString();
				final String propertyId = datatypeProperty.getNameSpace() + datatypeProperty.getLocalName();
				// The the label in the default language.
				final String defaultLabel = datatypeProperty.getLabel(null); 
				final OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(
						propertyRange, propertyId, defaultLabel); 
				// Look for the domains.
				ExtendedIterator<? extends OntResource> domainIterator = datatypeProperty.listDomain();
				while (domainIterator.hasNext()) { 
					OntResource domain = domainIterator.next(); 
					String iriOfDomain = domain.toString();
					if (!iriOfDomain.contains("#")) {
						iriOfDomain = StringUtils.replaceLast(iriOfDomain, '/', '#');
					}
					ontologyProperty.addPropertyDomain(iriOfDomain);
				}
				
				determineLabels(ontologyProperty, datatypeProperty);
				// Look for the label text in each language.
				/*for (final OntologyLabelLanguage language: OntologyLabelLanguage.values()) {
					if (!language.equals(OntologyLabelLanguage.DEFAULT)) {
						final String languageCode = language.getLanguageCode();
						final String label = datatypeProperty.getLabel(languageCode);
						// Is there a label in the desired language?
						if (label != null) {
							ontologyProperty.addLabel(label, language);
						}
					}
				}*/
				
				LogUtils.logDebug("Class " + className + " with property " + propertyId);
				LogUtils.logDebug("Range of the property: " + propertyRange);
				LogUtils.logDebug("Domain of the property: " + datatypeProperty.getDomain().toString());
				// Add the property to a class.
				if (ontologyProperty != null) {
					//ontologyCache.addDatatypeProperty(className, ontologyProperty);
					datatypePropertyList.addDatatypeProperty(ontologyProperty);
				}
			}
		}
		return datatypePropertyList;
	}
	
	/**
	 * Adds the labels in all languages to an element of an ontology. 
	 * @param ontologyElement The element that should get the labels.
	 * @param ontResource The resource that has been read from the ontology and that contains the 
	 * information about the labels. 
	 */
	private void determineLabels(OntologyElement ontologyElement, OntResource ontResource) {
		if (ontologyElement != null) {
			for (final OntologyLabelLanguage language: OntologyLabelLanguage.values()) {
				if (!language.equals(OntologyLabelLanguage.DEFAULT)) {
					final String languageCode = language.getLanguageCode();
					final String label = ontResource.getLabel(languageCode);
					// Is there a label in the desired language?
					if (label != null) {
						LogUtils.logDebug("Label in language " + languageCode + "was found: " + label);
						ontologyElement.addLabel(label, language);
					}
				}
			}
		}
	}
	
	private DatatypePropertyCollection extractFunctionalProperties(final OntModel ontModel) {
		final ExtendedIterator<FunctionalProperty> iterator = ontModel.listFunctionalProperties();
		DatatypePropertyCollection datatypePropertyList = ontologyModelFactory.createEmptyDatatypePropertyList();
		
		while (iterator.hasNext()) {
			FunctionalProperty functionalProperty = iterator.next(); 
			
			/*
			 * Super properties do not have a domain and a range. They are 
			 *  are skipped while the properties are processed.
			 */
			if (functionalProperty.getDomain() != null && functionalProperty.getRange() != null)  {
				final String className = functionalProperty.getDomain().toString(); 
				final String propertyRange = functionalProperty.getRange().toString();
				//String label = null;
				/*if (language != null) {
					label = datatypeProperty.getLabel(language.getLanguageCode());
				} else {
					label = datatypeProperty.getLabel(null);
				}*/

				
				final String propertyId = functionalProperty.getNameSpace() + functionalProperty.getLocalName();
				// The the label in the default language.
				final String defaultLabel = functionalProperty.getLabel(null); 
				final OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(
						propertyRange, propertyId, defaultLabel); 
				// Look for the domains.
				ExtendedIterator<? extends OntResource> domainIterator = functionalProperty.listDomain();
				while (domainIterator.hasNext()) {
					OntResource domain = domainIterator.next(); 
					String iriOfDomain = domain.toString();
					if (!iriOfDomain.contains(PackagingToolkitConstants.NAMESPACE_SEPARATOR)) {
						iriOfDomain = StringUtils.replaceLast(iriOfDomain, '/', '#');
					}
					ontologyProperty.addPropertyDomain(iriOfDomain);
				}
				
				// Look for the label text in each language.
				for (final OntologyLabelLanguage language: OntologyLabelLanguage.values()) {
					if (!language.equals(OntologyLabelLanguage.DEFAULT)) {
						final String languageCode = language.getLanguageCode();
						final String label = functionalProperty.getLabel(languageCode);
						// Is there a label in the desired language?
						if (label != null) {
							//ontologyProperty.addLabel(label, language);
						}
					}
				}
				
				/* TODO If-Anweisung nur waehrend Entwicklung von Bedeutung. Spaeter wieder raus
				 * Grund: Bei dem Property mit dem regulaeren Ausdruck wird hier ein null
				 * als Property ausgespuckt.
				 */
				LogUtils.logDebug("Class " + className + " with property " + propertyId);
				LogUtils.logDebug("Range of the property: " + propertyRange);
				LogUtils.logDebug("Domain of the property: " + functionalProperty.getDomain().toString());
				// Add the property to a class.
				if (ontologyProperty != null) {
					//ontologyCache.addDatatypeProperty(className, ontologyProperty);
					datatypePropertyList.addDatatypeProperty(ontologyProperty);
				}
			}
		}
		return datatypePropertyList;
	}
	
	private DatatypePropertyCollection extractInverseFunctionalProperties(final OntModel ontModel) {
		final ExtendedIterator<InverseFunctionalProperty> iterator = ontModel.listInverseFunctionalProperties();
		DatatypePropertyCollection datatypePropertyList = ontologyModelFactory.createEmptyDatatypePropertyList();
		
		while (iterator.hasNext()) {
			InverseFunctionalProperty inverseFunctionalProperty = iterator.next(); 
			
			/*
			 * Super properties do not have a domain and a range. They are 
			 *  are skipped while the properties are processed.
			 */
			if (inverseFunctionalProperty.getDomain() != null && inverseFunctionalProperty.getRange() != null)  {
				final String className = inverseFunctionalProperty.getDomain().toString(); 
				final String propertyRange = inverseFunctionalProperty.getRange().toString();
				//String label = null;
				/*if (language != null) {
					label = datatypeProperty.getLabel(language.getLanguageCode());
				} else {
					label = datatypeProperty.getLabel(null);
				}*/

				
				final String propertyId = inverseFunctionalProperty.getNameSpace() + inverseFunctionalProperty.getLocalName();
				// The the label in the default language.
				final String defaultLabel = inverseFunctionalProperty.getLabel(null); 
				final OntologyDatatypeProperty ontologyProperty = propertyFactory.getProperty(
						propertyRange, propertyId, defaultLabel); 
				// Look for the domains.
				ExtendedIterator<? extends OntResource> domainIterator = inverseFunctionalProperty.listDomain();
				while (domainIterator.hasNext()) {
					OntResource domain = domainIterator.next(); 
					String iriOfDomain = domain.toString();
					if (!iriOfDomain.contains("#")) {
						iriOfDomain = StringUtils.replaceLast(iriOfDomain, '/', '#');
					}
					ontologyProperty.addPropertyDomain(iriOfDomain);
				}
				
				// Look for the label text in each language.
				for (final OntologyLabelLanguage language: OntologyLabelLanguage.values()) {
					if (!language.equals(OntologyLabelLanguage.DEFAULT)) {
						final String languageCode = language.getLanguageCode();
						final String label = inverseFunctionalProperty.getLabel(languageCode);
						// Is there a label in the desired language?
						if (label != null) {
							//ontologyProperty.addLabel(label, language);
						}
					}
				}
				
				LogUtils.logDebug("Class " + className + " with property " + propertyId);
				LogUtils.logDebug("Range of the property: " + propertyRange);
				LogUtils.logDebug("Domain of the property: " + inverseFunctionalProperty.getDomain().toString());
				// Add the property to a class.
				if (ontologyProperty != null) {
					//ontologyCache.addDatatypeProperty(className, ontologyProperty);
					datatypePropertyList.addDatatypeProperty(ontologyProperty);
				}
			}
		}
		return datatypePropertyList;
	}
	
	@Override
	public DatatypePropertyCollection extractDatatypeProperties() {
		DatatypePropertyCollection datatypeProperties = extractDatatypeProperties(ontModel);
		DatatypePropertyCollection premisDatatypeProperties = extractDatatypeProperties(premisModel);
		DatatypePropertyCollection premisFunctionalProperties = extractFunctionalProperties(premisModel);
		DatatypePropertyCollection premisInverseFunctionalProperties = extractInverseFunctionalProperties(premisModel);
		datatypeProperties.addDatatypeProperties(premisDatatypeProperties);
		datatypeProperties.addDatatypeProperties(premisFunctionalProperties);
		datatypeProperties.addDatatypeProperties(premisInverseFunctionalProperties);
		
		return datatypeProperties;
	}
	
	@Override
	public AnnotationPropertyCollection extractAnnotationProperties() {
		return extractAnnotationProperties(skosModel);
	}
	
	/*@SuppressWarnings("deprecation")
	@Override
	public void savePropertyValues(final OntologyLiteralList ontologyLiteralList) {
		for (int i = 0; i < ontologyLiteralList.getLiteralsCount(); i++) {
			final OntologyLiteral ontologyLiteral = ontologyLiteralList.getLiteral(i);
			final String propertyName = ontologyLiteral.getName();
			final String propertyValue = ontologyLiteral.getValue();
			final ExtendedIterator<DatatypeProperty> datatypePropertyIterator = 
					ontModel.listDatatypeProperties();
			
			while(datatypePropertyIterator.hasNext()) {
				final DatatypeProperty datatypeProperty = datatypePropertyIterator.
						next();

				if (datatypeProperty.getRange() != null) {
					if (datatypeProperty.getLocalName().equals(propertyName)) {
						final OntResource domain = datatypeProperty.getDomain();
						/*
						 * The method addLiteral(Resource, Property, Object) is deprecated. The methods
						 * with typed literals should be preferred. But here it is not possible to determine
						 * typed literals.  
						 */
						/*ontModel.addLiteral(domain, datatypeProperty, propertyValue);
					}
				}
			}
		}
	}*/
	
	@Override
	public void saveOntology() {
		try (FileOutputStream fileOutputStream = new FileOutputStream(ontologyFile);
			final OutputStream outputStream = new BufferedOutputStream(fileOutputStream)) {
			ontModel.write(outputStream);
		} catch(IOException ex) {
			// TODO Eigene Exception werfen.
		}
	}
	
	/**
	 * Gets an individual of PackagingToolkit and replaces its iri that has been created while the creation of the 
	 * individual object with the iri that has been extracted from the ontology. This is only done if an iri has been
	 * extracted from the ontology.
	 * @param individual The individual that is independant from the Ontology framework Jena.
	 * @param ontologyIndividual The individual extracted by Jena.
	 */
	private void replaceDefaultIri(final Individual individual, final OntologyIndividual ontologyIndividual) {
		final String uri = individual.getURI();
		if (StringValidator.isNotNullOrEmpty(uri)) {
			Iri iriOfIndividual = ServerModelFactory.createIri(uri);
			ontologyIndividual.setIri(iriOfIndividual);
		} 
	}
	
	@Override
	public OntologyIndividualCollection getIndividualsFromOntology(OntologyClassesMap ontologyClassesMap) {
		final ExtendedIterator<Individual> individualIterator = ontModel.listIndividuals();
		final OntologyIndividualCollection individualList = OntologyModelFactory.createEmptyOntologyIndividualList();
		final Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getDefaultInformationPackageUuid();
		
		while(individualIterator.hasNext()) {
			 try {
				 final Individual individual = individualIterator.next();
				 
				// Create the ontology class of this individual.
				 final OntClass ontClass = individual.getOntClass();
				 final String namespace = ontClass.getNameSpace();
				 final String localName = ontClass.getLocalName();
				 final String nameWithNamespace = namespace + localName;
				 final OntologyClass ontologyClass = ontologyModelFactory.createOntologyClass(nameWithNamespace);
				// Create the individual.
				 final OntologyIndividual ontologyIndividual = ontologyModelFactory.createOntologyIndividual(ontologyClass, uuidOfInformationPackage);
				replaceDefaultIri(individual, ontologyIndividual);
				
				// Determine the properties and their values.
				final StmtIterator propertyIterator = individual.listProperties();
				while (propertyIterator.hasNext()) {
					final Statement statement = propertyIterator.next();
					//processSkosStatement(statement);
					
					final Triple triple = statement.asTriple();
					if (triple.getObject().isLiteral()) {
						final Node predicate = triple.getPredicate();
						final Object value = triple.getObject().getLiteralValue();
						final String propertyName = predicate.getNameSpace() + predicate.getLocalName();
						final Optional<OntologyProperty> ontologyPropertyOptional = findPropertyByIri(propertyName, ontologyClassesMap);
						if (ontologyPropertyOptional.isPresent()) {
							final OntologyProperty ontologyProperty = ontologyPropertyOptional.get();
							
							if (ontologyProperty instanceof OntologyDatatypeProperty) {
								final OntologyDatatypeProperty ontologyDatatypeProperty = (OntologyDatatypeProperty)ontologyProperty;
								ontologyDatatypeProperty.setPropertyValue(value);
								ontologyIndividual.addDatatypeProperty(ontologyDatatypeProperty);
								LogUtils.logDebug("Datatype property " + ontologyDatatypeProperty.toString() + " added to "
										+ "individual " + ontologyIndividual.toString());
							} else {
								final OntologyObjectProperty ontologyObjectProperty = (OntologyObjectProperty)ontologyProperty;
								ontologyObjectProperty.setPropertyValue(value);
								ontologyIndividual.addObjectProperty(ontologyObjectProperty);
								LogUtils.logDebug("Object property " + ontologyObjectProperty.toString() + " added to "
										+ "individual " + ontologyIndividual.toString());
							}
						}
					} else { 
						final Node predicate = triple.getPredicate();
						final Object value = triple.getObject();
						final String propertyName = predicate.getNameSpace() + predicate.getLocalName();
						final Optional<OntologyProperty> ontologyPropertyOptional = findPropertyByIri(propertyName, ontologyClassesMap);
						if (ontologyPropertyOptional.isPresent()) {
							final OntologyProperty ontologyProperty = ontologyPropertyOptional.get();
							if (ontologyProperty instanceof OntologyObjectProperty) {
								final OntologyObjectProperty ontologyObjectProperty = (OntologyObjectProperty)ontologyProperty;
								ontologyObjectProperty.setPropertyValue(value);
								ontologyIndividual.addObjectProperty(ontologyObjectProperty);
							}
						} else {
							final ObjectProperty objectProperty = ontModel.getObjectProperty(propertyName);
							if (objectProperty != null) {
								String propertyRange = null;
								if (objectProperty.getRange() == null) {
									propertyRange = StringUtils.EMPTY_STRING;
								} else {
									propertyRange = objectProperty.getRange().toString();
								}
								String propertyDomain = null;
								if (objectProperty.getDomain() == null) {
									propertyDomain = StringUtils.EMPTY_STRING;
								} else {
									propertyDomain = objectProperty.getDomain().toString();
								}
								// The the label in the default language.
								final String defaultLabel = objectProperty.getLabel(null);
								final OntologyObjectProperty ontologyObjectProperty = objectPropertyFactory.getObjectProperty(propertyName, 
										defaultLabel, propertyRange, propertyDomain, ontologyClassesMap);
								ontologyObjectProperty.setPropertyValue(value);
								ontologyIndividual.addObjectProperty(ontologyObjectProperty);
							}
						}
					}
				}
				
				taxonomyOperations.addIndividualToTaxonomy(individual);
				
				individualList.addIndividual(ontologyIndividual);
			  } catch (ConversionException ex) {
					
			  }
		}

		return individualList;
	}

	private void processSkosStatement(final Statement statement) {
		Resource concept = SKOS.Concept;
		RDFNode res = statement.getObject(); 
		System.out.println(concept.getLocalName());
		
		if (res.isResource()) {
			System.out.println(res.asResource().getLocalName());
			if (concept.getLocalName().equals(res.asResource().getLocalName())) {
				System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
			}	
		}
		
		
		/*Resource conceptScheme = SKOS.ConceptScheme;
		Resource res2 = statement.getPredicate(); 
		if (conceptScheme.getLocalName().equals(res2.getLocalName())) {
			System.out.println("????????????????????????????????????????????????????");
		}*/
		
	}
	
	@Override
	public void setPropertyFactory(PropertyFactory propertyFactory) {
		this.propertyFactory = propertyFactory;
	}

	@Override
	public void setOntologyModelFactory(OntologyModelFactory ontologyModelFactory) {
		this.ontologyModelFactory = ontologyModelFactory;
	}
	
	@Override
	public void setBaseOntologyFile(File ontologyFile) {
		this.ontologyFile = ontologyFile;
	}
	
	@Override
	public void setObjectPropertyFactory(ObjectPropertyFactory objectPropertyFactory) {
		this.objectPropertyFactory = objectPropertyFactory;
	}
	
	@Override
	public void setTaxonomyOperations(final TaxonomyOperations taxonomyOperations) {
		this.taxonomyOperations = taxonomyOperations;
	}
}
