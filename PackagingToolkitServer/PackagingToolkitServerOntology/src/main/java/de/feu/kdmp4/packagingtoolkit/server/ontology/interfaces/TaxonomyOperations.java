package de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;

public interface TaxonomyOperations {
	/**
	 * Adds an individual that has been extracted by a framework to a taxonomy.
	 * The the individual is a root individual of a taxonomy a new taxonomy is
	 * created. 
	 * @param objectWithIdividual The individual extracted by a framework.
	 */
	void addIndividualToTaxonomy(Object objectWithIdividual);
	/**
	 * Determines a taxonomy in the collection at a certain position.
	 * @param index The position we are looking at.
	 * @return The found taxonomy or an empty optional.
	 */
	Optional<Taxonomy> getTaxonomy(int index);
	/**
	 * Determines the number of taxonomies in this collection.
	 * @return The number of taxonomies in this collection.
	 */
	int getTaxonomyCount();
}
