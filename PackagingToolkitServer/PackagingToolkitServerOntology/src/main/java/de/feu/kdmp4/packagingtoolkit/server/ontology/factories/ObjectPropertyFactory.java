package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.SKOS;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;

/**
 * Creates objects for object properties.
 * @author Christopher Olbertz
 *
 */
public class ObjectPropertyFactory {
	/**
	 * Creates an object property object.
	 * @param propertyIri The iri of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created object property object.
	 */
	public OntologyObjectProperty getObjectProperty(final String propertyIri, final String label, 
			final String propertyRange, final String propertyDomain, final OntologyClassesMap ontologyClassesMap) {

		OntologyClass rangeOntologyClass = ontologyClassesMap.getOntologyClass(propertyRange);
		final OntologyClass domainOntologyClass = ontologyClassesMap.getOntologyClass(propertyDomain);
		OntologyObjectProperty ontologyObjectProperty = null;
		
		if (rangeOntologyClass == null) {
			if (propertyRange.contains(SKOS.ConceptScheme.getLocalName())) {
				final Resource conceptScheme = SKOS.ConceptScheme;
				final Namespace namespace = ServerModelFactory.createNamespace(conceptScheme.getNameSpace());
				final LocalName localname = ServerModelFactory.createLocalName(conceptScheme.getLocalName());
				rangeOntologyClass = new OntologyClassImpl(namespace, localname);
			} else if (propertyRange.contains(SKOS.Concept.getLocalName())) {
				final Resource conceptScheme = SKOS.Concept;
				final Namespace namespace = ServerModelFactory.createNamespace(conceptScheme.getNameSpace());
				final LocalName localname = ServerModelFactory.createLocalName(conceptScheme.getLocalName());
				rangeOntologyClass = new OntologyClassImpl(namespace, localname);
			}
		}
		
		ontologyObjectProperty = new OntologyObjectPropertyImpl(propertyIri, label, rangeOntologyClass, propertyRange, domainOntologyClass);
		return ontologyObjectProperty;
	}
}
