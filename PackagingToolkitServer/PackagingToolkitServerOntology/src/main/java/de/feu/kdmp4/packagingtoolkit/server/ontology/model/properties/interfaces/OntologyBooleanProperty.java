package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

/**
 * An ontology property with a boolean value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyBooleanProperty extends OntologyDatatypeProperty {
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public Boolean getPropertyValue();
}
