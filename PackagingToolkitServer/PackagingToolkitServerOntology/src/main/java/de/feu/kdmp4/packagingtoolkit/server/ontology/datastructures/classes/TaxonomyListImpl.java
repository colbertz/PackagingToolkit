package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;

public class TaxonomyListImpl extends AbstractList implements TaxonomyCollection {
	@Override
	public void addTaxonomy(final Taxonomy taxonomy) {
		super.add(taxonomy);
	}
	
	@Override
	public Optional<Taxonomy> getTaxonomy(final int index) {
		Taxonomy taxonomy = (Taxonomy)super.getElement(index);
		return OntologyOptionalFactory.createOptionalWithTaxonomy(taxonomy);
	}
	
	@Override
	public int getTaxonomyCount() {
		return super.getSize();
	}
	
	@Override
	public Optional<Taxonomy> findTaxonomyByNamespace(final Namespace namespace) {
		for (int i = 0; i < getSize(); i++) {
			final Taxonomy taxonomy = (Taxonomy)getElement(i);
			final Namespace thisNamespace = taxonomy.getNamespace();
			if (thisNamespace.equals(namespace)) {
				final Optional<Taxonomy> optionalWithTaxonomy = OntologyOptionalFactory.createOptionalWithTaxonomy(taxonomy);
				return optionalWithTaxonomy;
			}
		}
		
		return OntologyOptionalFactory.createEmptyOptionalWithTaxonomy();
	}
}
