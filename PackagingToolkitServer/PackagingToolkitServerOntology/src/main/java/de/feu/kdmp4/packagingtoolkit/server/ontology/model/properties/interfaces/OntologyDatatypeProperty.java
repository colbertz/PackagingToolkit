package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.Restriction;

/**
 * Represents a datatype property.
 * @author Christopher Olbertz
 *
 */
public interface OntologyDatatypeProperty extends OntologyProperty {
	/**
	 * Returns the label that should be shown in the graphical user interface.
	 */
	public String getLabel();
	/**
	 * Determines if the property is a boolean property.
	 * @return True if the property is a boolean property, false otherwise.
	 */
	boolean isBooleanProperty();
	/**
	 * Determines if the property is a byte property.
	 * @return True if the property is a byte property, false otherwise.
	 */
	boolean isByteProperty();
	/**
	 * Determines if the property is a date property.
	 * @return True if the property is a date property, false otherwise.
	 */
	boolean isDateProperty();
	/**
	 * Determines if the property is a datetime property.
	 * @return True if the property is a datetime property, false otherwise.
	 */
	boolean isDateTimeProperty();
	/**
	 * Determines if the property is a double property.
	 * @return True if the property is a double property, false otherwise.
	 */
	boolean isDoubleProperty();
	/**
	 * Determines if the property is a duration property.
	 * @return True if the property is a duration property, false otherwise.
	 */
	boolean isDurationProperty();
	/**
	 * Determines if the property is a float property.
	 * @return True if the property is a float property, false otherwise.
	 */
	boolean isFloatProperty();
	/**
	 * Determines if the property is an integer property.
	 * @return True if the property is an integer property, false otherwise.
	 */
	boolean isIntegerProperty();
	/**
	 * Determines if the property is a long property.
	 * @return True if the property is a long property, false otherwise.
	 */
	boolean isLongProperty();
	/**
	 * Determines if the property is a short property.
	 * @return True if the property is a short property, false otherwise.
	 */
	boolean isShortProperty();
	/**
	 * Determines if the property is a string property.
	 * @return True if the property is a string property, false otherwise.
	 */
	boolean isStringProperty();
	/**
	 * Determines if the property is a time property.
	 * @return True if the property is a time property, false otherwise.
	 */
	boolean isTimeProperty();
	/**
	 * Checks if a certain value satisfies the restrictions of this property. Throws an exception
	 * if a restriction is violated.
	 * @param value The value that has to satisfy the restrictions.
	 * @throws RestrictionNotSatisfiedException If a restriction is not satisfied.
	 */
	void checkRestrictions(final RestrictionValue value);
	/**
	 * Checks if a certain value satisfies the restrictions of this property.
	 * @param value The value that has to satisfy the restrictions.
	 * @return True if the restrictions are satisfied, false otherwise.
	 */
	boolean areRestrictionsSatisfied(final RestrictionValue value);
	/**
	 * Determines a restriction at a given position.
	 * @param index The position we want to look.
	 * @return The found restriction.
	 */
	Restriction getRestrictionAt(final int index);
}
