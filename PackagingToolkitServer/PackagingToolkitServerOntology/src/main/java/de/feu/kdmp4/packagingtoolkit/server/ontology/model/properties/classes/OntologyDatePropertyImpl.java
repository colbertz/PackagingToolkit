package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import java.time.LocalDate;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

public class OntologyDatePropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDateProperty {

	/**
	* 
	*/
	private static final long serialVersionUID = 1748033123603000410L;

	/**
	 * Creates a new property.
	 */
	public OntologyDatePropertyImpl() {
	}
	
	/**
	 * Constructs a new property with a value.
	 * @param propertyId The iri of the property.
	 * @param value The value of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDatePropertyImpl(final String propertyId, final LocalDate value, final String label, final String range) {
		super(propertyId, value, label, range);
	}

	/**
	 * Constructs a new property without a value.
	 * @param propertyId The iri of the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDatePropertyImpl(final String propertyId, final String range) {
		super(propertyId, null, null, range);
	}
	
	/**
	 * Constructs a new property without a value.
	 * @param propertyId The iri of the property.
	 * @param label The label for displaying the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyDatePropertyImpl(final String propertyId, final String label, final String range) {
		super(propertyId, null, label, range);
	}

	@Override
	public LocalDate getPropertyValue() {
		return (LocalDate) getValue();
	}

	@Override
	public boolean isDateProperty() {
		return true;
	}
	
	@Override
	public String toString() {
		return DateTimeUtils.getLocalDateAsOntologyString(getPropertyValue());
	}
}
