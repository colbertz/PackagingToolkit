package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyPropertyException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyElementImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * This class is the implementation of an ontology property. It is an abstract class 
 * and has to be implemented for each type of property, e.g. a boolean property or
 *  an integer property.
 * @author Christopher Olbertz
 *
 */  
public abstract class OntologyPropertyImpl extends OntologyElementImpl  implements OntologyProperty {
	private static final long serialVersionUID = 8440424325812921650L;
	private static final String CONSTRUCTOR_STRING_OBJECT = "OntologyPropertyImpl(String label, Object value)";
	protected static final String DECIMAL_SEPARATOR_DE = ",";
	protected static final String DECIMAL_SEPARATOR_EN = ".";

	/**
	 * The label that is shown in the gui.
	 */
	private String label;
	/**
	 * Contains the identifier of this property from the ontology. If the property does
	 * not have a label, propertyId is shown in the gui as output text.
	 */
	private String propertyId;
	/**
	 * The range of this property as defined in the ontology. Is needed for cloning
	 * this property. The cloning is easier if the range can be used. 
	 */
	private String propertyRange;
	/**
	 * The domain of the property. This is used to determine the class without the need saving an object
	 * of OntologyClass.
	 */
	private StringList propertyDomains;
	/**
	 * The value of the property. The subclasses control the range of this attribute.
	 */
	private Object value;
	
	private LocalName localName;
	
	private Uuid uuid;
	/* TODO
	 *  Namespace ermitteln. Generell steht er als Praefix vor dem lokalen Namen. Kein Praefix oder Namespace-Angabe
	 *  heisst, dass der Standard-Namespace verwendet wird. Implementiert werden muss also noch eine Moeglichkeit,
	 *  die Namespaces zu ermitteln und die Praefixe den Namensraeumen zuzuordnen. 
	 */
	private Namespace namespace;

	/**
	 * Constructs an empty property.
	 */
	public OntologyPropertyImpl() {
		uuid = new Uuid();
		propertyDomains = PackagingToolkitModelFactory.getStringList();
	}

	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param value The value of the property.
	 * @param label The label that represents the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyPropertyImpl(final String propertyId, final Object value,  
			final String label, final String range) {
		this();
		if (StringValidator.isNullOrEmpty(propertyId)) {
			final OntologyPropertyException exception = OntologyPropertyException.
					propertyIdMayNotBeNull(this.getClass().getName(), 
							CONSTRUCTOR_STRING_OBJECT);
			throw exception;
		}
		this.propertyId = propertyId;
		if (StringValidator.isNullOrEmpty(label)) {
			this.label = propertyId;
		} else {
			this.label = label;
		}
		
		this.propertyRange = StringUtils.deleteDoubleSharps(range);
		this.value = value;
	}
	
	/**
	 * Creates a new property.
	 * @param namespace The namespace of the property. This is the part in the iri before the # symbol.
	 * @param localName The local name of the property. This is the part in the iri after the # symbol.
	 */
	public OntologyPropertyImpl(final Namespace namespace, final LocalName localName) {
		this();
		this.namespace = namespace;
		this.localName = localName;
	}

	@Override
	public String toString() {
		return propertyId;
	}
	
	@Override
	public boolean hasValue() {
		if (value != null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		final PropertyFactory propertyFactory = new PropertyFactory();
		final OntologyProperty ontologyPropertyClone = propertyFactory.cloneOntologyProperty(this);
		
		for (int i = 0; i < propertyDomains.getSize(); i++) {
			final String propertyDomain = propertyDomains.getStringAt(i);
			ontologyPropertyClone.addPropertyDomain(propertyDomain);
		}
		
		return ontologyPropertyClone;
	}
	
	protected Object getValue() {
		return value;
	}
	
	protected void setValue(Object value) {
		this.value = value;
	}
	
	@Override
	public OntologyProperty cloneProperty() {
		try {
			return (OntologyProperty)clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((propertyId == null) ? 0 : propertyId.hashCode());
		result = prime * result + ((propertyRange == null) ? 0 : propertyRange.hashCode());
		//result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyPropertyImpl other = (OntologyPropertyImpl) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		if (propertyId == null) {
			if (other.propertyId != null)
				return false;
		} else if (!propertyId.equals(other.propertyId))
			return false;
		if (propertyRange == null) {
			if (other.propertyRange != null)
				return false;
		} else if (!propertyRange.equals(other.propertyRange))
			return false;
		return true;
	}

	public static String getConstructorStringObject() {
		return CONSTRUCTOR_STRING_OBJECT;
	}

	@Override
	public String getPropertyId() {
		return propertyId;
	}
	
	public abstract Object getPropertyValue();
	
	@Override
	public String getLabel() {
		return label;
	}
	
	@Override
	public boolean isObjectProperty() {
		return false;
	}

	@Override
	public String getPropertyRange() {
		return propertyRange;
	}
	
	@Override
	public Uuid getUuid() {
		return uuid;
	}
	
	@Override
	public Namespace getNamespace() {
		return namespace;
	}
	
	@Override
	public void addPropertyDomain(final String propertyDomain) {
		this.propertyDomains.addStringToList(propertyDomain);
	}
	
	@Override
	public int getPropertyDomainsCount() {
		return propertyDomains.getSize();
	}
	
	@Override
	public String getPropertyDomainAt(final int index) {
		return propertyDomains.getStringAt(index);
	}
	
	@Override
	public LocalName getLocalName() {
		return localName;
	}
}