package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.IrisTaxonomyIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;

public class IrisAndTaxonomyIndividualsHashMapImpl implements IrisTaxonomyIndividualsMap {
	/**
	 * Contains the iri as keys and the objects as values.
	 */
	private Map<Iri, TaxonomyIndividualCollection> taxonomyIndividualsMap;
	
	public IrisAndTaxonomyIndividualsHashMapImpl() {
		taxonomyIndividualsMap = new HashMap<>();
	}
	
	@Override
	public void addTaxonomyIndividal(final Iri iri, final TaxonomyIndividual taxonomyIndividual) {
		TaxonomyIndividualCollection taxonomyIndividualCollection = taxonomyIndividualsMap.get(iri);
		
		if (taxonomyIndividualCollection == null) {
			taxonomyIndividualCollection = OntologyModelFactory.createEmptyTaxonomyIndividualCollection();
			taxonomyIndividualsMap.put(iri, taxonomyIndividualCollection);
		}
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual);		
	}
	
	@Override
	public Optional<TaxonomyIndividualCollection> getTaxonomyIndividuals(final Iri iri) {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = taxonomyIndividualsMap.get(iri);
		return OntologyOptionalFactory.createTaxonomyIndividualCollectionOptional(taxonomyIndividualCollection);
	}
}
