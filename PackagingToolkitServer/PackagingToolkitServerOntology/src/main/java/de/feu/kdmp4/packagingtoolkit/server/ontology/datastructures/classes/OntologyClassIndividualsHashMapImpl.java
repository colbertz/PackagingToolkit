package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyPrototypeFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

public class OntologyClassIndividualsHashMapImpl implements OntologyClassIndividualsMap {
	/**
	 * Contains the iri of a class as key and the individuals of this class as value.
	 */
	private Map<String, OntologyIndividualCollection> individualMap;
	
	public OntologyClassIndividualsHashMapImpl() {
		individualMap = new HashMap<>();
	}
	
	@Override
	public OntologyIndividual findIndividualByUuid(final Uuid uuidOfIndividual) {
		final OntologyIndividualCollection individuals = toOntologyIndividualList();
		for (int i = 0; i < individuals.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = individuals.getIndividual(i);
			if (ontologyIndividual.getUuid().equals(uuidOfIndividual)) {
				return ontologyIndividual;
			}
		}
		
		return null;
	}
	
	@Override
	public boolean containsIndividual(final OntologyIndividual ontologyIndividual) {
		final String className = ontologyIndividual.getOntologyClass().getFullName();
		final OntologyIndividualCollection individualList = individualMap.get(className);
		
		if (individualList == null) {
			return false;
		}
		
		if (individualList.containsIndividual(ontologyIndividual)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void addOrUpdateIndividual(final OntologyIndividual ontologyIndividual) {
		final String className = ontologyIndividual.getOntologyClass().getFullName();
		OntologyIndividualCollection individualList = individualMap.get(className);
		
		if (individualList == null) {
			individualList = OntologyModelFactory.createEmptyOntologyIndividualList();
		}
		
		if (individualList.containsIndividual(ontologyIndividual)) {
			individualList.replaceIndividual(ontologyIndividual);
		} else {
				individualList.addIndividual(ontologyIndividual);
		}
		individualMap.put(className, individualList);
	}
	
	@Override
	public void addIndividuals(final String className, final OntologyIndividualCollection individualList) {
		OntologyIndividualCollection individualListOfClass = individualMap.get(className);
		
		if (individualListOfClass == null) {
			individualListOfClass = OntologyPrototypeFactory.createEmptyIndividualList();
		}
		individualListOfClass.appendIndividualList(individualList);
	}

	@Override
	public OntologyIndividualCollection getOntologyIndividuals(final String className) {
		OntologyIndividualCollection individualList = individualMap.get(className);
		
		if (individualList == null) {
			// TODO new raus
			individualList = new OntologyIndividualCollectionImpl();
		}
		
		return individualList;
	}
	
	@Override
	public OntologyIndividualCollection toOntologyIndividualList() {
		final OntologyIndividualCollection ontologyIndividualList = new OntologyIndividualCollectionImpl();
		
		final Collection<OntologyIndividualCollection> listFromMap = individualMap.values();
		
		for (OntologyIndividualCollection individualListFromMap: listFromMap) {
			ontologyIndividualList.appendIndividualList(individualListFromMap);
		}
		
		return ontologyIndividualList;
	}
	
	@Override
	public int getEntryCount() {
		return individualMap.size();
	}
	
	@Override
	public void removeIndividualByUuid(final Uuid uuidOfIndividual) {
		final OntologyClass ontologyClass = findClassByIndividualUuid(uuidOfIndividual);
		final OntologyIndividual ontologyIndividual = findIndividualByUuid(uuidOfIndividual);
		
		if (ontologyIndividual != null && ontologyClass != null) {
			final OntologyIndividualCollection ontologyIndividualList = individualMap.get(ontologyClass.getFullName());
			ontologyIndividualList.removeIndividual(ontologyIndividual);
		}
	}
	
	@Override
	public OntologyClass findClassByIndividualUuid(final Uuid uuidOfIndividual) {
		final OntologyIndividual ontologyIndividual = findIndividualByUuid(uuidOfIndividual);
		if (ontologyIndividual != null) {
			return ontologyIndividual.getOntologyClass();
		} else {
			return null;
		}
	}
}
