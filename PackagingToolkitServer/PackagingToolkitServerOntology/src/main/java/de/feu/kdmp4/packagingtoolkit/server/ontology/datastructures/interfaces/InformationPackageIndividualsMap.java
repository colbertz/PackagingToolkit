package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

/**
 * Assigns a list with individuals to an information package. The information
 * package is identified by its uuid.
 * @author Christopher Olbertz
 *
 */
public interface InformationPackageIndividualsMap {
	/**
	 * Adds an individual to the individual list of an information package.
	 * @param uuid The uuid of the information package where the individual is added to.
	 * @param individual The individual that should be added to the information package.
	 */
	public void addIndividual(OntologyIndividual ontologyIndividual);
	/**
	 * Gets the individual list of a given information package.
	 * @param uuid The uuid of the information package.
	 * @return The individual list of the class. If there are no individuals
	 * the return value is an empty individual list.
	 */
	OntologyIndividualCollection getOntologyIndividuals(Uuid uuid);
	/**
	 * Adds an individual list to an information package.
	 * @param uuid The uuid of the information package where the individuals are added to.
	 * @param individuals The individuals that should be added to the information package.
	 */
	void addIndividuals(Uuid uuid, OntologyIndividualCollection individualList);
}
