package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyPrototypeFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class OntologyClassesHashMap implements OntologyClassesMap {
	/**
	 * Contains the iris of ontology classes as keys and the respective objects as values. 
	 */
	private Map<String, OntologyClass> ontologyMap;
	
	public OntologyClassesHashMap() {
		ontologyMap = new HashMap<>();
	}
	
	@Override
	public void addClass(final OntologyClass ontologyClass) {
		/* 
		 * The namespace of premis classes end with a slash. Therefore we first delete the # sign,
		 * then we check if the namespace ends with / and if so, we delete it.
		 */
		String namespace = ontologyClass.getNamespace().toString();
		
		if (namespace.endsWith("/")) {
			namespace = StringUtils.deleteLastCharacter(namespace);
			ontologyClass.setNamespace(ServerModelFactory.createNamespace(namespace));
		}
		String className = ontologyClass.getFullName();
		ontologyMap.put(className, ontologyClass);
	}
	
	@Override
	public int getOntologyClassesCount() {
		return ontologyMap.size();  
	}
	
	@Override
	public void addProperty(final String className, final OntologyProperty ontologyProperty) {
		OntologyClass ontologyClass = ontologyMap.get(className); 
		/* Insert the property if the associated class was found in the map.
		   If the associated class was not found in the map, the class is
		   not a sub class of Information_Package and then it has not to be
		   regarded in this process. */
		if (ontologyClass == null) {
			ontologyClass = OntologyPrototypeFactory.createNewOntologyClass();
		}
		
		if (ontologyProperty instanceof OntologyDatatypeProperty) {
			OntologyDatatypeProperty ontologyDatatypeProperty = (OntologyDatatypeProperty)ontologyProperty;
			ontologyClass.addDatatypeProperty(ontologyDatatypeProperty);
		}
		
		ontologyMap.put(className, ontologyClass);
	}
	
	@Override
	public OntologyClass getOntologyClass(final String className) {
		return ontologyMap.get(className);
	}
}
