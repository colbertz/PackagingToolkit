/**
 * Contains classes that describe restrictions in an ontology.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;