package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;

/**
 * An element that is extracted from a ontology. Here, the compositum pattern is
 * used. The class, that is implementing OntologyElement, is the superclass of 
 * all classes that are involved into compositum. The interface 
 * {@link de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.OntologyClass}
 * represents the class that contains other objects of subclasses of OntologyElement.
 * @author Christopher Olbertz
 *
 */
public interface OntologyElement {
	public String getLabel();
	/**
	 * Adds a new label text to this element.
	 * @param label The label in a certain language.
	 */
	void addLabel(final TextInLanguage label);
	/**
	 * Adds a new label text to this element.
	 * @param labelText The text of the label.
	 * @param language The language the label is written in.
	 */
	void addLabel(String labelText, OntologyLabelLanguage language);
	/**
	 * Determines the number of texts that are associated to this element as labels.
	 * @return The number of texts that are associated to this element as labels.
	 */
	int getLabelCount();
	/**
	 * The label at a given position.
	 * @param index The position we are looking at.
	 * @return The label that has been found at index.
	 */
	TextInLanguage getLabelAt(int index);
}
