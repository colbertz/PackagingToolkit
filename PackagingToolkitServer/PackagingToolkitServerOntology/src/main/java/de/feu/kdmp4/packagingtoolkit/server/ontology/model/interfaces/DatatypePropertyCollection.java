package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;


import de.feu.kdmp4.packagingtoolkit.models.interfaces.ListInterface;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;

/**
 * A collection with properties.
 * @author Christopher Olbertz
 *
 */
public interface DatatypePropertyCollection extends ListInterface {
	/**
	 * Gets a property at a given index of the list.
	 * @param index The index.
	 * @return The property found at the index.
	 */
	OntologyDatatypeProperty getDatatypeProperty(final int index);
	/**
	 * Adds a property to the list.
	 * @param property The property that should be appended to the list.
	 */
	void addDatatypeProperty(final OntologyDatatypeProperty property);
	/**
	 * Adds some datatype properties to this list. 
	 * @param datatypeProperties The properties that are added.
	 */
	void addDatatypeProperties(final DatatypePropertyCollection datatypeProperties);
	/**
	 * Determines the number of properties in the collection.
	 * @return The number of properties.
	 */
	int getPropertiesCount();
	/**
	 * Determines if there are any objects in the collection.
	 * @return True if the collection is empty, false otherwise.
	 */
	boolean isEmpty();
}
