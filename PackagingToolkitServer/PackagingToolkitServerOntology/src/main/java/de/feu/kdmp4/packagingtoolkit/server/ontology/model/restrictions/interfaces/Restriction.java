package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;

/**
 * Represents a restriction in an ontology. A restriction contains a value that describes the restriction.
 * The restriction is checked against this value. 
 * <br />
 * Example: A MinInclusiveRestriction with the value 5 is only satisfied with values below and inclusive 5.
 * @author Christopher Olbertz
 *
 */
@SuppressWarnings("rawtypes")
public interface Restriction {
	/**
	 * Checks if the restriction is satisfied.
	 * @param value The value that has to satisfy the restriction.
	 * @return True if the restriction is satisfied.
	 */
	boolean isSatisfied(RestrictionValue value);
	/**
	 * Checks if the restriction is satisfied. Throws an exception if the restriction has been
	 * violated.
	 * @param value The value that has to satisfy the restriction.
	 * @throws RestrictionNotSatisfiedException If the restriction has been violated.  
	 */
	void checkRestriction(RestrictionValue value);
	/**
	 * Returns the value that used to check the restriction.
	 * @return The value that used to check the restriction.
	 */
	RestrictionValue getRestrictionValue();
}
