package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;

@SuppressWarnings("rawtypes")
public class MaxInclusiveImpl extends AbstractRestriction implements MaxInclusive {
	/**
	 * The value the restriction is checked against. 
	 */
	private RestrictionValue restrictionValue;
	
	/**
	 * Constructs a new object. The restriction value is set to 0. 
	 */
	public MaxInclusiveImpl() {
	}
	
	/**
	 * Constructs a new object.
	 * @param restrictionValue The value for checking the restrictions against is set to this parameter. 
	 */
	public MaxInclusiveImpl(final RestrictionValue restrictionValue) {
		this.restrictionValue = restrictionValue;
	}
	
	@Override
	public void checkRestriction(final RestrictionValue value) {
		if (!isSatisfied(value)) {
			final RestrictionNotSatisfiedException exception = RestrictionNotSatisfiedException.
					maxRestrictionNotSatisfied(value.toString(), 
							restrictionValue.toString());
			throw exception;
		}
	}

	@Override
	public RestrictionValue getRestrictionValue() {
		return restrictionValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isSatisfied(final RestrictionValue value) {
		if (restrictionValue.compare(value) == 1) {
			return true;
		} else if (restrictionValue.compare(value) == 0) {
			return true;
		} else {
			return false;
		}
	}
}
