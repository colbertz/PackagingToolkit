package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

/**
 * Contains the properties that have been read from the ontology. The map is needed 
 * because we need to find the properties with the help of their local name 
 * while processing the data the user has entered in the gui and creating an
 * information package.
 * @author Christopher Olbertz
 *
 */
public interface OntologyPropertiesMap {
	/**
	 * Adds a ontology property to the map. The iri of the property is taken from the object. 
	 * @param ontologyProperty The property that should be added to the map.
	 */
	void addProperty(final OntologyProperty ontologyProperty);
	/**
	 * Creates a copy of a property in the map. 
	 * @param propertyIri The iri of the property that should be cloned. 
	 * @return The copy of the individual. 
	 */
	OntologyProperty cloneOntologyProperty(final String propertyIri);
	/**
	 * Determines the number of properties in the map.
	 * @return The number of properties in the map.
	 */
	int getPropertiesCount();
}
