package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.InformationPackageIndividualsHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.IrisAndTheirAnnotationPropertiesHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.OntologyClassIndividualsHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.OntologyClassesHashMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.OntologyClassesPropertiesHashMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.OntologyPropertiesHashMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.InformationPackageIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.IrisAndTheirAnnotationPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyClassesPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.OntologyPropertiesMap;

/**
 * This factory constructs the objects that implements the interfaces of the data structures.
 * @author Christopher Olbertz
 *
 */
public class DataStructureFactory implements ApplicationContextAware {
	private ApplicationContext applicationContext;
	/**
	 * Creates an empty implementation of the type 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.client.ontology.model.interfaces.OntologyClassesMap}.
	 * The implementation {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.client.ontology.model.classes.
	 * OntologyClassesHashMap.OntologyClassesHashMap} is used.
	 * @return An implementation without any content.
	 */
	public final OntologyClassesMap createOntologyClassesMap() {
		final OntologyClassesMap ontologyClassesMap = new OntologyClassesHashMap();
		final AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyClassesMap);
		beanFactory.initializeBean(ontologyClassesMap, ontologyClassesMap.getClass().getSimpleName());
		return ontologyClassesMap;
	}
	
	/**
	 * Creates an empty implementation of the type 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.client.ontology.model.interfaces.OntologyPropertiesMap}.
	 * The implementation {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.client.ontology.model.classes.
	 * OntologyPropertiesMap} is used.
	 * @return An implementation without any content.
	 */
	public final OntologyPropertiesMap createOntologyPropertiesMap() {
		final OntologyPropertiesMap ontologyPropertiesMap = new OntologyPropertiesHashMap();
		final AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyPropertiesMap);
		beanFactory.initializeBean(ontologyPropertiesMap, ontologyPropertiesMap.getClass().getSimpleName());
		return ontologyPropertiesMap;
	}
	
	/**
	 * Creates an empty implementation of the type 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.client.ontology.model.interfaces.OntologyClassesPropertiesMap}.
	 * The implementation {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.client.ontology.model.classes.
	 * OntologyClassesPropertiesMap} is used.
	 * @return An implementation without any content.
	 */
	public final OntologyClassesPropertiesMap createOntologyClassesPropertiesMap() {
		final OntologyClassesPropertiesMap ontologyClassesPropertiesMap = new OntologyClassesPropertiesHashMap();
		final AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(ontologyClassesPropertiesMap);
		String beanname = ontologyClassesPropertiesMap.getClass().getSimpleName(); 
		beanFactory.initializeBean(ontologyClassesPropertiesMap, beanname);
		return ontologyClassesPropertiesMap;
	}
	
	/**
	 * Creates an empty implementation of the type 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.client.ontology.model.interfaces.OntologyClassIndividualsMap}.
	 * The implementation {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.client.ontology.model.classes.
	 * OntologyClassIndividualsMap} is used.
	 * @return An implementation without any content.
	 */
	public final InformationPackageIndividualsMap createInformationPackageIndividualsMap() {
		return new InformationPackageIndividualsHashMapImpl();
	}
	
	/**
	 * Creates an empty implementation of the type 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.client.ontology.model.interfaces.OntologyClassIndividualsMap}.
	 * The implementation {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.client.ontology.model.classes.
	 * OntologyClassIndividualsMap} is used.
	 * @return An implementation without any content.
	 */
	public final OntologyClassIndividualsMap createOntologyClassIndividualsMap() {
		return new OntologyClassIndividualsHashMapImpl();
	}
	
	/**
	 * Creates an empty implementation of the type 
	 * {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.client.ontology.model.interfaces.IrisAndTheirAnnotationPropertiesMap}.
	 * The implementation {@link de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.client.ontology.model.classes.
	 * IrisAndTheirAnnotationPropertiesMap} is used.
	 * @return An implementation without any content.
	 */
	public final IrisAndTheirAnnotationPropertiesMap createEmptyIrisAndTheirAnnotationPropertiesMap() {
		return new IrisAndTheirAnnotationPropertiesHashMapImpl();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
