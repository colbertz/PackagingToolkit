package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidAndIriResponse;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.OntologyService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyService;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Contains all methods of the ontology module that represent REST interfaces.
 */
@RestController
@RequestMapping(PathConstants.SERVER_ONTOLOGY_INTERFACE + "/**")
public class OntologyCommunication {
	/**
	 * A reference to an object that points to the business logic of this module.
	 */
	@Autowired
	private OntologyService ontologyService;
	@Autowired
	private TaxonomyService taxonomyService;
	
	/**
	 * Is called by the client to determine the hierarchy of the classes starting 
	 * with the base class configured on the server.
	 * @return Contains the classes we are looking for as subclasses.
	 */
	@RequestMapping(value = PathConstants.SERVER_READ_HIERARCHY, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<OntologyClassResponse> readHierarchy() {
			final OntologyClassResponse ontologyClassResponse = ontologyService.getHierarchy();
		return new ResponseEntity<>(ontologyClassResponse, HttpStatus.OK);
	}
	
	/**
	 * Is called by the client to determine the hierarchy of the classes whose names
	 * are in a list. 
	 * @param classnames The list with the names of the classes we are 
	 * looking for.
	 * @return Contains the classes we are looking for as subclasses.
	 */
	@RequestMapping(value = PathConstants.SERVER_READ_HIERARCHY_WITH_CLASS_NAMES, method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<OntologyClassResponse>  readHierarchyWithClassNames(
			@RequestBody	StringListResponse classnames) {
		final OntologyClassResponse ontologyClassResponse = ontologyService.getHierarchy(classnames);
		return new ResponseEntity<OntologyClassResponse>(ontologyClassResponse, HttpStatus.OK);
	}
	
	/**
	 * Forces the initialization of the ontology cache. Can be used if a new base ontology has been uploaded.
	 */
	@RequestMapping(value = PathConstants.SERVER_UPDATE_ONTOLOGY_CACHE, method = RequestMethod.GET)
	public ResponseEntity<String> updateOntologyCache() {
		ontologyService.updateOntologyCache();
		return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.OK);
	}
	
	/**
	 * Determines all classes in the Premis ontology.
	 * @return All classes in the Premis ontology.
	 */
	@RequestMapping(value = PathConstants.SERVER_READ_PREMIS, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	// DELETE_ME Wird wohl nicht mehr gebraucht.
	public ResponseEntity<OntologyClassResponse>  readAllPremisClasses() {
		final OntologyClassResponse ontologyClassResponse = ontologyService.getPremisHierarchy();
		return new ResponseEntity<OntologyClassResponse>(ontologyClassResponse, HttpStatus.OK);
	}

	/**
	 * Determines all classes in the SKOS ontology.
	 * @return All classes in the SKOS ontology.
	 */
	@RequestMapping(value = PathConstants.SERVER_READ_SKOS, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	// DELETE_ME Wird wohl nicht mehr gebraucht.
	public ResponseEntity<OntologyClassResponse>  readAllSkosClasses() {
		final OntologyClassResponse ontologyClassResponse = ontologyService.getSkosHierarchy();
		return new ResponseEntity<OntologyClassResponse>(ontologyClassResponse, HttpStatus.OK);
	}
	
	/**
	 * Reads the properties of a class in the base ontology from the server. 
	 * @param iriOfClass The fully qualified iri of the class.
	 * @return A response with all properties of the class.
	 */
	@RequestMapping(value = PathConstants.SERVER_GET_PROPERTIES_OF_CLASS, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<OntologyClassResponse> getPropertiesOfClass(
			@RequestParam(ParamConstants.PARAM_CLASSNAME) String iriOfClass) {
		final OntologyClassResponse ontologyClass = ontologyService.getPropertiesOfClass(iriOfClass);
		return new ResponseEntity<OntologyClassResponse>(ontologyClass, HttpStatus.OK);
	}
	
	/**
	 * Finds all individuals of a class that are predefined in the base ontology.
	 * @param iriOfClass The iri of the class we are interested in.
	 * @return The found individuals.
	 */
	@RequestMapping(value = PathConstants.SERVER_FIND_PREDEFINED_INDIVIDUALS_OF_CLASS, 
			method = RequestMethod.GET,	produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<IndividualListResponse> findPredefinedIndividualsOfClass(
			@RequestParam(ParamConstants.PARAM_CLASSNAME) String iriOfClass) {
		final IndividualListResponse individuals = ontologyService.findPredefinedIndividualsByClass(iriOfClass);
		return new ResponseEntity<IndividualListResponse>(individuals, HttpStatus.OK);
	}
	
	/**
	 * Finds all individuals of a class that are in a specific information package.
	 * @param uuidAndIriResponse Contains the uuid of the information package and the iri of the class.
	 * @return The found individuals.
	 */
	@RequestMapping(value = PathConstants.SERVER_FIND_INDIVIDUALS_OF_CLASS_AND_INFORMATION_PACKAGE, 
			method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<IndividualListResponse> findIndividualsOfClassAndInformationPackage (
			@RequestBody UuidAndIriResponse uuidAndIriResponse) {
		final IndividualListResponse individuals = ontologyService.getIndiviualsByOntologyClassAndInformationPackage(uuidAndIriResponse.getIri(), uuidAndIriResponse.getUuid());
		return new ResponseEntity<IndividualListResponse>(individuals, HttpStatus.OK);
	}
	
	/**
	 * Looks for the individuals of a certain class that are saved on the server. 
	 * @param iriAsString The iri of the class.
	 * @return The individuals of the class with the iri iriAsString.
	 */
	@RequestMapping(value = PathConstants.SERVER_FIND_INDIVIDUALS_OF_CLASS, 
			method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<IndividualListResponse> findIndividualsOfClass(@RequestParam(ParamConstants.PARAM_CLASSNAME) String iriAsString) {
		final IndividualListResponse individuals = ontologyService.getIndiviualsByOntologyClass(iriAsString);
		return new ResponseEntity<IndividualListResponse>(individuals, HttpStatus.OK);
	}
	
	/**
	 * Finds all taxonomies defined in the base ontology.
	 * @return The found taxonomies.
	 */
	@RequestMapping(value = PathConstants.SERVER_FIND_ALL_TAXONOMIES, 
			method = RequestMethod.GET,	produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<TaxonomyListResponse> findAllTaxonomies() {
		final TaxonomyListResponse taxonomies = taxonomyService.getAllTaxonomies();
		return new ResponseEntity<TaxonomyListResponse>(taxonomies, HttpStatus.OK);
	}
	
	/**
	 * Reads the class that determines the starting point for taxonomies from the server. 
	 * @return A response with the desired class.
	 */
	@RequestMapping(value = PathConstants.SERVER_GET_TAXONOMY_START_CLASS, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<OntologyClassResponse> getTaxonomyStartClass() {
		final OntologyClassResponse ontologyClass = ontologyService.getTaxonomyStartClass();
		return new ResponseEntity<OntologyClassResponse>(ontologyClass, HttpStatus.OK);
	}
	
	/*@RequestMapping(value = PathConstants.SERVER_SAVE_SELECTED_TAXONOMY_INDIVIDUALS, 
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> saveSelectedTaxonomyIndividuals (
			@RequestBody UuidAndIriResponse uuidAndIriResponse) {
		ontologyService.save
		final IndividualListResponse individuals = ontologyService.getIndiviualsByOntologyClassAndInformationPackage(uuidAndIriResponse.getIri(), uuidAndIriResponse.getUuid());
		return new ResponseEntity<IndividualListResponse>(individuals, HttpStatus.OK);
	}*/
	
}