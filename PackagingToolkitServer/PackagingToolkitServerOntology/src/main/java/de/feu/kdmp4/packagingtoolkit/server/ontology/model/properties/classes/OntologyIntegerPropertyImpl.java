package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;


import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.PropertyRangeException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;


public class OntologyIntegerPropertyImpl extends OntologyDatatypePropertyImpl 
										 implements OntologyIntegerProperty {
	private static final int DEFAULT_VALUE = 0;
	private static final long serialVersionUID = 106001928244230493L;
	private static final long SIGNED_MAX = Integer.MAX_VALUE;
	private static final long SIGNED_MIN = Integer.MIN_VALUE;
	private static final long UNSIGNED_MAX = 4294967295L;
	private static final long UNSIGNED_MIN = 0L;
	
	/**
	 * True if the value can contain the zero.
	 */
	private boolean inclusiveZero;
	/**
	 * The sign that can be only negative, only positive or the sign is not important.
	 */
	private Sign sign;
	/**
	 * True if the value is unsigned. Unsigned means that there are only positive
	 * values possible and the value ranged is doubled. 
	 */
	private boolean unsigned;
	
	/**
	 * Creates a new property.
	 */
	public OntologyIntegerPropertyImpl() {
	}
	
	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param unsigned True if the value may be unsigned, false otherwise.
	 * @param sign The information about how the sign is handled
	 * @param inclusiveZero True if the value may be equal zero.
	 * @param defaultLabel The label that represents the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyIntegerPropertyImpl(final String propertyId, final boolean unsigned, 
			final Sign sign, final boolean inclusiveZero, final String defaultLabel, final String range) {
		super(propertyId, DEFAULT_VALUE, defaultLabel, range);
		this.unsigned = unsigned;
		this.sign = sign;
		this.inclusiveZero = inclusiveZero;
		setPropertyValue(0);
	}
	
	/**
	 * Initializes the restrictions like specified in the constructor. 
	 */
	@PostConstruct
	public void initialize() {
		addRestrictions();
	}

	@Override
	public void addMaxInclusiveRestriction(final long value) {
		if (!checkIntegerRange(value)) {
			final PropertyRangeException exception = PropertyRangeException.
					valueNotInIntegerRange(value);
			throw exception;
		}
		final RestrictionValue<Long> restrictionValue = new RestrictionValue<>(value);
		final MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}
	
	@Override
	public void addMinInclusiveRestriction(final long value) {
		if (!checkIntegerRange(value)) {
			final PropertyRangeException exception = PropertyRangeException.
					valueNotInIntegerRange(value);
			throw exception;
		}
		final RestrictionValue<Long> restrictionValue = new RestrictionValue<>(value);
		final MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}
	
	/**
	 * Checks if a value is contained in the range of an integer. It checks if
	 * the integer is signed or unsigned and then checks the value.
	 * @param value The value to check.
	 * @return True if the value is in the range of either an unsigned or a
	 * signed integer.
	 */
	private boolean checkIntegerRange(final long value) {
		if (unsigned) {
			if (value >= UNSIGNED_MIN && value <= UNSIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		} else {
			if (value >= SIGNED_MIN && value <= SIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 * Adds the necessary restrictions to this property in dependency of the 
	 * parameters that were given to the constructor. 
	 */
	private void addRestrictions() {
		if (unsigned) {
			addMinInclusiveRestriction(UNSIGNED_MIN);
			addMaxInclusiveRestriction(UNSIGNED_MAX);
		} else if (!sign.equals(Sign.BOTH)) { 
			if (sign.equals(Sign.MINUS_SIGN) && !inclusiveZero) {
				addMaxInclusiveRestriction(-1);
				addMinInclusiveRestriction(SIGNED_MIN);
			} else if (sign.equals(Sign.MINUS_SIGN) && inclusiveZero) {
				addMaxInclusiveRestriction(0);
				addMinInclusiveRestriction(SIGNED_MIN);
			} else if (sign.equals(Sign.PLUS_SIGN) && inclusiveZero) {
				addMinInclusiveRestriction(0);
				addMaxInclusiveRestriction(SIGNED_MAX);
			} else if (sign.equals(Sign.PLUS_SIGN) && !inclusiveZero) {
				addMinInclusiveRestriction(1);
				addMaxInclusiveRestriction(SIGNED_MAX);
			}
		}
	}
	
	// ******* Setters and getters ********
	@Override
	public Long getPropertyValue() {
		final Object theValue = getValue();
		return (Long)theValue;
	}
	
	@Override
	public void setPropertyValue(final Object value) {
		Long myLong = null;
		
		if (value instanceof String) {
			myLong = Long.parseLong(value.toString());
		} else {
			final Number number = (Number)value;
			final long longValue = number.longValue();
			myLong = new Long(longValue);
		}
		
			super.setPropertyValue(myLong);
	}
	
	@Override
	public boolean isUnsigned() {
		return unsigned;
	}
	
	@Override
	public boolean isInclusiveZero() {
		return inclusiveZero;
	}
	
	@Override
	public Sign getSign() {
		return sign;
	}
	
	@Override
	public boolean isIntegerProperty() {
		return true;
	}
}
