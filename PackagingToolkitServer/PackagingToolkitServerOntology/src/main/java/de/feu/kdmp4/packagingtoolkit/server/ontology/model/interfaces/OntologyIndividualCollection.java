package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.ListInterface;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;

/**
 * Contains a collection with individuals.
 * @author Christopher Olbertz
 *
 */
public interface OntologyIndividualCollection extends Cloneable, ListInterface {
	/**
	 * Adds an individual to this collection. 
	 * @param individual The individual that should be added.
	 */
	void addIndividual(final OntologyIndividual individual);
	/**
	 * Adds a collection with individuals to this collection.
	 * @param individuals The collection we want to add to this collection.
	 */
	void appendIndividualList(final OntologyIndividualCollection individuals);
	/**
	 * Returns an individual at a given index.
	 * @param index The index.
	 * @return The found individual.
	 */
	OntologyIndividual getIndividual(final int index);
	/**
	 * Counts the individuals in this collection.
	 * @return The number of individuals.
	 */
	int getIndiviualsCount();
	/**
	 * Checks if there are objects in this collection.
	 * @return True if the collection is empty, false otherwise.
	 */
	boolean isEmpty();
	/**
	 * Checks if there are objects in this collection.
	 * @return True if the collection is not empty, false otherwise.
	 */
	boolean isNotEmpty();
	/**
	 * Looks for an individual by its uuid.
	 * @param uuid The uuid of the individual we are looking for. 
	 * @return The found individual. 
	 */
	OntologyIndividual getIndividualByUuid(final Uuid uuid);
	/**
	 * Removes an individual at a given position.
	 * @param index The position we are looking at. 
	 */
	void removeIndividual(final int index);
	/**
	 * Creates a copy of this individual. 
	 * @return A new object that contains the values of this individual.
	 * @throws CloneNotSupportedException
	 */
	OntologyIndividualCollection clone() throws CloneNotSupportedException;
	/**
	 * Creates a java.util.List with the objects contained in this collection.
	 * @return A java.util.List with the objects contained in this collection.
	 */
	List<OntologyIndividual> toList();
	/**
	 * Creates triple statements with the values of the individuals in this collection.  
	 * @return A collection with the statements.
	 */
	TripleStatementCollection toStatements();
	/**
	 * Determines the individuals that are assigned to a certain information package.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return The individuals assigned to the information package with the uuid uuidOfInformationPackage.
	 */
	OntologyIndividualCollection getIndividualsOfInformationPackage(final Uuid uuidOfInformationPackage);
	/**
	 * Replaces an individual in the collection with another individual with the same uuid.
	 * @param newOntologyIndividual The individual that replaces the other one.
	 */
	void replaceIndividual(final OntologyIndividual newOntologyIndividual);
	/**
	 * Determines if an individual exists already in the collection.
	 * @param ontologyIndividual The individual we want to check.
	 * @return True if ontologyIndividual exists in the collection false otherwise.
	 */
	boolean containsIndividual(final OntologyIndividual ontologyIndividual);
	boolean hasNextOntologyIndividual();
	OntologyIndividual getNextOntologyIndividual();
	/**
	 * Removes an individual from the list.
	 * @param index The individual we want to remove.  
	 */
	void removeIndividual(OntologyIndividual ontologyIndividual);
}
