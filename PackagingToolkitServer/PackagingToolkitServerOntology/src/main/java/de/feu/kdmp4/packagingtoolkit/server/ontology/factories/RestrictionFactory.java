package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.MaxInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.MinInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.RestrictionCollection;

/**
 * Creates restrictions.
 * @author Christopher Olbertz 
 *
 */
public class RestrictionFactory extends AbstractFactory {
	/**
	 * Creates a maxInclusive restriction. This means that a value x is restricted by a value y above x but x
	 * may be equal to y.   
	 * @param value The value that should be restricted.
	 * @return The restriction.
	 */
	public MaxInclusive createMaxInclusive(final RestrictionValue value) {
		MaxInclusive maxInclusive = new MaxInclusiveImpl(value);
		super.initBean(maxInclusive);
		
		return maxInclusive; 
	}
	
	/**
	 * Creates a minInclusive restriction. This means that a value x is restricted by a value y below x but x
	 * may be equal to y.   
	 * @param value The value that should be restricted.
	 * @return The restriction.
	 */
	public MinInclusive createMinInclusive(final RestrictionValue value) {
		MinInclusive minInclusive = new MinInclusiveImpl(value);
		super.initBean(minInclusive);
		
		return minInclusive; 
	}
	
	public RestrictionCollection createRestrictionList() {
		return new RestrictionCollectionImpl();
	}
}
