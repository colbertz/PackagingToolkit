package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.classes;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces.MinInclusive;

@SuppressWarnings("rawtypes")
public class MinInclusiveImpl extends AbstractRestriction implements MinInclusive {
	/**
	 * The value the restriction is checked against. 
	 */
	private RestrictionValue restrictionValue;
	
	/**
	 * Constructs a new object. The restriction value is set to 0. 
	 */
	public MinInclusiveImpl() {
	}
	
	/**
	 * Constructs a new object.
	 * @param restrictionValue The value for checking the restrictions against is set to this parameter. 
	 */
	public MinInclusiveImpl(final RestrictionValue restrictionValue) {
		this.restrictionValue = restrictionValue;
	}
	
	@Override
	public RestrictionValue getRestrictionValue() {
		return restrictionValue;
	}
	
	@Override
	public void checkRestriction(final RestrictionValue value) {
		if (!isSatisfied(value)) {
			final RestrictionNotSatisfiedException exception = RestrictionNotSatisfiedException.
					minRestrictionNotSatisfied(value.toString(), 
							restrictionValue.toString());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isSatisfied(final RestrictionValue value) {
		if (restrictionValue.compare(value) == 1) {
			return false;
		} else if (restrictionValue.compare(value) == 0) {
			return true;
		} else {
			return true;
		}
	}
}
