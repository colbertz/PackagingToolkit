package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguageCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyElement;

/**
 * An abstract superclass for all elements that can be contained in an ontology.
 * @author Christopher Olbertz
 *
 */
public abstract class OntologyElementImpl implements  OntologyElement {
	/**
	 * Contains the different label texts in  the different languages.
	 */
	private TextInLanguageCollection labelTexts;
	
	public OntologyElementImpl() {
		labelTexts = ServerModelFactory.createEmptyTextInLanguageCollection();
	}
	
	@Override
	public void addLabel(final TextInLanguage label) {
		labelTexts.addTextInLanguage(label);
	}
	
	@Override
	public void addLabel(final String labelText, final OntologyLabelLanguage language) {
		TextInLanguage labelInLanguage = ServerModelFactory.createTextInLanguage(labelText, language);
		addLabel(labelInLanguage);
	}
	
	@Override
	public int getLabelCount() {
		return labelTexts.getTextsInLanguageCount();
	}
	
	@Override
	public TextInLanguage getLabelAt(final int index) {
		return labelTexts.getTextInLanguage(index);
	}
}
