/**
 * Contains the facades that contains the methods that allow other modules to access the Ontology module.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.ontology.facades;