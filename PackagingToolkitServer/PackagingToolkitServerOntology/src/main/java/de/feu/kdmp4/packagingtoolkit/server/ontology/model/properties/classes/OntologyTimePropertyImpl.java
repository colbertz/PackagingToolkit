package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes;

import java.time.LocalTime;

import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

public class OntologyTimePropertyImpl extends OntologyDatatypePropertyImpl 
									  implements OntologyTimeProperty {

	private static final long serialVersionUID = 1748033123603000410L;

	/**
	 * Creates a new property.
	 */
	public OntologyTimePropertyImpl() {
	}
	
	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param value The value of the property.
	 * @param defaultLabel The label that represents the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyTimePropertyImpl(final String propertyId, final LocalTime value, final String defaultLabel, final String range) {
		super(propertyId, value, defaultLabel, range);
	}

	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param label The label that represents the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyTimePropertyImpl(final String propertyId, final String label, final String range) {
		super(propertyId, null, label, range);
	}
	
	/**
	 * Creates a new property.
	 * @param propertyId The iri of the property.
	 * @param range The range as specified in the ontology.
	 */
	public OntologyTimePropertyImpl(final String propertyId, final String range) {
		super(propertyId, null, null, range);
	}
	
	@Override
	public LocalTime getPropertyValue() {
		return (LocalTime) getValue();
	}

	@Override
	public boolean isTimeProperty() {
		return true;
	}
	
	@Override
	public String toString() {
		return DateTimeUtils.getLocalTimeAsOntologyString(getPropertyValue());
	}
}
