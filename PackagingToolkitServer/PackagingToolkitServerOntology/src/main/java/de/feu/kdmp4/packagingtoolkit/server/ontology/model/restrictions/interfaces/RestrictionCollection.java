package de.feu.kdmp4.packagingtoolkit.server.ontology.model.restrictions.interfaces;

/**
 * A list with restrictions.
 * @author Christopher Olbertz
 *
 */
public interface RestrictionCollection {
	/**
	 * Gets a restriction at a given index of the list.
	 * @param index The index.
	 * @return The restriction found at the index.
	 */
	Restriction getRestriction(final int index);
	/**
	 * Adds a restriction to the list.
	 * @param restriction The restriction that should be appended to the list.
	 */
	void addRestriction(final Restriction restriction);
	/**
	 * Counts the restrictions in this collection. 
	 * @return The number of restrictions.
	 */
	int getRestrictionsCount();

}
