package de.feu.kdmp4.packagingtoolkit.server.ontology.classes;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyResponse;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyOperations;
import de.feu.kdmp4.packagingtoolkit.server.ontology.interfaces.TaxonomyService;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;

public class TaxonomyServiceImpl implements TaxonomyService {
	private TaxonomyOperations taxonomyOperations;
	
	// DELETE_ME
	/*public TaxonomyServiceImpl(final OntologyCache ontologyCache) {
		while (ontologyCache.hasNextSkosIndividual()) {
			final OntologyIndividual skosIndividual = ontologyCache.getNextSkosIndividual();
			//final TaxonomyIndividual taxonomyIndividual = OntologyModelFactory.
			System.out.println("bla");
		}
	}*/
	
	@Override
	public TaxonomyListResponse getAllTaxonomies() {
		final TaxonomyListResponse taxonomies = ResponseModelFactory.createEmptyTaxonomyListResponse();
		
		for (int i = 0; i < taxonomyOperations.getTaxonomyCount(); i++) {
			final Optional<Taxonomy> optionalWithTaxonomy = taxonomyOperations.getTaxonomy(i);
			final Taxonomy taxonomy = optionalWithTaxonomy.get();
			final TaxonomyResponse taxonomyResponse = ServerResponseFactory.fromTaxonomy(taxonomy);
			taxonomies.addTaxonomy(taxonomyResponse);
		}
		
		return taxonomies;
	}
	
	@Override
	public void setTaxonomyOperations(TaxonomyOperations taxonomyOperations) {
		this.taxonomyOperations = taxonomyOperations;
	}
}
