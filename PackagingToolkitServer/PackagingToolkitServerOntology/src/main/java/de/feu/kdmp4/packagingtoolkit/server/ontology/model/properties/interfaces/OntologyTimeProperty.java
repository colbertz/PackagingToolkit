package de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces;

import java.time.LocalTime;

/**
 * An ontology property with a time value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyTimeProperty  extends OntologyDatatypeProperty {
	/**
	 * Returns the value of this property.
	 * @return The value of this property.
	 */
	public LocalTime getPropertyValue();
}
