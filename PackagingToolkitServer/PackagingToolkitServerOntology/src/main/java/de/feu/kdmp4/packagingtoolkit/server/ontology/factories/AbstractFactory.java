package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * An abstract superclass for the factories that like to work with the application context of Spring.
 * @author Christopher Olbertz
 *
 */
public abstract class AbstractFactory  implements ApplicationContextAware{
	@Autowired
	protected ApplicationContext applicationContext;
	
	/**
	 * Initializes a bean with the application context of Spring.
	 * @param myBean The bean that should be configured in the application context.
	 */
	protected void initBean(Object myBean) {
		// Is only done if an application context has been injected by Spring. 
		if (applicationContext != null) {
			AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
			beanFactory.autowireBean(myBean);
			String beanName = myBean.getClass().getSimpleName();
			beanFactory.configureBean(myBean, beanName);
		}
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}