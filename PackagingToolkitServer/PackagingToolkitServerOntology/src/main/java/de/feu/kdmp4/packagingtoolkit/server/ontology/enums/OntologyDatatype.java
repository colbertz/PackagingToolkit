package de.feu.kdmp4.packagingtoolkit.server.ontology.enums;

/**
 * Contains the information about the datatypes in an ontology.
 * @author Christopher Olbertz
 *
 */
public enum OntologyDatatype {
	BOOLEAN("boolean"), BYTE("byte"), 
	DATE("date"), DATETIME("datetime"), DOUBLE("double"), DURATION("duration"), 
	FLOAT("float"), 
	INTEGER("int"), 
	LONG("long"),
	NEGATIVE_INTEGER("negativeinteger"),
	NON_NEGATIVE_INTEGER("nonnegativeinteger"),	NON_POSITIVE_INTEGER("nonpositiveinteger"),	
	OBJECT("object"),
	POSITIVE_INTEGER("positiveinteger"),
	SHORT("short"), STRING("string"),
	TIME("time"),
	UNSIGNED("unsigned"), UNSIGNED_BYTE("unsignedbyte"), UNSIGNED_INT("unsignedint");
	
	/**
	 * The range that is extracted from an ontology and that contains the information
	 * about the datatype.
	 */
	private String range;
	
	private OntologyDatatype( String range) {
		this.range = range;
	}
	
	public String getRange() {
		return range;
	}
	
	/**
	 * Checks if the range describes a boolean property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a boolean property.
	 */
	public static boolean isBoolean(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(BOOLEAN.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a byte property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a byte property.
	 */
	public static boolean isByte(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(BYTE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a date property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a date property.
	 */
	public static boolean isDate(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DATE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a datetime property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a datetime property.
	 */
	public static boolean isDateTime(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DATETIME.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a double property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a double property.
	 */
	public static boolean isDouble(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DOUBLE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a duration property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a duration property.
	 */
	public static boolean isDuration(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DURATION.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a float property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a float property.
	 */
	public static boolean isFloat(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(FLOAT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes an integer property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes an integer property.
	 */
	public static boolean isInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a long property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a long property.
	 */
	public static boolean isLong(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(LONG.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes an object property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes an object property.
	 */
	public static boolean isObjectProperty(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(OBJECT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a short property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a short property.
	 */
	public static boolean isShort(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(SHORT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a String property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a String property.
	 */
	public static boolean isString(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(STRING.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a time property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a time property.
	 */
	public static boolean isTime(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(TIME.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes an unsigned property. The datatype itself is not important. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes an usigned property.
	 */
	public static boolean isUnsigned(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		if (lowerPropertyRange.contains(UNSIGNED.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes an unsigned integer property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes an unsigned integer property.
	 */
	public static boolean isUnsignedByte(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(UNSIGNED_BYTE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a negative integer property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a negative integer property.
	 */
	public static boolean isNegativeInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NEGATIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a non negative property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a non negative property.
	 */
	public static boolean isNonNegativeInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NON_NEGATIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a non positive property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a non positive property.
	 */
	public static boolean isNonPositiveInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NON_POSITIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes a positive integer property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes a positive integer property.
	 */
	public static boolean isPositiveInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(POSITIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the range describes an unsigned int property. 
	 * @param propertyRange The range of the property. 
	 * @return True, if propertyRange describes an unsigned int property.
	 */
	public static boolean isUnsignedInt(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(UNSIGNED_INT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
}