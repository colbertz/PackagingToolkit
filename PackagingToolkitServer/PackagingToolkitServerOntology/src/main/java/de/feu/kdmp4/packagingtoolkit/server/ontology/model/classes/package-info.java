package de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes;

/**
	* Contains all implementations of the interfaces related to models for the ontology module.
	*/