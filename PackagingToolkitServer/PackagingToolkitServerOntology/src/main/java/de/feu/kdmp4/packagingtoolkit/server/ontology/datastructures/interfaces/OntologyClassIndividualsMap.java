package de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;

/**
 * Assigns a list with individuals to an ontology class. The class is identified by its name.
 * @author Christopher Olbertz
 *
 */
public interface OntologyClassIndividualsMap {
	/**
	 * Adds an individual to the individual list of a class.
	 * @param individual The individual that should be added to the information package.
	 */
	void addOrUpdateIndividual(final OntologyIndividual ontologyIndividual);
	/**
	 * Gets the individual list of a given class.
	 * @param className The name of the class in the ontology.
	 * @return The individual list of the class. If there are no individuals
	 * the return value is an empty individual list.
	 */
	OntologyIndividualCollection getOntologyIndividuals(final String className);
	/**
	 * Adds an individual list to an ontology class.
	 * @param className The name of the class where the individuals are added to.
	 * @param individuals The individuals that should be added to the ontology class.
	 */
	void addIndividuals(final String className, final OntologyIndividualCollection individualList);
	/**
	 * Creates a list with the values saved in this map. 
	 * @return A list that contains all individuals saved in the map.
	 */
	OntologyIndividualCollection toOntologyIndividualList();
	/**
	 * Returns the number of the entries in the map.
	 * @return The number of entries.
	 */
	int getEntryCount();
	/**
	 * Checks if the map contains a certain individual.
	 * @param ontologyIndividual The individual we want to check for. 
	 * @return True if ontologyIndividual is contained, false otherwise. 
	 */
	boolean containsIndividual(OntologyIndividual ontologyIndividual);
	/**
	 * Looks for an individual with the help of its uuid.
	 * @param uuidOfIndividual The uuid of the individual we are looking for. 
	 * @return The individual that was found.
	 */
	OntologyIndividual findIndividualByUuid(Uuid uuidOfIndividual);
	/**
	 * Looks for an individual by its uuid and removes it from the map. 
	 * @param uuidOfIndividual The uuid of the individual we want to remove. 
	 */
	void removeIndividualByUuid(Uuid uuidOfIndividual);
	/**
	 * Looks for the class that contains a certain individual. 
	 * @param uuidOfIndividual The uuid of the individual we are looking for. 
	 * @return
	 */
	OntologyClass findClassByIndividualUuid(Uuid uuidOfIndividual);
}
