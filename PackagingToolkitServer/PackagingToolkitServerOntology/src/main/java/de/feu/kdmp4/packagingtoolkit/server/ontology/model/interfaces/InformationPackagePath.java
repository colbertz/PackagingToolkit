package de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces;

public interface InformationPackagePath {
	public void addPathComponent(String pathComponent);
	public void deleteLastPathComponent();
	int getComponentCount();
}
