package de.feu.kdmp4.packagingtoolkit.server.ontology.factories;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import de.feu.kdmp4.packagingtoolit.server.ontology.model.interfaces.TaxonomyIndividualMap;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.OntologyDurationImpl;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyResponse;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.IrisAndTaxonomyIndividualsHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.classes.TaxonomyListImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.IrisTaxonomyIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.server.ontology.datastructures.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.AnnotationPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.InformationPackagePathImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.PropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyIndividualHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.TaxonomyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.AnnotationPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.InformationPackagePath;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.PropertyCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.classes.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Creates the objects of the model classes for ontology elements.
 * @author Christopher Olbertz
 *
 */
public class OntologyModelFactory extends AbstractFactory {
	/**
	 * A reference to the factory that creates property objects.
	 */
	private PropertyFactory propertyFactory;
	/**
	 * Counts the class objects that have been created. Is used to create an unique name in the
	 * application context of Spring.
	 */
	private int ontologyClassCounter;
	
	/**
	 * Initializes the object.
	 */
	public OntologyModelFactory() {
		ontologyClassCounter = 0;
	}
	
	/**
	 * Creates a new object of an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.Taxonomy}.
	 * @param rootIndividual The individual that represents the root of the taxonomy.
	 * @param title The title of the taxonomy.
	 * @return The newly created object.
	 */
	public static final Taxonomy createTaxonomy(final TaxonomyIndividual rootIndividual, final String title, final Namespace namespace) {
		return new TaxonomyImpl(rootIndividual, title, namespace);
	}
	
	/**
	 * Creates a new object of an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividual}.
	 * @param iri The iri of the individual.
	 * @param preferredLabel The preferred label as defined in the ontology.
	 * @param inScheme The scheme this individual is assigned to. 
	 * @param iriOfBroader The iri of the broader of this individual. The broader is the parent individual in the 
	 * taxonomy.
	 * @return The newly created object.
	 */
	public static final TaxonomyIndividual createTaxonomyIndividual(final Iri iri, final String preferredLabel, final Namespace inScheme, 
			final Iri iriOfBroader) {
		return new TaxonomyIndividualImpl(iri, preferredLabel, inScheme, iriOfBroader);
	}
	
	/**
	 * Creates a new object of an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividual}. This individual is a root individual
	 * for a taxonomy or a top concept. In both cases there is no broader. Therefore the iri of the broader is null.
	 * @param iri The iri of the individual.
	 * @param preferredLabel The preferred label as defined in the ontology.
	 * @param inScheme The scheme this individual is assigned to. 
	 * @return The newly created object.
	 */
	public static final TaxonomyIndividual createRootTaxonomyIndividual(final Iri iri, final String preferredLabel, final Namespace inScheme) {
		return new TaxonomyIndividualImpl(iri, preferredLabel, inScheme);
	}
	
	/**
	 * Creates a new object of an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualMap}.
	 * @return The newly created object.
	 */
	public static final TaxonomyIndividualMap createTaxonomyIndividualMap() {
		return new TaxonomyIndividualHashMapImpl();
	}
	
	/**
	 * Creates a new object of an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.ontology.datastructures.interfaces.IrisTaxonomyIndividualsMap}.
	 * @return The newly created object.
	 */
	public static final IrisTaxonomyIndividualsMap createIrisTaxonomyIndividualsMap() {
		return new IrisAndTaxonomyIndividualsHashMapImpl();
	}
	
	
	
	/**
	 * Creates a new object of an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackagePath}.
	 * @return The newly created object.
	 */
	public final InformationPackagePath createInformationPackagePath() {
		return new InformationPackagePathImpl();
	}
	
	/**
	 * Creates a copy of a property object. 
	 * @param ontologyProperty The property object that should be copied.
	 * @return The copy of ontologyProperty.
	 */
	public OntologyDatatypeProperty copyOntologyDatatypeProperty(final OntologyDatatypeProperty ontologyProperty) {
		return propertyFactory.copyOntologyDatatypeProperty(ontologyProperty);
	}
	
	/**
	 * Creates an empty property collection.
	 * @return The newly created property collection.
	 */
	public static final PropertyCollection createEmptyPropertyCollection() {
		return new PropertyCollectionImpl();
	}
	
	/**
	 * Creates an empty collection with individuals in a taxonomy.
	 * @return The newly created collection.
	 */
	public static final TaxonomyIndividualCollection createEmptyTaxonomyIndividualCollection() {
		return new TaxonomyIndividualCollectionImpl();
	}

	/**
	 * Creates an empty collection with taxonomies.
	 * @return The newly created collection.
	 */
	public static final TaxonomyCollection createEmptyTaxonomyCollection() {
		return new TaxonomyListImpl();
	}
	
	/**
	 * Creates an empty ontology class. 
	 * @param namespace The namespace of the class.
	 * @param localname The name of the class in the ontology without the namespace.
	 * @return The new ontology class with the given namespace and the given local name.
	 */
	public final OntologyClass createOntologyClass(final Namespace namespace, 
												   final LocalName localname) {
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localname);
		super.initBean(ontologyClass);
		return ontologyClass;
	}
	
	public final OntologyDuration createOntologyDuration(final int years, final int months, final int days,
			final int hours, final int minutes, final int seconds) {
		return new OntologyDurationImpl(days, hours, minutes, months, seconds, years);
	}
	
	/**
	 * Creates a new ontology individual with empty collections of datatype and object properties.
	 * @param ontologyClass The class the individual belongs to.
	 * @param uuidOfInformationPackage The uuid of the information package the individual belongs to.
	 * @return The new individual.
	 */
	public final OntologyIndividual createOntologyIndividual(final OntologyClass ontologyClass, final Uuid uuidOfInformationPackage) {
		DatatypePropertyCollection emptyDatatypePropertyList = createEmptyDatatypePropertyList();
		ObjectPropertyCollection emptyObjectPropertyList = createEmptyObjectPropertyList();
		OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, 
				emptyDatatypePropertyList, emptyObjectPropertyList);
		super.initBean(ontologyIndividual);
		return ontologyIndividual; 
	}
	
	/**
	 * Creates a new ontology individual with empty collections of datatype and object properties.
	 * @param ontologyClass The class the individual belongs to.
	 * @param uuidOfInformationPackage The uuid of the information package the individual belongs to.
	 * @param uuidOfIndividual The uuid of the individual if some has been already created.
	 * @return The new individual.
	 */
	public final OntologyIndividual createOntologyIndividual(final OntologyClass ontologyClass, final Uuid uuidOfInformationPackage, final Uuid uuidOfIndividual) {
		DatatypePropertyCollection emptyDatatypePropertyList = createEmptyDatatypePropertyList();
		ObjectPropertyCollection emptyObjectPropertyList = createEmptyObjectPropertyList();
		OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, uuidOfInformationPackage, uuidOfIndividual, 
				emptyDatatypePropertyList, emptyObjectPropertyList);
		super.initBean(ontologyIndividual);
		return ontologyIndividual; 
	}
	
	/**
	 * Creates an empty  collection for individuals.
	 * @return The empty collection.
	 */
	public static final OntologyIndividualCollection createEmptyOntologyIndividualList() {
		return new OntologyIndividualCollectionImpl();
	}
	
	/**
	 * Creates a new ontology class object.
	 * @param nameWithNamespace The iri of the class. It is divided into a namespace and a local name. 
	 * If the namespace is empty, a default namespace of PackagingToolkit is used.
	 * @return The created ontology class with the given namespace and local name.
	 */
	public final OntologyClass createOntologyClass(final String nameWithNamespace) {
		final AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		int posSharp = nameWithNamespace.indexOf(PackagingToolkitConstants.NAMESPACE_SEPARATOR);
		Namespace namespace;
		
		if (posSharp >= 0) {
			namespace = ServerModelFactory.createNamespace(nameWithNamespace.substring(0, posSharp));
		} else  {
			namespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
		}
		
		final LocalName localname = ServerModelFactory.createLocalName(nameWithNamespace.substring(posSharp + 1));
		final OntologyClass ontologyClass =  new OntologyClassImpl(namespace, localname);
		beanFactory.autowireBean(ontologyClass);
		final String beanName = ontologyClass.getClass().getSimpleName() + ontologyClassCounter;
		beanFactory.initializeBean(ontologyClass, beanName);
		ontologyClassCounter++;
		
		return ontologyClass;
	}

	/**
	 * Creates an empty  collection for datatype properties.
	 * @return The empty collection.
	 */
	public final DatatypePropertyCollection createEmptyDatatypePropertyList() {
		DatatypePropertyCollection datatypePropertyList = new DatatypePropertyCollectionImpl();
		return  datatypePropertyList;
	}

	/**
	 * Creates a collection for individuals.
	 * @param individualListResponse Contains the individuals the collection will be filled with.
	 * @return The collection that has been created.
	 */
	public final OntologyIndividualCollection createOntologyIndividualList(final IndividualListResponse individualListResponse) {
		final OntologyIndividualCollection ontologyIndividualList = createEmptyOntologyIndividualList();
		
		for (int i = 0; i < individualListResponse.getIndividualCount(); i++) {
			final IndividualResponse individualResponse = individualListResponse.getIndividualAt(i);
			final OntologyIndividual ontologyIndividual = createOntologyIndividual(individualResponse);
			ontologyIndividualList.addIndividual(ontologyIndividual);
		}
		
		return ontologyIndividualList;
	}
	
	/**
	 * Creates an individual.
	 * @param individualResponse Contains the data that are used to create the new individual.
	 * @return The created individual. 
	 */
	public final OntologyIndividual createOntologyIndividual(final IndividualResponse individualResponse) {
		final String uuidAsString = individualResponse.getUuid().toString();
		final String uuidOfInformationPackage = individualResponse.getUuidOfInformationPackage().toString();
		final OntologyIndividual ontologyIndividual;
		if (StringValidator.isNotNullOrEmpty(uuidAsString)) {
			ontologyIndividual = new OntologyIndividualImpl(uuidAsString);
			ontologyIndividual.setUuid(uuidAsString);
		} else {
			ontologyIndividual = new OntologyIndividualImpl();
		}
		
		ontologyIndividual.setUuidOfInformationPackage(uuidOfInformationPackage);
		
		final DatatypePropertyListResponse datatypePropertyListResponse =  individualResponse.getOntologyDatatypeProperties();
		for (int i = 0; i < datatypePropertyListResponse.getOntologyPropertyCount(); i++) {
			final DatatypePropertyResponse datatypePropertyResponse = datatypePropertyListResponse.getOntologyPropertyAt(i);
			final OntologyDatatypeProperty ontologyDatatypeProperty = createOntologyProperty(datatypePropertyResponse);
			ontologyIndividual.addDatatypeProperty(ontologyDatatypeProperty);
		}
		
		final ObjectPropertyListResponse objectPropertyListResponse = individualResponse.getOntologyObjectProperties();
		
		for (int i = 0; i < objectPropertyListResponse.getObjectPropertyCount(); i++) {
			final ObjectPropertyResponse objectPropertyResponse = objectPropertyListResponse.getObjectPropertyAt(i);
			final OntologyObjectProperty ontologyObjectProperty = createOntologyObjectProperty(objectPropertyResponse);
			ontologyIndividual.addObjectProperty(ontologyObjectProperty);
		}
		
		return ontologyIndividual;
	}
	
	/**
	 * Creates a datatype property.
	 * @param propertyResponse Contains the values that are used to create the new property.
	 * @return The created property.
	 */
	public final OntologyDatatypeProperty createOntologyProperty(final DatatypePropertyResponse propertyResponse) {
		return (OntologyDatatypeProperty)propertyFactory.getProperty(propertyResponse);
	}
	
	/**
	 * Creates an object property with the values received via http. 
	 * @param objectPropertyResponse The values received via http.
	 * @return The object property with the values received via http. The classes for range and domain are null at this
	 * moment and have to be filled later if necessary.
	 */
	public final OntologyObjectProperty createOntologyObjectProperty(final ObjectPropertyResponse objectPropertyResponse) {
		final String propertyIri = objectPropertyResponse.getPropertyName();
		final String label = objectPropertyResponse.getLabel();
		final String rangeAsString = objectPropertyResponse.getRange();
		
		final OntologyObjectProperty ontologyObjectProperty = new OntologyObjectPropertyImpl(propertyIri, label, rangeAsString);
		for (int i = 0; i < objectPropertyResponse.getDomainCount(); i++) {
			final String domainAsString = objectPropertyResponse.getDomainAt(i);
			ontologyObjectProperty.addPropertyDomain(domainAsString);
		}
		
		final IriListResponse iriListResponse = objectPropertyResponse.getValue();
		final OntologyIndividualCollection ontologyIndividualList = new OntologyIndividualCollectionImpl();
		
		for (int i = 0; i < iriListResponse.getIriCount(); i++) {
			final IriResponse iri = iriListResponse.getIriAt(i);
			final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl();
			ontologyIndividual.setUuid(iri.toString());
			ontologyIndividualList.addIndividual(ontologyIndividual);
		}
		
		ontologyObjectProperty.setPropertyValue(ontologyIndividualList);
		return ontologyObjectProperty;
	}
	/**
	 * Creates an empty  collection for object properties.
	 * @return The empty collection.
	 */
	public final ObjectPropertyCollection createEmptyObjectPropertyList() {
		return new ObjectPropertyCollectionImpl();
	}

	/**
	 * Creates an empty  collection for annotation properties.
	 * @return The empty collection.
	 */
	public AnnotationPropertyCollection createEmptyAnnotationPropertyCollection() {
		return new AnnotationPropertyCollectionImpl();
	}
	
	/**
	 * Sets a reference to the application context of Spring.
	 * @param applicationContext The reference to the application context of Spring. 
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	/**
	 * Sets a reference to the property factory.
	 * @param propertyFactory The reference to the property factory. 
	 */
	public void setPropertyFactory(PropertyFactory propertyFactory) {
		this.propertyFactory = propertyFactory;
	}
}
