package de.feu.kdmp4.packagingtoolkit.server.exchange.classes;

import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;

public class MetadataExchangeImpl implements MetadataExchange {
	/**
	 * The name of the metadata.
	 */
	private String metadataName;
	/**
	 * The value of this metadata. 
	 */
	private String metadataValue;
	
	/**
	 * Creates a new exchange object for a metadata. 
	 * @param metadataName The name of the metadata. May not be empty.
	 * @param metadataValue The value of the metadata. May be empty.
	 */
	public MetadataExchangeImpl(final String metadataName, final String metadataValue) {
		this.metadataName = metadataName;
		this.metadataValue = metadataValue;
	}
	
	@Override
	public String getMetadataName() {
		return metadataName;
	}
	
	@Override
	public String getMetadataValue() {
		return metadataValue;
	}
}
