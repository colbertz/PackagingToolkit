package de.feu.kdmp4.packagingtoolkit.server.factories;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TriplePredicateImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleStatementCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleSubjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.utils.OntologyUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Defines the objects of the model classes that are send to the module TripleStore for saving and send from the
 * TripleStore for further processing. This class contains the only dependencies between interfaces and their
 * implementations. 
 * @author Christopher Olbertz
 *
 */
public abstract class RdfElementFactory {
	/**
	 * The namespace used for standard elements of rdf documents.
	 */
	public static final String NAMESPACE_RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns";
	/**
	 * The predicate for representing that a information package has a reference to a file
	 * in the storage directory. The subject is the uuid of the information package and the
	 * object is the uuid of the file that is used in the database.
	 */
	public static final String PREDICATE_FILE_REFERENCE = "hasFileReference";
	/**
	 * The predicate for representing that a information package has a reference to a remote file that
	 * is accessed via a protocol like http or ftp. The subject is the uuid of the information package and the
	 * object is the url of the file.
	 */
	public static final String PREDICATE_REMOTE_REFERENCE = "hasRemoteReference";
	/**
	 * The predicate for representing the information that an individual of a class in the taxonomy has
	 * been selected. The subject is a taxonomy individual and the object is an information package. 
	 */
	public static final String PREDICATE_TAXONOMY_INDIVIDUAL_ASSIGNED_TO = "taxonomyIndividualIsAssignedTo";
	/**
	 * This predicate is used to describe the datatype of a property. Subject is the property itself and object
	 * is the rdfs datatype.
	 */
	public static final String PREDICATE_HAS_DATATYPE = "hasDatatype";
	/**
	 * The local name for a type property. This property is used for describing that an individual is the instance of a
	 * class. 
	 */
	public static final String LOCALNAME_TYPE = "type";
	/**
	 * Contains the local name of the aggregatedBy property. This property is used for representing the fact that
	 * an individual is contained in an information package. 
	 */
	public static final String LOCALNAME_AGGREGATED_BY = "aggregatedBy";
	/**
	 * Contains the local name of the hasFixity property.
	 */
	public static final String PREDICATE_HAS_FIXITY = "hasFixity";
	
	public static final String PREDICATE_HAS_CREATING_APPLICATION = "hasCreatingApplication";
	/**
	 * This predicate is used to describe the class an object property is related to. Subject is the property itself and object
	 * is the class of the object property.
	 */
	private static final String PREDICATE_HAS_OBJECT = "hasObject";
	
	//public static final String 
	
	/**
	 * Gets the namespace that is used as default in the PackagingToolkit.
	 * @return The default namespace.
	 */
	public static final Namespace getPackagingToolkitDefaultNamespace() {
		return ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
	}
	
	/**
	 * Creates an empty implementation of TripleStatemntCollection. This object contains
	 * several statements. 
	 * @return An empty statement collection.
	 */
	public static final TripleStatementCollection createEmptyTripleStatementCollection() {
		return new TripleStatementCollectionImpl();
	}
	
	/**
	 * Creates a predicate for the property rdf:type that creates an individual.
	 * @return A predicate that represents the property rdf:type.
	 */
	public static final TriplePredicate createTypePredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(NAMESPACE_RDF);
		final LocalName localName = ServerModelFactory.createLocalName(LOCALNAME_TYPE);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(new Uuid(), namespace, localName);
		triplePredicate.setPredicateType(PredicateType.TYPE);
		return triplePredicate;
	}
	
	/**
	 * Creates a predicate for the hasDatatype property that describes which datatype is assigned to a property.
	 * @return A predicate that represents the hasDatatype property.
	 */
	public static final TriplePredicate createHasDatatypePredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(PREDICATE_HAS_DATATYPE);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(namespace, localName);
		triplePredicate.setPredicateType(PredicateType.HAS_DATATYPE);
		return triplePredicate;
	}

	/**
	 * Creates a predicate for the hasFixity property.
	 * @return A predicate that represents the hasFixity property.
	 */
	public static final TriplePredicate createHasFixityPredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(PREDICATE_HAS_FIXITY);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(namespace, localName);
		triplePredicate.setPredicateType(PredicateType.HAS_FIXITY);
		return triplePredicate;
	}
	
	/**
	 * Creates a predicate for the taxonomyIndividualAssignedTo property.
	 * @return A predicate that represents the taxonomyIndividualIsSelected property.
	 */
	public static final TriplePredicate createTaxonomyIndividualIsSelectedPredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(PREDICATE_TAXONOMY_INDIVIDUAL_ASSIGNED_TO);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(namespace, localName);
		triplePredicate.setPredicateType(PredicateType.TAXONOMY_INDIVIDUAL_SELECTED);
		return triplePredicate;
	}
	
	/**
	 * Creates a taxonomyIndividualAssignedTo statement. The subject is the taxonomy individual that is
	 * assigned to an information package. 
	 * @param iriOfTaxonomyIndividual The iri of the taxonomy individual that is assigned to an information
	 * package. This is the subject of the statement.
	 * @param iriOfInformationPackage The iri of the information package the individuals are assigned to.
	 * This is the object of the statement.
	 * @return The statement.
	 */
	public static final TripleStatement createTaxonomyIndividualAssignedStatement(final Iri iriOfTaxonomyIndividual, 
			final Iri iriOfInformationPackage) {
		final TripleSubject tripleSubject = createTripleSubject(iriOfTaxonomyIndividual.toString());
		final TriplePredicate triplePredicate = createTaxonomyIndividualIsSelectedPredicate();
		final TripleObject tripleObject = createTripleObject(iriOfInformationPackage.toString());
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a predicate for the hasCreatingApplication property.
	 * @return A predicate that represents the hasCreatingApplication property.
	 */
	public static final TriplePredicate createHasCreatingApplicationPredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(PREDICATE_HAS_CREATING_APPLICATION);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(namespace, localName);
		triplePredicate.setPredicateType(PredicateType.HAS_CREATING_APPLICATION);
		return triplePredicate;
	}
	
	/**
	 * Creates a triple statement that represents a datatype relationship. This means that the subject has the datatype
	 * represented by the object.
	 * @param propertyObject The object that represents the property. It is used as object in a property statement. It 
	 * contains all information about the datatype of the property. 
	 * @return A statement that represents the hasDatatype relationship of the subject.
	 */
	public static final TripleStatement createHasDatatypeStatement(final TripleStatement propertyStatement) {
		final TripleSubject tripleSubject = new TripleSubjectImpl(new Uuid(), propertyStatement.getPredicate().getNamespace(), 
				propertyStatement.getPredicate().getLocalName());
		final TriplePredicate triplePredicate = createHasDatatypePredicate();
		final OntologyDatatype datatype = propertyStatement.getObject().getDatatype();
		final TripleObject tripleObject = new TripleObjectImpl(datatype.getNamespace(), datatype.getLocalName());
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a triple statement that represents a hasFixity relationship. 
	 * @param propertyObject The object that represents the property. It is used as object in a property statement. It 
	 * contains all information about the datatype of the property. 
	 * @return A statement that represents the hasDatatype relationship of the subject.
	 */
	
	public static final TripleStatement createHasFixityStatement(final String uuidOfIndividual, final String uuidOfFixityIndividual) {
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual);
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(uuidOfFixityIndividual);
		final TriplePredicate triplePredicate = RdfElementFactory.createHasFixityPredicate();
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a triple statement that represents a hasCreatingApplication relationship. 
	 * @param propertyObject The object that represents the property. It is used as object in a property statement. It 
	 * contains all information about the datatype of the property. 
	 * @return A statement that represents the hasCreatingApplicationrelationship of the subject.
	 */
	
	public static final TripleStatement createHasCreatingApplicationStatement(final String uuidOfIndividual, final String uuidOfCreatingApplicationIndividual) {
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual);
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(uuidOfCreatingApplicationIndividual);
		final TriplePredicate triplePredicate = RdfElementFactory.createHasCreatingApplicationPredicate();
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a triple statement that represents a type relationship. This means that the subject is an individual 
	 * of the object.
	 * @param tripleSubject The subject of a rdf statement that represents an individual. 
	 * @param tripleObject The object of the statement that represents the class the individual belongs to. 
	 * @return A statement that represents the type relationship between subject and object.
	 */
	public static final TripleStatement createTypeStatement(final TripleSubject tripleSubject, final TripleObject tripleObject) {
		final TriplePredicate triplePredicate = createTypePredicate();
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a triple statement that represents a reference to a file in the storage directory. The uuid of the
	 * file is the object of the statement
	 * @param uuidOfInformationPackage The uuid of the information package that references the file. This uuid
	 * is the subject of the statement. 
	 * @param uuidOfFile The uuid of the file the information packages is pointing to. This uuid is the object
	 * of the statement. 
	 * @return A statement that represents the the reference of an information package to a file in the storage.
	 */
	public static final TripleStatement createStorageFileStatement(final Uuid uuidOfInformationPackage, final Uuid uuidOfFile) {
		final TriplePredicate triplePredicate = createFileReferencePredicate();
		final TripleSubject tripleSubject = createTripleSubject(uuidOfInformationPackage.toString());
		final TripleObject tripleObject = createTripleObject(uuidOfFile);
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}

	/**
	 * Creates a predicate for the reference to a file in the storage.
	 * @return A predicate that represents the reference to a local file.
	 */
	public static final TriplePredicate createFileReferencePredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(PREDICATE_FILE_REFERENCE);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(new Uuid(), namespace, localName);
		triplePredicate.setPredicateType(PredicateType.LOCAL_FILE_REFERENCE);
		return triplePredicate;
	}

	/**
	 * Creates a triple statement that represents a reference to a remote file. The url of the
	 * file is the object of the statement
	 * @param uuidOfInformationPackage The uuid of the information package that references the file. This uuid
	 * is the subject of the statement. 
	 * @param urlOfFile The url of the file the information packages is pointing to. The url is the object
	 * of the statement. 
	 * @return A statement that represents the the reference of an information package to a remote file.
	 */
	public static final TripleStatement createRemoteFileStatement(final Uuid uuidOfInformationPackage, final String urlOfFile) {
		final TriplePredicate triplePredicate = createRemoteFileReferencePredicate();
		final TripleSubject tripleSubject = createTripleSubject(uuidOfInformationPackage.toString());
		final TripleObject tripleObject = createTripleObject(urlOfFile);
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a predicate for the reference to a remote file.
	 * @return A predicate that represents the reference to a remote file.
	 */
	public static final TriplePredicate createRemoteFileReferencePredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(PREDICATE_REMOTE_REFERENCE);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(new Uuid(), namespace, localName);
		triplePredicate.setPredicateType(PredicateType.REMOTE_FILE_REFERENCE);
		return triplePredicate;
	}
	
	/**
	 * Creates a triple statement that represents a aggregatedBy relationship. This means that an individual is contained 
	 * in an information package. 
	 * During the serialization to OAI-ORE this predicate is used for building the aggregation for an information
	 * package. Furthermore the information expressed by this predicate is used for querying the triple store
	 * for the individuals of a certain information package.
	 * @param individual An individual that is contained in an information package.
	 * @param uuidOfInformationPackage The object is the uuid of the information package that represents an aggregation
	 * of individuals. 
	 * @return A statement that represents the aggregatedBy relationship between an information package and an individual.
	 */
	public static final TripleStatement createAggregatedByStatement(final TripleSubject individual, final TripleObject uuidOfInformationPackage) {
		final TriplePredicate triplePredicate = createAggregatedByPredicate();
		final TripleStatement tripleStatement = createStatement(individual, triplePredicate, uuidOfInformationPackage);
		return tripleStatement;
	}
	
	/**
	 * Creates a predicate that represents the fact that an individual is contained in an information package. 
	 * During the serialization to OAI-ORE this predicate is used for building the aggregation for an information
	 * package. Furthermore the information expressed by this predicate is used for querying the triple store
	 * for the individuals of a certain information package.
	 * @return The predicate that contains the information which information package an individual
	 * belongs to.
	 */
	public static final TriplePredicate createAggregatedByPredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(LOCALNAME_AGGREGATED_BY);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(new Uuid(), namespace, localName);
		triplePredicate.setPredicateType(PredicateType.AGGREGATED_BY);
		return triplePredicate;
	}
	
	/**
	 * Creates a general statement of a rdf document that consists of a subject, an object and a predicate. Subject and
	 * object are connected by the predicate.
	 * @param tripleSubject The subject of the rdf statement.
	 * @param triplePredicate The predicate of the rdf statement.
	 * @param tripleObject The object of the rdf statement.
	 * @return
	 */
	public static final TripleStatement createStatement(final TripleSubject tripleSubject, final TriplePredicate triplePredicate, 
			final TripleObject tripleObject) {
		return new TripleStatementImpl(tripleSubject, triplePredicate, tripleObject);
	}

	/**
	 * Creates a subject that is used in a rdf statement. 
	 * @param iri The iri of the subject.
	 * @return The subject.
	 */
	public static final TripleSubject createTripleSubject(final String iri) {
		return new TripleSubjectImpl(iri);
	}
	
	/**
	 * Creates a predicate that is used in a rdf statement. The most predicate types
	 * can be determined with the help of the iri of the predicate. The iri of the object
	 * is needed for object properties. If the iri of the property contains a valid uuid,
	 * the predicate is a predicate of an object property.
	 * @param iriOfPredicate The iri of the predicate. 
	 * @param iriOfObject The iri of the object. 
	 * @return The predicate.
	 */
	public static final TriplePredicate createTriplePredicate(final String iriOfPredicate, final String iriOfObject) {
		final TriplePredicate triplePredicate = new TriplePredicateImpl(iriOfPredicate); 
		if (iriOfPredicate.contains(LOCALNAME_AGGREGATED_BY)) {
			triplePredicate.setPredicateType(PredicateType.AGGREGATED_BY);
		} else if (iriOfPredicate.contains(LOCALNAME_TYPE)) {
			triplePredicate.setPredicateType(PredicateType.TYPE);
		} else {
			if (StringValidator.isNotNullOrEmpty(iriOfObject)) {
				final Iri iri = ServerModelFactory.createIri(iriOfObject);
				final LocalName localName = iri.getLocalName();
				if (OntologyUtils.isValidUuid(localName.toString())) {
					triplePredicate.setPredicateType(PredicateType.OBJECT_PROPERTY);
				}
			}
		}
		
		if (triplePredicate.getPredicateType() == null) {
			triplePredicate.setPredicateType(PredicateType.LITERAL);
		}
		
		return triplePredicate; 
	}

	/**
	 * Creates an object that is used in a rdf statement.
	 * @param iri The value of the object. Can be a literal or a reference to another individual. 
	 * @return The object.
	 */
	public static final TripleObject createTripleObject(final Object value) {
		return new TripleObjectImpl(value);
	}
	
	/**
	 * Creates an object that is used in a rdf statement and represents a literal.
	 * @param iri The value of the object. Can be a literal or a reference to another individual. 
	 * @return The object.
	 */
	public static final TripleObject createLiteralTripleObject(final Object value) {
		return new TripleObjectImpl(value, true);
	}
	
	/**
	 * Checks if a string contains the iri of a type predicate.
	 * @param iriOfPredicate The string to check.
	 * @return True, if the string contains a type predicate, false otherwise.
	 */
	public static final boolean isTypePredicate(final String iriOfPredicate) {
		if(iriOfPredicate.equals(NAMESPACE_RDF + "#" + LOCALNAME_TYPE)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if a string contains the iri of a aggregatedBy predicate.
	 * @param iriOfPredicate The string to check.
	 * @return True, if the string contains a aggregatedBy predicate, false otherwise.
	 */
	public static final boolean isAggregatedByPredicate(final String iriOfPredicate) {
		if(iriOfPredicate.equals(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT + "#" + LOCALNAME_AGGREGATED_BY)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Creates a triple statement that represents an object relationship. This means that the subject has the class
	 * assigned to the object property.
	 * @param propertyStatement The statement that describes the property. 
	 * @param iriOfRangeAsString The iri of the range of the object property.
	 * @return A statement that represents the hasObject relationship of the subject.
	 */
	public static TripleStatement createHasObjectStatement(final TripleStatement propertyStatement, final String iriOfRangeAsString) {
		final Iri iriOfRange = ServerModelFactory.createIri(iriOfRangeAsString);
		final TripleSubject tripleSubject = new TripleSubjectImpl(new Uuid(), propertyStatement.getPredicate().getNamespace(), 
				propertyStatement.getPredicate().getLocalName());
		final TriplePredicate triplePredicate = createHasObjectPredicate();
		final Namespace namespaceOfRange = iriOfRange.getNamespace();
		final LocalName localNameOfRange = iriOfRange.getLocalName();
		final TripleObject tripleObject = new TripleObjectImpl(namespaceOfRange, localNameOfRange);
		final TripleStatement tripleStatement = createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}

	/**
	 * Creates a predicate for the hasObject property that describes which class is assigned to an object property.
	 * @return A predicate that represents the hasObject property.
	 */
	private static TriplePredicate createHasObjectPredicate() {
		final Namespace namespace = ServerModelFactory.createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
		final LocalName localName = ServerModelFactory.createLocalName(PREDICATE_HAS_OBJECT);
		final TriplePredicate triplePredicate = new TriplePredicateImpl(namespace, localName);
		triplePredicate.setPredicateType(PredicateType.OBJECT_PROPERTY);
		return triplePredicate;
	}
}
