package de.feu.kdmp4.packagingtoolkit.server.enums;

import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * An enum for the languages that are used for the labels in the ontology.
 * Is used by the server to extract the labels in the given languages.
 * @author Christopher Olbertz
 *
 */
public enum OntologyLabelLanguage {
	DEFAULT(StringUtils.EMPTY_STRING), GERMAN("de"), ENGLISH("en"), FRENCH("fr");
	
	private String languageCode;
	
	private OntologyLabelLanguage(final String languageCode) {
		this.languageCode = languageCode;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}
}
