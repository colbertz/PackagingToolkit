package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import java.io.Serializable;
import java.util.Iterator;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class TripleStatementCollectionImpl extends AbstractList  implements TripleStatementCollection, Serializable {
	// ********* Constants **************
	private static final long serialVersionUID = -2774754858876620076L;

	private LiteralStatementsIterator literalStatementsIterator;
	private IndividualsStatementsIterator individualsStatementsIterator;
	private NotLiteralStatementsIterator notLiteralStatementsIterator;
	private ReferenceStatementsIterator referenceStatementsIterator;
	
	// ********* Public methods *********
	public TripleStatementCollectionImpl() {
		literalStatementsIterator = new LiteralStatementsIterator();
		notLiteralStatementsIterator = new NotLiteralStatementsIterator();
		referenceStatementsIterator = new ReferenceStatementsIterator();
	}
	
	@Override
	public void removeTripleStatement(final TripleStatement tripleStatement) {
		super.remove(tripleStatement);
	}
	
	@Override
	public void addTripleStatement(final TripleStatement tripleStatement) {
		super.add(tripleStatement);
		/* May not be done in the constructor because the iterator cannot be initialized 
			 with the first statement: There is no first statement.
		*/
		if (literalStatementsIterator == null) {
			
		}
	}
	
	@Override
	public TripleStatement getTripleStatement(final int index) {
		return (TripleStatement)super.getElement(index);
	}
	
	@Override
	public int getTripleStatementsCount() {
		return super.getSize();
	}

	@Override
	public void addStatements(TripleStatementCollection statementList) {
		for (int i = 0; i < statementList.getTripleStatementsCount(); i++) {
			TripleStatement tripleStatement = statementList.getTripleStatement(i);
			addTripleStatement(tripleStatement);
		}
	}
	
	@Override
	public String toString() {
		StringBuffer output = new StringBuffer();
		
		for (int i = 0; i < getSize(); i++) {
			TripleStatement currentStatement = getTripleStatement(i);
			output.append(currentStatement.toString()).append("\n");
		}
		
		return output.toString();
	}
	
	@Override
	public boolean hasNextReferenceStatement() {
		return referenceStatementsIterator.hasNext();
	}
	
	@Override
	public TripleStatement getNextReferenceStatement() {
		return referenceStatementsIterator.next();
	}
	
	@Override
	public boolean hasNextLiteralStatement() {
		return literalStatementsIterator.hasNext();
	}
	
	@Override
	public TripleStatement getNextLiteralStatement() {
		return literalStatementsIterator.next();
	}
	
	@Override
	public boolean hasNextNotLiteralStatement() {
		return notLiteralStatementsIterator.hasNext();
	}

	@Override
	public TripleStatement getNextNotLiteralStatement() {
		return notLiteralStatementsIterator.next();
	}	
	
	@Override
	public TripleStatement getNextStatementOfIndividual(final String iriOfIndividual) {
		if (individualsStatementsIterator == null) {
			individualsStatementsIterator = new IndividualsStatementsIterator(iriOfIndividual);
		} else {
			String currentIriOfIndividual = individualsStatementsIterator.getIriOfIndividual();
			if (StringUtils.areStringsUnequal(iriOfIndividual, currentIriOfIndividual)) {
				int statementCounter = individualsStatementsIterator.counter;
				individualsStatementsIterator = new IndividualsStatementsIterator(iriOfIndividual, statementCounter);
			}
		}
		return individualsStatementsIterator.next();
	}
	
	/*
	 * 	@Override
	public boolean hasNextStatementOfIndividual(String iriOfIndividual) {
		if (individualsStatementsIterator == null) {
			individualsStatementsIterator = new IndividualsStatementsIterator(iriOfIndividual);
		} else {
			String currentIriOfIndividual = individualsStatementsIterator.getIriOfIndividual();
			if (StringUtils.areStringsEqual(iriOfIndividual, currentIriOfIndividual)) {
				//int statementCounter = individualsStatementsIterator.counter;
				//individualsStatementsIterator = new IndividualsStatementsIterator(iriOfIndividual, statementCounter);
			} else {
				individualsStatementsIterator = new IndividualsStatementsIterator(iriOfIndividual);
			}
		}
		return individualsStatementsIterator.hasNext();
	}
	 */
	
	@Override
	public boolean hasNextStatementOfIndividual(final String iriOfIndividual) {
		if (individualsStatementsIterator == null) {
			individualsStatementsIterator = new IndividualsStatementsIterator(iriOfIndividual);
		} else {
			String currentIriOfIndividual = individualsStatementsIterator.getIriOfIndividual();
			if (StringUtils.areStringsUnequal(iriOfIndividual, currentIriOfIndividual)) {
				int statementCounter = individualsStatementsIterator.counter;
				individualsStatementsIterator = new IndividualsStatementsIterator(iriOfIndividual, statementCounter);
			}
		}
		return individualsStatementsIterator.hasNext();
	}
	
	@Override
	public TripleStatementCollection sortBySubject() {
		Object[] objects = toArray();
		TripleStatement[] tripleStatements = new TripleStatement[objects.length];
		
		for (int i = 0; i < tripleStatements.length; i++) {
			tripleStatements[i] = (TripleStatement)objects[i];
		}
		
		for(int i = getSize() - 1; i >  0; i--) {
			for (int j = 0; j <= i - 1; j++) {
				TripleStatement currentStatement = tripleStatements[j];
				TripleSubject currentSubject = currentStatement.getSubject();
				Iri currentIndividualsIri = currentSubject.getIri();
				
				TripleStatement nextStatement = tripleStatements[j+1];
				TripleSubject nextSubject = nextStatement.getSubject();
				Iri nextIndividualsIri = nextSubject.getIri();
				
				if (currentIndividualsIri.compareTo(nextIndividualsIri) >  0) {
					TripleStatement tempStatement = tripleStatements[j];
					tripleStatements[j] = tripleStatements[j+1];
					tripleStatements[j+1] = tempStatement;
				}
			}
		}

		TripleStatementCollection sortedStatementList = new TripleStatementCollectionImpl();
		
		for (int i = 0; i < tripleStatements.length; i++) {
			TripleStatement currentStatement = tripleStatements[i];
			sortedStatementList.addTripleStatement(currentStatement);
		}
		
		return sortedStatementList;
	}
	
	/**
	 * Iterates through all statements that have a literal as object.
	 * @author Christopher Olbertz
	 *
	 */
	private class LiteralStatementsIterator implements Iterator<TripleStatement> {
		/**
		 * The current statement. This is returned by the next call of the
		 * method next().
		 */
		private TripleStatement currentStatement;
		/**
		 * Counts the statements that have been already processed. 
		 */
		private int counter;
		
		public LiteralStatementsIterator() {
			counter = 0;
		}
		
		@Override
		public boolean hasNext() {
			int size = getTripleStatementsCount();
			if (counter < size) {
				for (int i = counter; i < size; i++) {
					TripleStatement tripleStatement = getTripleStatement(i);
					if (tripleStatement.isLiteralStatement()) {
						currentStatement = tripleStatement;
						return true;
					}
				}
				return false;
			} else {
				return false;
			}
		}

		@Override
		public TripleStatement next() {
			if (hasNext()) {
				counter++;
				return currentStatement;
			} else {
				return null;
			}
			
		}
	}
	
	/**
	 * Iterates through all statements that describe a reference.
	 * @author Christopher Olbertz
	 *
	 */
	private class ReferenceStatementsIterator implements Iterator<TripleStatement> {
		/**
		 * The current statement. This is returned by the next call of the
		 * method next().
		 */
		private TripleStatement currentStatement;
		/**
		 * Counts the statements that have been already processed. 
		 */
		private int counter;
		
		public ReferenceStatementsIterator() {
			counter = 0;
		}
		
		@Override
		public boolean hasNext() {
			int size = getTripleStatementsCount();
			if (counter < size) {
				for (int i = counter; i < size; i++) {
					TripleStatement tripleStatement = getTripleStatement(i);
					if (tripleStatement.isReferenceStatement()) {
						currentStatement = tripleStatement;
						return true;
					}
				}
				return false;
			} else {
				return false;
			}
		}

		@Override
		public TripleStatement next() {
			if (hasNext()) {
				counter++;
				return currentStatement;
			} else {
				return null;
			}
			
		}
	}
	
	/**
	 * Iterates through all statements that have not a literal as object.
	 * @author Christopher Olbertz
	 *
	 */
	private class NotLiteralStatementsIterator implements Iterator<TripleStatement> {
		/**
		 * The current statement. This is returned by the next call of the
		 * method next().
		 */
		private TripleStatement currentStatement;
		/**
		 * Counts the statements that have been already processed. 
		 */
		private int counter;
		
		public NotLiteralStatementsIterator() {
			counter = 0;
		}
		
		@Override
		public boolean hasNext() {
			int size = getTripleStatementsCount();
			if (counter < size) {
				for (int i = counter; i < size; i++) {
					TripleStatement tripleStatement = getTripleStatement(i);
					if (!tripleStatement.isLiteralStatement()) {
						currentStatement = tripleStatement;
						return true;
					}
				}
				return false;
			} else {
				return false;
			}
		}

		@Override
		public TripleStatement next() {
			if (hasNext()) {
				counter++;
				return currentStatement;
			} else {
				return null;
			}
		}
	}	
	
	/**
	 * Iterates through all statements of a given individual. The individual is determined by its 
	 * uuid.
	 * @author Christopher Olbertz
	 *
	 */
	private class IndividualsStatementsIterator implements Iterator<TripleStatement> {
		/**
		 * The current statement. This is returned by the next call of the
		 * method next().
		 */
		private TripleStatement currentStatement;
		/**
		 * Counts the statements that have been already processed. 
		 */
		private int counter;
		/**
		 * The iri of the individual whose statements we are looking for.
		 */
		private String iriOfIndividual;
		
		public IndividualsStatementsIterator(String iriOfIndividual) {
			counter = 0;
			this.iriOfIndividual = iriOfIndividual;
		}
		
		public IndividualsStatementsIterator(String iriOfIndividual, int counter) {
			this.counter = counter;
			this.iriOfIndividual = iriOfIndividual;
		}
		
		public String getIriOfIndividual() {
			return iriOfIndividual;
		}
		
		@Override
		public boolean hasNext() {
			int size = getTripleStatementsCount();
			if (counter < size) {
				for (int i = counter; i < size; i++) {
					TripleStatement tripleStatement = getTripleStatement(i);
					Iri currentIriOfIndividual = tripleStatement.getSubject().getIri();
					
					if (iriOfIndividual.equals(currentIriOfIndividual.toString())) {
						currentStatement = tripleStatement;
						return true;
					}
				}
				return false;
			} else {
				return false;
			}
		}

		@Override
		public TripleStatement next() {
			if (hasNext()) {
				counter++;
				return currentStatement;
			} else {
				return null;
			}
		}
	}

	@Override
	public TripleStatementCollection getIndividualStatements() {
		TripleStatementCollection individualStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		
		for (int i = 0; i < getSize(); i++) {
			TripleStatement tripleStatement = getTripleStatement(i);
			if (tripleStatement.isAggregatedByStatement()) {
				individualStatements.addTripleStatement(tripleStatement);
			}
		}
		
		return individualStatements;
	}

	@Override
	public boolean containsTripleStatement(TripleStatement tripleStatement) {
		for (int i = 0; i < getTripleStatementsCount(); i++) {
			TripleStatement currentTripleStatement = getTripleStatement(i);
			if (currentTripleStatement.equals(tripleStatement)) {
				return true;
			}
		}
		
		return false;
	}
	
	/*@Override
	public OntologyDatatype getDatatypeOfProperty(String iriOfProperty) {
		for (int i = 0; i < getSize(); i++) {
			TripleStatement tripleStatement = getTripleStatement(i);
			TriplePredicate triplePredicate = tripleStatement.getPredicate();
			TripleObject tripleObject = tripleStatement.getObject();
			
			if (tripleStatement.isHasDatatypeStatement()) {
				if (StringUtils.areStringsEqual(triplePredicate.getIri(), iriOfProperty)) {
					OntologyDatatype datatype = tripleObject.getDatatype();
					return datatype;
				}
			}
		}
		
		return null;
	}*/
}
