package de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;

/**
 * Maps meta data elements to properties in an ontology. The map contains as key the name of the 
 * meta data element and as value the iri of the property. An entry in this map could have the
 * following structure:
 * <br />
 * myMetadata1=http://my.namespace.de#myProperty1
 * <br />
 * This means that the value of metadata1 of a digital object is saved as value of the property 
 * http://my.namespace.de#myProperty1 in the triple store for the current information package. The 
 * application looks for the properties in the ontology cache on the server. This means that all
 * classes in all ontologies that are processed by the server can be used.
 * <br />
 * This map does not contain any values of meta data elements. It only contains the configuration of 
 * the mapping. If any meta data elements are received from the mediator, this map helps to determine
 * the triples for saving the meta data in the triple store. 
 * @author Christopher Olbertz
 *
 */
public interface MetadataMappingMap {
	/**
	 * Adds an entry with nameOfMetadataElement as key and iriOfProperty as value. This means that this
	 * meta data element is mapped to this property. 
	 * @param nameOfMetadataElement The name of the meta data element whose value is mapped to the 
	 * property. Correspondents to a meta data element returned by the mediator.
	 * @param iriOfProperty The iri of the property the meta data element is mapped to. 
	 */
	void addMetadataMapping(final String nameOfMetadataElement, final Iri iriOfProperty);
	/**
	 * Gets the iri of the property nameOfMetadataElement is mapped to. 
	 * @param nameOfMetadataElement The name of the meta data element we are looking for. 
	 * @return The iri of the property nameOfMetadataElement is mapped to. 
	 */
	Optional<Iri> getIriOfPropertyMappedBy(final String nameOfMetadataElement);
	/**
	 * Checks if there is a property mapped to a certain metadata element name.
	 * @param nameOfMetadataElement The name of the meta data element.
	 * @return  True if there is a property mapped to this name.
	 */
	boolean containsProperty(final String nameOfMetadataElement);
	/**
	 * Finds the iri of the property that contains the message digest algorithm according to the configuration.
	 * @return The iri of the property that contains the message digest algorithm.
	 */
	Optional<Iri> getIriOfMessageDigestAlgorithmProperty();
	/**
	 * Finds the iri of the property that contains the message digest according to the configuration.
	 * @return The iri of the property that contains the message digest.
	 */
	Optional<Iri> getIriOfMessageDigestProperty();
	/**
	 * Finds the iri of the class for the object characteristics according to the configuration.
	 * @return The iri of the class for the object characteristics.
	 */
	Optional<Iri> getIriOfObjectCharacteristicsClass();
	/**
	 * Finds the iri of the class for the creating application according to the configuration.
	 * @return The iri of the class for the creating application.
	 */
	Optional<Iri> getIriOfCreatingApplicationClass();
	/**
	 * Finds the iri of the property that contains the name of the application according to the configuration.
	 * @return The iri of the property that contains the name of the application.
	 */
	Optional<Iri> getIriOfCreatingApplicationNameProperty();
	/**
	 * Finds the iri of the class for the file according to the configuration.
	 * @return The iri of the class for the file.
	 */
	Optional<Iri> getIriOfFileClass();
	/**
	 * Finds the iri of the class for the intellectual entity according to the configuration.
	 * @return The iri of the class for the intellectual entity .
	 */
	Optional<Iri> getIriOfIntellectualEntityClass();
	/**
	 * Finds the iri of the property that contains the hasObjectCharacteristics property according to the configuration.
	 * @return The iri of the property that contains the hasObjectCharacteristics property.
	 */
	Optional<Iri> getIriOfHasObjectCharacteristicsProperty();
}
