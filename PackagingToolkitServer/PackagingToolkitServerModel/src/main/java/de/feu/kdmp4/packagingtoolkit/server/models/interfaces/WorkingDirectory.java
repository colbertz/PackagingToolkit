package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitFileException;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Describes the working directory of the server. The working directory is the directory the server uses for its work. There are several
 * data saved. A typical structure of a working directory looks as follows:
 * 
 * <ul>
 * 	<li>configuration: contains configuration files like the ontology files.</li>
 * 	<li>data: contains the data of the triple store.</li>
 * 	<li>storage: contains the digital objects that are saved on the server.</li>
 * 	<li>temp: contains a subdirectory for every information package zip archives are created of. Every subdirectory contains the files
 * that are included in the resulting zip file. These directories are deleted after the creation and the download of the zip file. The names of
 * the subdirectories are the uuids of the information packages.
 * 			<ul>
 * 				<li>uuidOfInformationPackage1: contains the files of information package 1</li>
 * 				<li>uuidOfInformationPackage2: contains the files of information package 2</li>
 * 			</ul>
 * 	</li>
 * </ul>
 * 
 * <br />
 * 
 * The classes that are implementing this interface provide methods for accessing these subdirectories. This class is the only one
 * that knows the concrete paths of these directories.  
 * 
 * @author Christopher Olbertz
 *
 */
public interface WorkingDirectory {
	/**
	 * Gets a path in the temporary directory. The path names the directory where the files for a zip archive are
	 * cached before they are zipped. If the user wants to download an information package from the server, its files
	 * are saved in a directory called temp/uuidOfInformationPackage. This method creates a path for a file in the 
	 * temporary directory of a certain information package.
	 * @param filename The name of the file in the temporary directory: temp/uuidOfInformationPackage/filename 
	 * @param uuid The uuid of the information package the file belongs to. This is the name of the subdirectory that contains
	 * the files of the information package and that is zipped into an archive.
	 * @return The path after the pattern temp/uuidOfInformationPackage/filename.
	 */
	File getFileInTempDirectory(final String filename, final String uuid);
	/**
	 * Creates the path of the working directory as string.
	 * @return The path of the working directory as string.
	 */
	String getWorkingDirectoryPath();
	/**
	 * Returns the temporary directory as File. This means the directory that contains the subdirectories for the information packages.
	 * @return The temporary directory as File.
	 */
	File getTemporaryDirectory();
	/**
	 * Determines the temporary directory for an certain information package. This directory contains the files that are zipped into the 
	 * zip file of this information package.
	 * @param uuid The uuid of the information package.
	 * @return The path of the temporary directory if this information package as String.
	 */
	File getTemporaryDirectoryForUuid(final String uuid);
	/**
	 * Determines the temporary directory for an certain information package. This directory contains the files that are zipped into the 
	 * zip file of this information package.
	 * @param uuid The uuid of the information package.
	 * @return The path of the temporary directory if this information package as File.
	 */
	File getTemporaryDirectoryForUuid(final Uuid uuid);
	/**
	 * Checks the temporary directory for a certain information package already exists.
	 * @param uuid The uuid of the information package.
	 * @return True if the temporary directory already exists, false otherwise.
	 */
	boolean temporaryDirectoryForUuidExists(final Uuid uuid);
	/**
	 * Returns the storage directory within the working directory.
	 * @return The storage directory within the working directory.
	 */
	File getStorageDirectory();
	/**
	 * Returns a file in the storage directory. 
	 * @param The name of the file. 
	 * @return A file in the storage directoy: storageDirectory/filename
	 */
	File getFileInStorageDirectory(final String filename);
	/**
	 * Returns the data directory within the working directory.
	 * @return The data directory within the working directory.
	 */
	File getDataDirectory();
	/**
	 * Returns the configuration directory that is a subdirectory of the working directory. Contains e.g. the ontology files.
	 * @return The configuation directory.
	 */
	File getConfigurationDirectory();
	/**
	 * Determines the temporary directory for a certain information packages or creates it if it does not exist.
	 * @param uuid The uuid of the information package.
	 * @return The temporary of the information package with the given uuid. 
	 */
	File getTemporaryDirectoryForUuidOrCreate(final String uuid);
	/**
	 * Deletes a temporary directory and all contained temporary files. 
	 * @param uuidOfInformationPackage The uuid of the information package whose temporary directory should
	 * be deleted.
	 * @throws PackagingException is thrown if archiveDirectory does not exist
	 * or if archiveDirectory does not contain any files.
	 * @throws PackagingToolkitFileException is thrown if archiveDirectory does not represent a directory.
	 */
	void deleteTemporaryDirectoryIfExists(final String uuidOfInformationPackage);
	/**
	 * Saves data as file in the configruation  directory of the server.
	 * @param fileContent The content of the file.
	 * @param filename The name of the file
	 * @return The saved file.
	 */
	File saveFileContentInConfigurationDirectory(final byte[] fileContent, final String filename);
}
