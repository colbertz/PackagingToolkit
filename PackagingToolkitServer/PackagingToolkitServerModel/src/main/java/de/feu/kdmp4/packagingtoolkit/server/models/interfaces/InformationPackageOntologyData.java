package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

// DELETEME
@Deprecated
public interface InformationPackageOntologyData {
	/**
	 * Adds a statement to the data of an information package.
	 * @param statement The statement.
	 */
	void addStatement(TripleStatement statement);

}
