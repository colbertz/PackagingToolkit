package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

/**
 * Represents a RDF statement that consists of a subject, a predicate and an object.
 * @author Christopher Olbertz
 *
 */
public interface TripleStatement {
	/**
	 * Determines if the statement contains a literal as object.
	 * @return True if the statement contains a literal as object, false otherwise.
	 */
	boolean isLiteralStatement();
	/**
	 * Gets the subject of this statement.
	 * @return The subject of this statement.
	 */
	TripleSubject getSubject();
	/**
	 * Gets the predicate of this statement.
	 * @return The predicate of this statement.
	 */
	TriplePredicate getPredicate();
	/**
	 * Gets the object of this statement.
	 * @return The object of this statement.
	 */
	TripleObject getObject();
	/**
	 * Determines if the statement contains a reference predicate. 
	 * @return True if the statement contains a reference predicate, false otherwise.
	 */
	boolean isReferenceStatement();
	/**
	 * Determines if the statement contains a aggregatedBy predicate. 
	 * @return True if the statement contains a aggregatedBy predicate, false otherwise.
	 */	
	boolean isAggregatedByStatement();
	/**
	 * Determines if the statement contains a hasDatatype predicate. 
	 * @return True if the statement contains a hasDatatype predicate, false otherwise.
	 */	
	boolean isHasDatatypeStatement();
}
