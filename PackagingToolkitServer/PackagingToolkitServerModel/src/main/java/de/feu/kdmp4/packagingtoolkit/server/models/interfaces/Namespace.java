package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

/**
 * Encapsulates a string that is a namespace. This interface and the implementing
 * class are used because there are methods that get a namespace and a local name as
 * String parameters. If you have to use these classes as parameters, you cannot mix up
 * the namespace and the local name.
 * @author Christopher Olbertz
 *
 */
public interface Namespace {
	String getNamespace();
	/**
	 * Checks if there is a text in this namespace. 
	 * @return True if there is a namespace contained in this object.
	 */
	boolean isNotEmpty();
}
