package de.feu.kdmp4.packagingtoolkit.server.datastructures.classes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;

public class MetadataMappingHashMapImpl implements MetadataMappingMap {
	/**
	 * The property that contains the iri of the creating application.
	 */
	private static final String CREATING_APPLICATION = "creatingApplication";
	/**
	 * The property that contains the iri of the property with the creating application.
	 */
	private static final String CREATING_APPLICATION_NAME = "creatingApplicationName";
	/**
	 * The property that contains the iri of the class with the file.
	 */
	private static final String FILE = "file";
	/**
	 * The property that contains the iri of the class with the intellectual entity.
	 */
	private static final String INTELLECTUAL_ENTITY = "intellectualEntity";
	/**
	 * The property that contains the iri of the hasObjectCharacteristics property.
	 */
	private static final String HAS_OBJECT_CHARACTERISTICS = "hasObjectCharacteristics";
	/**
	 * The property that contains the iri of the message digest.
	 */
	public static final String MESSAGE_DIGEST = "messageDigest";
	/**
	 * The property that contains the iri of the message digest algorithm.
	 */
	public static final String MESSAGE_DIGEST_ALGORITHM = "messageDigestAlgorithm";
	/**
	 * The property that contains the iri of the objects characteristics.
	 */
	private static final String OBJECT_CHARACTERISTICS = "objectCharacteristics";
	
	/**
	 * The map that contains the mapping data: the name of a meta data element as key and
	 * the iri of the corresponding property as value. 
	 */
	private Map<String, Iri> metadataMappingMap;
	
	/**
	 * Initializes the necessary objects.
	 */
	public MetadataMappingHashMapImpl() {
		metadataMappingMap = new HashMap<>();
	}
	
	@Override
	public void addMetadataMapping(String nameOfMetadataElement, Iri iriOfProperty) { 
		metadataMappingMap.put(nameOfMetadataElement, iriOfProperty);
	}
	
	@Override
	public Optional<Iri> getIriOfPropertyMappedBy(String nameOfMetadataElement) {
		Iri iriOfMetadataElement = metadataMappingMap.get(nameOfMetadataElement); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}

	@Override
	public boolean containsProperty(String nameOfMetadataElement) {
		if (metadataMappingMap.containsKey(nameOfMetadataElement)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Optional<Iri> getIriOfMessageDigestAlgorithmProperty() {
		Iri iriOfMetadataElement = metadataMappingMap.get(MESSAGE_DIGEST_ALGORITHM); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}
	
	@Override
	public Optional<Iri> getIriOfMessageDigestProperty() {
		Iri iriOfMetadataElement = metadataMappingMap.get(MESSAGE_DIGEST); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}

	@Override
	public Optional<Iri> getIriOfObjectCharacteristicsClass() {
		Iri iriOfMetadataElement = metadataMappingMap.get(OBJECT_CHARACTERISTICS); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}

	@Override
	public Optional<Iri> getIriOfCreatingApplicationClass() {
		Iri iriOfMetadataElement = metadataMappingMap.get(CREATING_APPLICATION); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}
	
	@Override
	public Optional<Iri> getIriOfCreatingApplicationNameProperty() {
		Iri iriOfMetadataElement = metadataMappingMap.get(CREATING_APPLICATION_NAME); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}

	@Override
	public Optional<Iri> getIriOfFileClass() {
		Iri iriOfMetadataElement = metadataMappingMap.get(FILE); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}

	@Override
	public Optional<Iri> getIriOfIntellectualEntityClass() {
		Iri iriOfMetadataElement = metadataMappingMap.get(INTELLECTUAL_ENTITY); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}
	
	@Override
	public Optional<Iri> getIriOfHasObjectCharacteristicsProperty() {
		Iri iriOfMetadataElement = metadataMappingMap.get(HAS_OBJECT_CHARACTERISTICS); 
		Optional<Iri> optionalWithIri = ServerOptionalFactory.createOptionalWithIri(iriOfMetadataElement);
		return optionalWithIri;
	}
}
