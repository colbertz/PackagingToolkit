package de.feu.kdmp4.packagingtoolkit.server.exchange.classes;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;

public class DigitalObjectExchangeImpl implements DigitalObjectExchange {
	/**
	 * The path of the file in the storage directory of the server.
	 */
	private String filePathInStorage;
	/**
	 * The uuid of the digital object.
	 */
	private Uuid uuidOfDigitalObject;
	/**
	 * The md5 checksum of the file the digital object is refering to.
	 */
	private String md5Checksum;
	
	/**
	 * Creates a new object.
	 * @param filePathInStorage The path of the file in the storage directory of the server.
	 * @param uuidOfDigitalObject The uuid of the digital object.
	 * @param md5Checksum The md5 checksum of the file the digital object is refering to.
	 */
	public DigitalObjectExchangeImpl(final String filePathInStorage, final Uuid uuidOfDigitalObject, final String md5Checksum) {
		this.filePathInStorage = filePathInStorage;
		this.uuidOfDigitalObject = uuidOfDigitalObject;
		this.md5Checksum = md5Checksum;
	}

	@Override
	public String getFilePathInStorage() {
		return filePathInStorage;
	}

	@Override
	public Uuid getUuidOfDigitalObject() {
		return uuidOfDigitalObject;
	}
	
	@Override
	public String getMd5Checksum() {
		return md5Checksum;
	}
}
