/**
 * Contains the exchange classes. These are used for exchanging data between modules. 
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.exchange.classes;