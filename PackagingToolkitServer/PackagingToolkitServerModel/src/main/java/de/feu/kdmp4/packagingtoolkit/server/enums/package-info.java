/**
 * Contains some enums that are important for the ontology module.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.enums;