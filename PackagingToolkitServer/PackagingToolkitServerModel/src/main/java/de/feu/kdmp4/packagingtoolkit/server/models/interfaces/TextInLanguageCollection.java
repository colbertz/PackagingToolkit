package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;

/**
 * Contains some TextInLanguage objects.
 * @author Christopher Olbertz
 *
 */
public interface TextInLanguageCollection {
	/**
	 * Adds a new oject to the collection.
	 * @param textInLanguage The object that should be added to the collection.
	 */
	void addTextInLanguage(final TextInLanguage textInLanguage);
	/**
	 * Returns an object at a given index.
	 * @param index The index of the object we are interested in.
	 * @return The found object.
	 */
	TextInLanguage getTextInLanguage(final int index);
	/**
	 * Determines the number of objects in the collection.
	 * @return The number of objects in the collection.
	 */
	int getTextsInLanguageCount();
	/**
	 * Looks for the text in a given language. 
	 * @param language The language of the text to look for.
	 * @return The found text. 
	 */
	Optional<TextInLanguage>  getTextByLanguage(final OntologyLabelLanguage language);
	/**
	 * Adds a new oject to the collection.
	 * @param text The text of a label.
	 * @param language The language the text is written in.
	 */
	void addTextInLanguage(final String text, final OntologyLabelLanguage language);
	/**
	 * Determines if the collection contains any objects.
	 * @return True if the collection is empty, false otherwise.
	 */
	boolean isEmpty() ;
}
