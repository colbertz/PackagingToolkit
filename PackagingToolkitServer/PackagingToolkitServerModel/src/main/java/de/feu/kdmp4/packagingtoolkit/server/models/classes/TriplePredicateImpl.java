package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;

public class TriplePredicateImpl implements TriplePredicate {
	/**
	 * The uuid of the predicate.
	 */
	private Uuid uuid;
	/** 
	 * Represents the type of relation this predicate expresses.
	 */
	private PredicateType predicateType;
	/**
	 * The iri of this predicate. An iri consists of the namespace followed by a # symbol and then the local name.
	 */
	private Iri iri;
	
	/**
	 * Creates a predicate with its iri. From the iri the namespace and the local name is extracted. If the iri is not valid - it does not
	 * contain a # symbol as separator between namespace and local name - the iri as used as local name and the default namespace
	 * of the PackagingToolkit is used as namespace.
	 * @param iri The iri that describes the predicate.
	 */
	public TriplePredicateImpl(final String iri) {
		this.iri = ServerModelFactory.createIri(iri);
		uuid = new Uuid();
	}
	
	/**
	 * Creates a predicate with an explicit namespace and local name. An iri is created with the localname and the namespace.
	 * @param uuid The uuid of the predicate.
	 * @param namespace The namespace of the predicate. Thats the part before the # in the iri.
	 * @param localName The localname of the predicate. Thats the part after the # in the iri.
	 */
	public TriplePredicateImpl(final Uuid uuid, final Namespace namespace, final LocalName localName) {
		super();
		this.uuid = uuid;
		this.iri = ServerModelFactory.createIri(namespace, localName);
	}
	
	/**
	 * Creates a predicate with an explicit namespace and local name. An iri is created with the localname and the namespace.
	 * @param namespace The namespace of the predicate. Thats the part before the # in the iri.
	 * @param localName The localname of the predicate. Thats the part after the # in the iri.
	 */
	public TriplePredicateImpl(final Namespace namespace, final LocalName localName) {
		super();
		this.uuid = new Uuid();
		this.iri = ServerModelFactory.createIri(namespace, localName);
	}

	@Override
	public Uuid getUuid() {
		return uuid;
	}

	@Override
	public PredicateType getPredicateType() {
		return predicateType;
	}

	@Override
	public void setPredicateType(final PredicateType predicateType) {
		this.predicateType = predicateType;
	}
	
	@Override
	public Iri getIri() {
		return iri;
	}

	@Override
	public String toString() {
		return iri.toString();
	}

	@Override
	public boolean isLocalFilePredicate() {
		if (predicateType.equals(PredicateType.LOCAL_FILE_REFERENCE)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isRemoteFilePredicate() {
		if (predicateType.equals(PredicateType.REMOTE_FILE_REFERENCE)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isTypePredicate() {
		if (predicateType.equals(PredicateType.TYPE)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isLiteralPredicate() {
		if (predicateType.equals(PredicateType.LITERAL)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isObjectPredicate() {
		if (predicateType.equals(PredicateType.OBJECT_PROPERTY)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Namespace getNamespace() {
		return iri.getNamespace();
	}

	@Override
	public LocalName getLocalName() {
		return iri.getLocalName();
	}

	@Override
	public boolean isTaxonomyIndividualStatement() {
		if (predicateType.equals(PredicateType.TAXONOMY_INDIVIDUAL_SELECTED)) {
			return true;
		} else {
			return false;
		}
	}
}
