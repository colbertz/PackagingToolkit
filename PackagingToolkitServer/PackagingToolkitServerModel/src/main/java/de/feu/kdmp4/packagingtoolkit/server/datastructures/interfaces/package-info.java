/**
 * Contains the interfaces for some data structures.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces;