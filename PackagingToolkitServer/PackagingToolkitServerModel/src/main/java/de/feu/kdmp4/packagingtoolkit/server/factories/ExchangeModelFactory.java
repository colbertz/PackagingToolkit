package de.feu.kdmp4.packagingtoolkit.server.factories;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.exchange.classes.DigitalObjectExchangeImpl;
import de.feu.kdmp4.packagingtoolkit.server.exchange.classes.InformationPackageExchangeCollectionListImpl;
import de.feu.kdmp4.packagingtoolkit.server.exchange.classes.InformationPackageExchangeImpl;
import de.feu.kdmp4.packagingtoolkit.server.exchange.classes.MetadataExchangeCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.exchange.classes.MetadataExchangeImpl;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;

/**
 * Contains the methods that create exchange objects. This factory is used for decoupling the implementations from their
 * interfaces. So, only the interfaces are used in the application and the only constructor calls are in this factory.
 * @author Christopher Olbertz
 *
 */
public class ExchangeModelFactory {
	/**
	 * Creates an exchange object with the data of an information package.
	 * @param title The title of the information package.
	 * @param uuid The uuid of the information package. 
	 * @param packageType The package type of the information package. At the moment only SIP is supported.
	 * @param creationDate The date the information package was created.
	 * @param lastModifiedDate The date the information package was modified the last time.
	 * @param serializationFormat The format the manifest file of the information package is serialized. At the moment only
	 * OAI-ORE is supported.
	 * @param informationPackageId The id of the information package in the database. It is the value of the primary key.
	 * @return The newly created object.
	 */
	public static final InformationPackageExchange createInformationPackageExchange(final String title, final String uuid, 
			final PackageType packageType, final LocalDateTime creationDate, final LocalDateTime lastModifiedDate, 
			final SerializationFormat serializationFormat, final long informationPackageId) {
		final InformationPackageExchange informationPackageExchange = new InformationPackageExchangeImpl();
		informationPackageExchange.setCreationDate(creationDate);
		informationPackageExchange.setLastModifiedDate(lastModifiedDate);
		informationPackageExchange.setPackageType(packageType);
		informationPackageExchange.setTitle(title);
		informationPackageExchange.setUuid(uuid);
		informationPackageExchange.setSerializationFormat(serializationFormat);
		informationPackageExchange.setInformationPackageId(informationPackageId);
		
		return informationPackageExchange;
	}
	
	/**
	 * Creates an exchange object with the data of an information package without a value for the primary key. This constructor is
	 * used for an object that is not persistent in the database yet.
	 * @param title The title of the information package.
	 * @param uuid The uuid of the information package. 
	 * @param packageType The package type of the information package. At the moment only SIP is supported.
	 * @param creationDate The date the information package was created.
	 * @param lastModifiedDate The date the information package was modified the last time.
	 * @param serializationFormat The format the manifest file of the information package is serialized. At the moment only
	 * OAI-ORE is supported.
	 * @return The newly created object.
	 */
	public static final InformationPackageExchange createInformationPackageExchange(final String title, final String uuid, 
			final PackageType packageType, final LocalDateTime creationDate, final LocalDateTime lastModifiedDate, 
			final SerializationFormat serializationFormat) {
		return createInformationPackageExchange(title, uuid, packageType, creationDate, lastModifiedDate, serializationFormat, 0);
	}
	
	/**
	 * Creates an empty collection for exchange objects for information packages.
	 * @return The empty collection.
	 */
	public static InformationPackageExchangeCollection createEmptyInformationPackageExchangeCollection() {
		return new InformationPackageExchangeCollectionListImpl();
	}
	
	/**
	 * Creates an exchange object that contains the data of a digital object.
	 * @param filePathInStorage The path of the digital object in the storage directory of the server. This is not the absolute path
	 * of the file but only the file name with eventually subdirectories within the storage directory.
	 * @param uuidOfDigitalObject The uuid of this digital object.
	 * @param md5Checksum The md5 checksum of the file the digital object is related to. 
	 * @return The newly created exchange object.
	 */
	public static final DigitalObjectExchange createDigitalObjectExchange(final String filePathInStorage, final Uuid uuidOfDigitalObject,
			final String md5Checksum) {
		return new DigitalObjectExchangeImpl(filePathInStorage, uuidOfDigitalObject, md5Checksum);
	}
	
	/**
	 * Creates a new exchange object for a metadata. 
	 * @param metadataName The name of the metadata. May not be empty.
	 * @param metadataValue The value of the metadata. May be empty.
	 * @return The newly created object.
	 */
	public static MetadataExchange createMetadataExchange(final String metadataName, final String metadataValue) {
		return new MetadataExchangeImpl(metadataName, metadataValue);
	}
	
	/**
	 * Creates an empty collection with exchange object for metadata.
	 * @param uuidOfFile The uuid of the file these metadata are assigned to.  
	* @return The empty collection.
	 */
	public static MetadataExchangeCollection createMetadataExchangeCollection(final String uuidOfFile) {
		return new MetadataExchangeCollectionImpl(uuidOfFile);
	}
}
