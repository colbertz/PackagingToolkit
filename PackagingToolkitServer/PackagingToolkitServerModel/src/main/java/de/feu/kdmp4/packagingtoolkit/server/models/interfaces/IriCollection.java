package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

public interface IriCollection {
	/**
	 * Adds a iri to the collection.
	 * @param iri The iri that should be added.
	 */
	void addIri(final Iri iri);
	/**
	 * Gets a iri at a given index.
	 * @param index The index we find the element at.
	 * @return The found iri.
	 */
	Iri getIri(final int index);
	/**
	 * Return the number of iris in the collection.
	 * @return The number of iris.
	 */
	int getIrisCount();
	/**
	 * Determines if the collection is empty.
	 * @return True if the collection is empty, false otherwise.
	 */
	boolean isEmpty();
	/**
	 * Determines if the collection is not empty.
	 * @return True if the collection is not empty, false otherwise.
	 */
	boolean isNotEmpty();
	/**
	 * Checks if the collection contains a given iri. 
	 * @param iri The iri we want to check.
	 * @return True if the collection contains the iri, false otherwise.
	 */
	boolean doesNotContainIri(Iri iri);
	/**
	 * Checks if the collection does not contain a given iri. 
	 * @param iri The iri we want to check.
	 * @return True if the collection does not contain the iri, false otherwise.
	 */
	boolean containsIri(Iri iri);
}
