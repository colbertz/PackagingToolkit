package de.feu.kdmp4.packagingtoolkit.server.models.exceptions;

import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;
import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class ArchiveException extends PackagingToolkitException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9105369727995545817L;
	
	private ArchiveException(final Uuid uuid,
							 final String theMessage, 
							 final String theClass, 
							 final String theMethod) {
		super(theMessage, theClass, theMethod, PackagingToolkitComponent.SERVER);
	}

	public static ArchiveException archiveMayNotBeNullException(final String theClass, 
			final String theMethod) {
		String message = I18nExceptionUtil.getArchiveMayNotBeNullString();
		return new ArchiveException(null, message, theClass, theMethod);
	}

	public static ArchiveException uuidMayNotBeNullException(final String theClass, 
			final String theMethod) {
		String message = I18nExceptionUtil.getUuidMayNotBeEmptyString();
		return new ArchiveException(null, message, theClass, theMethod);
	}
	
	
	/**
	 * Returns the additional information of this exception.
	 * @return The path of the invalid working directory.
	 */
	@Override
	public String getAdditionalInformation() {
		if (additionalInformation != null) {
			return (String)additionalInformation;
		} else {
			return StringUtils.EMPTY_STRING;
		}
	}

	@Override
	public String getMessage() {
		String message = super.getMessage();
		return String.format(message, getAdditionalInformation());
	}
}