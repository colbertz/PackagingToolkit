package de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces;

import java.util.Optional;

/**
 * Contains some exchange objects for metadata.
 * @author Christopher Olbertz
 *
 */
public interface MetadataExchangeCollection {
	/**
	 * Adds a new metadata to the list. An object is created in this method. 
	 * @param metadataName The name of the new metadata.
	 * @param metadataValue The value of the new metadata.
	 */
	void addMetadata(final String metadataName, final String metadataValue);
	/**
	 * Finds a metadata in the collection by its name. 
	 * @param metadataName The name of the metadata we are looking for. 
	 * @return The found metadata or an empty optional if the metadata is not contained in the
	 * collection.
	 */
	Optional<MetadataExchange> getMetadataByName(final String metadataName);
	/**
	 * Determins the uuid of the file these metadata are assigned to. 
	 * @return The uuid of the file. 
	 */
	String getUuidOfFile();
	/**
	 * Counts the metadata in the collection.
	 * @return The number of metadata in the collection. 
	 */
	int getMetadataCount();
	/**
	 * Determines the element at a given position.
	 * @param index The position we are looking at. 
	 * @return The found metadata.
	 */
	MetadataExchange getMetadataAt(int index);
}
