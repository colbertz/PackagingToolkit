package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface Reference {
	String getUrl();
	Uuid getUuidOfFile();
	/**
	 * Determines if the reference is a reference to a remote file.
	 * @return True if the reference is a reference to a remote file, false otherwise.
	 */
	boolean isRemoteFileReference();
	/**
	 * Determines if the reference is a reference to a local file.
	 * @return True if the reference is a reference to a local file, false otherwise.
	 */
	boolean isLocalFileReference();
	/**
	 * Creates a statement for the triple store of the information of this reference. The statement
	 * says that the reference is referenced by a specific information package. 
	 * @param uuidOfInformationPackage The uuid of the information package that is using this reference.
	 * @return The triple statement created.
	 */
	TripleStatement asTripleStatement(final Uuid uuidOfInformationPackage);
}
