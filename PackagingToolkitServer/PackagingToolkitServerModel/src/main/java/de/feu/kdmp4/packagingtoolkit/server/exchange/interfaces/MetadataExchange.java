package de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces;

/**
 * Is used for exchanging the data of metadata over the borders of the modules, because we do not want to
 * use entity object in another module than the module Database.
 * @author Christopher Olbertz
 *
 */
public interface MetadataExchange {
	String getMetadataName();
	String getMetadataValue();
}
