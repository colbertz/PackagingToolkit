package de.feu.kdmp4.packagingtoolkit.server.exchange.classes;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;

public class InformationPackageExchangeImpl implements InformationPackageExchange {
	/**
	 * The value of the primary key in the database.
	 */
	private long informationPackageId;
	/**
	 * The uuid of the information package.
	 */
	private String uuid;
	/**
	 * The title of the information package.
	 */
	private String title;
	/**
	 * The package type. At the moment only submission information package (SIP) is supported.
	 */
	private PackageType packageType;
	/**
	 * The date the information package was created at.
	 */
	private LocalDateTime creationDate;
	/**
	 * The date the information package was modified the last time at.
	 */
	private LocalDateTime lastModifiedDate;
	/**
	 * The format the manifest file of the information package is serialized in. At the moment only OAI-ORE is supported.
	 */
	private SerializationFormat serializationFormat;
	/**
	 * The uuids of the digital object this information packaging is referencing to.
	 */
	private StringList uuidsOfDigitalObjects;
	
	/**
	 * Initializes the information packages with an empty list of digital objects.
	 */
	public InformationPackageExchangeImpl() {
		uuidsOfDigitalObjects = PackagingToolkitModelFactory.getStringList();
	}
	
	@Override
	public void addUuidOfFile(final String uuidOfFile) {
		uuidsOfDigitalObjects.addStringToList(uuidOfFile);
	}
	
	@Override
	public int getDigitalObjectCount() {
		return uuidsOfDigitalObjects.getSize();
	}
	
	@Override
	public String getUuidAt(int index) {
		return uuidsOfDigitalObjects.getStringAt(index);
	}
	
	@Override
	public long getInformationPackageId() {
		return informationPackageId;
	}
	
	@Override
	public void setInformationPackageId(final long informationPackageId) {
		this.informationPackageId = informationPackageId;
	}
	
	@Override
	public String getUuid() {
		return uuid;
	}
	
	@Override
	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public void setTitle(final String title) {
		this.title = title;
	}
	
	@Override
	public PackageType getPackageType() {
		return packageType;
	}
	
	@Override
	public void setPackageType(final PackageType packageType) {
		this.packageType = packageType;
	}
	
	@Override
	public LocalDateTime getCreationDate() {
		return creationDate;
	}
	
	@Override
	public void setCreationDate(final LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}
	
	@Override
	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}
	
	@Override
	public void setLastModifiedDate(final LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	@Override
	public void setSerializationFormat(final SerializationFormat serializationFormat) {
		this.serializationFormat = serializationFormat;
	}
	
	@Override
	public SerializationFormat getSerializationFormat() {
		return serializationFormat;
	}

	@Override
	public boolean isSubmissionInformationPackage() {
		if (packageType.equals(PackageType.SIP)) {
			return true;
		} else {
			return false;
		}
	}
}
