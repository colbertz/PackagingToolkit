package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.utils.RandomUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class IriImpl implements Iri, Comparable<Iri> {
	/**
	 * The namespace of this iri.
	 */
	private Namespace namespace;
	/**
	 * The local name of this iri.
	 */
	private LocalName localName;

	/**
	 * Creates a new iri object. If the parameter contains a # symbol it is considered as a 
	 * valid iri and it is split into a namespace and a local name. If the parameter does not
	 * contain a # symbol, the parameter is used as local name and the default namespace of
	 * the PackagingToolkit is used.
	 * @param iri Contains a valid iri or only a local name.
	 */
	public IriImpl(String iri) {
		final String namespaceAsString = StringUtils.extractNamespaceFromIri(iri);
		final String localnameAsString = StringUtils.extractLocalNameFromIri(iri); 
		
		if (StringValidator.isNullOrEmpty(iri)) {
			final long randomLocalName = RandomUtils.nextLong();
			this.namespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
			final String randomLocalNameAsString = String.valueOf(randomLocalName);
			this.localName = ServerModelFactory.createLocalName(randomLocalNameAsString);
		} else if (iri.contains("://")) {
			this.namespace = ServerModelFactory.createNamespace(namespaceAsString);
			this.localName = ServerModelFactory.createLocalName(localnameAsString);
		} else if (StringUtils.areStringsEqual(namespaceAsString, iri)) {
			this.namespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
			this.localName = ServerModelFactory.createLocalName(iri);
		} else {
			this.namespace = ServerModelFactory.createNamespace(namespaceAsString);
			this.localName = ServerModelFactory.createLocalName(localnameAsString);
		}
	}
	
	/**
	 * Creates a new iri object with a namespace and a local name.
	 * @param namespace The namespace of this iri.
	 * @param localName The local name of this iri.
	 */
	public IriImpl(final Namespace namespace, final LocalName localName) {
		this.namespace = namespace;
		this.localName = localName;
	}
	
	/**
	 * Creates an iri with an uuid as local name. As namespace the default namespace of the
	 * PackagingToolkit is used.
	 * @param uuid The uuid that should be consideres as local name.
	 */
	public IriImpl(final Uuid uuid) {
		this(uuid.toString());
	}
	
	@Override
	public Namespace getNamespace() {
		return namespace;
	}
	
	@Override
	public LocalName getLocalName() {
		return localName;
	}
	
	@Override
	public String toString() {
		if (namespace != null && localName == null) {
			return namespace.toString();
		} else {
			return namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localName;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localName == null) ? 0 : localName.hashCode());
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IriImpl other = (IriImpl) obj;
		if (localName == null) {
			if (other.localName != null)
				return false;
		} else if (!localName.equals(other.localName))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		return true;
	}

	@Override
	public int compareTo(final Iri otherIri) {
		final String iriAsString = this.toString();
		final String theOtherIriAsString = otherIri.toString();
		return iriAsString.compareTo(theOtherIriAsString);
	}
}
