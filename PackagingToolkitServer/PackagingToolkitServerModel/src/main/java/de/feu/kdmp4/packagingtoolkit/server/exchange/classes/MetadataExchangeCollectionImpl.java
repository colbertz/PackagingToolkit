package de.feu.kdmp4.packagingtoolkit.server.exchange.classes;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.factories.ExchangeModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;

public class MetadataExchangeCollectionImpl  extends AbstractList implements MetadataExchangeCollection {
	/**
	 * The uuid of the file these metadata are assigned to. 
	 */
	private String uuidOfFile;
	
	/**
	 * Constructs a new object with an array list.
	 */
	public MetadataExchangeCollectionImpl(final String uuidOfFile) {
		this.uuidOfFile = uuidOfFile;
	}
	
	@Override
	public void addMetadata(final String metadataName, final String metadataValue) {
		MetadataExchange metadata = ExchangeModelFactory.createMetadataExchange(metadataName, metadataValue);
		super.add(metadata);
	}
	
	@Override
	public Optional<MetadataExchange> getMetadataByName(final String metadataName) {
		for (int i  = 0; i < super.getSize(); i++) {
			final MetadataExchange metadataExchange = getMetadataAt(i);
			final String theMetadataname = metadataExchange.getMetadataName();
			
			if (areStringsEqual(metadataName, theMetadataname)) {
				return ServerOptionalFactory.createOptionalWithUuid(metadataExchange);
			}
		}
		
		return ServerOptionalFactory.createEmptyOptionalWithMetadataExchange();
	}
	
	@Override
	public MetadataExchange getMetadataAt(final int index) {
		return (MetadataExchange)super.getElement(index);
	}
	
	@Override
	public String getUuidOfFile() {
		return uuidOfFile;
	}

	@Override
	public int getMetadataCount() {
		return super.getSize();
	}
}
