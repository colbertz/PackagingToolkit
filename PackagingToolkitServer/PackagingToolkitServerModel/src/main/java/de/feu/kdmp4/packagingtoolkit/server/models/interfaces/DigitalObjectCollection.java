package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

/**
 * Contains some digital objects.
 * @author Christopher Olbertz
 *
 */
public interface DigitalObjectCollection {
	/**
	 * Adds a new digital object to the collection.
	 * @param digitalObject The digital object we want add to the collection.
	 */
	public void addDigitalObject(final DigitalObject digitalObject);
	/**
	 * Determines the number of the digital objects in the collection.
	 * @return The number of the digital objects in the collection.
	 */
	int getDigitalObjectCount();
	/**
	 * Returns a digital object at a given index.
	 * @param index The index of the digital object we are interested in.
	 * @return The digital index found at index.
	 */
	DigitalObject getDigitalObjectAt(final int index);
	/**
	 * Deletes a digital object from the collection.
	 * @param digitalObject The digital object we want to delete.
	 */
	void deleteDigitalObject(final DigitalObject digitalObject);
	/**
	 * Determines if the collection is empty.
	 * @return True if the collection does not contain any digital objects, false otherwise.
	 */
	boolean isEmpty();
}
