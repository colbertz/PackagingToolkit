package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitFileException;
import de.feu.kdmp4.packagingtoolkit.models.classes.OaisEntityImpl;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class DigitalObjectImpl extends OaisEntityImpl implements DigitalObject {
	/**
	 * The file name of the digital object in the storage of the server.
	 */
	private String filename;
	/**
	 * A referencing that is pointing to this digital object.
	 */
	private Reference reference;
	/**
	 * The md5 checksum of the file.
	 */
	private String md5Checksum;
	
	/**
	 * Constructs a new digital object with a random uuid  and initializes the reference object.
	 * @param filename The file name of the digital object in the storage of the server.
	 * @param md5Checksum The md5 checksum of the file.
	 */
	public DigitalObjectImpl(String filename, String md5Checksum) {
		this(null, filename, md5Checksum);
	}
	
	/**
	 * Comstructs a new digital object and initializes the reference object.
	 * @param uuid The uuid of the digital object. It is saved in the database too and used for unique idenfication.
	 * @param filename  The file name of the digital object in the storage of the server.
	 * @param md5Checksum The md5 checksum of the file.
	 */
	public DigitalObjectImpl(Uuid uuid, String filename, String md5Checksum) {
		super(uuid);
		if (!StringValidator.isNotNullOrEmpty(filename)) {
			PackagingToolkitFileException exception = PackagingToolkitFileException.
					filenameMayNotBeEmptyException();
			throw exception;
		}
		
		this.filename = filename;
		this.md5Checksum = md5Checksum;
		reference = ServerModelFactory.createLocalFileReference(filename, uuid);
	}

	@Override
	public int compareTo(DigitalObject digitalObject) {
		return filename.compareTo(digitalObject.getFilename());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitalObjectImpl other = (DigitalObjectImpl) obj;
		final Uuid uuid = getUuid();
		final Uuid otherUuid = other.getUuid();
		if (!uuid.equals(otherUuid)) {
			return false;
		} else	if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename)) {
			return false; 
		} else if (!md5Checksum.equals(other.getMd5Checksum())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return filename;
	}
	
	@Override
	public String getFilename() {
		return filename;
	}
	
	@Override
	public Reference getReference() {
		return reference;
	}
	
	@Override
	public String getMd5Checksum() {
		return md5Checksum;
	}
}
