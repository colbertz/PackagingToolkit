/**
 * Contains the implementating classes for some datastructures.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.datastructures.classes;