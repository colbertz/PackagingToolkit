package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import java.io.Serializable;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguageCollection;

public class TextInLanguageCollectionImpl extends AbstractList implements TextInLanguageCollection, Serializable {
	private static final long serialVersionUID = -2774754858876620076L;

	@Override
	public void addTextInLanguage(final TextInLanguage textInLanguage) {
		super.add(textInLanguage);
	}
	
	@Override
	public void addTextInLanguage(final String text, final OntologyLabelLanguage language) {
		final TextInLanguage textInLanguage = ServerModelFactory.createTextInLanguage(text, language);
		super.add(textInLanguage);
	}
	
	@Override
	public TextInLanguage getTextInLanguage(final int index) {
		return (TextInLanguage)super.getElement(index);
	}
	
	@Override
	public Optional<TextInLanguage> getTextByLanguage(final OntologyLabelLanguage language) {
		int i = 0;
		boolean found = false;
		TextInLanguage foundTextInLanguage = null;
		
		while (i < getSize() && found == false) {
			final TextInLanguage textInLanguage = getTextInLanguage(i);
			final OntologyLabelLanguage otherLanguage = textInLanguage.getLanguage();
			
			if (otherLanguage.equals(language)) {
				found = true;
				foundTextInLanguage = textInLanguage;
			}
			i++;
		}
		
		if (foundTextInLanguage == null) {
			return ServerOptionalFactory.createEmptyOptionalWithTextInLanguage();
		} else {
			return ServerOptionalFactory.createOptionalWithTextInLanguage(foundTextInLanguage);
		}
	}
	
	@Override
	public int getTextsInLanguageCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		if (getTextsInLanguageCount() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
