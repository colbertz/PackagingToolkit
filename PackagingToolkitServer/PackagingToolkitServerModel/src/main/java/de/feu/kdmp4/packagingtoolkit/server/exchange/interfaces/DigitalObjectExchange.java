package de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Is used for exchanging the data of digital objects over the borders of the modules, because we do not want to
 * use entity object in another module than the module Database.
 * @author Christopher Olbertz
 *
 */
public interface DigitalObjectExchange {
	/**
	 * Returns the path of the file in the storage directory of the server.
	 * @return The path of the file in the storage directory of the server.
	 */
	String getFilePathInStorage();
	/**
	 * Returns the uuid of the digital object.
	 * @return The uuid of the digital object.
	 */
	Uuid getUuidOfDigitalObject();
	/**
	 * Returns the md5 checksum of the file the digital object is referencing to. 
	 * @return The md5 checksum of the file the digital object is referencing to. 
	 */
	String getMd5Checksum();
}
