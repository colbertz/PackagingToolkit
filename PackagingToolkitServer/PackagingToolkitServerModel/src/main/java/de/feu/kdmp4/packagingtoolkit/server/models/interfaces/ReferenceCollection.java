package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

/**
 * A collection that contains references.
 * @author Christopher Olbertz
 *
 */
public interface ReferenceCollection {
	/**
	 * Adds a reference to the collection.
	 * @param reference The reference that should be added.
	 */
	void addReference(final Reference reference);
	/**
	 * Gets a reference at a given index.
	 * @param index The index we find the element at.
	 * @return The found reference.
	 */
	Reference getReference(final int index);
	/**
	 * Return the number of references in the collection.
	 * @return The number of references.
	 */
	int getReferencesCount();
	/**
	 * Determines if the collection is empty.
	 * @return True if the collection is empty, false otherwise.
	 */
	boolean isEmpty();
	/**
	 * Determines if the collection is not empty.
	 * @return True if the collection is not empty, false otherwise.
	 */
	boolean isNotEmpty();
}
