package de.feu.kdmp4.packagingtoolkit.server.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;

/**
 * Encapsulates the most frequent statements for logging. Allows that the other classes
 * do not need code for logging.
 * @author Christopher Olbertz
 *
 */
public class ServerLogUtil {
	/**
	 * The object that can be used for logging.
	 */
	private static Log log = LogFactory.getLog(ServerLogUtil.class);
	
	/**
	 * Private constructor because we don't want to create any objects of this class.
	 */
	private ServerLogUtil() { }
	
	/**
	 * Writes an info message if an information package has been created and not yet been put in the session.
	 * @param title The title of the information package.
	 * @param uuid The uuid of the information package.
	 */
	public static final void logInfoInformationPackageCreated(final String title,final  Uuid uuid) {
		log.info("Information package created with title \"" + title + "\" and Uuid \"" + uuid.toString() + "\"");
	}
	
	/**
	 * Logs an info message if an information package has been put in the session.
	 * @param uuid The uuid of the information package.
	 */
	public static void logInfoInformationPackagePutInSession(final Uuid uuid) {
		log.info("Information package with Uuid \"" + uuid.toString() + "\" has been put in session");
	}
	
	/**
	 * Logs an info message with the number of information packages in the session.
	 * @param informationPackageCount The number of information packages in the session.
	 */
	public static void logInfoSessionState(final int informationPackageCount) {
		log.info(informationPackageCount + " information packages are contained in the session.");
	}
	
	/**
	 * Logs an info message about an information package in the session.
	 * @param uuid The uuid of the information package.
	 */
	public static void logInfoInformationPackageInSession(final Uuid uuid) {
		log.info("Uuid of information package in the session: \"" + uuid.toString() + "\"");
	}
	
	/**
	 * Logs an info message if a reference has been added to an information package. The message contains
	 * the uuid of the information package, the type of the reference (local / remote) and the url of the reference.
	 * @param uuidOfInformationPackage The uuid of the information package the reference is added to.
	 * @param reference The reference hat has been added to the information package.
	 */
	public static void logInfoReferenceAdded(final Uuid uuidOfInformationPackage, final Reference reference) {
		log.info("A reference has been added to the information package with the uuid \"" + uuidOfInformationPackage + "\"");
		if (reference.isLocalFileReference()) {
			log.info("Reference points to local file: " + reference.getUrl());
		} else {
			log.info("Reference points to remote file: " + reference.getUrl());
		}
	}
}
