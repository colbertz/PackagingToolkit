package de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;

/**
 * Contains the data of an information package for exchanging between modules.
 * @author Christopher Olbertz
 *
 */
public interface InformationPackageExchange {
	/**
	 * Adds a new uuid of a file to this information package.
	 * @param uuidOfFile The uuid of a file the information package is referencing to .
	 */
	void addUuidOfFile(final String uuidOfFile);
	/**
	 * Gets the id of the information package. This is the primary key in the database.
	 * @return The id of the information package. 
	 */
	long getInformationPackageId();
	/**
	 * Sets the id of the information package. This is the primary key and this method may only be used
	 * if there is already an object in the database because the database framework administrates new
	 * primary key values by itself.
	 * @param informationPackageId The id of the information package. 
	 */
	void setInformationPackageId(final long informationPackageId);
	/**
	 * Gets the uuid of this information package that is unique for every information package.
	 * @return The uuid of this information package.
	 */
	String getUuid();
	/**
	 * Sets the uuid of this information package that is unique for every information package.
	 * @param uuid The uuid of this information package.
	 */
	void setUuid(final String uuid);
	/**
	 * Returns the title of the information package.
	 * @return The title of the information package.
	 */
	String getTitle();
	/**
	 * Sets the title of the information package.
	 * @param title The title of the information package.
	 */
	void setTitle(final String title);
	/**
	 * Returns the package type of the information package. At the moment only the package type SIP 
	 * (Submission Information Package) is supported.
	 * @return The package type of the information package.
	 */
	PackageType getPackageType();
	/**
	 * Sets the package type of the information package. At the moment only the package type SIP 
	 * (Submission Information Package) is supported.
	 * @param packageType The package type of the information package.
	 */
	void setPackageType(final PackageType packageType);
	/**
	 * Returns the time the information package has been created. 
	 * @return The time the information package has been created.
	 */
	LocalDateTime getCreationDate();
	/**
	 * Returns the time the information package has been modified the last time. 
	 * @return The time the information package has been modified the last time.
	 */
	LocalDateTime getLastModifiedDate();
	/**
	 * Sets the time the information package has been modified the last time. 
	 * @param lastModifiedDate The time the information package has been modified the last time.
	 */
	void setLastModifiedDate(final LocalDateTime lastModifiedDate);
	/**
	 * Sets the time the information package has been created. 
	 * @param creationDate The time the information package has been created.
	 */
	void setCreationDate(final LocalDateTime creationDate);
	/**
	 * Sets the serialization format of the information package. At the Moment only OAI-ORE is 
	 * supported as serialization format. 
	 * @param serializationFormat The serialization format of the information package.
	 */
	void setSerializationFormat(final SerializationFormat serializationFormat);
	/**
	 * Gets the serialization format of the information package. At the Moment only OAI-ORE is 
	 * supported as serialization format. 
	 * @return The serialization format of the information package.
	 */
	SerializationFormat getSerializationFormat();
	/**
	 * Determines the number of digital objects the information package is referencing to. Because only SIPs are supported
	 * at the moment, the return value can either be 0 or 1.
	 * @return The number of digital objects the information package is referencing to.
	 */
	int getDigitalObjectCount();
	/**
	 * Determines the uuid of a file at a given index.
	 * @param index The index of the uuid we are interested in.
	 * @return The uuid of the file at index. 
	 */
	String getUuidAt(final int index);
	/**
	 * Determines if the information package is a submission information package. Because only SIPs are supported
	 * at the moment, this method will always return true.
	 * @return True, if the information package is a SIP, false otherwise.
	 */
	boolean isSubmissionInformationPackage();
}
