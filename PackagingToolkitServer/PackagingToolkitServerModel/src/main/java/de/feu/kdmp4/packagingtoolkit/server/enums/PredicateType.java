package de.feu.kdmp4.packagingtoolkit.server.enums;

/**
 * This enum describes the type of an prediacte. Is necessary for  determining the function
 * of a predicate. A predicate can represent a datatype property, an object property, a type-relation
 * etc. This enum offers the possibility to differ these different types of predicates.  It is important
 * for the TripleObject to know which type of predicate it is related to. 
 * @author Christopher Olbertz
 *
 */
public enum PredicateType {
	AGGREGATED_BY, LOCAL_FILE_REFERENCE, HAS_CREATING_APPLICATION, HAS_DATATYPE, HAS_FIXITY, LITERAL, OBJECT_PROPERTY, 
	REMOTE_FILE_REFERENCE, TYPE, TAXONOMY_INDIVIDUAL_SELECTED;
}
