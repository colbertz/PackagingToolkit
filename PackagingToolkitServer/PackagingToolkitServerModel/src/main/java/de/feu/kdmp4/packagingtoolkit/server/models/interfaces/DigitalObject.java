package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.OaisEntity;

/**
 * Represents a digital object that is stored on the server and referenced by an information package. For 
 * every file and every reference in an information package, there is one DigitalObject object.
 * @author Christopher Olbertz
 *
 */
public interface DigitalObject extends OaisEntity, Comparable<DigitalObject> {
	/**
	 * Returns the filename of this digital object.
	 * @return The filename of this digital object.
	 */
	String getFilename();
	/**
	 * Returns the reference saved in this digital object. 
	 * @return The reference saved in this digital object.
	 */
	Reference getReference();
	/**
	 * Returns the md5 checksum of the file the digital object is referencing to.
	 * @return The md5 checksum of the file the digital object is referencing to.
	 */
	String getMd5Checksum();
}