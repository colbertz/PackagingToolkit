package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Contains the information of a RDF subject for being saved in an information package and in
 * the triple store. 
 * @author Christopher Olbertz
 *
 */
public interface TripleSubject {
	/**
	 * Gets the local name of the iri of the subject. The local name is the part after the # symbol.
	 * @return The local name of the iri of the subject.
	 */
	LocalName getLocalName();
	/**
	 * Gets the namespace of the iri of the subject. The namespace is the part before the # symbol.
	 * @return The namespace of the iri of the subject.
	 */
	Namespace getNamespace();
	/**
	 * Gets the uuid of the subject.
	 * @return The uuid of the subject.
	 */
	Uuid getUuid();
	/**
	 * Gets the iri of the subject. The iri consists of a namespace and a local name separated by a # symbol.
	 * @return The iri of the subject
	 */
	Iri getIri();
}
