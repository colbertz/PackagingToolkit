package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import java.io.Serializable;
import java.util.Collection;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;

public class IriCollectionImpl extends AbstractList  implements IriCollection, Serializable {
	private static final long serialVersionUID = -2774754858876620076L;
	
	public IriCollectionImpl() {
	}
	
	public IriCollectionImpl(final Collection<Iri> iriCollection) {
		for (Iri iri: iriCollection) {
			addIri(iri);
		}
	}
	
	@Override
	public boolean containsIri(final Iri iri) {
		return super.contains(iri);
	}
	
	@Override
	public boolean doesNotContainIri(final Iri iri) {
		return !super.contains(iri);
	}
	
	@Override
	public void addIri(final Iri iri) {
		super.add(iri);

	}
	
	@Override
	public Iri getIri(final int index) {
		return (Iri)super.getElement(index);
	}
	
	@Override
	public int getIrisCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		if (getIrisCount() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isNotEmpty() {
		if (getIrisCount() != 0) {
			return true;
		} else {
			return false;
		}
	}
}
