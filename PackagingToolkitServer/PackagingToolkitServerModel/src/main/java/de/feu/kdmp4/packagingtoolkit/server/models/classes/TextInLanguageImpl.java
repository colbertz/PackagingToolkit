package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguageCollection;

public class TextInLanguageImpl implements TextInLanguage {
	/**
	 * The text of the label.
	 */
	private String labelText;
	/**
	 * The language the text is written in.
	 */
	private OntologyLabelLanguage language;
	
	/**
	 * Creates an object with a text and the language the
	 * text is written in.
	 * @param labelText The text.
	 * @param language The language the text is written in.
	 */
	public TextInLanguageImpl(final String labelText, final OntologyLabelLanguage language) {
		super();
		this.labelText = labelText;
		this.language = language;
	}
	
	/**
	 * Creates an object with a text. There are no information
	 * about the language indicated and therefore a default 
	 * language can be used.
	 * @param labelText The text.
	 * @param language The language the text is written in.
	 */
	public TextInLanguageImpl(final String labelText) {
		this(labelText, null);
	}
	
	@Override
	public String getLabelText() {
		return labelText;
	}
	
	@Override
	public void setLabelText(final String labelText) {
		this.labelText = labelText;
	}
	
	@Override
	public OntologyLabelLanguage getLanguage() {
		return language;
	}
	
	@Override
	public void setLanguage(final OntologyLabelLanguage language) {
		this.language = language;
	}
}
