package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

public interface TripleStatementCollection {

	void addTripleStatement(TripleStatement tripleStatement);

	TripleStatement getTripleStatement(int index);

	int getTripleStatementsCount();
	void addStatements(TripleStatementCollection statementList);

	boolean hasNextNotLiteralStatement();
	TripleStatement getNextNotLiteralStatement();
	
	boolean hasNextLiteralStatement();

	TripleStatement getNextLiteralStatement();
	/**
	 * Determines the next statement in the list whose subject is a given individual identified by
	 * its iri. If there is already a used iterator that looks for individuals with the iri a and
	 * now the iterator should look for individuals with the iri b, a new iterator for the iri b 
	 * is created. It overrides the old iterator. There can only be one iterator at one time.
	 * @param iriOfIndividual The iri of the individual to look for.
	 * @return The next statement with the individual as subject that is found in the collection.
	 */
	TripleStatement getNextStatementOfIndividual(String iriOfIndividual);
	/**
	 * Determines the next statement in the list whose subject is a given individual identified by
	 * its iri. If there is already a used iterator that looks for individuals with the iri a and
	 * now the iterator should look for individuals with the iri b, a new iterator for the iri b 
	 * is created. It overrides the old iterator. There can only be one iterator at one time.
	 * @param iriOfIndividual The iri of the individual to look for.
	 * @return The next statement with the individual as subject that is found in the collection.
	 */
	boolean hasNextStatementOfIndividual(String iriOfIndividual);
	/**
	 * Sorts the statements by the iri of the subject.
	 * @return The sorted statements.
	 */
	TripleStatementCollection sortBySubject();
	/**
	 * Checks if there is another statement in the list that describes a reference to a file.
	 * @return True if there is another statement that describes a reference to a file, false otherwise.
	 */
	boolean hasNextReferenceStatement();
	/**
	 * Returns the next statement in the collection that describes a reference to a file.
	 * @return The next statement in the collection that describes a reference to a file.
	 */
	TripleStatement getNextReferenceStatement();
	/**
	 * Finds all statements in this collection that are identifying an individual. These are the statements
	 * with an aggregatedBy predicate. 
	 * @return
	 */
	TripleStatementCollection getIndividualStatements();
	/**
	 * Searches for a statement in this collection.
	 * @param tripleStatement The statement we are looking for.
	 * @return True, if the collection contains the statement, false otherwise. 
	 */
	boolean containsTripleStatement(TripleStatement tripleStatement);
	/**
	 * Determines the datatype of a property.
	 * @param iriOfProperty The iri of the property whose datatype should be determined.
	 * @return The datatype of the property.
	 */
	//OntologyDatatype getDatatypeOfProperty(String iriOfProperty);
	/**
	 * Removes a statement from the collection.
	 * @param tripleStatement The statement that should be removed. 
	 */
	void removeTripleStatement(TripleStatement tripleStatement);
}
