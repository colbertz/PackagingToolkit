package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;

public class TripleSubjectImpl implements TripleSubject {
	/**
	 * Gets the uuid of the subject.
	 */
	private Uuid uuid;
	/**
	 * The iri of the subject. It consists of a namespace and a local name, separated by a # symbol.
	 */
	private Iri iri;

	public TripleSubjectImpl(final String iri) {
		this.iri = ServerModelFactory.createIri(iri);
	}
	
	/**
	 * If the subject has not an explizit namespace and local name, the namespace for the PackagingToolkit is used
	 * and the uuid is used as local name. 
	 * @param uuid The uuid of the subject that is used as local name.
	 */
	public TripleSubjectImpl(final Uuid uuid) {
		this.iri = ServerModelFactory.createIri(uuid);
	}
	
	public TripleSubjectImpl(final Uuid uuid, final Namespace namespace, final LocalName localName) {
		super();
		this.uuid = uuid;
		this.iri = ServerModelFactory.createIri(namespace, localName);
	}

	@Override
	public Uuid getUuid() {
		return uuid;
	}
	
	
	@Override
	public String toString() {
		return iri.toString();
	}

	@Override
	public Iri getIri() {
		return iri;
	}

	@Override
	public LocalName getLocalName() {
		return iri.getLocalName();
	}

	@Override
	public Namespace getNamespace() {
		return iri.getNamespace();
	}
}
