package de.feu.kdmp4.packagingtoolkit.server.factories;

import java.util.Collection;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.classes.MetadataMappingHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.DigitalObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.ReferenceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.ReferenceCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TextInLanguageImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TextInLanguageCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguageCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;

/**
 * Creates objects of the model classes of the server. So the implementations are decoupled from their interfaces and
 * and the whole application interfaces can be references and not implementations.
 * @author Christopher Olbertz
 *
 */
public abstract class ServerModelFactory {
	/**
	 * Creates a digital object.
	 * @param uuid The uuid of the file as saved in the database.
	 * @param filename The name of the file in the storage directory of the server.
	 * @param md5Checksum The md5 checksum of the file.
	 * @return The newly created digital object.
	 */
	public static final DigitalObject createDigitalObject(final Uuid uuid, final String filename, final String md5Checksum) {
		final DigitalObject digitalObject = new DigitalObjectImpl(uuid, filename, md5Checksum);
		
		return digitalObject;
	}
	
	/**
	 * Creates an empty object that implements MetadataMappingMap. 
	 * @return The newly created object.
	 */
	public static final MetadataMappingMap createEmptyMetadataMappingMap() {
		return new MetadataMappingHashMapImpl();
	}
	
	/**
	 * Creates a reference object to a local file in the storage.
	 * @param url The path url of the file.
	 * @param uuidOfFile The uuid of the file as saved in the database.
	 * @return The reference.
	 */
	public static final Reference createLocalFileReference(final String url, final Uuid uuidOfFile) {
		return ReferenceImpl.createLocalFileReference(url, uuidOfFile);
	}
	
	/**
	 * Creates a reference object to a remote file that is saved on another server.
	 * @param url The url of the file.
	 * @return The reference.
	 */
	public static final Reference createRemoteReference(final String url) {
		return ReferenceImpl.createRemoteFileReference(url);
	}

	/**
	 * Creates a list with references that contains the values in a given collection.
	 * @param referenceCollection Contains the collection that the created list should contain.
	 * @return A list with the values of referenceCollection.
	 */
	public static final ReferenceCollection createReferenceList(final Collection<Reference> referenceCollection) {
		return new ReferenceCollectionImpl(referenceCollection);
	}
	
	/**
	 * Creates an empty collection for iris.
	 * @return An empty collection for iris.
	 */
	public static final IriCollection createEmptyIriCollection() {
		return new IriCollectionImpl();
	}
	
	/**
	 * Creates an object that contains text in the default language of the application.
	 * @param labelText The text the object should contain.
	 * @return The newly created object.
	 */
	public static final TextInLanguage createTextInLanguage(final String labelText) {
		return new TextInLanguageImpl(labelText);
	}
	
	/**
	 * Creates an object that contains text in a specific language.
	 * @param labelText The text the object should contain.
	 * @param language The language labelText is written in.
	 * @return The newly created object.
	 */
	public static final TextInLanguage createTextInLanguage(final String labelText,
			final OntologyLabelLanguage language) {
		return new TextInLanguageImpl(labelText, language);
	}
	
	/**
	 * Creates an empty collection with texts in a specific language.
	 * @return The empty collection.
	 */
	public static final TextInLanguageCollection createEmptyTextInLanguageCollection() {
		return new TextInLanguageCollectionImpl();
	}
	
	/**
	 * Creates an object for the working directory of the server.
	 * @param workingDirectoryPath The path of the working directory on the server.
	 * @return The newly created object.
	 */
	public static final WorkingDirectory createWorkingDirectory(final String workingDirectoryPath) {
		return new WorkingDirectoryImpl(workingDirectoryPath);
	}
	
	/**
	 * Creates an uuid.
	 * @param uuidAsString The value of the uuid as string.
	 * @return The newly created uuid.
	 */
	public static final Uuid createUuid(final String uuidAsString) {
		return new Uuid(uuidAsString);
	}
	
	/**
	 * Creates an uuid with a random value.
	 * @return The newly created uuid.
	 */
	public static final Uuid createUuid() {
		return new Uuid();
	}
	
	/**
	 * Creates a namespace.
	 * @param The string that is encapsulated as namespace in this object.
	 * @return The newly created namespace.
	 */
	public static final Namespace createNamespace(final String namespace) {
		return new NamespaceImpl(namespace);
	}
	
	/**
	 * Creates a local name.
	 * @param The string that is encapsulated as local name in this object.
	 * @return The newly created local name.
	 */
	public static final LocalName createLocalName(final String localName) {
		return new LocalNameImpl(localName);
	}
	
	/**
	 * Creates an iri.
	 * @param namespace The namespace of this iri.
	 * @param localName The local name of this iri.
	 * @return The newly created iri.
	 */
	public static final Iri createIri(final Namespace namespace, final LocalName localName) {
		return new IriImpl(namespace, localName);
	}
	
	/**
	 * Creates an iri.
	 * @param iri The string that is encapsulated as iri in this object.
	 * @return The newly created iri.
	 */
	public static final Iri createIri(final String iri) {
		return new IriImpl(iri);
	}
	
	/**
	 * Creates an iri with the default namespace of the PackagingToolkit.
	 * @param uuid The uuid that is the local name in this iri.
	 * @return The newly created iri.
	 */
	public static final Iri createIri(final Uuid uuid) {
		return new IriImpl(uuid);
	}
}
