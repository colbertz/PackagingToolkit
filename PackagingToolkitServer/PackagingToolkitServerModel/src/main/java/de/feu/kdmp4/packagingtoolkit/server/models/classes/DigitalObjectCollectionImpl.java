package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import java.util.Collection;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.SortableAbstractList;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObjectCollection;
 
public class DigitalObjectCollectionImpl extends SortableAbstractList<DigitalObject>
								   implements DigitalObjectCollection {
	/**
	 * Creates an empty collection for digital objects.
	 */
	public DigitalObjectCollectionImpl() {
	}
	
	/**
	 * Creates a collection for digital objects with the values in a Java list.
	 * @param digitalObjectList Contains the values that should be contained in the collection.
	 */
	public DigitalObjectCollectionImpl(final List<DigitalObject> digitalObjectList) {
		super(digitalObjectList);
	}
	
	/**
	 * Creates a collection for digital objects with the values in a Java collection.
	 * @param digitalObjectCollection
	 */
	public DigitalObjectCollectionImpl(final Collection<DigitalObject> digitalObjectCollection) {
		super(digitalObjectCollection);
	}
	
	@Override
	public void addDigitalObject(final DigitalObject digitalObject) {
			super.add(digitalObject);
	}
	
	@Override
	public void deleteDigitalObject(final DigitalObject digitalObject) {
			super.remove(digitalObject);
	}
	
	@Override
	public boolean isEmpty() {
		if (getDigitalObjectCount() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public DigitalObject getDigitalObjectAt(final int index) {
		return super.getElement(index);
	}
	
	@Override
	public int getDigitalObjectCount() {
		return super.getSize();
	}
}
