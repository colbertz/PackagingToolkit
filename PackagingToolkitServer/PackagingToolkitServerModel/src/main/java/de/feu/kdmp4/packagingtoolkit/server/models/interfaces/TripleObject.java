package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;

/**
 * Represents the object in a RDF tripel that consists of a subject, a predicate and an object.
 * @author Christopher Olbertz
 *
 */
public interface TripleObject {
	/**
	 * Gets the uuid of the object.
	 * @return The uuid of the object.
	 */
	Uuid getUuid();
	/**
	 * Gets the namespace of the iri of the object. The namespace is the part before the # symbol.
	 * @return The namespace of the iri of the object.
	 */
	Namespace getNamespace();
	/**
	 * Gets the local name of the iri of the object. The local name is the part after the # symbol.
	 * @return The local name of the iri of the object.
	 */
	LocalName getLocalName();
	/**
	 * Gets the object that is assigned to this object.
	 * @return The object that is assigned to this object.
	 */
	Object getValue();
	/**
	 * Gets the datatype of the value.
	 * @return The datatype of the value.
	 */
	OntologyDatatype getDatatype();
	/**
	 * Sets the datatype of the value.
	 * @param datatype The datatype of the value.
	 */
	void setDatatype(final OntologyDatatype datatype);
	/**
	 * Gets the iri of the object. The iri consists of a namespace and a local name separated by a # symbol.
	 * @return The iri of the object
	 */
	Iri getIri();
	/**
	 * Determines if this object contains only a literal. The object is containing a literal if the namespace
	 * is null.
	 * @return True if this object contains only a literal, false otherwise.
	 */
	boolean isLiteralObject();
}
