/**
 * Contains the interfaces for classes that are used to exchange data between modules.
 * These objects are not send to another component, they are not saved in the databases, they
 * are only used for the exchange.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces;