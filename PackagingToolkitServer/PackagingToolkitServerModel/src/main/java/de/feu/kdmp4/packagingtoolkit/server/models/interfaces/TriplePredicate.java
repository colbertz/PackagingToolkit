package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;

/**
 * Represents the predicate in a RDF tripel that consists of a subject, a predicate and an object.
 * @author Christopher Olbertz
 *
 */
public interface TriplePredicate {
	/**
	 * Gets the namespace of the iri of the predicate. The namespace is the part before the # symbol.
	 * @return The namespace of the iri of the predicate.
	 */
	Namespace getNamespace();
	/**
	 * Gets the local name of the iri of the predicate. The local name is the part after the # symbol.
	 * @return The local name of the iri of the predicate.
	 */
	LocalName getLocalName();
	/**
	 * Gets the uuid of the object.
	 * @return The uuid of the object.
	 */
	Uuid getUuid();
	/**
	 * Gets the type of the predicate. 
	 * @return The type of the predicate. 
	 */
	PredicateType getPredicateType();
	/**
	 * Sets the type of the predicate. 
	 * @param predicateType The type of the predicate. 
	 */
	void setPredicateType(PredicateType predicateType);
	/**
	 * Gets the iri of the object. The iri consists of a namespace and a local name separated by a # symbol.
	 * @return The iri of the object
	 */
	Iri getIri();
	/**
	 * Determines if the predicate describes a relationship to a local file on the server.
	 * @return True if the predicate describes a relationship to a local file on the server, false otherwise.
	 */
	boolean isLocalFilePredicate();
	/**
	 * Determines if the predicate describes a relationship to a file on a remote server. 
	 * @return True if the predicate describes a relationship to a file on a remote server.
	 */
	boolean isRemoteFilePredicate();
	/**
	 * Determines if the predicate describes a typeOf relationship between an individual and a class. 
	 * @return True if the predicate describes a typeOf relationship.
	 */
	boolean isTypePredicate();
	/**
	 * Determines if the predicate describes a relationship to a literal. 
	 * @return True if the predicate describes a relationship to a literal, false otherwise.
	 */
	boolean isLiteralPredicate();
	/**
	 * Determines if the predicate describes a relationship to an object. 
	 * @return True if the predicate describes a relationship to an object, false otherwise.
	 */
	boolean isObjectPredicate();
	/**
	 * Determines if the predicate describes a relationship to an individual in a taxonomy. 
	 * @return True if the predicate describes a relationship to an individual in a taxonomy., false otherwise.
	 */
	boolean isTaxonomyIndividualStatement();
}
