package de.feu.kdmp4.packagingtoolkit.server.models.exceptions;

import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;
import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class WorkingDirectoryException extends PackagingToolkitException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9105369727995545817L;
	
	private WorkingDirectoryException(final String workingDirectoryPath,
							 final String theMessage, 
							 final String theClass, 
							 final String theMethod) {
		super(theMessage, theClass, theMethod, PackagingToolkitComponent.SERVER);
	}

	public static WorkingDirectoryException pathMayNotBeNullOrEmptyException(final String theClass, 
			final String theMethod) {
		String message = I18nExceptionUtil.getPathnameMayNotBeEmptyString();
		return new WorkingDirectoryException("", message, theClass, theMethod);
	}
	
	/**
	 * Returns the additional information of this exception.
	 * @return The path of the invalid working directory.
	 */
	@Override
	public String getAdditionalInformation() {
		return (String)additionalInformation;
	}

	@Override
	public String getMessage() {
		String message = super.getMessage();
		return String.format(message, getAdditionalInformation());
	}
}