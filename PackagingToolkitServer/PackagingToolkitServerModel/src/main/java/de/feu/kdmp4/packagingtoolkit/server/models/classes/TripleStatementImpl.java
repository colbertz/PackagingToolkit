package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;

public class TripleStatementImpl implements TripleStatement {
	/**
	 * The subject of the triple. Can be any object that can be identified by an iri.
	 */
	private TripleSubject subject;
	/**
	 * The predicate of the tripel. It describes the relationship between the subject and the object.
	 */
	private TriplePredicate predicate;
	/**
	 * The object of the tripel. Can be an object identified by an iri or a literal.
	 */
	private TripleObject object;
	
	/**
	 * Creates a statement that can be saved in the tripel store. 
	 * @param subject The subject of the triple. Can be any object that can be identified by an iri.
	 * @param predicate The predicate of the tripel. It describes the relationship between the subject and the object.
	 * @param object The object of the tripel. Can be an object identified by an iri or a literal.
	 */
	public TripleStatementImpl(final TripleSubject subject, final TriplePredicate predicate, final TripleObject object) {
		super();
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	@Override
	public TripleSubject getSubject() {
		return subject;
	}

	@Override
	public TriplePredicate getPredicate() {
		return predicate;
	}

	@Override
	public TripleObject getObject() {
		return object;
	}

	@Override
	public boolean isLiteralStatement() {
		if (predicate.getPredicateType() == PredicateType.LITERAL) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isReferenceStatement() {
		if (predicate.getPredicateType().equals(PredicateType.REMOTE_FILE_REFERENCE)) {
			return true;
		} else if (predicate.getPredicateType().equals(PredicateType.LOCAL_FILE_REFERENCE)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isAggregatedByStatement() {
		if (predicate.getPredicateType().equals(PredicateType.AGGREGATED_BY)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isHasDatatypeStatement() {
		if (predicate.getPredicateType().equals(PredicateType.HAS_DATATYPE)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		result = prime * result + ((predicate == null) ? 0 : predicate.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TripleStatementImpl other = (TripleStatementImpl) obj;
		if (object == null) {
			if (other.object != null)
				return false;
		} else if (!object.equals(other.object))
			return false;
		if (predicate == null) {
			if (other.predicate != null)
				return false;
		} else if (!predicate.equals(other.predicate))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return subject.toString() + " " + predicate.toString() + " " + object.toString();
	}
}
