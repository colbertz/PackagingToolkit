package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import java.io.Serializable;
import java.util.Collection;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;

public class ReferenceCollectionImpl extends AbstractList  implements ReferenceCollection, Serializable {
	private static final long serialVersionUID = -2774754858876620076L;
	
	public ReferenceCollectionImpl() {
	}
	
	public ReferenceCollectionImpl(final Collection<Reference> referenceCollection) {
		for (Reference reference: referenceCollection) {
			addReference(reference);
		}
	}
	
	@Override
	public void addReference(final Reference reference) {
		super.add(reference);

	}
	
	@Override
	public Reference getReference(final int index) {
		return (Reference)super.getElement(index);
	}
	
	@Override
	public int getReferencesCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		if (getReferencesCount() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isNotEmpty() {
		if (getReferencesCount() != 0) {
			return true;
		} else {
			return false;
		}
	}
}
