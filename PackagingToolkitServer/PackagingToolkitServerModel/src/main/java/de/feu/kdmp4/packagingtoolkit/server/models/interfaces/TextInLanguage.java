package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;

/**
 * Represents a text in a specific language. It contains the text 
 * itself and the language the text is written in. Is used for the 
 * property labels that can be written in different languages. 
 * @author Christopher Olbertz
 *
 */
public interface TextInLanguage {
	String getLabelText();
	void setLabelText(final String labelText);
	OntologyLabelLanguage getLanguage();
	void setLanguage(final OntologyLabelLanguage language);
}
