package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class TripleObjectImpl implements TripleObject {
	/**
	 * The uuid of the object.
	 */
	private Uuid uuid;
	/**
	 * The data type of the object. 
	 */
	private OntologyDatatype datatype;
	/**
	 * The namespace of the object. Thats the part before the # in the iri.
	 */
	private Namespace namespace;
	/**
	 * The local name of the object. Thats the part after the # in the iri.
	 */
	private LocalName localName;
	/**
	 * The value of this object.
	 */
	private Object value;
	
	/**
	 * Creates a new triple object. 
	 * @param namespace The namespace of the object. Thats the part before the # in the iri.
	 * @param localName The local name of the object. Thats the part after the # in the iri.
	 */
	public TripleObjectImpl(final Namespace namespace, final LocalName localName) {
		this.namespace = namespace;
		this.localName = localName;
		datatype = OntologyDatatype.OBJECT;
	}
	
	/**
	 * Creates an object only with a value. If the value is a valid iri that begins with a namespace and a #,
	 * then this namespace is used as namespace. If the value does not contain a namespace the default 
	 * namespace of the PackagingToolkit is used. Then the datatype is determined with the help of
	 * the value.
	 * @param value The value of the object. Can be an iri but do not have to.
	 */
	public TripleObjectImpl(final Object value) {
		this(value, false);
	}
	
	/**
	 * Creates an object only with a value. If the value is a valid iri that begins with a namespace and a #,
	 * then this namespace is used as namespace. If the value does not contain a namespace the default 
	 * namespace of the PackagingToolkit is used. Then the datatype is determined with the help of
	 * the value.
	 * @param value The value of the object. Can be an iri but do not have to.
	 */
	public TripleObjectImpl(final Object value, final boolean isLiteral) {
		if (!isLiteral) {
			final String namespaceAsString = StringUtils.extractNamespaceFromIri(value.toString()); 
			
			if (StringUtils.areStringsEqual(namespaceAsString, value.toString())) {
				this.namespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
				this.localName = ServerModelFactory.createLocalName(value.toString());
			} else {
				final String localNameAsString = StringUtils.extractLocalNameFromIri(value.toString());
				this.localName = ServerModelFactory.createLocalName(localNameAsString);
				this.namespace = ServerModelFactory.createNamespace(namespaceAsString);
			}
			this.value = value;
		} else {
			this.namespace = null;
			this.localName = ServerModelFactory.createLocalName(value.toString());
			this.value = value;
		}
		
		determineDatatype();
	}
	
	/**
	 * Determines the datatype of the value and sets the attribute datatype.
	 */
	private void determineDatatype() {
		final String valueAsString = value.toString();
		if (value instanceof Boolean) {
			datatype = OntologyDatatype.BOOLEAN;
		} else if (value instanceof Short || value instanceof Byte)	{
			datatype = OntologyDatatype.BYTE;
		} else if (value instanceof LocalDate)	{
			datatype = OntologyDatatype.DATE;
		} else if (value instanceof LocalDateTime)	{
			datatype = OntologyDatatype.DATETIME;
		} else if (value instanceof Double)	{
			datatype = OntologyDatatype.DOUBLE;
		} else if (value instanceof OntologyDuration)	{
			datatype = OntologyDatatype.DURATION;
		} else if (value instanceof Float)	{
			datatype = OntologyDatatype.FLOAT;
		} else if (value instanceof Long)	{
			datatype = OntologyDatatype.INTEGER;
		} else if (value instanceof BigInteger)	{
			datatype = OntologyDatatype.LONG;
		} else if (value instanceof Integer)	{
			datatype = OntologyDatatype.SHORT;
		} else if ((value instanceof String) && (valueAsString.contains("[a-zA-Z0-9]*#[a-zA-Z0-9]*") == false)) {
			datatype = OntologyDatatype.STRING;
		} else if (value instanceof LocalTime)	{
			datatype = OntologyDatatype.TIME;
		} else { 
			datatype = OntologyDatatype.OBJECT;
			// TODO Aus einem ObjectProperty die Uuid bestimmen. Am besten getUuid() in ein Interface einbauen.
		}
	}
	
	@Override
	public Uuid getUuid() {
		return uuid;
	}
	
	@Override
	public Namespace getNamespace() {
		return namespace;
	}

	@Override
	public LocalName getLocalName() {
		return localName;
	}
	
	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public OntologyDatatype getDatatype() {
		return datatype;
	}

	@Override
	public void setDatatype(final OntologyDatatype datatype) {
		this.datatype = datatype;
	}

	public void setUuid(final Uuid uuid) {
		this.uuid = uuid;
	}

	public void setNamespace(final Namespace namespace) {
		this.namespace = namespace;
	}

	public void setLocalName(final LocalName localName) {
		this.localName = localName;
	}

	public void setValue(final Object value) {
		this.value = value;
	}
	
	/**
	 * Returns the value of this object. If the namespace and the local name are not empty, then the value is
	 * a reference to an object. In this case, the iri of this object is returned.
	 * @return A string that contains either the value or the iri of the referenced object. 
	 */
	@Override
	public String toString() {
		if (namespace != null) {
			if (localName.isNotEmpty() & namespace.isNotEmpty()) {
				return namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localName;
			} else {
				return value.toString();
			}
		} else {
			return value.toString();
		}
	}

	@Override
	public Iri getIri() {
		if (localName.isNotEmpty() && namespace.isNotEmpty()) {
			return ServerModelFactory.createIri(namespace, localName);
		} else {
			return ServerModelFactory.createIri(value.toString());
		}
	}
	
	@Override
	public boolean isLiteralObject() {
		if (namespace == null) {
			return true;
		} else {
			return false;
		}
	}
}
