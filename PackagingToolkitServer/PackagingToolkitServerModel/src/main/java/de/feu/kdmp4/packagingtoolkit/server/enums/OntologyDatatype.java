package de.feu.kdmp4.packagingtoolkit.server.enums;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;

/**
 * Contains the information about the datatypes in an ontology.
 * @author Christopher Olbertz
 *
 */
public enum OntologyDatatype {
	BOOLEAN("boolean"), BYTE("byte"), 
	DATE("date"), DATETIME("datetime"), DOUBLE("double"), DURATION("duration"), 
	FLOAT("float"), 
	INTEGER("int"), 
	LONG("long"),
	NEGATIVE_INTEGER("negativeinteger"),
	NON_NEGATIVE_INTEGER("nonnegativeinteger"),	NON_POSITIVE_INTEGER("nonpositiveinteger"),	
	OBJECT("object"),
	POSITIVE_INTEGER("positiveinteger"),
	SHORT("short"), STRING("string"),
	TIME("time"),
	UNSIGNED("unsigned"), UNSIGNED_BYTE("unsignedbyte"), UNSIGNED_INT("unsignedint");
	
	/**
	 * The range that is extracted from an ontology and that contains the information
	 * about the datatype.
	 */
	private String range;
	/**
	 * The namespace of the datatypes. If the iri of a datatype is requested, the namespace is written before
	 * the name of the datatype.
	 */
	private String NAMESPACE_DATATYPES = "http://www.w3.org/2001/XMLSchema";
	
	/**
	 * The constructor constructs a new object of the enum.
	 * @param range The xml range of the datatype the enum object is representing.
	 */
	private OntologyDatatype(final String range) {
		this.range = range;
	}
	
	/**
	 * Determines a datatype by the information given in the iri.
	 * @param iri The iri of the datatype.
	 * @return The determined datatype.
	 */
	public static OntologyDatatype getDatatypeByIri(final String iri) {
		if (iri.contains(BOOLEAN.range)) {
			return OntologyDatatype.BOOLEAN; 
		} else if (iri.contains(UNSIGNED_BYTE.range)) {
			return OntologyDatatype.UNSIGNED_BYTE;
		} else if (iri.contains(UNSIGNED_INT.range)) {
			return OntologyDatatype.UNSIGNED_INT;
		} else if (iri.contains(BYTE.range)) {
			return OntologyDatatype.BYTE;
		} else if (iri.contains(DATETIME.range)) {
			return OntologyDatatype.DATETIME;
		} else if (iri.contains(DATE.range)) {
			return OntologyDatatype.DATE;
		} else if (iri.contains(DOUBLE.range)) {
			return OntologyDatatype.DOUBLE;
		} else if (iri.contains(DURATION.range)) {
			return OntologyDatatype.DURATION;
		} else if (iri.contains(FLOAT.range)) {
			return OntologyDatatype.FLOAT;
		} else if (iri.contains(LONG.range)) {
			return OntologyDatatype.LONG;
		} else if (iri.contains(NEGATIVE_INTEGER.range)) {
			return OntologyDatatype.NEGATIVE_INTEGER;
		} else if (iri.contains(NON_NEGATIVE_INTEGER.range)) {
			return OntologyDatatype.NON_NEGATIVE_INTEGER;
		} else if (iri.contains(INTEGER.range)) {
			return OntologyDatatype.INTEGER;
		} else if (iri.contains(NON_POSITIVE_INTEGER.range)) {
			return OntologyDatatype.NON_POSITIVE_INTEGER;
		} else if (iri.contains(POSITIVE_INTEGER.range)) {
			return OntologyDatatype.POSITIVE_INTEGER;
		} else if (iri.contains(OBJECT.range)) {
			return OntologyDatatype.OBJECT;
		} else if (iri.contains(SHORT.range)) {
			return OntologyDatatype.SHORT;
		} else if (iri.contains(STRING.range)) {
			return OntologyDatatype.STRING;
		} else if (iri.contains(TIME.range)) {
			return OntologyDatatype.TIME;
		} else {
			return null;
		}
	}
	
	/**
	 * Returns the default namespace of all datatypes.
	 * @return The default namespace of all datatypes.
	 */
	public Namespace getNamespace() {
		return ServerModelFactory.createNamespace(NAMESPACE_DATATYPES);
	}
	
	/**
	 * Returns the iri of the datatype. The iri starts with the default namespace.
	 * @return The iri of the datatype the enum object is representing.
	 */
	public String getIri() {
		return NAMESPACE_DATATYPES + PackagingToolkitConstants.NAMESPACE_SEPARATOR + range;
	}
	
	/**
	 * Returns the range of this datatype.
	 * @return The range of this datatype.
	 */
	public String getRange() {
		return range;
	}
	
	/**
	 * Returns the local name of this datatype.
	 * @return The local name of this datatype.
	 */
	public LocalName getLocalName() {
		return ServerModelFactory.createLocalName(range);
	}
	
	/**
	 * Checks with the help of a property range if the property is a boolean property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a boolean property, false otherwise.
	 */
	public static boolean isBoolean(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(BOOLEAN.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a byte property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a byte property, false otherwise.
	 */
	public static boolean isByte(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(BYTE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a date property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a date property, false otherwise.
	 */
	public static boolean isDate(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DATE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a datetimeproperty.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a datetime property, false otherwise.
	 */
	public static boolean isDateTime(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DATETIME.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a double property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a double property, false otherwise.
	 */
	public static boolean isDouble(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DOUBLE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a duration property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a duration property, false otherwise.
	 */
	public static boolean isDuration(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DURATION.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a float property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a float property, false otherwise.
	 */
	public static boolean isFloat(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(FLOAT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is an integer property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is an integer property, false otherwise.
	 */
	public static boolean isInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a long property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a long property, false otherwise.
	 */
	public static boolean isLong(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(LONG.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is an object property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is an object property, false otherwise.
	 */
	public static boolean isObjectProperty(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(OBJECT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a short property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a short property, false otherwise.
	 */
	public static boolean isShort(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(SHORT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a string property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a string property, false otherwise.
	 */
	public static boolean isString(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(STRING.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a time property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a time property, false otherwise.
	 */
	public static boolean isTime(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(TIME.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is an unsigned property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is an unsigned property, false otherwise.
	 */
	public static boolean isUnsigned(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		if (lowerPropertyRange.contains(UNSIGNED.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is an unsigned byte property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is an unsigned byte property, false otherwise.
	 */
	public static boolean isUnsignedByte(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(UNSIGNED_BYTE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a negative integer property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a negative integer property, false otherwise.
	 */
	public static boolean isNegativeInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NEGATIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a non negative integer property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a non negative integer property, false otherwise.
	 */
	public static boolean isNonNegativeInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NON_NEGATIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a non positive integer property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a non positive integer property, false otherwise.
	 */
	public static boolean isNonPositiveInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NON_POSITIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is a positive integer property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is a positive integer property, false otherwise.
	 */
	public static boolean isPositiveInteger(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(POSITIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks with the help of a property range if the property is an unsigned int property.
	 * @param propertyRange The range to use for the check.
	 * @return True if the property is an unsigned int property, false otherwise.
	 */
	public static boolean isUnsignedInt(final String propertyRange) {
		final String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(UNSIGNED_INT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
}