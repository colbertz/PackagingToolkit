package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitFileException;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.exceptions.WorkingDirectoryException;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class WorkingDirectoryImpl implements WorkingDirectory {
	// *********** Constants **********
	/**
	 * The name of the directory that contains configuration data like
	 * the rule ontology.
	 */
	private static final String CONFIG_DIRECTORY = "config";
	private static final String CONSTRUCTOR_STRING = "WorkingDirectoryImpl(String workingDirectoryPath)";
	/**
	 * The name of the directory that contains the data of the triple store.
	 */
	private static final String DATA_DIRECTORY = "data";
	/**
	 * Contains the name of the storage directory that is contained
	 * in the working directory and contains itself the zipped archives.
	 */
	private static final String STORAGE_DIRECTORY = "storage"; 
	/**
	 * Contains the name of the temp directory that is contained
	 * in the working directory and contains itself a directory for
	 * each unzipped archive. Each unzipped archive is in the session.
	 */
	private static final String TEMP_DIRECTORY = "temp";
	
	// ********* Attributes ***********
	private String workingDirectoryPath;
	
	// ********* Constructors *********
	public WorkingDirectoryImpl(final String workingDirectoryPath) {
		if (StringValidator.isNullOrEmpty(workingDirectoryPath)) {
			WorkingDirectoryException exception = 
					WorkingDirectoryException.pathMayNotBeNullOrEmptyException(
							this.getClass().getName(), CONSTRUCTOR_STRING);
			throw exception;
		}
		
		this.workingDirectoryPath = workingDirectoryPath;
	}
	
	// ******** Public methods *********
	@Override
	public File getConfigurationDirectory() {
		File configurationDirectory = new File(getConfigurationDirectoryPath());
		
		if (!configurationDirectory.exists()) {
			configurationDirectory.mkdirs();
		}
		
		return configurationDirectory;
	}
	
	@Override
	public File getFileInTempDirectory(final String filename, final String uuid) {
		String temporaryDirectoryPath = getTempDirectoryPath() + File.separator + uuid;
		File tempDirectory = new File(temporaryDirectoryPath);
		
		if (!tempDirectory.exists()) {
			tempDirectory.mkdirs();
		}
		
		String filePath = temporaryDirectoryPath + File.separator + filename;
		return new File(filePath);
	}

	@Override
	public File getDataDirectory() {
		File dataDirectory = new File(getDataDirectoryPath());
		
		if (!dataDirectory.exists()) {
			dataDirectory.mkdirs();
		}
		
		return dataDirectory;
	}
	
	@Override
	public File getStorageDirectory() {
		File storageDirectory = new File(getStorageDirectoryPath());
		if (!storageDirectory.exists()) {
			storageDirectory.mkdir();
		}
		
		return storageDirectory;
	}
	
	@Override
	public File getTemporaryDirectory() {
		String temporaryDirectoryPath = workingDirectoryPath + File.separator + TEMP_DIRECTORY;
		File temporaryDirectory = new File(temporaryDirectoryPath);
		
		if (!temporaryDirectory.exists()){
			temporaryDirectory.mkdirs();
		}
		
		return temporaryDirectory;
	}
	
	@Override
	public File getTemporaryDirectoryForUuid(final String uuid) {
		String temporaryDirectoryPath = getTempDirectoryPath();
		temporaryDirectoryPath = temporaryDirectoryPath + File.separator + uuid;
		File temporaryDirectory = new File(temporaryDirectoryPath);
		
		return temporaryDirectory; 
	}
	
	@Override
	public File getTemporaryDirectoryForUuidOrCreate(final String uuid) {
		File temporaryDirectory = getTemporaryDirectoryForUuid(uuid);
		
		if (!temporaryDirectory.exists()) {
			temporaryDirectory.mkdirs();
		}
		
		return temporaryDirectory; 
	}
	
	@Override
	public File getTemporaryDirectoryForUuid(final Uuid uuid) {
		return getTemporaryDirectoryForUuid(uuid.toString());
	}
	
	@Override
	public String getWorkingDirectoryPath() {
		File file = new File(workingDirectoryPath);
		return file.getAbsolutePath();
	}
	
	@Override
	public File saveFileContentInConfigurationDirectory(final byte[] fileContent, final String filename) {
		File temporaryFile = new File(getConfigurationDirectory(), filename);
		PackagingToolkitFileUtils.writeByteArrayToFile(temporaryFile, fileContent);
		return temporaryFile;
	}
	
	// ******** Private methods **********
	/**
	 * Returns the path of the configuration directory within the working direcory. The
	 * path ends with a file separator.
	 * @return The path of the configuration directory within the working direcory.
	 */
	private String getConfigurationDirectoryPath() {
		return workingDirectoryPath + File.separator + CONFIG_DIRECTORY + File.separator;
	}
	
	/**
	 * Returns the path of the data directory within the working direcory. The
	 * path ends with a file separator.
	 * @return The path of the data directory within the working direcory.
	 */
	private String getDataDirectoryPath() {
		return workingDirectoryPath + File.separator + DATA_DIRECTORY + File.separator;
	}
	
	/**
	 * Returns the path of the storage directory within the working direcory. The
	 * path ends with a file separator.
	 * @return The path of the storage directory within the working direcory.
	 */
	private String getStorageDirectoryPath() {
		return workingDirectoryPath + File.separator + STORAGE_DIRECTORY + File.separator;
	}
	
	/**
	 * Returns the path of the temporary directory within the working direcory. The
	 * path ends with a file separator.
	 * @return The path of the temporary directory within the working direcory.
	 */
	private String getTempDirectoryPath() {
		return workingDirectoryPath + File.separator + TEMP_DIRECTORY + File.separator;
	}

	@Override
	public File getFileInStorageDirectory(final String filename) {
		File file = new File(getStorageDirectory(), filename);
		return file;
	}

	@Override
	public boolean temporaryDirectoryForUuidExists(final Uuid uuid) {
		File temporaryDirectory = getTemporaryDirectoryForUuid(uuid);
		return temporaryDirectory.exists();
	}

	@Override
	public void deleteTemporaryDirectoryIfExists(final String uuidOfInformationPackage) {
		String pathOfTemporaryDirectory = getTemporaryDirectoryForUuid(uuidOfInformationPackage).getAbsolutePath();
		File tempDir = new File(pathOfTemporaryDirectory);
		
		if (tempDir.exists()) {
			validateArchiveDirectory(tempDir);
			File[] files = tempDir.listFiles();
			if (files != null) {
				for (File file: files) {
					file.delete();
				}
			}
			tempDir.delete();
		}
	}
	
	/**
	 * Checks, if a directory satisfies the constraints for a directory
	 * that contains files for zipping into a archive. The following 
	 * constraints are defined:
	 * <ul>
	 *  <li>The file object that represents the directory exists.</li>
	 *  <li>The file object has to represent a directory.</li>
	 *  <li>The directory may not be empty.</li>
	 * </ul>
	 * Exceptions are thrown, if one of these constraints is not satisfied.
	 * @param archiveDirectory Represents the directory whose files should be packed
	 * in an archive.
	 * @throws PackagingException is thrown if archiveDirectory does not exist
	 * or if archiveDirectory does not contain any files.
	 * @throws PackagingToolkitFileException is thrown if archiveDirectory does not represent a directory.
	 */
	// TEST_ME
	private void validateArchiveDirectory(final File archiveDirectory) {
		if (!archiveDirectory.exists()) {
			//throw PackagingException.getArchiveDirectoryDoesNotExist(archiveDirectory.getName());
		}
		
		if (!archiveDirectory.isDirectory()) {
			throw PackagingToolkitFileException.notADirectoryException(archiveDirectory.getAbsolutePath());
		}

		File[] files = archiveDirectory.listFiles(); 
		if (files == null || files.length == 0) {
			//throw PackagingException.getArchiveDirectoryMayNotBeEmpty(archiveDirectory.getAbsolutePath());
		}
	}
}
