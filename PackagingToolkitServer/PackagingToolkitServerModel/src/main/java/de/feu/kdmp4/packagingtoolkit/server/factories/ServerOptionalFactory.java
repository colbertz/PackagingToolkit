package de.feu.kdmp4.packagingtoolkit.server.factories;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;

/**
 * Encapsulates the creation of objects of {@link java.util.Optional}. The reason for this class is to remove the need
 * of knowing how Optionals are created. 
 * @author Christopher Olbertz
 *
 */
public class ServerOptionalFactory {
	/**
	 * Creates an optional with an exchange object for metadata.
	 * @param metadataExchange The object that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<MetadataExchange> createOptionalWithUuid(final MetadataExchange metadataExchange) {
		return Optional.of(metadataExchange);
	}
	
	/**
	 * Creates an empty optional for an exchange object for metadata.
	 * @return The created empty Optional.
	 */
	public final static Optional<MetadataExchange> createEmptyOptionalWithMetadataExchange() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an exchange object for labels.
	 * @param textInLanguage The object that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TextInLanguage> createOptionalWithTextInLanguage(final TextInLanguage textInLanguage) {
		return Optional.of(textInLanguage);
	}
	
	/**
	 * Creates an empty optional for an exchange object for labels.
	 * @return The created empty Optional.
	 */
	public final static Optional<TextInLanguage> createEmptyOptionalWithTextInLanguage() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a triple statements.
	 * @param tripleStatements The triple statements that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TripleStatementCollection> createOptionalWithTripleStatements(final TripleStatementCollection tripleStatements) {
		return Optional.of(tripleStatements);
	}
	
	/**
	 * Creates an empty optional for a triple statements.
	 * @return The created empty Optional.
	 */
	public final static Optional<TripleStatementCollection> createEmptyOptionalWithTripleStatements() {
		return Optional.empty();
	}

	/**
	 * Creates an optional with a triple statement.
	 * @param tripleStatements The triple statement that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TripleStatement> createOptionalWithTripleStatement(final TripleStatement tripleStatement) {
		return Optional.of(tripleStatement);
	}
	
	/**
	 * Creates an empty optional for a triple statement.
	 * @return The created empty Optional.
	 */
	public final static Optional<TripleStatement> createEmptyOptionalWithTripleStatement() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an information package exchange.
	 * @param informationPackageExchange The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<InformationPackageExchange> createOptionalWithInformationPackageExchange(final InformationPackageExchange informationPackageExchange) {
		return Optional.of(informationPackageExchange);
	}
	
	/**
	 * Creates an empty optional for an information package exchange.
	 * @return The created empty Optional.
	 */
	public final static Optional<InformationPackageExchange> createEmptyOptionalWithInformationPackageExchange() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an ontology datatype.
	 * @param ontologyDatatype The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyDatatype> createOptionalWithOntologyDatatype(final OntologyDatatype ontologyDatatype) {
		return Optional.of(ontologyDatatype);
	}
	
	/**
	 * Creates an empty optional for an ontology datatype.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyDatatype> createEmptyOptionalWithOntologyDatatype() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a digital object.
	 * @param digitalObject The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<DigitalObject> createOptionalWithDigitalObject(final DigitalObject digitalObject) {
		return Optional.of(digitalObject);
	}
	
	/**
	 * Creates an empty optional for an digital object exchange.
	 * @return The created empty Optional.
	 */
	public final static Optional<DigitalObjectExchange> createEmptyOptionalWithDigitalObjectExchange() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a digital object exchange.
	 * @param digitalObject The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<DigitalObjectExchange> createOptionalWithDigitalObjectExchange(final DigitalObjectExchange digitalObjectExchange) {
		return Optional.of(digitalObjectExchange);
	}
	
	/**
	 * Creates an empty optional for a digital object.
	 * @return The created empty Optional.
	 */
	public final static Optional<DigitalObject> createEmptyOptionalWithDigitalObject() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an iri.
	 * @param iri The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<Iri> createOptionalWithIri(final Iri iri) {
		if (iri != null) {
			return Optional.of(iri);
		}
		return createEmptyOptionalWithIri();
	}
	
	/**
	 * Creates an empty optional for an iri.
	 * @return The created empty Optional.
	 */
	public final static Optional<Iri> createEmptyOptionalWithIri() {
		return Optional.empty();
	}
}
