package de.feu.kdmp4.packagingtoolkit.server.exchange.classes;

import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;

/**
 * The objects are organized in a list in this implementation.
 * @author Christopher Olbertz
 *
 */
public class InformationPackageExchangeCollectionListImpl extends AbstractList implements InformationPackageExchangeCollection {
	@Override
	public void addInformationPackageExchange(final InformationPackageExchange informationPackageExchange) {
		super.add(informationPackageExchange);
	}
	
	@Override
	public int getInformationPackageExchangeCount() {
		return super.getSize();
	}
	
	@Override
	public InformationPackageExchange getInformationPackageExchangeAt(final int index) {
		Object object = super.getElement(index);
		return (InformationPackageExchange)object;
	}
}
