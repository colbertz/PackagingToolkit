package de.feu.kdmp4.packagingtoolkit.server.models.interfaces;

import java.util.Calendar;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Contains the data about information packages read from the database. Is used for avoiding that
 * the entity objects have to be known in the whole program.
 * @author Christopher Olbertz
 *
 */
public interface InformationPackageData {
	void setLastModifiedDate(Calendar lastModifiedDate);
	Calendar getLastModifiedDate();
	void setCreationDate(Calendar creationDate);
	Calendar getCreationDate();
	void setPackageType(PackageType packageType);
	PackageType getPackageType();
	void setTitle(String title);
	void setUuidOfInformationPackage(Uuid uuidOfInformationPackage);
	Uuid getUuidOfInformationPackage();
	String getTitle();
}
