package de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces;

/**
 * Contains some information packages for exchanging between the borders of modules. 
 * @author Christopher Olbertz
 *
 */
public interface InformationPackageExchangeCollection {
	/**
	 * Add a new information package to the collection.
	 * @param informationPackageExchange The information package that should be added.
	 */
	void addInformationPackageExchange(final InformationPackageExchange informationPackageExchange);
	/**
	 * Determines the number of information package that are stored in the collection.
	 * @return The number of information packages.
	 */
	int getInformationPackageExchangeCount();
	/**
	 * Returns an information package at a given index in the collection.
	 * @param index  The index we want to look at.
	 * @return The information package found at index.
	 */
	InformationPackageExchange getInformationPackageExchangeAt(final int index);
}
