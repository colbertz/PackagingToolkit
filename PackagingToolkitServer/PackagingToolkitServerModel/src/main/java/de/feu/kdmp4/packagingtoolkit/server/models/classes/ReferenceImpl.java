package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;

/**
 * A reference to a file. At the moment there are two types of reference:
 * <ul>
 * 	<li>A reference to a file on the local file system of the server. These files are stored
 * in the storage directory.</li>
 * 	<li>A reference to a remote file that is saved on another server.
 * </ul>
 * @author Christopher Olbertz
 *
 */
public class ReferenceImpl implements Reference {
	/**
	 * The url of the file. This could be a path on a file system or the url on a remote server.
	 */
	private String url;
	/**
	 * The type of the reference says wether it is a remote or a local reference.
	 */
	private ReferenceType referenceType;
	
	/**
	 * The uuid of the file this reference belongs to. The reference type is determined
	 * by the url. If the url starts with http the reference is a http reference to a remote
	 * file, if the url starts with file the reference references a file in the storage directory
	 * of the server.
	 */
	private Uuid uuidOfFile;
	
	/**
	 * If an uuid is given to the constructor, it creates a local reference.
	 * @param url The path of the file on the file system.
	 * @param uuidOfFile The uuid of the file. The uuid is saved in the database.
	 */
	private ReferenceImpl(final String url, final Uuid uuidOfFile) {
		super();
		this.url = url;
		this.uuidOfFile = uuidOfFile;
		referenceType = ReferenceType.FILE;
	}
	
	/**
	 * If only an url is given to the constructor, it creates a remote reference.
	 * @param url The url of the file on another server.
	 */
	private ReferenceImpl(final String url) {
		super();
		this.url = url;
		referenceType = ReferenceType.REMOTE;
	}
	
	/**
	 * Creates a remote reference.
	 * @param url The url of the file on another server.
	 * @return The newly created remote reference.
	 */
	public static Reference createRemoteFileReference(final String url) {
		return new ReferenceImpl(url);
	}

	/**
	 * Creates a reference to a local file.
	 * @param url The path of the file on the file system.
	 * @param uuidOfFile The uuid of the file. The uuid is saved in the database.
	 * @return The newly created remote reference.
	 */
	public static Reference createLocalFileReference(final String url, final Uuid uuidOfFile) {
		return new ReferenceImpl(url, uuidOfFile);
	}
	
	@Override
	public String getUrl() {
		return url;
	}
	
	@Override
	public Uuid getUuidOfFile() {
		return uuidOfFile;
	}
	
	@Override
	public boolean isLocalFileReference() {
		if (referenceType == ReferenceType.FILE) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isRemoteFileReference() {
		if (referenceType == ReferenceType.REMOTE) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public TripleStatement asTripleStatement(final Uuid uuidOfInformationPackage) {
		TripleStatement tripleStatement = null;
		
		if (isLocalFileReference()) {
			tripleStatement = RdfElementFactory.createStorageFileStatement(uuidOfInformationPackage, uuidOfFile);
		} else {
			tripleStatement = RdfElementFactory.createRemoteFileStatement(uuidOfInformationPackage, url);
		}
		
		return tripleStatement;
	}
	
	/**
	 * An enum for the type of the reference. It can be a reference to a file that is saved in the storage
	 * or a reference to a remote file via http. An enum instead a boolean value is used because there may be
	 * an extension in the future. 
	 * @author Christopher Olbertz
	 *
	 */
	private enum ReferenceType {
		FILE, REMOTE;
	}
}
