package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;

public class IriTest {
	/**
	 * Tests the constructor of the class IriImpl with an String that contains a valid
	 * iri.
	 */
	@Test
	public void testConstructor_withString_validIri() {
		final String namespaceAsString = "http://my.namespace.de";
		final String localNameAsString = "myLocalName";
		final String iriAsString = namespaceAsString + "#" + localNameAsString;
		final Iri iri = new IriImpl(iriAsString);
		
		final Namespace expectedNamespace = ServerModelFactory.createNamespace(namespaceAsString);
		final LocalName expectedLocalName = ServerModelFactory.createLocalName(localNameAsString);
		final Namespace actualNamespace = iri.getNamespace();
		final LocalName actualLocalName = iri.getLocalName();
		
		assertThat(actualNamespace, is(equalTo(expectedNamespace)));
		assertThat(actualLocalName, is(equalTo(expectedLocalName)));
	}
	
	/**
	 * Tests the constructor of the class IriImpl with an String that contains not a valid
	 * iri.
	 */
	@Test
	public void testConstructor_withString_notValidIri() {
		final String localNameAsString = "myLocalName";
		final Iri iri = new IriImpl(localNameAsString);
		
		final Namespace expectedNamespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
		final LocalName expectedLocalName = ServerModelFactory.createLocalName(localNameAsString);
		final Namespace actualNamespace = iri.getNamespace();
		final LocalName actualLocalName = iri.getLocalName();
		
		assertThat(actualNamespace, is(equalTo(expectedNamespace)));
		assertThat(actualLocalName, is(equalTo(expectedLocalName)));
	}
	
	/**
	 * Tests the constructor of the class IriImpl with an uuid.
	 */
	@Test
	public void testConstructor_withUuid() {
		final Uuid uuid = ServerModelFactory.createUuid();
		final Iri iri = new IriImpl(uuid);
		
		final Namespace expectedNamespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
		final LocalName expectedLocalName = ServerModelFactory.createLocalName(uuid.toString());
		final Namespace actualNamespace = iri.getNamespace();
		final LocalName actualLocalName = iri.getLocalName();
		
		assertThat(actualNamespace, is(equalTo(expectedNamespace)));
		assertThat(actualLocalName, is(equalTo(expectedLocalName)));
	}
}
