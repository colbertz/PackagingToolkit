package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;

/**
 * Tests the most important methods of TripleSubject.
 * @author Christopher Olbertz
 *
 */
public class TripleSubjectTest {
	@Test
	public void testConstructor_withString() {
		final Namespace namespace = ServerModelFactory.createNamespace("http://my.namespace.de");
		final LocalName localname = ServerModelFactory.createLocalName("mysubject");
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TripleSubject tripleSubject = new TripleSubjectImpl(iri);
		
		assertThat(tripleSubject.getNamespace(), is(equalTo(namespace)));
		assertThat(tripleSubject.getLocalName(), is(equalTo(localname)));
	} 
	
	@Test
	public void testConstructor_withUuidStringAndString() {
		final Uuid uuid = new Uuid();
		final Namespace namespace = ServerModelFactory.createNamespace("http://my.namespace.de");
		final LocalName localname = ServerModelFactory.createLocalName("mysubject");
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TripleSubject tripleSubject = new TripleSubjectImpl(uuid, namespace, localname);
		
		assertThat(tripleSubject.getNamespace(), is(equalTo(namespace)));
		assertThat(tripleSubject.getLocalName(), is(equalTo(localname)));
		assertThat(tripleSubject.getIri().toString(), is(equalTo(iri)));
		assertNotNull(tripleSubject.getUuid());
	}
}
