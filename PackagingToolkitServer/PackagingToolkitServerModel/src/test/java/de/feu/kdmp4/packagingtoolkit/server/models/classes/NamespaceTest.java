package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;

public class NamespaceTest {
	@Test
	public void testIsNotEmpty_emptyNamespace() {
		final Namespace namespace = new NamespaceImpl("");
		final boolean isNotEmpty = namespace.isNotEmpty();
		assertTrue(isNotEmpty);
	}
	
	@Test
	public void testIsNotEmpty_notEmptyNamespace() {
		final Namespace namespace = new NamespaceImpl("my.namespace.de");
		final boolean isNotEmpty = namespace.isNotEmpty();
		assertFalse(isNotEmpty);
	}
}
