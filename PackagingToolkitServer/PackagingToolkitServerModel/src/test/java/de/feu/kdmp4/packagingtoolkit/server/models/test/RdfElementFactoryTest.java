package de.feu.kdmp4.packagingtoolkit.server.models.test;

import static org.junit.Assert.*;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;

public class RdfElementFactoryTest {

	@Test
	public void testIsTypePredicate_resultTrue() {
		String predicateIri = "http://www.w3.org/1999/02/22-rdf-syntax-ns" + "#" + "type";
		
		boolean isTypePredicate = RdfElementFactory.isTypePredicate(predicateIri);
		assertTrue(isTypePredicate);
	}

	@Test
	public void testIsTypePredicate_resultFalse() {
		String predicateIri = "http://www.w3.org/1999/02/22-rdf-syntax-ns" + "#" + "notType";
		
		boolean isTypePredicate = RdfElementFactory.isTypePredicate(predicateIri);
		assertFalse(isTypePredicate);
	}
	
	@Test
	public void testIsAggregatedByPredicate_resultTrue() {
		String predicateIri = "http://de.feu.kdmp4.pkgtkt" + "#" + "aggregatedBy";
		
		boolean isAggregatedByPredicate = RdfElementFactory.isAggregatedByPredicate(predicateIri);
		assertTrue(isAggregatedByPredicate);
	}

	@Test
	public void testIsAggegatedByPredicate_resultFalse() {
		String predicateIri = "http://de.feu.kdmp4.pkgtkt" + "#" + "notAggregatedBy";
		
		boolean isAggregatedByPredicate = RdfElementFactory.isAggregatedByPredicate(predicateIri);
		assertFalse(isAggregatedByPredicate);
	}
}
