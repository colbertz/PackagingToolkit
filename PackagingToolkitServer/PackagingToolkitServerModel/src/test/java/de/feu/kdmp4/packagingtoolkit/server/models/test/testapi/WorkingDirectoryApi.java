package de.feu.kdmp4.packagingtoolkit.server.models.test.testapi;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
 
/**
 * Contains some convenient methods for testing the class 
 * {@link de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory}.
 * @author Christopher Olbertz
 *
 */
public class WorkingDirectoryApi {
	private static final String FILENAME = "theFile";
	
	private Uuid uuid = new Uuid();
	
	//private TestDirectories testDirectories;
	private WorkingDirectory workingDirectory;
	
	public WorkingDirectoryApi(String parentDirectory) {
		workingDirectory = new WorkingDirectoryImpl(parentDirectory);
		//testDirectories = new TestDirectories(workingDirectory);
	}
	
	public String getWorkingDirectoryPath() {
		return workingDirectory.getWorkingDirectoryPath();
	}

	public File getConfigurationDirectory() {
		File expectedConfigurationDirectory = workingDirectory.getConfigurationDirectory();
		if (!expectedConfigurationDirectory.exists()) {
			expectedConfigurationDirectory.mkdirs();
		}
		return expectedConfigurationDirectory;
	}
	
	public WorkingDirectory getWorkingDirectory() {
		return workingDirectory;
	}
	
	public File getTemporaryDirectory() {
		File temporaryDirectory = workingDirectory.getTemporaryDirectory();
		if (!temporaryDirectory.exists()) {
			temporaryDirectory.mkdirs();
		}
		
		return temporaryDirectory;
	}
	
	public String getTestFileName() {
		return FILENAME;
	}
	
	public File getExpectedFileInStorage() {
		File storageDirectory = workingDirectory.getStorageDirectory();
		return new File(storageDirectory, FILENAME);
	}
	
	public Uuid getTestUuid() {
		return uuid;
	}
	
	public File getFileInNotExistingTemporaryDirectoryOfUuid() {
		Uuid uuid = getTestUuid();
		File temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(uuid);
		if (temporaryDirectory.exists()) {
			temporaryDirectory.delete();
		}
		return new File(temporaryDirectory, FILENAME);
	}
	
	public File getFileInTemporaryDirectoryOfUuid() {
		Uuid uuid = getTestUuid();
		File temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(uuid);
		return new File(temporaryDirectory, FILENAME);
	}
	
	public File getNotExistingTemporaryDirectoryOfUuid() {
		Uuid uuid = getTestUuid();
		File temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(uuid);
		if (temporaryDirectory.exists()) {
			temporaryDirectory.delete();
		}
		return temporaryDirectory;
	}
	
	public File getTemporaryDirectoryForUuid() {
		File temporaryDirectory = getTemporaryDirectory();
		File temporaryDirectoryForUuid = new File(temporaryDirectory, uuid.toString());
		return temporaryDirectoryForUuid;
	}
	
	public File getNotExistingTemporaryDirectoryForUuid() {
		File temporaryDirectory = getTemporaryDirectory();
		File temporaryDirectoryForUuid = new File(temporaryDirectory, uuid.toString());
		return temporaryDirectoryForUuid;
	}
	
	
	public File getDataDirectory() {
		return workingDirectory.getDataDirectory();
	}
	
	public File getNotExistingDataDirectory() {
		File dataDirectory = workingDirectory.getDataDirectory();
		if (dataDirectory.exists()) {
			dataDirectory.delete();
		}
		return dataDirectory;
	}

	public File getNotExistingTemporaryDirectory() {
		File temporaryDirectory = workingDirectory.getTemporaryDirectory();
		if (temporaryDirectory.exists()) {
			temporaryDirectory.delete();
		}
		return temporaryDirectory;
	}
	
	/**
	 * A helper class that contains the directories for the tests. 
	 * @author Christopher Olbertz
	 *
	 */
	/*private class TestDirectories {
		private static final String FILENAME = "theFile";
		private static final String DATA_DIRECTORY = "data";
		private static final String TEMPORARY_DIRECTORY = "temp";
		private static final String STORAGE_DIRECTORY = "storage";
		
		private Uuid uuid = new Uuid();
		private WorkingDirectory workingDirectory;
		
		
		public TestDirectories(WorkingDirectory workingDirectory) {
			this.workingDirectory = workingDirectory;
			File temporaryDirectory = getTempDirectory();
			temporaryDirectory.mkdirs();
			File configurationDirectory = getConfigurationDirectory();
			configurationDirectory.mkdirs();
			File dataDirectory = getDataDirectory();
			dataDirectory.mkdirs();
			File storageDirectory = getStorageDirectory();
		}
		
		private File getTemporaryDirectory() {
			return new File(workingDirectory. + File.separator + TEMPORARY_DIRECTORY);
		}
		
		private File getDataDirectory() {
			return new File(WORKING_DIRECTORY + File.separator + "data");
		}
		
		private File getConfigurationDirectory() {
			return new File(WORKING_DIRECTORY + File.separator + "config" + File.separator);
		}
		
		private File getStorageDirectory() {
			return new File(WORKING_DIRECTORY + File.separator + "data");
		}
		
		private File getTempFile() {
			String tempDirectory = getTempDirectoryForUuid().getAbsolutePath();
			return new File(tempDirectory + File.separator + FILENAME);
		}
		
		private File getTempDirectory() {
			String workingDirectory = getWorkingDirectory().getAbsolutePath();
			return new File(workingDirectory + File.separator + "temp");
		}
		
		private File getTempDirectoryForUuid() {
			String workingDirectory = getWorkingDirectory().getAbsolutePath();
			return new File(workingDirectory + File.separator + "temp" + File.separator + uuid);
		}
	}*/
}
