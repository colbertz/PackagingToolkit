package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.test.testapi.DigitalObjectTestApi;

/**
 * Tests the methods of the collection for digital objects.
 * @author Christopher Olbertz
 *
 */
public class DigitalObjectCollectionTest {
	
	/**
	 * Tests the construction of a collection with an arraylist as parameter.
	 */
	@Test
	public void testConstructor_withArrayList() {
		final List<DigitalObject> digitalObjectList = DigitalObjectTestApi.getDigitalObjectList();
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl(digitalObjectList);
		final boolean valuesEqual = DigitalObjectTestApi.compareWithTestList(digitalObjectCollection);
		assertTrue(valuesEqual);
	}
	
	/**
	 * Tests the construction of a collection with a Java collection as parameter.
	 */
	@Test
	public void testConstructor_withCollection() {
		final Collection<DigitalObject> digitalObjects = DigitalObjectTestApi.createCollectionWithThreeDigitalObjects();
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl(digitalObjects);
		final boolean valuesEqual = DigitalObjectTestApi.compareWithTestList(digitalObjectCollection);
		assertTrue(valuesEqual);
	}
	
	/**
	 * Tests the insertion of digital objects with three digital objects.
	 */
	@Test
	public void testAddDigitalObject() {
		final List<DigitalObject> digitalObjectList = DigitalObjectTestApi.getDigitalObjectList();
		int expectedDigitalObjectCount = 1;
		
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl();
		
		for (DigitalObject digitalObject: digitalObjectList) {
			digitalObjectCollection.addDigitalObject(digitalObject);
			int actualDigitalObjectCount = digitalObjectCollection.getDigitalObjectCount();
			assertThat(actualDigitalObjectCount, is(equalTo(expectedDigitalObjectCount)));
			
			final int index = expectedDigitalObjectCount - 1;
			assertThat(digitalObject, is(equalTo(digitalObjectList.get(index))));
			expectedDigitalObjectCount++;
		}
	}
	
	/**
	 * Tests the deletion of digital objects. Starts with the last digital object in the collection and removes them one after one.
	 */
	@Test
	public void testRemoveDigitalObject() {
		final List<DigitalObject> digitalObjectList = DigitalObjectTestApi.getDigitalObjectList();
		int expectedDigitalObjectCount = digitalObjectList.size() - 1;
		
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl(digitalObjectList);
		
		for (int i = digitalObjectCollection.getDigitalObjectCount() - 1; i >= 0; i--) {
			DigitalObject digitalObject = digitalObjectCollection.getDigitalObjectAt(i);
			digitalObjectCollection.deleteDigitalObject(digitalObject);
			int actualDigitalObjectCount = digitalObjectCollection.getDigitalObjectCount();
			assertThat(actualDigitalObjectCount, is(equalTo(expectedDigitalObjectCount)));
			expectedDigitalObjectCount--;
		}
	}
	
	/**
	 * Tests if an empty list is recognized as empty.
	 */
	@Test
	public void testIsEmpty_resultTrue() {
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl();
		assertTrue(digitalObjectCollection.isEmpty());
	}
	
	/**
	 * Tests if a not empty list is recognized as not empty.
	 */
	@Test
	public void testIsEmpty_resultFalse() {
		final List<DigitalObject> digitalObjectList = DigitalObjectTestApi.getDigitalObjectList();
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl(digitalObjectList);
		assertFalse(digitalObjectCollection.isEmpty());
	}
	
	/**
	 * Tests if the number of digital objects contained in the collection is correctly count.
	 */
	@Test
	public void testCountDigitalObjects() {
		final List<DigitalObject> digitalObjectList = DigitalObjectTestApi.getDigitalObjectList();
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl(digitalObjectList);
		assertThat(digitalObjectCollection.getDigitalObjectCount(), is(equalTo(3)));
	}
	
	/**
	 * Tests if a digital object at a given index is correctly returned.
	 */
	@Test
	public void testGetDigitalObject() {
		final List<DigitalObject> digitalObjectList = DigitalObjectTestApi.getDigitalObjectList();
		final DigitalObjectCollection digitalObjectCollection = new DigitalObjectCollectionImpl(digitalObjectList);
		final int index = 1;
		final DigitalObject actualDigitalObject = digitalObjectCollection.getDigitalObjectAt(index);
		final DigitalObject expectedDigitalObject = digitalObjectList.get(index);
		
		assertThat(actualDigitalObject, is(equalTo(expectedDigitalObject)));
	}
}
