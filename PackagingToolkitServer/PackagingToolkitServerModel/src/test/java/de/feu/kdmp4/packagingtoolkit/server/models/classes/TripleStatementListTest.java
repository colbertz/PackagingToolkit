package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;

public class TripleStatementListTest {
	// *****************************
	// testAddTripleStatement()
	// *****************************
	/**
	 * Tests the method addTripleStatement. Three statements are added to the list. After
	 * every addition the list is checked.
	 */
	@Test
	public void testAddTripleStatement() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		testAddTripleStatement_FirstTripleStatement(tripleStatementList);
		testAddTripleStatement_SecondTripleStatement(tripleStatementList);
		testAddTripleStatement_ThirdTripleStatement(tripleStatementList);
	}

	/**
	 * Adds the first statement to the list and tests if the list has the size 1 and if the last element is the inserted element.
	 * @param tripleStatementList The list for the test.
	 */
	private void testAddTripleStatement_FirstTripleStatement(TripleStatementCollection tripleStatementList) {
		TripleStatement tripleStatement = new TripleStatementImpl(new TripleSubjectImpl("a"), new TriplePredicateImpl("b"), 
							new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement);
		int actualSize = tripleStatementList.getTripleStatementsCount();
		TripleStatement actualStatement = tripleStatementList.getTripleStatement(0);
		assertThat(actualSize, is(equalTo(1)));
		assertThat(actualStatement, is(equalTo(tripleStatement)));
	}
	
	/**
	 * Adds the second statement to the list and tests if the list has the size 21 and if the last element is the inserted element.
	 * @param tripleStatementList The list for the test.
	 */
	private void testAddTripleStatement_SecondTripleStatement(TripleStatementCollection tripleStatementList) {
		TripleStatement tripleStatement = new TripleStatementImpl(new TripleSubjectImpl("b"), new TriplePredicateImpl("c"), 
							new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement);
		int actualSize = tripleStatementList.getTripleStatementsCount();
		TripleStatement actualStatement = tripleStatementList.getTripleStatement(1);
		assertThat(actualSize, is(equalTo(2)));
		assertThat(actualStatement, is(equalTo(tripleStatement)));
	}	
	
	/**
	 * Adds the third statement to the list and tests if the list has the size 3 and if the last element is the inserted element.
	 * @param tripleStatementList The list for the test.
	 */
	private void testAddTripleStatement_ThirdTripleStatement(TripleStatementCollection tripleStatementList) {
		TripleStatement tripleStatement = new TripleStatementImpl(new TripleSubjectImpl("c"), new TriplePredicateImpl("d"), 
							new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement);
		int actualSize = tripleStatementList.getTripleStatementsCount();
		TripleStatement actualStatement = tripleStatementList.getTripleStatement(2);
		assertThat(actualSize, is(equalTo(3)));
		assertThat(actualStatement, is(equalTo(tripleStatement)));
	}	
	
	// *****************************
	// testGetTripleStatement()
	// *****************************
	/**
	 * Tests the method getTripleStatement(). Three statements are inserted into the list, then the 
	 * method getTripleStatement is tested for all three statements.
	 */
	@Test
	public void testGetTripleStatement() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		List<TripleStatement> actualTripleStatements = prepareTestGetTripleStatement_addStatementsToList(tripleStatementList);
		TripleStatement actualTripleStatement1 = tripleStatementList.getTripleStatement(0);
		TripleStatement expectedTripleStatement1 = actualTripleStatements.get(0);
		assertThat(actualTripleStatement1, is(equalTo(expectedTripleStatement1)));
		
		TripleStatement actualTripleStatement2 = tripleStatementList.getTripleStatement(1);
		TripleStatement expectedTripleStatement2 = actualTripleStatements.get(1);
		assertThat(actualTripleStatement2, is(equalTo(expectedTripleStatement2)));
		
		TripleStatement actualTripleStatement3 = tripleStatementList.getTripleStatement(2);
		TripleStatement expectedTripleStatement3 = actualTripleStatements.get(2);
		assertThat(actualTripleStatement3, is(equalTo(expectedTripleStatement3)));
	}
	
	/**
	 * Adds three statements to the list.
	 * @param tripleStatementList The list the statements are added to. This list is checked in the tests.
	 * @return An ArrayList 
	 */
	private List<TripleStatement> prepareTestGetTripleStatement_addStatementsToList(TripleStatementCollection tripleStatementList) {
		List<TripleStatement> tripleStatements = new ArrayList<>();
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), new TriplePredicateImpl("b"), 
				new TripleObjectImpl("c"));
		tripleStatements.add(tripleStatement1);
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("b"), new TriplePredicateImpl("c"), 
				new TripleObjectImpl("d"));
		tripleStatements.add(tripleStatement2);
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), new TriplePredicateImpl("d"), 
				new TripleObjectImpl("e"));
		tripleStatements.add(tripleStatement3);
		tripleStatementList.addTripleStatement(tripleStatement3);
		
		return tripleStatements;
	}
	
	// *****************************
	// testAddStatements()
	// *****************************
	/**
	 * Tests the method addStatements(). A TripleStatementList with three statements is added to a list with three statements.
	 * The the result is checked.
	 */
	@Test
	public void testAddStatements() {
		TripleStatementCollection firstTripleStatementList = prepareTestAddStatements_addTestStatementsToList();
		TripleStatementCollection secondTripleStatementList = prepareTestAddStatements_addTestStatementsToList();
		
		firstTripleStatementList.addStatements(secondTripleStatementList);
		
		checkTestAddStatements(firstTripleStatementList, secondTripleStatementList);
	}
	
	/**
	 * Adds three statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTestAddStatements_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), new TriplePredicateImpl("b"), 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("b"), new TriplePredicateImpl("c"), 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), new TriplePredicateImpl("d"), 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);
		
		return tripleStatementList;
	}
	
	/**
	 * Checks if the elements of the second list where added to the first list.
	 * @param firstTripleStatementList The list the elements should have be added to.
	 * @param secondTripleStatementList This list contains the elements that should have be added to the first list.
	 */
	private void checkTestAddStatements(TripleStatementCollection firstTripleStatementList, TripleStatementCollection secondTripleStatementList) {
		int actualSize = firstTripleStatementList.getTripleStatementsCount();
		assertThat(actualSize, is(equalTo(6)));
		
		TripleStatement actualTripleStatement1 = firstTripleStatementList.getTripleStatement(3);
		TripleStatement expectedTripleStatement1 = secondTripleStatementList.getTripleStatement(0);
		assertThat(actualTripleStatement1, is(equalTo(expectedTripleStatement1)));
		
		TripleStatement actualTripleStatement2 = firstTripleStatementList.getTripleStatement(4);
		TripleStatement expectedTripleStatement2 = secondTripleStatementList.getTripleStatement(1);
		assertThat(actualTripleStatement2, is(equalTo(expectedTripleStatement2)));
		
		TripleStatement actualTripleStatement3 = firstTripleStatementList.getTripleStatement(5);
		TripleStatement expectedTripleStatement3 = secondTripleStatementList.getTripleStatement(2);
		assertThat(actualTripleStatement3, is(equalTo(expectedTripleStatement3)));
	}
	
	// *****************************
	// testNextLiteralStatement()
	// *****************************
	/**
	 * Tests the method getNextLiteralStatement(). Three statements are inserted into the list, but only the first and
	 * the third statement are literal statements.
	 */
	@Test
	public void testGetNextLiteralStatement() {
		TripleStatementCollection tripleStatementList = prepareTestGetNextLiteralStatement_addTestStatementsToList();
		
		TripleStatement actualTripleStatement = tripleStatementList.getNextLiteralStatement();
		TripleStatement expectedTripleStatement = tripleStatementList.getTripleStatement(0);
		assertThat(actualTripleStatement, is(equalTo(expectedTripleStatement)));
		
		actualTripleStatement = tripleStatementList.getNextLiteralStatement();
		expectedTripleStatement = tripleStatementList.getTripleStatement(2);
		assertThat(actualTripleStatement, is(equalTo(expectedTripleStatement)));
	}
	
	/**
	 * Adds three statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTestGetNextLiteralStatement_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("b");
		triplePredicate1.setPredicateType(PredicateType.LITERAL);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
				
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("b"), new TriplePredicateImpl("c"), 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("d");
		triplePredicate2.setPredicateType(PredicateType.LITERAL);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), triplePredicate2, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);
		
		return tripleStatementList;
	}
	
	// *****************************
	// testHasLiteralStatement()
	// *****************************
	/**
	 * Tests the method hasLiteralStatement(). First this method is called and has to return true. Then the method
	 * nextLiteralStatement() is called. Another call of hasLiteralStatement() has to return false.  
	 */
	@Test
	public void testHasLiteralStatement() {
		TripleStatementCollection tripleStatementList = prepareTestHasLiteralStatement_addTestStatementsToList();
		boolean actualHasLiteralStatement = tripleStatementList.hasNextLiteralStatement();
		assertTrue(actualHasLiteralStatement);
		
		tripleStatementList.getNextLiteralStatement();
		actualHasLiteralStatement = tripleStatementList.hasNextLiteralStatement();
		assertFalse(actualHasLiteralStatement);
	}
	
	/**
	 * Adds three statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTestHasLiteralStatement_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("b");
		triplePredicate1.setPredicateType(PredicateType.LITERAL);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
				
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("b"), new TriplePredicateImpl("c"), 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("d");
		triplePredicate2.setPredicateType(PredicateType.OBJECT_PROPERTY);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), triplePredicate2, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);
		
		return tripleStatementList;
	}
	
	// *****************************
	// testHasLiteralStatement_WithNoLiteralStatements()
	// *****************************
	/**
	 * Tests the method hasLiteralStatement(), but the list does not contain any literal statements. Therefore the result has to be
	 * false.  
	 */
	@Test
	public void testHasLiteralStatement_WithNoLiteralStatements() {
		TripleStatementCollection tripleStatementList = prepareTestHasLiteralStatement_WithNoLiteralStatements_addTestStatementsToList();
		boolean actualHasLiteralStatement = tripleStatementList.hasNextLiteralStatement();
		assertFalse(actualHasLiteralStatement);
	}
	
	/**
	 * Adds three statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTestHasLiteralStatement_WithNoLiteralStatements_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("b");
		triplePredicate1.setPredicateType(PredicateType.TYPE);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("c");
		triplePredicate2.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("b"), triplePredicate2, 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate3 = new TriplePredicateImpl("d");
		triplePredicate3.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), triplePredicate3, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);
		
		return tripleStatementList;
	}
	

	// *****************************
	// testNextLiteralStatement()
	// *****************************
	/**
	 * Tests the method getNextLiteralStatement(), but the list does not contain any literal statements. Therefore the result has to be
	 * null.  
	 */
	@Test
	public void testNextLiteralStatement_WithNoLiteralStatements() {
		TripleStatementCollection tripleStatementList = prepareTestNextLiteralStatement_WithNoLiteralStatements_addTestStatementsToList();
		
		TripleStatement actualTripleStatement = tripleStatementList.getNextLiteralStatement();
		assertNull(actualTripleStatement);
	}
	
	/**
	 * Adds three statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTestNextLiteralStatement_WithNoLiteralStatements_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("b");
		triplePredicate1.setPredicateType(PredicateType.TYPE);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("c");
		triplePredicate2.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("b"), triplePredicate2, 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate3 = new TriplePredicateImpl("d");
		triplePredicate3.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), triplePredicate3, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);
		
		return tripleStatementList;
	}
	
	// *****************************
	// testNextLiteralStatement_hasNext_before_next()
	// *****************************
	/**
	 * Tests the method getNextLiteralStatement(), but first the method hasNext() is called. Three statements are inserted into the list, but only the first and
	 * the third statement are literal statements.
	 */
	@Test
	public void testNextLiteralStatement_hasNext_before_next() {
		TripleStatementCollection tripleStatementList = prepareTestGetNextLiteralStatement_addTestStatementsToList();
		
		if (tripleStatementList.hasNextLiteralStatement()) {
			TripleStatement actualTripleStatement = tripleStatementList.getNextLiteralStatement();
			TripleStatement expectedTripleStatement = tripleStatementList.getTripleStatement(0);
			assertThat(actualTripleStatement, is(equalTo(expectedTripleStatement)));
			
			actualTripleStatement = tripleStatementList.getNextLiteralStatement();
			expectedTripleStatement = tripleStatementList.getTripleStatement(2);
			assertThat(actualTripleStatement, is(equalTo(expectedTripleStatement)));
		}
	}
	
	// *****************************
	// testNextStatementOfIndividual()
	// *****************************
	/**
	 * Tests the iterator that iterates through the statements with one given individual as subject. In the list
	 * with the test data, there a five statements and three suitable statements. The test calls the method
	 * getNextStatementOfIndividual() four times. The first three times, the result may not be null. 
	 */
	@Test
	public void testNextStatementOfIndividual() {
		TripleStatementCollection tripleStatementList = prepareTest_testNextStatementOfIndividual_addTestStatementsToList();
		
		TripleStatement tripleStatement = tripleStatementList.getNextStatementOfIndividual("a");
		assertNotNull(tripleStatement);
		tripleStatement = tripleStatementList.getNextStatementOfIndividual("a");
		assertNotNull(tripleStatement);
		tripleStatement = tripleStatementList.getNextStatementOfIndividual("a");
		assertNotNull(tripleStatement);
		tripleStatement = tripleStatementList.getNextStatementOfIndividual("a");
		assertNull(tripleStatement);
	}
	
	/**
	 * Adds five statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTest_testNextStatementOfIndividual_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("b");
		triplePredicate1.setPredicateType(PredicateType.TYPE);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("c");
		triplePredicate2.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate2, 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate3 = new TriplePredicateImpl("d");
		triplePredicate3.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), triplePredicate3, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);

		TriplePredicate triplePredicate4 = new TriplePredicateImpl("e");
		triplePredicate4.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement4 = new TripleStatementImpl(new TripleSubjectImpl("x"), triplePredicate4, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement4);
		
		TriplePredicate triplePredicate5 = new TriplePredicateImpl("f");
		triplePredicate5.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement5 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate5, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement5);
		
		return tripleStatementList;
	}
	
	// *****************************
	// testHasNextStatementOfIndividual()
	// *****************************
	/**
	 * Tests the iterator that iterates through the statements with one given individual as subject. In the list
	 * with the test data, there a five statements and three suitable statements. The test calls the method
	 * hasNextStatementOfIndividual() four times. The first three times, the result must be true. 
	 */
	@Test
	public void testHasNextStatementOfIndividual() {
		TripleStatementCollection tripleStatementList = prepareTest_testHasNextStatementOfIndividual_addTestStatementsToList();
		
		boolean hasStatement = tripleStatementList.hasNextStatementOfIndividual("a");
		assertTrue(hasStatement);
		hasStatement = tripleStatementList.hasNextStatementOfIndividual("a");
		assertTrue(hasStatement);
		hasStatement = tripleStatementList.hasNextStatementOfIndividual("a");
		assertTrue(hasStatement);
		hasStatement = tripleStatementList.hasNextStatementOfIndividual("a");
		assertFalse(hasStatement);
	}
	
	/**
	 * Adds five statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTest_testHasNextStatementOfIndividual_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("b");
		triplePredicate1.setPredicateType(PredicateType.TYPE);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("c");
		triplePredicate2.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate2, 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate3 = new TriplePredicateImpl("d");
		triplePredicate3.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("c"), triplePredicate3, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);

		TriplePredicate triplePredicate4 = new TriplePredicateImpl("e");
		triplePredicate4.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement4 = new TripleStatementImpl(new TripleSubjectImpl("x"), triplePredicate4, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement4);
		
		TriplePredicate triplePredicate5 = new TriplePredicateImpl("f");
		triplePredicate5.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement5 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate5, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement5);
		
		return tripleStatementList;
	}
	
	// *****************************
	// testNextStatementOfIndividual_withChangeOfIndividual()
	// *****************************
	/**
	 * Tests the iterator that iterates through the statements with one given individual as subject. In the list
	 * with the test data, there a five statements. In the second step of the test, there is another iri for
	 * the individual given than in the first step. The iterator has to recognize that and to start over with
	 * the new iri. The first three calls of the method under test have to return an other value than null. 
	 */
	@Test
	public void testNextStatementOfIndividual_withChangeOfIndividual() {
		TripleStatementCollection tripleStatementList = prepareTest_testNextStatementOfIndividual_withChangeOfIndividual_addTestStatementsToList();
		
		TripleStatement tripleStatement = tripleStatementList.getNextStatementOfIndividual("a");
		assertNotNull(tripleStatement);
		tripleStatement = tripleStatementList.getNextStatementOfIndividual("b");
		assertNotNull(tripleStatement);
		tripleStatement = tripleStatementList.getNextStatementOfIndividual("b");
		assertNotNull(tripleStatement);
		tripleStatement = tripleStatementList.getNextStatementOfIndividual("b");
		assertNull(tripleStatement);
	}
	
	/**
	 * Adds five statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTest_testNextStatementOfIndividual_withChangeOfIndividual_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("a");
		triplePredicate1.setPredicateType(PredicateType.TYPE);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("b"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("c");
		triplePredicate2.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate2, 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate3 = new TriplePredicateImpl("d");
		triplePredicate3.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("b"), triplePredicate3, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);

		TriplePredicate triplePredicate4 = new TriplePredicateImpl("e");
		triplePredicate4.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement4 = new TripleStatementImpl(new TripleSubjectImpl("x"), triplePredicate4, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement4);
		
		TriplePredicate triplePredicate5 = new TriplePredicateImpl("f");
		triplePredicate5.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement5 = new TripleStatementImpl(new TripleSubjectImpl("x"), triplePredicate5, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement5);
		
		return tripleStatementList;
	}
	
	// *****************************
	// testNextStatementOfIndividual_withChangeOfIndividual()
	// *****************************
	/**
	 * Tests the iterator that iterates through the statements with one given individual as subject. In the list
	 * with the test data, there a five statements. In the second step of the test, there is another iri for
	 * the individual given than in the first step. The iterator has to recognize that and to start over with
	 * the new iri. The first three calls of the method under test have to return true. 
	 */
	@Test
	public void testHasNextStatementOfIndividual_withChangeOfIndividual() {
		final TripleStatementCollection tripleStatementList = prepareTest_testHasNextStatementOfIndividual_withChangeOfIndividual_addTestStatementsToList();
		final Namespace defaultNamespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
		
		boolean hasStatement = tripleStatementList.hasNextStatementOfIndividual(defaultNamespace + "#a");
		assertTrue(hasStatement);
		hasStatement = tripleStatementList.hasNextStatementOfIndividual(defaultNamespace + "#b");
		assertTrue(hasStatement);
		hasStatement = tripleStatementList.hasNextStatementOfIndividual(defaultNamespace + "#b");
		assertTrue(hasStatement);
		hasStatement = tripleStatementList.hasNextStatementOfIndividual(defaultNamespace + "#b");
		assertFalse(hasStatement);
	}
	
	/**
	 * Adds five statements to a list.
	 * @return The list the statements are added to. 
	 */
	private TripleStatementCollection prepareTest_testHasNextStatementOfIndividual_withChangeOfIndividual_addTestStatementsToList() {
		TripleStatementCollection tripleStatementList = new TripleStatementCollectionImpl();
		TriplePredicate triplePredicate1 = new TriplePredicateImpl("a");
		triplePredicate1.setPredicateType(PredicateType.TYPE);
				
		TripleStatement tripleStatement1 = new TripleStatementImpl(new TripleSubjectImpl("b"), triplePredicate1, 
				new TripleObjectImpl("c"));
		tripleStatementList.addTripleStatement(tripleStatement1);
		
		TriplePredicate triplePredicate2 = new TriplePredicateImpl("c");
		triplePredicate2.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement2 = new TripleStatementImpl(new TripleSubjectImpl("a"), triplePredicate2, 
				new TripleObjectImpl("d"));
		tripleStatementList.addTripleStatement(tripleStatement2);
		
		TriplePredicate triplePredicate3 = new TriplePredicateImpl("d");
		triplePredicate3.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement3 = new TripleStatementImpl(new TripleSubjectImpl("b"), triplePredicate3, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement3);

		TriplePredicate triplePredicate4 = new TriplePredicateImpl("e");
		triplePredicate4.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement4 = new TripleStatementImpl(new TripleSubjectImpl("x"), triplePredicate4, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement4);
		
		TriplePredicate triplePredicate5 = new TriplePredicateImpl("f");
		triplePredicate5.setPredicateType(PredicateType.TYPE);
		TripleStatement tripleStatement5 = new TripleStatementImpl(new TripleSubjectImpl("x"), triplePredicate5, 
				new TripleObjectImpl("e"));
		tripleStatementList.addTripleStatement(tripleStatement5);
		
		return tripleStatementList;
	}
	
	// *****************************
	// testSortBySubject()
	// *****************************
	@Test
	public void testSortBySubject() {
		TripleStatementCollection tripleStatementList = prepateTest_testSortBySubject_statements();
		tripleStatementList = tripleStatementList.sortBySubject();
		checkResults_testSortBySubject(tripleStatementList);
	}
	
	private TripleStatementCollection prepateTest_testSortBySubject_statements() {
		TripleStatementCollection tripleStatementList = RdfElementFactory.createEmptyTripleStatementCollection();
		
		Uuid anUuid = ServerModelFactory.createUuid();
		
		for (int  i = 0; i < 10; i++) {
			Uuid uuid = ServerModelFactory.createUuid();
			
			TripleSubject tripleSubject = null;
			if (i == 1 || i == 3 || i == 6) {
				tripleSubject = RdfElementFactory.createTripleSubject(anUuid.toString());
			} else {
				tripleSubject = RdfElementFactory.createTripleSubject(uuid.toString());
			}
			
			TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate("abc " + i, null);
			TripleObject tripleObject = RdfElementFactory.createTripleObject(i);
			TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
			tripleStatementList.addTripleStatement(tripleStatement);
		}
		
		return tripleStatementList;
	}
	
	private void checkResults_testSortBySubject(TripleStatementCollection tripleStatementList) {
		for (int i = 0; i < tripleStatementList.getTripleStatementsCount() - 1; i++) {
			TripleStatement currentTripleStatement = tripleStatementList.getTripleStatement(i);
			TripleStatement nextTripleStatement = tripleStatementList.getTripleStatement(i + 1);
			TripleSubject currentSubject = currentTripleStatement.getSubject();
			TripleSubject nextSubject = nextTripleStatement.getSubject();
			
			int twoSubjectsCompared = currentSubject.getIri().compareTo(nextSubject.getIri());
			assertThat(twoSubjectsCompared, is(lessThanOrEqualTo(1)));
		}
	}
}
