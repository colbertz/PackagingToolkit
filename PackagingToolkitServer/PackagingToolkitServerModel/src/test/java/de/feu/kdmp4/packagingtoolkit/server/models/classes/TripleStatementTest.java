package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;

/**
 * Tests the statement that are saved in the triple store.
 * @author Christopher Olbertz
 *
 */
public class TripleStatementTest {
	/**
	 * Tests if a literal statement is correctly recognized.
	 */
	@Test
	public void testIsLiteralStatement() {
		TripleSubject tripleSubject = RdfElementFactory.createTripleSubject("http://my.namespace.de#subject");
		TripleObject tripleObject = RdfElementFactory.createTripleObject(4);
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate("http://my.namespace.de#predicate", null);
		triplePredicate.setPredicateType(PredicateType.LITERAL);
		
		TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		boolean isLiteralStatement = tripleStatement.isLiteralStatement();
		assertTrue(isLiteralStatement);
		assertFalse(tripleStatement.isAggregatedByStatement());
		assertFalse(tripleStatement.isHasDatatypeStatement());
		assertFalse(tripleStatement.isReferenceStatement());
	}
	
	/**
	 * Tests if an aggregatedby statement is correctly recognized.
	 */
	@Test
	public void testIsAggregatedByStatement() {
		TripleSubject tripleSubject = RdfElementFactory.createTripleSubject("http://my.namespace.de#subject");
		TripleObject tripleObject = RdfElementFactory.createTripleObject(4);
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate("http://my.namespace.de#predicate", null);
		triplePredicate.setPredicateType(PredicateType.AGGREGATED_BY);
		
		TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		boolean isAggregatedByStatement = tripleStatement.isAggregatedByStatement();
		assertTrue(isAggregatedByStatement);
		assertFalse(tripleStatement.isLiteralStatement());
		assertFalse(tripleStatement.isHasDatatypeStatement());
		assertFalse(tripleStatement.isReferenceStatement());
	}
	
	/**
	 * Tests if a hasdatatype statement is correctly recognized.
	 */
	@Test
	public void testIsHasDatatypeStatement() {
		TripleSubject tripleSubject = RdfElementFactory.createTripleSubject("http://my.namespace.de#subject");
		TripleObject tripleObject = RdfElementFactory.createTripleObject(4);
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate("http://my.namespace.de#predicate", null);
		triplePredicate.setPredicateType(PredicateType.HAS_DATATYPE);
		
		TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		boolean isHasDatatypeStatement = tripleStatement.isHasDatatypeStatement();
		assertTrue(isHasDatatypeStatement);
		assertFalse(tripleStatement.isAggregatedByStatement());
		assertFalse(tripleStatement.isLiteralStatement());
		assertFalse(tripleStatement.isReferenceStatement());
	}
	
	/**
	 * Tests if a reference statement is correctly recognized.
	 */
	@Test
	public void testIsReferenceStatement_withLocalFile() {
		TripleSubject tripleSubject = RdfElementFactory.createTripleSubject("http://my.namespace.de#subject");
		TripleObject tripleObject = RdfElementFactory.createTripleObject(4);
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate("http://my.namespace.de#predicate", null);
		triplePredicate.setPredicateType(PredicateType.LOCAL_FILE_REFERENCE);
		
		TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		boolean isLocalFileStatement = tripleStatement.isReferenceStatement();
		assertTrue(isLocalFileStatement);
		assertFalse(tripleStatement.isAggregatedByStatement());
		assertFalse(tripleStatement.isHasDatatypeStatement());
		assertFalse(tripleStatement.isLiteralStatement());
	}
	
	/**
	 * Tests if a reference statement is correctly recognized.
	 */
	@Test
	public void testIsReferenceStatement_withRemoteFile() {
		TripleSubject tripleSubject = RdfElementFactory.createTripleSubject("http://my.namespace.de#subject");
		TripleObject tripleObject = RdfElementFactory.createTripleObject(4);
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate("http://my.namespace.de#predicate", null);
		triplePredicate.setPredicateType(PredicateType.REMOTE_FILE_REFERENCE);
		
		TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		boolean isLocalFileStatement = tripleStatement.isReferenceStatement();
		assertTrue(isLocalFileStatement);
		assertFalse(tripleStatement.isAggregatedByStatement());
		assertFalse(tripleStatement.isHasDatatypeStatement());
		assertFalse(tripleStatement.isLiteralStatement());
	}
}
