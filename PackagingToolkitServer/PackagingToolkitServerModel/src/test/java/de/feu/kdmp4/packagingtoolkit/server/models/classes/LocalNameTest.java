package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;

public class LocalNameTest {
	@Test
	public void testIsNotEmpty_emptyLocalName() {
		final LocalName localName = new LocalNameImpl("");
		final boolean isNotEmpty = localName.isNotEmpty();
		assertTrue(isNotEmpty);
	}
	
	@Test
	public void testIsNotEmpty_notEmptyLocalName() {
		final LocalName localName = new LocalNameImpl("myLocalName");
		final boolean isNotEmpty = localName.isNotEmpty();
		assertFalse(isNotEmpty);
	}
}
