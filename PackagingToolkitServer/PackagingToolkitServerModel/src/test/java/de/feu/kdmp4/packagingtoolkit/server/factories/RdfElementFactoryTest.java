package de.feu.kdmp4.packagingtoolkit.server.factories;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleObjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TripleSubjectImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;

/**
 * Contains tests for the creation of rdf elements.
 * @author Christopher Olbertz
 *
 */
public class RdfElementFactoryTest {
	/**
	 * Tests the creation of a type predicate.
	 */
	@Test
	public void testCreateTypePredicate() {
		final TriplePredicate triplePredicate = RdfElementFactory.createTypePredicate();
		final LocalName localname = triplePredicate.getLocalName();
		assertThat(localname.toString(), is(equalTo(RdfElementFactory.LOCALNAME_TYPE)));
		final Namespace namespace = triplePredicate.getNamespace();
		assertThat(namespace.toString(), is(equalTo(RdfElementFactory.NAMESPACE_RDF)));
		assertNotNull(triplePredicate.getUuid());
	}
	
	/**
	 * Tests the creation of an aggregatedBy predicate without any parameters.
	 */
	@Test
	public void testCreateAggregatedByPredicate_noParameters() {
		final TriplePredicate triplePredicate = RdfElementFactory.createAggregatedByPredicate();
		final LocalName localname = triplePredicate.getLocalName();
		assertThat(localname.toString(), is(equalTo(RdfElementFactory.LOCALNAME_AGGREGATED_BY)));
		final Namespace namespace = triplePredicate.getNamespace();
		assertThat(namespace, is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertNotNull(triplePredicate.getUuid());
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.AGGREGATED_BY)));
	}
	
	/**
	 * Tests the creation of a type statement.
	 */
	@Test
	public void testCreateTypeStatement() {
		final Uuid uuidOfIndividual = new Uuid();
		final TripleSubject tripleSubject = new TripleSubjectImpl(uuidOfIndividual);
		final String iriOfClass = "http://my.de#class";
		final TripleObject tripleObject = new TripleObjectImpl(iriOfClass);
		
		final TripleStatement tripleStatement = RdfElementFactory.createTypeStatement(tripleSubject, tripleObject);
		
		final TripleSubject actualSubject = tripleStatement.getSubject();
		assertThat(actualSubject, is(equalTo(tripleSubject)));
		final TripleObject actualObject = tripleStatement.getObject();
		assertThat(actualObject, is(equalTo(tripleObject)));
		final TriplePredicate triplePredicate = tripleStatement.getPredicate();
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.TYPE)));
	}
	
	/**
	 * Tests the creation of a hasType statement.
	 */
	@Test
	public void testCreateHasTypeStatement() {
		final Uuid uuidOfIndividual = new Uuid();
		final TripleSubject tripleSubject = new TripleSubjectImpl(uuidOfIndividual);
		final String iriOfClass = "http://my.de#class";
		final TripleObject tripleObject = new TripleObjectImpl(iriOfClass);
		
		final TripleStatement tripleStatement = RdfElementFactory.createTypeStatement(tripleSubject, tripleObject);
		
		final TripleSubject actualSubject = tripleStatement.getSubject();
		assertThat(actualSubject, is(equalTo(tripleSubject)));
		final TripleObject actualObject = tripleStatement.getObject();
		assertThat(actualObject, is(equalTo(tripleObject)));
		final TriplePredicate triplePredicate = tripleStatement.getPredicate();
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.TYPE)));
	}
	
	/**
	 * Tests the creation of a remote reference predicate.
	 */
	@Test
	public void testCreateRemoteReferencePredicate() {
		final TriplePredicate triplePredicate = RdfElementFactory.createRemoteFileReferencePredicate();
		final LocalName localname = triplePredicate.getLocalName();
		assertThat(localname.toString(), is(equalTo(RdfElementFactory.PREDICATE_REMOTE_REFERENCE)));
		final Namespace namespace = triplePredicate.getNamespace();
		assertThat(namespace, is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertNotNull(triplePredicate.getUuid());
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.REMOTE_FILE_REFERENCE)));
	}
	
	/**
	 * Tests the creation of a local reference predicate.
	 */
	@Test
	public void testCreateFileReferencePredicate() {
		final TriplePredicate triplePredicate = RdfElementFactory.createFileReferencePredicate();
		final LocalName localname = triplePredicate.getLocalName();
		assertThat(localname.toString(), is(equalTo(RdfElementFactory.PREDICATE_FILE_REFERENCE)));
		final Namespace namespace = triplePredicate.getNamespace();
		assertThat(namespace, is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertNotNull(triplePredicate.getUuid());
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.LOCAL_FILE_REFERENCE)));
	}
	
	/**
	 * Tests the creation of a has fixity predicate.
	 */
	@Test
	public void testCreateHasFixityPredicate() {
		final TriplePredicate triplePredicate = RdfElementFactory.createHasFixityPredicate();
		final LocalName localName = triplePredicate.getLocalName();
		assertThat(localName.toString(), is(equalTo(RdfElementFactory.PREDICATE_HAS_FIXITY)));
		final Namespace namespace = triplePredicate.getNamespace();
		assertThat(namespace, is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertNotNull(triplePredicate.getUuid());
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.HAS_FIXITY)));
	}
	
	/**
	 * Tests the creation of a has fixity statement.
	 */
	@Test
	public void testCreateHasFixityStatement() {
		final String uuidOfIndividual = new Uuid().toString();
		final String uuidOfFixityIndividual = new Uuid().toString();
		
		final TripleStatement tripleStatement = RdfElementFactory.createHasFixityStatement(uuidOfIndividual, uuidOfFixityIndividual);
		
		final TripleSubject tripleSubject = tripleStatement.getSubject();
		final LocalName localnameOfSubject = tripleSubject.getLocalName();
		assertThat(localnameOfSubject.toString(), is(equalTo(uuidOfIndividual)));
		final Namespace namespaceOfSubject = tripleSubject.getNamespace();
		assertThat(namespaceOfSubject, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		
		final TripleObject tripleObject = tripleStatement.getObject();
		final LocalName localnameOfObject = tripleObject.getLocalName();
		assertThat(localnameOfObject.toString(), is(equalTo(uuidOfFixityIndividual)));
		final Namespace namespaceOfObject = tripleSubject.getNamespace();
		assertThat(namespaceOfObject, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		
		final TriplePredicate triplePredicate = tripleStatement.getPredicate();
		final LocalName localnameOfPredicate = triplePredicate.getLocalName();
		assertThat(localnameOfPredicate.toString(), is(equalTo(RdfElementFactory.PREDICATE_HAS_FIXITY)));
		
		final Namespace namespaceOfPredicate = triplePredicate.getNamespace();
		assertThat(namespaceOfPredicate, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.HAS_FIXITY)));
	}
	
	/**
	 * Tests the creation of a remote reference statement.
	 */
	@Test
	public void testCreateRemoteReferenceStatement() {
		final Uuid uuidOfInformationPackage = new Uuid();
		final String urlOfFile = "http://www.any.test.server.de/file";
		
		final TripleStatement tripleStatement = RdfElementFactory.createRemoteFileStatement(uuidOfInformationPackage, urlOfFile);
		
		final TripleSubject tripleSubject = tripleStatement.getSubject();
		final LocalName localnameOfSubject = tripleSubject.getLocalName();
		assertThat(localnameOfSubject.toString(), is(equalTo(uuidOfInformationPackage.toString())));
		final Namespace namespaceOfSubject = tripleSubject.getNamespace();
		assertThat(namespaceOfSubject, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		
		final TripleObject tripleObject = tripleStatement.getObject();
		final LocalName localnameOfObject = tripleObject.getLocalName();
		assertThat(localnameOfObject.toString(), is(equalTo(urlOfFile)));
		final Namespace namespaceOfObject = tripleSubject.getNamespace();
		assertThat(namespaceOfObject, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		
		final TriplePredicate triplePredicate = tripleStatement.getPredicate();
		final LocalName localnameOfPredicate = triplePredicate.getLocalName();
		assertThat(localnameOfPredicate.toString(), is(equalTo(RdfElementFactory.PREDICATE_REMOTE_REFERENCE)));
		
		final Namespace namespaceOfPredicate = triplePredicate.getNamespace();
		assertThat(namespaceOfPredicate, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.REMOTE_FILE_REFERENCE)));
	}
	
	/**
	 * Tests the creation of a local reference statement.
	 */
	@Test
	public void testCreateLocalReferenceStatement() {
		final Uuid uuidOfInformationPackage = new Uuid();
		final Uuid uuidOfFile = new Uuid();
		
		final TripleStatement tripleStatement = RdfElementFactory.createStorageFileStatement(uuidOfInformationPackage, uuidOfFile);
		
		final TripleSubject tripleSubject = tripleStatement.getSubject();
		final LocalName localnameOfSubject = tripleSubject.getLocalName();
		assertThat(localnameOfSubject.toString(), is(equalTo(uuidOfInformationPackage.toString())));
		final Namespace namespaceOfSubject = tripleSubject.getNamespace();
		assertThat(namespaceOfSubject, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		
		final TripleObject tripleObject = tripleStatement.getObject();
		final LocalName localnameOfObject = tripleObject.getLocalName();
		assertThat(localnameOfObject.toString(), is(equalTo(uuidOfFile.toString())));
		final Namespace namespaceOfObject = tripleSubject.getNamespace();
		assertThat(namespaceOfObject, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		
		final TriplePredicate triplePredicate = tripleStatement.getPredicate();
		final LocalName localnameOfPredicate = triplePredicate.getLocalName();
		assertEquals(RdfElementFactory.PREDICATE_FILE_REFERENCE, localnameOfPredicate.toString());
		
		final Namespace namespaceOfPredicate = triplePredicate.getNamespace();
		assertThat(namespaceOfPredicate, is(RdfElementFactory.getPackagingToolkitDefaultNamespace()));
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.LOCAL_FILE_REFERENCE)));
	}
	
	/**
	 * Tests the creation of an aggregatedBy statement.
	 */
	@Test
	public void testCreateAggregatedByStatement() {
		final Uuid uuid = new Uuid();
		final TripleObject uuidOfInformationPackage = new TripleObjectImpl(uuid);
		final Uuid uuidOfIndividual = new Uuid();
		final TripleSubject individual = new TripleSubjectImpl(uuidOfIndividual);
		
		final TripleStatement tripleStatement = RdfElementFactory.createAggregatedByStatement(individual, uuidOfInformationPackage);
		
		final TripleSubject actualSubject = tripleStatement.getSubject();
		assertThat(actualSubject, is(equalTo(individual)));
		
		final TripleObject actualObject = tripleStatement.getObject();
		assertThat(actualObject, is(equalTo(uuidOfInformationPackage)));
		
		final TriplePredicate actualPredicate = tripleStatement.getPredicate();
		final PredicateType expectededPredicateType = PredicateType.AGGREGATED_BY;
		assertThat(actualPredicate.getPredicateType(), is(equalTo(expectededPredicateType)));
	}
	
	/**
	 * Tests the creation of a has datatype predicate.
	 */
	@Test
	public void testCreateHasDatatypePredicate() {
		final TriplePredicate triplePredicate = RdfElementFactory.createHasDatatypePredicate();
		final PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.HAS_DATATYPE)));
		final Iri actualIri = triplePredicate.getIri();
		final String expectedIri = RdfElementFactory.getPackagingToolkitDefaultNamespace() + PackagingToolkitConstants.NAMESPACE_SEPARATOR
				+ RdfElementFactory.PREDICATE_HAS_DATATYPE;
		assertThat(actualIri.toString(), is(equalTo(expectedIri)));
		final Namespace namespace = triplePredicate.getNamespace();
		assertThat(namespace, is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		final LocalName localname = triplePredicate.getLocalName();
		assertThat(localname.toString(), is(equalTo(RdfElementFactory.PREDICATE_HAS_DATATYPE)));
		assertNotNull(triplePredicate.getUuid());
	}
	
	/**
	 * Tests if an iri is recognized as an iri of an aggregatedby predicate. The expected result is true.
	 */
	@Test
	public void testIsAggregatedByPredicate_resultTrue() {
		final String iri = "http://de.feu.kdmp4.pkgtkt#aggregatedBy";
		final boolean isAggregatedbyPredicate = RdfElementFactory.isAggregatedByPredicate(iri);
		assertTrue(isAggregatedbyPredicate);
	}
	
	/**
	 * Tests if an iri is recognized as an iri of an aggregatedby predicate. The expected result is false.
	 */
	@Test
	public void testIsAggregatedByPredicate_resultFalse() {
		final String iri = "http://de.feu.kdmp4.pkgtkt#ggregatedBy";
		final boolean isAggregatedbyPredicate = RdfElementFactory.isAggregatedByPredicate(iri);
		assertFalse(isAggregatedbyPredicate);
	}
	
	/**
	 * Tests if an iri is recognized as an iri of an type predicate. The expected result is true.
	 */
	@Test
	public void testIsTypePredicate_resultTrue() {
		final String iri = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
		final boolean isTypePredicate = RdfElementFactory.isTypePredicate(iri);
		assertTrue(isTypePredicate);
	}
	
	/**
	 * Tests if an iri is recognized as an iri of an type predicate. The expected result is false.
	 */
	@Test
	public void testIsTypePredicate_resultFalse() {
		final String iri = "http://www.w3.org/1999/02/22-rdf-syntax-ns#ype";
		final boolean isTypePredicate = RdfElementFactory.isTypePredicate(iri);
		assertFalse(isTypePredicate);
	}
	
	/**
	 * Tests if an aggregatedby predicate can be created.
	 */
	@Test
	public void testCreateTriplePredicate_aggregatedBy() {
		final String iri = "http://de.feu.kdmp4.pkgtkt#aggregatedBy";
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(iri, null);
		PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.AGGREGATED_BY)));
	}
	
	/**
	 * Tests if a type predicate can be created.
	 */
	@Test
	public void testCreateTriplePredicate_type() {
		final String iri = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(iri, null);
		PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.TYPE)));
	}
	
	/**
	 * Tests if a literal predicate can be created.
	 */
	@Test
	public void testCreateTriplePredicate_literal() {
		final String iri = "http://www.w3.org/1999/02/22-rdf-syntax-ns#integer";
		TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(iri, null);
		PredicateType predicateType = triplePredicate.getPredicateType();
		assertThat(predicateType, is(equalTo(PredicateType.LITERAL)));
	}
}
