package de.feu.kdmp4.packagingtoolkit.server.models.test.testapi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObjectCollection;

/**
 * Contain some method for supporting the tests of digital objects.
 * @author Christopher Olbertz
 *
 */
public class DigitalObjectTestApi {
	/**
	 * A list with digital objects for the test.
	 */
	private static List<DigitalObject> digitalObjectList;
	
	/**
	 * Initializes the test list with three digital objects.
	 */
	static {
		digitalObjectList = createThreeDigitalObjects();
	}
	
	/**
	 * Returns the list with the test objects.
	 * @return The list with the test objects.
	 */
	public static List<DigitalObject> getDigitalObjectList() {
		return digitalObjectList;
	}

	/**
	 * Creates a list with three digital objects.
	 * @return A list with three digital objects.
	 */
	private static final List<DigitalObject> createThreeDigitalObjects() {
		final DigitalObject digitalObject1 = ServerModelFactory.createDigitalObject(new Uuid(), "file1", "3826bc5d2d7219c0f787c718b61646dc");
		final DigitalObject digitalObject2 = ServerModelFactory.createDigitalObject(new Uuid(), "file2", "3826bc5d2d7219c0f787c718b61646de");
		final DigitalObject digitalObject3 = ServerModelFactory.createDigitalObject(new Uuid(), "file3", "3826bc5d2d7219c0f787c718b61646d1");
		
		final List<DigitalObject> digitalObjectList = new ArrayList<>();
		digitalObjectList.add(digitalObject1);
		digitalObjectList.add(digitalObject2);
		digitalObjectList.add(digitalObject3);
		
		return digitalObjectList;
	}
	
	/**
	 * Creates a collection with three digital objects.
	 * @return A collection with three digital objects.
	 */
	public static final Collection<DigitalObject> createCollectionWithThreeDigitalObjects() {
		final Collection<DigitalObject> digitalObjects = new ArrayList<>(digitalObjectList);
		return digitalObjects;
	}
	
	/**
	 * Compares a collection as an result of a test with the list that contains the test data.
	 * @param digitalObjectCollection The collection we want to test.
	 * @return True if the collection contains the correct value false otherwise.
	 */
	public static final boolean compareWithTestList(DigitalObjectCollection digitalObjectCollection) {
		if (digitalObjectCollection.getDigitalObjectCount() != digitalObjectList.size()) {
			return false;
		}
		
		for (int i = 0; i < digitalObjectCollection.getDigitalObjectCount(); i++) {
			DigitalObject actualDigitalObject = digitalObjectCollection.getDigitalObjectAt(i);
			DigitalObject expectedDigitalObject = digitalObjectList.get(i);
			
			if (!actualDigitalObject.equals(expectedDigitalObject)) {
				return false;
			}
		}
		
		return true;
	}
}
