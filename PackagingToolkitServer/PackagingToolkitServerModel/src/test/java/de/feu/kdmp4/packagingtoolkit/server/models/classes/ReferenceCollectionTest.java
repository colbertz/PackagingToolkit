package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.test.testapi.ReferenceTestApi;

/**
 * Tests the methods of the collection for references.
 * @author Christopher Olbertz
 *
 */
public class ReferenceCollectionTest {
	
	/**
	 * Tests the construction of a collection with an arraylist as parameter.
	 */
	@Test
	public void testConstructor_withArrayList() {
		final List<Reference> referenceList = ReferenceTestApi.getReferenceList();
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl(referenceList);
		final boolean valuesEqual = ReferenceTestApi.compareWithTestList(referenceCollection);
		assertTrue(valuesEqual);
	}
	
	/**
	 * Tests the construction of a collection with a Java collection as parameter.
	 */
	@Test
	public void testConstructor_withCollection() {
		final Collection<Reference> references = ReferenceTestApi.createCollectionWithThreeReferences();
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl(references);
		final boolean valuesEqual = ReferenceTestApi.compareWithTestList(referenceCollection);
		assertTrue(valuesEqual);
	}
	
	/**
	 * Tests the insertion of digital objects with three references.
	 */
	@Test
	public void testAddReference() {
		final List<Reference> referenceList = ReferenceTestApi.getReferenceList();
		int expectedReferenceCount = 1;
		
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl();
		
		for (Reference reference: referenceList) {
			referenceCollection.addReference(reference);
			int actualReferenceCount = referenceCollection.getReferencesCount();
			assertThat(actualReferenceCount, is(equalTo(expectedReferenceCount)));
			
			final int index = expectedReferenceCount - 1;
			assertThat(reference, is(equalTo(referenceList.get(index))));
			expectedReferenceCount++;
		}
	}
	
	/**
	 * Tests if an empty list is recognized as empty.
	 */
	@Test
	public void testIsEmpty_resultTrue() {
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl();
		assertTrue(referenceCollection.isEmpty());
	}
	
	/**
	 * Tests if a not empty list is recognized as not empty.
	 */
	@Test
	public void testIsEmpty_resultFalse() {
		final List<Reference> referenceList = ReferenceTestApi.getReferenceList();
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl(referenceList);
		assertFalse(referenceCollection.isEmpty());
	}
	
	/**
	 * Tests if an empty list is recognized as empty.
	 */
	@Test
	public void testIsNotEmpty_resultFalse() {
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl();
		assertFalse(referenceCollection.isNotEmpty());
	}
	
	/**
	 * Tests if a not empty list is recognized as not empty.
	 */
	@Test
	public void testIsNotEmpty_resultTrue() {
		final List<Reference> referenceList = ReferenceTestApi.getReferenceList();
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl(referenceList);
		assertTrue(referenceCollection.isNotEmpty());
	}
	
	/**
	 * Tests if the number of references contained in the collection is correctly count.
	 */
	@Test
	public void testCountReferences() {
		final List<Reference> referenceList = ReferenceTestApi.getReferenceList();
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl(referenceList);
		assertThat(referenceCollection.getReferencesCount(), is(equalTo(3)));
	}
	
	/**
	 * Tests if a reference at a given index is correctly returned.
	 */
	@Test
	public void testGetReference() {
		final List<Reference> referenceList = ReferenceTestApi.getReferenceList();
		final ReferenceCollection referenceCollection = new ReferenceCollectionImpl(referenceList);
		final int index = 1;
		final Reference actualReference = referenceCollection.getReference(index);
		final Reference expectedReference = referenceList.get(index);
		
		assertThat(actualReference, is(equalTo(expectedReference)));
	}
}
