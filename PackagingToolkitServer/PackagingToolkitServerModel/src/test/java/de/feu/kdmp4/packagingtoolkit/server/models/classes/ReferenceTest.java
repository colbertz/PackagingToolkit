package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;
/**
 * Tests the references.
 * @author Christopher Olbertz
 *
 */
public class ReferenceTest {
	/**
	 * Tests the creation of a reference to a local file in the storage of the server.
	 */
	@Test
	public void testCreateLocalFileReference() {
		final String url = "file1";
		final Uuid uuid = ServerModelFactory.createUuid();
		
		final Reference reference = ReferenceImpl.createLocalFileReference(url, uuid);
		
		final String actualUrl = reference.getUrl();
		assertTrue(areStringsEqual(url, actualUrl));
		final Uuid actualUuid = reference.getUuidOfFile();
		assertThat(actualUuid, is(equalTo(uuid)));
		assertTrue(reference.isLocalFileReference());
	}
	
	/**
	 * Tests the creation of a reference to a remote file on another server.
	 */
	@Test
	public void testCreateRemoteFileReference() {
		final String url = "http://my.server.de/file1";
		
		final Reference reference = ReferenceImpl.createRemoteFileReference(url);
		
		final String actualUrl = reference.getUrl();
		assertTrue(areStringsEqual(url, actualUrl));
		assertTrue(reference.isRemoteFileReference());
	}
	
	/**
	 * Tests if it is correctly recognized that a reference points to a remote file.
	 */
	@Test
	public void testIsRemoteReference() {
		final String url = "http://my.server.de/file1";
		
		final Reference reference = ReferenceImpl.createRemoteFileReference(url);
		
		assertTrue(reference.isRemoteFileReference());
		assertFalse(reference.isLocalFileReference());
	}
	
	/**
	 * Tests if it is correctly recognized that a reference points to a local file.
	 */
	@Test
	public void testIsLocalReference() {
		final String url = "file1";
		final Uuid uuid = ServerModelFactory.createUuid();
		
		final Reference reference = ReferenceImpl.createLocalFileReference(url, uuid);
		
		assertTrue(reference.isLocalFileReference());
		assertFalse(reference.isRemoteFileReference());
	}
}
