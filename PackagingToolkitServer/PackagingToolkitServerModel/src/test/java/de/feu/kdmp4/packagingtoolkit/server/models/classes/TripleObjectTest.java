package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.models.classes.OntologyDurationImpl;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;

/**
 * Tests the methods of a triple object.
 * @author Christopher Olbertz
 *
 */
public class TripleObjectTest {
	@Test
	public void testConstructor_withStringAndString() {
		final Namespace namespace = ServerModelFactory.createNamespace("http://my.namespace.de");
		final LocalName localname = ServerModelFactory.createLocalName("aLocalName");
		final TripleObject tripleObject = new TripleObjectImpl(namespace, localname);
		assertThat(tripleObject.getNamespace(), is(equalTo(namespace)));
		assertThat(tripleObject.getLocalName(), is(equalTo(localname)));
	}
	
	@Test
	public void testConstructor_withBooleanObject() {
		final boolean value = true;
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final Namespace namespace = RdfElementFactory.getPackagingToolkitDefaultNamespace();
		final LocalName localname = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(namespace)));
		assertThat(tripleObject.getLocalName(), is(equalTo(localname)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.BOOLEAN)));
	}
	
	@Test
	public void testConstructor_withShortObject() {
		final short value = 4;
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.BYTE)));
	}
	
	@Test
	public void testConstructor_withByteObject() {
		final byte value = 4;
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.BYTE)));
	}	
	
	@Test
	public void testConstructor_withDateObject() {
		final LocalDate value = LocalDate.now();
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.DATE)));
	}	
	
	@Test
	public void testConstructor_withDateTimeObject() {
		final LocalDateTime value = LocalDateTime.now();
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.DATETIME)));
	}	
	
	@Test
	public void testConstructor_withDoubleObject() {
		final double value = 4.4;
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.DOUBLE)));
	}	
	
	@Test
	public void testConstructor_withDurationObject() {
		final OntologyDuration value = new OntologyDurationImpl(1, 2, 3, 4, 5, 6);
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.DURATION)));
	}	
	
	@Test
	public void testConstructor_withFloatObject() {
		final float value = 4.4f;
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.FLOAT)));
	}	
	
	@Test
	public void testConstructor_withIntegerObject() {
		final int value = 4;
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.SHORT)));
	}	
	
	@Test
	public void testConstructor_withLongObject() {
		final long value = 4l;
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.INTEGER)));
	}	
	
	@Test
	public void testConstructor_withBigIntegerObject() {
		final BigInteger value = BigInteger.valueOf(44l);
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.LONG)));
	}	
	
	@Test
	public void testConstructor_withTimeObject() {
		final LocalTime value = LocalTime.now();
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.TIME)));
	}	
	
	@Test
	public void testConstructor_withObject() {
		final Uuid value = new Uuid();
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.OBJECT)));	
	}
	
	@Test
	public void testConstructor_withStringWithoutNamespace() {
		final Uuid uuid = new Uuid();
		final TripleObject tripleObject = new TripleObjectImpl(uuid);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(uuid));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.OBJECT)));
	}
	
	@Test
	public void testConstructor_withStringObject() {
		final String value = "abc";
		final TripleObject tripleObject = new TripleObjectImpl(value);
		final LocalName localName = ServerModelFactory.createLocalName(String.valueOf(value));
		assertThat(tripleObject.getNamespace(), is(equalTo(RdfElementFactory.getPackagingToolkitDefaultNamespace())));
		assertThat(tripleObject.getLocalName(), is(equalTo(localName)));
		assertThat(tripleObject.getDatatype(), is(equalTo(OntologyDatatype.STRING)));
	}	
	
	@Test
	public void testToString_withNamespaceAndLocalname() {
		final Namespace namespace = ServerModelFactory.createNamespace("http://my.namespace.de");
		final LocalName localname = ServerModelFactory.createLocalName("aLocalName");
		final TripleObject tripleObject = new TripleObjectImpl(namespace, localname);
		
		final String actualString = tripleObject.toString();
		final String expectedString = "http://my.namespace.de#aLocalName";
		assertThat(actualString, is(equalTo(expectedString)));
	}
}
