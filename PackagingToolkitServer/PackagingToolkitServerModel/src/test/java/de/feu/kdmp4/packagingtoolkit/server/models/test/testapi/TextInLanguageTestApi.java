package de.feu.kdmp4.packagingtoolkit.server.models.test.testapi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.server.enums.OntologyLabelLanguage;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.TextInLanguageCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TextInLanguageCollection;

/**
 * Contain some method for supporting the tests of textInLanguages.
 * @author Christopher Olbertz
 *
 */
public class TextInLanguageTestApi {
	/**
	 * A list with textInLanguages for the test.
	 */
	private static List<TextInLanguage> textInLanguageList;
	
	/**
	 * Initializes the test list with three textInLanguages.
	 */
	static {
		textInLanguageList = createThreeTextInLanguages();
	}
	
	/**
	 * Returns the list with the test objects.
	 * @return The list with the test objects.
	 */
	public static List<TextInLanguage> getTextInLanguageList() {
		return textInLanguageList;
	}

	/**
	 * Returns the value with the german text from the test list. 
	 * @return The value with the german text.
	 */
	public static TextInLanguage getGermanText() {
		return textInLanguageList.get(0);
	}
	
	/**
	 * Returns the value with the english text from the test list. 
	 * @return The value with the english text.
	 */
	public static TextInLanguage getEnglishText() {
		return textInLanguageList.get(1);
	}
	
	/**
	 * Creates a list with three textInLanguages.
	 * @return A list with three textInLanguages.
	 */
	private static final List<TextInLanguage> createThreeTextInLanguages() {
		final TextInLanguage textInLanguage1 = ServerModelFactory.createTextInLanguage("text 1", OntologyLabelLanguage.GERMAN);
		final TextInLanguage textInLanguage2 = ServerModelFactory.createTextInLanguage("text 2", OntologyLabelLanguage.ENGLISH);
		final TextInLanguage textInLanguage3 = ServerModelFactory.createTextInLanguage("text 3", OntologyLabelLanguage.DEFAULT);
		
		final List<TextInLanguage> textInLanguageList = new ArrayList<>();
		textInLanguageList.add(textInLanguage1);
		textInLanguageList.add(textInLanguage2);
		textInLanguageList.add(textInLanguage3);
		
		return textInLanguageList;
	}
	
	/**
	 * Creates a collection with three textInLanguages.
	 * @return A collection with three textInLanguages.
	 */
	public static final Collection<TextInLanguage> createCollectionWithThreeTextInLanguages() {
		final Collection<TextInLanguage> textInLanguages = new ArrayList<>(textInLanguageList);
		return textInLanguages;
	}
	
	/**
	 * Compares a collection as an result of a test with the list that contains the test data.
	 * @param textInLanguageCollection The collection we want to test.
	 * @return True if the collection contains the correct value false otherwise.
	 */
	public static final boolean compareWithTestList(TextInLanguageCollection textInLanguageCollection) {
		if (textInLanguageCollection.getTextsInLanguageCount() != textInLanguageList.size()) {
			return false;
		}
		
		for (int i = 0; i < textInLanguageCollection.getTextsInLanguageCount(); i++) {
			TextInLanguage actualTextInLanguage = textInLanguageCollection.getTextInLanguage(i);
			TextInLanguage expectedTextInLanguage = textInLanguageList.get(i);
			
			if (!actualTextInLanguage.equals(expectedTextInLanguage)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Creates a collection with some test data.
	 * @return The newly created collection.
	 */
	public static final TextInLanguageCollection createTextInLanguageCollectionWithTestData() {
		TextInLanguageCollection inLanguageCollection = new TextInLanguageCollectionImpl();
		
		for (TextInLanguage textInLanguage: textInLanguageList) {
			inLanguageCollection.addTextInLanguage(textInLanguage);
		}
		
		return inLanguageCollection;
	}
}
