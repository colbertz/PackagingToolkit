package de.feu.kdmp4.packagingtoolkit.server.exchange.classes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;

/**
 * Contains some tests of the not trivial methods.
 * @author Christopher Olbertz
 *
 */
public class InformationPackageExchangeTest {
	/**
	 * Tests if the method recognizing a sip is working. The information package we are testing is a sip.
	 */
	@Test
	public void testIsSubmissionInformationPackage_resultTrue() {
		final InformationPackageExchange informationPackageExchange = new InformationPackageExchangeImpl();
		informationPackageExchange.setPackageType(PackageType.SIP);
		assertTrue(informationPackageExchange.isSubmissionInformationPackage());
	}
	
	/**
	 * Tests if the method recognizing a sip is working. The information package we are testing is not a sip.
	 */
	@Test
	public void testIsSubmissionInformationPackage_resultFalse() {
		final InformationPackageExchange informationPackageExchange = new InformationPackageExchangeImpl();
		informationPackageExchange.setPackageType(PackageType.SIU);
		assertFalse(informationPackageExchange.isSubmissionInformationPackage());
	}
}
