package de.feu.kdmp4.packagingtoolkit.server.models.test.testapi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;

/**
 * Contain some method for supporting the tests of references.
 * @author Christopher Olbertz
 *
 */
public class ReferenceTestApi {
	/**
	 * A list with references for the test.
	 */
	private static List<Reference> referenceList;
	
	/**
	 * Initializes the test list with three references.
	 */
	static {
		referenceList = createThreeReferences();
	}
	
	/**
	 * Returns the list with the test objects.
	 * @return The list with the test objects.
	 */
	public static List<Reference> getReferenceList() {
		return referenceList;
	}

	/**
	 * Creates a list with three references.
	 * @return A list with three references.
	 */
	private static final List<Reference> createThreeReferences() {
		final Reference reference1 = ServerModelFactory.createLocalFileReference("file1", new Uuid());
		final Reference reference2 = ServerModelFactory.createRemoteReference("http://my.server.de/file");
		final Reference reference3 = ServerModelFactory.createLocalFileReference("file2", new Uuid());
		
		final List<Reference> referenceList = new ArrayList<>();
		referenceList.add(reference1);
		referenceList.add(reference2);
		referenceList.add(reference3);
		
		return referenceList;
	}
	
	/**
	 * Creates a collection with three references.
	 * @return A collection with three references.
	 */
	public static final Collection<Reference> createCollectionWithThreeReferences() {
		final Collection<Reference> references = new ArrayList<>(referenceList);
		return references;
	}
	
	/**
	 * Compares a collection as an result of a test with the list that contains the test data.
	 * @param referenceCollection The collection we want to test.
	 * @return True if the collection contains the correct value false otherwise.
	 */
	public static final boolean compareWithTestList(ReferenceCollection referenceCollection) {
		if (referenceCollection.getReferencesCount() != referenceList.size()) {
			return false;
		}
		
		for (int i = 0; i < referenceCollection.getReferencesCount(); i++) {
			Reference actualReference = referenceCollection.getReference(i);
			Reference expectedReference = referenceList.get(i);
			
			if (!actualReference.equals(expectedReference)) {
				return false;
			}
		}
		
		return true;
	}
}
