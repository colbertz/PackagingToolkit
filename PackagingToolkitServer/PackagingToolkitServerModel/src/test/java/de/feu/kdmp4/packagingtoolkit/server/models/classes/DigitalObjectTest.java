package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.DigitalObject;

/**
 * Contains some tests for the digital objects.
 * @author Christopher Olbertz
 *
 */
public class DigitalObjectTest {
	/**
	 * Tests the constructor that gets a filename and a checksum.
	 */
	@Test
	public void testConstructor_withTwoParameters() {
		final String filename = "file1";
		final String md5Checksum = "d41d8cd98f00b204e9800998ecf8427e";
		final DigitalObject digitalObject = new DigitalObjectImpl(filename, md5Checksum);
		
		assertThat(digitalObject.getFilename(), is(equalTo(filename)));
		assertThat(digitalObject.getMd5Checksum(), is(equalTo(md5Checksum)));
		assertNotNull(digitalObject.getUuid());
		assertNotNull(digitalObject.getReference());
	}
	
	/**
	 * Tests the constructor that gets an uuid, a filename and a checksum.
	 */
	@Test
	public void testConstructor_withThreeParameters() {
		final Uuid uuidOfFile = ServerModelFactory.createUuid(); 
		final String filename = "file1";
		final String md5Checksum = "d41d8cd98f00b204e9800998ecf8427e";
		final DigitalObject digitalObject = new DigitalObjectImpl(uuidOfFile, filename, md5Checksum);
		
		assertThat(digitalObject.getFilename(), is(equalTo(filename)));
		assertThat(digitalObject.getMd5Checksum(), is(equalTo(md5Checksum)));
		assertNotNull(digitalObject.getUuid());
		assertNotNull(digitalObject.getReference());
		assertThat(digitalObject.getUuid(), is(equalTo(uuidOfFile)));
	}
	
	/**
	 * Tests the equals() method. The both objects are equal.
	 */
	@Test
	public void testEquals_resultTrue() {
		final Uuid uuidOfFile = ServerModelFactory.createUuid(); 
		final String filename = "file1";
		final String md5Checksum = "d41d8cd98f00b204e9800998ecf8427e";
		final DigitalObject digitalObject1 = new DigitalObjectImpl(uuidOfFile, filename, md5Checksum);
		
		final DigitalObject digitalObject2 = new DigitalObjectImpl(uuidOfFile, filename, md5Checksum);
		final boolean equal = digitalObject1.equals(digitalObject2);
		assertTrue(equal);
	}
	
	/**
	 * Tests the equals() method. The both objects are not equal. They are different in the filename.
	 */
	@Test
	public void testEquals_resultFalse_differentFilename() {
		final Uuid uuidOfFile = ServerModelFactory.createUuid(); 
		final String filename = "file1";
		final String md5Checksum = "d41d8cd98f00b204e9800998ecf8427e";
		final DigitalObject digitalObject1 = new DigitalObjectImpl(uuidOfFile, filename, md5Checksum);
		
		final DigitalObject digitalObject2 = new DigitalObjectImpl(uuidOfFile, "file2", md5Checksum);
		final boolean equal = digitalObject1.equals(digitalObject2);
		assertFalse(equal);
	}
	
	/**
	 * Tests the equals() method. The both objects are not equal. They are different in the checksum.
	 */
	@Test
	public void testEquals_resultFalse_differentChecksum() {
		final Uuid uuidOfFile = ServerModelFactory.createUuid(); 
		final String filename = "file1";
		final String md5Checksum = "d41d8cd98f00b204e9800998ecf8427f";
		final DigitalObject digitalObject1 = new DigitalObjectImpl(uuidOfFile, filename, md5Checksum);
		
		final DigitalObject digitalObject2 = new DigitalObjectImpl(uuidOfFile, filename, "d41d8cd98f00b204e9800998ecf8427e");
		final boolean equal = digitalObject1.equals(digitalObject2);
		assertFalse(equal);
	}
	
	/**
	 * Tests the equals() method. The both objects are not equal. They are different in the uuid.
	 */
	@Test
	public void testEquals_resultFalse_differentUuid() {
		final Uuid uuidOfFile1 = ServerModelFactory.createUuid(); 
		final String filename = "file1";
		final String md5Checksum = "d41d8cd98f00b204e9800998ecf8427f";
		final DigitalObject digitalObject1 = new DigitalObjectImpl(uuidOfFile1, filename, md5Checksum);
		
		final DigitalObject digitalObject2 = new DigitalObjectImpl(new Uuid(), filename, md5Checksum);
		final boolean equal = digitalObject1.equals(digitalObject2);
		assertFalse(equal);
	}
}
