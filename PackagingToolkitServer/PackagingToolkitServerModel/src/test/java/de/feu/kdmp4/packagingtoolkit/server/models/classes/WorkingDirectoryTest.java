package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.junit.MockitoRule;

import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.models.test.testapi.WorkingDirectoryApi;

public class WorkingDirectoryTest {
	@Rule
	public MockitoRule mockRule;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private WorkingDirectoryApi workingDirectoryApi;
	
	@Before
	public void setUp() throws IOException {
		File workingDirectory = temporaryFolder.newFolder();
		String parentDirectory = workingDirectory.getAbsolutePath();
		workingDirectoryApi = new WorkingDirectoryApi(parentDirectory);
	}
	
	/**
	 * Tests the method getConfigurationDirectory() with an existing configuration directory.
	 */
	@Test
	public void testGetWorking_DirectoryExists() {
		WorkingDirectory workingDirectory =  workingDirectoryApi.getWorkingDirectory();
		File actualConfigurationDirectory = workingDirectory.getConfigurationDirectory();
		File expectedConfigurationDirectory = workingDirectory.getConfigurationDirectory();
		assertThat(actualConfigurationDirectory, is(equalTo(expectedConfigurationDirectory)));
	}
	
	/**
	 * Tests the method getConfigurationDirectory() with an configuration directory that 
	 * does not exist.
	 */
	@Test
	public void testGetWorkingDirectory_DirectoryDoesNotExist() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		File actualConfigurationDirectory = workingDirectory.getConfigurationDirectory();
		
		File expectedConfigurationDirectory = workingDirectory.getConfigurationDirectory();
		
		assertThat(actualConfigurationDirectory, is(equalTo(expectedConfigurationDirectory)));
		boolean configurationDirectoryExists = workingDirectory.getConfigurationDirectory().exists();
		assertTrue(configurationDirectoryExists);
	}
	
	/**
	 * Tests the method getFileInTempDirectory_directoryExists().
	 */
	@Test
	public void testGetFileInTempDirectory() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		File expectedTempFile = workingDirectoryApi.getFileInTemporaryDirectoryOfUuid();
		
		File actualTempFile = workingDirectory.getFileInTempDirectory(workingDirectoryApi.getTestFileName(), 
				workingDirectoryApi.getTestUuid().toString());
		
		assertThat(actualTempFile, is(equalTo(expectedTempFile)));
	}
	
	/**
	 * Tests the method getFileInTempDirectory_directoryDoesNotExist().
	 */
	@Test
	public void testGetFileInTempDirectory_directoryDoesNotExist() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		File expectedTempFile = workingDirectoryApi.getFileInNotExistingTemporaryDirectoryOfUuid();
		
		File actualTempFile = workingDirectory.getFileInTempDirectory(workingDirectoryApi.getTestFileName(), 
				workingDirectoryApi.getTestUuid().toString());
		
		assertThat(actualTempFile, is(equalTo(expectedTempFile)));
		
		boolean temporyDirectoryExists = workingDirectoryApi.getTemporaryDirectoryForUuid().exists();
		assertTrue(temporyDirectoryExists);
		assertThat(actualTempFile.getAbsolutePath(), is(equalTo(expectedTempFile.getAbsolutePath())));
	}
	
	/**
	 * Tests the method getDataDirectory() with an existing data directory.
	 */
	@Test
	public void testGetDataDirectory_DirectoryExists() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		File actualDataDirectory = workingDirectory.getDataDirectory();
		File expectedDataDirectory = workingDirectoryApi.getDataDirectory();
		assertThat(actualDataDirectory, is(equalTo(expectedDataDirectory)));
	}
	
	/**
	 * Tests the method getDataDirectory() with a data directory that 
	 * does not exist.
	 */
	@Test
	public void testGetDataDirectory_DirectoryDoesNotExist() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		File actualDataDirectory = workingDirectory.getDataDirectory();
		File expectedDataDirectory = workingDirectoryApi.getNotExistingDataDirectory();
		assertThat(actualDataDirectory, is(equalTo(expectedDataDirectory)));
		boolean dataDirectoryExists = workingDirectory.getDataDirectory().exists();
		assertTrue(dataDirectoryExists);
	}
	
	/**
	 * Tests the method getTemporaryDirectory() with an existing temporary directory.
	 */
	@Test
	public void testGetTemporaryDirectory_DirectoryExists() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		File actualTemporaryDirectory = workingDirectory.getTemporaryDirectory();
		File expectedTemporaryDirectory = workingDirectoryApi.getTemporaryDirectory();
		assertThat(actualTemporaryDirectory, is(equalTo(expectedTemporaryDirectory)));
	}
	
	/**
	 * Tests the method getTemporaryDirectory() with a temporary directory that 
	 * does not exist.
	 */
	@Test
	public void testGetTemporaryDirectory_DirectoryDoesNotExist() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory(); 
		File expectedTemporaryDirectory = workingDirectoryApi.getNotExistingTemporaryDirectory();
		
		File actualTemporaryDirectory = workingDirectory.getTemporaryDirectory();
		
		assertThat(actualTemporaryDirectory, is(equalTo(expectedTemporaryDirectory)));
		boolean temporaryDirectoryExists = actualTemporaryDirectory.exists();
		assertTrue(temporaryDirectoryExists);
	}
	
	@Test
	public void testGetWorkingDirectoryPath() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		
		String expectedWorkingDirectoryPath = workingDirectoryApi.getWorkingDirectoryPath();
		String actualWorkingDirectoryPath = workingDirectory.getWorkingDirectoryPath();
		
		assertThat(actualWorkingDirectoryPath, is(equalTo(expectedWorkingDirectoryPath)));
	}

	/**
	 * Tests the method getTemporaryDirectoryForUuid(String) with an existing temporary directory.
	 */
	@Test
	public void testGetTemporaryDirectoryForUuid_withString_DirectoryExists() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		String uuidAsString = workingDirectoryApi.getTestUuid().toString();
		File actualTemporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(uuidAsString);

		String expectedPath = workingDirectoryApi.getTemporaryDirectoryForUuid().getAbsolutePath();
		
		String actualPath = actualTemporaryDirectory.getAbsolutePath(); 
		assertThat(actualPath, is(equalTo(expectedPath)));
	}
	
	/**
	 * Tests the method getTemporaryDirectoryForUuid(Uuid) with an existing temporary directory.
	 */
	@Test
	public void testGetTemporaryDirectoryForUuid_withUuid_DirectoryExists() {
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		File actualTemporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(workingDirectoryApi.getTestUuid());
		String expectedPath = workingDirectoryApi.getTemporaryDirectoryForUuid().getAbsolutePath();
		String actualPath = actualTemporaryDirectory.getAbsolutePath(); 
		assertThat(actualPath, is(equalTo(expectedPath)));
	}
	
	public void testGetFileInStorageDirectory() {
		File expectedFile = workingDirectoryApi.getExpectedFileInStorage();
		WorkingDirectory workingDirectory = workingDirectoryApi.getWorkingDirectory();
		
		File actualFile = workingDirectory.getFileInStorageDirectory(workingDirectoryApi.getTestFileName());
		
		assertThat(actualFile, is(equalTo(expectedFile)));
	}
}
