package de.feu.kdmp4.packagingtoolkit.server.models.classes;

import org.junit.Test;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.enums.PredicateType;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;

/**
 * Tests the predicates for the triple store.
 * @author Christopher Olbertz
 *
 */
public class TriplePredicateTest {
	@Test
	public void testConstructor_withString() {
		final Namespace namespace = ServerModelFactory.createNamespace("http://my.namespace.de");
		final LocalName localname = ServerModelFactory.createLocalName("mypredicate");
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TriplePredicate triplePredicate = new TriplePredicateImpl(iri);
		
		assertThat(triplePredicate.getNamespace(), is(equalTo(namespace)));
		assertThat(triplePredicate.getLocalName(), is(equalTo(localname)));
		assertNotNull(triplePredicate.getUuid());
	}
	
	@Test
	public void testConstructor_withUuidStringAndString() {
		final Uuid uuid = new Uuid();
		final Namespace namespace = ServerModelFactory.createNamespace("http://my.namespace.de");
		final LocalName localname = ServerModelFactory.createLocalName("mypredicate");
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TriplePredicate triplePredicate = new TriplePredicateImpl(uuid, namespace, localname);
		
		assertThat(triplePredicate.getNamespace(), is(equalTo(namespace)));
		assertThat(triplePredicate.getLocalName(), is(equalTo(localname)));
		assertThat(triplePredicate.getIri().toString(), is(equalTo(iri)));
		assertNotNull(triplePredicate.getUuid());
	}
	
	@Test
	public void testIsLocalFilePredicate() {
		final String namespace = "http://my.namespace.de";
		final String localname = "mypredicate";
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TriplePredicate triplePredicate = new TriplePredicateImpl(iri);
		triplePredicate.setPredicateType(PredicateType.LOCAL_FILE_REFERENCE);
		
		final boolean isLocalFileReference = triplePredicate.isLocalFilePredicate();
		assertTrue(isLocalFileReference);
	}
	
	@Test
	public void testIsRemoteFilePredicate() {
		final String namespace = "http://my.namespace.de";
		final String localname = "mypredicate";
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TriplePredicate triplePredicate = new TriplePredicateImpl(iri);
		triplePredicate.setPredicateType(PredicateType.REMOTE_FILE_REFERENCE);
		
		final boolean isRemoteFileReference = triplePredicate.isRemoteFilePredicate();
		assertTrue(isRemoteFileReference);
	}
	
	@Test
	public void testIsTypePredicate() {
		final String namespace = "http://my.namespace.de";
		final String localname = "mypredicate";
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TriplePredicate triplePredicate = new TriplePredicateImpl(iri);
		triplePredicate.setPredicateType(PredicateType.TYPE);
		
		final boolean isTypeReference = triplePredicate.isTypePredicate();
		assertTrue(isTypeReference);
	}
	
	@Test
	public void testIsObjectPredicate() {
		final String namespace = "http://my.namespace.de";
		final String localname = "mypredicate";
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TriplePredicate triplePredicate = new TriplePredicateImpl(iri);
		triplePredicate.setPredicateType(PredicateType.OBJECT_PROPERTY);
		
		final boolean isObjectFileReference = triplePredicate.isObjectPredicate();
		assertTrue(isObjectFileReference);
	}
	
	@Test
	public void testIsLiteralPredicate() {
		final String namespace = "http://my.namespace.de";
		final String localname = "mypredicate";
		final String iri = namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localname;
		final TriplePredicate triplePredicate = new TriplePredicateImpl(iri);
		triplePredicate.setPredicateType(PredicateType.LITERAL);
		
		final boolean isLiteralFileReference = triplePredicate.isLiteralPredicate();
		assertTrue(isLiteralFileReference);
	}
}
