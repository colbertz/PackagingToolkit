package de.feu.kdmp4.packagingtoolkit.server.metadata.enums;

/**
 * An enum that contains the algorithms for creating message digests that are relevant in the application.
 * @author Christopher Olbertz 
 *
 */
public enum MessageDigestAlgorithms {
	MD5("http://id.loc.gov/vocabulary/preservation/cryptographicHashFunctions/md5");
	
	/**
	 * Contains the name that is used for the resource in the triple store.
	 */
	private String resourceName;
	
	private MessageDigestAlgorithms(final String resourceName) {
		this.resourceName = resourceName;
	}
	
	public String getResourceName() {
		return resourceName;
	}
}
