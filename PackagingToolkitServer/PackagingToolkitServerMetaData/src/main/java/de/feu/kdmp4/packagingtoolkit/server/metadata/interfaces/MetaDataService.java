package de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces;

import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.MetaDataConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;

public interface MetaDataService {
	/**
	 * Sets a reference to the object that caches all information about the analyzed ontologies.
	 * @param ontologyCache The cache object.
	 */
	void setOntologyCache(final OntologyCache ontologyCache);
	/**
	 * Sets a reference to the object that contains the operations of this module.
	 * @param metaDataOperations A reference to the object that contains the operations of this module.
	 */
	void setMetaDataOperations(final MetaDataOperations metaDataOperations);
	/**
	 * Sets a reference to the object that contains the methods for accessing the configuration module.
	 * @param metaDataOperations A reference to the object that contains the methods for accessing 
	 * the configuration module.
	 */
	void setMetaDataConfigurationFacade(final MetaDataConfigurationFacade metaDataConfigurationFacade);
	/**
	 * Creates statements with the information received from the mediator.
	 * @param mediatorDataListResponse The data received from the mediator.
	 * @return The statements that describe the data received from the mediator. 
	 */
	TripleStatementCollection createStatements(final MediatorDataListResponse mediatorDataListResponse);
}
