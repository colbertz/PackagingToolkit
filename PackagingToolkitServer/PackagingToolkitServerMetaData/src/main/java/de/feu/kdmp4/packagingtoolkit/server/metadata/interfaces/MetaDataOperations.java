package de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces;

import de.feu.kdmp4.packagingtoolkit.server.metadata.enums.MessageDigestAlgorithms;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

public interface MetaDataOperations {
	/**
	 * Sets a reference to the object that caches all information about the analyzed ontologies.
	 * @param ontologyCache The cache object.
	 */
	void setOntologyCache(final OntologyCache ontologyCache);
	/**
	 * Creates the statement that are used for describing a message digest. The result contains the following statements:
	 * <ul>
	 * 	<li>One type statement for an individual for the message digest.</li>
	 * 	<li>One statement that describes the property for the message digest algorithm.</li>
	 * 	<li>One statement that describes the property for the message digest itself.</li>
	 * 	<li>One type statement for the object characteristics.</li>
	 * 	<li>One statement with the hasFixity relationship from the object characteristics individual to the fixity information
	 * individual.</li> 
	 * </ul>
	 * @param propertyWithMessageDigest Contains the information about the message digest.
	 * @param propertyWithMessageDigestAlgorithm The property that describes the relationship between the message digest
	 * and the algorithm.
	 * @param messageDigestAlgorithm The algorithm that has been used.
	 * @return A collection with the statements needed for describing the message digest.
	 */
	TripleStatementCollection createStatementsForMessageDigest(final OntologyDatatypeProperty propertyWithMessageDigest,
			final OntologyDatatypeProperty propertyWithMessageDigestAlgorithm, final MessageDigestAlgorithms messageDigestAlgorithm,
			final OntologyClass classForObjectCharacteristics);
	/**
	 * Creates the statement that are used for describing a creating application. The result contains the following statements:
	 * <ul>
	 * 	<li>One type statement for an individual for the creating application. It contains the name of the application in the iri.</li>
	 * 	<li>One type statement for the creating application characteristics.</li>
	 * 	<li>One statement with the hasCreating application relationship from the object characteristics individual to the creating application
	 * individual.</li> 
	 * </ul>
	 * @param propertyWithCreatingApplication Contains the information about the creating application.
	 * @param classForCreatingApplicationCharacteristics The class that represents the creating application characteristics.
	 * @return A collection with the statements needed for describing the creating application.
	 */	
	TripleStatementCollection createStatementsForCreatingApplication(OntologyClass classCreatingApplication,
			OntologyClass classForCreatingApplicationCharacteristics, String creatingApplicationName,
			OntologyDatatypeProperty creatingApplicationNameProperty);
	/**
	 * Creates two statements for a file:
	 * <ul>
	 * 	<li>One type statement for the fileClass.</li>
	 * 	<li>One type statement for the intellectualEntityClass.</li>
	 * </ul>
	 * @param filename The name of the file is used as name for the individual together with the default namespace.
	 * @param fileClass The ontology class that contains the class that is assigned to the property file in the mapping file.
	 * @param intellectualEntityClass The ontology class that contains the class that is assigned to the property intellectualEntity in the mapping file.
	 * @return
	 */
	TripleStatementCollection createStatementsForDigitalObject(String filename, OntologyClass fileClass,
			OntologyClass intellectualEntityClass);
	/**
	 * Creates a statement for the property hasObjectCharacteristics.
	 * @param filename The name of the file that has the object characteristics.
	 * @param objectCharacteristicsProperty Contains the hasObjectCharacteristics property. 
	 * @param iriOfObject The iri of the object for the property.
	 * @return The statement that represents the property hasObjectCharacteristics.
	 */
	TripleStatement createStatementForHasObjectCharacteristics(String filename,
			OntologyProperty objectCharacteristicsProperty, Iri iriOfObject);
}
