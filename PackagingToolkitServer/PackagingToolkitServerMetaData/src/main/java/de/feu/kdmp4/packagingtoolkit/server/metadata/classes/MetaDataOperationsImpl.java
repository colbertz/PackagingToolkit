package de.feu.kdmp4.packagingtoolkit.server.metadata.classes;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.metadata.enums.MessageDigestAlgorithms;
import de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces.MetaDataOperations;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleObject;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TriplePredicate;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleSubject;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;

public class MetaDataOperationsImpl implements MetaDataOperations {
	private OntologyCache ontologyCache;
	
	@Override
	public TripleStatementCollection createStatementsForMessageDigest(final OntologyDatatypeProperty propertyWithMessageDigest,
			final OntologyDatatypeProperty propertyWithMessageDigestAlgorithm, 
			final MessageDigestAlgorithms messageDigestAlgorithm,
			final OntologyClass classForObjectCharacteristics) {
		final TripleStatementCollection tripleStatementList = RdfElementFactory.createEmptyTripleStatementCollection();
		
		final TripleStatement typeStatementOfFixityInformation = createStatementForMessageDigestIndividual(propertyWithMessageDigest);
		tripleStatementList.addTripleStatement(typeStatementOfFixityInformation);
		
		final String uuidOfIndividual = typeStatementOfFixityInformation.getSubject().getLocalName().toString();
		final TripleStatement statementWithAlgorithm = createStatementForMessageDigestAlgorithm(propertyWithMessageDigestAlgorithm, 
				uuidOfIndividual, messageDigestAlgorithm);
		tripleStatementList.addTripleStatement(statementWithAlgorithm);
		final TripleStatement statementWithMessageDigest = createStatementForMessageDigest(propertyWithMessageDigest, uuidOfIndividual);
		tripleStatementList.addTripleStatement(statementWithMessageDigest);
		
		final String uuidOfFixityIndividual = typeStatementOfFixityInformation.getSubject().getLocalName().toString();
		tripleStatementList.addStatements(createStatementsForFixityCharacteristics(classForObjectCharacteristics, uuidOfFixityIndividual));
		
		return tripleStatementList;
	}
	
	/**
	 * Creates the triple that describes an individual for this message digest.
	 * @param propertyWithMessageDigest The property that contains the message digest.
	 * @return The triple that describes an individual for this message digest.
	 */
	private TripleStatement createStatementForMessageDigestIndividual(final OntologyDatatypeProperty propertyWithMessageDigest) {
		final Uuid uuidOfIndividual = new Uuid();
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual.toString());
		final String domainOfProperty = propertyWithMessageDigest.getPropertyDomainAt(0);
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(domainOfProperty);
		final TripleStatement typeStatement = RdfElementFactory.createTypeStatement(tripleSubject, tripleObject);
		
		return typeStatement;
	}
	
	/**
	 * Creates a statement for the algorithm that creates a message digest.
	 * @param propertyWithMessageDigestAlgorithm The property that contains the algorithm. Is used as predicate of the statement. 
	 * @param uuidOfIndividual The uuid of the individual the triple should be assigned to. Is used as subject of the statement.
	 * @param messageDigestAlgorithm The algorithm itself that is used as object of the statement.
	 * @return The statement that describes the message digest algorithm.
	 */
	private TripleStatement createStatementForMessageDigestAlgorithm(final OntologyDatatypeProperty propertyWithMessageDigestAlgorithm, 
			final String uuidOfIndividual, final MessageDigestAlgorithms messageDigestAlgorithm) {
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual);
		final TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(propertyWithMessageDigestAlgorithm.getPropertyId(), null);
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(messageDigestAlgorithm.getResourceName());
		final TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates a statement for the message digest.
	 * @param propertyWithMessageDigest The property that contains the message digest. The iri used as predicate of the statement and the
	 * value is the object of the statement. 
	 * @param uuidOfIndividual The uuid of the individual the triple should be assigned to. Is used as subject of the statement.
	 * @return The statement that describes the message digest.
	 */
	private TripleStatement createStatementForMessageDigest(final OntologyDatatypeProperty propertyWithMessageDigest, 
			final String uuidOfIndividual) {
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual);
		final TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(propertyWithMessageDigest.getPropertyId(), null);
		final TripleObject tripleObject = RdfElementFactory.createLiteralTripleObject(propertyWithMessageDigest.getPropertyValue());
		final TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	/**
	 * Creates the statements that describe the fixity characteristics.
	 * @param ontologyClass The class that should be used for the fixity characteristics. 
	 * @param uuidOfFixityIndividual The uuid of the individual that contains the fixity information themselves.
	 * @return The statements that describe the fixity characteristics.
	 */
	private TripleStatementCollection createStatementsForFixityCharacteristics(final OntologyClass ontologyClass, final String uuidOfFixityIndividual) {
		final TripleStatementCollection tripleStatementList = RdfElementFactory.createEmptyTripleStatementCollection();
		final TripleStatement typeStatement = createStatementForFixityCharacteristicsIndividual(ontologyClass);
		tripleStatementList.addTripleStatement(typeStatement);
		
		final String uuidOfIndividual = typeStatement.getSubject().getLocalName().toString();
		final TripleStatement hasFixityStatement = RdfElementFactory.createHasFixityStatement(uuidOfIndividual, uuidOfFixityIndividual);
		tripleStatementList.addTripleStatement(hasFixityStatement);
		return tripleStatementList;
	}
	
	/**
	 * Creates the triple that describes an individual for this fixity characteristics.
	 * @param ontologyClass The class that should be used as type for the fixity information. 
	 * @return The triple that describes an individual for the fixity information.
	 */
	private TripleStatement createStatementForFixityCharacteristicsIndividual(final OntologyClass ontologyClass) {
		final Uuid uuidOfIndividual = new Uuid();
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual.toString());
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(ontologyClass.getFullName());
		final TripleStatement typeStatement = RdfElementFactory.createTypeStatement(tripleSubject, tripleObject);
		
		return typeStatement;
	}
	
	@Override
	public TripleStatementCollection createStatementsForCreatingApplication(final OntologyClass classCreatingApplication, 
			final OntologyClass classForCreatingApplicationCharacteristics, final String creatingApplicationName, 
			final OntologyDatatypeProperty creatingApplicationNameProperty) {
		final TripleStatementCollection tripleStatementList = RdfElementFactory.createEmptyTripleStatementCollection();
		
		final TripleStatementCollection statementsOfCreatingApplication = createStatementForCreatingApplicationIndividual(classCreatingApplication, 
				creatingApplicationName, creatingApplicationNameProperty);
		tripleStatementList.addStatements(statementsOfCreatingApplication);
		
		final String uuidOfCreatingApplicationIndividual = statementsOfCreatingApplication.getTripleStatement(0).getSubject().getLocalName().toString();
		tripleStatementList.addStatements(createStatementsForCreatingApplicationCharacteristics(classForCreatingApplicationCharacteristics, uuidOfCreatingApplicationIndividual));
		
		return tripleStatementList;
	}
	
	/**
	 * Creates the triple that describes an individual for this creating application.
	 * @param propertyWithCreatingApplication The property that contains the creating application.
	 * @return The triple that describes an individual for this creating application.
	 */
	private TripleStatementCollection createStatementForCreatingApplicationIndividual(final OntologyClass classWithCreatingApplication, final String creatingApplicationName,
			final OntologyDatatypeProperty creatingApplicationNameProperty) {
		final Uuid uuidOfIndividual = new Uuid();
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual.toString());
		final String domainOfProperty = classWithCreatingApplication.getFullName();
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(domainOfProperty);
		final TripleStatement typeStatement = RdfElementFactory.createTypeStatement(tripleSubject, tripleObject);
		
		final TripleObject tripleObjectApplicationName = RdfElementFactory.createLiteralTripleObject(creatingApplicationName);
		final TriplePredicate predicateHasCreationApplicationName = RdfElementFactory.createTriplePredicate(
				creatingApplicationNameProperty.getPropertyId(), null);
		final TripleStatement statementHasCreationApplicationName = RdfElementFactory.createStatement(tripleSubject, predicateHasCreationApplicationName, 
				tripleObjectApplicationName);
		
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		tripleStatements.addTripleStatement(typeStatement);
		tripleStatements.addTripleStatement(statementHasCreationApplicationName);
		return tripleStatements;
	}
	
	/**
	 * Creates the statements that describe the creating application characteristics.
	 * @param ontologyClass The class that should be used for the creating application characteristics. 
	 * @param uuidOfCreatingApplication The uuid of the individual that contains the creating application itself.
	 * @return The statements that describe the creating application characteristics.
	 */
	private TripleStatementCollection createStatementsForCreatingApplicationCharacteristics(final OntologyClass ontologyClass, final String uuidOfCreatingApplicationIndividual) {
		final TripleStatementCollection tripleStatementList = RdfElementFactory.createEmptyTripleStatementCollection();
		final TripleStatement typeStatement = createStatementForCreatingApplicationCharacteristicsIndividual(ontologyClass);
		tripleStatementList.addTripleStatement(typeStatement);
		
		final String uuidOfIndividual = typeStatement.getSubject().getLocalName().toString();
		final TripleStatement hasFixityStatement = RdfElementFactory.createHasCreatingApplicationStatement(uuidOfIndividual, uuidOfCreatingApplicationIndividual);
		tripleStatementList.addTripleStatement(hasFixityStatement);
		return tripleStatementList;
	}
	
	/**
	 * Creates the triple that describes an individual for this creating application characteristics.
	 * @param ontologyClass The class that should be used as type for the creating application information. 
	 * @return The triple that describes an individual for the creating application information.
	 */
	private TripleStatement createStatementForCreatingApplicationCharacteristicsIndividual(final OntologyClass ontologyClass) {
		final Uuid uuidOfIndividual = new Uuid();
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(uuidOfIndividual.toString());
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(ontologyClass.getFullName());
		final TripleStatement typeStatement = RdfElementFactory.createTypeStatement(tripleSubject, tripleObject);
		
		return typeStatement;
	}
	
	@Override
	public TripleStatementCollection createStatementsForDigitalObject(final String filename, final OntologyClass fileClass,
			final OntologyClass intellectualEntityClass) {
		TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		TripleSubject subjectWithFilename = RdfElementFactory.createTripleSubject(filename);
		TripleObject fileTripleObject = RdfElementFactory.createTripleObject(fileClass.getFullName());
		TripleStatement fileTypeStatement = RdfElementFactory.createTypeStatement(subjectWithFilename, fileTripleObject);
		tripleStatements.addTripleStatement(fileTypeStatement);
		
		TripleObject intellectualEntityTripleObject = RdfElementFactory.createTripleObject(intellectualEntityClass.getFullName());
		TripleStatement intellectualEntityStatement = RdfElementFactory.createTypeStatement(subjectWithFilename, intellectualEntityTripleObject);
		tripleStatements.addTripleStatement(intellectualEntityStatement);
		
		
		return tripleStatements;
	}
	
	@Override
	public TripleStatement createStatementForHasObjectCharacteristics(final String filename, final OntologyProperty objectCharacteristicsProperty,
			final Iri iriOfObject) {
		final TripleSubject tripleSubject = RdfElementFactory.createTripleSubject(filename);
		final TriplePredicate triplePredicate = RdfElementFactory.createTriplePredicate(
				objectCharacteristicsProperty.getPropertyId(), null);
		final TripleObject tripleObject = RdfElementFactory.createTripleObject(iriOfObject.toString());
		final TripleStatement tripleStatement = RdfElementFactory.createStatement(tripleSubject, triplePredicate, tripleObject);
		return tripleStatement;
	}
	
	@Override
	public void setOntologyCache(final OntologyCache ontologyCache) {
		this.ontologyCache = ontologyCache;
	}
}
