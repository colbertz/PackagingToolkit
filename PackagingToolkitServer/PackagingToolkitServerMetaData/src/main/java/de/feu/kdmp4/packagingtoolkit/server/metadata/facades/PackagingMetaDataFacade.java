package de.feu.kdmp4.packagingtoolkit.server.metadata.facades;

import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces.MetaDataService;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;

public class PackagingMetaDataFacade {
	private MetaDataService metaDataService;
	
	public TripleStatementCollection createStatements(MediatorDataListResponse mediatorDataListResponse) {
		return metaDataService.createStatements(mediatorDataListResponse);
	}
	
	public void setMetaDataService(MetaDataService metaDataService) {
		this.metaDataService = metaDataService;
	}
}
