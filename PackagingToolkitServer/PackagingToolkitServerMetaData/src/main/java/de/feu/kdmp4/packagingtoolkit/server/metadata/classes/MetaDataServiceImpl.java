package de.feu.kdmp4.packagingtoolkit.server.metadata.classes;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.MetaDataConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.datastructures.interfaces.MetadataMappingMap;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.metadata.constants.MetaDataConstants;
import de.feu.kdmp4.packagingtoolkit.server.metadata.enums.MessageDigestAlgorithms;
import de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces.MetaDataOperations;
import de.feu.kdmp4.packagingtoolkit.server.metadata.interfaces.MetaDataService;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatement;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.properties.interfaces.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class MetaDataServiceImpl implements MetaDataService {
	/**
	 * Contains the cache with the elements read from the ontologies.
	 */
	private OntologyCache ontologyCache;
	/**
	 * A reference to the object with the atomar operations of this module.
	 */
	private MetaDataOperations metaDataOperations;
	/**
	 * A reference to the object that contains the methods for accessing the configuration
	 * module.
	 */
	private MetaDataConfigurationFacade metaDataConfigurationFacade;
	/**
	 * Contains the information which meta data are mapped to which classes and 
	 * properties of the ontologies. 
	 */
	private MetadataMappingMap metadataMappingMap; 
	
	/**
	 * Reads the map with the configuration of the meta data after the initialization of all beans.
	 */
	@PostConstruct
	public void initialize() {
		metadataMappingMap = metaDataConfigurationFacade.readMetadataMappingConfiguration();
	}
	
	@Override
	public TripleStatementCollection createStatements(final MediatorDataListResponse mediatorDataListResponse) {
		final TripleStatementCollection tripleStatements = RdfElementFactory.createEmptyTripleStatementCollection();
		tripleStatements.addStatements(createStatementsForFile(mediatorDataListResponse));
		
		return tripleStatements;
	}
	
	/**
	 * Creates the statements that describe the file of the digital object. An individual is created that is a type of the classes
	 * read from the configuration.  
	 * @return
	 */
	private TripleStatementCollection createStatementsForFile(final MediatorDataListResponse mediatorDataListResponse) {
		final Optional<Iri> optionalWithFileClass = metadataMappingMap.getIriOfFileClass();
		final Iri iriOfFileClass = optionalWithFileClass.get();
		final OntologyClass fileClass = ontologyCache.getClassByIri(iriOfFileClass.toString());
		
		final Optional<Iri> optionalWithIntellectualEntity = metadataMappingMap.getIriOfIntellectualEntityClass();
		final Iri iriOfIntellectualEntity = optionalWithIntellectualEntity.get();
		final OntologyClass intellectualEntityClass = ontologyCache.getClassByIri(iriOfIntellectualEntity.toString());
		
		String filename = mediatorDataListResponse.getOriginalFileName();
		filename = StringUtils.deleteSpaces(filename);
		final TripleStatementCollection tripleStatements = metaDataOperations.createStatementsForDigitalObject(filename, fileClass, intellectualEntityClass);
		final TripleStatementCollection fixityStatements = createStatementsForFixityInformation(mediatorDataListResponse, filename);
		tripleStatements.addStatements(fixityStatements);
		final String creatingApplicationName = findValueOf(MetaDataConstants.CREATING_APPLICATION_NAME, mediatorDataListResponse);
		tripleStatements.addStatements(createStatementsForCreatingApplication(creatingApplicationName, filename));
		return tripleStatements;
	}
	
	/**
	 * Creates the statements that are needed for saving the fixity information of a digital object. Looks for the iris that
	 * has been configured and takes the respective classes and properties from the ontology cache. Then the classes
	 * and properties are used for creating the statements that describe the fixity information.
	 * @return The statements that represent the fixity information.
	 */
	private TripleStatementCollection createStatementsForFixityInformation(final MediatorDataListResponse mediatorDataListResponse,
			final String filename) {
		final MessageDigestAlgorithms messageDigestAlgorithm = determineMessageDigestAlgorithm(mediatorDataListResponse);
		final String messageDigestValue = determineMessageDigest(messageDigestAlgorithm, mediatorDataListResponse);
		final Optional<Iri> optionalWithPropertyIri = metadataMappingMap.getIriOfMessageDigestProperty();
		final Iri propertyIri = optionalWithPropertyIri.get();
		final OntologyDatatypeProperty propertyWithMessageDigest = (OntologyDatatypeProperty)ontologyCache.
				cloneOntologyProperty(propertyIri.toString());
		propertyWithMessageDigest.setPropertyValue(messageDigestValue);
		final Optional<Iri> optionalWithMessageDigestAlgorithmIri = metadataMappingMap.getIriOfMessageDigestAlgorithmProperty();
		final Iri iriOfMessageDigestAlgorithm = optionalWithMessageDigestAlgorithmIri.get();
		final OntologyDatatypeProperty propertyWithMessageDigestAlgorithm = (OntologyDatatypeProperty)ontologyCache.
				cloneOntologyProperty(iriOfMessageDigestAlgorithm.toString());
		final Optional<Iri> optionalWithObjectCharacteristicsIri = metadataMappingMap.getIriOfObjectCharacteristicsClass();
		final Iri iriForObjectCharacteristics = optionalWithObjectCharacteristicsIri.get();
		final OntologyClass classForObjectCharacteristics = ontologyCache.getClassByIri(iriForObjectCharacteristics.toString());
		final TripleStatementCollection fixityTripleStatements = metaDataOperations.createStatementsForMessageDigest(propertyWithMessageDigest, 
				propertyWithMessageDigestAlgorithm, messageDigestAlgorithm, classForObjectCharacteristics);
		
		final Optional<Iri> optionalWithHasObjectCharacteristicsIri = metadataMappingMap.getIriOfHasObjectCharacteristicsProperty();
		final Iri iriOfHasObjectCharacteristics = optionalWithHasObjectCharacteristicsIri.get();
		final OntologyProperty hasObjectCharacteristics = ontologyCache.cloneOntologyProperty(iriOfHasObjectCharacteristics.toString());
		final int indexOfObjectCharacteristics = fixityTripleStatements.getTripleStatementsCount() - 1;
		final Iri iriOfFixityCharacteristics = fixityTripleStatements.getTripleStatement(indexOfObjectCharacteristics).getSubject().getIri();
		final TripleStatement hasObjectCharacteristicsStatement = metaDataOperations.createStatementForHasObjectCharacteristics(filename, hasObjectCharacteristics, iriOfFixityCharacteristics);
		fixityTripleStatements.addTripleStatement(hasObjectCharacteristicsStatement);
		
		return fixityTripleStatements;
	}
	
	/**
	 * Creates the statements that are needed for saving the creating application of a digital object. Looks for the iris that
	 * has been configured and takes the respective classes and properties from the ontology cache. Then the classes
	 * and properties are used for creating the statements that describe the fixity information.
	 * @return The statements that represent the creating application.
	 */
	private TripleStatementCollection createStatementsForCreatingApplication(final String creatingApplicationName, final String filename) {
		final Optional<Iri> iriWithCreatingApplication = metadataMappingMap.getIriOfCreatingApplicationClass();
		if (iriWithCreatingApplication.isPresent()) {
			final Iri iriOfCreatingApplicationClass = iriWithCreatingApplication.get();
			final OntologyClass creatingApplicationClass = ontologyCache.getClassByIri(iriOfCreatingApplicationClass.toString());
			return createCreatingApplicationCharacteristics(creatingApplicationName, filename, creatingApplicationClass);
		}
		
		return RdfElementFactory.createEmptyTripleStatementCollection();
	}
	
	/**
	 * Creates statements for the creating application characteristics for a digital object.
	 * @param creatingApplicationName The name of the application that has created the digital object.
	 * @param filename The name of the file of the digital object.
	 * @param creatingApplicationClass The class that represents the creating application in the ontology.
	 * @return The statements that have been created in this method and that describe the creating 
	 * application characteristics for a digital object. 
	 */
	private TripleStatementCollection createCreatingApplicationCharacteristics(final String creatingApplicationName, final String filename,
			final OntologyClass creatingApplicationClass) {
		final Optional<Iri> optionalWithCreatingApplicationCharacteristicsIri = metadataMappingMap.getIriOfObjectCharacteristicsClass();
		if (optionalWithCreatingApplicationCharacteristicsIri.isPresent()) {
			final Iri iriForCreatingApplicationCharacteristics = optionalWithCreatingApplicationCharacteristicsIri.get();
			final OntologyClass classForCreatingApplicationCharacteristics = ontologyCache.getClassByIri(iriForCreatingApplicationCharacteristics.toString());
			
			final Optional<Iri> optionalWithHasCreatingApplicationName = metadataMappingMap.getIriOfCreatingApplicationNameProperty();
			if (optionalWithHasCreatingApplicationName.isPresent()) {
				final Iri iriForHasCreatingApplicationName = optionalWithHasCreatingApplicationName.get();
				final OntologyDatatypeProperty propertyWithCreatingApplicationName = (OntologyDatatypeProperty)ontologyCache.cloneOntologyProperty(
						iriForHasCreatingApplicationName.toString());
				final TripleStatementCollection creatingApplicationTripleStatements = metaDataOperations.createStatementsForCreatingApplication(creatingApplicationClass, 
						classForCreatingApplicationCharacteristics, creatingApplicationName, propertyWithCreatingApplicationName);
				
				return createObjectCharacteristis(creatingApplicationTripleStatements, filename);
			}
		}
		
		return RdfElementFactory.createEmptyTripleStatementCollection();
	}
	
	/**
	 * Creates statements for the object characteristics for a digital object
	 * @param creatingApplicationTripleStatements Contains the statements that describe the digital object to this
	 * point of the application. 
	 * @param filename The name of the file of the digital object.
	 * @return The statements of creatingApplicationTripleStatements additionally the statements that have been created
	 * in this method.
	 */
	private TripleStatementCollection createObjectCharacteristis(final TripleStatementCollection creatingApplicationTripleStatements, final String filename) {
		final Optional<Iri> optionalWithHasObjectCharacteristicsIri = metadataMappingMap.getIriOfHasObjectCharacteristicsProperty();
				
		if (optionalWithHasObjectCharacteristicsIri.isPresent()) {
			final Iri iriOfHasObjectCharacteristics = optionalWithHasObjectCharacteristicsIri.get();
			final OntologyProperty hasObjectCharacteristics = ontologyCache.cloneOntologyProperty(iriOfHasObjectCharacteristics.toString());
			final int indexOfCreatingApplicationCharacteristics = creatingApplicationTripleStatements.getTripleStatementsCount() - 1;
			final Iri iriOfCreatingApplicationCharacteristics = creatingApplicationTripleStatements.getTripleStatement(indexOfCreatingApplicationCharacteristics).getSubject().getIri();
			final TripleStatement hasObjectCharacteristicsStatement = metaDataOperations.createStatementForHasObjectCharacteristics(filename, hasObjectCharacteristics, iriOfCreatingApplicationCharacteristics);
			creatingApplicationTripleStatements.addTripleStatement(hasObjectCharacteristicsStatement);
		}
		return creatingApplicationTripleStatements;
	}
	
	/**
	 * Determines the algorithm that was used to create the message digest. Can easily be extended by other algorithms.  
	 * @return The message digest algorithm.
	 */
	private MessageDigestAlgorithms determineMessageDigestAlgorithm(final MediatorDataListResponse mediatorDataListResponse) {
		if (mediatorDataListResponse.containsMd5Checksum()) {
			return MessageDigestAlgorithms.MD5;
		} else {
			return null;
		}
	}
	
	/**
	 * Determines the value of the message digest from the map with the meta data.
	 * @param messageDigestAlgorithm Contains the information which algorithm has been used. 
	 * @return The iri of the property that should contain the message digest.
	 */
	private String determineMessageDigest(final MessageDigestAlgorithms messageDigestAlgorithm, 
			MediatorDataListResponse mediatorDataListResponse) {
		switch(messageDigestAlgorithm) {
		case MD5: return findValueOf(MetaDataConstants.MD5_CHECKSUM, mediatorDataListResponse);
		}
		
		return StringUtils.EMPTY_STRING;
	}
	
	@Override
	public void setOntologyCache(final OntologyCache ontologyCache) {
		this.ontologyCache = ontologyCache;
	}
	
	@Override
	public void setMetaDataOperations(final MetaDataOperations metaDataOperations) {
		this.metaDataOperations = metaDataOperations;
	}
	
	/**
	 * Finds the value of a certain meta data element from a list. 
	 * @param metaDataName The name of the element we are looking for.
	 * @param mediatorDataListResponse The list with the elements we are searching in. 
	 * @return The found meta data value or an empty string if the desired element is not contained in mediatorDataListResponse.
	 */
	private String findValueOf(final String metaDataName, MediatorDataListResponse mediatorDataListResponse) {
		final String foundValue = StringUtils.EMPTY_STRING;
		final List<MediatorDataResponse> mediatorDataList = mediatorDataListResponse.getMediatorData(); 
		
		for (final MediatorDataResponse mediatorDataResponse: mediatorDataList) {
			if (areStringsEqual(metaDataName, mediatorDataResponse.getName())) {
				return mediatorDataResponse.getValue();
			}
		}
		
		return foundValue;
	}

	@Override
	public void setMetaDataConfigurationFacade(final MetaDataConfigurationFacade metaDataConfigurationFacade) {
		this.metaDataConfigurationFacade = metaDataConfigurationFacade;
	}
}
