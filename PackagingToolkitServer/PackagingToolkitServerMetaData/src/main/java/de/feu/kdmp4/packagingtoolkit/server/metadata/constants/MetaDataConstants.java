package de.feu.kdmp4.packagingtoolkit.server.metadata.constants;

/**
 * Contains some constants for the module MetaData.
 * @author Christopher Olbertz
 *
 */
public class MetaDataConstants {
	/**
	 * 
	 */
	public static final String CREATING_APPLICATION_NAME = "creatingApplicationName"; 
	/**
	 * The name of the meta data that contains the md5 checksum.
	 */
	public static final String MD5_CHECKSUM = "md5checksum";
}
