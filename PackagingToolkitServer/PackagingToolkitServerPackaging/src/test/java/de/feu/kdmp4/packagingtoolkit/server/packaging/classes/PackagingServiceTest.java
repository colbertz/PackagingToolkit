package de.feu.kdmp4.packagingtoolkit.server.packaging.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.PackagingConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.PackagingDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.factories.RdfElementFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.ReferenceImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.ReferenceCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.facades.PackagingOntologyFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingOperations;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingService;
import de.feu.kdmp4.packagingtoolkit.server.serialization.facade.PackagingSerializationFacade;
import de.feu.kdmp4.packagingtoolkit.server.session.facade.PackagingSessionFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.facade.PackagingStorageFacade;


//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {ApplicationConfiguration.class})
//@SpringBootTest(classes = ServerMain.class)
@RunWith(MockitoJUnitRunner.class)
public class PackagingServiceTest  {
	// **************** Constants ******************
	/**
	 * The title of the information package the tests are working with.
	 */
	private static final String EXPECTED_TITLE = "myTitle";
	/**
	 * The content of the new file that should be added in the temporary directory.
	 */
	private static final String NEW_FILE_CONTENT_1 = "abcdefghijklmnopqrstuvwxyz";
	/**
	 * The name of the new file that should be added in the temporary directory.
	 */
	private static final String NEW_FILE_NAME_1 = "newFile";
	/**
	 * The content of the new file that should be added in the temporary directory.
	 */
	private static final String NEW_FILE_CONTENT_2 = "12345678900987654321";
	/**
	 * The name of the new file that should be added in the temporary directory.
	 */
	private static final String NEW_FILE_NAME_2 = "anotherNewFile";
	/**
	 * The path to the working directory on the server for testing 
	 * purposes. 
	 */
	private static final String WORKING_DIRECTORY = "src/test/resources/workdir";
	/**
	 * The path to the temporary directory.
	 */
	private static final String TEMP_DIRECTORY = "src/test/resources/workdir/temp";
	/**
	 * The index of the file that should be deleted in the list with the test files.
	 */
	private static final int INDEX_DELETED_FILE = 1;
	// **************** Attributes ****************
	/**
	 * An information package for testing.
	 */
	private PackageResponse informationPackage;
	/**
	 * The temporary directory of the test information package.
	 */
	private File temporaryDirectory;
	@Rule
	public MockitoRule mockitoRule;
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	@Mock
	private PackagingSessionFacade packagingSessionFacade;
	@Mock
	private PackagingSerializationFacade packagingSerializationFacade; 
	@Mock
	private PackagingConfigurationFacade packagingConfigurationFacade;
	@Mock
	private PackagingDatabaseFacade packagingDatabaseFacade;
	//@Mock
	private PackagingOperations packagingOperations;
	private PackagingService packagingService;
	
	//*************** Setup and tear down *************
	/**
	 * Sets up the tests. Creates an implementation for the operation class and
	 * creates an object for the configuration file. 
	 * @throws IOException 
	 */
	@Before
	public void setup() throws IOException {
		packagingService = new PackagingServiceImpl();
	}
	
	/**
	 * For some tests the attributes of the configuration file have to be 
	 * modified. 
	 */
	@After
	public void tearDown() {
		
	}
	
	/**
	 * Creates a working directory for the tests. It creates also the temporary directory
	 * for the information package under test.
	 * @return The working directory that can be used for the tests.
	 */
	private WorkingDirectory createTestWorkingDirectory() {
		File workingDir = new File(WORKING_DIRECTORY);
		
		if (!workingDir.exists()) {
			workingDir.mkdir();
		}
		createTempDirectory();
		
		return new WorkingDirectoryImpl(workingDir.getAbsolutePath());
	}
	
	/**
	 * Creates a temporary directory in the working directory for the tests.
	 * @return A File object that represents the temporary directory.
	 */
	private File createTempDirectory() {
		File tempDir = new File(TEMP_DIRECTORY);
		
		if (!tempDir.exists()) {
			tempDir.mkdir();
		}
		return tempDir;
	}

	/**
	 * Deletes all files and all sub directories in a given path. It deletes also the directory
	 * at the given path itself.
	 * @param path The path that contains the files and directories to delete.
	 */
	private  void deleteFilesAndDirectories(File path) {
		for (File file: path.listFiles()) {
	         if (file.isDirectory())
	        	 deleteFilesAndDirectories(file);
	         file.delete();
	      }
	      path.delete();
	}
	
	/**
	 * Creates a directory for the test information package in the temporary
	 * directory.
	 */
	private File createDirectoryForInformationPackage(Uuid testUuid) {
		File dirToZip = new File(TEMP_DIRECTORY + File.separator + testUuid.toString());		
		dirToZip.mkdir();
		return dirToZip;
	}
	
	/**
	 * Deletes the working directory and all files and directories contained.
	 * @param workingDirectory The working directory that should be deleted.
	 */
	private void deleteWorkingDirectory(WorkingDirectory workingDirectory) {
		String workingDirectoryPath = workingDirectory.getWorkingDirectoryPath();
		File workingDir = new File(workingDirectoryPath);
		deleteFilesAndDirectories(workingDir);
	}
	
	/**
	 * Creates a sorted list that contains the names of the files contained in a zip file.
	 * @param zipFile The zip file with the files.
	 * @return The sorted list with the file names.
	 */
	private List<String> createSortedListWithNamesOfFilesInZipFile(ZipFile zipFile) {
		Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
		ZipEntry zipEntry = null;
		// Create a list with the zip file names for sorting and comparing with the expected list.
		List<String> zipFileNameList = new ArrayList<>();
		while(zipEntries.hasMoreElements()) {
			zipEntry = zipEntries.nextElement();
			zipFileNameList.add(zipEntry.getName());
		}
		
		Collections.sort(zipFileNameList);
		return zipFileNameList;
	}
	
	// ****************** Test methods *******************
	// *****************************************************************************************************
	// *********** testAddDigitalObjectToInformationPackage_digitalObjectDoesNotExist
	// *****************************************************************************************************
	/**
	 * Tests if digital objects are correctly added to information packages.
	 * It creates two files and adds them to the information packages. The 
	 * test is successfull if the two files are lying in the temporary
	 * directory of this information package.
	 */
	@Test
	public void testAddDigitalObjectToInformationPackage_digitalObjectDoesNotExist() {
		// Prepare the test.
		Uuid testUuid = new Uuid();
		String testUuidAsString = testUuid.toString();
		WorkingDirectory workingDirectory = createTestWorkingDirectory();
		prepareTest_testAddDigitalObjectToInformationPackage_mockedObjects(workingDirectory);
		prepareTest_testAddDigitalObjectToInformationPackage_tempDirectory(testUuid, workingDirectory);
		
		// Add the files to the information package.
		byte[] dataOfFile1 = NEW_FILE_CONTENT_1.getBytes();
		byte[] dataOfFile2 = NEW_FILE_CONTENT_2.getBytes();

		prepareTest_testAddDigitalObjectToInformationPackage_mockedPackagingStorageFacade(dataOfFile1, dataOfFile2);
		
		packagingService.addDigitalObjectToInformationPackage(testUuidAsString, NEW_FILE_NAME_1, dataOfFile1);
		packagingService.addDigitalObjectToInformationPackage(testUuidAsString, NEW_FILE_NAME_2, dataOfFile2);
		
		checkTestResults_testAddDigitalObjectToInformationPackage_existingFiles();
		
		deleteWorkingDirectory(workingDirectory);
	}
	
	/**
	 * Prepares the mock objects for the test. Because the files have to be copied into the temporary directory, the object
	 * packagingOperations is spied. Configures configurationService, that is returns the working directory defined in this
	 * test class.
	 * @param workingDirectory The working directory that should be returned by condfiguration service.
	 */
	private void prepareTest_testAddDigitalObjectToInformationPackage_mockedObjects(WorkingDirectory workingDirectory) {
		packagingOperations = new PackagingOperationsImpl();
		PackagingOperations packagingOperationsSpy = spy(packagingOperations);
		
		packagingService.setPackagingConfigurationFacade(packagingConfigurationFacade);
		packagingService.setPackagingOperations(packagingOperationsSpy);
		packagingService.setPackagingSessionFacade(packagingSessionFacade);
		
		when(packagingConfigurationFacade.getWorkingDirectory()).thenReturn(workingDirectory);
	}
	
	/**
	 * Mocks the PackagingStorageFacade for the two input streams. For every input stream the PackagingStorageFacade returns
	 * that the file that contains the data of the stream does not exist.
	 * @param inputStream1
	 * @param inputStream2
	 */
	private void prepareTest_testAddDigitalObjectToInformationPackage_mockedPackagingStorageFacade(byte[] dataOfFile1, byte[] dataOfFile2) {
		PackagingStorageFacade packagingStorageFacade = mock(PackagingStorageFacade.class);
		packagingService.setPackagingStorageFacade(packagingStorageFacade);
		when(packagingStorageFacade.fileExistsInStorage(dataOfFile1)).thenReturn(false);
		when(packagingStorageFacade.fileExistsInStorage(dataOfFile2)).thenReturn(false);
	}
	
	/**
	 * Creates the temporary directory.
	 * @param testUuid The uuid that is the name of the temporary directory.
	 * @param workingDirectory The working directory for the test.
	 */
	private void prepareTest_testAddDigitalObjectToInformationPackage_tempDirectory(Uuid testUuid, WorkingDirectory workingDirectory) {		
		createDirectoryForInformationPackage(testUuid);
		temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(testUuid);
	}
	
	/**
	 * Checks the results of this test. The two file have to exist in the correct temporary directory.
	 */
	private void checkTestResults_testAddDigitalObjectToInformationPackage_existingFiles() {
		File file1 = new File(temporaryDirectory + File.separator + NEW_FILE_NAME_1);
		File file2 = new File(temporaryDirectory + File.separator + NEW_FILE_NAME_2);
		
		assertTrue(file1.exists());
		assertTrue(file2.exists());
	}
	
	// *****************************************************************************************************
	// *********** testGetTemporaryDirectoryForUuid
	// *****************************************************************************************************
	@Test 
	public void testGetTemporaryDirectoryForUuid() {
		Uuid testUuid = new Uuid();
		String expectedTemporaryDirectory = WORKING_DIRECTORY + File.separator + testUuid.toString();
		
		String actualTemporaryDirectory = packagingService.getTemporaryDirectoryForUuid(WORKING_DIRECTORY, testUuid);
		
		assertThat(actualTemporaryDirectory, is(equalTo(expectedTemporaryDirectory)));
	}
	
	// *****************************************************************************************************
	// *********** testDownloadVirtualInformationPackage
	// *****************************************************************************************************
	/**
	 * Tests the download of a virtual information package. The zip file may contain only the manifest file.
	 * @throws IOException
	 */
	@Test
	public void testDownloadVirtualInformationPackage() throws IOException {
		WorkingDirectory workingDirectory = prepareTest_testDownloadVirtualInformationPackage_WorkingDirectory();
		File manifestFile = prepareTest_testDownloadVirtualInformationPackage_manifestFile();
		UuidResponse uuidResponse = new UuidResponse();
		PackagingService packagingService = prepareTest_testDownloadVirtualInformationPackage_mockPackagingService(manifestFile,
				uuidResponse.toString(), workingDirectory);
		
		File actualZipFile = packagingService.downloadVirtualInformationPackage(uuidResponse);
		checkResults_testDownloadVirtualInformationPackage_zipFileContent(actualZipFile, manifestFile);
	}
	
	/**
	 * Creates the working directory for the test.
	 * @return The working directory.
	 * @throws IOException
	 */
	private WorkingDirectory prepareTest_testDownloadVirtualInformationPackage_WorkingDirectory() throws IOException {
		File workingDirectoryFile = temporaryFolder.newFolder("work");
		WorkingDirectory workingDirectory = new WorkingDirectoryImpl(workingDirectoryFile.getAbsolutePath());
		
		return workingDirectory;
	}
	
	/**
	 * Creates the manifest file for the test. 
	 * @return The manifest file.
	 * @throws IOException
	 */
	private File prepareTest_testDownloadVirtualInformationPackage_manifestFile() throws IOException {
		File manifestFile = temporaryFolder.newFile(PackagingToolkitConstants.NAME_OF_MANIFEST_FILE);
		try (OutputStream outputStream = new FileOutputStream(manifestFile);
				PrintWriter printWriter = new PrintWriter(outputStream)) {
			printWriter.println("This is a manifest");
			printWriter.println("This is the second line of the manifest");
		}
		
		return manifestFile;
	}
	
	/**
	 * Mocks the PackagingService object for the test. 
	 * @param manifestFile The manifest file that is used during the test.
	 * @param uuidOfInformationPackageAsString The uuid of the information package for the test.
	 * @param workingDirectory The working directory that is used during the test.
	 * @return The mocked PackagingService.
	 */
	private PackagingService prepareTest_testDownloadVirtualInformationPackage_mockPackagingService(File manifestFile,
			String uuidOfInformationPackageAsString, WorkingDirectory workingDirectory) {
		OntologyIndividualCollection ontologyIndividualList = prepareTest_testDownloadVirtualInformationPackage_createOntologyIndividuals();
		
		PackagingOperations packagingOperations = new PackagingOperationsImpl();
		PackagingOntologyFacade packagingOntologyFacade = mock(PackagingOntologyFacade.class);
		Uuid uuidOfInformationPackage = new Uuid(uuidOfInformationPackageAsString);

		PackagingService packagingService = new PackagingServiceImpl();
		packagingService.setPackagingSerializationFacade(packagingSerializationFacade);
		packagingService.setPackagingOperations(packagingOperations);
		packagingService.setPackagingConfigurationFacade(packagingConfigurationFacade);
		packagingService.setPackagingOntologyFacade(packagingOntologyFacade);
		
		when(packagingOntologyFacade.findIndiviualsByInformationPackage(uuidOfInformationPackageAsString)).thenReturn(ontologyIndividualList);
		when(packagingSerializationFacade.createManifest(uuidOfInformationPackage, ontologyIndividualList, SerializationFormat.OAIORE)).thenReturn(manifestFile);
		when(packagingConfigurationFacade.getWorkingDirectory()).thenReturn(workingDirectory);
		
		return packagingService;
	}
	
	private OntologyIndividualCollection prepareTest_testDownloadVirtualInformationPackage_createOntologyIndividuals() {
		OntologyIndividualCollection ontologyIndividualList = new OntologyIndividualCollectionImpl();
		Uuid uuidOfInformationPackage1 = new Uuid();
		Uuid uuidOfInformationPackage2 = new Uuid();
		
		OntologyClass ontologyClass1 = new OntologyClassImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace(), new LocalNameImpl("class1"));
		OntologyClass ontologyClass2 = new OntologyClassImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace(), new LocalNameImpl("class2"));
		
		OntologyIndividual individual1 = new OntologyIndividualImpl(ontologyClass1, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage1);
		OntologyIndividual individual2 = new OntologyIndividualImpl(ontologyClass2, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage1);
		
		OntologyIndividual individual3 = new OntologyIndividualImpl(ontologyClass1, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage2);
		OntologyIndividual individual4 = new OntologyIndividualImpl(ontologyClass2, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage2);
		
		ontologyIndividualList.addIndividual(individual1);
		ontologyIndividualList.addIndividual(individual2);
		ontologyIndividualList.addIndividual(individual3);
		ontologyIndividualList.addIndividual(individual4);
		
		return ontologyIndividualList;
	}
	
	/**
	 * Checks the zip file that was created by the test. It may only contain the manifest file. 
	 * @param theZipFile The zip file that was created by the test.
	 */
	private void checkResults_testDownloadVirtualInformationPackage_zipFileContent(File theZipFile, File theManifestFile) {
		// Check if the correct files were zipped.
		try (ZipFile zipFile = new ZipFile(theZipFile)) {
			List<String> zipFileNameList = createSortedListWithNamesOfFilesInZipFile(zipFile);
			int actualZipFileCount = zipFileNameList.size();
			assertThat(actualZipFileCount, is(equalTo(1)));
			String actualManifestFileName = zipFileNameList.get(0);
			String expectedManifestFileName = theManifestFile.getName();
			assertThat(actualManifestFileName, is (equalTo(expectedManifestFileName)));
		} catch (IOException ex) {
			fail(ex.getMessage());
		}
	}
	
	// *****************************************************************************************************
	// *********** testDownloadNotVirtualInformationPackage
	// *****************************************************************************************************
	/**
	 * Tests the download of a not virtual information package. The zip file may contain the manifest file and two other 
	 * files.
	 * @throws IOException
	 */
	@Test
	public void testDownloadNotVirtualInformationPackage() throws IOException {
		WorkingDirectory workingDirectory = prepareTest_testDownloadNotVirtualInformationPackage_WorkingDirectory();
		ReferenceCollection referenceList = prepareTest_testDownloadNotVirtualInformationPackage_ReferenceListAndFiles(workingDirectory);
		File manifestFile = prepareTest_testDownloadNotVirtualInformationPackage_manifestFile();
		UuidResponse uuidResponse = new UuidResponse();
		PackagingService packagingService = prepareTest_testDownloadNotVirtualInformationPackage_mockPackagingService(manifestFile,
				uuidResponse.toString(), workingDirectory, referenceList);
		
		File actualZipFile = packagingService.downloadNotVirtualInformationPackage(uuidResponse);
		checkResults_testDownloadNotVirtualInformationPackage_zipFileContent(actualZipFile, manifestFile, referenceList);
	}
	
	/**
	 * Creates the working directory for the test.
	 * @return The working directory.
	 * @throws IOException
	 */
	private WorkingDirectory prepareTest_testDownloadNotVirtualInformationPackage_WorkingDirectory() throws IOException {
		File workingDirectoryFile = temporaryFolder.newFolder("work");
		WorkingDirectory workingDirectory = new WorkingDirectoryImpl(workingDirectoryFile.getAbsolutePath());
		
		return workingDirectory;
	}
	
	/**
	 * Creates the manifest file for the test. 
	 * @return The manifest file.
	 * @throws IOException
	 */
	private File prepareTest_testDownloadNotVirtualInformationPackage_manifestFile() throws IOException {
		File manifestFile = temporaryFolder.newFile(PackagingToolkitConstants.NAME_OF_MANIFEST_FILE);
		try (OutputStream outputStream = new FileOutputStream(manifestFile);
				PrintWriter printWriter = new PrintWriter(outputStream)) {
			printWriter.println("This is a manifest");
			printWriter.println("This is the second line of the manifest");
		}
		
		return manifestFile;
	}
	
	/**
	 * Mocks the PackagingService object for the test. 
	 * @param manifestFile The manifest file that is used during the test.
	 * @param uuidOfInformationPackageAsString The uuid of the information package for the test.
	 * @param workingDirectory The working directory that is used during the test.
	 * @return The mocked PackagingService.
	 */
	private PackagingService prepareTest_testDownloadNotVirtualInformationPackage_mockPackagingService(File manifestFile,
			String uuidOfInformationPackageAsString, WorkingDirectory workingDirectory, ReferenceCollection referenceList) {
		OntologyIndividualCollection ontologyIndividualList = prepareTest_testDownloadNotVirtualInformationPackage_createOntologyIndividuals();
		
		PackagingOperations packagingOperations = new PackagingOperationsImpl();
		PackagingOntologyFacade packagingOntologyFacade = mock(PackagingOntologyFacade.class);
		Uuid uuidOfInformationPackage = new Uuid(uuidOfInformationPackageAsString);
		
		PackagingService packagingService = new PackagingServiceImpl();
		packagingService.setPackagingSerializationFacade(packagingSerializationFacade);
		packagingService.setPackagingOperations(packagingOperations);
		packagingService.setPackagingConfigurationFacade(packagingConfigurationFacade);
		packagingService.setPackagingDatabaseFacade(packagingDatabaseFacade);
		packagingService.setPackagingOntologyFacade(packagingOntologyFacade);
		
		when(packagingOntologyFacade.findIndiviualsByInformationPackage(uuidOfInformationPackageAsString)).thenReturn(ontologyIndividualList);
		when(packagingSerializationFacade.createManifest(uuidOfInformationPackage, ontologyIndividualList, SerializationFormat.OAIORE)).thenReturn(manifestFile);
		when(packagingConfigurationFacade.getWorkingDirectory()).thenReturn(workingDirectory);
		when(packagingDatabaseFacade.findReferencesOfInformationPackage(uuidOfInformationPackageAsString)).thenReturn(referenceList);
		
		return packagingService;
	}
	
	private OntologyIndividualCollection prepareTest_testDownloadNotVirtualInformationPackage_createOntologyIndividuals() {
		OntologyIndividualCollection ontologyIndividualList = new OntologyIndividualCollectionImpl();
		Uuid uuidOfInformationPackage1 = new Uuid();
		Uuid uuidOfInformationPackage2 = new Uuid();
		
		OntologyClass ontologyClass1 = new OntologyClassImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace(), new LocalNameImpl("class1"));
		OntologyClass ontologyClass2 = new OntologyClassImpl(RdfElementFactory.getPackagingToolkitDefaultNamespace(), new LocalNameImpl("class2"));
		
		OntologyIndividual individual1 = new OntologyIndividualImpl(ontologyClass1, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage1);
		OntologyIndividual individual2 = new OntologyIndividualImpl(ontologyClass2, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage1);
		
		OntologyIndividual individual3 = new OntologyIndividualImpl(ontologyClass1, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage2);
		OntologyIndividual individual4 = new OntologyIndividualImpl(ontologyClass2, new DatatypePropertyCollectionImpl(), 
				new ObjectPropertyCollectionImpl(), uuidOfInformationPackage2);
		
		ontologyIndividualList.addIndividual(individual1);
		ontologyIndividualList.addIndividual(individual2);
		ontologyIndividualList.addIndividual(individual3);
		ontologyIndividualList.addIndividual(individual4);
		
		return ontologyIndividualList;
	}
	
	/**
	 * Creates some references to digital objects and their files in the storage directory.
	 * @param workingDirectory The working directory that contains the storage.
	 * @return The references that are used for the test.
	 * @throws IOException
	 */
	private ReferenceCollection prepareTest_testDownloadNotVirtualInformationPackage_ReferenceListAndFiles(WorkingDirectory workingDirectory) throws IOException {
		ReferenceCollection referenceList = new ReferenceCollectionImpl();
		Reference reference1 = ReferenceImpl.createLocalFileReference("testfile1", new Uuid());
		Reference reference2 = ReferenceImpl.createLocalFileReference("testfile2", new Uuid());
		File file1 = workingDirectory.getFileInStorageDirectory(reference1.getUrl());
		File file2 = workingDirectory.getFileInStorageDirectory(reference2.getUrl());

		String pathFile1 = file1.getAbsolutePath();
		int indexOfWorkingDirectory = pathFile1.indexOf("work");
		pathFile1 = pathFile1.substring(indexOfWorkingDirectory);
		String pathFile2 = file2.getAbsolutePath();
		pathFile2 = pathFile2.substring(indexOfWorkingDirectory);
		
		temporaryFolder.newFile(pathFile1);
		temporaryFolder.newFile(pathFile2);
		
		referenceList.addReference(reference1);
		referenceList.addReference(reference2);
		
		return referenceList;
	}
	
	/**
	 * Checks the zip file that was created by the test. It contains the manifest file and two other files. 
	 * @param theManifestFile The only file the zip file may contain.
	 */
	private void checkResults_testDownloadNotVirtualInformationPackage_zipFileContent(File theZipFile, File theManifestFile, 
			ReferenceCollection referenceList) {
		// Check if the correct files were zipped.
		try (ZipFile zipFile = new ZipFile(theZipFile)) {
			List<String> zipFileNameList = createSortedListWithNamesOfFilesInZipFile(zipFile);
			int actualZipFileCount = zipFileNameList.size();
			int expectedZipFileCount = referenceList.getReferencesCount() + 1;
			
			assertThat(actualZipFileCount, is(equalTo(expectedZipFileCount)));
			String actualManifestFileName = zipFileNameList.get(0);
			String expectedManifestFileName = theManifestFile.getName();
			assertThat(actualManifestFileName, is (equalTo(expectedManifestFileName)));
			
			for (int i = 0; i < referenceList.getReferencesCount(); i++) {
				Reference reference = referenceList.getReference(i);
				String actualFileName = zipFileNameList.get(i + 1);
				String expectedFileName = reference.getUrl();
				assertThat(actualFileName, is (equalTo(expectedFileName)));
			}
		} catch (IOException ex) {
			fail(ex.getMessage());
		}
	}

	// *****************************************************************************************************
	// *********** addReferenceToExistingFile_fileExists()
	// *****************************************************************************************************
	/**
	 * Tests if a reference to file existing in the storage can be added to an information package. 
	 */
	@Test
	public void addReferenceToExistingFile_fileExists() {
		prepareTest_addReferenceToExistingFile_fileExists_mockObjects();
		Uuid uuidOfInformationPackage = new Uuid();
		boolean resultOfMethod = packagingService.addReferenceToExistingFile("abc", uuidOfInformationPackage.toString());
		assertTrue(resultOfMethod);
	}
	
	/**
	 * Mocks all important objects for the case that the file exists in the storage.
	 */
	private void prepareTest_addReferenceToExistingFile_fileExists_mockObjects() {
		PackagingStorageFacade packagingStorageFacade = mock(PackagingStorageFacade.class);
		when(packagingStorageFacade.fileExistsInStorage("abc")).thenReturn(true);
		when(packagingStorageFacade.findFilePathByChecksum("abc")).thenReturn("/path/to/file");
		packagingService.setPackagingStorageFacade(packagingStorageFacade);
		packagingService.setPackagingSessionFacade(packagingSessionFacade);
	}
	
	// *****************************************************************************************************
	// *********** addReferenceToExistingFile_fileDoesNotExist()
	// *****************************************************************************************************
	/**
	 * Tests if the method addReferenceToExistingFile() recognizes that the file does not exist in the storage. In this case
	 * it returns false and the file has to be uploaded by another method. 
	 */
	@Test
	public void addReferenceToExistingFile_fileDoesNotExist() {
		prepareTest_addReferenceToExistingFile_fileDoesNotExist_mockObjects();
		Uuid uuidOfInformationPackage = new Uuid();
		boolean resultOfMethod = packagingService.addReferenceToExistingFile("abc", uuidOfInformationPackage.toString());
		assertFalse(resultOfMethod);
	}
	
	/**
	 * Mocks all important objects for the case that the file does not exist in the storage.
	 */
	private void prepareTest_addReferenceToExistingFile_fileDoesNotExist_mockObjects() {
		PackagingStorageFacade packagingStorageFacade = mock(PackagingStorageFacade.class);
		when(packagingStorageFacade.fileExistsInStorage("abc")).thenReturn(false);
		packagingService.setPackagingStorageFacade(packagingStorageFacade);
		packagingService.setPackagingSessionFacade(packagingSessionFacade);
	}
	
	// *****************************************************************************************************
	// *********** testSaveArchive()
	// *****************************************************************************************************
	
	/*@Test
	public void testSaveArchive() {
		Uuid uuidOfInformationPackage = new Uuid();
		String uuidAsString = uuidOfInformationPackage.toString();
		WorkingDirectory workingDirectory = createTestWorkingDirectory();
		packagingService.saveArchive(workingDirectory, uuidOfInformationPackage);
		File temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(uuidOfInformationPackage.toString());
		String temporaryDirectoryPath = temporaryDirectory.getAbsolutePath();
		String zipFilePath = temporaryDirectoryPath + ""
		
		packagingOperations = mock(PackagingOperationsImpl.class);
		when(packagingOperations.zipAllFilesInDirectory(temporaryDirectoryPath, uuidAsString)).thenReturn();
		
		informationPackage = sessionService.createNewNotVirtualInformationPackage(
				PackageType.SIU, "myTitle");
		WorkingDirectory workingDirectory = serviceFacade.getWorkingDirectory();
		String testUuid = informationPackage.getUuid();
		Uuid uuid = new Uuid(testUuid);
		
		String actualZipFilePath = serviceFacade.saveArchive(workingDirectory, uuid);
		String expectedZipFilePath = workingDirectory.getStorageDirectory().getAbsolutePath() + 
				File.separator + testUuid + ".zip";
		File zipFile = new File(expectedZipFilePath);
		assertThat(actualZipFilePath, is(equalTo(zipFile.getAbsolutePath())));
		assertTrue(zipFile.exists());
		
		String temporaryDirectoryPath = workingDirectory + File.separator + "temp" + 
				File.separator + testUuid;
		File temporaryDirectory = new File(temporaryDirectoryPath);
		assertFalse(temporaryDirectory.exists());
	}*/
}