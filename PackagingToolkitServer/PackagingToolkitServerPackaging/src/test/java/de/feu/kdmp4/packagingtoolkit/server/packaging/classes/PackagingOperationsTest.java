package de.feu.kdmp4.packagingtoolkit.server.packaging.classes;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.models.classes.WorkingDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingOperations;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * Tests the operations of the module Packaging.
 * @author Christopher Olbertz
 *
 */
public class PackagingOperationsTest {
	// ************** Constants *************
	/**
	 * The content of the new file that should be added in the temporary directory.
	 */
	private static final String NEW_FILE_CONTENT = "abcdefghijklmnopqrstuvwxyz";
	/**
	 * The name of the new file that should be added in the temporary directory.
	 */
	private static final String NEW_FILE_NAME = "newFile";
	/**
	 * The path to the working directory on the server for testing 
	 * purposes. 
	 */
	private static final String WORKING_DIRECTORY = "src/test/resources/workdir";
	/**
	 * The path to the temporary directory.
	 */
	private static final String TEMP_DIRECTORY = "src/test/resources/workdir/temp";
	/**
	 * The number of test files that should be created. 
	 */
	private static final int ZIPPED_FILES_COUNT = 5;
	
	// *********** Attributes ****************
	/**
	 * The directory that should be packed into a zip file.
	 */
	private File dirToZip;	
	/**
	 * The operations object to work with.
	 */
	private PackagingOperations packagingOperations;
	/**
	 * The uuid of the information package for testing.
	 */
	private Uuid testUuid;
	
	// ************ Setup and tear down *************
	/**
	 * Initializes the important objects.
	 */
	@Before
	public void setUp() {
		packagingOperations = new PackagingOperationsImpl();
		testUuid = new Uuid();
	}
	
	/**
	 * Creates some files for this information package that can be used in the tests.
	 * @throws IOException 
	 */
	private List<File> createFilesForInformationPackage() throws IOException {
		createTestWorkingDirectory();
		createTempDirectory();
		createDirectoryForInformationPackage();
		
		List<File> zippedFiles = new ArrayList<>();
		
		for (int i = 0; i < ZIPPED_FILES_COUNT; i++) {
			File zippedFile = new File(dirToZip.getAbsolutePath() + File.separator + "file" + i); 
			zippedFiles.add(zippedFile);
			zippedFile.createNewFile();
		}
		
		return zippedFiles;
	}
	
	/**
	 * Creates a directory for the test information package in the temporary
	 * directory.
	 */
	private void createDirectoryForInformationPackage() {
		dirToZip = new File(TEMP_DIRECTORY + File.separator + testUuid.toString());		
		dirToZip.mkdir();
	}
	
	/**
	 * Creates a working directory for the tests. It creates also the temporary directory
	 * for the information package under test.
	 * @return The working directory that can be used for the tests.
	 */
	private WorkingDirectory createTestWorkingDirectory() {
		File workingDir = new File(WORKING_DIRECTORY);
		
		if (!workingDir.exists()) {
			workingDir.mkdir();
		}
		createTempDirectory();
		
		return new WorkingDirectoryImpl(workingDir.getAbsolutePath());
	}
	
	/**
	 * Creates a temporary directory in the working directory for the tests.
	 * @return A File object that represents the temporary directory.
	 */
	private File createTempDirectory() {
		File tempDir = new File(TEMP_DIRECTORY);
		
		if (!tempDir.exists()) {
			tempDir.mkdir();
		}
		return tempDir;
	}

	/**
	 * Deletes all files and all sub directories in a given path. It deletes also the directory
	 * at the given path itself.
	 * @param path The path that contains the files and directories to delete.
	 */
	private  void deleteFilesAndDirectories(File path) {
		for (File file: path.listFiles()) {
	         if (file.isDirectory())
	        	 deleteFilesAndDirectories(file);
	         file.delete();
	      }
	      path.delete();
	}
	
	/**
	 * Deletes the working directory and all files and directories contained.
	 * @param workingDirectory The working directory that should be deleted.
	 */
	private void deleteWorkingDirectory(WorkingDirectory workingDirectory) {
		String workingDirectoryPath = workingDirectory.getWorkingDirectoryPath();
		File workingDir = new File(workingDirectoryPath);
		deleteFilesAndDirectories(workingDir);
	}
	
	// **************** Test methods ***************
	// *****************************************************************************************************
	// *********** testAddFileToTemporaryDirectory
	// *****************************************************************************************************	
	
	/**
	 * Tests the method for adding a new file to the temporay directory of an information package. The following
	 * constraints have to be satisfied:
	 * <ul>
	 * 	<li>The file has to exist in the temporary directory of the information package.</li>
	 * 	<li>The content of the file has to be the expected content.</li>
	 * </ul>
	 * @throws IOException 
	 */
	@Test
	public void testAddFileToTemporaryDirectory() throws IOException {
		// Test preparation.
		WorkingDirectory workingDirectory = createTestWorkingDirectory();
		prepareTest_testAddFileToTemporaryDirectory_createTemporaryDirectoryForInformationPackage(workingDirectory);
		InputStream inputStreamForFile = prepareTest_testAddFileToTemporaryDirectory_inputStream();
		
		// Run the method under test.
		packagingOperations.addFileToTemporaryDirectory(NEW_FILE_NAME, inputStreamForFile, testUuid.toString(), workingDirectory);

		checkTestResults_testAddFileToTemporaryDirectory(workingDirectory);
		
		deleteWorkingDirectory(workingDirectory);
	}
	
	/**
	 * Creates a File object for the file we are expecting in the test. The file has to exist in the temporary directory 
	 * of the information package. Its name is newFile
	 * @return
	 */
	private File prepareTest_testAddFileToTemporaryDirectory_expectedFile(WorkingDirectory workingDirectory) {
		File expectedFile = workingDirectory.getFileInTempDirectory(
				NEW_FILE_NAME, testUuid.toString());
		return expectedFile;
	}
	
	/**
	 * Creates an input stream for the test. That stream is written in the test file.
	 * @return The input stream.
	 */
	private InputStream prepareTest_testAddFileToTemporaryDirectory_inputStream() {
		return new ByteArrayInputStream(NEW_FILE_CONTENT.getBytes(StandardCharsets.UTF_8));
	}
	
	/**
	 * Creates a temporary directory for the information package for the test. 
	 * @param workingDirectory The working directory that is used for the test. 
	 */
	private void prepareTest_testAddFileToTemporaryDirectory_createTemporaryDirectoryForInformationPackage(WorkingDirectory workingDirectory) {
		File temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(testUuid);
		if (!temporaryDirectory.exists()) {
			temporaryDirectory.mkdirs();
		}
	} 
	
	/**
	 * Checks the conditions for the test.
	 * @param workingDirectory The working directory that is used for the test. 
	 * @throws IOException
	 */
	private void checkTestResults_testAddFileToTemporaryDirectory(WorkingDirectory workingDirectory) throws IOException {
		File expectedFile = prepareTest_testAddFileToTemporaryDirectory_expectedFile(workingDirectory);
		assertTrue(expectedFile.exists());
		
		// Check the content of the file.
		FileReader fileReader = new FileReader(expectedFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String content = bufferedReader.readLine();
		assertThat(content, is(equalTo(NEW_FILE_CONTENT)));
		bufferedReader.close();
	}
	
	// *****************************************************************************************************
	// *********** testDeleteFileFromTemporaryDirectory
	// *****************************************************************************************************	
	/**
	 * Checks if deleting a file from the temporary directory of an
	 * information package is working. Before the test is started
	 * it is checked if the file is existing. After the test it is
	 * checked if the file has been deleted.
	 * @throws IOException 
	 */
	@Test
	public void testDeleteFileFromTemporaryDirectory() throws IOException {
		// Test preparation.
		File fileToDelete = prepareTest_testDeleteFileFromTemporaryDirectory_getFileToDelete();
		WorkingDirectory workingDirectory = createTestWorkingDirectory();
		
		packagingOperations.deleteFileFromTemporaryDirectory(fileToDelete.getAbsolutePath(), 
				testUuid.toString(), workingDirectory);
		
		checkTestResults_testDeleteFileFromTemporaryDirectory(fileToDelete);
		
		deleteWorkingDirectory(workingDirectory);
	}
	
	/**
	 * Determines the file that should be deleted in the test. Asserts that the file exists.
 	 * @return A File object that represents the file that should be deleted in the test.
	 * @throws IOException 
	 */
	private File prepareTest_testDeleteFileFromTemporaryDirectory_getFileToDelete() throws IOException {
		List<File> zippedFiles = createFilesForInformationPackage();
		File fileToDelete = zippedFiles.get(0);
		assertTrue(fileToDelete.exists());
		return fileToDelete;
	}
	
	/**
	 * Checks if the file was deleted.
	 * @param fileToDelete The file that has to be deleted.
	 */
	private void checkTestResults_testDeleteFileFromTemporaryDirectory(File fileToDelete) {
		assertTrue(fileToDelete.exists());
	}
	

	// *****************************************************************************************************
	// *********** testZipAllFilesInDirectory
	// *****************************************************************************************************	
	/**
	 * Tests the method zipAllFilesInDirectory. Because the files are not sorted in the zip file,
	 * the test creates a list with file names, opens the zip files, inserts the file names into the
	 * list and sorts the list. Then, the list with the actual file names is compared with the
	 * list with the expected file names.
	 * The following checks are performed:
	 * <ul>
	 * 	<li>Is the return value of the method the correct name of the zip file? The correct file name is the 
	 * uuid of the information package with the suffix .zip in the working directory.</li>
	 *  <li>Does the zip file exist?</li>
	 *  <li>Has the zip file the correct name?</li>
	 *  <li>Does the zip file contains the correct number of files?</li>
	 *  <li>Does the zip file contain the correct files?</li>
	 * </ul>
	 * @throws IOException 
	 */
	@Test
	public void testZipAllFilesInDirectory() throws IOException {
		// Test preparation.
		WorkingDirectory workingDirectory = createTestWorkingDirectory();
		createFilesForInformationPackage();
		
		String zipFileName = packagingOperations.zipAllFilesInDirectory(dirToZip.getAbsolutePath(), testUuid.toString());
		checkTestResults_testZipAllFilesInDirectory_zipFileName(zipFileName);
		checkTestResults_testZipAllFilesInDirectory_zipFileExists(zipFileName);
		checkTestResults_testZipAllFilesInDirectory_zipFileContent(zipFileName);
		
		deleteWorkingDirectory(workingDirectory);
	}
	
	/**
	 * Checks if the name of the created zip file is correct.
	 * @param zipFileName The name that has to be checked.
	 */
	private void checkTestResults_testZipAllFilesInDirectory_zipFileName(String zipFileName) {
		String expectedZipFileName = dirToZip.getParentFile().getAbsolutePath() + File.separator + 
				testUuid.toString() + File.separator + testUuid.toString() + 
				PackagingToolkitFileUtils.ZIP_FILE_EXTENSION;
		assertThat(zipFileName, is(equalTo(expectedZipFileName)));
	}
	
	/**
	 * Checks if the zip file has been created.
	 * @param zipFileName The name of the zip file that is checked.
	 */
	private void checkTestResults_testZipAllFilesInDirectory_zipFileExists(String zipFileName) {
		File testZipFile = new File(zipFileName);
		boolean fileExists = testZipFile.exists();
		assertTrue(fileExists);
	}
	
	/**
	 * Checks the files contained in the created zip file. It creates a sorted list with the names of the 
	 * contained files. The list is sorted because it is easier to check.
	 * @param zipFileName The name of the created zip file.
	 */
	private void checkTestResults_testZipAllFilesInDirectory_zipFileContent(String zipFileName) {
		File testZipFile = new File(zipFileName);
		
		// Check if the correct files were zipped.
		try (ZipFile zipFile = new ZipFile(testZipFile)) {
			List<String> zipFileNameList = createSortedListWithNamesOfFilesInZipFile(zipFile);
			checkTestResults_testZipAllFilesInDirectory_zippedFilesCount(zipFileNameList);
			checkTestResults_testZipAllFilesInDirectory_zippedFiles(zipFileNameList);
		} catch (IOException ex) {
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Checks the number of files contained in the zip file.
	 * @param zipFileName The name of the created zip file.
	 */
	private void checkTestResults_testZipAllFilesInDirectory_zippedFilesCount(List<String> zipFileNameList) {
		assertThat(zipFileNameList.size(), is(equalTo(ZIPPED_FILES_COUNT)));
	}
	
	/**
	 * Creates a sorted list that contains the names of the files contained in a zip file.
	 * @param zipFile The zip file with the files.
	 * @return The sorted list with the file names.
	 */
	private List<String> createSortedListWithNamesOfFilesInZipFile(ZipFile zipFile) {
		Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
		ZipEntry zipEntry = null;
		// Create a list with the zip file names for sorting and comparing with the expected list.
		List<String> zipFileNameList = new ArrayList<>();
		while(zipEntries.hasMoreElements()) {
			zipEntry = zipEntries.nextElement();
			zipFileNameList.add(zipEntry.getName());
		}
		
		Collections.sort(zipFileNameList);
		return zipFileNameList;
	}
	
	/**
	 * Asserts that the correct files were packed in the zip file.
	 * @param zipFileNameList A sorted list with the file names.
	 * @throws IOException
	 */
	private void checkTestResults_testZipAllFilesInDirectory_zippedFiles(List<String> zipFileNameList) throws IOException {
		List<File> zippedFiles = createFilesForInformationPackage();
		for (int i = 0; i < ZIPPED_FILES_COUNT; i++) {
			String actualFileName = zipFileNameList.get(i);
			String expectedFileName = zippedFiles.get(i).getName();
			assertThat(actualFileName, is(equalTo(expectedFileName)));
		}
	}
}
