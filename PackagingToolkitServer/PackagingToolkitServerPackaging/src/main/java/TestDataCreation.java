import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.server.packaging.classes.PackagingServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingService;

/**
 * Nur zum Erzeugen von Testdaten fuer das Downloaden, bevor ein Informationspaket erstellt werden 
 * kann. Kann spaeter geloescht werden.
 * @author christopher
 *
 */
// DELETE_ME
public class TestDataCreation {
	/*	private static final String PATH_FILE_1 = "/home/christopher/top_head.jsp";
	private static final String PATH_FILE_2 = "/home/christopher/witze.txt";
	
	public static void main(String [] args) throws FileNotFoundException, IOException {
		PackagingService packagingService = new PackagingServiceImpl();
		
		UuidResponse uuidOfFirstIP = new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357572");
		UuidResponse uuidOfSecondIP = new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357573");
		UuidResponse uuidOfThirdIP = new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357574");
		
		
		File file = new File(PATH_FILE_1);
		try (InputStream inputStream = new FileInputStream(file)) {
			packagingService.addDigitalObjectToInformationPackage(uuidOfFirstIP.toString(), "top_head.jsp", inputStream);
		}
		
		file = new File(PATH_FILE_2);
		try (InputStream inputStream = new FileInputStream(file)) {
			packagingService.addDigitalObjectToInformationPackage(uuidOfSecondIP.toString(), "witze.txt", inputStream);
		}
		
		IndividualResponse individualResponse1 = new IndividualResponse();
		individualResponse1.setClassIri("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#Representation_Information");
		individualResponse1.setUuid(new UuidResponse());
		individualResponse1.setUuidOfInformationPackage(uuidOfFirstIP);
		DatatypePropertyResponse property1 = new DatatypePropertyResponse();
		property1.setDatatype(OntologyDatatypeResponse.DOUBLE);
		property1.setPropertyName("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasDoubleProperty");
		property1.setValue("0.5");
		individualResponse1.addOntologyProperty(property1);
		
		DatatypePropertyResponse property2 = new DatatypePropertyResponse();
		property2.setDatatype(OntologyDatatypeResponse.FLOAT);
		property2.setPropertyName("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasFloatProperty");
		property2.setValue("0.05");
		individualResponse1.addOntologyProperty(property2);
		
		DatatypePropertyResponse property3 = new DatatypePropertyResponse();
		property3.setDatatype(OntologyDatatypeResponse.INTEGER);
		property3.setPropertyName("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasIntegerProperty");
		property3.setValue("500");
		individualResponse1.addOntologyProperty(property3);
		
		DatatypePropertyResponse property4 = new DatatypePropertyResponse();
		property4.setDatatype(OntologyDatatypeResponse.DOUBLE);
		property4.setPropertyName("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasDoubleProperty");
		property4.setValue("0.4");
		individualResponse1.addOntologyProperty(property4);
		
		DatatypePropertyResponse property5 = new DatatypePropertyResponse();
		property5.setDatatype(OntologyDatatypeResponse.FLOAT);
		property5.setPropertyName("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasFloatProperty");
		property5.setValue("0.04");
		individualResponse1.addOntologyProperty(property5);
		
		DatatypePropertyResponse property6 = new DatatypePropertyResponse();
		property6.setDatatype(OntologyDatatypeResponse.INTEGER);
		property6.setPropertyName("http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasIntegerProperty");
		property6.setValue("400");
		individualResponse1.addOntologyProperty(property6);
		
		packagingService.saveIndividualForInformationPackage(uuidOfFirstIP, individualResponse1);
		
		packagingService.saveInformationPackage(uuidOfFirstIP);
		//packagingService.saveArchive(uuidOfSecondIP);
		//packagingService.saveArchive(uuidOfThirdIP);
	}*/
}