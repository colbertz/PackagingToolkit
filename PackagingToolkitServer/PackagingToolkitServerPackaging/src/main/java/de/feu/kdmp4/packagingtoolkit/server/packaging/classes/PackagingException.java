package de.feu.kdmp4.packagingtoolkit.server.packaging.classes;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class PackagingException extends UserException {
	private static final long serialVersionUID = 7499416379461803236L;
	private static final String SUMMARY = I18nExceptionUtil.getSummaryPackagingExceptionString();
	
	private PackagingException(String message) {
		super(message);
	}
	
	public static PackagingException getArchiveDirectoryDoesNotExist(String archiveDirectory) {
		String message = I18nExceptionUtil.getArchiveDirectoryDoesNotExistString(archiveDirectory);
		return new PackagingException(message);
	}
	
	public static PackagingException getArchiveDirectoryMayNotBeEmpty(String archiveDirectory) {
		String message = I18nExceptionUtil.getArchiveDirectoryMayNotBeEmptyString(archiveDirectory);
		return new PackagingException(message);
	}
	
	public static PackagingException getZipFileCouldNotBeCreated(String filename) {
		String message = I18nExceptionUtil.getZipFileCouldNotBeCreatedString(filename);
		return new PackagingException(message);
	} 

	@Override
	public String getSummary() {
		return SUMMARY;
	}

}
