/**
 * Contains the implementations of the classes for the packaging module.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.packaging.classes;