package de.feu.kdmp4.packagingtoolkit.server.packaging.classes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingOperations;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * The implementation of the operations of the packaging module. The
 * depencies are injected by Spring. The classes of java.util are used 
 * for the zipping process.
 * @author Christopher Olbertz
 *
 */
public class PackagingOperationsImpl implements PackagingOperations {
	
	@Override
	public void addFileToTemporaryDirectory(final String filename, final InputStream inputStream, 
			final String uuid, final WorkingDirectory workingDirectory) {
		final File digitalObject = workingDirectory.getFileInTempDirectory(filename, uuid);
		
		try {
			PackagingToolkitFileUtils.createFileFromInputStream(digitalObject, inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void writeMetadataToFile(final Reference reference, final MetadataExchangeCollection metadata,
			final WorkingDirectory workingDirectory, final String uuidOfInformationPackage) {
		final File metadataFile = workingDirectory.getFileInTempDirectory(PackagingToolkitConstants.FILENAME_METADATA_FITS, 
				uuidOfInformationPackage);
		
		try {
			if (!metadataFile.exists()) {
				metadataFile.createNewFile();
			}
			
			try (final FileWriter fileWriter = new FileWriter(metadataFile);
					 final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
					bufferedWriter.write(reference.getUrl());
					bufferedWriter.newLine();
					
					for (int i = 0; i < metadata.getMetadataCount(); i++) {
						final MetadataExchange metadataExchange = metadata.getMetadataAt(i);
						final String metadataLine = metadataExchange.getMetadataName() + ": " + metadataExchange.getMetadataValue();
						bufferedWriter.write(metadataLine);
						bufferedWriter.newLine();
					}
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void deleteFileFromTemporaryDirectory(final String filename, final String uuid, 
			final WorkingDirectory workingDirectory) {
		final File temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuid(uuid);
		final String filePath = temporaryDirectory.getAbsolutePath() + File.separator + filename;
		final File digitalObject = new File(filePath);
		digitalObject.delete();
	}
	
	@Override
	public String zipAllFilesInDirectory(final String directoryPath, 
			final   String filenameWithoutSuffix) {
		final byte[] buffer = new byte[PackagingToolkitConstants.BITS_OF_ONE_BYTE];
		
		final File directory = new File(directoryPath); 		
		final String zipFilename = directory.getAbsolutePath() + File.separator + filenameWithoutSuffix + 
				PackagingToolkitFileUtils.ZIP_FILE_EXTENSION;
		
		// Zip the files in directory into an archive with the name zipFilename.
		try (FileOutputStream fileOutputStream = new FileOutputStream(zipFilename);
			final ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {
			final File[] files = directory.listFiles();
			if (files != null) {
				// Put every file as an entry in the zip file.
				for (final File fileToZip: files) {
					/* The zip file itself already exists in the directory but it may not 
					   be packed into the archive. */
					if (!fileToZip.getName().contains(filenameWithoutSuffix)) {
						final ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
						zipOutputStream.putNextEntry(zipEntry);
						final FileInputStream fileInputStreamToZip = new FileInputStream(fileToZip);
						
						int length = 0;
						while ((length = fileInputStreamToZip.read(buffer)) > 0) {
							zipOutputStream.write(buffer, 0, length);
						}
						
						fileInputStreamToZip.close();
					}
				}
			}
			return zipFilename;
			
		} catch(IOException ex) {
			throw PackagingException.getZipFileCouldNotBeCreated(zipFilename);
		}
	}
}
