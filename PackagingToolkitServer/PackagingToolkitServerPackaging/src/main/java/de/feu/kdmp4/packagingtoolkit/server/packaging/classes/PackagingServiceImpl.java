package de.feu.kdmp4.packagingtoolkit.server.packaging.classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.PackagingConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.PackagingDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.factories.ExchangeModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.metadata.facades.PackagingMetaDataFacade;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.TripleStatementCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.facades.PackagingOntologyFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.ServerResponseFactory;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingOperations;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingService;
import de.feu.kdmp4.packagingtoolkit.server.serialization.facade.PackagingSerializationFacade;
import de.feu.kdmp4.packagingtoolkit.server.session.facade.PackagingSessionFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.facade.PackagingStorageFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.interfaces.StorageOperations;

public class PackagingServiceImpl implements PackagingService {
	/**
	 * Contains the methods for accessing the business logic of the module Configuration.
	 */
	private PackagingConfigurationFacade packagingConfigurationFacade;
	/**
	 * Contains the methods for accessing the business logic of the module Database.
	 */
	private PackagingDatabaseFacade packagingDatabaseFacade;
	/**
	 * The object for accessing the methods of the operations layer.
	 */
	private PackagingOperations packagingOperations;
	/**
	 * Contains the methods for accessing the business logic of the module Serialization.
	 */
	private PackagingSerializationFacade packagingSerializationFacade;
	/**
	 * Contains the methods for accessing the business logic of the module Storage.
	 */
	private PackagingStorageFacade packagingStorageFacade;
	/**
	 * The object for accessing the operations of the storage module.
	 */	
	private StorageOperations storageOperations;
	/**
	 * Contains the methods for accessing the business logic of the module Ontology.
	 */
	private PackagingOntologyFacade packagingOntologyFacade;
	/**
	 * Contains the methods for accessing the business logic of the module Session.
	 */
	private PackagingSessionFacade packagingSessionFacade;
	/**
	 * Contains a reference to the object that creates model objects for ontologies.
	 */
	private OntologyModelFactory ontologyModelFactory;
	
	private OntologyCache ontologyCache;
	/**
	 * Contains the methods for accessing the business logic of the module MetaData.
	 */
	private PackagingMetaDataFacade packagingMetaDataFacade;
	
	@Override
	public void addDigitalObjectToInformationPackage(final String uuidOfInformationPackageAsString, final String filename,
			final byte[] dataOfFile) {
		final Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getUuid(uuidOfInformationPackageAsString);
		final boolean fileAlreadyExists = packagingStorageFacade.fileExistsInStorage(dataOfFile);
		Uuid uuidOfDigitalObject = null;
		
		if (!fileAlreadyExists) {
			uuidOfDigitalObject = packagingStorageFacade.saveFileDataInStorage(dataOfFile, filename);
		}
		// Metadaten anfordern.
		packagingSessionFacade.addLocalFileReferenceToInformationPackage(filename, uuidOfInformationPackage, uuidOfDigitalObject);
	}
	
	@Override
	public boolean addReferenceToExistingFile(final String md5Checksum, final String uuidOfInformationPackageAsString) {
		final Uuid uuidOfInformationPackage = PackagingToolkitModelFactory.getUuid(uuidOfInformationPackageAsString);
		final boolean fileAlreadyExists = packagingStorageFacade.fileExistsInStorage(md5Checksum); 
		
		if (fileAlreadyExists) {
			final Optional<DigitalObjectExchange> optionalWithDigitalObject = packagingDatabaseFacade.findDigitalObjectByMd5Checksum(md5Checksum);
			if (optionalWithDigitalObject.isPresent()) {
				final DigitalObjectExchange digitalObjectExchange = optionalWithDigitalObject.get(); 
				final String filename = digitalObjectExchange.getFilePathInStorage(); 
				final Uuid uuidOfDigitalObject = digitalObjectExchange.getUuidOfDigitalObject();
				packagingSessionFacade.addLocalFileReferenceToInformationPackage(filename, uuidOfInformationPackage, uuidOfDigitalObject);
				return true;
			}
		} 
		return false;
	}
	
	@Override
	public File findFileByChecksum(final String md5Checksum) {
		return packagingStorageFacade.findFileByChecksum(md5Checksum);
	}
	
	@Override
	public void addRemoteReferenceToInformationPackage(final String uuidOfInformationPackage, final String url) {
		final Uuid uuid = PackagingToolkitModelFactory.getUuid(uuidOfInformationPackage);
		packagingSessionFacade.addRemoteFileReferenceToInformationPackage(url, uuid);
	}
	
	@Override
	public File downloadVirtualInformationPackage(final UuidResponse uuidResponseOfInformationPackage) {
		final WorkingDirectory workingDirectory = packagingConfigurationFacade.getWorkingDirectory();
		final String uuidOfInformationPackageAsString = uuidResponseOfInformationPackage.toString();
		
		workingDirectory.deleteTemporaryDirectoryIfExists(uuidOfInformationPackageAsString);
		/*
		 * Because only OAIORE is possible for the serialization process the serialization format is
		 * set here and not send by the client. If other formats are implemented, the user can
		 * choose the desired format and it has to be send to the server.
		 */
		return completeZipFile(uuidOfInformationPackageAsString, SerializationFormat.OAIORE);
	}
	
	@Override
	public File downloadNotVirtualInformationPackage(final UuidResponse uuidResponseOfInformationPackage) {
		final WorkingDirectory workingDirectory = packagingConfigurationFacade.getWorkingDirectory();
		final String uuidOfInformationPackageAsString = uuidResponseOfInformationPackage.toString();
		workingDirectory.deleteTemporaryDirectoryIfExists(uuidOfInformationPackageAsString);
		
		final ReferenceCollection references = packagingDatabaseFacade.findReferencesOfInformationPackage(uuidOfInformationPackageAsString);
		addDigitalObjectFilesToTemporaryDirectory(references, uuidOfInformationPackageAsString);
		
		/*
		 * Because only OAIORE is possible for the serialization process the serialization format is
		 * set here and not send by the client. If other formats are implemented, the user can
		 * choose the desired format and it has to be send to the server.
		 */
		return completeZipFile(uuidOfInformationPackageAsString, SerializationFormat.OAIORE);
	}
	
	@Override
	public ReferenceListResponse findAllReferencesOfInformationPackage(final UuidResponse uuidResponseOfInformationPackage) {
		final String uuidOfInformationPackageAsString = uuidResponseOfInformationPackage.toString();
		final ReferenceCollection references = packagingDatabaseFacade.findReferencesOfInformationPackage(uuidOfInformationPackageAsString);
		final ReferenceListResponse referencesResponse = ServerResponseFactory.toResponse(references, uuidResponseOfInformationPackage);
		return referencesResponse;
	}
	
	/**
	 * Copies the files of the digital objects an information package references to in the temporary 
	 * directory of this information package.
	 * @param references The references that point to the digital objects.
	 * @param uuidOfInformationPackageAsString The uuid of the information package whose 
	 * digital objects should be copied in its temporary directory.
	 */
	private void addDigitalObjectFilesToTemporaryDirectory(final ReferenceCollection references, final String uuidOfInformationPackageAsString) {
		for (int i = 0; i < references.getReferencesCount(); i++) {
			final Reference reference = references.getReference(i);
			final String filename = reference.getUrl();
			final WorkingDirectory workingDirectory = packagingConfigurationFacade.getWorkingDirectory();
			final File digitalObjectFile = workingDirectory.getFileInStorageDirectory(filename);
			writeMetadataToFile(reference, uuidOfInformationPackageAsString);
			
			InputStream inputStream;
			try {
				inputStream = new FileInputStream(digitalObjectFile);
				packagingOperations.addFileToTemporaryDirectory(filename, 
						inputStream, uuidOfInformationPackageAsString, workingDirectory);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Calls the methods for writing the metadata of a file in a text file in the temporary directory. The method
	 * checks if the reference points to a local file on the server because there are no metadata extracted
	 * of a remote file. 
	 * @param reference A reference to the file whose metadata we want to write in the text file.
	 */
	private void writeMetadataToFile(final Reference reference, final String uuidOfInformationPackage) {
		if (reference.isLocalFileReference()) {
			final Uuid uuidOfFile = reference.getUuidOfFile();
			final MetadataExchangeCollection metadataOfFile = packagingDatabaseFacade.findMetadataOfFileByUuid(uuidOfFile);
			final WorkingDirectory workingDirectory = packagingConfigurationFacade.getWorkingDirectory();
			packagingOperations.writeMetadataToFile(reference, metadataOfFile, workingDirectory, uuidOfInformationPackage);
		}
	}
	
	@Override
	public void saveMetadata(final MediatorDataListResponse mediatorDataList) {
		final String uuidOfDigitalObject = mediatorDataList.getUuidOfFile().toString();
		final MetadataExchangeCollection metadataCollection = ExchangeModelFactory.createMetadataExchangeCollection(uuidOfDigitalObject);
		
		for (final MediatorDataResponse mediatorDataResponse: mediatorDataList.getMediatorData()) {
			final String metadataName = mediatorDataResponse.getName();
			final String metadataValue = mediatorDataResponse.getValue();
			metadataCollection.addMetadata(metadataName, metadataValue);
		}
		
		packagingDatabaseFacade.saveMetadataForDigitalObject(metadataCollection);
	}
	
	/**
	 * Processes the last steps for completing a zip file for download: adding a manifest to the temporary
	 * directory and pack the files into a zip file.
	 * @param uuidOfInformationPackageAsString The uuid of the information package whose manifest file should
	 * be created.
	 * @param serializationFormat The serialization format that should be used. At the moment only OAIORE is possible.
	 */
	private File completeZipFile(final String uuidOfInformationPackageAsString, final SerializationFormat serializationFormat) {
		final WorkingDirectory workingDirectory = packagingConfigurationFacade.getWorkingDirectory();
		final File temporaryDirectory = workingDirectory.getTemporaryDirectoryForUuidOrCreate(uuidOfInformationPackageAsString);
		
		addManifestToTemporaryDirectory(uuidOfInformationPackageAsString, serializationFormat);
		final String zipFilePath = packagingOperations.zipAllFilesInDirectory(temporaryDirectory.getAbsolutePath(), 
				uuidOfInformationPackageAsString);
		return new File(zipFilePath);
	}
	
	/**
	 * Creates a manifest for an information package and saves this file in the temporary directory that contains the files
	 * that are zipped and provided for download.
	 * @param uuidOfInformationPackageAsString The uuid of the information package whose manifest file should
	 * be created.
	 * @param serializationFormat The serialization format that should be used. At the moment only OAIORE is possible.
	 */
	private void addManifestToTemporaryDirectory(final String uuidOfInformationPackageAsString, 
			final SerializationFormat serializationFormat) {
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(uuidOfInformationPackageAsString);
		final OntologyIndividualCollection ontologyIndividualList = packagingOntologyFacade.findIndiviualsByInformationPackage(uuidOfInformationPackageAsString);
		
		final WorkingDirectory workingDirectory = packagingConfigurationFacade.getWorkingDirectory();
		final File manifest = packagingSerializationFacade.createManifest(uuidOfInformationPackage, ontologyIndividualList,
				serializationFormat);
		
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(manifest);
			packagingOperations.addFileToTemporaryDirectory(PackagingToolkitConstants.NAME_OF_MANIFEST_FILE, 
					inputStream, uuidOfInformationPackageAsString, workingDirectory);
			// Delete the temporary manifest file.
			manifest.delete();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void saveIndividualForInformationPackage(final UuidResponse uuidResponse, final IndividualResponse individualResponse) {
		final String uuidAsString = uuidResponse.toString();
		final String classIri = individualResponse.getClassIri();
		
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(uuidAsString);
		final OntologyIndividual individual = ontologyModelFactory.createOntologyIndividual(individualResponse);
		final OntologyClass ontologyClass = packagingOntologyFacade.findOntologyClassByIri(classIri);
		individual.setOntologyClass(ontologyClass);
		//packagingOntologyFacade.validateIndividual(individual);
		packagingSessionFacade.addIndividualToInformationPackage(uuidOfInformationPackage, individual);
	}
	
	@Override
	public PackageResponse createNewSubmissionInformationPackage(final String title) {
		final PackageResponse packageResponse = packagingSessionFacade.createNewSubmissionInformationPackage(title);
		return packageResponse;
	}
	
	@Override
	public PackageResponse createNewSubmissionInformationUnit(final String title) {
		final PackageResponse packageResponse = packagingSessionFacade.createNewSubmissionInformationUnit(title);
		return packageResponse;
	}
	
	@Override
	public ArchiveListResponse findAllArchives() {
		final InformationPackageExchangeCollection informationPackageExchanges = packagingDatabaseFacade.findAllInformationPackages();
		final ArchiveListResponse archiveListResponse = ResponseModelFactory.getArchiveListResponse();
		
		for (int i = 0; i < informationPackageExchanges.getInformationPackageExchangeCount(); i++) {
			final InformationPackageExchange informationPackageExchange = informationPackageExchanges.getInformationPackageExchangeAt(i);
			final long archiveId = informationPackageExchange.getInformationPackageId();
			final PackageType packageType = informationPackageExchange.getPackageType();
			final String title = informationPackageExchange.getTitle();
			final UuidResponse uuid = ResponseModelFactory.getUuidResponse(informationPackageExchange.getUuid());
			final LocalDateTime creationDate = informationPackageExchange.getCreationDate();
			final LocalDateTime lastmodificationDate = informationPackageExchange.getLastModifiedDate();
			final SerializationFormat serializationFormat = informationPackageExchange.getSerializationFormat();
			
			final ArchiveResponse archiveResponse = ResponseModelFactory.getArchiveResponse(archiveId, packageType, 
					serializationFormat, title, uuid, creationDate, lastmodificationDate);
			final String uuidOfInformationPackage = informationPackageExchange.getUuid();
			final DigitalObjectListResponse digitalObjects = determineDigitalObjectsInformationPackage(uuidOfInformationPackage);
			archiveResponse.setDigitalObjects(digitalObjects);
			archiveListResponse.addArchiveToList(archiveResponse);
		}
		
		return archiveListResponse;
	}
	
	/**
	 * Determines the digital objects of an information package. The resulting objects also contain the metadata
	 * of the files. 
	 * @param uuidOfInformationPackage The uuid of the information package whose digital objects we want to receive.
	 * @return A list with the digital objects and their metadata.
 	 */
	private DigitalObjectListResponse determineDigitalObjectsInformationPackage(final String uuidOfInformationPackage) {
		final ReferenceCollection referencesInInformationPackage = packagingDatabaseFacade.findReferencesOfInformationPackage
				(uuidOfInformationPackage);
		final DigitalObjectListResponse digitalObjects = ResponseModelFactory.createEmptyDigitalObjectListResponse();
		
		for (int i = 0; i < referencesInInformationPackage.getReferencesCount(); i++) {
			final Reference reference = referencesInInformationPackage.getReference(i);
			final Uuid uuidOfFile = reference.getUuidOfFile();
			final MetadataExchangeCollection metadataFromDatabase = packagingDatabaseFacade.findMetadataOfFileByUuid(uuidOfFile);
			final String filename = reference.getUrl();
			final UuidResponse informationPackageUuid = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
			final DigitalObjectResponse digitalObject = ResponseModelFactory.getDigitalObjectResponse(filename, informationPackageUuid);
			
			for (int j = 0; j < metadataFromDatabase.getMetadataCount(); j++) {
				final MetadataExchange metadataExchange = metadataFromDatabase.getMetadataAt(j);
				final String key = metadataExchange.getMetadataName();
				final String value = metadataExchange.getMetadataValue();
				digitalObject.addMetadata(key, value);
			}
			
			digitalObjects.addDigitalObjectToList(digitalObject);
		}
		
		return digitalObjects;
	}
	
	@Override
	public void saveInformationPackage(final UuidResponse uuidResponse) {
		Uuid uuid = new Uuid(uuidResponse.toString());
		packagingSessionFacade.saveInformationPackageAndRemoveFromSession(uuid); 
	}
	
	@Override
	public void updateIndividualInInformationPackage(final UuidResponse uuidResponse, final IndividualResponse individualResponse) {
		final String uuidAsString = uuidResponse.toString();
		final String classIri = individualResponse.getClassIri();
		
		final Uuid uuid = ServerModelFactory.createUuid(uuidAsString);
		final OntologyIndividual individual = ontologyModelFactory.createOntologyIndividual(individualResponse);
		final OntologyClass ontologyClass = packagingOntologyFacade.findOntologyClassByIri(classIri);
		individual.setOntologyClass(ontologyClass);
		//packagingOntologyFacade.validateIndividual(individual);
		packagingSessionFacade.editIndividualInInformationPackage(uuid, individual);
	}
	
	@Override
	public String getTemporaryDirectoryForUuid(final String workingDirectoryPath, final Uuid uuid) {
		return workingDirectoryPath + File.separator + uuid.toString();
	}
	
	@Override
	public void cancelInformationPackageCreation(final String uuidOfInformationPackage) {
		final Uuid uuid = new Uuid(uuidOfInformationPackage);
		if (packagingSessionFacade.informationPackageExistsInSession(uuid)) { 
			//packagingStorageFacade.deleteInformationPackage(uuidOfInformationPackage);
			packagingSessionFacade.removeInformationPackageFromSession(uuid);
		}
	}
	
	@Override
	public Optional<Uuid> findUuidOfFileByChecksum(final String md5Checksum) {
		return packagingStorageFacade.findUuidOfFileByChecksum(md5Checksum);
	}
	
	@Override
	public void setPackagingOperations(PackagingOperations packagingOperations) {
		this.packagingOperations = packagingOperations;
	}

	@Override
	public void setOntologyModelFactory(OntologyModelFactory ontologyModelFactory) {
		this.ontologyModelFactory = ontologyModelFactory;
	}
	
	@Override
	public void setPackagingConfigurationFacade(PackagingConfigurationFacade packagingConfigurationFacade) {
		this.packagingConfigurationFacade = packagingConfigurationFacade;
	}
	
	@Override
	public void setPackagingOntologyFacade(PackagingOntologyFacade packagingOntologyFacade) {
		this.packagingOntologyFacade = packagingOntologyFacade;
	}
	
	@Override
	public void setPackagingStorageFacade(PackagingStorageFacade packagingStorageFacade) {
		this.packagingStorageFacade = packagingStorageFacade;
	}
	
	@Override
	public void setPackagingSessionFacade(PackagingSessionFacade packagingSessionFacade) {
		this.packagingSessionFacade = packagingSessionFacade;
	}
	
	@Override
	public void setPackagingSerializationFacade(PackagingSerializationFacade packagingSerializationFacade) {
		this.packagingSerializationFacade = packagingSerializationFacade;
	}
	
	@Override
	public void setPackagingDatabaseFacade(PackagingDatabaseFacade packagingDatabaseFacade) {
		this.packagingDatabaseFacade = packagingDatabaseFacade;
	}

	@Override
	public void loadInformationPackage(final String uuid) {
		final Uuid uuidOfInformationPackage = new Uuid(uuid);
		packagingSessionFacade.loadInformationPackage(uuidOfInformationPackage);
	}

	@Override
	public void addMetadataToInformationPackage(final MediatorDataListResponse mediatorDataListResponse) {
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(mediatorDataListResponse.getUuidOfInformationPackage().toString());
		final Uuid uuidOfFile = ServerModelFactory.createUuid(mediatorDataListResponse.getUuidOfFile().toString());
		final TripleStatementCollection statementsForMetaData = packagingMetaDataFacade.createStatements(mediatorDataListResponse);
		packagingSessionFacade.addMetadataToInformationPackage(statementsForMetaData, uuidOfInformationPackage, uuidOfFile);
	}
	
	@Override
	public boolean hasDigitalObjectSavedMetadata(final Uuid uuidOfDigitalObject) {
		return packagingDatabaseFacade.hasDigitalObjectSavedMetadata(uuidOfDigitalObject);
	}
	
	@Override
	public void setOntologyCache(OntologyCache ontologyCache) {
		this.ontologyCache = ontologyCache;
	}
	
	@Override
	public void setPackagingMetaDataFacade(PackagingMetaDataFacade packagingMetaDataFacade) {
		this.packagingMetaDataFacade = packagingMetaDataFacade;
	}

	@Override
	public MetadataExchangeCollection findMetadataOfFileByUuid(final Uuid uuidOfFile) {
		return packagingDatabaseFacade.findMetadataOfFileByUuid(uuidOfFile);
	}

	@Override
	public IndividualListResponse findAllIndividualsOfInformationPackage(final UuidResponse uuidOfInformationPackage) {
		final String uuidAsString = uuidOfInformationPackage.toString();
		
		final OntologyIndividualCollection individuals = packagingOntologyFacade.findIndiviualsByInformationPackage(uuidAsString);
		return ServerResponseFactory.fromOntologyIndividualList(individuals);
	}

	@Override
	public void assignIndividualsToInformationPackage(final UuidListResponse uuids) {
		final UuidResponse uuidOfInformationPackageResponse = uuids.getUuidAt(0);
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(uuidOfInformationPackageResponse.toString());
		final UuidList uuidsOfIndividuals = PackagingToolkitModelFactory.createEmptyUuidList();
		
		for (int i = 1; i < uuids.getUuidCount(); i++) {
			final UuidResponse uuidResponse = uuids.getUuidAt(i);
			final Uuid uuid = PackagingToolkitModelFactory.getUuid(uuidResponse.toString());
			uuidsOfIndividuals.addUuidToList(uuid);
		}
		
		packagingSessionFacade.assignIndividualsToInformationPackage(uuidOfInformationPackage, uuidsOfIndividuals);
	}
	
	@Override
	public void unassignIndividualsFromInformationPackage(final UuidListResponse uuids) {
		final UuidResponse uuidOfInformationPackageResponse = uuids.getUuidAt(0);
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(uuidOfInformationPackageResponse.toString());
		final UuidList uuidsOfIndividuals = PackagingToolkitModelFactory.createEmptyUuidList();
		
		for (int i = 1; i < uuids.getUuidCount(); i++) {
			final UuidResponse uuidResponse = uuids.getUuidAt(i);
			final Uuid uuid = PackagingToolkitModelFactory.getUuid(uuidResponse.toString());
			uuidsOfIndividuals.addUuidToList(uuid);
		}
		
		packagingSessionFacade.unassignIndividualsFromInformationPackage(uuidOfInformationPackage, uuidsOfIndividuals);
	}

	@Override
	public void assignTaxonomyIndividualsToInformationPackage(final IriListResponse irisOfIndividuals) {
		final IriCollection iriCollection = ServerModelFactory.createEmptyIriCollection();
		final String iriOfInformationPackageAsString = irisOfIndividuals.getIriAt(0).toString();
		final Iri iriOfInformationPackage = ServerModelFactory.createIri(iriOfInformationPackageAsString);
		
		for (int i = 1; i < irisOfIndividuals.getIriCount(); i++) {
			final String iriAsString = irisOfIndividuals.getIriAt(i).toString();
			final Iri iri = ServerModelFactory.createIri(iriAsString);
			iriCollection.addIri(iri);
		}
		
		packagingSessionFacade.assignTaxonomyIndividualsToInformationPackage(iriCollection, iriOfInformationPackage);
	}

	@Override
	public IriListResponse findAllAssignedTaxonomyIndividuals(UuidResponse uuidOfInformationPackageResponse) {
		final IriListResponse iriListResponse = ResponseModelFactory.createEmptyIriListResponse();
		final String uuidAsString = uuidOfInformationPackageResponse.toString();
		final Uuid uuidOfInformationPackage = ServerModelFactory.createUuid(uuidAsString);
		final IriCollection irisOfTaxonomyIndividuals = packagingSessionFacade.findAllIrisOfAssignedTaxonomyIndividuals(
				uuidOfInformationPackage);
		
		for (int i = 0; i < irisOfTaxonomyIndividuals.getIrisCount(); i++) {
			final Iri iri = irisOfTaxonomyIndividuals.getIri(i);
			final Namespace namespace = iri.getNamespace();
			final LocalName localName = iri.getLocalName();
			final IriResponse iriResponse = ResponseModelFactory.createIriResponse(namespace.toString(), localName.toString());
			iriListResponse.addIriToList(iriResponse);
		}
		
		return iriListResponse;
	}
}
