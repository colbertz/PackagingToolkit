/**
 * Contains the interfaces for the packaging module. This module creates the zip
 * archives with the information package.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces;