package de.feu.kdmp4.packagingtoolkit.server.packaging.classes;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces.PackagingService;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * The RESTful interface for the communication between the client and the 
 * packaging module. It contains the methods for working with the
 * information package while its files are stored in its temporary
 * directory.
 * @author Christopher Olbertz
 *
 */
@RestController
@RequestMapping(PathConstants.SERVER_PACKAGING_INTERFACE + "/**")
public class PackagingCommunication {
	/**
	 * Used for the communication via REST.
	 */
	private RestTemplate restTemplate;
	/**
	 * The facade that encapsulates the methods of the service layer.
	 */
	@Autowired
	private PackagingService packagingService;
	
	/**
	 * Is called by the client in order to save an information package that is in the session.
	 * @param uuidOfInformationPackage The uuid of the information package. Identifies the information
	 * package in the session.
	 * @return An response that says if the information package has been saved successfully.
	 */
	@RequestMapping(value = PathConstants.SERVER_SAVE_INFORMATION_PACKAGE, method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> saveInformationPackage(@RequestBody UuidResponse uuidOfInformationPackage) {
		packagingService.saveInformationPackage(uuidOfInformationPackage);
		return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.OK);
	}
	
	/**
	 * Loads a certain information package. This information package is put into the session. Then the user can work
	 * with it until it is either saved or removed from the session.
	 * @param uuidOfInformationPackage The uuid of the information package that should be loaded.
	 * @return An response that says if the information package has been loaded successfully.
	 */
	@RequestMapping(value = PathConstants.SERVER_LOAD_INFORMATION_PACKAGE)
	public ResponseEntity<String> loadInformationPackage(@PathVariable(ParamConstants.PARAM_UUID) 
			final String uuidOfInformationPackage) {
		packagingService.loadInformationPackage(uuidOfInformationPackage);
		return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.OK);
	}
	
	/**
	 * Creates a new virtual submission information package.
	 * @param title The title of the newly created submission information package.
	 * @return The information of the submission information package for transmission.
	 */
	@RequestMapping(value = PathConstants.SERVER_CREATE_SUBMISSION_INFORMATION_PACKAGE)
	public ResponseEntity<PackageResponse> createNewVirtualSubmissionInformationPackage(@RequestParam(ParamConstants.PARAM_TITLE) 
			final String title) {
		PackageResponse packageResponse = packagingService.
				createNewSubmissionInformationPackage(title);
		return new ResponseEntity<PackageResponse>(packageResponse, HttpStatus.OK);
	}
	
	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param irisOfIndividuals The iris of the individuals from the taxonomy that should be 
	 * assigned to the information package. The first entry is the iri of the information package.
	 * @return An response that says if the assignments were successful.
	 */
	@RequestMapping(value = PathConstants.SERVER_ASSIGN_TAXONOMY_INDIVIDUALS, method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> assignTaxonomyIndividuals(
			@RequestBody IriListResponse irisOfIndividuals) {
		packagingService.assignTaxonomyIndividualsToInformationPackage(irisOfIndividuals);
		return ResponseEntity.status(HttpStatus.OK).build(); 
	}
	
	/**
	 * Adds a reference to a file on a remote server to the information package. 
	 * @param reference The reference that should be add to the information package.
	 * @return An response that says if the information package has been updated successfully.
	 */
	@RequestMapping(value = PathConstants.SERVER_ADD_HTTP_REFERENCE, method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> addRemoteReferenceToInformationPackage(@RequestBody ReferenceResponse reference) {
		final String uuidOfInformationPackage = reference.getUuidOfInformationPackage().toString();
		final String urlOfReference = reference.getUrl();
		packagingService.addRemoteReferenceToInformationPackage(uuidOfInformationPackage, urlOfReference);
		return ResponseEntity.status(HttpStatus.OK).build(); 
	}
	
	/**
	 * Adds a reference to a file that is saved on the server to the information package. If the file does not exist on the server
	 * yet it has to be uploaded. This method can be used to decide if an upload is necessary.  
	 * @param digitalObjectResponse Contains all information about the file.
	 * @return Contains true if the file already exists on the server. Contains false if the file must be uploaded on the server.
	 */
	@RequestMapping(value = PathConstants.SERVER_ADD_FILE_REFERENCE, method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<Boolean> addFileReferenceToInformationPackage(@RequestBody DigitalObjectResponse digitalObjectResponse) {
		final String uuidOfInformationPackage = digitalObjectResponse.getInformationPackageUuid();
		final String md5Checksum = digitalObjectResponse.getMd5Checksum();
		final boolean fileExisting = packagingService.addReferenceToExistingFile(md5Checksum, uuidOfInformationPackage);
		
		if (fileExisting) {
			final File file = packagingService.findFileByChecksum(md5Checksum);
			final String filename = file.getName();
			final Optional<Uuid> optionalWithUuid = packagingService.findUuidOfFileByChecksum(md5Checksum);
			
			if (optionalWithUuid.isPresent()) {
				final Uuid uuidOfFile = optionalWithUuid.get();
				final UuidResponse uuidResponseOfFile = ResponseModelFactory.getUuidResponse(uuidOfFile);
				final boolean hasSavedMetadata = packagingService.hasDigitalObjectSavedMetadata(uuidOfFile);
				MediatorDataListResponse mediatorData = ResponseModelFactory.createEmptyMediatorDataListResponse();

				final UuidResponse uuid = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
				mediatorData.setUuidOfInformationPackage(uuid);
				mediatorData.setOriginalFileName(filename);
				mediatorData.setUuidOfFile(uuidResponseOfFile);
				
				if (!hasSavedMetadata) {
					mediatorData = collectMetadataFromMediator(file, digitalObjectResponse.getMediatorUrl());
					mediatorData.setUuidOfInformationPackage(uuid);
					mediatorData.setOriginalFileName(filename);
					mediatorData.setUuidOfFile(uuidResponseOfFile);
					packagingService.saveMetadata(mediatorData);
				} else {
					final MetadataExchangeCollection metadata = packagingService.findMetadataOfFileByUuid(uuidOfFile);
					
					for (int i = 0; i < metadata.getMetadataCount(); i++) {
						final MetadataExchange metadataExchange = metadata.getMetadataAt(i);
						final MediatorDataResponse mediatorDataResponse = ResponseModelFactory.createMediatorDataResponse();
						mediatorDataResponse.setName(metadataExchange.getMetadataName());
						mediatorDataResponse.setValue(metadataExchange.getMetadataValue());
						mediatorData.addMediatorData(mediatorDataResponse);
					}
				}
				
				final UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(uuidOfFile);
				mediatorData.setUuidOfFile(uuidResponse);
				packagingService.addMetadataToInformationPackage(mediatorData);
				return new ResponseEntity<Boolean>(fileExisting, HttpStatus.OK);
			} else {
				return new ResponseEntity<Boolean>(false, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<Boolean>(fileExisting, HttpStatus.OK);
	}
	
	private MediatorDataListResponse collectMetadataFromMediator(File file, String mediatorUrl) {
		try{
			final MultiValueMap<String, Object> parameterMap = createMultipartFileParam(file.getAbsolutePath());

			HttpHeaders fileHeader = new HttpHeaders();
			fileHeader.setContentType(MediaType.MULTIPART_FORM_DATA);
			
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameterMap, fileHeader);
			String url = mediatorUrl + PathConstants.MEDIATOR_CLIENT_INTERFACE + PathConstants.MEDIATOR_COLLECT_DATA_FROM_FILE;
			restTemplate = getRestTemplate();
			ResponseEntity<MediatorDataListResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, MediatorDataListResponse.class);
			 return response.getBody();
		} catch (ResourceAccessException exception) {
			//ConnectionException connectionException = ConnectionException.createMediatorConnectionError(mediatorUrl);
			//throw connectionException;
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			return null;
		} 
		return null;
	}
	
	/**
	 * Creates an object of RestTemplate. If there is already an object existing, there
	 * is no new object created.
	 * @return The object of RestTemplate.
	 */
    public RestTemplate getRestTemplate() {
    	if(restTemplate == null) {
             restTemplate = createRestTemplate(); 
         }
         return restTemplate;
     }

     public void setRestTemplate(RestTemplate restTemplate) {
         this.restTemplate = restTemplate;
     }

     private RestTemplate createRestTemplate() {
         restTemplate = new RestTemplate();
         return restTemplate;
     }
	
	/**
	 * Creates an object for sending a file via http.
	 * @param filePath The path of the file we want to send on the local disk.
	 * @return Contains the information about the file for sending.
	 */
	private MultiValueMap<String, Object> createMultipartFileParam(String filePath) {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();   
        File file = new File(filePath);
        parts.add("file", new FileSystemResource(file));
        return parts;
	}
	
	/**
	 * Finds all individuals assigned to an information package.
	 * @param uuidOfInformationPackage The uuid of the information package we are interested in.
	 * @return The individuals assigned to the information package.
	 */
	@RequestMapping(value = PathConstants.SERVER_FIND_ALL_INDIVIDUALS_OF_INFORMATION_PACKAGE, 
			method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<IndividualListResponse> findIndividualsOfInformationPackage(
			@RequestBody UuidResponse uuidOfInformationPackage) {
		final IndividualListResponse individuals = packagingService.findAllIndividualsOfInformationPackage(uuidOfInformationPackage);
		return new ResponseEntity<IndividualListResponse>(individuals, HttpStatus.OK);
	}
	
	/**
	 * Adds meta data to an information package.
	 * @param mediatorDataListResponse The meta data from the mediator. Contains also some information about the 
	 * information package and the file the meta data are assigned to.
	 * @return A response with http status 200.
	 */
	@RequestMapping(value = PathConstants.SERVER_RECEIVE_METADATA, method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> addMetadataToInformationPackage(@RequestBody MediatorDataListResponse mediatorDataListResponse) {
		packagingService.addMetadataToInformationPackage(mediatorDataListResponse);
		return new ResponseEntity<String>(StringUtils.EMPTY_STRING, HttpStatus.OK); 
	}
	
	/**
	 * Adds an individual to the information package.
	 * @param individualResponse The individual that has been created on the client. Contains also the uuid of the information
	 * package the individual belongs to. 
	 * @return An response that says if the individual has been saved successfully.
	 */
	@RequestMapping(value = PathConstants.SERVER_ADD_INDIVIDUAL_TO_INFORMATION_PACKAGE, 
		      method = RequestMethod.POST,	consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> addIndividualToInformationPackage(@RequestBody final IndividualResponse individualResponse) {
		try {
			packagingService.saveIndividualForInformationPackage(individualResponse.getUuidOfInformationPackage(), individualResponse);
			return ResponseEntity.status(HttpStatus.OK).build(); 
		} catch (OntologyReasonerException ex) {
			return new ResponseEntity<String>(ex.getNotHtmlMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Updates an individual in an information package. Is called if the user has changed property values and wants to save them.
	 * @param individualResponse The individual with the new values. 
	 * @return An response that says if the individual has been saved successfully.
	 */
	@RequestMapping(value = PathConstants.SERVER_UPDATE_INDIVIDUAL_IN_INFORMATION_PACKAGE, 
		      method = RequestMethod.POST,	consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> updateIndividualInInformationPackage(@RequestBody final IndividualResponse individualResponse) {
		packagingService.saveIndividualForInformationPackage(individualResponse.getUuidOfInformationPackage(), individualResponse);
		return ResponseEntity.status(HttpStatus.OK).build(); 
	}
	
	/**
	 * Creates a not virtual archive for download. A not virtual archive contains all digital objects as files and the
	 * manifest file. The archive is downloaded as zip file.
	 * @param response The response that should contain the archive file.
	 * @param uuid The uuid of the information package whose file we want to download.
	 * @throws IOException
	 */
	@RequestMapping(value = PathConstants.SERVER_DOWNLOAD_NOT_VIRTUAL_ARCHIVE,
			method = RequestMethod.GET)
	public void downloadNotVirtualArchive (HttpServletResponse response, 
			@PathVariable(ParamConstants.PARAM_UUID) String uuid) throws IOException {
		final UuidResponse uuidOfInformationPackage = new UuidResponse(uuid);
		final File downloadFile = packagingService.downloadNotVirtualInformationPackage(uuidOfInformationPackage);
		
		createFileDownload(downloadFile, response);
	}
	
	@RequestMapping(value = PathConstants.SERVER_FIND_ALL_REFERENCES_OF_INFORMATION_PACKAGE,
			method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<ReferenceListResponse> findAllReferencesOfInformationPackage(HttpServletResponse response, 
			@PathVariable(ParamConstants.PARAM_UUID) String uuid) {
		final UuidResponse uuidOfInformationPackage = new UuidResponse(uuid);
		final ReferenceListResponse references =  packagingService.findAllReferencesOfInformationPackage(uuidOfInformationPackage);
		return new ResponseEntity<ReferenceListResponse>(references, HttpStatus.OK);
	}
	
	/**
	 * Finds all information packages saved on the server.
	 * @return All information packages that have been found on the server.
	 */
	@RequestMapping(value = PathConstants.SERVER_FIND_ALL_ARCHIVES, method = RequestMethod.GET,
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<ArchiveListResponse> findAllArchives() {
		ArchiveListResponse archiveListResponse = packagingService.findAllArchives();
		return new ResponseEntity<ArchiveListResponse>(archiveListResponse, HttpStatus.OK);
	}
	
	/**
	 * Cancels the creation of an information package. Is called if the user clicks on a cancel button. The information packages
	 * is removed from the session.
	 * @param uuidOfInformationPackage The uuid of the information package. Is used to identify it in the session.
	 */
	@RequestMapping(value = PathConstants.SERVER_CANCEL_INFORMATION_PACKAGE_CREATION, method = RequestMethod.DELETE)
	public void cancelInformationPackageCreation(@PathVariable(ParamConstants.PARAM_UUID) String uuidOfInformationPackage) {
		packagingService.cancelInformationPackageCreation(uuidOfInformationPackage);
	}
	
	/**
	 * Creates a virtual archive for download. A virtual archive does not contain any digital objects. Only the manifest
	 * file is contained. The archive is downloaded as zip file.
	 * @param response The response that should contain the archive file.
	 * @param uuid The uuid of the information package whose file we want to download.
	 * @throws IOException
	 */
	@RequestMapping(value = PathConstants.SERVER_DOWNLOAD_VIRTUAL_ARCHIVE,
			method = RequestMethod.GET)
	public void downloadVirtualArchive (HttpServletResponse response, 
			@PathVariable(ParamConstants.PARAM_UUID) 	String uuid) throws IOException {
		final UuidResponse uuidOfInformationPackage = new UuidResponse(uuid);
		final File downloadFile = packagingService.downloadVirtualInformationPackage(uuidOfInformationPackage);
		createFileDownload(downloadFile, response);
	}
	
	/**
	 * Puts the file to download in the response that is send to the client.
	 * @param downloadFile The file the client wants to download.
	 * @param response The response that contains the file.
	 * @throws IOException
	 */
	private void createFileDownload(File downloadFile, HttpServletResponse response) throws IOException {
		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		response.setContentLength((int)downloadFile.length());
		final InputStream inputStream = new BufferedInputStream(new FileInputStream(downloadFile));
		FileCopyUtils.copy(inputStream, response.getOutputStream());
	}
	
	/**
	 * Updates an individual in an information package. Is called if the user has changed property values and wants to save them.
	 * @param uuids Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals. 
	 * @return An response that says if the individuals have been assigned successfully.
	 */
	@RequestMapping(value = PathConstants.SERVER_ASSIGN_INDIVIDUALS_TO_INFORMATION_PACKAGE, 
		      method = RequestMethod.POST,	consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> assignIndividualsToInformationPackage(@RequestBody final UuidListResponse uuids) {
		packagingService.assignIndividualsToInformationPackage(uuids);
		return ResponseEntity.status(HttpStatus.OK).build(); 
	}
	
	/**
	 * Updates an individual in an information package. Is called if the user has changed property values and wants to delete
	 * these individuals from the information package.
	 * @param uuids Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals. 
	 * @return An response that says if the individuals have been unassigned successfully.
	 */
	@RequestMapping(value = PathConstants.SERVER_UNASSIGN_INDIVIDUALS_FROM_INFORMATION_PACKAGE, 
		      method = RequestMethod.POST,	consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> unassignIndividualsToInformationPackage(@RequestBody final UuidListResponse uuids) {
		packagingService.unassignIndividualsFromInformationPackage(uuids);
		return ResponseEntity.status(HttpStatus.OK).build(); 
	}
	
	/**
	 * Finds all taxonomy individuals that are assigend to a certain information package. 
	 * @param uuidOfInformationPackage The uuid of the information package whose assigned
	 * taxonomy individuals we want to see.
	 * @return Contains the taxonomy individuals that are assigned to the information package.
	 */
	@RequestMapping(value = PathConstants.SERVER_FIND_ALL_ASSIGNED_TAXONOMY_INDIVIDUALS, method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<IriListResponse> findAllAssignedTaxonomyIndividuals(@RequestBody UuidResponse uuidOfInformationPackage) {
		final IriListResponse iris = packagingService.findAllAssignedTaxonomyIndividuals(uuidOfInformationPackage);
		return new ResponseEntity<IriListResponse>(iris, HttpStatus.OK);
	}
}
