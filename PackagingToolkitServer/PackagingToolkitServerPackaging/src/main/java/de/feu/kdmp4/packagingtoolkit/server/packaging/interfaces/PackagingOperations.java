package de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces;

import java.io.InputStream;

import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitFileException;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.WorkingDirectory;
import de.feu.kdmp4.packagingtoolkit.server.packaging.classes.PackagingException;

/**
 * The interface for the operations for the packaging module. This module creates 
 * the zip files that contain the file of the information package.
 * @author Christopher Olbertz
 *
 */
public interface PackagingOperations {
	/**
	 * Creates a zip files and puts all files in a directory in this
	 * zip file. The zip file is created in the same directory that contains
	 * the files to zip.
	 * @param directoryPath The path containing the files to zip.
	 * @param filenameWithoutSuffix The filename for the zip file without
	 * the .zip suffix.
	 * @return The path and the name of the zip file.
	 * @throws PackagingException is thrown if archiveDirectory does not exist
	 * or if archiveDirectory does not contain any files.
	 * @throws PackagingToolkitFileException is thrown if archiveDirectory does not represent a directory.
	 */
	String zipAllFilesInDirectory(final String directoryPath, final String filenameWithoutSuffix);
	/**
	 * Adds a file to the temporary directory of an information package. 
	 * @param filename The name of the file.
	 * @param inputStream Contains the data of the file.
	 * @param uuid The uuid of the information package for determine the temporary
	 * directory.
	 * @param workingDirectory The working directory of the server.
	 */
	void addFileToTemporaryDirectory(final String filename, final InputStream inputStream, 
			String uuid, WorkingDirectory workingDirectory);
	/**
	 * Deletes a file assigned to an information object from the temporary directory. This file
	 * will not be packed into a zip file later. 
	 * @param filename The name of the file that should be deleted.
	 * @param uuid The uuid of the information package the file is assigned to.
	 * @param workingDirectory The working directory of the server.
	 */
	void deleteFileFromTemporaryDirectory(final String filename, final String uuid, 
			WorkingDirectory workingDirectory);
	/**
	 * Writes the metadata of a file to a text file in the temporary directory.
	 * @param reference A reference to the file whose metadata we are interested in.
	 * @param metadata The metadata of this file.
	 * @param workingDirectory The working directory of the server.
	 * @param uuidOfInformationPackage The uuid of the information package that contains the
	 * digital object.
	 */
	void writeMetadataToFile(Reference reference, MetadataExchangeCollection metadata,
			WorkingDirectory workingDirectory, String uuidOfInformationPackage);
}
