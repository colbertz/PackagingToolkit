package de.feu.kdmp4.packagingtoolkit.server.packaging.interfaces;

import java.io.File;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.PackagingConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.database.facades.PackagingDatabaseFacade;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.metadata.facades.PackagingMetaDataFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.classes.OntologyCache;
import de.feu.kdmp4.packagingtoolkit.server.ontology.facades.PackagingOntologyFacade;
import de.feu.kdmp4.packagingtoolkit.server.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.serialization.facade.PackagingSerializationFacade;
import de.feu.kdmp4.packagingtoolkit.server.session.facade.PackagingSessionFacade;
import de.feu.kdmp4.packagingtoolkit.server.storage.facade.PackagingStorageFacade;

/**
 * Contains the methods that contain the business logic of the module Packaging.
 * @author Christopher Olbertz
 *
 */
public interface PackagingService {
	/**
	 * If the creation of an information package has been cancelled, all information that has been written already has to be deleted.
	 * These information can be files or triples in the tripel store.
	 * @param uuidOfInformationPackage
	 */
	void cancelInformationPackageCreation(final String uuidOfInformationPackage);
	
	/**
	 * Determines the path of the temporary directory for the files of an 
	 * information package with a given uuid. 
	 * @param workingDirectoryPath The path of the server's working directory.
	 * @param uuid The uuid of the information package.
	 * @return The path of the temporary directory of the information package with
	 * the uuid.
	 */
	String getTemporaryDirectoryForUuid(final String workingDirectoryPath,final  Uuid uuid);
	/**
	 * Saves a newly creates archives. This process contains the following steps:
	 * <ul>
	 * 	<li>There are rdf statements created with the information contained in the information package.</li>
	 * 	<li>The information package is removed from the session.</li>
	 * </ul>
	 * @param uuid The uuid of the information package for identifying it in the session. 
	 */
	void saveInformationPackage(final UuidResponse uuid);
	/**
	 * Saves an uploaded file to the temporary directory of the information package
	 * with a given uuid.
	 * @param inputStream The input stream with the file data.
	 * @param fileName The name of the file that should be saved.
	 * @param workingDirectory The working directory of the server.
	 * @param uuid The uuid of the information package. 
	 */
//	public void saveFileToTemporaryDirectory(final InputStream inputStream, final String fileName, 
	//		final String workingDirectoryPath, final Uuid uuid);
	/**
	 * Sets a reference to the object that contains the atomar operations of this module.
	 * @param packagingOperations A reference to the object that contains the atomar operations of this module.
	 */
	void setPackagingOperations(final PackagingOperations packagingOperations);
	/**
	 * Sets a reference to the object that creates the model objects of the Ontology module
	 * @param packagingOperations A reference to the object that creates the model objects of the Ontology module
	 */
	void setOntologyModelFactory(final OntologyModelFactory ontologyModelFactory);
	/**
	 * Saves an individual in an information package.
	 * @param uuidResponse The uuid of the information package the individual belongs to. 
	 * @param individualResponse The individual that should be saved.
	 * @throws OntologyReasonerException if the individual contains values that could not be verified by the reasoner.
	 */
	void saveIndividualForInformationPackage(final UuidResponse uuidResponse, final IndividualResponse individualResponse);
	/**
	 * Sets a reference to the object that contains methods of the Configuration module that can be called by the Packaging module.
	 * @param packagingConfigurationFacade A reference to the object contains methods of the Configuration module that can be called by the Packaging module.
	 */
	void setPackagingConfigurationFacade(final PackagingConfigurationFacade packagingConfigurationFacade);
	/**
	 * Sets a reference to the object that contains methods of the Ontology module that can be called by the Packaging module.
	 * @param packagingOntologyFacade A reference to the object contains methods of the Ontology module that can be called by the Packaging module.
	 */
	void setPackagingOntologyFacade(final PackagingOntologyFacade packagingOntologyFacade);
	/**
	 * Sets a reference to the object that contains methods of the Storage module that can be called by the Packaging module.
	 * @param packagingStorageFacade A reference to the object contains methods of the Storage module that can be called by the Packaging module.
	 */
	void setPackagingStorageFacade(final PackagingStorageFacade packagingStorageFacade);
	/**
	 * Creates a new submission information package in the session of the server. The user can then edit it. 
	 * @param title The title of the information package.
	 * @return Represents the new information package for sending to the client via http.
	 */
	PackageResponse createNewSubmissionInformationPackage(final String title);
	/**
	 * Creates a new submission information unit in the session of the server. The user can then edit it. 
	 * @param title The title of the information package.
	 * @return Represents the new information package for sending to the client via http.
	 */
	PackageResponse createNewSubmissionInformationUnit(final String title);
	/**
	 * Determines if there are any metadata saved for this digital object.
	 * @param uuidOfDigitalObject The uuid of the digital object we want to determine if there are any metadata of.
	 * @return True, if there are metadata for this digital object saved on the server, false otherwise. 
	 */
	boolean hasDigitalObjectSavedMetadata(final Uuid uuidOfDigitalObject);
	/**
	 * Sets the object that contains all methods for accessing the module Session.
	 * @param packagingDatabaseFacade The object that contains all methods for accessing the module Session.
	 */
	void setPackagingSessionFacade(final PackagingSessionFacade packagingSessionFacade);
	/**
	 * Adds a reference to a file on a remote server to the information package.
	 * @param uuidOfInformationPackage The uuid of the information package the reference should be added to.
	 * @param url The url where the file can be found.
	 */
	void addRemoteReferenceToInformationPackage(final String uuidOfInformationPackage, final String url);
	/**
	 * Sets the object that contains all methods for accessing the module Serialization.
	 * @param packagingDatabaseFacade The object that contains all methods for accessing the module Serialization.
	 */
	void setPackagingSerializationFacade(final PackagingSerializationFacade packagingSerializationFacade);
	/**
	 * Prepares a zip file for a virtual information package for download. The zip file only contains the manifest.
	 * It does not contain any files of digital objects.
	 * @param uuidResponseOfInformationPackage The uuid of the information package that the user wants
	 * to download.
	 * @return The file that represents the zip file.
	 */
	File downloadVirtualInformationPackage(final UuidResponse uuidResponseOfInformationPackage);
	/**
	 * Prepares a zip file for a not virtual information package for download. The zip file contains the manifestand the 
	 * files of its digital objects.
	 * @param uuidResponseOfInformationPackage The uuid of the information package that the user wants
	 * to download.
	 * @return The file that represents the zip file.
	 */
	File downloadNotVirtualInformationPackage(final UuidResponse uuidResponseOfInformationPackage);
	/**
	 * Sets the object that contains all methods for accessing the module Database.
	 * @param packagingDatabaseFacade The object that contains all methods for accessing the module Database.
	 */
	void setPackagingDatabaseFacade(final PackagingDatabaseFacade packagingDatabaseFacade);
	/**
	 * Creates a reference that points to an existing file. The method returns true if the file already exists in the storage. 
	 * @param md5Checksum Identifies the file in the storage.
	 * @param uuidOfInformationPackage The uuid of the information package that contains the reference.
	 * @return True if the file already exists in the storage and the reference to it has been enterend in the information
	 * package, false otherwise.
	 */ 
	boolean addReferenceToExistingFile(final String md5Checksum, final String uuidOfInformationPackage);
	/**
	 * Adds a digital object to the temporary directory of an information package 
	 * identified by its uuid.  
	 * @param digitalObjectResponse The digital object.
	 * @param dataOfFile The data of the file.
	 */
	void addDigitalObjectToInformationPackage(final String uuidOfInformationPackage, final String filename, final byte[] dataOfFile);
	/**
	 * Finds all archives saved on the server.
	 * @return All archives.
	 */
	ArchiveListResponse findAllArchives();
	/**
	 * Updates an individual in an information package with a new one.
	 * @param uuidResponse The uuid of the information package that contains the individual.
	 * @param individualResponse The individual that should replace another one with the same uuid.
	 */
	void updateIndividualInInformationPackage(final UuidResponse uuidResponse,final  IndividualResponse individualResponse);
	/**
	 * Loads an information package in the session so that the user can edit it. 
	 * @param uuid The uuid of the information package that should be loaded.
	 */
	void loadInformationPackage(final String uuid);
	/**
	 * Sets the reference to the object that contains all information about the ontologies.
	 * @param ontologyCache The object that caches all information extracted from the ontologies.
	 */
	void setOntologyCache(final OntologyCache ontologyCache);
	/**
	 * Adds meta data to an information package. 
	 * @param mediatorDataListResponse The meta data that should be added to the information package. Contains also
	 * the information about the information package and the file.
	 */
	void addMetadataToInformationPackage(final MediatorDataListResponse mediatorDataListResponse);
	/**
	 * Sets the object that contains all methods for accessing the module meta data.
	 * @param packagingDatabaseFacade The object that contains all methods for accessing the module meta data.
	 */
	void setPackagingMetaDataFacade(final PackagingMetaDataFacade packagingMetaDataFacade);
	/**
	 * Determines a file in the storage directory. For the search the md5 checksum of the file is used.
	 * @param md5Checksum The md5 checksum for the search.
	 * @return The file in the storage directory.
	 */
	File findFileByChecksum(final String md5Checksum);
	/**
	 * Determines the uuid of the digital object of a file. The file is identified by its checksum.
	 * @param md5Checksum The checksum that is used for identifying the file.
	 * @return The uuid of the digital object of the file or an empty optional. 
	 */
	Optional<Uuid> findUuidOfFileByChecksum(String md5Checksum);
	/**
	 * Saves the metadata of a digital object in the database.
	 * @param mediatorDataList Contains the metadata that should be saved.
	 */
	void saveMetadata(final MediatorDataListResponse mediatorDataList);
	/**
	 * Finds the metadata of a certain file with the help of its uuid.
	 * @param uuidOfFile The uuid of the file whose metadata we want to find.
	 * @return The metadata of the file with the uuid uuidOfFile.
	 */
	MetadataExchangeCollection findMetadataOfFileByUuid(final Uuid uuidOfFile);
	/**
	 * Finds all individuals that are assigned to a certain information package.
	 * @param uuidOfInformationPackage The uuid of the information package whose 
	 * individuals we want to see.
	 * @return the individuals that are assigned to the information package.
	 */
	IndividualListResponse findAllIndividualsOfInformationPackage(UuidResponse uuidOfInformationPackage);
	/**
	 * Assigns some individuals to an information package.
	 * @param uuidsResponse Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals.
	 */
	void assignIndividualsToInformationPackage(UuidListResponse uuids);
	/**
	 * Removes some individuals that are saved in the triple store from the information package.
	 * @param uuids Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals. 
	 */
	void unassignIndividualsFromInformationPackage(UuidListResponse uuids);
	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param irisOfIndividuals The iris of the individuals from the taxonomy that should be 
	 * assigned to the information package. The first entry is the iri of the information package.
	 */
	void assignTaxonomyIndividualsToInformationPackage(IriListResponse irisOfIndividuals);
	/**
	 * Finds all taxonomy individuals that are assigend to a certain information package. 
	 * @param uuidOfInformationPackage The uuid of the information package whose assigned
	 * taxonomy individuals we want to see.
	 * @return Contains the taxonomy individuals that are assigned to the information package.
	 */
	IriListResponse findAllAssignedTaxonomyIndividuals(UuidResponse uuidOfInformationPackage);
	/**
	 * Looks for all references to digital objects assigned to a certain information package.
	 * @param uuidResponseOfInformationPackage The uuid of the information package we are 
	 * interested in. 
	 * @return The list with the references that were found.
	 */
	ReferenceListResponse findAllReferencesOfInformationPackage(UuidResponse uuidResponseOfInformationPackage);
}
