package de.feu.kdmp4.packagingtoolkit.server.database.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage.ViewException;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.OntologyClassEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.ViewEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.factories.EntityFactory;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.DatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.DigitalObjectRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.InformationPackageRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.OntologyClassRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.ViewRepository;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.factories.ExchangeModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

/**
 * Contains the business logic for the database module and encapuslated the repositories. 
 * @author Christopher Olbertz
 *
 */
public class DatabaseServiceImpl implements DatabaseService {
	/**
	 * The repository for accessing the view table in the database.
	 */
	@Autowired
	protected ViewRepository viewRepository;
	/**
	 * The repository for accessing the ontologyclass table in the database.
	 */
	@Autowired
	protected OntologyClassRepository ontologyClassRepository;
	/**
	 * The repository for accessing the information package table in the database.
	 */
	@Autowired
	protected InformationPackageRepository informationPackageRepository;
	/**
	 * The repository for accessing the digitalobject table in the database.
	 */
	@Autowired 
	protected DigitalObjectRepository digitalObjectRepository;
	
	@Override
	public void deleteView(final int viewId) {
		viewRepository.delete(viewId);
	}
	
	@Override
	public void deleteInformationPackage(final String uuidOfInformationPackage) {
		final InformationPackageEntity informationPackageEntity = informationPackageRepository.findInformationPackageEntityByUuid(uuidOfInformationPackage);
		if (informationPackageEntity != null) {
			final List<DigitalObjectEntity> digitalObjects = digitalObjectRepository.findDigitalObjectsOfInformationPackage(uuidOfInformationPackage);
			
			for (final DigitalObjectEntity digitalObjectEntity: digitalObjects) {
				digitalObjectEntity.removeReferenceToInformationPackage(informationPackageEntity);
				digitalObjectRepository.save(digitalObjectEntity);
			}
			
			informationPackageRepository.delete(informationPackageEntity);
		}
	}

	@Override
	public ViewListResponse findAllViews() {
		final ViewListResponse viewListResponse = ResponseModelFactory.getViewListResponse();
		
		for(ViewEntity view: viewRepository.findAll()) {
			final String viewName = view.getViewName();
			final int viewId = view.getViewId();
			final ViewResponse viewResponse = ResponseModelFactory.getViewResponse(viewName, viewId);
			
			for(OntologyClassEntity ontologyClassEntity: view.getOntologyClassEntities()) {
				final String localName = ontologyClassEntity.getClassName();
				final String namespace = ontologyClassEntity.getNamespace();
				final OntologyClassResponse ontologyClassResponse = ResponseModelFactory.getOntologyClassResponse(namespace, 
						localName);
				viewResponse.addOntologyClass(ontologyClassResponse);
			}
			
			viewListResponse.addViewToList(viewResponse);
		}
		
		return viewListResponse;
	}
	
	@Override
	public void saveNewView(final ViewResponse viewResponse) {
		final List<OntologyClassEntity> ontologyClassEntities = new ArrayList<>();
		final ViewEntity viewEntity = EntityFactory.createViewEntity();
		final String viewName = viewResponse.getViewName();
		
		if (viewExistsByName(viewName)) {
			throw ViewException.duplicateViewNameException(viewName);
		}
		viewEntity.setViewName(viewName);
		viewEntity.setViewId(viewResponse.getViewId());
		
		for (int i = 0; i < viewResponse.getOntologyClassesCount(); i++) {
			final OntologyClassResponse ontologyClassResponse = viewResponse.getOntologyClass(i);
			final String localName = ontologyClassResponse.getLocalName();
			final String namespace = ontologyClassResponse.getNamespace();
			OntologyClassEntity ontologyClassEntity = ontologyClassRepository.findByClassNameAndNamespace(localName, namespace);
			if (ontologyClassEntity == null) {
				ontologyClassEntity = EntityFactory.createOntologyClassEntity();
				ontologyClassEntity.setClassName(localName);
				ontologyClassEntity.setNamespace(namespace);
				ontologyClassRepository.save(ontologyClassEntity);
			}
			ontologyClassEntities.add(ontologyClassEntity);
		}
		
		viewEntity.setOntologyClassEntities(ontologyClassEntities);
		viewRepository.save(viewEntity);
	}
	
	@Override
	public boolean viewExistsByName(final String viewName) {
		final ViewEntity viewEntity = viewRepository.findByViewName(viewName);
		if (viewEntity == null) {
			return false;
 		} else {
 			return true;
 		}
	}
	
	@Override
	public void saveInformationPackage(final InformationPackageExchange informationPackageExchange) {
		final InformationPackageEntity informationPackageEntity = createInformationPackageEntity(informationPackageExchange);
		final String uuidOfInformationPackage = informationPackageEntity.getUuid();
		final InformationPackageEntity informationPackageFromDatabase = informationPackageRepository.findInformationPackageEntityByUuid(uuidOfInformationPackage);
		
		if (informationPackageFromDatabase != null) {
			informationPackageEntity.setInformationPackageId(informationPackageFromDatabase.getInformationPackageId());
		}
		informationPackageRepository.save(informationPackageEntity);
		
		if (informationPackageExists(uuidOfInformationPackage)) {
			updateDigitalObjectsOfInformationPackage(informationPackageExchange, informationPackageEntity);
		}
	}
	
	@Override
	public boolean informationPackageExists(final String uuidOfInformationPackage) {
		final Optional<InformationPackageExchange> optionalWithnformationPackage = findInformationPackageByUuid(uuidOfInformationPackage);
		if (optionalWithnformationPackage.isPresent()) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean informationPackageDoesNotExist(final String uuidOfInformationPackage) {
		return !informationPackageExists(uuidOfInformationPackage);
	}
	
	/**
	 * Updates the references of an information package to files in the storage directory in the database. Uses the checksum that
	 * are saved in the parameter informationPackageExchange to determine the digital objects in the database and to save
	 * a reference. 
	 * @param informationPackageExchange Contains the necessary information about the digital objects.
	 * @param informationPackageEntity The information package that has references to these digital objects in the
	 * database. 
	 */
	private void updateDigitalObjectsOfInformationPackage(final InformationPackageExchange informationPackageExchange,
			final InformationPackageEntity informationPackageEntity) {
		for (int i = 0; i < informationPackageExchange.getDigitalObjectCount(); i++) {
			final String uuid = informationPackageExchange.getUuidAt(i);
			final DigitalObjectEntity digitalObject = digitalObjectRepository.findByUuid(uuid);
			digitalObject.addReferenceToInformationPackage(informationPackageEntity);
			digitalObjectRepository.save(digitalObject);
		}
	}
	
	/**
	 * Creates a information package entity with the data of an exchange object.
	 * @param informationPackageExchange Contains the data that are used for the entity object.
	 * @return The entity object.
	 */
	private InformationPackageEntity createInformationPackageEntity(final InformationPackageExchange informationPackageExchange) {
		final InformationPackageEntity informationPackageEntity = EntityFactory.createInformationPackageEntity();
		informationPackageEntity.setCreationDate(informationPackageExchange.getCreationDate());
		informationPackageEntity.setLastModifiedDate(informationPackageExchange.getLastModifiedDate());
		informationPackageEntity.setPackageType(informationPackageExchange.getPackageType());
		informationPackageEntity.setTitle(informationPackageExchange.getTitle());
		informationPackageEntity.setUuid(informationPackageExchange.getUuid());
		informationPackageEntity.setSerializationFormat(informationPackageExchange.getSerializationFormat());
		
		return informationPackageEntity;
	}
	
	@Override
	public void setViewRepository(final ViewRepository viewRepository) {
		this.viewRepository = viewRepository;
	}

	@Override
	public void setOntologyClassRepository(final OntologyClassRepository ontologyClassRepository) {
		this.ontologyClassRepository = ontologyClassRepository;
	}

	@Override
	public Optional<InformationPackageExchange> findInformationPackageByUuid(final String uuidOfInformationPackage) {
		final InformationPackageEntity informationPackageEntity = informationPackageRepository.findInformationPackageEntityByUuid(uuidOfInformationPackage);
		if (informationPackageEntity != null) {
			final InformationPackageExchange informationPackageExchange = ExchangeModelFactory.createInformationPackageExchange(
					informationPackageEntity.getTitle(), informationPackageEntity.getUuid().toString(), informationPackageEntity.getPackageType(), 
					informationPackageEntity.getCreationDate(), informationPackageEntity.getLastModifiedDate(), informationPackageEntity.getSerializationFormat());
			return ServerOptionalFactory.createOptionalWithInformationPackageExchange(informationPackageExchange);
		} else {
			return ServerOptionalFactory.createEmptyOptionalWithInformationPackageExchange();
		}
	}
}
