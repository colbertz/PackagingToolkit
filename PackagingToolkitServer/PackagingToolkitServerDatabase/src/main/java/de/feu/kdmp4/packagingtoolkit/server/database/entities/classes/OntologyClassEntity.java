package de.feu.kdmp4.packagingtoolkit.server.database.entities.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;

/**
 * Represents the data in the table ontologyclass in the database.
 * @author Christopher Olbertz
 *
 */
@Entity
@Table(name = "ontologyClass")
public class OntologyClassEntity {
	/**
	 * The primary key of the table ontologyclass.
	 */
	private int ontologyClassId;
	/**
	 * The local name of the class.
	 */
	private String className;
	/**
	 * The namespace of the class.
	 */
	private String namespace;
	
	public OntologyClassEntity(String namespace, String className) {
		this.namespace = namespace;
		this.className = className;
	}
	
	public OntologyClassEntity() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getOntologyClassId() {
		return ontologyClassId;
	}
	
	public void setOntologyClassId(int ontologyClassId) {
		this.ontologyClassId = ontologyClassId;
	}
	
	@Column(nullable = false)
	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public OntologyClassResponse toResponse() {
		return ResponseModelFactory.getOntologyClassResponse(namespace, className);
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result + ontologyClassId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyClassEntity other = (OntologyClassEntity) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		if (ontologyClassId != other.ontologyClassId)
			return false;
		return true;
	}
}
