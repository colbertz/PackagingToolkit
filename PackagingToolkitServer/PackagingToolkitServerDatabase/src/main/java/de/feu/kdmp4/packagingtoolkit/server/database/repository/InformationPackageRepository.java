package de.feu.kdmp4.packagingtoolkit.server.database.repository;

import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;

/**
 * Contains the methods for accessing the table informationpackage. This interface is implemented by Spring Data JPA during
 * the runtime. You can find the reference documentation of the Spring Data JPA here for further information:
 * {@link https://docs.spring.io/spring-data/jpa/docs/2.0.0.RELEASE/reference/html/}
 * @author Christopher Olbertz
 *
 */
public interface InformationPackageRepository  extends CrudRepository<InformationPackageEntity, Long> {
	/**
	 * Finds an information package by its uuid in the database.
	 * @param uuid The uuid of the information package.
	 * @return The found information package.
	 */
	public InformationPackageEntity findInformationPackageEntityByUuid(@Param(ParamConstants.PARAM_UUID)String uuid);
}
