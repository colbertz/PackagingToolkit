package de.feu.kdmp4.packagingtoolkit.server.database.facades;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.DatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.StorageDatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;

/**
 * This facade contains the methods the module storage can use for accessing the module database.
 * @author Christopher Olbertz
 *
 */
public class StorageDatabaseFacade {
	/**
	 * Contains the business logic for the storage part of the database.
	 */
	private StorageDatabaseService storageDatabaseService;
	/**
	 * A reference to an object that contains the business logic of the module database.
	 */
	private DatabaseService databaseService;
	
	/**
	 * Deletes an information package from the database.
	 * @param uuid The uuid of the information package that should be deleted.
	 */
	public void deleteInformationPackage(final String uuidOfInformationPackage) {
		databaseService.deleteInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Checks if a file with a given md5 checksum exists in the storage directory. 
	 * @param md5Checksum The md5 checksum for the search for a digital object.
	 * @return The found digital object or an empty optional.
	 */
	public  Optional<DigitalObjectExchange> findDigitalObjectByMd5Checksum(String md5Checksum) {
		return storageDatabaseService.findDigitalObjectByMd5Checksum(md5Checksum);
	}
	
	/**
	 * Looks for the references of an information package.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return The references this information package is pointing to.
	 */
	public ReferenceCollection findReferencesOfInformationPackage(String uuidOfInformationPackage) {
		return storageDatabaseService.findReferencesOfInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Counts the information packages that contain this digital object.
	 * @param uuidOfDigitalObject The uuid of the digital object we are interested in.
	 * @return The number of the information packages.
	 */
	public long countReferencesOfDigitalObject(String uuidOfDigitalObject) {
		return storageDatabaseService.countReferencesOfDigitalObject(uuidOfDigitalObject);
	}
	
	/**
	 * Finds a file in the storage by its checksum and returns its path in the storage.
	 * @param md5Checksum The md5 checksum of the file.
	 * @return The path of the file.
	 */
	public String findFilePathByChecksum(String md5Checksum) {
		return storageDatabaseService.findFilePathByChecksum(md5Checksum);
	}
	
	/**
	 * Deletes a digital object from the database.
	 * @param uuidOfDigitalObject The uuid of the digital object to delete.
	 */
	public void deleteDigitalObject(String uuidOfDigitalObject) {
		storageDatabaseService.deleteDigitalObject(uuidOfDigitalObject);
	}
	
	/**
	 * Saves the data of a digital object in the database.
	 * @param filename The filename of the digital object in the storage directory of the server. 
	 * @param md5Checksum The md5 checksum of this file.
	 * @return The uuid of the newly created digital object.
	 */
	public Uuid saveDigitalObject(String filename, String md5Checksum) {
		return storageDatabaseService.saveDigitalObject(filename, md5Checksum);
	}
	
	/**
	 * Sets a reference to the object that contains the business logic of the part for the storage of the module database.
	 * @param storageDatabaseService A reference to the object that contains the business logic of the part for the storage of 
	 * the module database.
	 */
	public void setStorageDatabaseService(StorageDatabaseService storageDatabaseService) {
		this.storageDatabaseService = storageDatabaseService;
	}
	
	/**
	 * Sets a reference to an object that contains the business logic of the module database.
	 * @param databaseService A reference to an object that contains the business logic of the module database.
	 */
	public void setDatabaseService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}
}
