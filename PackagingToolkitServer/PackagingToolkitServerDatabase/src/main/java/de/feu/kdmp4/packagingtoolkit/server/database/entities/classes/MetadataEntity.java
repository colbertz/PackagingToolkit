package de.feu.kdmp4.packagingtoolkit.server.database.entities.classes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "metadata")
public class MetadataEntity {
	private int metadataId;
	private String metadataName;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getMetadataId() {
		return metadataId;
	}
	
	public void setMetadataId(int metadataId) {
		this.metadataId = metadataId;
	}
	
	public String getMetadataName() {
		return metadataName;
	}
	
	public void setMetadataName(String metadataName) {
		this.metadataName = metadataName;
	}
}
