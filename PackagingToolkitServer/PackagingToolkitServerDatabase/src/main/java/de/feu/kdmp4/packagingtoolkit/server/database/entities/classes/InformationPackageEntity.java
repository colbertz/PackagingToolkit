package de.feu.kdmp4.packagingtoolkit.server.database.entities.classes;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;

/**
 * Represents an information package in the database. An information package can reference to some files in the storage directory.
 * @author Christopher Olbertz
 *
 */
@Entity
@Table(name = "informationPackage")
public class InformationPackageEntity {
	/**
	 * The primary key for the information package. The uuid of the file is not used as primary key because an incrementing long
	 * is simpler to use.
	 */
	private long informationPackageId;
	/**
	 * The uuid of the information package. 
	 */
	private String uuid;
	/**
	 * The title of the information package given by the user.
	 */
	private String title;
	/**
	 * At the moment, only SIP is supported as package type.
	 */
	private PackageType packageType;
	/**
	 * Date and time the information package was created.
	 */
	private LocalDateTime creationDate;
	/**
	 * Date and time the information package was modified.
	 */
	private LocalDateTime lastModifiedDate;
	/**
	 * At the moment, the only serialization format supported is OAI-ORE.
	 */
	private SerializationFormat serializationFormat;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getInformationPackageId() {
		return informationPackageId;
	}

	public void setInformationPackageId(long informationPackageId) {
		this.informationPackageId = informationPackageId;
	}

	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Enumerated
	public PackageType getPackageType() {
		return packageType;
	}

	public void setPackageType(PackageType packageType) {
		this.packageType = packageType;
	}
	
	@Enumerated
	public SerializationFormat getSerializationFormat() {
		return serializationFormat;
	}
	
	public void setSerializationFormat(SerializationFormat serializationFormat) {
		this.serializationFormat = serializationFormat;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InformationPackageEntity other = (InformationPackageEntity) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
