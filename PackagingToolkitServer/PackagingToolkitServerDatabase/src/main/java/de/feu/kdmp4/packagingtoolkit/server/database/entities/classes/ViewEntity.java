package de.feu.kdmp4.packagingtoolkit.server.database.entities.classes;

import java.util.ArrayList; 
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

/**
 * Represents a view in the database. A view on a certain ontology contains several 
 * classes. A view is a template for selecting the classes of an ontology.
 * <br />
 * Example: User A wants to select the classes X, Y and Z of an ontology. Then he creates
 * an information package on the base of the selected classes and saves the selected
 * classes as view with the name userAView. Now user B wants to use the PackagingToolkit.
 * He does not want to select the classes by himself. Instead he chooses the view
 * user A created: userAView. Now he does not need to choose the classes. That is the 
 * view.  
 * @author Christopher Olbertz
 *
 */
@Entity
@Table(name = "view")
public class ViewEntity {
	/**
	 * The primary key of the table view.
	 */
	private int viewId;
	/**
	 * A name for the view.
	 */
	private String viewName;
	/**
	 * The classes that are contained in this view. 
	 */
	private List<OntologyClassEntity> ontologyClassEntities;
	
	public ViewEntity(int viewId, String viewName) {
		this();
		this.viewId = viewId;
		this.viewName = viewName;
	}
	
	public ViewEntity() {
		ontologyClassEntities = new ArrayList<>();
	}
	
	/**
	 * Adds a new class to the view.
	 * @param ontologyClass The class that should be added to the view.
	 */
	public void addOntologyClass(OntologyClassEntity ontologyClass) {
		ontologyClassEntities.add(ontologyClass);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "viewId")
	public int getViewId() {
		return viewId;
	}
	
	public void setViewId(int viewId) {
		this.viewId = viewId;
	}
	
	@Column(nullable = false, name = "viewName")
	public String getViewName() {
		return viewName;
	}
	
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "classesInView", 
			   joinColumns = {@JoinColumn(name = "viewId_fk")},
			   inverseJoinColumns = {@JoinColumn(name = "ontologyClassId_fk")})
	public List<OntologyClassEntity> getOntologyClassEntities() {
		return ontologyClassEntities;
	}

	public void setOntologyClassEntities(List<OntologyClassEntity> ontologyClassEntities) {
		this.ontologyClassEntities = ontologyClassEntities;
	}
	
	// CHANGE_ME
	public ViewResponse toResponse() {
		ViewResponse viewResponse = ResponseModelFactory.getViewResponse(viewName, viewId);
		OntologyClassListResponse ontologyClassList = ResponseModelFactory.
				getOntologyClassListResponse();
		
		for(OntologyClassEntity ontologyClassEntity: ontologyClassEntities) {
			OntologyClassResponse ontologyClassResponse = ontologyClassEntity.toResponse();
			ontologyClassList.addOntologyClassToList(ontologyClassResponse);
		}
		
		//viewResponse.setOntologyClassList(ontologyClassList);
		
		return viewResponse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ontologyClassEntities == null) ? 0 : ontologyClassEntities.hashCode());
		result = prime * result + viewId;
		result = prime * result + ((viewName == null) ? 0 : viewName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViewEntity other = (ViewEntity) obj;
		if (ontologyClassEntities == null) {
			if (other.ontologyClassEntities != null)
				return false;
		} else if (!ontologyClassEntities.equals(other.ontologyClassEntities))
			return false;
		if (viewId != other.viewId)
			return false;
		if (viewName == null) {
			if (other.viewName != null)
				return false;
		} else if (!viewName.equals(other.viewName))
			return false;
		return true;
	}
}