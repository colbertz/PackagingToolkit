package de.feu.kdmp4.packagingtoolkit.server.database.repository;

import org.springframework.data.repository.CrudRepository;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.ViewEntity;

/**
 * Contains the methods for accessing the table view. This interface is implemented by Spring Data JPA during
 * the runtime. You can find the reference documentation of the Spring Data JPA here for further information:
 * {@link https://docs.spring.io/spring-data/jpa/docs/2.0.0.RELEASE/reference/html/}
 * @author Christopher Olbertz
 *
 */
public interface ViewRepository extends CrudRepository<ViewEntity, Integer> {
	public ViewEntity findByViewName(final String viewName);
}
