package de.feu.kdmp4.packagingtoolkit.server.database.repository;

import org.springframework.data.repository.CrudRepository;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.OntologyClassEntity;

/**
 * Contains the methods for accessing the table ontologyclass. This interface is implemented by Spring Data JPA during
 * the runtime. You can find the reference documentation of the Spring Data JPA here for further information:
 * {@link https://docs.spring.io/spring-data/jpa/docs/2.0.0.RELEASE/reference/html/}
 * @author Christopher Olbertz
 *
 */
public interface OntologyClassRepository extends 
			CrudRepository<OntologyClassEntity, Integer> {
	/**
	 * Finds an ontology class by its name and namespace. 
	 * @param className The local name of the class we are looking for.
	 * @param namespace The namespace of the class we are looking for.
	 * @return The class with the iri localname#className.
	 */
	public OntologyClassEntity findByClassNameAndNamespace(String className, String namespace);
}
