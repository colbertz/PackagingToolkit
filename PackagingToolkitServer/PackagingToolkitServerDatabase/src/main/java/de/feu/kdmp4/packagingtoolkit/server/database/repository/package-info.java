/**
 * Contains the interfaces that contain the methods for accessing the database.
 */

package de.feu.kdmp4.packagingtoolkit.server.database.repository;