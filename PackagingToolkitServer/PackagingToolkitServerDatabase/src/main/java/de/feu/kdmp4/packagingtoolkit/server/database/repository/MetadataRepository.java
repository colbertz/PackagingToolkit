package de.feu.kdmp4.packagingtoolkit.server.database.repository;

import org.springframework.data.repository.CrudRepository;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.MetadataEntity;

/**
 * Contains the methods for accessing the table metadata. This interface is implemented by Spring Data JPA during
 * the runtime. You can find the reference documentation of the Spring Data JPA here for further information:
 * {@link https://docs.spring.io/spring-data/jpa/docs/2.0.0.RELEASE/reference/html/}
 * @author Christopher Olbertz
 *
 */
public interface MetadataRepository extends CrudRepository<MetadataEntity, Long>  {
	/**
	 * Finds a metadata element by its name.
	 * @param metadataName The name of metadata we are interested in.
	 * @return The found metadata. 
	 */
	MetadataEntity findByMetadataName(String metadataName);
}
