/**
 * Contains the classes that represent tables in the database.
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.server.database.entities.classes;