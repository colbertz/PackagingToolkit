package de.feu.kdmp4.packagingtoolkit.server.database.entities.classes;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Contains a value a digital object has in a certain meta data.
 * @author Christopher Olbertz
 *
 */
@Entity
@Table(name = "DigitalObjectHasMetadata")
public class DigitalObjectHasMetadataEntity {
	private long digitalObjectHasMetadataId;
	private DigitalObjectEntity digitalObject;
	private MetadataEntity metadata;
	private String value;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getDigitalObjectHasMetadataId() {
		return digitalObjectHasMetadataId;
	}
	
	public void setDigitalObjectHasMetadataId(long digitalObjectHasMetadataId) {
		this.digitalObjectHasMetadataId = digitalObjectHasMetadataId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	public DigitalObjectEntity getDigitalObject() {
		return digitalObject;
	}
	
	public void setDigitalObject(DigitalObjectEntity digitalObject) {
		this.digitalObject = digitalObject;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	public MetadataEntity getMetadata() {
		return metadata;
	}
	
	public void setMetadata(MetadataEntity metadata) {
		this.metadata = metadata;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
