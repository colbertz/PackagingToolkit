package de.feu.kdmp4.packagingtoolkit.server.database.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectHasMetadataEntity;

/**
 * Contains the methods for accessing the table DigitalObjectHasMetadata. This interface is implemented by Spring Data JPA during
 * the runtime. You can find the reference documentation of the Spring Data JPA here for further information:
 * {@link https://docs.spring.io/spring-data/jpa/docs/2.0.0.RELEASE/reference/html/}
 * @author Christopher Olbertz
 *
 */
public interface DigitalObjectHasMetadataRepository extends CrudRepository<DigitalObjectHasMetadataEntity, Long>{
	public List<DigitalObjectHasMetadataEntity> findByDigitalObject(DigitalObjectEntity digitalObject);
}
