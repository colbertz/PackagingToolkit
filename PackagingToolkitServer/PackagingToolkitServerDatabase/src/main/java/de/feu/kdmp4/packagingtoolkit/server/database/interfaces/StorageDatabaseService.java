package de.feu.kdmp4.packagingtoolkit.server.database.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.DigitalObjectRepository;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;

public interface StorageDatabaseService {
	/**
	 * Checks if a file with a given md5 checksum exists in the storage directory. 
	 * @param md5Checksum The md5 checksum for the search for a digital object.
	 * @return The found digital object or an empty optional.
	 */
	Optional<DigitalObjectExchange> findDigitalObjectByMd5Checksum(final String md5Checksum);
	/**
	 * Sets a reference to the object that contains the methods for accessing the table digitalobject.
	 * @param digitalObjectRepository The object that contains the methods for accessing the table digitalobject.
	 */
	void setDigitalObjectRepository(final DigitalObjectRepository digitalObjectRepository);
	/**
	 * Saves the data of a digital object in the database.
	 * @param filename The filename of the digital object in the storage directory of the server. 
	 * @param md5Checksum The md5 checksum of this file.
	 * @return The uuid of the newly created digital object.
	 */
	Uuid saveDigitalObject(final String filename, final String md5Checksum);
	/**
	 * Counts the information packages that contain this digital object.
	 * @param uuidOfDigitalObject The uuid of the digital object we are interested in.
	 * @return The number of the information packages.
	 */
	long countReferencesOfDigitalObject(final String uuidOfDigitalObject);
	/**
	 * Looks for the references of an information package.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return The references this information package is pointing to.
	 */
	ReferenceCollection findReferencesOfInformationPackage(final String uuidOfInformationPackage);
	/**
	 * Deletes a digital object from the database.
	 * @param uuidOfDigitalObject The uuid of the digital object to delete.
	 */
	void deleteDigitalObject(final String uuidOfDigitalObject);
	/**
	 * Finds a file in the storage by its checksum and returns its path in the storage.
	 * @param md5Checksum The md5 checksum of the file.
	 * @return The path of the file.
	 */
	String findFilePathByChecksum(final String md5Checksum);
	/**
	 * Finds all information packages stored in the database.
	 * @return All found information packages.
	 */
	InformationPackageExchangeCollection findAllInformationPackages();
	// DELETE_ME Gibt es schon in dieser Klasse.
	//DigitalObjectExchange findDigitalObjectByChecksum(final String md5Checksum);
	/**
	 * Finds a digital object in the database identified by its uuid.
	 * @param uuidOfReference The uuid of the reference.
	 * @return The reference found in the database.
	 */
	Reference findReferenceByUuid(final String uuidOfReference);
	/**
	 * Determines if there are any metadata saved for this digital object.
	 * @param uuidOfDigitalObject The uuid of the digital object we want to determine if there are any metadata of.
	 * @return True, if there are metadata for this digital object saved on the server, false otherwise. 
	 */
	boolean hasDigitalObjectSavedMetadata(final Uuid uuidOfDigitalObject);
	/**
	 * Saves some metadata for a digital object in the database.
	 * @param metadataExchangeCollection Contains the information about the metadata to saved and their digital
	 * object.
	 */
	void saveMetadataForDigitalObject(final MetadataExchangeCollection metadataExchangeCollection);
	/**
	 * Finds the metadata of a certain file with the help of its uuid.
	 * @param uuidOfFile The uuid of the file whose metadata we want to find.
	 * @return The metadata of the file with the uuid uuidOfFile.
	 */
	MetadataExchangeCollection findMetadataOfFileByUuid(final Uuid uuidOfFile);
}
