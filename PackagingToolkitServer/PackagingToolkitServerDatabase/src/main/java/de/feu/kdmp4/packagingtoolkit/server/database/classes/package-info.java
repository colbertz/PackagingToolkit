/**
 * Contains the classes that contain the business logic of the module Database.
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.server.database.classes;