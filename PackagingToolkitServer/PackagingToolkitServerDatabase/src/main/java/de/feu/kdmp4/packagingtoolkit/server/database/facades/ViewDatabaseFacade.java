package de.feu.kdmp4.packagingtoolkit.server.database.facades;

import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.DatabaseService;

/**
 * Contains the methods the module Views uses for accessing the methods of the module Database.
 * @author Christopher Olbertz
 *
 */
public class ViewDatabaseFacade {
	/**
	 * A reference to an object that contains the business logic of the module database.
	 */
	private DatabaseService databaseService;
	
	/**
	 * Determines all views from the database that are assigned to the
	 * current base ontology.
	 * @return All views assigned to the current base ontology found in the
	 * database.
	 */
	public ViewListResponse getAllViews() {
		return databaseService.findAllViews();
	}
	
	/**
	 * Saves a new view in the database.
	 * @param viewResponse The view that should be saved in the database. 
	 */
	public void saveNewView(ViewResponse view) {
		databaseService.saveNewView(view);
	}
	
	/**
	 * Deletes a view from the database.
	 * @param viewId The id of the view that should be deleted.
	 */
	public void deleteView(int viewId) {
		databaseService.deleteView(viewId);
	}
	
	/**
	 * Sets a reference to an object that contains the business logic of the module database.
	 * @param databaseService A reference to an object that contains the business logic of the module database.
	 */
	public void setDatabaseService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}
}
