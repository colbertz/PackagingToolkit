/**
 * Contains the facades classes that other modules have to use to access the methods of this module.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.server.database.facades;