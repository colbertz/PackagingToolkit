package de.feu.kdmp4.packagingtoolkit.server.database.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage.ViewException;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.OntologyClassRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.ViewRepository;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;

/**
 * Contains the methods for accessing the database.
 * @author Christopher Olbertz
 *
 */
public interface DatabaseService {
	/**
	 * Determines all views from the database that are assigned to the
	 * current base ontology.
	 * @return All views assigned to the current base ontology found in the
	 * database.
	 */
	ViewListResponse findAllViews();
	/**
	 * Saves a new view in the database.
	 * @param viewResponse The view that should be saved in the database. 
	 * @throws ViewException if the name of the view already exists in the database.
	 */
	void saveNewView(final ViewResponse viewResponse);
	/**
	 * Sets a reference to the view repository.
	 * @param viewRepository A reference to the repository that contains the methods for accessing the view
	 * table.
	 */
	void setViewRepository(final ViewRepository viewRepository);
	/**
	 * Sets a reference to the view repository.
	 * @param viewRepository A reference to the repository that contains the methods for accessing the ontologyclass
	 * table.
	 */
	void setOntologyClassRepository(final OntologyClassRepository ontologyClassRepository);
	/**
	 * Deletes a view from the database.
	 * @param viewId The id of the view that should be deleted.
	 */
	void deleteView(final int viewId);
	/**
	 * Saves a new information package in the database. If the information already exists it is updated in the database.
	 * @param informationPackageExchange Contains the information about the information package that should be saved.
	 */
	void saveInformationPackage(final InformationPackageExchange informationPackageExchange);
	/**
	 * Deletes an information package from the database.
	 * @param uuid The uuid of the information package that should be deleted.
	 */
	void deleteInformationPackage(final String uuid);
	/**
	 * Finds an information package in the database.
	 * @param uuidOfInformationPackage The uuid of the information package we are looking for.
	 * @return Contains the information package or is an empty optional if no information package with this uuid was found.
	 */
	Optional<InformationPackageExchange> findInformationPackageByUuid(final String uuidOfInformationPackage);
	/**
	 * Checks if there are data for an information package in the database.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return True, if there are data for this information package in the database, false otherwise.
	 */
	boolean informationPackageExists(final String uuidOfInformationPackage);
	/**
	 * Checks if there are data for an information package in the database.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return True, if there are no data for this information package in the database, false otherwise.
	 */
	boolean informationPackageDoesNotExist(final String uuidOfInformationPackage);
	/**
	 * Checks if there is already a view existing in the database with a given name.
	 * @param viewName The name of the view we are looking for. 
	 * @return True if there is a view with the name viewName in the database, false otherwise.
	 */
	boolean viewExistsByName(String viewName);
}
