package de.feu.kdmp4.packagingtoolkit.server.database.facades;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.StorageDatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;

/**
 * This facade contains the methods the module packaging can use for accessing the module database.
 * @author Christopher Olbertz
 *
 */
public class PackagingDatabaseFacade {
	/**
	 * Contains the business logic for the storage part of the database.
	 */
	private StorageDatabaseService storageDatabaseService;
	
	/**
	 * Finds all information packages stored in the database.
	 * @return All found information packages.
	 */
	public InformationPackageExchangeCollection findAllInformationPackages() {
		return storageDatabaseService.findAllInformationPackages();
	}
	
	/**
	 * Checks if a file with a given md5 checksum exists in the storage directory. 
	 * @param md5Checksum The md5 checksum for the search for a digital object.
	 * @return The found digital object or an empty optional.
	 */
	public Optional<DigitalObjectExchange> findDigitalObjectByMd5Checksum(final String md5Checksum) {
		return storageDatabaseService.findDigitalObjectByMd5Checksum(md5Checksum);
	}
	
	/**
	 * Looks for the references of an information package.
	 * @param uuidOfInformationPackage The uuid of the information package.
	 * @return The references this information package is pointing to.
	 */
	public ReferenceCollection findReferencesOfInformationPackage(String uuidOfInformationPackage) {
		return storageDatabaseService.findReferencesOfInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Determines if there are any metadata saved for this digital object.
	 * @param uuidOfDigitalObject The uuid of the digital object we want to determine if there are any metadata of.
	 * @return True, if there are metadata for this digital object saved on the server, false otherwise. 
	 */
	public boolean hasDigitalObjectSavedMetadata(final Uuid uuidOfDigitalObject) {
		return storageDatabaseService.hasDigitalObjectSavedMetadata(uuidOfDigitalObject);
	}
	
	/**
	 * Finds the metadata of a certain file with the help of its uuid.
	 * @param uuidOfFile The uuid of the file whose metadata we want to find.
	 * @return The metadata of the file with the uuid uuidOfFile.
	 */
	public MetadataExchangeCollection findMetadataOfFileByUuid(final Uuid uuidOfFile) {
		return storageDatabaseService.findMetadataOfFileByUuid(uuidOfFile);
	}
	
	/**
	 * Saves some metadata for a digital object in the database.
	 * @param metadataExchangeCollection Contains the information about the metadata to saved and their digital
	 * object.
	 */
	public void saveMetadataForDigitalObject(final MetadataExchangeCollection metadataExchangeCollection) {
		storageDatabaseService.saveMetadataForDigitalObject(metadataExchangeCollection);
	}
	
	/**
	 * Sets a reference to the object that contains the business logic of the part for the storage of the module database.
	 * @param storageDatabaseService A reference to the object that contains the business logic of the part for the storage of 
	 * the module database.
	 */
	public void setStorageDatabaseService(StorageDatabaseService storageDatabaseService) {
		this.storageDatabaseService = storageDatabaseService;
	}
}
