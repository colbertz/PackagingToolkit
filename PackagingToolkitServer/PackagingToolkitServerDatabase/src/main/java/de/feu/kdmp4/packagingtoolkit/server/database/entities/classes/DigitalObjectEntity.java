package de.feu.kdmp4.packagingtoolkit.server.database.entities.classes;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Represents a digital object in the database. A digital object is  related to a file in the storage, but not every file in the 
 * storage has a digital object in the database. The digital objects in the database are only local files in the storage.
 * @author Christopher Olbertz
 *
 */
@Entity
@Table(name = "digitalObject")
public class DigitalObjectEntity {
	/**
	 * The primary key. The uuid of the file is not used as primary key because an incrementing long
	 * is simpler to use.
	 */
	private long digitalObjectId;
	/**
	 * The path of the file in the storage directory. Does not contain the absolute path but only the filename and eventually some
	 * subdirectories in the storage directory.
	 */
	private String filePathInStorage;
	/**
	 * The md5 checksum of the file. 
	 */
	private String md5Checksum;
	/**
	 * The uuid of the digital object.
	 */
	private String uuid;
	/**
	 * The information packages that references to the digital object.
	 */
	private List<InformationPackageEntity> informationPackages;
	
	/**
	 * Constructs a new digital object and creates a new uuid.
	 */
	public DigitalObjectEntity() {
		uuid = new Uuid().toString();
		informationPackages = new ArrayList<>();
	}

	/**
	 * Constructs a new digital object and creates a new uuid.
	 * @param filePathInStorage A path in the storage directory.
	 * @param md5Checksum The md5 checksum of the file.
	 */
	public DigitalObjectEntity(final String filePathInStorage, final String md5Checksum) {
		this();
		this.filePathInStorage = filePathInStorage;
		this.md5Checksum = md5Checksum;
	}
	
	/**
	 * Adds an information package to this digital object. This means that informationPackage is referencing to this
	 * digital object.
	 * @param informationPackage The information package that is referencing to this digital object.
	 */
	public void addReferenceToInformationPackage(final InformationPackageEntity informationPackage) {
		informationPackages.add(informationPackage);
	}
	
	/**
	 * Removes a reference to this digital object. This is necessary if an information package has been removed.
	 * @param informationPackage The information package that should not more longer reference to this
	 * digital object.
	 */
	public void removeReferenceToInformationPackage(final InformationPackageEntity informationPackage) {
		informationPackages.remove(informationPackage);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getDigitalObjectId() {
		return digitalObjectId;
	}
	
	public void setDigitalObjectId(long digitalObjectId) {
		this.digitalObjectId = digitalObjectId;
	}
	
	public String getFilePathInStorage() {
		return filePathInStorage;
	}
	
	public void setFilePathInStorage(String filePathInStorage) {
		this.filePathInStorage = filePathInStorage;
	}
	
	public String getMd5Checksum() {
		return md5Checksum;
	}
	
	public void setMd5Checksum(String md5Checksum) {
		this.md5Checksum = md5Checksum;
	}
	
	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	public List<InformationPackageEntity> getInformationPackages() {
		return informationPackages;
	}

	public void setInformationPackages(List<InformationPackageEntity> informationPackages) {
		this.informationPackages = informationPackages;
	}
}
