package de.feu.kdmp4.packagingtoolkit.server.database.classes;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectHasMetadataEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.MetadataEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.factories.EntityFactory;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.StorageDatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.DigitalObjectHasMetadataRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.DigitalObjectRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.InformationPackageRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.MetadataRepository;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.MetadataExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.factories.ExchangeModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerModelFactory;
import de.feu.kdmp4.packagingtoolkit.server.factories.ServerOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;

public class StorageDatabaseServiceImpl implements StorageDatabaseService {
	/**
	 * The repository for accessing the table digitalobject.
	 */
	@Autowired
	protected DigitalObjectRepository digitalObjectRepository;
	/**
	 * The repository for accessing the table digitalobjecthasmetadata.
	 */
	@Autowired
	protected DigitalObjectHasMetadataRepository digitalObjectHasMetadataRepository;
	
	/**
	 * The repository for accessing the table informationpackage.
	 */
	@Autowired
	protected InformationPackageRepository informationPackageRepository;
	/**
	 * The repository for accessing the table metadata.
	 */
	@Autowired
	protected MetadataRepository metadataRepository;
	
	@Override
	public Optional<DigitalObjectExchange> findDigitalObjectByMd5Checksum(final String md5Checksum) {
		final DigitalObjectEntity digitalObjectEntity = digitalObjectRepository.findByMd5Checksum(md5Checksum);
		
		if (digitalObjectEntity != null) {
			final String uuidAsString = digitalObjectEntity.getUuid();
			final Uuid uuid = PackagingToolkitModelFactory.getUuid(uuidAsString);
			final String filename = digitalObjectEntity.getFilePathInStorage();
			final DigitalObjectExchange digitalObjectExchange = ExchangeModelFactory.createDigitalObjectExchange(filename, uuid, md5Checksum);
			return ServerOptionalFactory.createOptionalWithDigitalObjectExchange(digitalObjectExchange);
		}
		
		return ServerOptionalFactory.createEmptyOptionalWithDigitalObjectExchange();
	} 
	
	@Override
	public boolean hasDigitalObjectSavedMetadata(final Uuid uuidOfDigitalObject) {
		final int metadataCount = digitalObjectRepository.countMetadataOfDigitalObject(uuidOfDigitalObject.toString());
		if (metadataCount == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public void saveMetadataForDigitalObject(final MetadataExchangeCollection metadataExchangeCollection) {
		final String uuidOfFile = metadataExchangeCollection.getUuidOfFile();
		final DigitalObjectEntity digitalObjectEntity = digitalObjectRepository.findByUuid(uuidOfFile);
		
		for (int i = 0; i < metadataExchangeCollection.getMetadataCount(); i++) {
			final MetadataExchange metadataExchange = metadataExchangeCollection.getMetadataAt(i);
			final String metadataName = metadataExchange.getMetadataName();
			final String metadataValue = metadataExchange.getMetadataValue();
			
			MetadataEntity metadataEntity = metadataRepository.findByMetadataName(metadataName);
			if (metadataEntity == null) {
				metadataEntity = EntityFactory.createMetadataEntity();
				metadataEntity.setMetadataName(metadataName);
				metadataRepository.save(metadataEntity);
			}
			
			DigitalObjectHasMetadataEntity digitalObjectHasMetadataEntity = EntityFactory.createDigitalObjectHasMetadataEntity(digitalObjectEntity, metadataEntity, metadataValue);
			digitalObjectHasMetadataRepository.save(digitalObjectHasMetadataEntity);
		}
	}
	
	@Override
	public ReferenceCollection findReferencesOfInformationPackage(final String uuidOfInformationPackage) {
		final List<Reference> referenceCollection = new ArrayList<>();
		final List<DigitalObjectEntity> digitalObjects = digitalObjectRepository.findDigitalObjectsOfInformationPackage(uuidOfInformationPackage);
		
		for(DigitalObjectEntity digitalObjectEntity: digitalObjects) {
			final String url = digitalObjectEntity.getFilePathInStorage();
			final String uuidOfDigitalObjectAsString = digitalObjectEntity.getUuid();
			final Uuid uuidOfDigitalObject = PackagingToolkitModelFactory.getUuid(uuidOfDigitalObjectAsString);
			final Reference reference = ServerModelFactory.createLocalFileReference(url, uuidOfDigitalObject);
			referenceCollection.add(reference);
		}
		
		final ReferenceCollection references = ServerModelFactory.createReferenceList(referenceCollection);
		return references;
	}
	
	@Override
	public long countReferencesOfDigitalObject(final String uuidOfDigitalObject) {
		return digitalObjectRepository.countReferencesOfDigitalObject(uuidOfDigitalObject);
	}
	
	@Override
	public Reference findReferenceByUuid(final String uuidOfReference) {
		final DigitalObjectEntity digitalObjectEntity = digitalObjectRepository.findByUuid(uuidOfReference);
		final String url = digitalObjectEntity.getFilePathInStorage();
		final Reference reference = ServerModelFactory.createLocalFileReference(url, new Uuid(uuidOfReference));
		return reference;
	}
	
	@Override
	public String findFilePathByChecksum(final String md5Checksum) {
		final DigitalObjectEntity digitalObjectEntity = digitalObjectRepository.findByMd5Checksum(md5Checksum);
		return digitalObjectEntity.getFilePathInStorage();
	}
	
	@Override
	public void deleteDigitalObject(final String uuidOfDigitalObject) {
		final DigitalObjectEntity digitalObjectEntity = digitalObjectRepository.findByUuid(uuidOfDigitalObject);
		final List<DigitalObjectHasMetadataEntity> metadata = digitalObjectHasMetadataRepository.findByDigitalObject(digitalObjectEntity);
		digitalObjectHasMetadataRepository.delete(metadata);
		digitalObjectRepository.delete(digitalObjectEntity);
	}
	
	private InformationPackageExchange createInformationPackageExchange(final InformationPackageEntity informationPackageEntity) {
		final InformationPackageExchange informationPackageExchange = ExchangeModelFactory.createInformationPackageExchange(informationPackageEntity.getTitle(), 
				informationPackageEntity.getUuid(), informationPackageEntity.getPackageType(), informationPackageEntity.getCreationDate(),
				informationPackageEntity.getLastModifiedDate(), informationPackageEntity.getSerializationFormat(), 
				informationPackageEntity.getInformationPackageId());
		return informationPackageExchange;
	}
	
	@Override
	public Uuid saveDigitalObject(final String filename, final String md5Checksum) {
		final DigitalObjectEntity digitalObjectEntity = EntityFactory.createDigitalObjectEntity(filename, md5Checksum);
		digitalObjectRepository.save(digitalObjectEntity);
		return new Uuid(digitalObjectEntity.getUuid());
	}
	
	@Override
	public void setDigitalObjectRepository(final DigitalObjectRepository digitalObjectRepository) {
		this.digitalObjectRepository = digitalObjectRepository;
	}

	@Override
	public InformationPackageExchangeCollection findAllInformationPackages() {
		final List<InformationPackageEntity> informationPackageEntities = (List<InformationPackageEntity>) informationPackageRepository.findAll();
		final InformationPackageExchangeCollection informationPackageExchangeCollection = ExchangeModelFactory.createEmptyInformationPackageExchangeCollection();
		
		for (final InformationPackageEntity informationPackageEntity: informationPackageEntities) {
			final InformationPackageExchange informationPackageExchange = createInformationPackageExchange(informationPackageEntity);
			informationPackageExchangeCollection.addInformationPackageExchange(informationPackageExchange);
		}
		
		return informationPackageExchangeCollection;
	}

	@Override
	public MetadataExchangeCollection findMetadataOfFileByUuid(final Uuid uuidOfFile) {
		final String uuidOfFileAsString = uuidOfFile.toString();
		final List<DigitalObjectHasMetadataEntity> metadataOfDigitalObject = digitalObjectRepository.
				findMetadataOfFileByUuid(uuidOfFileAsString);
		final MetadataExchangeCollection metadata = ExchangeModelFactory.createMetadataExchangeCollection(uuidOfFileAsString);
		
		for (DigitalObjectHasMetadataEntity entity: metadataOfDigitalObject) {
			final String metadataName = entity.getMetadata().getMetadataName();
			final String metadataValue = entity.getValue();
			metadata.addMetadata(metadataName, metadataValue);
		}
		
		return metadata;
	}
}
