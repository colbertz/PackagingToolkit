package de.feu.kdmp4.packagingtoolkit.server.database.factories;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectHasMetadataEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.MetadataEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.OntologyClassEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.ViewEntity;

/**
 * A factory for the entity objects. They represent the data in the database and they are only known in
 * the database module.
 * @author Christopher Olbertz
 *
 */
public abstract class EntityFactory {
	/**
	 * Creates an entity object for the table informationpackage.
	 * @return The entity object.
	 */
	public static InformationPackageEntity createInformationPackageEntity() {
		return new InformationPackageEntity();
	}
	
	/**
	 * Creates an entity object for the table ontologyclass.
	 * @return The entity object.
	 */
	public static OntologyClassEntity createOntologyClassEntity() {
		return new OntologyClassEntity();
	}
	
	/**
	 * Creates an entity object for the table metadata.
	 * @return The entity object.
	 */
	public static MetadataEntity createMetadataEntity() {
		return new MetadataEntity();
	}
	
	/**
	 * Creates an entity object for the table view.
	 * @return The entity object.
	 */
	public static ViewEntity createViewEntity() {
		return new ViewEntity();
	}
	
	/**
	 * Creates an entity object for the table digitalobject.
	 * @param filename The name of the digital object in the storage directory.
	 * @param md5Checksum The md5 checksum of the file.
	 * @return The entity object.
	 */
	public static DigitalObjectEntity createDigitalObjectEntity(final String filename, final String md5Checksum) {
		return new DigitalObjectEntity(filename, md5Checksum);
	}
	
	/**
	 * Creates an entity object for the table digitalobjecthasmetadata.
	 * @param digitalObjectEntity The digital object the metadata is assigned to.
	 * @param metadataEntity The metadata that the value is assigned to.
	 * @param metadataValue The value the metadata contains for this digital object.
	 * @return The entity object.
	 */
	public static DigitalObjectHasMetadataEntity createDigitalObjectHasMetadataEntity(final DigitalObjectEntity digitalObjectEntity,
			final MetadataEntity metadataEntity, final String metadataValue) {
		DigitalObjectHasMetadataEntity digitalObjectHasMetadataEntity = new DigitalObjectHasMetadataEntity();
		digitalObjectHasMetadataEntity.setDigitalObject(digitalObjectEntity);
		digitalObjectHasMetadataEntity.setMetadata(metadataEntity);
		digitalObjectHasMetadataEntity.setValue(metadataValue);
		return digitalObjectHasMetadataEntity;
	}
}
