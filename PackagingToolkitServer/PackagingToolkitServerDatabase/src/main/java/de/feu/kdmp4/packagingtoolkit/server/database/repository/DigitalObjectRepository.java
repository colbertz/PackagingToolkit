package de.feu.kdmp4.packagingtoolkit.server.database.repository;

import java.util.List; 

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectHasMetadataEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.MetadataEntity;
import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;

/**
 * Contains the methods for accessing the table digitalobject. This interface is implemented by Spring Data JPA during
 * the runtime. You can find the reference documentation of the Spring Data JPA here for further information:
 * {@link https://docs.spring.io/spring-data/jpa/docs/2.0.0.RELEASE/reference/html/}
 * @author Christopher Olbertz
 *
 */
public interface DigitalObjectRepository extends CrudRepository<DigitalObjectEntity, Long> {
	/**
	 * Looks for a digital object with a given md5 checksum.
	 * @param md5Checksum The md5 checksum we are looking for.
	 * @return The digital object with this md5 checksum saved in the storage directory of the server.
	 */
	DigitalObjectEntity findByMd5Checksum(String md5Checksum);
	/**
	 * Counts the information packages a digital object is referenced by.
	 * @param uuidOfDigitalObject The uuid of the digital object.
	 * @return The number of information packages pointing to the digital object identified by the uuid.
	 */
	@Query("SELECT COUNT(ip) FROM DigitalObjectEntity do JOIN do.informationPackages ip WHERE do.uuid = :uuidOfDigitalObject")
	long countReferencesOfDigitalObject(@Param("uuidOfDigitalObject") String uuidOfDigitalObject);
	/**
	 * Searches all digital objects the information packages with a given uuid references to.
	 * @param uuidOfInformationPackage The uuid of the information package whose digital objects we are looking for.
	 * @return The digital objects of this information package.
	 */
	@Query("SELECT do FROM DigitalObjectEntity do JOIN do.informationPackages ip WHERE ip.uuid = :uuid")
	List<DigitalObjectEntity> findDigitalObjectsOfInformationPackage(@Param(ParamConstants.PARAM_UUID)String uuidOfInformationPackage);
	/**
	 * Looks for a digital object.
	 * @param uuid The uuid of the digital object.
	 * @return The found digital object.
	 */
	DigitalObjectEntity findByUuid(String uuid);
	/**
	 * Counts the metadata that are saved in the database of a given digital object.
	 * @param uuidOfDigitalObject The uuid of the digital object whose metadata we want to count.
	 * @return The number of metadata of this digital object.
	 */
	@Query("SELECT COUNT (dm) FROM DigitalObjectHasMetadataEntity dm JOIN dm.digitalObject d WHERE d.uuid = :uuidOfDigitalObject")
	int countMetadataOfDigitalObject(@Param("uuidOfDigitalObject") String uuidOfDigitalObject);
	/**
	 * Finds all metadata that are saved for a certain digital object.
	 * @param uuidOfFile The uuid of the file that we are interested in.
	 * @return The metadata of this digital object.
	 */
	//@Query("SELECT dm FROM DigitalObjectHasMetadataEntity dm, MetadataEntity m JOIN dm.metadata m WHERE dm.digitalObject.uuid = :uuidOfFile")
	@Query("SELECT dm FROM DigitalObjectHasMetadataEntity dm WHERE dm.digitalObject.uuid = :uuidOfFile")
	List<DigitalObjectHasMetadataEntity> findMetadataOfFileByUuid(@Param("uuidOfFile") String uuidOfFile);
}
