/**
 * Contains the interfaces for the classes that contain the business logic of the 
 * module Database.
 */

package de.feu.kdmp4.packagingtoolkit.server.database.interfaces;