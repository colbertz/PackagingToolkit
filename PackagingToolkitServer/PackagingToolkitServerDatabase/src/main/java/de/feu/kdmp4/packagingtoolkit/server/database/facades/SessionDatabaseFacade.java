package de.feu.kdmp4.packagingtoolkit.server.database.facades;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.DatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.StorageDatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;

/**
 * This facade contains the methods the module session can use for accessing the module database.
 * @author Christopher Olbertz
 *
 */
public class SessionDatabaseFacade {
	/**
	 * A reference to an object that contains the business logic of the module database.
	 */
	private DatabaseService databaseService;
	/**
	 * Contains the business logic for the storage part of the database.
	 */
	private StorageDatabaseService storageDatabaseService;
	
	/**
	 * Saves an information package in the database. If the information already exists it is updated in the database.
	 * @param informationPackageExchange Contains the information about the information package that should be saved.
	 */
	public void saveInformationPackage(final InformationPackageExchange informationPackageExchange) {
		databaseService.saveInformationPackage(informationPackageExchange);
	}
	
	/**
	 * Sets a reference to an object that contains the business logic of the module database.
	 * @param databaseService A reference to an object that contains the business logic of the module database.
	 */
	public void setDatabaseService(final DatabaseService databaseService) {
		this.databaseService = databaseService;
	}
	
	/**
	 * Finds a digital object in the database identified by its uuid.
	 * @param uuidOfReference The uuid of the reference.
	 * @return The reference found in the database.
	 */
	public Reference findReferenceByUuid(final String uuidOfReference) {
		return storageDatabaseService.findReferenceByUuid(uuidOfReference);
	}
	
	/**
	 * Finds an information package in the database.
	 * @param uuidOfInformationPackage The uuid of the information package we are looking for.
	 * @return Contains the information package or is an empty optional if no information package with this uuid was found.
	 */
	public Optional<InformationPackageExchange> findInformationPackageByUuid(final String uuidOfInformationPackage) {
		return databaseService.findInformationPackageByUuid(uuidOfInformationPackage);
	}
	
	/**
	 * Sets a reference to the object that contains the business logic of the part for the storage of the module database.
	 * @param storageDatabaseService A reference to the object that contains the business logic of the part for the storage of 
	 * the module database.
	 */
	public void setStorageDatabaseService(final StorageDatabaseService storageDatabaseService) {
		this.storageDatabaseService = storageDatabaseService;
	}
}
