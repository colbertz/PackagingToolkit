package de.feu.kdmp4.server.database.test.api;

import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

public class ViewTestApi {
    /**
     * Creates a view response that is send to the service layer for storing in the database. The view has no 
     * id because it should get the id during the saving process.
     * @param ontologyClassListResponse The ontology classes that are stored in the view.
     * @return The view that is send to the service layer.
     */
    public static ViewResponse createNewViewResponse(OntologyClassListResponse ontologyClassListResponse) {
    	ViewResponse newView = new ViewResponse();
    	
    	newView.setViewName("test view");
    	newView.addOntologyClass(ontologyClassListResponse.getOntologyClassAt(0));
    	newView.addOntologyClass(ontologyClassListResponse.getOntologyClassAt(1));
    	newView.addOntologyClass(ontologyClassListResponse.getOntologyClassAt(2));
    	
    	return newView;
    }
}
