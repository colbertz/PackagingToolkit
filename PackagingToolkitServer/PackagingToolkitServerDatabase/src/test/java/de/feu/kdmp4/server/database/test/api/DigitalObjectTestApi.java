package de.feu.kdmp4.server.database.test.api;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsUnequal;

public class DigitalObjectTestApi {
	
	/**
	 * Compares the values of two digital objects.
	 * @param digitalObject A digital object.
	 * @param digitalObjectEntity A digital object entity from the database.
	 * @return True if the both objects have the same values, false otherwise.
	 */
	public static boolean compareDigitalObjects(DigitalObjectExchange digitalObject, DigitalObjectEntity digitalObjectEntity) {
		if (!digitalObject.getUuidOfDigitalObject().toString().equals(digitalObjectEntity.getUuid())) {
			return false;
		}
		
		if (areStringsUnequal(digitalObject.getFilePathInStorage(), digitalObjectEntity.getFilePathInStorage())) {
			return false;
		}
		
		if(areStringsUnequal(digitalObject.getMd5Checksum(), digitalObjectEntity.getMd5Checksum())) {
			return false;
		}
		
		return true;
	}
}
