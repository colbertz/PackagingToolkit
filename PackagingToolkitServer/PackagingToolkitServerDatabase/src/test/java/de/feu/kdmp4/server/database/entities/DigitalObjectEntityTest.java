package de.feu.kdmp4.server.database.entities;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;

import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;

/**
 * Contains the tests for the class {@link de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity}.
 * @author Christopher Olbertz
 *
 */
public class DigitalObjectEntityTest {
	/**
	 * Tests if an information package can be added to the digital object. This test is important because of the danger of 
	 * NullPointerException with the list.
	 */
	@Test
	public void testAddInformationPackage() {
		final DigitalObjectEntity digitalObjectEntity = new DigitalObjectEntity("/temp/file", "f396b2302c4e6281029efad8ab253443");
		final InformationPackageEntity informationPackage = new InformationPackageEntity();
		digitalObjectEntity.addReferenceToInformationPackage(informationPackage);
		
		assertThat(digitalObjectEntity.getInformationPackages().size(), is(equalTo(1)));
	}
	
	/**
	 * Tests if an information package can be added to the digital object. This test is important because of the danger of 
	 * NullPointerException with the list.
	 */
	@Test
	public void testRemoveInformationPackage() {
		final DigitalObjectEntity digitalObjectEntity = new DigitalObjectEntity("/temp/file", "f396b2302c4e6281029efad8ab253443");
		final InformationPackageEntity informationPackage1 = new InformationPackageEntity();
		digitalObjectEntity.addReferenceToInformationPackage(informationPackage1);
		final InformationPackageEntity informationPackage2 = new InformationPackageEntity();
		digitalObjectEntity.addReferenceToInformationPackage(informationPackage2);
		
		digitalObjectEntity.removeReferenceToInformationPackage(informationPackage1);
		assertThat(digitalObjectEntity.getInformationPackages().size(), is(equalTo(1)));
		
		digitalObjectEntity.removeReferenceToInformationPackage(informationPackage2);
		assertThat(digitalObjectEntity.getInformationPackages().size(), is(equalTo(0)));
	}
	
	/**
	 * Tests if the constructor creates an uuid.
	 */
	@Test
	public void testConstructor() {
		final DigitalObjectEntity digitalObjectEntity = new DigitalObjectEntity("/temp/file", "f396b2302c4e6281029efad8ab253443");
		assertNotNull(digitalObjectEntity.getUuid());
	}
}
