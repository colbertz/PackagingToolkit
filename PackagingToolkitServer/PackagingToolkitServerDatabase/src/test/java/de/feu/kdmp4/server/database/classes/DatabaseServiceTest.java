package de.feu.kdmp4.server.database.classes;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.ViewEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.DatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.InformationPackageRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.OntologyClassRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.ViewRepository;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.server.database.test.api.InformationPackageTestApi;
import de.feu.kdmp4.server.database.test.api.OntologyClassTestApi;
import de.feu.kdmp4.server.database.test.api.ViewTestApi;

/**
 * Tests the class DatabaseService. The test uses DBUnit. The testdata are in a xml file that is read by the test. Before a test is
 * started, the database is initialized with the test data. The tests uses HSQLDB as in memory database. If you want to use another
 * database like MySQL you must change the configuration in application.properties.  
 * @author Christopher Olbertz
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = DatabaseConfiguration.class)
@ActiveProfiles("local")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("/META-INF/dbtest/testdata.xml")
public class DatabaseServiceTest {
	/**
	 * The number of views in the test database.
	 */
	private static final int VIEW_COUNT = 4;
	/**
	 * The class under test.
	 */
	@Autowired
	private DatabaseService databaseService;
	/**
	 * Contains the methods for accessing the table informationpackage.
	 */
	@Autowired
	private InformationPackageRepository informationPackageRepository;
	/**
	 * Contains the methods for accessing the table ontologyclass.
	 */
	@Autowired
	private OntologyClassRepository ontologyClassRepository;
	/**
	 * Contains the methods for accessing the table view.
	 */
	@Autowired
	private ViewRepository viewRepository;

	/**
	 * Tests the method getAllViews(). Four views are prepared for a mocked
	 * repository.
	 */
    @Test
    public void testGetAllViews() {
    	System.out.println(new Uuid());
        ViewListResponse viewListResponse = databaseService.findAllViews();
        int viewCount = viewListResponse.getViewCount();
        assertThat(viewCount, is(equalTo(VIEW_COUNT)));
    }
    
    /**
     * Tests if a view can be removed from the database. The view with the id 2 is removed.
     */
    @Test
    public void testDeleteView() {
    	databaseService.deleteView(2);
    	long actualViewCount = viewRepository.count();
    	long expectedViewCount = VIEW_COUNT - 1;
    	assertThat(actualViewCount, is(equalTo(expectedViewCount)));
    	boolean viewCorrectDeleted = checkResults_testDeleteView_correctViewDeleted() ;
    	assertTrue(viewCorrectDeleted);
    }
    
    /**
     * Checks if only the view with the id 2 was deleted.
     * @return False if another view was deleted than the desired one. 
     */
    private boolean checkResults_testDeleteView_correctViewDeleted() {
    	boolean correctDeleted = true;
    	Iterable<ViewEntity> viewIterable = viewRepository.findAll();
    	for (ViewEntity view: viewIterable) {
    		if (view.getViewId() == 2) {
    			correctDeleted = false;
    		}
    	}
    	
    	return correctDeleted;
    }
    
	// ****************************************
	// testSaveNewView()
	// ****************************************
    /**
     * Tests the method saveNewView(). A fifth view is written into the database. The new view contains three ontology
     * classes. 
     */
    @Test
    public void testSaveNewView() {
    	OntologyClassListResponse ontologyClassListResponse = OntologyClassTestApi.findThreeOntologyClassesFromDatabase(ontologyClassRepository);
    	ViewResponse viewResponse = ViewTestApi.createNewViewResponse(ontologyClassListResponse);

    	databaseService.saveNewView(viewResponse);
    	
    	ViewEntity view = viewRepository.findOne(5);
    	assertNotNull(view);
    	boolean ontologyClassesCountCorrect = checkResults_testSaveNewView_correctNumberOfOntologyClasses(view, ontologyClassListResponse);
    	assertTrue(ontologyClassesCountCorrect);
    }
    
    /**
     * Checks the result of the method for adding a new view to the database. Checks if the class read from the database contain the correct
     * number of ontology classes.
     * @param view The view read from the database.
     * @param ontologyClassListResponse The ontology classes we are expecting.
     * @return True if the number of ontology classes is correct, false otherwise.
     */
    private boolean checkResults_testSaveNewView_correctNumberOfOntologyClasses(ViewEntity view, OntologyClassListResponse ontologyClassListResponse) {
    	int actualOntologyClassesCount = view.getOntologyClassEntities().size();
    	int expectedOntologyClassesCount = ontologyClassListResponse.getOntologyClassCount();
    	return actualOntologyClassesCount == expectedOntologyClassesCount;
    }
    
	// ****************************************
	// testSaveNewInformationPackage()
	// ****************************************
    @Test
    public void testSaveNewInformationPackage() {
    	long expectedInformationPackageCount = informationPackageRepository.count() + 1;
    	InformationPackageExchange newInformationPackage = InformationPackageTestApi.createNewInformationPackage();
    	databaseService.saveInformationPackage(newInformationPackage);
    	
    	long actualInformationPackageCount = informationPackageRepository.count();
    	assertThat(actualInformationPackageCount, is(equalTo(expectedInformationPackageCount)));
    	InformationPackageEntity actualInformationPackage = informationPackageRepository.findOne(actualInformationPackageCount);
    	boolean valuesEqual = InformationPackageTestApi.compareInformationPackageValues(actualInformationPackage, newInformationPackage);
    	assertTrue(valuesEqual);
    }
    
    @Test
    public void testDeleteInformationPackage() {
    	long expectedInformationPackageCount = informationPackageRepository.count() - 1;
    	String uuid = "c3e9d521-e6b7-4e50-ad5d-44fb6ce27454";
    	databaseService.deleteInformationPackage(uuid);
    	
    	long actualInformationPackageCount = informationPackageRepository.count();
    	assertThat(actualInformationPackageCount, is(equalTo(expectedInformationPackageCount)));
    	boolean correctInformationPackageDeleted = checkResults_testDeleteInformationPackage_correctInformationPackages(uuid);
    	assertTrue(correctInformationPackageDeleted);
    }
    
    /**
     * Checks the result of the method for deleting an information package. The information package with the given uuid
     * may not be found in the database. 
     * @param uuid The uuid of the information package that has been deleted.
     * @return False if the information package with the given uuid is found.
     */
    private boolean checkResults_testDeleteInformationPackage_correctInformationPackages(String uuid) {
    	Iterable<InformationPackageEntity> iteratorInformationPackages = informationPackageRepository.findAll();
    	for (InformationPackageEntity informationPackageEntity: iteratorInformationPackages) {
    		if (areStringsEqual(informationPackageEntity.getUuid(), uuid)) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    /**
     * Tests if an information package can be find with its uuid. In this test the information package exists in the database.
     */
    @Test
    public void findInformationPackageByUuid_informationPackageFound() {
    	String uuid = "c3e9d521-e6b7-4e50-ad5d-44fb6ce27454";
    	Optional<InformationPackageExchange> optionalWithActualInformationPackage = databaseService.findInformationPackageByUuid(uuid);
    	assertTrue(optionalWithActualInformationPackage.isPresent());
    	InformationPackageExchange actualInformationPackage = optionalWithActualInformationPackage.get();
    	InformationPackageEntity expectedInformationPackage = informationPackageRepository.findInformationPackageEntityByUuid(uuid);
    	boolean valuesEqual = InformationPackageTestApi.compareInformationPackageValues(expectedInformationPackage, actualInformationPackage);
    	assertTrue(valuesEqual);
    }
    
    /**
     * Tests if an information package can be find with its uuid. In this test the information package does not exist in the database.
     */
    @Test
    public void findInformationPackageByUuid_informationPackageNotFound() {
    	String uuid = "c3e9d521-e6b7-4e50-ad5d-44fb6ce27433";
    	Optional<InformationPackageExchange> optionalWithActualInformationPackage = databaseService.findInformationPackageByUuid(uuid);
    	assertFalse(optionalWithActualInformationPackage.isPresent());
    }

    /**
     * Tests if an information package can be found in the database. 
     */
    @Test
    public void testInformationPackageExists_resultTrue() {
    	String uuid = "c3e9d521-e6b7-4e50-ad5d-44fb6ce27454";
    	boolean informationPackageExists = databaseService.informationPackageExists(uuid);
    	assertTrue(informationPackageExists);
    }
    
    /**
     * Tests if an information package can be found in the database. The information package does not exist in the database. 
     */
    @Test
    public void testInformationPackageExists_resultFalse() {
    	String uuid = "c3e9d521-e6b7-4e50-ad5d-44fb6ce27433";
    	boolean informationPackageExists = databaseService.informationPackageExists(uuid);
    	assertFalse(informationPackageExists);
    }
    
    /**
     * Tests if an information package can be found in the database. 
     */
    @Test
    public void testInformationPackageDoesExists_resultFalse() {
    	String uuid = "c3e9d521-e6b7-4e50-ad5d-44fb6ce27433";
    	boolean informationPackageExists = databaseService.informationPackageDoesNotExist(uuid);
    	assertTrue(informationPackageExists);
    }
    
    /**
     * Tests if an information package can be found in the database. The information package does not exist in the database. 
     */
    @Test
    public void testInformationPackageDoesNotExists_resultFalse() {
    	String uuid = "c3e9d521-e6b7-4e50-ad5d-44fb6ce27454";
    	boolean informationPackageExists = databaseService.informationPackageDoesNotExist(uuid);
    	assertFalse(informationPackageExists);
    }
}