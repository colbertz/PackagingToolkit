package de.feu.kdmp4.server.database.test.api;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsUnequal;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;
import de.feu.kdmp4.packagingtoolkit.server.exchange.classes.InformationPackageExchangeImpl;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;

/**
 * Creates test data for information packages.
 * @author Christopher Olbertz
 *
 */
public class InformationPackageTestApi {
	/**
	 * Creates an information package for writing in the database. This information package has no id because id gets its 
	 * id during the saving process.
	 * @return The information package for saving in the database.
	 */
	public static InformationPackageExchange createNewInformationPackage() {
		InformationPackageExchange informationPackageExchange = new InformationPackageExchangeImpl();
		
		informationPackageExchange.setCreationDate(LocalDateTime.now());
		informationPackageExchange.setLastModifiedDate(LocalDateTime.now());
		informationPackageExchange.setPackageType(PackageType.SIP);
		informationPackageExchange.setSerializationFormat(SerializationFormat.OAIORE);
		informationPackageExchange.setTitle("New information package");
		Uuid uuid = new Uuid();
		informationPackageExchange.setUuid(uuid.toString());
		
		return informationPackageExchange;
	}
	
	/**
	 * Compares an iterable with information packages and a collection. The contained information packages must be equal.
	 * @param iterableInformationPackages An iterable with information packages.
	 * @param informationPackageCollection A collection with information packages
	 * @return True if the contained information packages are equal, false otherwise.
	 */
	public static boolean compareInformationPackageCollections(Iterable<InformationPackageEntity> iterableInformationPackages, 
			InformationPackageExchangeCollection informationPackageCollection) {
    	int counter = 0;
    	
    	boolean okay = true;
    	
    	for (final InformationPackageEntity informationPackageEntity: iterableInformationPackages) {
    		final InformationPackageExchange informationPackageExchange = informationPackageCollection.getInformationPackageExchangeAt(counter);
    		okay = compareInformationPackageValues(informationPackageEntity, informationPackageExchange);
    		counter++;
    	}
    	
    	return okay;
	}
	
	/**
	 * Compares the values of two information packages.
	 * @param informationPackageEntity An information package entity object.
	 * @param informationPackageExchange An information package exchange object.
	 */
	public static boolean compareInformationPackageValues(InformationPackageEntity informationPackageEntity, InformationPackageExchange informationPackageExchange) {
		String actualTitle = informationPackageEntity.getTitle();
    	String expectedTitle = informationPackageExchange.getTitle();
    	if (areStringsUnequal(actualTitle, expectedTitle)) {
    		return false;
    	}
    	
    	String actualUuid = informationPackageEntity.getUuid();
    	String expectedUuid = informationPackageExchange.getUuid();
    	if (areStringsUnequal(actualUuid, expectedUuid)) {
    		return false;
    	}
    	 	
    	SerializationFormat actualSerializationFormat = informationPackageEntity.getSerializationFormat();
    	SerializationFormat expectedSerializationFormat = informationPackageExchange.getSerializationFormat();
    	if (!actualSerializationFormat.equals(expectedSerializationFormat)) {
    		return false;
    	}
    	
    	PackageType actualPackageType = informationPackageEntity.getPackageType();
    	PackageType expectedPackageType = informationPackageExchange.getPackageType();
    	if (!actualPackageType.equals(expectedPackageType)) {
    		return false;
    	}
    	
    	LocalDateTime actualCreationDate = informationPackageEntity.getCreationDate();
    	LocalDateTime expectedCreationDate = informationPackageExchange.getCreationDate();
    	if (actualCreationDate != null) {
	    	if (!actualCreationDate.equals(expectedCreationDate)) {
	    		return false;
	    	}
    	}
    	
    	LocalDateTime actualLastModifiedDate = informationPackageEntity.getLastModifiedDate();
    	LocalDateTime expectedLastModifiedDate = informationPackageExchange.getLastModifiedDate();
    	if (actualLastModifiedDate != null) {
	    	if (!actualLastModifiedDate.equals(expectedLastModifiedDate)) {
	    		return false;
	    	}
    	}
    	
    	return true;
	}
}
