package de.feu.kdmp4.server.database.classes;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
//import de.feu.kdmp4.packagingtoolkit.server.database.DatabaseConfiguration;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.DigitalObjectEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.InformationPackageEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.interfaces.StorageDatabaseService;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.DigitalObjectRepository;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.InformationPackageRepository;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.DigitalObjectExchange;
import de.feu.kdmp4.packagingtoolkit.server.exchange.interfaces.InformationPackageExchangeCollection;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.server.models.interfaces.ReferenceCollection;
import de.feu.kdmp4.server.database.test.api.DigitalObjectTestApi;
import de.feu.kdmp4.server.database.test.api.InformationPackageTestApi;

/**
 * Tests the class StorageDatabaseService. The test uses DBUnit. The testdata are in a xml file that is read by the test. Before a test is
 * started, the database is initialized with the test data. The tests uses HSQLDB as in memory database. If you want to use another
 * database like MySQL you must change the configuration in application.properties.  
 * @author Christopher Olbertz
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = DatabaseConfiguration.class)
@ActiveProfiles("local")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("/META-INF/dbtest/testdata.xml")
public class StorageDatabaseServiceTest {
	private static final long DIGITAL_OBJECT_COUNT = 5;
	
	/**
	 * The class under test.
	 */
	@Autowired
	private StorageDatabaseService storageDatabaseService;
	/**
	 * Contains the methods for accessing the table digitalobject.
	 */
	@Autowired
	private DigitalObjectRepository digitalObjectRepository;
	/**
	 * Contains the methods for accessing the table informationpackage.
	 */
	@Autowired
	private InformationPackageRepository informationPackageRepository;

	/**
	 * Tests if a digital object can be found with the help with its md5 checksum. In this test, the
	 * digital object exists.
	 */
	@Test
	public void testFindDigitalObjectByMd5Checksum_digitalObjectFound() {
		final String md5Checksum = "8399c3290dcbae050f002c0b8de19569";
		final Optional<DigitalObjectExchange> optionalWithDigitalObject = storageDatabaseService.findDigitalObjectByMd5Checksum(md5Checksum);
		assertTrue(optionalWithDigitalObject.isPresent());
		final DigitalObjectExchange digitalObject = optionalWithDigitalObject.get();
		
		final DigitalObjectEntity digitalObjectEntity = digitalObjectRepository.findOne(3l);
		boolean valuesEqual = DigitalObjectTestApi.compareDigitalObjects(digitalObject, digitalObjectEntity);
		assertTrue(valuesEqual);
	}
	
	/**
	 * Tests if a digital object can be found with the help with its md5 checksum. In this test, the
	 * digital object does not exist.
	 */
	@Test
	public void testFindDigitalObjectByMd5Checksum_digitalObjectNotFound() {
		final String md5Checksum = "8399c3290dcbae050f002c0b8de19511";
		final Optional<DigitalObjectExchange> optionalWithDigitalObject = storageDatabaseService.findDigitalObjectByMd5Checksum(md5Checksum);
		assertFalse(optionalWithDigitalObject.isPresent());
	}
	
	/**
	 * Tests saving a digital object in the database. The test checks if an uuid has been created and if the object was
	 * saved in the database.
	 */
	@Test
	public void testSaveDigitalObject() {
		final Uuid uuid = storageDatabaseService.saveDigitalObject("test55", "d374cf0300115d900718311688d47eaa");
		
		assertNotNull(uuid);
		final long digitalObjectCount = digitalObjectRepository.count();
		assertThat(digitalObjectCount, is(equalTo(DIGITAL_OBJECT_COUNT + 1)));
		checkResults_testSaveDigitalObject_digitalObject();
	}
	
	/**
	 * Checks if the object with the correct data has been saved in the database.
	 */
	private void checkResults_testSaveDigitalObject_digitalObject() {
		final DigitalObjectEntity digitalObjectEntity = digitalObjectRepository.findOne(DIGITAL_OBJECT_COUNT + 1);
		assertThat(digitalObjectEntity.getFilePathInStorage(), is(equalTo("test55")));
		assertThat(digitalObjectEntity.getMd5Checksum(), is(equalTo("d374cf0300115d900718311688d47eaa")));
	}
	
	/**
	 * Tests if the information packages that point to a specific digital object are correctly count. The expected count is 2.
	 */
	@Test
	public void testCountReferencesOfDigitalObject_result2() {
		final long referencesCount = storageDatabaseService.countReferencesOfDigitalObject("d587f1f1-587b-4170-b6ff-27fb083c24c6");
		assertThat(referencesCount, is(equalTo(2l)));
	}
	
	/**
	 * Tests if the information packages that point to a specific digital object are correctly count. The expected count is 1.
	 */
	@Test
	public void testCountReferencesOfDigitalObject_result1() {
		final long referencesCount = storageDatabaseService.countReferencesOfDigitalObject("d587f1f1-587b-4170-b6ff-27fb083c24c7");
		assertThat(referencesCount, is(equalTo(1l)));
	}
	
	/**
	 * Tests if the digital objects an information package references to are correctly determined.
	 */
	@Test
	public void testFindReferencesOfInformationPackage() {
		final ReferenceCollection referenceList = storageDatabaseService.findReferencesOfInformationPackage("c3e9d521-e6b7-4e50-ad5d-44fb6ce27454");
		checkResults_testFindReferencesOfInformationPackage_referenceList(referenceList);
	}
	
	/**
	 * Checks if the list contains the correct references.
	 * @param referenceList The list that should be checked.
	 */
	private void checkResults_testFindReferencesOfInformationPackage_referenceList(ReferenceCollection referenceList) {
		assertThat(referenceList.getReferencesCount(), is(equalTo(1)));
		final Reference reference = referenceList.getReference(0);
		assertThat(reference.getUuidOfFile().toString(), is(equalTo("d587f1f1-587b-4170-b6ff-27fb083c24c7")));
		assertThat(reference.getUrl(), is(equalTo("/tmp/file2")));
	}
	
	@Test
	public void testDeleteDigitalObject() {
		final long expectedDigitalObjectCount = digitalObjectRepository.count() - 1;
		final String uuid = "d587f1f1-587b-4170-b6ff-27fb083c24c4";
		storageDatabaseService.deleteDigitalObject(uuid);
		final long actualDigitalObjectCount = digitalObjectRepository.count() ;
		assertThat(actualDigitalObjectCount, is(equalTo(expectedDigitalObjectCount)));
		checkResults_testDeleteDigitalObject_correctDigitalObject(uuid);
	}
	
    /**
     * Checks the result of the method for deleting an information package. The information package with the given uuid
     * may not be found in the database. 
     * @param uuid The uuid of the information package that has been deleted.
     * @return False if the information package with the given uuid is found.
     */
    private boolean checkResults_testDeleteDigitalObject_correctDigitalObject(String uuid) {
    	final Iterable<DigitalObjectEntity> iteratorDigitalObjects = digitalObjectRepository.findAll();
    	for (DigitalObjectEntity DigitalObjectEntity: iteratorDigitalObjects) {
    		if (areStringsEqual(DigitalObjectEntity.getUuid(), uuid)) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    /**
     * Tests if a the correct path of a file is found with the help of its md5 checksum.
     */
    @Test
    public void testFindFilePathByChecksum() {
    	final String md5Checksum = "8399c3290dcbae050f002c0b8de19569";
    	final String actualFilePath = storageDatabaseService.findFilePathByChecksum(md5Checksum);
    	final String expectedFilePath = "/tmp/file3";
    	assertThat(actualFilePath, is(equalTo(expectedFilePath)));
    }
    
    /**
     * Tests if all information packages are correctly found in the database.
     */
    @Test
    public void testFindAllInformationPackages() {
    	final long expectedInformationPackageCount = informationPackageRepository.count();
    	Iterable<InformationPackageEntity> expectedInformationPackages = informationPackageRepository.findAll();
    	
    	final InformationPackageExchangeCollection actualInformationPackages = storageDatabaseService.findAllInformationPackages();
    	final long actualInformationPackageCount = actualInformationPackages.getInformationPackageExchangeCount();
    	assertThat(actualInformationPackageCount, is(equalTo(expectedInformationPackageCount)));
    	InformationPackageTestApi.compareInformationPackageCollections(expectedInformationPackages, actualInformationPackages);
    }
    
    /**
     * Tests if a reference can be found by its uuid. 
     */
    @Test
    public void testFindReferenceByUuid() {
    	String uuidOfReference = "d587f1f1-587b-4170-b6ff-27fb083c24c8";
    	Reference reference = storageDatabaseService.findReferenceByUuid(uuidOfReference);
    	assertThat(reference.getUrl(), is(equalTo("/tmp/file1")));
    	assertThat(reference.getUuidOfFile().toString(), is(equalTo(uuidOfReference))); 
    }
}
