/**
 * Contains helper classes for the tests. They for example create some test data.
 * @author Christopher Olbertz 
 *
 */
package de.feu.kdmp4.server.database.test.api;