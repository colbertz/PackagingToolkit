package de.feu.kdmp4.server.database.test.api;

import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.server.database.entities.classes.OntologyClassEntity;
import de.feu.kdmp4.packagingtoolkit.server.database.repository.OntologyClassRepository;

public class OntologyClassTestApi {
    /**
     * Creates some ontology classes reponse objects that are stored in the view, that is send to
     * the service layer in the test.
     * @return The list with the ontology classes for the test.
     */
    public static OntologyClassListResponse prepareTest_testSaveNewView_ontologyClassListResponse() {
    	OntologyClassResponse ontologyClass1 = new OntologyClassResponse();
    	OntologyClassResponse ontologyClass2 = new OntologyClassResponse();
    	OntologyClassResponse ontologyClass3 = new OntologyClassResponse();
    	ontologyClass1.setLocalName("Class 1");
    	ontologyClass1.setNamespace("http://de.feu.kdmp4");
    	ontologyClass2.setLocalName("Class 2");
    	ontologyClass2.setNamespace("http://de.feu.kdmp4");
    	ontologyClass3.setLocalName("Class 3");
    	ontologyClass3.setNamespace("http://de.feu.kdmp4");
    	OntologyClassListResponse ontologyClassListResponse = new OntologyClassListResponse();
    	ontologyClassListResponse.addOntologyClassToList(ontologyClass1);
    	ontologyClassListResponse.addOntologyClassToList(ontologyClass2);
    	ontologyClassListResponse.addOntologyClassToList(ontologyClass3);
    	return ontologyClassListResponse;
    }
    
    /**
     * Reads the first three ontology classes from the database and creates response objects for the test.
     * @param ontologyClassRepository The repository for the access to the database.
     * @return The ontology classes that can be used for the test.
     */
    public static OntologyClassListResponse findThreeOntologyClassesFromDatabase(OntologyClassRepository ontologyClassRepository) {
    	OntologyClassEntity ontologyClass1 = ontologyClassRepository.findOne(1);
    	OntologyClassEntity ontologyClass2 = ontologyClassRepository.findOne(2);
    	OntologyClassEntity ontologyClass3 = ontologyClassRepository.findOne(3);
    	
    	OntologyClassListResponse ontologyClassListResponse = new OntologyClassListResponse();
    	OntologyClassResponse ontologyClassResponse1 = new OntologyClassResponse(ontologyClass1.getNamespace(), ontologyClass1.getClassName());
    	ontologyClassResponse1.setOntologyClassId(ontologyClass1.getOntologyClassId());
    	OntologyClassResponse ontologyClassResponse2 = new OntologyClassResponse(ontologyClass2.getNamespace(), ontologyClass2.getClassName());
    	ontologyClassResponse2.setOntologyClassId(ontologyClass2.getOntologyClassId());
    	OntologyClassResponse ontologyClassResponse3 = new OntologyClassResponse(ontologyClass3.getNamespace(), ontologyClass3.getClassName());
    	ontologyClassResponse3.setOntologyClassId(ontologyClass3.getOntologyClassId());
    	
    	ontologyClassListResponse.addOntologyClassToList(ontologyClassResponse1);
    	ontologyClassListResponse.addOntologyClassToList(ontologyClassResponse2);
    	ontologyClassListResponse.addOntologyClassToList(ontologyClassResponse3);
    	
    	return ontologyClassListResponse;
    }
}
