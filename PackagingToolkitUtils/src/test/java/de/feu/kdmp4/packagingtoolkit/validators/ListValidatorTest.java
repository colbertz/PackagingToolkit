package de.feu.kdmp4.packagingtoolkit.validators;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class ListValidatorTest {
	private List<String> testList;
	private List<String> emptyList;
	
	@Before
	public void setUp() {
		testList = new ArrayList<>();
		testList.add("A");
		testList.add("B");
		testList.add("C");
		emptyList = new ArrayList<>();
	}
	
	@Test
	public void testIsNotEmpty1() {
		boolean isNotEmpty = ListValidator.isNotEmpty(testList);
		assertTrue(isNotEmpty);
	}

	@Test
	public void testIsNotEmpty2() {
		boolean isNotEmpty = ListValidator.isNotEmpty(emptyList);
		assertFalse(isNotEmpty);
	}

	@Test
	public void testIsEmpty1() {
		boolean isEmpty = ListValidator.isEmpty(testList);
		assertFalse(isEmpty);
	}

	@Test
	public void testIsEmpty2() {
		boolean isEmpty = ListValidator.isNotEmpty(emptyList);
		assertFalse(isEmpty);
	}
	
	@Test
	public void testIsNotNull1() {
		boolean isNotNull = ListValidator.isNotNull(testList);
		assertTrue(isNotNull);
	}

	@Test
	public void testIsNotNull2() {
		boolean isNotNull = ListValidator.isNotNull(null);
		assertFalse(isNotNull);
	}
	
	@Test
	public void testIsNotNullOrEmpty1() {
		boolean isNotNullOrEmpty = ListValidator.isNotNullOrEmpty(emptyList);
		assertFalse(isNotNullOrEmpty);
	}

	@Test
	public void testIsNotNullOrEmpty2() {
		boolean isNotNullOrEmpty = ListValidator.isNotNullOrEmpty(null);
		assertFalse(isNotNullOrEmpty);
	}
	
	@Test
	public void testIsNotNullOrEmpty3() {
		boolean isNotNullOrEmpty = ListValidator.isNotNullOrEmpty(testList);
		assertTrue(isNotNullOrEmpty);
	}
	
	@Test
	public void testIsIndexInRange1() {
		boolean isIndexInRange = ListValidator.isIndexInRange(-1, testList);
		assertFalse(isIndexInRange);
	}
	
	@Test
	public void testIsIndexInRange2() {
		boolean isIndexInRange = ListValidator.isIndexInRange(3, testList);
		assertFalse(isIndexInRange);
	}
	
	@Test
	public void testIsIndexInRange3() {
		boolean isIndexInRange = ListValidator.isIndexInRange(0, testList);
		assertTrue(isIndexInRange);
	}
}
