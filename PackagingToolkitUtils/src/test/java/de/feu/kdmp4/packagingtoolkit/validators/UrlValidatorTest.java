package de.feu.kdmp4.packagingtoolkit.validators;

import static org.junit.Assert.*;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.exceptions.UrlException;

public class UrlValidatorTest {

	@Test
	public void testIsUrlValid_validUrl_httpUrl() {
		String url = "http://mypage.com/test.html";
		boolean isUrlValid = UrlValidator.isValidUrl(url);
		assertTrue(isUrlValid);
	}
	
	@Test
	public void testIsUrlValid_validUrl_fileUrl() {
		String url = "file://mypage.com/myfile.txt";
		boolean isUrlValid = UrlValidator.isValidUrl(url);
		assertTrue(isUrlValid);
	}
	
	@Test
	public void testIsUrlValid_invalidUrl() {
		String url = "mypage.com";
		boolean isUrlValid = UrlValidator.isValidUrl(url);
		assertFalse(isUrlValid);
	}
	
	@Test
	public void testIsFileUrlTrue() {
		boolean isFileUrl = UrlValidator.isFileUrl("file://myfile");
		assertTrue(isFileUrl);
	}
	
	@Test(expected = UrlException.class)
	public void testCheckUrl_invalidUrl() {
		String url = "xyz://mypage.com/test.html";
		UrlValidator.checkUrl(url);
	}

	@Test
	public void testIsFileUrlFalse() {
		boolean isFileUrl = UrlValidator.isFileUrl("http://myPage.html");
		assertFalse(isFileUrl);
	}
	
	@Test
	public void testCheckFileUrlTrue() {
		UrlValidator.checkFileUrl("file://myfile");
	}

	@Test//(expected = UrlException.class)
	public void testCheckFileUrlFalse() {
		UrlValidator.checkFileUrl("http://myPage.html");
	}
	
	@Test
	public void testCheckHttpUrlTrue() {
		UrlValidator.checkHttpUrl("http://myPage.html");
	}

	@Test//(expected = UrlException.class)
	public void testCheckHttpUrlFalse() {
		UrlValidator.checkHttpUrl("file://myfile");
	}
	
	@Test
	public void testIsFileUrlNull() {
		boolean isFileUrl = UrlValidator.isFileUrl(null);
		assertFalse(isFileUrl);
	}
	
	@Test
	public void testIsHttpUrlFalse() {
		boolean isHttpUrl = UrlValidator.isHttpUrl("file://myfile");
		assertFalse(isHttpUrl);
	}
	
	@Test
	public void testIsHttpUrlTrue() {
		boolean isTrueUrl = UrlValidator.isHttpUrl("http://myPage.html");
		assertTrue(isTrueUrl);
	}
	
	@Test
	public void testIsHttpUrlNull() {
		boolean isHttpUrl = UrlValidator.isHttpUrl(null);
		assertFalse(isHttpUrl);
	}
}
