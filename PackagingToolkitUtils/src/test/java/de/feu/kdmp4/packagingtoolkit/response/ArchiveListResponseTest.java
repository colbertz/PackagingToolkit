package de.feu.kdmp4.packagingtoolkit.response;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.exceptions.response.ArchiveListResponseException;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.testapi.response.ResponseTestApi;

public class ArchiveListResponseTest {

	@Test
	public void testAddArchiveToList() {
		ArchiveListResponse archiveListResponse = new ArchiveListResponse();
		Uuid uuid1 = new Uuid();
		ArchiveResponse archive1 = new ArchiveResponse();
		archive1.setUuid(uuid1.toString());
		archiveListResponse.addArchiveToList(archive1);
		int size = archiveListResponse.getArchiveCount();
		assertThat(size, is(equalTo(1)));
		
		ArchiveResponse archive2 = new ArchiveResponse();
		Uuid uuid2 = new Uuid();
		archive2.setUuid(uuid2.toString());
		archiveListResponse.addArchiveToList(archive2);
		size = archiveListResponse.getArchiveCount();
		assertThat(size, is(equalTo(2)));
		
		ArchiveResponse archive3 = new ArchiveResponse();
		Uuid uuid3 = new Uuid();
		archive3.setUuid(uuid3.toString());
		archiveListResponse.addArchiveToList(archive3);
		size = archiveListResponse.getArchiveCount();
		assertThat(size, is(equalTo(3)));
	}
	
	@Test(expected = ArchiveListResponseException.class)
	public void testAddArchiveToListExceptions() {
		ArchiveListResponse archiveListResponse = new ArchiveListResponse();
		Uuid uuid1 = new Uuid();
		ArchiveResponse archive1 = new ArchiveResponse();
		archive1.setUuid(uuid1.toString());
		archiveListResponse.addArchiveToList(archive1);
		
		ArchiveResponse archive2 = new ArchiveResponse();
		archive2.setUuid(uuid1.toString());
		archiveListResponse.addArchiveToList(archive2);
	}

	// ******************************************************
	// testGetArchiveAt()
	// ******************************************************
	@Test
	public void testGetArchiveAt() {
		ArchiveListResponse archiveListResponse = new ArchiveListResponse();
		ArchiveResponse expectedArchive = ResponseTestApi.prepareArchivesAndReturnOneArchive(archiveListResponse);
		ArchiveResponse actualArchive = archiveListResponse.getArchiveAt(1);
		assertThat(actualArchive, is(equalTo(expectedArchive)));
	}
	
	// ******************************************************
	// testGetSize()
	// ******************************************************
	@Test
	public void testGetSize() {
		ArchiveListResponse archiveListResponse = new ArchiveListResponse();
		ResponseTestApi.prepareArchivesAndReturnOneArchive(archiveListResponse);
		int actualSize = archiveListResponse.getArchiveCount();
		assertThat(actualSize, is(equalTo(3)));
	}
	
	// ******************************************************
	// testRemoveArchive()
	// ******************************************************
	@Test
	public void testRemoveArchive() {
		ArchiveListResponse archiveListResponse = new ArchiveListResponse();
		ArchiveResponse archive = ResponseTestApi.prepareArchivesAndReturnOneArchive(archiveListResponse);
		archiveListResponse.removeArchive(archive);
		int actualSize = archiveListResponse.getArchiveCount();
		assertThat(actualSize, is(equalTo(2)));
	}
}
