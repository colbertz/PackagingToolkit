package de.feu.kdmp4.packagingtoolkit.validators;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import de.feu.kdmp4.packagingtoolkit.exceptions.ObjectException;

public class ObjectValidatorTest {

	@Test(expected = ObjectException.class)
	public void testCheckIsNullPositive() {
		ObjectValidator.checkIsNull(null);
	}

	@Test
	public void testCheckIsNullNegative() {
		String testString = "ABC";
		ObjectValidator.checkIsNull(testString);
	}
	
	@Test
	public void testIsNotNullPositive() {
		String testString = "ABC";
		boolean isNotNull = ObjectValidator.isNotNull(testString);
		assertTrue(isNotNull);
	}
	
	@Test
	public void testIsNotNullNegative() {
		boolean isNotNull = ObjectValidator.isNotNull(null);
		assertFalse(isNotNull);
	}
}
