package de.feu.kdmp4.packagingtoolkit.utils;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

public class StringUtilsTest {
	private StringList stringList;
	
	@Before
	public void setUp() throws Exception {
		stringList = new StringList();
		stringList.addStringToList("abc");
		stringList.addStringToList("def");
		stringList.addStringToList("ghij");
		stringList.addStringToList("klmopq");
		stringList.addStringToList("rst");
		stringList.addStringToList("uvw");
		stringList.addStringToList("xyz");
	}

	@Test
	public void testDeleteDoubleSharps_withDoubleSharps() {
		final String stringUnderTest = "stringWith##twoSharps";
		final String expectedString = "stringWith#twoSharps";
		final String actualString = StringUtils.deleteDoubleSharps(stringUnderTest);
		
		assertThat(actualString, is(equalTo(expectedString)));
	}
	
	@Test
	public void testDeleteDoubleSharps_withOneSharp() {
		final String stringUnderTest = "stringWith#oneSharp";
		final String expectedString = "stringWith#oneSharp";
		final String actualString = StringUtils.deleteDoubleSharps(stringUnderTest);
		
		assertThat(actualString, is(equalTo(expectedString)));
	}
	
	@Test
	public void testDeleteDoubleSharps_withNoSharp() {
		final String stringUnderTest = "stringWithNoSharp";
		final String expectedString = "stringWithNoSharp";
		final String actualString = StringUtils.deleteDoubleSharps(stringUnderTest);
		
		assertThat(actualString, is(equalTo(expectedString)));
	}
	
	@Test
	public void testDeleteTextInParentheses_textWithoutParantheses() {
		final String textWithoutParantheses = "I do not contain any parantheses and do not want to be modified!";
		final String actualString = StringUtils.deleteTextInParentheses(textWithoutParantheses);
		assertThat(actualString, is(equalTo(textWithoutParantheses)));
	}
	
	@Test
	public void testDeleteTextInParentheses_textEndingWithParantheses() {
		final String textWithoutParantheses = "I contain some parantheses (at the end)";
		final String actualString = StringUtils.deleteTextInParentheses(textWithoutParantheses);
		final String expectedString = "I contain some parantheses";
		assertEquals(expectedString, actualString);
	}
	
	@Test
	public void testDeleteTextInParentheses_textWithParanthesesInTheMiddle() {
		final String textWithoutParantheses = "I contain (some) parantheses";
		final String actualString = StringUtils.deleteTextInParentheses(textWithoutParantheses);
		final String expectedString = "I contain  parantheses";
		assertEquals(expectedString, actualString);
	}
	
	@Test
	public void testDeleteLastCharacter() {
		String aString = "abc";
		String expectedString = "ab";
		String actualString = StringUtils.deleteLastCharacter(aString);
		assertEquals(expectedString, actualString);
	}
	
	@Test
	public void testExtractLocalNameWithIri() {
		String iri = "http://kdmp4.feu.de#localName";
		String expectedLocalName = "localName";
		String actualLocalName = StringUtils.extractLocalNameFromIri(iri);
		assertThat(actualLocalName, is(equalTo(expectedLocalName)));
	}
	
	@Test
	public void testExtractLocalNameWithOutIri() {
		String iri = "notAnIri";
		String expectedLocalName = "notAnIri";
		String actualLocalName = StringUtils.extractLocalNameFromIri(iri);
		assertThat(actualLocalName, is(equalTo(expectedLocalName)));
	}
	@Test
	public void testReplaceLast() {
		String aString = "abc/def/xyz/xxx";
		String expectedString = "abc/def/xyz#xxx";
		String actualString = StringUtils.replaceLast(aString, '/', '#');
		assertEquals(actualString, expectedString);
	}
	
	@Test
	public void testExtractNamespaceWithIri() {
		String iri = "http://kdmp4.feu.de#localName";
		String expectedNamespace = "http://kdmp4.feu.de";
		String actualNamespace = StringUtils.extractNamespaceFromIri(iri);
		assertThat(actualNamespace, is(equalTo(expectedNamespace)));
	}
	
	@Test
	public void testExtractNamespaceWithOutIri() {
		String iri = "notAnIri";
		String expectedNamespace = "notAnIri";
		String actualNamespace = StringUtils.extractNamespaceFromIri(iri);
		assertThat(actualNamespace, is(equalTo(expectedNamespace)));
	}
	
	@Test
	public void testCreateCommaSeperatedList() {
		String expectedString = "abc,def,ghij,klmopq,rst,uvw,xyz";
		String actualString = StringUtils.createCommaSeperatedList(stringList);
		assertThat(actualString, is(equalTo(expectedString)));
	}

	@Test
	public void testSplitCommaSeparatedList() {
		String stringToSplit = "abc,def,ghij,klmopq,rst,uvw,xyz";
		StringList actualStringList = StringUtils.splitCommaSeparatedList(stringToSplit);
		
		for (int i = 0; i < actualStringList.getSize(); i++) {
			String actualString = actualStringList.getStringAt(i);
			String expectedString = stringList.getStringAt(i);
			assertThat(actualString, is(equalTo(expectedString)));
		}
	}
	
	@Test
	public void testLineFeedCommaSeparatedList() {
		String stringToSplit = "abc\ndef\nghij\nklmopq\nrst\nuvw\nxyz";
		StringList actualStringList = StringUtils.splitLineFeedSeparatedString(stringToSplit);
		
		for (int i = 0; i < actualStringList.getSize(); i++) {
			String actualString = actualStringList.getStringAt(i);
			String expectedString = stringList.getStringAt(i);
			assertEquals(actualString, expectedString);
		}
	}
	
	@Test
	public void testDeleteSpaces() {
		String stringWithSpaces = "   abc def ghi     jkl   mno   ";
		String expectedString = "abcdefghijklmno";
		String actualString = StringUtils.deleteSpaces(stringWithSpaces);
		assertThat(actualString, is(equalTo(expectedString)));
	}
	
	@Test
	public void testSplitAtUnderscore() {
		final String theString = "abc_x_xy";
		final String[] resultArray = StringUtils.splitAtUnderscore(theString);
		
		assertThat(resultArray.length, is(equalTo(3)));
		final String firstString = resultArray[0];
		assertThat(firstString, is(equalTo("abc")));
		final String secondString = resultArray[1];
		assertThat(secondString, is(equalTo("x")));
		final String thirdString = resultArray[2];
		assertThat(thirdString, is(equalTo("xy")));
	}
	
	@Test
	public void testAreStringsEqual_resultTrue() {
		String string1 = "abc";
		String string2 = "abc";
		
		boolean areTheyEqual = StringUtils.areStringsEqual(string1, string2);
		assertTrue(areTheyEqual);
	}
	
	@Test
	public void testAreStringsEqual_resultFalse() {
		String string1 = "abc";
		String string2 = "xyz";
		
		boolean areTheyEqual = StringUtils.areStringsEqual(string1, string2);
		assertFalse(areTheyEqual);
	}
	
	@Test
	public void testAreStringsUnequal_resultTrue() {
		String string1 = "abc";
		String string2 = "xyz";
		
		boolean areTheyEqual = StringUtils.areStringsUnequal(string1, string2);
		assertTrue(areTheyEqual);
	}
	
	@Test
	public void testAreStringsUnequal_resultFalse() {
		String string1 = "abc";
		String string2 = "abc";
		
		boolean areTheyEqual = StringUtils.areStringsUnequal(string1, string2);
		assertFalse(areTheyEqual);
	}
	
	@Test
	public void testIsUuid_validUuid() {
		String theString = "5ab16313-86c6-48fa-9c22-d5ed9e5112c7";
		boolean isUuid = StringUtils.isUuid(theString);
		assertTrue(isUuid);
	}
	
	@Test
	public void testIsUuid_invalidUuid() {
		String theString = "5ab16313-86c6";
		boolean isUuid = StringUtils.isUuid(theString);
		assertFalse(isUuid);
	}
}
