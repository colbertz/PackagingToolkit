package de.feu.kdmp4.packagingtoolkit.response;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ArchiveResponseTest {
	@Test
	public void testUuidsEqual() {
		UuidResponse uuid = new UuidResponse("0f32d13d-3c51-4f34-8d7b-efcae7fca802");
		
		ArchiveResponse archive = new ArchiveResponse();
		archive.setUuid("0f32d13d-3c51-4f34-8d7b-efcae7fca802");
		boolean uuidEqual = archive.areUuidsEqual(uuid);
		assertTrue(uuidEqual);
	}
	
	@Test
	public void isSiu_true() {
		ArchiveResponse archive = new ArchiveResponse();
		archive.setPackageType("SIU");
		
		boolean isSiu = archive.isSIU();
		assertTrue(isSiu);
	}
	
	@Test
	public void isSiu_false() {
		ArchiveResponse archive = new ArchiveResponse();
		archive.setPackageType("SIP");
		
		boolean isSiu = archive.isSIU();
		assertFalse(isSiu);
	}
}
