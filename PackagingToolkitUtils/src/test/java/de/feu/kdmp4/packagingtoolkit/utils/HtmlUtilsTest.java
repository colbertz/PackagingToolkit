package de.feu.kdmp4.packagingtoolkit.utils;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

public class HtmlUtilsTest {
	@Test
	public void testCreateHtmlUnorderedList() {
		final StringList stringsForTest = prepareTest_createStrings();
		final String actualString = HtmlUtils.createHtmlUnorderedList(stringsForTest);
		final String expectedString = "<UL><LI>abc</LI><LI>def</LI><LI>ghi</LI><LI>jkl</LI></UL>";
		assertEquals(expectedString, actualString);
	}
	
	@Test
	public void testCreateHtmlOrderedList() {
		final StringList stringsForTest = prepareTest_createStrings();
		final String actualString = HtmlUtils.createHtmlOrderedList(stringsForTest);
		final String expectedString = "<OL><LI>abc</LI><LI>def</LI><LI>ghi</LI><LI>jkl</LI></OL>";
		assertEquals(expectedString, actualString);
	}
	
	@Test
	public void testCreateHtmlItemOfList() {
		final String item = "abc";
		final String actualString = HtmlUtils.createHtmlItemOfList(item);
		final String expectedString = "<LI>abc</LI>";
		assertEquals(expectedString, actualString);
	}
	
	private StringList prepareTest_createStrings( ) {
		final StringList stringsForTest = new StringList();
		stringsForTest.addStringToList("abc");
		stringsForTest.addStringToList("def");
		stringsForTest.addStringToList("ghi");
		stringsForTest.addStringToList("jkl");
		
		return stringsForTest;
	}
}
