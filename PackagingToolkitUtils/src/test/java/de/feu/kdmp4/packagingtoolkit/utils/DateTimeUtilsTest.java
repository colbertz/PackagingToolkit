package de.feu.kdmp4.packagingtoolkit.utils;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.junit.Test;

public class DateTimeUtilsTest {
	
	@Test
	public void testGetValueAsDate1() {
		String dateAsString = "2017-02-01";
		LocalDate expectedDate = LocalDate.of(2017, 2, 1);
		LocalDate currentDate = DateTimeUtils.getValueAsDate(dateAsString);
		assertThat(currentDate, is(equalTo(expectedDate)));
	}
	
	@Test
	public void testGetValueAsDate2() {
		String dateAsString = "2017-12-11";
		LocalDate expectedDate = LocalDate.of(2017, 12, 11);
		LocalDate currentDate = DateTimeUtils.getValueAsDate(dateAsString);
		assertThat(currentDate, is(equalTo(expectedDate)));
	}
	
	@Test
	public void testGetValueAsDateTime1() {
		String dateTimeAsString = "2017-12-11 05:22:06";
		LocalDateTime expectedDateTime = LocalDateTime.of(2017, 12, 11, 5, 22, 6);
		LocalDateTime currentDateTime = DateTimeUtils.getValueAsDateTime(dateTimeAsString);
		assertThat(currentDateTime, is(equalTo(expectedDateTime)));
	}
	
	@Test
	public void testGetValueAsDateTime2() {
		String dateTimeAsString = "2017-12-11 13:22:11";
		LocalDateTime expectedDateTime = LocalDateTime.of(2017, 12, 11, 13, 22, 11);
		LocalDateTime currentDateTime = DateTimeUtils.getValueAsDateTime(dateTimeAsString);
		assertThat(currentDateTime, is(equalTo(expectedDateTime)));
	}
	
	@Test
	public void testGetValueAsTime1() {
		String timeAsString = "05:22:06";
		LocalTime expectedTime = LocalTime.of(5, 22, 6);
		LocalTime currentTime = DateTimeUtils.getValueAsTime(timeAsString);
		assertThat(currentTime, is(equalTo(expectedTime)));
	}
	
	@Test
	public void testGetValueAsTime2() {
		String timeAsString = "13:22:11";
		LocalTime expectedTime = LocalTime.of(13, 22, 11);
		LocalTime currentTime = DateTimeUtils.getValueAsTime(timeAsString);
		assertThat(currentTime, is(equalTo(expectedTime)));
	}
	
	@Test
	public void testGetOntologyStringAsDate_withAllValues() {
		String ontologyValue = "2014-01-05";
		LocalDate expectedLocalDate = LocalDate.of(2014, 1, 5);
		LocalDate actualLocalDate = DateTimeUtils.getOntologyStringAsDate(ontologyValue);
		assertThat(actualLocalDate, is(equalTo(expectedLocalDate)));
	}
	
	@Test(expected = DateTimeParseException.class)
	public void testGetOntologyStringAsDateTime_wrongFormat() {
		String ontologyValue = "2014-0105";
		DateTimeUtils.getOntologyStringAsDateTime(ontologyValue);
	}
	
	@Test
	public void testGetOntologyStringAsDateTime_withAllValues() {
		String ontologyValue = "2014-01-05T12:15:16";
		LocalDateTime expectedLocalDateTime = LocalDateTime.of(2014, 1, 5, 12, 15, 16);
		LocalDateTime actualLocalDateTime = DateTimeUtils.getOntologyStringAsDateTime(ontologyValue);
		assertThat(actualLocalDateTime, is(equalTo(expectedLocalDateTime)));
	}
	
	@Test
	public void testGetOntologyStringAsDateTime_withoutSeconds() {
		String ontologyValue = "2014-01-05T12:15";
		LocalDateTime expectedLocalDateTime = LocalDateTime.of(2014, 1, 5, 12, 15, 0);
		LocalDateTime actualLocalDateTime = DateTimeUtils.getOntologyStringAsDateTime(ontologyValue);
		assertThat(actualLocalDateTime, is(equalTo(expectedLocalDateTime)));
	}
	
	@Test(expected = DateTimeParseException.class)
	public void testGetOntologyStringAsDateTime_withoutT() {
		String ontologyValue = "2014-01-05 12:15:16";
		DateTimeUtils.getOntologyStringAsDateTime(ontologyValue);
	}
	
	@Test
	public void testConvertDateToLocalDate() {
		Date date = new Date(2015, 2, 5);
		LocalDate expectedLocalDate = LocalDate.of(2015, 2, 5);
		
		LocalDate actualLocalDate = DateTimeUtils.convertDateToLocalDate(date);
		assertThat(actualLocalDate, is(equalTo(expectedLocalDate)));
	}
	
	@Test
	public void testConvertDateToLocalDateTime() {
		Date date = new Date(2015, 2, 5, 12, 15, 16);
		LocalDateTime expectedLocalDateTime = LocalDateTime.of(2015, 2, 5, 12, 15, 16);
		
		LocalDateTime actualLocalDateTime = DateTimeUtils.convertDateToLocalDateTime(date);
		assertThat(actualLocalDateTime, is(equalTo(expectedLocalDateTime)));
	}
	
	@Test
	public void testConvertDateToLocalDateTime_withJanuary() {
		Date date = new Date(2015, 0, 5, 12, 15, 16);
		LocalDateTime expectedLocalDateTime = LocalDateTime.of(2015, 1, 5, 12, 15, 16);
		
		LocalDateTime actualLocalDateTime = DateTimeUtils.convertDateToLocalDateTime(date);
		assertThat(actualLocalDateTime, is(equalTo(expectedLocalDateTime)));
	}
	
	@Test
	public void testConvertLocalDateTimeToDate() {
		LocalDateTime localDateTime = LocalDateTime.of(2018, 2, 3, 4, 5, 6);
		Date expectedDate = new Date(2018, 3, 3, 4, 5, 6);
		Date actualDate = DateTimeUtils.convertLocalDateTimeToDate(localDateTime);
		
		assertThat(actualDate, is(equalTo(expectedDate)));
	}
}
