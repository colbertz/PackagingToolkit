package de.feu.kdmp4.packagingtoolkit.utils;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

public class UrlUtilsTest {
	@Test
	public void testInsertUrlParameters_oneParameter() {
		String url = "http://test.de/{parameter1}";
		String expectedUrl = "http://test.de/1";
		StringList parameterNames = new StringList(new String[]{"parameter1"});
		StringList parameterValues = new StringList(new String[]{"1"});
		
		String actualUrl = UrlUtils.replaceUrlParameters(url, parameterNames, parameterValues);
		assertThat(actualUrl, is(equalTo(expectedUrl)));
	}
	
	@Test
	public void testInsertUrlParameters_twoParameters() {
		String url = "http://test.de/{parameter1}/{parameter2}";
		String expectedUrl = "http://test.de/1/2";
		StringList parameterNames = new StringList(new String[]{"parameter1", "parameter2"});
		StringList parameterValues = new StringList(new String[]{"1", "2"});
		
		String actualUrl = UrlUtils.replaceUrlParameters(url, parameterNames, parameterValues);
		assertThat(actualUrl, is(equalTo(expectedUrl)));
	}
	
	@Test
	public void testInsertUrlParameters_withStrings() {
		String url = "http://test.de/{parameter1}";
		String expectedUrl = "http://test.de/1";
		
		String actualUrl = UrlUtils.replaceUrlParameters(url, "parameter1", "1");
		assertThat(actualUrl, is(equalTo(expectedUrl)));
	}
}
