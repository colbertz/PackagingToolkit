package de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

public class ViewExceptionTest {
	@Test
	public void testDuplicateViewNameException() {
		final ViewException viewException = ViewException.duplicateViewNameException("aName");
		final String expectedString = "In der Datenbank existiert bereits eine Sicht mit diesem Namen: aName!";
		assertEquals(expectedString, viewException.getMessage());
	}
}
