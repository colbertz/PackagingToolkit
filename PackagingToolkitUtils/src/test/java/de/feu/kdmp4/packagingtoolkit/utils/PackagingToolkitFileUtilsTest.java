package de.feu.kdmp4.packagingtoolkit.utils;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitFileException;
import de.feu.kdmp4.packagingtoolkit.exceptions.SystemException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.testapi.response.FileTestApi;

public class PackagingToolkitFileUtilsTest {
	// ************** Constants ****************
	private static final String FILE_CONTENT = "abcdefghijklmnopqrstuvwxyz";
	private static final String DIRECTORY_TO_DELETE_ROOT = "src/test/resources/testfiles/deleteRootFolder";
	private static final String DIRECTORY_TO_DELETE_1 = "src/test/resources/testfiles/deleteRootFolder/deleteFolder1";
	private static final String DIRECTORY_TO_DELETE_2 = "src/test/resources/testfiles/deleteRootFolder/deleteFolder2";
	private static final String DIRECTORY_TO_DELETE_3 = "src/test/resources/testfiles/deleteRootFolder/deleteFolder1/deleteFolder3";
	private static final String FILE_TO_DELETE_1 = "src/test/resources/testfiles/deleteRootFolder/deleteFile1";
	private static final String FILE_TO_DELETE_2 = "src/test/resources/testfiles/deleteRootFolder/deleteFile2";
	private static final String FILE_TO_DELETE_3 = "src/test/resources/testfiles/deleteRootFolder/deleteFolder1/deleteFolder3/deleteFile3";
	private static final String FILE_TO_DELETE_4 = "src/test/resources/testfiles/deleteRootFolder/deleteFolder1/deleteFile4";
	private static final String FILE_TO_DELETE_5 = "src/test/resources/testfiles/deleteRootFolder/deleteFolder2/deleteFile5";
	private static final String EMPTY_DIRECTORY = "src/test/resources/testfiles/emptyFolder";
	private static final String EMPTY_FILE = "src/test/resources/testfiles/notEmptyFolder/emptyFile";
	private static final String NEW_DIRECTORY = "src/test/resources/testfiles/newDirectory";
	private static final String NEW_FILE = "src/test/resources/testfiles/newFile";
	private static final String NOT_EMPTY_DIRECTORY = "src/test/resources/testfiles/notEmptyFolder";
	private static final String ZIP_FILE = "src/test/resources/testfiles/notEmptyFolder/emptyFile.zip";
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	
	// ****************** Setup and tear down ************
	/**
	 * Creates the directories and the files for the tests.
	 * @throws IOException
	 */
	@Before
	public void setUp() throws IOException {
		prepareDirectoriesAndFiles();
	}
	
	/**
	 * Deletes the directories and the files for the tests.
	 */
	@After
	public void tearDown() {
		deleteDirectoriesAndFiles();
	}
	
	// ***************** Test method *****************
	/**
	 * Tests the method for getting a filename without the suffix. This test uses
	 * a zip file.
	 */
	@Test
	public void testGetFileNameWithoutSuffix1() {
		String actualFileName = PackagingToolkitFileUtils.getFileNameWithoutSuffix(ZIP_FILE);
		assertThat(actualFileName, is(equalTo("emptyFile")));
	}
	
	/**
	 * Tests the method for getting a filename without the suffix. This test uses
	 * a file without a suffix. The file name may not be modified after the test.
	 */
	@Test
	public void testGetFileNameWithoutSuffix2() {
		String actualFileName = PackagingToolkitFileUtils.getFileNameWithoutSuffix(EMPTY_FILE);
		assertThat(actualFileName, is(equalTo("emptyFile")));
	}
	
	@Test
	public void testConstructRelativeFilePath() {
		StringList pathComponents = PackagingToolkitModelFactory.getStringList();
		pathComponents.addStringToList("aaa");
		pathComponents.addStringToList("bbb");
		pathComponents.addStringToList("ccc");
		
		String path = PackagingToolkitFileUtils.constructFilePath(pathComponents, false);
		String expectedPath = ""; 
		 
		if(SystemUtils.isWindowsSystem()) {
			expectedPath = "aaa\\bbb\\ccc\\";
		} else {
			expectedPath = "aaa/bbb/ccc/";
		}
		
		assertThat(path, is(equalTo(expectedPath)));
	}
	
	@Test
	public void testConstructAbsoluteFilePath() {
		StringList pathComponents = PackagingToolkitModelFactory.getStringList();
		pathComponents.addStringToList("aaa");
		pathComponents.addStringToList("bbb");
		pathComponents.addStringToList("ccc");
		
		if(!SystemUtils.isWindowsSystem()) {
			String path = PackagingToolkitFileUtils.constructFilePath(pathComponents, true);
			String expectedPath = ""; 
			expectedPath = "/aaa/bbb/ccc/";
			assertThat(path, is(equalTo(expectedPath)));
		} else {
			try {
				PackagingToolkitFileUtils.constructFilePath(pathComponents, true);
				fail();
			} catch (SystemException ex) {
				
			}
		}
		
	}
	
	/**
	 * Tests the check if a directory is empty with a directory that is not
	 * empty. 
	 */
	@Test
	public void testIsDirectoryEmptyFalse() {
		File directory = new File(NOT_EMPTY_DIRECTORY);
		Boolean isDirectoryEmpty = PackagingToolkitFileUtils.isDirectoryEmpty(directory);
		assertFalse(isDirectoryEmpty);
	}
	
	/**
	 * Tests the check if a directory is empty with a file that is not a directory.
	 * The method that is tested has to have the returning value NULL. 
	 */
	@Test
	public void testIsDirectoryEmptyNotAFile() {
		File directory = new File(EMPTY_FILE);
		Boolean isDirectoryEmpty = PackagingToolkitFileUtils.isDirectoryEmpty(directory);
		assertNull(isDirectoryEmpty);
	}
	
	/**
	 * Tests the check if a directory is empty with a directory that is empty. 
	 */
	@Test
	public void testIsDirectoryEmptyTrue() {
		File directory = new File(EMPTY_DIRECTORY);
		Boolean isDirectoryEmpty = PackagingToolkitFileUtils.isDirectoryEmpty(directory);
		assertTrue(isDirectoryEmpty);
	}

	/**
	 * Tests the check if a directory is not empty with a directory that is not
	 * empty. 
	 */
	@Test
	public void testIsDirectoryNotEmptyTrue() {
		File directory = new File(NOT_EMPTY_DIRECTORY);
		Boolean isDirectoryNotEmpty = PackagingToolkitFileUtils.isDirectoryNotEmpty(directory);
		assertTrue(isDirectoryNotEmpty);
	}
	
	/**
	 * Tests the check if a directory is not empty with a file that is not a directory.
	 * The method that is tested has to have the returning value NULL. 
	 */
	@Test
	public void testIsDirectoryNotEmptyNotAFile() {
		File directory = new File(EMPTY_FILE);
		Boolean isDirectoryNotEmpty = PackagingToolkitFileUtils.isDirectoryNotEmpty(directory);
		assertNull(isDirectoryNotEmpty);
	}
	
	/**
	 * Tests the check if a directory is empty with a directory that is 
	 * empty. 
	 */
	@Test
	public void testIsNotDirectoryEmptyTrue() {
		File directory = new File(NOT_EMPTY_DIRECTORY);
		Boolean isDirectoryNotEmpty = PackagingToolkitFileUtils.isDirectoryNotEmpty(directory);
		assertTrue(isDirectoryNotEmpty);
	}
	
	@Test
	public void testWriteByteArrayToFile() throws IOException {
		File directory = temporaryFolder.newFolder();
		String filepath = directory.getAbsolutePath() + File.separator + "testfile";
		byte[] dataOfFile = FILE_CONTENT.getBytes();
		PackagingToolkitFileUtils.writeByteArrayToFile(filepath, dataOfFile);
		
		File file = new File(directory, "testfile");
		boolean fileExists = file.exists();
		assertTrue(fileExists);
		
		boolean contentsEqual = FileTestApi.checkFileContent(FILE_CONTENT, file);
		assertTrue(contentsEqual);
	}

	/**
	 * Tests if deleting of one file is working. The file may not exist after
	 * processing of the test.
	 * @throws IOException
	 */
	@Test
	public void testDeleteFilesAndDirectoriesWithOnlyOneFile() throws IOException {
		prepareDirectoriesAndFiles();
		File file = new File(FILE_TO_DELETE_1);
		PackagingToolkitFileUtils.deleteFilesAndDirectories(file);

		boolean file1Exists = file.exists();
		assertFalse(file1Exists);
	}
	
	@Test
	public void testCreateDirectory() {
		File file = new File(NEW_DIRECTORY);
		// The directory may not exist before the test.
		boolean directoryExists = file.exists();
		assertFalse(directoryExists);
		PackagingToolkitFileUtils.createDirectory(NEW_DIRECTORY);
		directoryExists = file.exists();
		assertTrue(directoryExists);
	}
	
	@Test(expected = PackagingToolkitFileException.class)
	public void testCreateDirectoryException() throws IOException {
		prepareDirectoriesAndFiles();
		// The directory has to exist.
		File file = new File(DIRECTORY_TO_DELETE_1);
		boolean directoryExists = file.exists();
		assertTrue(directoryExists);
		PackagingToolkitFileUtils.createDirectory(DIRECTORY_TO_DELETE_1);
	}

	/**
	 * Tests if deleting of a directory with several subdirectories and files 
	 * is working. The directory may not exist after the test.
	 * @throws IOException
	 */
	@Test
	public void testDeleteFilesAndDirectoriesWithDirectory() throws IOException {
		prepareDirectoriesAndFiles();

		File root = new File(DIRECTORY_TO_DELETE_ROOT);
		PackagingToolkitFileUtils.deleteFilesAndDirectories(root);
		boolean rootExists = root.exists();
		assertFalse(rootExists);
	}
	
	@Test
	public void testCreateFileFromInputStream() throws IOException {
		InputStream inputStream = new ByteArrayInputStream(
				FILE_CONTENT.getBytes(StandardCharsets.UTF_8));
		File file = new File(NEW_FILE);
		PackagingToolkitFileUtils.createFileFromInputStream(file, inputStream);
		boolean fileExists = file.exists();
		assertTrue(fileExists);
		
		// Check the content of the file.
		try (FileReader fileReader = new FileReader(file);
			 BufferedReader bufferedReader = new BufferedReader(fileReader);) {
			String content = bufferedReader.readLine();
			assertThat(content, is(equalTo(FILE_CONTENT)));
		}
	}
	
	@Test
	public void testCopyFile() {
		File sourceFile = new File(FILE_TO_DELETE_1);		
		boolean fileExists = sourceFile.exists();
		assertTrue(fileExists);
		File targetFile = new File(NEW_FILE);
		fileExists = targetFile.exists();
		assertFalse(fileExists);
		
		PackagingToolkitFileUtils.copyFile(sourceFile, targetFile);
		fileExists = targetFile.exists();
		assertTrue(fileExists);
	}
	
	// **************** Private methods *****************
	/**
	 * Deletes all directories and files that have been created for the tests.
	 */
	private void deleteDirectoriesAndFiles() {
		File file1 = new File(FILE_TO_DELETE_1);
		file1.delete();
		File file2 = new File(FILE_TO_DELETE_2);
		file2.delete();
		File file3 = new File(FILE_TO_DELETE_3);
		file3.delete();
		File file4 = new File(FILE_TO_DELETE_4);
		file4.delete();
		File file5 = new File(FILE_TO_DELETE_5);
		file5.delete();
		File file6 = new File(DIRECTORY_TO_DELETE_2);
		file6.delete();
		File file7 = new File(DIRECTORY_TO_DELETE_3);
		file7.delete();
		File file8 = new File(DIRECTORY_TO_DELETE_1);
		file8.delete();
		File file9 = new File(DIRECTORY_TO_DELETE_ROOT);
		file9.delete();
		File emptyDirectory = new File(EMPTY_DIRECTORY);
		emptyDirectory.delete();
		File zipFile = new File(EMPTY_FILE);
		zipFile.delete();
		File notEmptyDirectory = new File(NOT_EMPTY_DIRECTORY);
		notEmptyDirectory.delete();
		File newDirectory = new File(NEW_DIRECTORY);
		newDirectory.delete();
		File newFile = new File(NEW_FILE);
		newFile.delete();
	}
	
	/**
	 * Creates the directories and files necessary for the tests.
	 * @throws IOException
	 */
	private void prepareDirectoriesAndFiles()  throws IOException {
		File deleteDirectory3 = new File(DIRECTORY_TO_DELETE_3);
		deleteDirectory3.mkdirs();
		File deleteDirectory2 = new File(DIRECTORY_TO_DELETE_2);
		deleteDirectory2.mkdirs();
		File file1 = new File(FILE_TO_DELETE_1);
		file1.createNewFile();
		File file2 = new File(FILE_TO_DELETE_2);
		file2.createNewFile();
		File file3 = new File(FILE_TO_DELETE_3);
		file3.createNewFile();
		File file4 = new File(FILE_TO_DELETE_4);
		file4.createNewFile();
		File file5 = new File(FILE_TO_DELETE_5);
		file5.createNewFile();
		File emptyDirectory = new File(EMPTY_DIRECTORY);
		emptyDirectory.mkdir();
		File notEmptyDirectory = new File(NOT_EMPTY_DIRECTORY);
		notEmptyDirectory.mkdir();
		File zipFile = new File(EMPTY_FILE);
		zipFile.createNewFile();
	}
	
	@Test
	public void testDetermineDirectoryAboveThisDirectoryPath() {
		String testPathAsString = "aaa" + File.separator + "ccc" + File.separator + "ddd" + File.separator + "eee";
		String expectedPath = "aaa" + File.separator + "bbb";
		File expectedFile = new File(expectedPath);
		
		Path testPath = Paths.get(testPathAsString);
		File actualFile = PackagingToolkitFileUtils.determineDirectoryAboveThisDirectoryPath(3, testPath, "bbb");
		
		assertThat(actualFile.getAbsolutePath(), is(equalTo(expectedFile.getAbsolutePath())));
	}
	
	// *********************************************************
	// testMoveFileReplaceExisting_fileDoesNotExist_ResultTrue()
	// *********************************************************
	/**
	 * Tests the method PackagingToolkitFileUtils.moveFileReplaceExisting() with a file that is moved to a location that
	 * does not contain a file with the same name. Because there are no problems, the return value of the method under
	 * test is true.
	 * <br />
	 * The test checks the following conditions:
	 * <ul>
	 * 	<li>The return value of the method under test is true.</li>
	 * 	<li>The original file does not exist any more.</li>
	 * 	<li>The target file of the operation exists.</li>
	 * </ul>
	 * @throws IOException
	 */
	@Test
	public void testMoveFileReplaceExisting_fileDoesNotExist_ResultTrue() throws IOException {
		File testFile = temporaryFolder.newFile("test.txt");
		File destDirectory = temporaryFolder.newFolder("testfolder");
		Path sourcePath = Paths.get(testFile.getAbsolutePath());
		Path targetPath = Paths.get(destDirectory.getAbsolutePath(), "test.txt");
		
		boolean successful = PackagingToolkitFileUtils.moveFileReplaceExisting(sourcePath, targetPath);
		boolean movedFileExists = testFile.exists();
		assertFalse(movedFileExists);
		File expectedFile = new File(destDirectory, testFile.getName());
		boolean expectedFileExists = expectedFile.exists();
		assertTrue(expectedFileExists);
		assertTrue(successful);
	}
	
	// *********************************************************
	// testMoveFileReplaceExisting_fileExists_ResultTrue()
	// *********************************************************
	/**
	 * Tests the method PackagingToolkitFileUtils.moveFileReplaceExisting() with a file that is moved to a location that
	 * contains a file with the same name. Because there are no problems, the return value of the method under
	 * test is true.
	 * <br />
	 * The test checks the following conditions:
	 * <ul>
	 * 	<li>The return value of the method under test is true.</li>
	 * 	<li>The original file does not exist any more.</li>
	 * 	<li>The target file of the operation exists.</li>
	 * </ul>
	 * @throws IOException
	 */
	@Test
	public void testMoveFileReplaceExisting_fileExists_ResultTrue() throws IOException {
		File testFile = temporaryFolder.newFile("test.txt");
		File destDirectory = temporaryFolder.newFolder("testfolder");
		Path sourcePath = Paths.get(testFile.getAbsolutePath());
		Path targetPath = Paths.get(destDirectory.getAbsolutePath(), "test.txt");
		targetPath.toFile().createNewFile(); 
		
		boolean successful = PackagingToolkitFileUtils.moveFileReplaceExisting(sourcePath, targetPath);
		boolean movedFileExists = testFile.exists();
		assertFalse(movedFileExists);
		File expectedFile = new File(destDirectory, testFile.getName());
		boolean expectedFileExists = expectedFile.exists();
		assertTrue(expectedFileExists);
		assertTrue(successful);
	}
	
	// *********************************************************
	// testMoveFileReplaceExisting_fileDoesNotExist_ResultFalse()
	// *********************************************************
	/**
	 * Tests the method PackagingToolkitFileUtils.moveFileReplaceExisting(). Because the target path does not exist,
	 * the return value of the method under test is false.
	 * <br />
	 * The test checks the following conditions:
	 * <ul>
	 * 	<li>The return value of the method under test is false.</li>
	 * 	<li>The original file exists.</li>
	 * </ul>
	 * @throws IOException
	 */
	@Test
	public void testMoveFileReplaceExisting_fileDoesNotExist_ResultFalse() throws IOException {
		File testFile = temporaryFolder.newFile("test.txt");
		File destDirectory = new File(testFile.getPath(), "wrongfolder");
		Path sourcePath = Paths.get(testFile.getAbsolutePath());
		Path targetPath = Paths.get(destDirectory.getAbsolutePath(), "test.txt");
		
		boolean successful = PackagingToolkitFileUtils.moveFileReplaceExisting(sourcePath, targetPath);
		boolean movedFileExists = testFile.exists();
		assertTrue(movedFileExists);
		assertFalse(successful);
	}
	
	// *********************************************************
	// testRemoveFileExtension_filehasAnExtensionWithThreeLetters()
	// *********************************************************
	@Test 
	public void testRemoveFileExtension_filehasAnExtensionWithThreeLetters() {
		String filenameWithExtension = "file.zip";
		String expectedFilename = "file";
		
		String actualFilename = PackagingToolkitFileUtils.removeFileExtension(filenameWithExtension);
		
		assertThat(actualFilename, is(equalTo(expectedFilename)));
	}
	
	// *********************************************************
	// testRemoveFileExtension_filehasAnExtensionWithOneLetter()
	// *********************************************************
	@Test 
	public void testRemoveFileExtension_filehasAnExtensionWithOneLetter() {
		String filenameWithExtension = "file.c";
		String expectedFilename = "file";
		
		String actualFilename = PackagingToolkitFileUtils.removeFileExtension(filenameWithExtension);
		
		assertThat(actualFilename, is(equalTo(expectedFilename)));
	}
	
	// *********************************************************
	// testRemoveFileExtension_filehasAnExtensionWithNoExtension()
	// *********************************************************
	@Test 
	public void testRemoveFileExtension_filehasAnExtensionWithNoExtension() {
		String expectedFilename = "file";
		
		String actualFilename = PackagingToolkitFileUtils.removeFileExtension(expectedFilename);
		
		assertThat(actualFilename, is(equalTo(expectedFilename)));
	}	
	
	// *********************************************************
	// testDeleteOldFileIfChanged_fileHasChanged()
	// *********************************************************
	@Test
	public void testDeleteOldFileIfChanged_fileHasChanged() throws IOException {
		final String oldFileName = "oldFile";
		final String newFileName = "newFile";
		final File directory = temporaryFolder.newFolder();
		final File fileHasNotToBeDeleted = new File(directory, newFileName);
		final File fileHasToBeDeleted = new File(directory, oldFileName);
		fileHasNotToBeDeleted.createNewFile();
		fileHasToBeDeleted.createNewFile();
		
		PackagingToolkitFileUtils.deleteOldFileIfChanged(oldFileName, newFileName, directory);
		assertFalse(fileHasToBeDeleted.exists());
		assertTrue(fileHasNotToBeDeleted.exists());
	}
	
	// *********************************************************
	// testDeleteOldFileIfChanged_fileHasNotChanged()
	// *********************************************************
	@Test
	public void testDeleteOldFileIfChanged_fileHasNotChanged() throws IOException {
		final String fileName = "file";
		final File directory = temporaryFolder.newFolder();
		final File fileHasNotToBeDeleted = new File(directory, fileName);
		
		fileHasNotToBeDeleted.createNewFile();
		
		PackagingToolkitFileUtils.deleteOldFileIfChanged(fileName,fileName, directory);
		assertTrue(fileHasNotToBeDeleted.exists());
	}
}