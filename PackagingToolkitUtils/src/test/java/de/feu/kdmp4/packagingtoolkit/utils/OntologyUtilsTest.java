package de.feu.kdmp4.packagingtoolkit.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Test;

public class OntologyUtilsTest {
	@Test
	public void testIsValidUuid_withValidUuid() {
		final UUID uuid = UUID.randomUUID();
		final boolean isValidUuid = OntologyUtils.isValidUuid(uuid.toString());
		assertTrue(isValidUuid);
	}
	
	@Test
	public void testIsValidUuid_withInvalidUuid() {
		final UUID uuid = UUID.randomUUID();
		final String invalidUuid = "xxx" + uuid.toString() + "xxx";
		final boolean isValidUuid = OntologyUtils.isValidUuid(invalidUuid);
		assertFalse(isValidUuid);
	}
}
