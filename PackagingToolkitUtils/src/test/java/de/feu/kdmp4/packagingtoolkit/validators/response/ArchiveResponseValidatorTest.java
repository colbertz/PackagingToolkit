package de.feu.kdmp4.packagingtoolkit.validators.response;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.validators.ListValidator;

public class ArchiveResponseValidatorTest {
	
	@Before
	public void setUp() {
	}
	
	@Test
	public void testIsArchiveIdValid1() {
		boolean isValid = ArchiveResponseValidator.isArchiveIdValid(null);
		assertFalse(isValid);		
	}
	
	@Test
	public void testIsArchiveIdValid2() {
		boolean isValid = ArchiveResponseValidator.isArchiveIdValid("   ");
		assertFalse(isValid);		
	}
	
	@Test
	public void testIsArchiveIdValid3() {
		boolean isValid = ArchiveResponseValidator.isArchiveIdValid("1234567890");
		assertTrue(isValid);		
	}

	@Test
	public void testIsEmpty1() {
	}

	@Test
	public void testIsEmpty2() {
	}
	
	@Test
	public void testIsNotNull1() {
	}

	@Test
	public void testIsNotNull2() {
	}
	
	@Test
	public void testIsNotNullOrEmpty1() {
	}

	@Test
	public void testIsNotNullOrEmpty2() {
	}
	
	@Test
	public void testIsNotNullOrEmpty3() {
	}
	
	@Test
	public void testIsIndexInRange1() {
	}
	
	@Test
	public void testIsIndexInRange2() {
	}
	
	@Test
	public void testIsIndexInRange3() {
	}
}
