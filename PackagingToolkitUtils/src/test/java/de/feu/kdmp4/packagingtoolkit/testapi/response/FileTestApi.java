package de.feu.kdmp4.packagingtoolkit.testapi.response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class FileTestApi {
	public static boolean checkFileContent(String expectedDataOfFile, File file) throws FileNotFoundException, IOException {
		try (FileReader fileReader = new FileReader(file);
			 BufferedReader bufferedReader = new BufferedReader(fileReader);) {
			String content = bufferedReader.readLine();
			if (StringUtils.areStringsEqual(content, expectedDataOfFile)) {
				return true; 
			} else {
				return false;
			}
		} 
	}
}
