package de.feu.kdmp4.packagingtoolkit.validators;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringValidatorTest {

	@Test
	public void testIsNotEmptyWithEmptyString() {
		boolean isNotEmpty = StringValidator.isNotEmpty("  ");
		assertFalse(isNotEmpty);
	}

	@Test
	public void testIsNotEmptyWithNull() {
		boolean isNotEmpty = StringValidator.isNotEmpty(null);
		assertFalse(isNotEmpty);
	}
	
	@Test
	public void testIsNotEmpty() {
		boolean isNotEmpty = StringValidator.isNotEmpty("ABC");
		assertTrue(isNotEmpty);
	}
	
	@Test
	public void testIsEmptyWithEmptyString() {
		boolean isEmpty = StringValidator.isEmpty("  ");
		assertTrue(isEmpty);
	}

	@Test
	public void testIsEmptyWithNull() {
		boolean isEmpty = StringValidator.isEmpty(null);
		assertTrue(isEmpty);
	}
	
	@Test
	public void testIsEmpty() {
		boolean isNotEmpty = StringValidator.isEmpty("ABC");
		assertFalse(isNotEmpty);
	}
	
	@Test
	public void testIsNotNullFalse() {
		boolean isNotNull = StringValidator.isNotEmpty(null);
		assertFalse(isNotNull);
	}
	
	@Test
	public void testIsNotNullTrue() {
		boolean isNotNull = StringValidator.isNotEmpty("ABC");
		assertTrue(isNotNull);
	}
	
	@Test
	public void testIsNotNullOrEmptyWithNull() {
		boolean isNotNullOrEmpty = StringValidator.isNotNullOrEmpty(null);
		assertFalse(isNotNullOrEmpty);
	}
	
	@Test
	public void testIsNotNullOrEmptyWithEmptyString() {
		boolean isNotNullOrEmpty = StringValidator.isNotNullOrEmpty("   ");
		assertFalse(isNotNullOrEmpty);
	}
	
	@Test
	public void testIsNotNullOrEmptyTrue() {
		boolean isNotNullOrEmpty = StringValidator.isNotEmpty("ABC");
		assertTrue(isNotNullOrEmpty);
	}
	
	@Test
	public void testIsNullOrEmptyWithNull() {
		boolean isNullOrEmpty = StringValidator.isNullOrEmpty(null);
		assertTrue(isNullOrEmpty);
	}
	
	@Test
	public void testIsNullOrEmptyWithEmptyString() {
		boolean isNullOrEmpty = StringValidator.isNullOrEmpty("   ");
		assertTrue(isNullOrEmpty);
	}
	
	@Test
	public void testIsNullOrEmptyTrue() {
		boolean isNullOrEmpty = StringValidator.isNullOrEmpty("ABC");
		assertFalse(isNullOrEmpty);
	}
}
