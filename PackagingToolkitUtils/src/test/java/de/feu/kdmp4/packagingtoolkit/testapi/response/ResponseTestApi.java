package de.feu.kdmp4.packagingtoolkit.testapi.response;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;

public class ResponseTestApi {
	public static ArchiveResponse prepareArchivesAndReturnOneArchive(ArchiveListResponse archiveListResponse) {
		Uuid uuid1 = new Uuid();
		ArchiveResponse archive1 = new ArchiveResponse();
		archive1.setUuid(uuid1.toString());
		archiveListResponse.addArchiveToList(archive1);
		
		Uuid uuid2 = new Uuid();
		ArchiveResponse archive2 = new ArchiveResponse();
		archive2.setUuid(uuid2.toString());
		archiveListResponse.addArchiveToList(archive2);
		
		Uuid uuid3 = new Uuid();
		ArchiveResponse archive3 = new ArchiveResponse();
		archive3.setUuid(uuid3.toString());
		archiveListResponse.addArchiveToList(archive3);
		
		return archive2;
	}
}
