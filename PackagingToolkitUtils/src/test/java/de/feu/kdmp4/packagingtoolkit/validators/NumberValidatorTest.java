package de.feu.kdmp4.packagingtoolkit.validators;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumberValidatorTest {

	@Test
	public void testIsNegative1() {
		int value = 5;
		boolean isNegative = NumberValidator.isNegative(value);
		assertFalse(isNegative);
	}

	@Test
	public void testIsNegative2() {
		int value = -5;
		boolean isNegative = NumberValidator.isNegative(value);
		assertTrue(isNegative);
	}
	
	@Test
	public void testIsNegative3() {
		int value = 0;
		boolean isNegative = NumberValidator.isNegative(value);
		assertFalse(isNegative);
	}
}
