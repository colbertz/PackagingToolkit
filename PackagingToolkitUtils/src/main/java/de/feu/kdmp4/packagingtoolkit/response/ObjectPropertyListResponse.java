package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with object properties for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
//@XmlAccessorType(XmlAccessType.PROPERTY)
public class ObjectPropertyListResponse implements Serializable { 
	private static final long serialVersionUID = 9152272928881619738L;
	/**
	 * A list with the properties.
	 */
	private List<ObjectPropertyResponse> objectPropertyList;
	/**
	 * Contains the labels of this object property written in different languages.
	 */
	private TextInLanguageListResponse labels;
	
	//**************** Constructors *********************
	/**
	 * The default constructor is important for frameworks like JAXB. It
	 * initializes the list. 
	 */
	public ObjectPropertyListResponse() {
		objectPropertyList = new ArrayList<>();
	}
	
	//**************** Public methods *******************
	/**
	 * Adds a property to the list. 
	 * @param objectProperty The property that should be added to the list.
	 */
	public void addObjectPropertyToList(final ObjectPropertyResponse objectProperty) {
		objectPropertyList.add(objectProperty);
	}
	
	/**
	 * Gets a property at a given index.
	 * @param index The index of the property that should be determined.
	 * @return The found property.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public ObjectPropertyResponse getObjectPropertyAt(final int index) {
		return objectPropertyList.get(index);
	}
	
	/**
	 * Counts the object properties in the list.
	 * @return The number of the object properties in the list.
	 */
	public int getObjectPropertyCount() {
		return objectPropertyList.size();
	}

	// ************* Getters and Setters **************
	@XmlElement
	public List<ObjectPropertyResponse> getObjectPropertyList() {
		return objectPropertyList;
	}

	public void setObjectPropertyList(List<ObjectPropertyResponse> objectPropertyList) {
		this.objectPropertyList = objectPropertyList;
	}
	
	@XmlElement
	public TextInLanguageListResponse getLabels() {
		return labels;
	}
	
	public void setLabels(TextInLanguageListResponse labels) {
		this.labels = labels;
	}
}
