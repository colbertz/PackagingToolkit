package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
/**
 * A list with the tools of a data source for sending via http.
 * @author Christopher Olbertz
 *
 */
public class DataSourceToolListResponse {
	/**
	 * The data source tools that are contained in this list.
	 */
	private List<DataSourceToolResponse> dataSourceTools;
	
	public DataSourceToolListResponse() {
		dataSourceTools = new ArrayList<>();
	}

	public List<DataSourceToolResponse> getDataSourceTools() {
		return dataSourceTools;
	}

	public void setDataSourceTools(List<DataSourceToolResponse> dataSourceTools) {
		this.dataSourceTools = dataSourceTools;
	}
	
	public void addDataSourceTool(DataSourceToolResponse dataSourceTool) {
		dataSourceTools.add(dataSourceTool);
	}
	
	public int getDataSourceToolsCount() {
		return dataSourceTools.size();
	}
	
	public DataSourceToolResponse getDataSourceTool(int index) {
		return dataSourceTools.get(index);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataSourceTools == null) ? 0 : dataSourceTools.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSourceToolListResponse other = (DataSourceToolListResponse) obj;
		if (dataSourceTools == null) {
			if (other.dataSourceTools != null)
				return false;
		} else if (!dataSourceTools.equals(other.dataSourceTools))
			return false;
		return true;
	}
}
