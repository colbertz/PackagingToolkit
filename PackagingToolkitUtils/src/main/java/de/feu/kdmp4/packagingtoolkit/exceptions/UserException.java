package de.feu.kdmp4.packagingtoolkit.exceptions;

/**
 * The super class of all exception that should be shown to the user. 
 * @author Christopher Olbertz
 *
 */
public abstract class UserException extends RuntimeException {
	// *************** Constants **************
	private static final long serialVersionUID = 2579108274987979432L;

	// ************** Constructors ************
	public UserException(String message) {
		super(message);
	}
	
	// ************** Abstract methods ********
	/**
	 * Returns a summary of the exception method. This is a short form 
	 * of the detailled message and is something like a title for this
	 * exception.
	 * @return The summary.
	 */
	public abstract String getSummary();
}
