package de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * A general exception for the information packages. 
 * @author Christopher Olbertz
 *
 */
public class InformationPackageException extends UserException {
	private static final String SUMMARY = I18nExceptionUtil.getTitleInvalidMayNotBeEmptyString();
	
	private InformationPackageException(String message) {
		super(message);		
	}

	public static InformationPackageException titleMayNotBeEmptyException() {
		String message = I18nExceptionUtil.getTitleInvalidMayNotBeEmptyString();
		return new InformationPackageException(message);
	}
	
	@Override
	public String getSummary() {
		return SUMMARY;
	}
}
