package de.feu.kdmp4.packagingtoolkit.enums;

import javax.xml.bind.annotation.XmlEnum;

/**
 * An enum for the mathematical signs: plus and minus sign. There is a
 * third possibility and that is reason why this enum is needed: the sign
 * is not important and the whole range has to be considered.
 * @author Christopher Olbertz
 *
 */
@XmlEnum
public enum Sign {
	PLUS_SIGN, MINUS_SIGN, BOTH;
}
