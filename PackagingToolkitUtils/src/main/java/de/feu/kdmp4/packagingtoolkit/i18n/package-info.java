/**
 * Contains all classes for internationalization.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.i18n;