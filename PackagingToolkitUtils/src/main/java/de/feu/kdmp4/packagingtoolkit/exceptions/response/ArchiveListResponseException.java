package de.feu.kdmp4.packagingtoolkit.exceptions.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;

/**
 * An exception that is thrown if there were any problems with an archive list response.
 * @author Christopher Olbertz
 *
 */
public class ArchiveListResponseException extends ProgrammingException {
	// *************** Constants ****************
	private static final long serialVersionUID = 5190194735669260905L;

	// *************** Constructors **************
	/**
	 * Constructs an exception.
	 * @param message The message.
	 */
	private ArchiveListResponseException(String message) {
		super(message);
	}

	// ************** Public methods *************
	/**
	 * Creates an exception that is thrown if an archive is null.
	 * @return The exception.
	 */
	public static final ArchiveListResponseException archiveMayNotBeNull() {
		String message = I18nExceptionUtil.getArchiveMayNotBeNullString();
		return new ArchiveListResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the uuid is already in the archive.
	 * @return The exception.
	 */
	public static final ArchiveListResponseException uuidAlreadyInArchive(final UuidResponse uuid) {
		String message = I18nExceptionUtil.getUuidAlreadyInArchiveString();
		message = String.format(message, uuid.toString());
		return new ArchiveListResponseException(message);
	}
}
