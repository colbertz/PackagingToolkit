package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * This class encapsulates a property in an ontology for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class DatatypePropertyResponse implements Serializable {
	private static final long serialVersionUID = -5202456395817505551L;
	/**
	 * The data type of this datatype property.
	 */
	private OntologyDatatypeResponse datatype;
	/**
	 * True if the value of this property can contain zero. False if the value cannot contain zero or if the
	 * datatype itself does not allow any numeric values.
	 */
	private boolean inclusiveZero;
	/**
	 * The label of the property that has been read from the ontology.
	 */
	private String label;
	/**
	 * The iri of the property as defined in the ontology.
	 */
	private String propertyName;
	/**
	 * Contains the information if the value is in the positive ar negative numbers.
	 */
	private Sign sign;
	/**
	 * True if the value of this property is unsigend. False is the value is sigend or if the
	 * datatype itself does not allow any numeric values.
	 */
	private boolean unsigned;
	/**
	 * The value of this property.
	 */
	private String value;
	/**
	 * Contains a list with labels. Every label is written in a certain language.
	 */
	private TextInLanguageListResponse labelTexts;
	
	// ****************** Constructors ***********
	/**
	 * The default constructor may only be used by frameworks
	 * like JAXB.
	 */
	public DatatypePropertyResponse() {
		labelTexts = new TextInLanguageListResponse();
	}
	
	/**
	 * Creates a new datatype property.
	 * @param propertyName The iri of the property as defined in the ontology.
	 * @param value The value that is contained in the property.
	 * @param datatype The data type of this datatype property.
	 * @param label The default label of the property as defined in the ontology.
	 * @param sign Contains the information if the value is in the positive ar negative numbers.
	 * @param inclusiveZero True if the value of this property can contain zero. False if the value cannot contain zero or if the
	 * datatype itself does not allow any numeric values.
	 * @param unsigned True if the value of this property is unsigend. False is the value is sigend or if the
	 * datatype itself does not allow any numeric values.
	 */
	public DatatypePropertyResponse(final String propertyName, final String value, 
			final OntologyDatatypeResponse datatype, String label, final Sign sign, final boolean inclusiveZero,
			boolean unsigned) {
		this.propertyName = propertyName;
		this.value = value;
		this.datatype = datatype;
		
		if (StringValidator.isNullOrEmpty(label)) {
			label = propertyName;
		} else {
			this.label = label;
		}
		this.sign = sign;
		this.inclusiveZero = inclusiveZero;
		this.unsigned = unsigned;
		labelTexts = new TextInLanguageListResponse();
	}
	
	// **************** Public methods *************
	/**
	 * Converts the value of this datatype property to Boolean. 
	 * @return The value as Boolean or null if the value cannot be converted.
	 */
	public Boolean getValueAsBoolean() {
		if (datatype.equals(OntologyDatatypeResponse.BOOLEAN) && value != null) {
			return Boolean.valueOf(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to Byte. 
	 * @return The value as Byte or null if the value cannot be converted.
	 */
	public Short getValueAsByte() {
		if (datatype.equals(OntologyDatatypeResponse.BYTE) && value != null) {
			return Short.valueOf(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to Date. 
	 * @return The value as Date or null if the value cannot be converted.
	 */
	public LocalDate getValueAsDate() {
		if (datatype.equals(OntologyDatatypeResponse.DATE) && value != null) {
			// The value contains the epochDays.
			long epochDays = Long.parseLong(value);
			return LocalDate.ofEpochDay(epochDays);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to DateTime. 
	 * @return The value as DateTime or null if the value cannot be converted.
	 */
	public LocalDateTime getValueAsDateTime() {
		if (datatype.equals(OntologyDatatypeResponse.DATETIME) && value != null) {
			long epochSeconds = Long.parseLong(value);
			LocalDateTime dateTime = LocalDateTime.ofEpochSecond(epochSeconds, 0, ZoneOffset.UTC);
			return dateTime;
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to Double. 
	 * @return The value as Double or null if the value cannot be converted.
	 */
	public Double getValueAsDouble() {
		if (datatype.equals(OntologyDatatypeResponse.DOUBLE) && value != null) {
			return Double.valueOf(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to OntologyDuration. 
	 * @return The value as OntologyDuration or null if the value cannot be converted.
	 */
	public OntologyDuration getValueAsDuration() {
		if (datatype.equals(OntologyDatatypeResponse.DURATION) && value != null) {
			return OntologyDuration.fromString(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to Float. 
	 * @return The value as Flaot or null if the value cannot be converted.
	 */
	public Float getValueAsFloat() {
		if (datatype.equals(OntologyDatatypeResponse.FLOAT) && value != null) {
			return Float.valueOf(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to Long. 
	 * @return The value as Long or null if the value cannot be converted.
	 */
	public Long getValueAsInteger() {
		if (datatype.equals(OntologyDatatypeResponse.INTEGER) && value != null) {
			return Long.valueOf(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to BigInteger. 
	 * @return The value as BigInteger or null if the value cannot be converted.
	 */
	public BigInteger getValueAsLong() {
		if (datatype.equals(OntologyDatatypeResponse.LONG) && value != null) {
			return new BigInteger(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to Integer. 
	 * @return The value as Integer or null if the value cannot be converted.
	 */
	public Integer getValueAsShort() {
		if (datatype.equals(OntologyDatatypeResponse.SHORT) && value != null) {
			return Integer.valueOf(value);
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to String. 
	 * @return The value as String or null if the value cannot be converted.
	 */
	public String getValueAsString() {
		if (datatype.equals(OntologyDatatypeResponse.STRING) && value != null) {
			return value;
		}
		
		return null;
	}
	
	/**
	 * Converts the value of this datatype property to LocalTime. 
	 * @return The value as LocalTime or null if the value cannot be converted.
	 */
	public LocalTime getValueAsTime() {
		if (datatype.equals(OntologyDatatypeResponse.TIME) && value != null) {
			long nanoOfDay = Long.parseLong(value);
			LocalTime time = LocalTime.ofNanoOfDay(nanoOfDay);
			return time;
		}
		
		return null;
	}

	// **************** Getters and setters ********
	@XmlElement
	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
		if (StringValidator.isNullOrEmpty(label)) {
			label = propertyName;
		}
	}

	@XmlElement
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlElement
	public OntologyDatatypeResponse getDatatype() {
		return datatype;
	}

	public void setDatatype(OntologyDatatypeResponse datatype) {
		this.datatype = datatype;
	}

	@XmlElement
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@XmlElement
	public Sign getSign() {
		return sign;
	}

	public void setSign(Sign sign) {
		this.sign = sign;
	}

	@XmlElement
	public boolean isInclusiveZero() {
		return inclusiveZero;
	}

	public void setInclusiveZero(boolean inclusiveZero) {
		this.inclusiveZero = inclusiveZero;
	}

	@XmlElement
	public boolean isUnsigned() {
		return unsigned;
	}

	public void setUnsigned(boolean unsigned) {
		this.unsigned = unsigned;
	}	
	
	@XmlElement
	public TextInLanguageListResponse getLabelTexts() {
		return labelTexts;
	}
	
	public void setLabelTexts(TextInLanguageListResponse labelTexts) {
		this.labelTexts = labelTexts;
	}
}
