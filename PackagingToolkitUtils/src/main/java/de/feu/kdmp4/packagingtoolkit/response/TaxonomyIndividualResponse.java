package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaxonomyIndividualResponse implements Serializable {
	private static final long serialVersionUID = 8024999456273601305L;
	private String iri;
	private String preferredLabel;
	private TaxonomyIndividualListResponse narrowers;
	
	public TaxonomyIndividualResponse() {
		narrowers = new TaxonomyIndividualListResponse();
	}
	
	public void addNarrower(final TaxonomyIndividualResponse narrower) {
		narrowers.addTaxonomyIndividual(narrower);
	}
	
	public String getIri() {
		return iri;
	}
	
	public void setIri(String iri) {
		this.iri = iri;
	}
	
	public String getPreferredLabel() {
		return preferredLabel;
	}
	
	public void setPreferredLabel(String preferredLabel) {
		this.preferredLabel = preferredLabel;
	}
	
	@XmlElement
	public TaxonomyIndividualListResponse getNarrowers() {
		return narrowers;
	}
	
	public void setNarrowers(TaxonomyIndividualListResponse narrowers) {
		this.narrowers = narrowers;
	}
}
