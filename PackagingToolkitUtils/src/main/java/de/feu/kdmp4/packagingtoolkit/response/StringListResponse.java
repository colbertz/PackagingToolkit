package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A list that contains strings for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class StringListResponse {
	/**
	 * The strings contained in this list.
	 */
	private List<String> strings;

	/**
	 * Initializes the list as array list.
	 */
	public StringListResponse() {
		strings = new ArrayList<>();
	}
	
	/**
	 * Creates a new object with a list of strings.
	 * @param strings The list that should be contained in this object.
	 */
	public StringListResponse(final List<String> strings) {
		super();
		this.strings = strings;
	}

	/**
	 * Adds a new string to this list.
	 * @param theString
	 */
	public void addString(final String theString) {
		strings.add(theString);
	}
	
	/**
	 * Determines a string at a given position.
	 * @param index The position we want to look at.
	 * @return The string found at this position.
	 */
	public String getStringAtIndex(final int index) {
		return strings.get(index);
	}
	
	/**
	 * Determines the number of strings in the list.
	 * @return The number of strings in the list.
	 */
	public int getStringCount() {
		return strings.size();
	}
	
	/**
	 * Removes a string from the list.
	 * @param aString The string we want to remove.
	 */
	public void removeString(String aString) {
		strings.remove(aString);
	}
	
	// ********* Getters and setters ********
	@XmlElement
	public List<String> getStrings() {
		return strings;
	}

	public void setStrings(List<String> strings) {
		this.strings = strings;
	}
}
