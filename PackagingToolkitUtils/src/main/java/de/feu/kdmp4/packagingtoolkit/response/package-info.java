@XmlSchema( 
    namespace = "http://kdmp4.feu.de", 
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED) 
package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlSchema;
