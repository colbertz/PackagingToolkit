package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains some data that are mapped to XML for sending via http. This class contains only a uuid and an iri. 
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class UuidAndIriResponse {
	/**
	 * An uuid that identifies an object with this iri.
	 */
	private String uuid;
	/**
	 * An iri that identifies an object with this uuid.
	 */
	private String iri;
	
	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	public String getIri() {
		return iri;
	}
	
	public void setIri(String iri) {
		this.iri = iri;
	}
}
