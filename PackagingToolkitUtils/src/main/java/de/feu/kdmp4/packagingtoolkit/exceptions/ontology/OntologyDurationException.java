package de.feu.kdmp4.packagingtoolkit.exceptions.ontology;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class OntologyDurationException extends UserException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 452713334136154885L;

	private OntologyDurationException(String message) {
		super(message);
	}

	public static final OntologyDurationException daysMayNotBeNegative(int days) {
		String message = I18nExceptionUtil.getDaysInvalidString();
		message = String.format(message, days);
		return new OntologyDurationException(message);
	}

	public static final OntologyDurationException hoursMayNotBeNegative(int hours) {
		String message = I18nExceptionUtil.getHoursInvalidString();
		message = String.format(message, hours);
		return new OntologyDurationException(message);
	}
	
	public static final OntologyDurationException minutesMayNotBeNegative(int minutes) {
		String message = I18nExceptionUtil.getMinutesInvalidString();
		message = String.format(message, minutes);
		return new OntologyDurationException(message);
	}
	
	public static final OntologyDurationException monthsMayNotBeNegative(int months) {
		String message = I18nExceptionUtil.getMonthsInvalidString();
		message = String.format(message, months);
		return new OntologyDurationException(message);
	}
	
	public static final OntologyDurationException secondsMayNotBeNegative(int seconds) {
		String message = I18nExceptionUtil.getSecondsInvalidString();
		message = String.format(message, seconds);
		return new OntologyDurationException(message);
	}
	
	public static final OntologyDurationException yearsMayNotBeNegative(int years) {
		String message = I18nExceptionUtil.getYearsInvalidString();
		message = String.format(message, years);
		return new OntologyDurationException(message);
	}
	
	@Override
	public String getSummary() {
		return I18nExceptionUtil.getSummaryDurationExceptionString();
	}
}
