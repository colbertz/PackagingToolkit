package de.feu.kdmp4.packagingtoolkit.validators;

import de.feu.kdmp4.packagingtoolkit.exceptions.StringException;

/**
 * This class contains methods for validation of strings. The following checks are
 * implemented:
 * <ul>
 * 	<li>A string is not empty.</li>
 *  <li>A string is empty.</li>
 *  <li>A string is not null.</li>
 *  <li>A string is neither null nor empty.</li>
 *  <li>A string is null or empty.</li>
 * </ul>
 * @author christopher
 *
 */
public class StringValidator {
	/**
	 * Checks if a String is null or empty. A String, that only consists of
	 * spaces, is reported as empty.
	 * @param aString The String that has to be checked.
	 * @throws StringException if the String is null or empty.
	 */
	public static final void checkIsNotNullOrEmpty(String aString) {
		if (isNullOrEmpty(aString)) {
			throw StringException.stringMayNotBeEmptyException();
		}
	}
	
	/**
	 * Checks if a String is not empty. Before the test, the String is trimmed.
	 * Therefore, a String, that only contains of spaces, is reported as empty.
	 * @param aString The String that has to be checked.
	 * @return False if the String is empty or if it only contains spaces.
	 */
	public static boolean isNotEmpty(String aString) {
		if (aString == null) {
			return false;
		}
		return !aString.trim().isEmpty();
	}

	/**
	 * Checks if a String is empty. Before the test, the String is trimmed.
	 * Therefore, a String, that only contains of spaces, is reported as empty.
	 * @param aString The String that has to be checked.
	 * @return True if the String is empty or if it only contains spaces.
	 */
	public static boolean isEmpty(String aString) {
		if (aString == null) {
			return true;
		}
		return aString.trim().isEmpty();
	}
	
	/**
	 * Checks if a String is not null.
	 * @param aString The String that has to be checked.
	 * @return False, if the String is null, true otherwise.
	 */
	public static boolean isNotNull(String aString) {
		return aString != null;
	}
	
	/**
	 * Checks if a String is null or empty. A String, that only consists of
	 * spaces, is reported as empty.
	 * @param aString The String that has to be checked.
	 * @return False, if the String is null or empty, true otherwise.
	 */
	public static boolean isNotNullOrEmpty(String aString) {
		if (isNotNull(aString) && isNotEmpty(aString)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if a String is null or empty. A String, that only consists of
	 * spaces, is reported as empty.
	 * @param aString The String that has to be checked.
	 * @return True, if the String is null or empty, true otherwise.
	 */
	public static boolean isNullOrEmpty(String aString) {
		if (aString == null || isEmpty(aString)) {
			return true;
		} else {
			return false;
		}
	}
}
