package de.feu.kdmp4.packagingtoolkit.validators;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * Performs some file checks. Contains the following checks:
 * <ul>
 * 	<li>Checks, if the file is a zip file.</li>
 * </ul>
 * @author christopher
 *
 */
public class FileValidator {
	/**
	 * Checks if the file is an owl or a rdf file. Uses the file suffix for the check.
	 * @param filename The name of the file to check.
	 * @throws NotAnOntologyFileException if the file is not an ontology file.
	 */
	public static final void checkOntologyFile(final String filename) {
		// TODO Hier kann ruhig eine Exception geworfen werden.
		if (!isOntologyFile(filename)) {
			/*throw new NotAnOntologyFileException(filename, CLASSNAME, 
												 METHOD_CHECK_ONTOLOGY_FILE, 
												 PackagingToolkitComponent.CLIENT);*/
		}
	}
	
	/**
	 * Checks if a file is an owl or a rdf file. Uses the file suffix for the check.
	 * @param filename The name of the file to check.
	 * @return True if the file is an ontology file, false otherwise.
	 */
	public static final boolean isOntologyFile(final String filename) {
		if (!PackagingToolkitFileUtils.isOwlFile(filename) && 
				!PackagingToolkitFileUtils.isRdfFile(filename)) {
			return false;
		} else {
			return true;
		}
	}
	
	
	/**
	 * Checks with the help of the file suffix if the file is a zip file. 
	 * @param file The file to check.
	 * @return True, if the file has the suffix .zip, false otherwise.
	 */
	public static boolean isZipFile(File file) {
		return isZipFile(file.getName());
	}
	
	/**
	 * Checks with the help of the file suffix if the file is a zip file. 
	 * @param file The file to check.
	 * @return True, if the file has the suffix .zip, false otherwise.
	 */
	public static boolean isZipFile(String filename) {
		// TODO Hier kann ruhig eine Exception geworfen werden.
		/*if (filename == null) {
			throw new FilenameMayNotBeEmptyException(CLASSNAME, METHOD_IS_ZIP_FILE, 
													 PackagingToolkitComponent.SERVER);
		}*/
		
		if (filename.endsWith(PackagingToolkitFileUtils.ZIP_FILE_EXTENSION)) {
			return true;
		} else {
			return false;
		}
	}
}