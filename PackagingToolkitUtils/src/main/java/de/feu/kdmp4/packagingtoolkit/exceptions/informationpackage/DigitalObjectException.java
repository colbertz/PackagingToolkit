package de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class DigitalObjectException extends ProgrammingException {
	// ************* Constants *************
	private static final long serialVersionUID = 3514197737642301592L;
	
	// ************* Constructors ***********
	private DigitalObjectException(String message) {
		super(message);
	}

	// ************* Public methods *********
	/**
	 * Creates an exception that is thrown if a digital object is not found in a SIU.
	 * @param uuidOfSIU The uuid of the SIU.
	 * @param uuidOfDigitalObject The uuid of the digital object that has not been found.
	 * @return The exception. 
	 */
	public static DigitalObjectException digitalObjectNotFoundinSIUException(String uuidOfSIU, String uuidOfDigitalObject) {
		String message = I18nExceptionUtil.getDigitalObjectMayNotBeNullString();
		return new DigitalObjectException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the digital object in a SIP
	 * is already set.
	 * @param theClass The class that wants to throw the exception.
	 * @param theMethod The method that wants to throw the exception.
	 * @param digitalObjectName The filename of the digital object that cannot be
	 * added to the SIP.
	 * @param uuid The uuid of the information package.
	 * @param containedDigitalObjectName The filename of the digital object that is
	 * already contained in the SIP.
	 * @return The exception. 
	 */
	public static DigitalObjectException digitalObjectAlreadySetInSIPException(String digitalObjectName, 
			String uuid,
			String containedDigitalObjectName) {
		String message = I18nExceptionUtil.getDigitalObjectAlreadySetString();
		return new DigitalObjectException(message);
	}
	
	
}
