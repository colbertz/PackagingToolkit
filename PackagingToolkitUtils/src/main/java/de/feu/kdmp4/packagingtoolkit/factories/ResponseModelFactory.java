package de.feu.kdmp4.packagingtoolkit.factories;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidAndIriResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

public abstract class ResponseModelFactory {
	public final static ArchiveResponse getArchiveResponse(long archiveId,  
			PackageType packageType, SerializationFormat serializationFormat, 
			String title, UuidResponse uuid, LocalDateTime creationDate, LocalDateTime lastmodificationDate) {
		return new ArchiveResponse(archiveId, packageType, 
				serializationFormat, title, uuid, creationDate, lastmodificationDate);
	}
	
	public static final DigitalObjectListResponse createEmptyDigitalObjectListResponse() {
		return new DigitalObjectListResponse();
	}
	
	public static final IriListResponse createEmptyIriListResponse() {
		return new IriListResponse();
	}
	
	public static final IriResponse createIriResponse(final String namespace, final String localName) {
		return new IriResponse(namespace, localName);
	}
	
	public final static ArchiveListResponse getArchiveListResponse() {
		return new ArchiveListResponse();
	}
	
	public final static DigitalObjectResponse getDigitalObjectResponse(String filename, 
																   UuidResponse informationPackageUuid) {
		return new DigitalObjectResponse(informationPackageUuid, filename);
	}
	
	public static final MediatorDataListResponse createEmptyMediatorDataListResponse() {
		return new MediatorDataListResponse();
	}
	
	public static final MediatorDataResponse createMediatorDataResponse() {
		return new MediatorDataResponse();
	}
	
	public final static UuidAndIriResponse createUuidAndResponse(String iri, String uuid) {
		UuidAndIriResponse uuidAndIriResponse = new UuidAndIriResponse();
		uuidAndIriResponse.setIri(iri);
		uuidAndIriResponse.setUuid(uuid);
		
		return uuidAndIriResponse;
	}
	
	public final static ReferenceListResponse createEmptyReferenceListResponse() {
		return new ReferenceListResponse();
	}
	
	public final static IndividualResponse getIndividualResponse() {
		return new IndividualResponse();
	}
	
	public final static IndividualResponse getIndividualResponse(Uuid uuidOfIndividual, Uuid uuidOfInformationPackage) {
		return new IndividualResponse(uuidOfIndividual, uuidOfInformationPackage);
	}
	
	public final static IndividualListResponse getIndividualListResponse() {
		return new IndividualListResponse();
	}
	
	public final static MetadataElementResponse getMetadataElementResponse(String name, String value) {
		return new MetadataElementResponse(name, value);
	}
	
	public final static MetadataElementListResponse getMetadataElementListResponse() {
		return new MetadataElementListResponse();
	}
	
	public static final ReferenceResponse getReferenceResponse() {
		return new ReferenceResponse();
	}
	
	/*public final static PackageResponse getPackageResponse(PackageType packageType, 
			Uuid uuid, boolean virtual, String title) {
		return new PackageResponse(packageType, uuid, virtual, title);
	}*/
	
	public static final ObjectPropertyResponse getObjectPropertyResponse() {
		return new ObjectPropertyResponse();
	}
	
	public static final ObjectPropertyListResponse getObjectPropertyListResponse() {
		return new ObjectPropertyListResponse();
	}
	
	public final static OntologyClassResponse getOntologyClassResponse(String namespace, 
			String localName) {
		return new OntologyClassResponse(namespace, localName);
	}
	
	public final static OntologyClassListResponse getOntologyClassListResponse() {
		return new OntologyClassListResponse();
	}
	
	public final static DatatypePropertyResponse getOntologyPropertyResponse() {
		return new DatatypePropertyResponse();
	}
	
	public final static DatatypePropertyListResponse getOntologyPropertyListResponse() {
		return new DatatypePropertyListResponse();
	}
	
	public final static ServerConfigurationDataResponse getServerConfigurationData(
			final String configuredBaseOntologyPath, final String configuredRuleOntologyPath, 
			final String workingDirectoryPath, final String baseOntologyClass) {
		return new ServerConfigurationDataResponse(configuredBaseOntologyPath,
				configuredRuleOntologyPath,	workingDirectoryPath, baseOntologyClass);
	}

	/**
	 * Constructs an object that contains the configuration of the server. 
	 * This method does not get a name for the ontology base class. If
	 * the ontology base class is configurable via the gui, this method
	 * can be deleted.
	 * @param configuredBaseOntologyPath The path of the configured base ontology.
	 * @param configuredRuleOntologyPath The path of the configured rule ontology.
	 * @param workingDirectoryPath The working directory of the server.
	 * @return An object that contains the server configuration.
	 */
	public final static ServerConfigurationDataResponse getServerConfigurationData(
			String configuredBaseOntologyPath, String configuredRuleOntologyPath, 
			String workingDirectoryPath, final String premisOntologyPath,
			final String skosOntologyPath) {
		return new ServerConfigurationDataResponse(configuredBaseOntologyPath,
				configuredRuleOntologyPath,	workingDirectoryPath, "");
	}
	
	public final static StringListResponse getStringListResponse() {
		return new StringListResponse();
	}
	
	public static final UuidResponse getUuidResponse(Uuid uuid) {
		String uuidAsString = uuid.toString();
		return new UuidResponse(uuidAsString);
	}
	
	public static final UuidResponse getUuidResponse(String uuid) {
		return new UuidResponse(uuid);
	}
	
	public static final UuidResponse getUuidResponse() {
		return new UuidResponse();
	}
	
	public final static UuidListResponse getUuidListResponse() {
		return new UuidListResponse();
	}
	
	public final static UuidListResponse createUuidListResponse(final UuidList uuids) {
		final UuidListResponse uuidsReponse = new UuidListResponse();
		for (int i = 0; i < uuids.getSize(); i++) {
			final Uuid uuid = uuids.getUuidAt(i);
			final UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(uuid);
			uuidsReponse.addUuidToList(uuidResponse);
		}
		
		return uuidsReponse;
	}
	
	public final static ViewListResponse getViewListResponse() {
		return new ViewListResponse();
	}
	
	public final static ViewResponse getViewResponse(String viewName, int viewId) {
		return new ViewResponse(viewName, viewId);
	}
	
	public final static ViewResponse getViewResponse(String viewName) {
		return new ViewResponse(viewName);
	}
	
	public final static DataSourceToolResponse getDataSourceToolResponse() {
		return new DataSourceToolResponse();
	}
	
	public final static DataSourceToolListResponse getDataSourceToolListResponse() {
		return new DataSourceToolListResponse();
	}
	
	public final static DataSourceResponse getDataSourceResponse() {
		return new DataSourceResponse();
	}
	
	public final static DataSourceListResponse getDataSourceListResponse() {
		return new DataSourceListResponse();
	}
	
	public final static TaxonomyIndividualResponse createTaxonomyIndividualResponse() {
		return new TaxonomyIndividualResponse();
	}
	
	public static final TaxonomyIndividualListResponse createEmptyTaxonomyIndividualListResponse() {
		return new TaxonomyIndividualListResponse();
	}
	
	public static final TaxonomyListResponse createEmptyTaxonomyListResponse() {
		return new TaxonomyListResponse();
	}
	
	public static final TaxonomyResponse createTaxonomyResponse() {
		return new TaxonomyResponse();
	}
}