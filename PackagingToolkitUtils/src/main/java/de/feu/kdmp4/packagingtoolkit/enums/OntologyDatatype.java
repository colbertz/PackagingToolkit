package de.feu.kdmp4.packagingtoolkit.enums;

/**
 * Contains the information about the datatypes in an ontology.
 * @author Christopher Olbertz
 *
 */
public enum OntologyDatatype {
	BOOLEAN("boolean"), BYTE("byte"), 
	DATE("date"), DATETIME("datetime"), DOUBLE("double"), DURATION("duration"), 
	FLOAT("float"), 
	INTEGER("int"), 
	LONG("long"),
	NEGATIVE_INTEGER("negativeinteger"),
	NON_NEGATIVE_INTEGER("nonnegativeinteger"),	NON_POSITIVE_INTEGER("nonpositiveinteger"),	
	OBJECT("object"),
	POSITIVE_INTEGER("positiveinteger"),
	SHORT("short"), STRING("string"),
	TIME("time"),
	UNSIGNED("unsigned"), UNSIGNED_BYTE("unsignedbyte"), UNSIGNED_INT("unsignedint");
	
	/**
	 * The range that is extracted from an ontology and that contains the information
	 * about the datatype.
	 */
	private String range;
	
	private OntologyDatatype(String range) {
		this.range = range;
	}
	
	public String getRange() {
		return range;
	}
	
	public static boolean isBoolean(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(BOOLEAN.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isByte(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(BYTE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isDate(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DATE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isDateTime(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DATETIME.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isDouble(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DOUBLE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isDuration(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(DURATION.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isFloat(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(FLOAT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isInteger(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isLong(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(LONG.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isObjectProperty(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(OBJECT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isShort(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(SHORT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isString(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(STRING.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isTime(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(TIME.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isUnsigned(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		if (lowerPropertyRange.contains(UNSIGNED.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isUnsignedByte(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(UNSIGNED_BYTE.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNegativeInteger(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NEGATIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNonNegativeInteger(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NON_NEGATIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNonPositiveInteger(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(NON_POSITIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isPositiveInteger(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(POSITIVE_INTEGER.getRange())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isUnsignedInt(String propertyRange) {
		String lowerPropertyRange = propertyRange.toLowerCase();
		
		if (lowerPropertyRange.contains(UNSIGNED_INT.getRange())) {
			return true;
		} else {
			return false;
		}
	}
}