/**
 * Contains the exceptions for the mediator.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.exceptions.mediator;