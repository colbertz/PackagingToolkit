package de.feu.kdmp4.packagingtoolkit.validators.response;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.exceptions.response.ArchiveResponseException;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public final class ArchiveResponseValidator {
	/**
	 * Checks if the archive id is not null or empty.
	 * @param archiveId The archive id.
	 * @return True if the archive id is not null or empty and 
	 * false otherwise.
	 */
	public static final boolean isArchiveIdValid(final String archiveId) {
		return StringValidator.isNotNullOrEmpty(archiveId);
	}

	/**
	 * Checks if the package type is not null.
	 * @param packageType The package type that should be checked.
	 * @throws ArchiveResponseException if the package type is null.
	 */
	public static final boolean isPackageTypeValid(final PackageType packageType) {
		return packageType != null;
	}
	
	/**
	 * Checks if a string represents a package type.
	 * @param packageType The package type that should be checked.
	 * @throws ArchiveResponseException if the string is empty or it does
	 * not represent a valid package type.
	 */
	public static final boolean isPackageTypeValid(final String packageTypeAsString) {
		if (StringValidator.isNullOrEmpty(packageTypeAsString)) {
			return false;
		} else {
			return PackageType.isPackageType(packageTypeAsString);
		}
	}

	/**
	 * Checks if a string represents a serialization format.
	 * @param serializationFormatAsString The serialization format that should be checked.
	 * @throws ArchiveResponseException if the string is empty or it does
	 * not represent a valid serialization format.
	 */
	public static final boolean isSerializationFormatValid(final String serializationFormatAsString) {
		if (StringValidator.isNullOrEmpty(serializationFormatAsString)) {
			return false;
		} else {
			return SerializationFormat.isSerializationFormat(serializationFormatAsString);
		}
	}
	
	/**
	 * Checks if the archive title is not null or empty.
	 * @param title The title.
	 * @return True if the title id is not null or empty and 
	 * false otherwise.
	 */
	public static final boolean isTitleValid(final String title) {
		return StringValidator.isNotNullOrEmpty(title);
	}
	
	/**
	 * Checks if the uuid is not null.
	 * @param uuid The uuid.
	 * @return True if the uuid id is not null and  
	 * false otherwise.
	 */
	public static final boolean isUuidValid(final UuidResponse uuid) {
		return uuid != null;
	}
	
	/**
	 * Checks if a string contains a valid uuid.
	 * @param uuid The uuid.
	 * @return True if the string contains a valid uuid and false otherwise.
	 */
	public static final boolean isUuidValid(final String uuid) {
		if (StringValidator.isNullOrEmpty(uuid)) {
			throw ArchiveResponseException.uuidMayNotBeNullException();
		} else {
			try {
				UuidResponse uuidResponse = new UuidResponse(uuid);
			} catch (IllegalArgumentException ex) {
				throw ArchiveResponseException.doesNotContainValidUuidException(uuid);
			}
		}
		return uuid != null;
	}
	
	/**
	 * Checks if the archive id is not null or empty.
	 * @param archiveId The archive id.
	 * @throws ArchiveResponseException if the archive id is null or
	 * empty.
	 */
	public static final void checkArchiveId(final String archiveId) {
		if (!isArchiveIdValid(archiveId)) {
			throw ArchiveResponseException.archiveIdEmptyException();
		}
	}
	
	/**
	 * Checks if the package type is not null.
	 * @param packageType The package type that should be checked.
	 * @throws ArchiveResponseException if the package type is invalid.
	 */
	public static final void checkPackageType(final PackageType packageType) {
		if (!isPackageTypeValid(packageType)) {
			throw ArchiveResponseException.archiveNameInvalidException();
		}
	}
	
	/**
	 * Checks if a string represents a package type.
	 * @param packageType The package type that should be checked.
	 * @throws ArchiveResponseException if the string is empty or it does
	 * not represent a valid package type.
	 */
	public static final void checkPackageType(final String packageTypeAsString) {
		if (!isPackageTypeValid(packageTypeAsString)) {
			throw ArchiveResponseException.packageTypeInvalidException();
		} 
	}
	
	/**
	 * Checks if the string represents a valid serialization format.
	 * @param serializationFormatAsString The serialization format that should be checked.
	 * @throws ArchiveResponseException if the serialization format is null.
	 */
	public static final void checkSerializationFormat(final String serializationFormatAsString) {
		if (!isSerializationFormatValid(serializationFormatAsString)) {
			throw ArchiveResponseException.serializationFormatInvalidException();
		}
	}
	
	/**
	 * Checks if the archive title is not null or empty
	 * @param title The title that should be checked. 
	 * @throws ArchiveResponseException if the title is null or empty.
	 */
	public static final void checkTitle(final String title) {
		if (!isTitleValid(title)) {
			throw ArchiveResponseException.archiveTitleInvalidException();
		}
	}
	
	/**
	 * Checks if the uuid is not null.
	 * @param uuid The uuid that should be checked.
	 * @throws ArchiveResponseException if the uuid is null.
	 */
	public static final void checkUuid(final UuidResponse uuid) {
		if (!isUuidValid(uuid)) {
			throw ArchiveResponseException.uuidMayNotBeNullException();
		}
	}
	
	/**
	 * Checks if a string contains a valid uuid.
	 * @param uuid The uuid that should be checked.
	 * @throws ArchiveResponseException if the uuid is null.
	 */
	public static final void checkUuid(String uuid) {
		if (!isUuidValid(uuid)) {
			throw ArchiveResponseException.uuidMayNotBeNullException();
		}
	}
}
