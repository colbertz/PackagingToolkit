package de.feu.kdmp4.packagingtoolkit.utils;

import java.util.UUID;

/**
 * Contains some helper methods for working with ontologies.
 * <ul>
 * 	<li>Checks if an iri is an iri of Premis.</li>
 * </ul>
 * @author Christopher Olbertz
 *
 */
public class OntologyUtils {
	private static final String PREMIS_NAMESPACE = "http://www.loc.gov/premis/rdf";
	
	/**
	 * Checks if an iri is an iri of Premis. This means that the namespace is checked.
	 * @param iri The iri to check.
	 * @return True, if iri is a Premis iri, false otherwise.
	 */
	public static final boolean isPremisIri(String iri) {
		if (iri.startsWith(PREMIS_NAMESPACE)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if a string contains a valid uuid.
	 * @param uuidAsString The string we want to check if it contains a valid uuid.
	 * @return True if uuidAsString contains a valid uuid, false otherwise.  
	 */
	public static final boolean isValidUuid(final String uuidAsString) {
		try {
			UUID.fromString(uuidAsString);
			return true;
		} catch (IllegalArgumentException exception) {
			return false;
		}
	}
}
