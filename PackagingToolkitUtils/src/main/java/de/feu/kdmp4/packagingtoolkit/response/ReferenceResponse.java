package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains a reference to a digital object contained in an information package. The reference can either point to a file on the
 * local file system of the server or to a file on a remote server.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class ReferenceResponse {
	/**
	 * The url of the digital object if it is saved on a remote server.
	 */
	private String url;
	/**
	 * The uuid of the file in the database if the reference points to a file on the local file system of the server.
	 */
	private UuidResponse uuidOfFile;
	/**
	 * Tge uuid of the information package the reference is contained in.
	 */
	private UuidResponse uuidOfInformationPackage;

	/**
	 *  Checks  if the reference points to a file on the local file system of the server.
	 * @return True if the reference points to a file on the local file system of the server, false otherwise.
	 */
	public boolean isLocalFileReference() {
		return uuidOfFile != null;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public UuidResponse getUuidOfFile() {
		return uuidOfFile;
	}
	
	public void setUuidOfFile(UuidResponse uuidOfFile) {
		this.uuidOfFile = uuidOfFile;
	}

	public UuidResponse getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}

	public void setUuidOfInformationPackage(UuidResponse uuidOfInformationPackage) {
		this.uuidOfInformationPackage = uuidOfInformationPackage;
	}
}
