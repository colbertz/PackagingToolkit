package de.feu.kdmp4.packagingtoolkit.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;

// DELETE_ME Wahrscheinlich
public interface BasicInformationPackage extends OaisEntity {
	public String getTitle();
	public boolean isVirtual();
	public PackageType getPackageType();
	public SerializationFormat getSerializationFormat();
}
