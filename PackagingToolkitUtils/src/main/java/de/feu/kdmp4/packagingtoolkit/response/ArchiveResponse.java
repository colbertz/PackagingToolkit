package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.adapters.xml.LocalDateTimeAdapter;
import de.feu.kdmp4.packagingtoolkit.validators.response.ArchiveResponseValidator;


@XmlRootElement
public class ArchiveResponse implements /*Comparable<ArchiveOutput>,*/ Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3900684556797467357L;
	// *********** Private attributes ********
	/**
	 * The archiveId is the uuid as String.
	 */
	private String archiveId;
	/**
	 * The type of the package. 
	 */
	private String packageType;
	/**
	 * The serialization format of the package.
	 */
	private String serializationFormat;
	/**
	 * The title of the package. 
	 */
	private String title;
	/**
	 * The uuid for identifying the archive on the server. 
	 */
	private String uuid;
	/**
	 * The date the archive was created at.
	 */
	private LocalDateTime creationDate;
	/**
	 * The date of the last modification of this archive.
	 */
	private LocalDateTime lastModificationDate;
	/**
	 * Contains the digital objects that are contained
	 * in this information package.
	 */
	private DigitalObjectListResponse digitalObjects;
	
	public ArchiveResponse() {
		digitalObjects = ResponseModelFactory.createEmptyDigitalObjectListResponse();
	}
	
	/**
	 * Creates a new object for sending the data of an archive via http.
	 * @param archiveId The id of the archive.
	 * @param archiveName The name of the zip file that contains the files of this archive.
	 * @param packageType The package type of this archive.
	 * @param serializationFormat The serialization format of this archive.
	 * @param title The title of this archive.
	 * @param uuid The unique identifier of this archive.
	 */
	public ArchiveResponse(final String archiveId, final PackageType packageType,
			final SerializationFormat serializationFormat, final String title, final UuidResponse uuid) {
		super();
		ArchiveResponseValidator.checkArchiveId(archiveId);
		this.archiveId = archiveId;
		ArchiveResponseValidator.checkPackageType(packageType);
		this.packageType = packageType.toString();
		this.serializationFormat = serializationFormat.toString();
		ArchiveResponseValidator.checkTitle(title);
		this.title = title;
		ArchiveResponseValidator.checkUuid(uuid);
		this.uuid = uuid.toString();
		digitalObjects = ResponseModelFactory.createEmptyDigitalObjectListResponse();
	}
	
	/**
	 * Creates a new object for sending the data of an archive via http. The archive id is
	 * taken as a long.
	 * @param archiveId The id of the archive.
	 * @param packageType The package type of this archive.
	 * @param serializationFormat The serialization format of this archive.
	 * @param title The title of this archive.
	 * @param uuid The unique identifier of this archive.
	 * @param virtual True if the archive is virtual false otherwise.
	 */
	public ArchiveResponse(final long archiveId, final PackageType packageType,
			final SerializationFormat serializationFormat, final String title, final UuidResponse uuid, final LocalDateTime creationDate,
			final LocalDateTime lastmodificationDate) {
		this(String.valueOf(archiveId), packageType, serializationFormat, title, uuid);
		this.creationDate = creationDate;
		this.lastModificationDate = lastmodificationDate;
	}
	
	// *********** Public methods ********
	/**
	 * Checks if a given uuid is equal to the uuid of this archive.
	 * @param uuid The uuid that should be checked.
	 * @return True, if uuid is equal to the uuid of this archive, false
	 * otherwise.
	 */
	public boolean areUuidsEqual(final UuidResponse uuid) {
		ArchiveResponseValidator.checkUuid(uuid);
		final String theOtherUuid = uuid.toString();
		int compareTo = this.uuid.compareTo(theOtherUuid); 
		if (compareTo == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isSIU() {
		if (packageType.equals(PackageType.SIU.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isSIP() {
		if (packageType.equals(PackageType.SIP.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public void addDigitalObject(final DigitalObjectResponse digitalObjectResponse) {
		if (isSIP()) {
			if (digitalObjects.getDigitalObjectCount() == 0) {
				digitalObjects.addDigitalObjectToList(digitalObjectResponse);
			}
		} else {
			digitalObjects.addDigitalObjectToList(digitalObjectResponse);
		}
	}
	
	// *********** Getters ***********
	@XmlElement
	public String getArchiveId() {
		return archiveId;
	}
	
	public String getTitle() {
		return title;
	}

	@XmlElement
	public String getUuid() {
		return uuid;
	}

	public void setArchiveId(String archiveId) {
		ArchiveResponseValidator.checkArchiveId(archiveId);
		this.archiveId = archiveId;
	}

	public void setTitle(String title) {
		ArchiveResponseValidator.checkTitle(title);
		this.title = title;
	}

	@XmlElement
	public String getPackageType() {
		return packageType;
	}

	public void setPackageType(String packageType) {
		ArchiveResponseValidator.checkPackageType(packageType);
		this.packageType = packageType;
	}

	@XmlElement
	public String getSerializationFormat() {
		return serializationFormat;
	}

	public void setSerializationFormat(String serializationFormat) {
		this.serializationFormat = serializationFormat;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	@XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
	public LocalDateTime getLastModificationDate() {
		return lastModificationDate;
	}

	public void setLastModificationDate(LocalDateTime lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}
	
	@XmlElement
	public DigitalObjectListResponse getDigitalObjects() {
		return digitalObjects;
	}
	
	public void setDigitalObjects(DigitalObjectListResponse digitalObjects) {
		this.digitalObjects = digitalObjects;
	}
}
