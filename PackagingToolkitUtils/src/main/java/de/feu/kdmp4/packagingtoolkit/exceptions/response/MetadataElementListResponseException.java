package de.feu.kdmp4.packagingtoolkit.exceptions.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;

/**
 * An exception that is thrown if there were any problems with a metadata element list response.
 * @author Christopher Olbertz
 *
 */
public class MetadataElementListResponseException extends ProgrammingException {
	// *************** Constants ****************
	private static final long serialVersionUID = 5190194735669260905L;

	// *************** Constructors **************
	/**
	 * Constructs an exception.
	 * @param message The message.
	 */
	private MetadataElementListResponseException(String message) {
		super(message);
	}

	// ************** Public methods *************
	/**
	 * Creates an exception that is thrown if a metadata element is null.
	 * @return The exception.
	 */
	public static final MetadataElementListResponseException metadataElementMayNotBeNull() {
		String message = I18nExceptionUtil.getMetadataElementMayNotBeNullString();
		return new MetadataElementListResponseException(message);
	}
}
