package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaxonomyResponse implements Serializable {
	private static final long serialVersionUID = 4917752767280842275L;
	private String title;
	private String namespace;
	private TaxonomyIndividualResponse rootIndividual;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getNamespace() {
		return namespace;
	}
	
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	@XmlElement
	public TaxonomyIndividualResponse getRootIndividual() {
		return rootIndividual;
	}
	
	public void setRootIndividual(TaxonomyIndividualResponse rootIndividual) {
		this.rootIndividual = rootIndividual;
	}
}
