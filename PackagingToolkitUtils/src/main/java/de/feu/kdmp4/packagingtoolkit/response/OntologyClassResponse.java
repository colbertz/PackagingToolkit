package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;

/**
 * An ontology class for sending via http. It contains subclasses and properties. 
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class OntologyClassResponse implements Serializable {
	private static final long serialVersionUID = 3503636933166022760L;
	
	/**
	 * An id for this class.
	 */
	private int ontologyClassId;
	/**
	 * The local name of the class.
	 */
	private String localName;
	/**
	 * The namespace of this class.
	 */
	private String namespace;
	/**
	 * The object properties of this class.
	 */
	private ObjectPropertyListResponse objectProperties;
	/**
	 * The properties of this class.
	 */
	private DatatypePropertyListResponse properties;
	/**
	 * The subclasses of this class.
	 */
	private OntologyClassListResponse subclasses;
	/**
	 * The iri of this class.
	 */
	private String iri;
	/**
	 * Contains several labels in different languages.
	 */
	private TextInLanguageListResponse labelTexts;
	
	// ******** Constructors *********
	/**
	 * The default constructor is necessary for JAXB and similar frameworks.
	 */
	public OntologyClassResponse() {
		subclasses = ResponseModelFactory.getOntologyClassListResponse();
		objectProperties = ResponseModelFactory.getObjectPropertyListResponse();
		properties = ResponseModelFactory.getOntologyPropertyListResponse();
		labelTexts = new TextInLanguageListResponse();
	}
	
	/**
	 * A constructor that constructs an ontology class and gets its namespace and
	 * local name.
	 * @param namespace The namespace of this class.
	 * @param localName The local name of this class.
	 */
	public OntologyClassResponse(final String namespace, final String localName) {
		this();
		this.namespace = namespace;
		this.localName = localName;
		labelTexts = new TextInLanguageListResponse();
	}
	
	// ******* Public methods **************
	/**
	 * Adds a subclass to this class.
	 * @param individual The individual that should be added to this class.
	 */
	public void addSubclass(OntologyClassResponse ontologyClass) {
		subclasses.addOntologyClassToList(ontologyClass);
	}

	/**
	 * Counts the number of subclasses.
	 * @return The number of subclasses.
	 */
	public int getSubclassCount() {
		return subclasses.getOntologyClassCount();
	}
	
	/**
	 * Determines the subclass at a given index.
	 * @param index The index.
	 * @return The subclass at the given index in the list of subclasses.
	 */
	public OntologyClassResponse getSubclassAt(int index) {
		return subclasses.getOntologyClassAt(index);
	}
	
	/**
	 * Counts the number of object properties.
	 * @return The number of object properties.
	 */
	public int getObjectPropertiesCount() {
		return objectProperties.getObjectPropertyCount();
	}
	
	/**
	 * Counts the number of properties.
	 * @return The number of properties.
	 */
	public int getPropertiesCount() {
		return properties.getOntologyPropertyCount();
	}
	
	/**
	 * Determines the property at a given index.
	 * @param index The index.
	 * @return The property at the given index in the list of properties.
	 */
	public DatatypePropertyResponse getPropertyAt(int index) {
		return properties.getOntologyPropertyAt(index);
	}
	
	/**
	 * Determines the object property at a given index.
	 * @param index The index.
	 * @return The object property at the given index in the list of object properties.
	 */
	public ObjectPropertyResponse getObjectPropertyAt(int index) {
		return objectProperties.getObjectPropertyAt(index);
	}
	
	@Override
	public String toString() {
		return getIri();
	}
	
	// ******* Getters and setters *********
	@XmlAttribute
	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	@XmlAttribute
	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	@XmlElement
	public DatatypePropertyListResponse getProperties() {
		return properties;
	}

	public void setProperties(DatatypePropertyListResponse properties) {
		this.properties = properties;
	}
	
	@XmlElement
	public ObjectPropertyListResponse getObjectProperties() {
		return objectProperties;
	}

	public void setObjectProperties(ObjectPropertyListResponse objectProperties) {
		this.objectProperties = objectProperties;
	}

	@XmlElement
	public OntologyClassListResponse getSubclasses() {
		return subclasses;
	}

	public void setSubclasses(OntologyClassListResponse subclasses) {
		this.subclasses = subclasses;
	}

	@XmlElement
	public TextInLanguageListResponse getLabelTexts() {
		if (labelTexts.getTexts().isEmpty()) {
			final TextInLanguageResponse defaultText = new TextInLanguageResponse();
			defaultText.setLanguage(getIri());
			labelTexts.addTextInLanguage(defaultText);
		}
		return labelTexts;
	}
	
	public void setLabelTexts(TextInLanguageListResponse labelTexts) {
		this.labelTexts = labelTexts;
	}
	
	@XmlAttribute
	public String getIri() {
		return namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localName;
	}
	
	public void setIri(String iri) {
		this.iri = iri;
	}

	@XmlAttribute
	public int getOntologyClassId() {
		return ontologyClassId;
	}

	public void setOntologyClassId(int ontologyClassId) {
		this.ontologyClassId = ontologyClassId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iri == null) ? 0 : iri.hashCode());
		result = prime * result + ((localName == null) ? 0 : localName.hashCode());
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result + ontologyClassId;
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		result = prime * result + ((subclasses == null) ? 0 : subclasses.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyClassResponse other = (OntologyClassResponse) obj;
		if (iri == null) {
			if (other.iri != null)
				return false;
		} else if (!iri.equals(other.iri))
			return false;
		if (localName == null) {
			if (other.localName != null)
				return false;
		} else if (!localName.equals(other.localName))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		if (ontologyClassId != other.ontologyClassId)
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (properties.getOntologyPropertyCount() == 0) {
			if (other.properties.getOntologyPropertyCount() != 0) {
				return false;
			}
		} else if (!properties.equals(other.properties))
			return false;
		if (subclasses == null) {
			if (other.subclasses != null) {
				return false;
			}
		} else if (subclasses.getOntologyClassCount() == 0) {
			if (other.subclasses.getOntologyClassCount() != 0) {
				return false;
			}
		} else if (!subclasses.equals(other.subclasses))
			return false;
		return true;
	}
}