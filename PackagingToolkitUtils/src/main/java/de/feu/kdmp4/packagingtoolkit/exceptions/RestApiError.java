package de.feu.kdmp4.packagingtoolkit.exceptions;

import org.springframework.http.HttpStatus;

// DELETE_ME
public class RestApiError {
	private String message;
	private HttpStatus httpStatus;
	
	public RestApiError(final String message, final HttpStatus httpStatus) {
		this.message = message;
		 this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
