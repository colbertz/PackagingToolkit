package de.feu.kdmp4.packagingtoolkit.exceptions.ontology;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class RestrictionNotSatisfiedException extends UserException {
	private static final long serialVersionUID = 3172897520056486347L;
	private static final String MAX_INCLUSIVE_RESTRICTION = "MaxInclusiveRestriction";
	private static final String MIN_INCLUSIVE_RESTRICTION = "MinInclusiveRestriction";
	
	private String summary;
	
	private RestrictionNotSatisfiedException(String message, String summary) {
		super(message);
		this.summary = summary;
	}
	
	public static RestrictionNotSatisfiedException maxRestrictionNotSatisfied(
			String value, String restrictionValue) {
		String message = I18nExceptionUtil.getMaxInclusiveRestrictionNotSatisfiedString();
		message = String.format(message, restrictionValue.toString(), value.toString());
		return new RestrictionNotSatisfiedException(message, MAX_INCLUSIVE_RESTRICTION);
	}
	
	public static RestrictionNotSatisfiedException minRestrictionNotSatisfied(
			String value, String restrictionValue) {
		String message = I18nExceptionUtil.getMinInclusiveRestrictionNotSatisfiedString();
		message = String.format(message, restrictionValue, value);
		return new RestrictionNotSatisfiedException(message, MIN_INCLUSIVE_RESTRICTION);
	}
	
	@Override
	public String getSummary() {
		return summary;
	}
}
