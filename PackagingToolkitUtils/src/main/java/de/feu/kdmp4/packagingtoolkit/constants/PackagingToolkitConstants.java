package de.feu.kdmp4.packagingtoolkit.constants;

/**
 * Contains some general contants that are used in the PackagingToolkit.
 * @author Christopher Olbertz
 *
 */
public class PackagingToolkitConstants {
	/**
	 * The number of bits in a byte.
	 */
	public static final int BITS_OF_ONE_BYTE = 1024;
	/**
	 * A constant for a comma. 
	 */
	public static final String COMMA = ",";
	/**
	 * The name of the class that contains the digital objects.
	 */
	public static final String DIGITAL_OBJECT_CLASS_NAME = "Digital_Object";
	/**
	 * The boolean value false as string.
	 */
	public static final String FALSE = "false";
	/**
	 * The name of the file that contains the metadata that have been extracted by
	 * FITS. This file is contained in the zip file if a not virtual information
	 * package is downloaded.
	 */
	public static final String FILENAME_METADATA_FITS = "fits.txt";
	/**
	 * This string is send by components (server and mediator) to signalize that they
	 * are accessible.
	 */
	public static final String I_AM_HERE = "I am here!";
	/**
	 * A constant for a line feed in a string.
	 */
	public static final String LINE_FEED = "\n";
	/**
	 * The name of the manifest file that is created through the serialization.
	 */
	public static final String NAME_OF_MANIFEST_FILE = "manifest.rdf";
	/**
	 * The symbol that separates the namespace and the local name from each other in an iri.
	 */
	public static final String NAMESPACE_SEPARATOR = "#";
	/**
	 * A string for marking a submission information package
	 * that is not virtual.
	 */
	public static final String NOT_VIRTUAL = "notvirtual";
	
	public static final String PROPERTY_HAS_TAXONOMY = "hasTaxonomie";
	/**
	 * In a SIP there is only one digital object allowed.
	 */
	public static final int SIP_MAX_DIGITAL_OBJECTS_NUMBER = 1;
	public static final String IDENTIFIER_DAY = "Day";
	public static final String IDENTIFIER_HOUR = "Hour";
	public static final String IDENTIFIER_MINUTE = "Minute";
	public static final String IDENTIFIER_MONTH = "Month";
	public static final String IDENTIFIER_NEGATIVE = "Negative";
	public static final String IDENTIFIER_SECOND = "Second";
	public static final String IDENTIFIER_YEAR = "Year";
	/**
	 * A namespace for the elements that are defined only in the PackagingToolkit.
	 */
	public static final String NAMESPACE_PACKACKING_TOOLKIT = "http://de.feu.kdmp4.pkgtkt";
	/**
	 * Contains the suffix of a turtle file.
	 */
	public static final String SUFFIX_TTL = ".ttl";
	/**
	 * The boolean value true as string.
	 */
	public static final String TRUE = "true";
	/**
	 * A string for marking a submission information package
	 * that is virtual.
	 */
	public static final String VIRTUAL = "virtual";
}