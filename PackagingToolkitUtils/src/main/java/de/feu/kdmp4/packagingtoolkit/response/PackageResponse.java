package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The representation of an information package as XML. It is necessary, because some information
 * can not be send via http - for example the type information given by a class hierarchy is lost. 
 * Therefore the enum {@see de.feu.kdmp4.packagingtoolkit.enums.PackageType} is used. 
 * @author Christopher Olbertz
 *
 */
@XmlRootElement(name = "PackageResponse")
public class PackageResponse {
	/**
	 * The type of the package - SIP oder SIU.
	 */
	private String packageType;
	/**
	 * The title of this information package.
	 */
	private String title;
	/**
	 * The uuid of the information package.
	 */
	private String uuid;
	
	public PackageResponse() {
	}
	
	/**
	 * Creates a new object.
	 * @param packageType The type of the package - SIP oder SIU.
	 * @param uuid The uuid of the information package.
	 * @param title The title of this information package.
	 */
	public PackageResponse(final String packageType, final UuidResponse uuid, final String title) {
		super();
		
		this.packageType = packageType;
		this.uuid = uuid.toString();
		this.title = title;
	}

	@XmlElement
	public String getPackageType() {
		return packageType;
	}
	
	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}
	
	@XmlElement
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@XmlElement
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
