package de.feu.kdmp4.packagingtoolkit.exceptions.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * This exception is thrown when there are problems with objects of the type
 * {@link de.feu.kdmp4.packagingtoolkit.response.UuidResponse}.
 * @author Christopher Olbertz
 *
 */
public class UuidResponseException extends ProgrammingException {
	// ************** Constants ******************
	private static final long serialVersionUID = -5504472830906850000L;

	// ************** Constructors ***************
	private UuidResponseException(String message) {
		super(message);
	}
	
	// ************** Public methods *************
	/**
	 * Throws an exception if a object of {@link de.feu.kdmp4.packagingtoolkit.response.UuidResponse}
	 * is null.
	 * @return A new exception.
	 */
	public static UuidResponseException uuidResponseMayNotBeNull() {
		String message = I18nExceptionUtil.getUuidResponseMayNotBeNullString();
		return new UuidResponseException(message);
	}

}
