package de.feu.kdmp4.packagingtoolkit.models.abstractClasses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.ListInterface;
import de.feu.kdmp4.packagingtoolkit.validators.ListValidator;

public abstract class SortableAbstractList<T extends Comparable<T>> implements ListInterface {
	/**
	 * The list that is encapsulated.
	 */
	private List<T> objectList;
	
	public SortableAbstractList() {
		objectList = new ArrayList<>();
	}
	
	public SortableAbstractList(final List<T> objectList) {
		this.objectList = objectList;
	}
	
	public SortableAbstractList(final Collection<T> objectCollection) {
		this();
		for (T object: objectCollection) {
			objectList.add(object);
		}
	}
	
	/**
	 * Adds an element to the list. The element is checked. It may not be null.
	 * @param element The new element.
	 * @throws ListException Is thrown, when a element that 
	 * is null should be inserted into the list.
	 */
	protected void add(final T element) {
		ListValidator.checkListElement(element);
		objectList.add(element);
		Collections.sort(objectList);
	}
	
	/**
	 * Checks if the list contains an object. The object has to implement the methods equals() and hashCode().
	 * @param object The object to look for.
	 * @return True if the list contains the object, false otherwise.
	 * @throws ListException if the parameter object is null.
	 */
	protected boolean contains(T object) {
		ListValidator.checkListElement(object);
		return objectList.contains(object);
	}
	
	/**
	 * Returns an element from the list. The index is validated.
	 * @param index The index of the element to look for.
	 * @return The found element.
	 * @throws ListException Is thrown, if it is tried to
	 * access an element in the list with an invalid index. An index is 
	 * invalid if it is less than zero or greater than the number of 
	 * elements in the list.
	 */
	@Override
	public T getElement(final int index) {
		ListValidator.checkIndex(index, objectList);
		return objectList.get(index);
	}
	
	/**
	 * Determines the number of the objects contained in the list.
	 * @return The number of objects.
	 */
	public int getSize() {
		return objectList.size();
	}
	
	public boolean isEmpty() {
		return objectList.isEmpty();
	}
	
	/**
	 * Removes an element from the list.
	 * @param element The element that should be removed.
	 * @throws ListException Is thrown if the list element that
	 * should be remove is null.
	 */
	protected void remove(final T element) {
		ListValidator.checkListElement(element);
		objectList.remove(element);
	}
}