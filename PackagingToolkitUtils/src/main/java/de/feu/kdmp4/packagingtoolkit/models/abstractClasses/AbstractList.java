package de.feu.kdmp4.packagingtoolkit.models.abstractClasses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.ListInterface;
import de.feu.kdmp4.packagingtoolkit.validators.ListValidator;


/**
 * An abstract class that encapsulates a list and provides some access methods 
 * for this list. This class contains a list with objects. Every concrete subclass
 * has to implement own methods that call the methods in AbstractList. The subclasses
 * have to perform such things like type casting. AbstractList validates the data
 * like indizes. The greatest part of the logic is implemented here. 
 * @author Christopher Olbertz
 * 
 */
public abstract class AbstractList implements ListInterface {
	// ****************** Attributes *****************
	/**
	 * The list that is encapsulated.
	 */
	private List<Object> objectList;
	
	// ****************** Constructors ***************
	public AbstractList() {
		objectList = new ArrayList<>();
	}
	
	public AbstractList(final Collection<Object> objectCollection) {
		this();
		for (Object object: objectCollection) {
			objectList.add(object);
		}
	}
	
	protected Object[] toArray() {
		return objectList.toArray();
	}
	
	// ***************** Protected methods ***********
	/**
	 * Adds an element to the list. The element is checked. It may not be null.
	 * @param element The new element.
	 * @throws ListException Is thrown, when a element that 
	 * is null should be inserted into the list.
	 */
	protected void add(final Object element) {
		ListValidator.checkListElement(element);
		objectList.add(element);
	}
	
	/**
	 * Adds several object to the list.
	 * @param elements The object that should be added.
	 */
	protected void addCollection(final ListInterface elements) {
		for (int i = 0; i < elements.getSize(); i++) {
			Object element = elements.getElement(i);
			objectList.add(element);
		}
	}
	/**
	 * Checks if the list contains an object. The object has to implement the methods equals() and hashCode().
	 * @param object The object to look for.
	 * @return True if the list contains the object, false otherwise.
	 * @throws ListException Is thrown if the object is null.
	 */
	protected boolean contains(final Object object) {
		ListValidator.checkListElement(object);
		return objectList.contains(object);
	}
	
	/**
	 * Adds an object at the given index.
	 * @param index The index where the object should be placed.
	 * @param object The object that should be added to the list.
	 */
	protected void addObjectAt(final int index, final Object object) {
		objectList.add(index, object);
	}
	
	/**
	 * Returns an element from the list. The index is validated.
	 * @param index The index of the element to look for.
	 * @return The found element.
	 * @throws ListException Is thrown, if it is tried to
	 * access an element in the list with an invalid index. An index is 
	 * invalid if it is less than zero or greater than the number of 
	 * elements in the list.
	 */
	@Override
	public Object getElement(final int index) {
		ListValidator.checkIndex(index, objectList);
		return objectList.get(index);
	}
	
	/**
	 * Determines the number of the objects contained in the list.
	 * @return The number of objects.
	 */
	@Override
	public int getSize() {
		return objectList.size();
	}
	
	@Override
	public boolean isEmpty() {
		return objectList.isEmpty();
	}
	
	@Override
	public boolean isNotEmpty() {
		return !objectList.isEmpty();
	}
	
	/**
	 * Removes an element from the list.
	 * @param element The element that should be removed.
	 * @throws ListException Is thrown if the list element that
	 * should be removed is null.
	 */
	protected void remove(final Object element) {
		ListValidator.checkListElement(element);
		objectList.remove(element);
	}
	
	/**
	 * Removes an element at a given index from the list.
	 * @param index The index of the element to remove.
	 * @return The removed element.
	 */
	protected Object remove(final int index) {
		Object object = objectList.get(index);
		objectList.remove(index);
		return object;
	}
	
	@Override
	public String toString()  {
		final StringBuffer buffer = new StringBuffer();
		
		for (Object object: objectList) {
			buffer.append(object.toString()).append(PackagingToolkitConstants.LINE_FEED);
		}
		
		return buffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objectList == null) ? 0 : objectList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractList other = (AbstractList) obj;
		if (objectList == null) {
			if (other.objectList != null)
				return false;
		} else if (!objectList.equals(other.objectList))
			return false;
		return true;
	}
}