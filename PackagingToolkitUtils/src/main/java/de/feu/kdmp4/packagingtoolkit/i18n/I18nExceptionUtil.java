package de.feu.kdmp4.packagingtoolkit.i18n;

import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * Encapsulates the keys for internationalization. Java code is not allowed
 * to call the message bundles but it has to use this class and its static
 * methods. Therefore the other classes do not have to know the names of 
 * the keys. This class contains the keys only for exceptions.
 * @author Christopher Olbertz
 *
 */
public class I18nExceptionUtil {
	private static final String ARCHIVE_DIRECTORY_COULD_NO_BE_CREATED = "archive-directory-could-not-be-created";
	private static final String ARCHIVE_DIRECTORY_MAY_NOT_BE_EMPTY = "archive-directory-may-not-be-empty";
	private static final String ARCHIVE_ID_MAY_NOT_BE_EMPTY = "archive-id-may-not-be-empty";
	private static final String ARCHIVE_MAY_NOT_BE_NULL = "archive-may-not-be-null";
	private static final String ARCHIVE_NAME_INVALID = "archive-name-invalid";
	private static final String ARCHIVE_TITLE_MAY_NOT_BE_EMPTY = "archive-title-may-not-be-empty";
	private static final String ARRAY_MAY_NOT_BE_NULL = "array-may-not-be-null";
	private static final String BASE_ONTOLOGY_NOT_WRITABLE = "base-ontology-not-writable";
	private static final String CONFIGURATION_FILE_DOES_NOT_EXIST = "configuration-file-does-not-exist";
	private static final String CONFIGURATION_FILE_NOT_READABLE = "configuration-file-not-readable";
	private static final String CONFIGURATION_FILE_NOT_WRITABLE = "configuration-file-not-writable";
	private static final String COPY_PROCESS_NOT_POSSIBLE = "copy-process-not-possible";
	private static final String DATASOURCE_NOT_CORRECTLY_CONFIGURED = "datasource-not-correctly-configured";
	private static final String DATE_MAY_NOT_BE_NULL = "date-may-not-be-null";
	private static final String DAYS_INVALID = "days-invalid";
	private static final String DEFAULT_LABEL_MAY_NOT_BE_EMPTY = "default-label-may-not-be-empty";
	private static final String DIGITAL_OBJECT_ALREADY_SET = "digital-object-already-set";
	private static final String DIGITAL_OBJECT_MAY_NOT_BE_NULL = "digital-object-may-not-be-null";
	private static final String DIGITAL_OBJECT_NOT_CONTAINED_IN_SIP = "digital-object-not-contained-in-sip";
	private static final String DIGITAL_OBJECT_NOT_FOUND_BY_UUID = "digital-object-not-found-by-uuid";
	private static final String DIRECTORY_COULD_NOT_BE_CREATED = "directory-could-not-be-created";
	private static final String DIRECTORY_EXISTS = "directory-exists";
	private static final String DUPLICATE_VIEW_NAME_EXCEPTION = "duplicate-view-name-exception";
	private static final String DOES_NOT_CONTAIN_VALID_UUID = "does-not-contain-valid-uuid";
	private static final String EXTRACTOR_TOOL_MAY_NOT_BE_NULL = "extractor-tool-may-not-be-null";
	private static final String EXTRACTOR_TOOL_NAME_MAY_NOT_BE_EMPTY = "extractor-tool-name-may-not-be-empty";
	private static final String EXTRACTOR_TOOL_LIST_MAY_NOT_BE_NULL = "extractor-tool-list-may-not-be-null";
	private static final String FILE_ALREADY_UPLOADED = "file-already-uploaded";
	private static final String FILE_MAY_NOT_BE_NULL = "file-may-not-be-null";
	private static final String FILENAME_MAY_NOT_BE_EMPTY = "filename-may-not-be-empty";
	private static final String FITS_INSTALLATION_PATH_NOT_CORRECT = "fits-installation-path-not-correct";
	private static final String GENERAL_IO_EXCEPTION = "general-io-exception";
	private static final String HOURS_INVALID = "hours-invalid";
	private static final String INDIVIDUAL_INVALID = "individual-invalid";
	private static final String INFORMATION_PACKAGE_MAY_NOT_BE_NULL = "information-package-may-not-be-null";
	private static final String INFORMATION_PACKAGE_NOT_AVAILABLE = "information-package-not-available";
	private static final String INDEX_NOT_IN_RANGE = "index-not-in-range";
	private static final String LANGUAGE_CODE_MAY_NOT_BE_EMPTY = "language-code-may-not-be-empty";
	private static final String LANGUAGE_DESCRIPTION_MAY_NOT_BE_EMPTY = "language-description-may-not-be-empty";
	private static final String LIST_ELEMENT_MAY_NOT_BE_NULL = "list-element-may-not-be-null";
	private static final String LOCALE_MAY_NOT_BE_NULL = "locale-may-not-be-null";
	private static final String MAX_INCLUSIVE_RESTRICTION_NOT_SATISFIED = "max-inclusive-restriction-not-satisfied";
	private static final String METADATA_ELEMENT_MAY_NOT_BE_NULL = "metadata-element-may-not-be-null";
	private static final String ABSOLUTE_FILEPATH_CANNOT_BE_CREATED_UNDER_WINDOWS = "abolute-path-is-not-supported-under-windows";
	private static final String MIN_INCLUSIVE_RESTRICTION_NOT_SATISFIED = "min-inclusive-restriction-not-satisfied";
	private static final String MINUTES_INVALID = "minutes-invalid";
	private static final String MONTHS_INVALID = "months-invalid";
	private static final String NOT_A_DIRECTORY = "not-a-directory";
	// DELETE_ME
	private static final String NOT_A_HTTP_REFERENCE = "not-a-http-reference";
	// DELETE_ME
	private static final String NOT_A_FILE_REFERENCE = "not-a-file-reference";
	private static final String NOT_A_ZIP_FILE = "not-a-zip-file";
	private static final String NOT_AN_ONTOLOGY_FILE = "not-an-ontology-file";
	private static final String NOT_A_VALID_URL = "not-a-valid-url";
	private static final String OBJECT_MAY_NOT_BE_NULL = "object-may-not-be-null";
	private static final String ONTOLOGY_CLASS_NOT_IN_MAP = "ontology-class-not-in-map";
	private static final String PACKAGE_TYPE_MAY_NOT_BE_EMPTY = "package-type-may-not-be-empty";
	private static final String PACKAGING_TOOLKIT_IO = "packaging-toolkit-io";
	private static final String PATHNAME_MAY_NO_BE_EMPTY = "pathname-may-no-be-empty";
	private static final String PROPERTY_MAY_NOT_BE_NULL = "property-may-not-be-null";
	private static final String PROPERTY_ID_MAY_NOT_BE_NULL = "property-id-may-not-be-null";
	private static final String REFERENCE_DOES_NOT_EXIST = "reference-does-not-exist";
	private static final String REFERENCE_MAY_NOT_BE_NULL = "reference-may-not-be-null";
	private static final String RULE_ONTOLOGY_NOT_WRITABLE = "rule-ontology-not-writable";
	private static final String SECONDS_INVALID = "seconds-invalid";
	private static final String SERIALIZATION_FORMAT_MAY_NOT_BE_EMPTY = "serialization-format-may-not-be-empty";
	private static final String STORAGE_NOT_A_DIRECTORY = "storage-not-a-directory";
	private static final String STRING_MAY_NOT_BE_EMPTY = "string-may-not-be-empty";
	private static final String SUMMARY_CONFIGURATION_EXCEPTION = "summary-configuration-exception";
	private static final String SUMMARY_MEDIATOR_CONFIGURATION_EXCEPTION = "summary-mediator-configuration-exception";
	private static final String SUMMARY_DURATION_EXCEPTION = "summary-duration-exception";
	private static final String SUMMARY_FILE_EXCEPTION = "summary-file-exception";
	private static final String SUMMARY_INDIVIDUAL_INVALID = "summary-individual-invalid";
	private static final String SUMMARY_INFORMATION_PACKAGE_EXCEPTION = "summary-information-package-exception";
	private static final String SUMMARY_PACKAGING_EXCEPTION = "summary-packaging-exception";
	private static final String SUMMARY_PROPERTY_RANGE_EXCEPTION = "summary-property-range-exception";
	private static final String SUMMARY_URL_EXCEPTION = "summary-url-exception";
	private static final String URL_INVALID = "url-invalid";
	private static final String UUID_ALREADY_IN_ARCHIVE = "uuid-already-in-archive";
	private static final String UUID_MAY_NOT_BE_EMPTY = "uuid-may-not-be-empty";
	private static final String UUID_NOT_IN_QUEUE_THREAD = "uuid-not-in-queue-thread";
	private static final String UUID_RESPONSE_MAY_NOT_BE_NULL = "uuid-response-may-not-be-null";
	private static final String VALUE_NOT_IN_BYTE_RANGE = "value-not-in-byte-range";
	private static final String VALUE_NOT_IN_INTEGER_RANGE = "value-not-in-integer-range";
	private static final String VALUE_NOT_IN_LONG_RANGE = "value-not-in-long-range";
	private static final String VALUE_NOT_IN_SHORT_RANGE = "value-not-in-short-range";
	private static final String VIEW_MAY_NOT_BE_NULL = "view-may-not-be-null";
	private static final String WRONG_ZIP_FILENAME_FORMAT = "wrong-zip-filename-format";
	private static final String YEARS_INVALID = "years-invalid";
	private static final String ZIP_FILE_COULD_NOT_BE_CREATED = "zip-file-could-no-created";
	private static final String VIEW_EXCEPTION = "view-exception";
	
	private static ResourceBundle messagesResourceBundle;
	
	static {
		messagesResourceBundle = I18nUtil.getExceptionsResourceBundle();
	}
	
	public static final String getArchiveDirectoryDoesNotExistString(String uuid) {
		String message = messagesResourceBundle.getString(ARCHIVE_DIRECTORY_COULD_NO_BE_CREATED);
		return String.format(message, uuid); 
	}

	public static final String getArchiveDirectoryMayNotBeEmptyString(String filename) {
		String message = messagesResourceBundle.getString(ARCHIVE_DIRECTORY_MAY_NOT_BE_EMPTY);
		return String.format(message, filename); 
	}
	
	public static final String getArchiveIdMayNotBeEmptyString() {
		return messagesResourceBundle.getString(ARCHIVE_ID_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getArchiveMayNotBeNullString() {
		return messagesResourceBundle.getString(ARCHIVE_MAY_NOT_BE_NULL);
	}
	
	public static final String getArchiveNameInvalidString() {
		return messagesResourceBundle.getString(ARCHIVE_NAME_INVALID);
	}
	
	public static final String getTitleInvalidMayNotBeEmptyString() {
		return messagesResourceBundle.getString(ARCHIVE_TITLE_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getArrayMayNotBeNullString() {
		return messagesResourceBundle.getString(ARRAY_MAY_NOT_BE_NULL);
	}
	
	public static final String getBaseOntologyNotWritableString() {
		return messagesResourceBundle.getString(BASE_ONTOLOGY_NOT_WRITABLE);
	}
	
	public static final String getConfigurationFileDoesNotExistString() {
		return messagesResourceBundle.getString(CONFIGURATION_FILE_DOES_NOT_EXIST);
	}
	
	public static final String getConfigurationFileNotReadableString() {
		return messagesResourceBundle.getString(CONFIGURATION_FILE_NOT_READABLE);
	}
	
	public static final String getConfigurationFileNotWritableString() {
		return messagesResourceBundle.getString(CONFIGURATION_FILE_NOT_WRITABLE);
	}

	public static final String getCopyProcessNotPossibleString() {
		return messagesResourceBundle.getString(COPY_PROCESS_NOT_POSSIBLE);
	}
	
	public static final String getDataSourceNotCorrectlyConfiguredString(String dataSourceName, String originalMessage) {
		String message = messagesResourceBundle.getString(DATASOURCE_NOT_CORRECTLY_CONFIGURED);
		message = String.format(dataSourceName, originalMessage);
		return message;
	}
	
	public static final String getDateMayNotBeNullString() {
		return messagesResourceBundle.getString(DATE_MAY_NOT_BE_NULL);
	}

	public static final String getDaysInvalidString() {
		return messagesResourceBundle.getString(DAYS_INVALID);
	}
	
	public static final String getDefaultLabelMayNotBeEmptyString() {
		return messagesResourceBundle.getString(DEFAULT_LABEL_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getDigitalObjectAlreadySetString() {
		return messagesResourceBundle.getString(DIGITAL_OBJECT_ALREADY_SET);
	}

	public static final String getDigitalObjectMayNotBeNullString() {
		return messagesResourceBundle.getString(DIGITAL_OBJECT_MAY_NOT_BE_NULL);
	}
	
	public static final String getDigitalObjectNotContainedInSIPString(final String uuidOfDigitalObject, 
			final String uuidOfSIP) {
		String message = messagesResourceBundle.getString(DIGITAL_OBJECT_NOT_CONTAINED_IN_SIP);
		message = MessageFormat.format(message, uuidOfDigitalObject, uuidOfSIP);
		return message;
	}
	
	public static final String getDuplicateViewNameString(final String viewName) {
		String message = messagesResourceBundle.getString(DUPLICATE_VIEW_NAME_EXCEPTION);
		message = String.format(message, viewName);
		return message;
	}
	
	public static final String getNotAValidUrlString(final String invalidUrl) {
		String message = messagesResourceBundle.getString(NOT_A_VALID_URL);
		message = String.format(message, invalidUrl);
		return message;
	}
	
	public static final String getDigitalObjectNotFoundByUuidString() {
		return messagesResourceBundle.getString(DIGITAL_OBJECT_NOT_FOUND_BY_UUID);
	}
	
	public static final String getDirectoryExistsString() {
		return messagesResourceBundle.getString(DIRECTORY_EXISTS);
	}
	
	public static final String getFitsInstallationPathNotCorrect(String wrongFitsPath) {
		String message = messagesResourceBundle.getString(FITS_INSTALLATION_PATH_NOT_CORRECT);
		message = String.format(message, wrongFitsPath);
		return message;
	}
	
	public static final String getDirectoryCouldNotBeCreatedString() {
		return messagesResourceBundle.getString(DIRECTORY_COULD_NOT_BE_CREATED);
	}

	public static final String getDoesNotContainValidUuidString() {
		return messagesResourceBundle.getString(DOES_NOT_CONTAIN_VALID_UUID);
	}
	
	public static final String getExtractorToolMayNotBeNullString() {
		return messagesResourceBundle.getString(EXTRACTOR_TOOL_MAY_NOT_BE_NULL);
	}
	
	public static final String getExtractorToolNameMayNotBeEmptyString() {
		return messagesResourceBundle.getString(EXTRACTOR_TOOL_NAME_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getExtractorToolListMayNotBeNullString() {
		return messagesResourceBundle.getString(EXTRACTOR_TOOL_LIST_MAY_NOT_BE_NULL);
	}

	public static final String getFileAlreadyUploadedString() {
		return messagesResourceBundle.getString(FILE_ALREADY_UPLOADED);
	}
	
	public static final String getFileMayNotBeNullString() {
		return messagesResourceBundle.getString(FILE_MAY_NOT_BE_NULL);
	}
	
	public static final String getFilenameMayNotBeEmptyString() {
		return messagesResourceBundle.getString(FILENAME_MAY_NOT_BE_EMPTY);
	}

	public static final String getGeneralIOExceptionString() {
		return messagesResourceBundle.getString(GENERAL_IO_EXCEPTION);
	}
	
	public static final String getHoursInvalidString() {
		return messagesResourceBundle.getString(HOURS_INVALID);
	}
	
	public static final String getIndividualInvalidString() {
		return messagesResourceBundle.getString(INDIVIDUAL_INVALID);
	}
	
	public static final String getInformationPackageMayNotBeNullString() {
		return messagesResourceBundle.getString(INFORMATION_PACKAGE_MAY_NOT_BE_NULL);
	}
	
	public static String getInformationPackageNotAvailableString() {
		return messagesResourceBundle.getString(INFORMATION_PACKAGE_NOT_AVAILABLE);
	}
	
	public static final String getIndexNotInRangeString() {
		return messagesResourceBundle.getString(INDEX_NOT_IN_RANGE);
	}
	
	public static final String getLanguageCodeMayNotBeEmptyString() {
		return messagesResourceBundle.getString(LANGUAGE_CODE_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getLanguageDescriptionMayNotBeEmptyString() {
		return messagesResourceBundle.getString(LANGUAGE_DESCRIPTION_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getListElementMayNotBeNullString() {
		return messagesResourceBundle.getString(LIST_ELEMENT_MAY_NOT_BE_NULL);
	}
	
	public static final String getLocaleMayNotBeNullString() {
		return messagesResourceBundle.getString(LOCALE_MAY_NOT_BE_NULL);
	}
	
	public static final String getMaxInclusiveRestrictionNotSatisfiedString() {
		return messagesResourceBundle.getString(MAX_INCLUSIVE_RESTRICTION_NOT_SATISFIED);
	}
	
	public static final String getMethodNotSupportedUnderWindowsString() {
		return messagesResourceBundle.getString(ABSOLUTE_FILEPATH_CANNOT_BE_CREATED_UNDER_WINDOWS);
	}
	
	public static final String getMetadataElementMayNotBeNullString() {
		return messagesResourceBundle.getString(METADATA_ELEMENT_MAY_NOT_BE_NULL);
	}
	
	public static final String getMinInclusiveRestrictionNotSatisfiedString() {
		return messagesResourceBundle.getString(MIN_INCLUSIVE_RESTRICTION_NOT_SATISFIED);
	}
	
	public static final String getMinutesInvalidString() {
		return messagesResourceBundle.getString(MINUTES_INVALID);
	}
	
	public static final String getMonthsInvalidString() {
		return messagesResourceBundle.getString(MONTHS_INVALID);
	}
	
	public static final String getNotADirectoryString(String filename) {
		String message = messagesResourceBundle.getString(NOT_A_DIRECTORY); 
		return String.format(message, filename);
	}
	
	public static final String getNotAFileReferenceString() {
		return messagesResourceBundle.getString(NOT_A_FILE_REFERENCE);
	}
	
	public static final String getNotAHttpReferenceString() {
		return messagesResourceBundle.getString(NOT_A_HTTP_REFERENCE);
	}
	
	public static final String getNotAZipFileString() {
		return messagesResourceBundle.getString(NOT_A_ZIP_FILE);
	}
	
	public static final String getNotAnOntologyFileString() {
		return messagesResourceBundle.getString(NOT_AN_ONTOLOGY_FILE);
	}
	
	public static final String getObjectMayNotBeNullString() {
		return messagesResourceBundle.getString(OBJECT_MAY_NOT_BE_NULL);
	}
	
	public static final String getOntologyClassNotInMapString(String className) {
		String message = messagesResourceBundle.getString(ONTOLOGY_CLASS_NOT_IN_MAP);
		message = String.format(message, className);
		return message;
	}
	
	public static final String getPackageTypeMayNotBeEmptyString() {
		return messagesResourceBundle.getString(PACKAGE_TYPE_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getPackagingToolkitIoString() {
		return messagesResourceBundle.getString(PACKAGING_TOOLKIT_IO);
	}
	
	public static final String getPathnameMayNotBeEmptyString() {
		return messagesResourceBundle.getString(PATHNAME_MAY_NO_BE_EMPTY);
	}
	
	public static final String getPropertyMayNotBeEmptyString() {
		return messagesResourceBundle.getString(PROPERTY_MAY_NOT_BE_NULL);
	}
	
	public static final String getPropertyIdMayNotBeEmptyString() {
		return messagesResourceBundle.getString(PROPERTY_ID_MAY_NOT_BE_NULL);
	}
	
	public static final String getReferenceDoesNotExistString() {
		return messagesResourceBundle.getString(REFERENCE_DOES_NOT_EXIST);
	}
	
	public static final String getReferenceMayNotBeNullString() {
		return messagesResourceBundle.getString(REFERENCE_MAY_NOT_BE_NULL);
	}
	
	public static final String getRuleOntologyNotWritableString() {
		return messagesResourceBundle.getString(RULE_ONTOLOGY_NOT_WRITABLE);
	}
	
	public static final String getSecondsInvalidString() {
		return messagesResourceBundle.getString(SECONDS_INVALID);
	}
	
	public static final String getSerializationFormatMayNotBeEmptyString() {
		return messagesResourceBundle.getString(SERIALIZATION_FORMAT_MAY_NOT_BE_EMPTY);
	}

	public static final String getStorageNotADirectoryString() {
		return messagesResourceBundle.getString(STORAGE_NOT_A_DIRECTORY);
	}
	
	public static final String getStringMayNotBeEmptyString() {
		return messagesResourceBundle.getString(STRING_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getSummaryConfigurationExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_CONFIGURATION_EXCEPTION);
	}

	public static final String getSummaryMediatorConfigurationExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_MEDIATOR_CONFIGURATION_EXCEPTION);
	}
	
	public static final String getSummaryDurationExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_DURATION_EXCEPTION);
	}
	
	public static final String getSummaryFileExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_FILE_EXCEPTION);
	}
	
	public static final String getSummaryIndividualInvalidString() {
		return messagesResourceBundle.getString(SUMMARY_INDIVIDUAL_INVALID);
	}
	
	public static final String getSummaryInformationPackageExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_INFORMATION_PACKAGE_EXCEPTION);
	}
	
	public static final String getSummaryPackagingExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_PACKAGING_EXCEPTION);
	}
	
	public static final String getSummaryPropertyRangeExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_PROPERTY_RANGE_EXCEPTION);
	}
	
	public static final String getSummaryUrlExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_URL_EXCEPTION);
	}
	
	public static String getUrlInvalidString() {
		return messagesResourceBundle.getString(URL_INVALID);
	}

	public static final String getUuidAlreadyInArchiveString() {
		return messagesResourceBundle.getString(UUID_ALREADY_IN_ARCHIVE);
	}
	
	public static final String getUuidResponseMayNotBeNullString() {
		return messagesResourceBundle.getString(UUID_RESPONSE_MAY_NOT_BE_NULL);
	}
	
	public static final String getUuidMayNotBeEmptyString() {
		return messagesResourceBundle.getString(UUID_MAY_NOT_BE_EMPTY);
	}
	
	public static final String getUuidNotInQueueThreadString() {
		return messagesResourceBundle.getString(UUID_NOT_IN_QUEUE_THREAD);
	}
	
	public static final String getValueNotInByteRangeString() {
		return messagesResourceBundle.getString(VALUE_NOT_IN_BYTE_RANGE);
	}
	
	public static final String getValueNotInIntegerRangeString() {
		return messagesResourceBundle.getString(VALUE_NOT_IN_INTEGER_RANGE);
	}
	
	public static final String getValueNotInLongRangeString() {
		return messagesResourceBundle.getString(VALUE_NOT_IN_LONG_RANGE);
	}
	
	public static final String getValueNotInShortRangeString() {
		return messagesResourceBundle.getString(VALUE_NOT_IN_SHORT_RANGE);
	}
	
	public static final String getViewExceptionString() {
		return messagesResourceBundle.getString(VIEW_EXCEPTION);
	}
	
	public static final String getViewMayNotBeNullString() {
		return messagesResourceBundle.getString(VIEW_MAY_NOT_BE_NULL);
	}
	
	public static final String getWrongZipFilenameFormatString() {
		return messagesResourceBundle.getString(WRONG_ZIP_FILENAME_FORMAT);
	}
	
	public static final String getYearsInvalidString() {
		return messagesResourceBundle.getString(YEARS_INVALID);
	}
	
	public static String getZipFileCouldNotBeCreatedString(String filename) {
		String message = messagesResourceBundle.getString(ZIP_FILE_COULD_NOT_BE_CREATED); 
		return String.format(message, filename);
	}
}