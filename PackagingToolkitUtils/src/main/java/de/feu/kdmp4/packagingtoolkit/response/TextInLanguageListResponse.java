package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Is used for transfering the information about texts in a certain language to the client.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class TextInLanguageListResponse {
	/**
	 * A list with texts in different languages.
	 */
	private List<TextInLanguageResponse> texts;
	
	/**
	 * Initializes the object with an empty array list.
	 */
	public TextInLanguageListResponse() {
		texts = new ArrayList<>();
	}
	
	/**
	 * Adds a text in a certain language to the list.
	 * @param textInLanguageResponse
	 */
	public void addTextInLanguage(final TextInLanguageResponse textInLanguageResponse) {
		texts.add(textInLanguageResponse);
	}
	
	@XmlElement
	public List<TextInLanguageResponse> getTexts() {
		return texts;
	}
	
	public void setTexts(List<TextInLanguageResponse> texts) {
		this.texts = texts;
	}
}
