package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * An exception that is thrown if there were any problems with any object that cannot
 * be assigned to any other class.
 * @author Christopher Olbertz
 *
 */
public class ObjectException extends ProgrammingException {
	// ************* Constants *************
	private static final long serialVersionUID = 3514197737642301592L;
	
	// ************* Constructors ***********
	/**
	 * Constructs an exception.
	 * @param message The message.
	 */
	private ObjectException(String message) {
		super(message);
	}

	// ************* Public methods *********
	
	/**
	 * Throws an exception if an object is null.
	 * @return The exception.
	 */
	public static ObjectException createObjectMayNotBeNullException() {
		String message = I18nExceptionUtil.getObjectMayNotBeNullString();
		return new ObjectException(message);
	}
}
