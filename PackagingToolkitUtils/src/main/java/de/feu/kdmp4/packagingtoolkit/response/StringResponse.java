package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorApiError;

@XmlRootElement
public class StringResponse {
	private MediatorApiError mediatorApiError;
	private String value;
	
	public StringResponse() {
		
	}
	
	public StringResponse(String value) {
		this.value = value;
	}
	
	public StringResponse(MediatorApiError mediatorApiError) {
		this.mediatorApiError = mediatorApiError;
	}

	@XmlElement
	public MediatorApiError getMediatorApiError() {
		return mediatorApiError;
	}

	public void setMediatorApiError(MediatorApiError mediatorApiError) {
		this.mediatorApiError = mediatorApiError;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
