package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A tool a data source is working with for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class DataSourceToolResponse {
	/**
	 * The name of the tool.
	 */
	private String dataSourceToolName;
	/**
	 * True if the tool ist activated, false otherwise.
	 */
	private boolean activated;
	
	public String getDataSourceToolName() {
		return dataSourceToolName;
	}
	
	public void setDataSourceToolName(String dataSourceToolName) {
		this.dataSourceToolName = dataSourceToolName;
	}
	
	public boolean isActivated() {
		return activated;
	}
	
	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activated ? 1231 : 1237);
		result = prime * result + ((dataSourceToolName == null) ? 0 : dataSourceToolName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSourceToolResponse other = (DataSourceToolResponse) obj;
		System.out.println(this.getDataSourceToolName() + "       " + other.dataSourceToolName);
		if (activated != other.activated)
			return false;
		if (dataSourceToolName == null) {
			if (other.dataSourceToolName != null)
				return false;
		} else if (!dataSourceToolName.equals(other.dataSourceToolName)) {
			return false;
		}
			
		return true;
	}
	
	@Override
	public String toString() {
		return dataSourceToolName;
	}
}
