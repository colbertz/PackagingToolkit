package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.constants.MediatorDataConstants;

/**
 * Contains the data extracted by the mediator.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class MediatorDataListResponse {
	/**
	 * The data that are contained in this list.
	 */
	private List<MediatorDataResponse> mediatorData;
	/**
	 * The uuid of the information package these data are assigned to. 
	 */
	private UuidResponse uuidOfInformationPackage;
	/**
	 * The uuid of the file these data are assigned to if any.
	 */
	private UuidResponse uuidOfFile;
	/**
	 * The name of the file these data are assigned to if any. 
	 */
	private String originalFileName;
	
	/**
	 * Creates a new object and initializes the list as an array list.
	 */
	public MediatorDataListResponse() {
		mediatorData = new ArrayList<>();
	}
	
	/**
	 * Adds new data to this list.
	 * @param mediatorDataResponse The data that should be added to the list.
	 */
	public void addMediatorData(final MediatorDataResponse mediatorDataResponse) {
		mediatorData.add(mediatorDataResponse);
	}
	
	public List<MediatorDataResponse> getMediatorData() {
		return mediatorData;
	}
	
	public void setMediatorData(List<MediatorDataResponse> mediatorData) {
		this.mediatorData = mediatorData;
	}
	
	/**
	 * Checks if there is a data record with a  md5 checksum contained in the list.
	 * @return True if there is a data record with a  md5 checksum contained in the list, false otherwise.
	 */
	public boolean containsMd5Checksum() {
		for (MediatorDataResponse mediatorDataResponse: mediatorData) {
			if (mediatorDataResponse.getName().equals(MediatorDataConstants.MD5_CHECKSUM)) {
				return true;
			}
		}
			
		return false;
	}

	@XmlElement
	public UuidResponse getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}
	
	public void setUuidOfInformationPackage(UuidResponse uuid) {
		this.uuidOfInformationPackage = uuid;
	}
	
	@XmlElement
	public UuidResponse getUuidOfFile() {
		return uuidOfFile;
	}
	
	public void setUuidOfFile(UuidResponse uuidOfFile) {
		this.uuidOfFile = uuidOfFile;
	}
	
	@XmlElement
	public String getOriginalFileName() {
		return originalFileName;
	}
	
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}
	
	/**
	 * Creates a string of the following form:
	 * <br />
	 * data1<br/>
	 * data2<br/
	 * data2
	 */
	@Override
	public String toString() {
		final StringBuffer outputString = new StringBuffer();
		for (final MediatorDataResponse mediatorDataResponse: mediatorData) {
			outputString.append(mediatorDataResponse.toString()).append("\n");
		}
		
		return outputString.toString();
	}
}
