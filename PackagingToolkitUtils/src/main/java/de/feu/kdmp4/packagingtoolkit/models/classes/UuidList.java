package de.feu.kdmp4.packagingtoolkit.models.classes;

import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.exceptions.UuidException;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;

/**
 * Encapsulates a list with Uuids.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class UuidList extends AbstractList {
	// *************** Constructors *********************
	/**
	 * This constructor is only for the use through frameworks like JAXB.
	 */
	public UuidList() {
		
	}
	
	//**************** Public methods *******************
	/**
	 * Adds an uuid to the list. 
	 * @param uuid The uuid that should be added to the list.
	 * @throws UuidException Is thrown if the uuid is null or empty.
	 */
	public void addUuidToList(final Uuid uuid) {
		try {
			super.add(uuid);
		} catch (ListException e) {
			UuidException exception = UuidException.uuidMayNotBeEmptyException();
			throw exception;
		}
	}
	
	/**
	 * Checks if the list contains a certain uuid.
	 * @param uuid The uuid we are looking for. 
	 * @return True if the list contains this uuid, false otherwise.
	 */
	public boolean containsUuid(final Uuid uuid) {
		return super.contains(uuid);
	}
	
	/**
	 * Checks if the list does not contain a certain uuid.
	 * @param uuid The uuid we are looking for. 
	 * @return False if the list contains this uuid, true otherwise.
	 */
	public boolean notContainsUuid(final Uuid uuid) {
		return !super.contains(uuid);
	}
	
	/**
	 * Gets an uuid at a given index.
	 * @param index The index of the uuid that should be determined.
	 * @return The found uuid.
	 * @throws ListException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public Uuid getUuidAt(final int index) {
		return (Uuid)super.getElement(index);
	}
	
	/**
	 * Counts the uuids in the list.
	 * @return The number of the uuids in the list.
	 */
	public int getUuidCount() {
		return super.getSize();
	}
	
	/**
	 * Removes an uuid from the list. 
	 * @param uuid The uuid that should be removed.
	 * @throws UuidException Is thrown if the uuid is null.
	 */
	public void removeUuid(final Uuid uuid) {
		try {
			super.remove(uuid);
		} catch (ListException e) {
			UuidException exception = UuidException.uuidMayNotBeEmptyException();
			throw exception;
		}
	}
	
	/**
	 * Converts the uuid list to a response object for sending via http.
	 * @return An object for sending via http.
	 */
	public UuidListResponse toResponse() {
		UuidListResponse response = ResponseModelFactory.getUuidListResponse();
		
		for (int i = 0; i < this.getSize(); i++) {
			Uuid myUuid = this.getUuidAt(i);
			response.addUuidToList(ResponseModelFactory.getUuidResponse(myUuid));
		}
		
		return response;
	}
}
