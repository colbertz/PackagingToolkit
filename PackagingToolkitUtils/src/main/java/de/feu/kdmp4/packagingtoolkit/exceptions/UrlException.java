package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class UrlException extends UserException {
	private static final long serialVersionUID = -1798148958275940798L;

	private UrlException(String message) {
		super(message);
	}
	
	// DELETE_ME
	public static UrlException notAHttpReferenceException(String url) {
		String message = I18nExceptionUtil.getNotAHttpReferenceString();
		message = String.format(message, url);
		return new UrlException(message);
	}

	// DELETE_ME
	public static UrlException notAFileReferenceException(String url) {	
		String message = I18nExceptionUtil.getNotAFileReferenceString();
		message = String.format(message, url);
		return new UrlException(message);
	}

	public static UrlException notAValidUrlException(String url) {
		String message = I18nExceptionUtil.getNotAValidUrlString(url);
		return new UrlException(message);
	}
	
	@Override
	public String getSummary() {
		return I18nExceptionUtil.getSummaryUrlExceptionString();
	}
}
