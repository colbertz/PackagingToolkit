package de.feu.kdmp4.packagingtoolkit.models.interfaces;

/**
 * An interface for all lists used in the packaging toolkit.
 * @author Christopher Olbertz
 *
 */
public interface ListInterface {
	/**
	 * Gets the element at the given index.
	 * @param index The index of the element.
	 * @return The element at the index.
	 */
	public Object getElement(final int index);
	/**
	 * Gets the size of the list.
	 * @return The size of the list.
	 */
	int getSize();
	/**
	 * Checks if the list is empty.
	 * @return True if the list is empty, false otherwise.
	 */
	boolean isEmpty();
	/**
	 * Checks if the list is not empty.
	 * @return True if the list is not empty, false otherwise.
	 */
	boolean isNotEmpty();
}
