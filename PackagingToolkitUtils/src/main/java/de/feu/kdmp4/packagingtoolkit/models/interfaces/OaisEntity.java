package de.feu.kdmp4.packagingtoolkit.models.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * The class that implements this interface is the base class of all 
 * entities in OAIS. 
 * @author christopher
 *
 */
public interface OaisEntity {
	public Uuid getUuid();
}
