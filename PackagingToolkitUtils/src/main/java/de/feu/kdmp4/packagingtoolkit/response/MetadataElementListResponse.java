package de.feu.kdmp4.packagingtoolkit.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.exceptions.response.MetadataElementListResponseException;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

public class MetadataElementListResponse extends AbstractList {
	/**
	 * Returns a metadata element at a given index in the list.
	 * @param index The index.
	 * @return The metadata element at index.
	 * @throws ListException if the index is out of the lists range.
	 */
	public MetadataElementResponse getMetadataAt(int index) {
		return (MetadataElementResponse)super.getElement(index);
	}
	
	/**
	 * Adds a new metadata element to the list.
	 * @param metadata The new metadata element.
	 * @throws MetadataElementListResponseException if metadata is null.
	 */
	public void addMetadata(MetadataElementResponse metadata) {
		try {
			super.add(metadata);
		} catch (ListException e) {
			throw MetadataElementListResponseException.metadataElementMayNotBeNull();
		}
	}
	
	/**
	 * Returns the number of metadata elements in the list.
	 * @return The number of metadata elements in the list.
	 */
	public int getMetadataCount() {
		return super.getSize();
	} 
}