package de.feu.kdmp4.packagingtoolkit.utils;

import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

/**
 * Contains some helper methods for working with HTML:
 * <ul>
 * 	<li>Create a HTML unordered list with the values of a string list.</li>
 * 	<li>Create a HTML ordered list with the values of a string list.</li>
 * 	<li>Create an item of a HTML list with a string.</li>
 * </ul>
 * 
 * @author Christopher Olbertz
 *
 */
public class HtmlUtils {
	/**
	 * The begin tag of an item in a list.
	 */
	private static final String LI_START_TAG = "<LI>";
	/**
	 * The end tag of an item in a list.
	 */
	private  static final String LI_END_TAG = "</LI>";
	/**
	 * The begin tag of an an ordered list.
	 */
	private static final String OL_START_TAG = "<OL>";
	/**
	 * The end tag of an an ordered list.
	 */
	private static final String OL_END_TAG = "</OL>";
	/**
	 * The begin tag of an an unordered list.
	 */
	private static final String UL_START_TAG = "<UL>";
	/**
	 * The end tag of an an unordered list.
	 */
	private static final String UL_END_TAG = "</UL>";

	/**
	 * It should not be possible to create objects of this class. 
	 */
	private HtmlUtils() {
	}
	
	/**
	 * Creates a HTML ordered list with the values of a string list 
	 * @param strings The strings that should be contained in the ordered list.
	 * @return The html ordered list. 
	 */
	public static String createHtmlOrderedList(final StringList strings) {
		final StringBuffer htmlString = new StringBuffer();
		
		htmlString.append(OL_START_TAG);
		
		for (int i = 0; i < strings.getStringCount(); i++) {
			final String aString = strings.getStringAt(i);
			final String item = createHtmlItemOfList(aString);
			htmlString.append(item);
		}
		
		htmlString.append(OL_END_TAG);
		
		return htmlString.toString();
	}
	
	/**
	 * Creates a HTML unordered list with the values of a string list 
	 * @param strings The strings that should be contained in the unordered list.
	 * @return The html unordered list. 
	 */
	public static String createHtmlUnorderedList(final StringList strings) {
		final StringBuffer htmlString = new StringBuffer();
		
		htmlString.append(UL_START_TAG);
		
		for (int i = 0; i < strings.getStringCount(); i++) {
			final String aString = strings.getStringAt(i);
			final String item = createHtmlItemOfList(aString);
			htmlString.append(item);
		}
		
		htmlString.append(UL_END_TAG);
		
		return htmlString.toString();
	}
	
	/**
	 * Creates a HTML item of a list. 
	 * @param string The value of the item.
	 * @return The html itme of a list. 
	 */
	public static String createHtmlItemOfList(final String string) {
		final StringBuffer htmlString = new StringBuffer();
		
		htmlString.append(LI_START_TAG);
		htmlString.append(string);
		htmlString.append(LI_END_TAG);
		
		return htmlString.toString();
	}
}
