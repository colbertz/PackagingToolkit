package de.feu.kdmp4.packagingtoolkit.validators;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;

/**
 * Validates a java.util.List. Contains the following checks:
 * <ul>
 * 	<li>Is a list not null?</li>
 *  <li>Is a list empty?</li>
 *  <li>Is a list not empty?</li>
 *  <li>Is a list nor null neither empty?</li>
 * </ul>Is a index within the range of a list?</li>
 * @author Christopher Olbertz
 *
 */
public class ListValidator {
	/**
	 * Checks, if an index is between 0 and the size of a list. In other words: 
	 * Checks, if an index is valid for a given list, and throws an exception
	 * if the index is not valid.
	 * @param index The index that should be checked.
	 * @param aList The list for that the index should be checked.
	 * @throws ListException Is thrown if the index is not valid.
	 */
	public static final void checkIndex(final int index, final List<? extends Object> aList) {
		if (!ListValidator.isIndexInRange(index, aList)) {
			throw ListException.indexNotInRangeException();
		}
	}
	
	/**
	 * Checks if a list element is null. 
	 * @param element The element that should be checked.
	 * @throws ListException is thrown if the element is null.
	 */
	public static final void checkListElement(final Object element) {
		if (element == null) {
			throw ListException.listElementMayNotBeNullException();
		}
	}
	
	/**
	 * Checks if a list contains elements. 
	 * @param aList The list to check.
	 * @return True, if the list contains elements, false otherwise.
	 */
	public static final boolean isNotEmpty(final List<? extends Object> aList) {
		return !aList.isEmpty();
	}

	/**
	 * Checks if a list is empty. 
	 * @param aList The list to check.
	 * @return True, if the list is empty, false otherwise.
	 */
	public static final boolean isEmpty(final List<? extends Object> aList) {
		return aList.isEmpty();
	}
	
	/**
	 * Checks if a list is not null.
	 * @param aList The list to check.
	 * @return True, if the list is not null, false otherwise.
	 */
	public static final boolean isNotNull(final List<? extends Object> aList) {
		return aList != null;
	}
	
	/**
	 * Checks if a list is neither null nor empty.
	 * @param aList The list to check.
	 * @return True, if the list is not null and contains elements, false otherwise.
	 */
	public static final boolean isNotNullOrEmpty(final List<? extends Object> aList) {
		return isNotNull(aList) && isNotEmpty(aList);
	}
	
	/**
	 * Checks, if an index is between 0 and the size of a list. In other words: 
	 * Checks, if an index is valid for a given list.
	 * @param index The index that has to be valid for the list.
	 * @param aList The list.
	 * @return True, if the index is valid for the list, false otherwise.
	 */
	public static final boolean isIndexInRange(final int index, 
										 final List<? extends Object> aList) {
		if (index < 0 || index >= aList.size()) {
			return false;
		} else {
			return true;
		}
	}
}
