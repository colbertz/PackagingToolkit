package de.feu.kdmp4.packagingtoolkit.models.classes;

import static de.feu.kdmp4.packagingtoolkit.validators.NumberValidator.isNegative;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyDurationException;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class OntologyDurationImpl implements OntologyDuration {
	// ************** Constants **************
	public static final String DAYS_MARKER = "D";
	public static final String HOURS_MARKER = "H";
	public static final String MINUTES_MARKER = "M";
	public static final String MONTHS_MARKER = "M";
	public static final String NEGATIVE_MARKER = "-";
	public static final String PERIOD_MARKER = "P";
	public static final String SECONDS_MARKER = "S";
	public static final String TIME_MARKER = "T";
	public static final String YEARS_MARKER = "Y";
	
	// ************* Attributes **************
	private int days;
	private int hours;
	private int minutes;
	private int months;
	private boolean negative;
	private int seconds;
	private int years;
	
	public static void main(String [] args) throws DatatypeConfigurationException{
		DatatypeFactory df = DatatypeFactory.newInstance();
		Duration d = df.newDuration("-P50Y10M");
		System.out.println(d.getYears());
		System.out.println(d.getMonths());
		System.out.println(d.getDays());
		System.out.println(d.getSign());
	}
	
	// ************ Constructors *************
	public OntologyDurationImpl(int days, int hours, int minutes, int months, 
			int seconds, int years) {
		this(days, hours, minutes, months, seconds, years, false);
	}
	
	public OntologyDurationImpl(int days, int hours, int minutes, int months, 
			int seconds, int years, boolean negative) {
		super();
		
		if (isNegative(days)) {
			OntologyDurationException exception = OntologyDurationException.daysMayNotBeNegative(days);
			throw exception;
		}
		this.days = days;
		if (isNegative(hours)) {
			OntologyDurationException exception = OntologyDurationException.hoursMayNotBeNegative(hours);
			throw exception;
		}
		this.hours = hours;
		if (isNegative(minutes)) {
			OntologyDurationException exception = OntologyDurationException.minutesMayNotBeNegative(minutes);
			throw exception;
		}
		this.minutes = minutes;
		if (isNegative(months)) {
			OntologyDurationException exception = OntologyDurationException.monthsMayNotBeNegative(months);
			throw exception;
		}
		this.months = months;
		if (isNegative(seconds)) {
			OntologyDurationException exception = OntologyDurationException.secondsMayNotBeNegative(seconds);
			throw exception;
		}
		this.seconds = seconds;
		if (isNegative(years)) {
			OntologyDurationException exception = OntologyDurationException.yearsMayNotBeNegative(years);
			throw exception;
		}
		this.years = years;
		this.negative = negative;
	}
	
	// ******** Public methods **********
	/**
	 * Returns a string that represents the duration value as a value valid for the xsd datatype
	 * Duration. 
	 * <br />
	 * Example: A duration of two years, one month and three days is represented as: P2Y1M3D. 
	 */
	@Override
	public String toString() {
		// For avoiding a lonely P in the output if all values are 0.
		if (years == 0 && months == 0 && days == 0 && hours == 0 && minutes == 0 && seconds == 0) {
			return StringUtils.EMPTY_STRING;
		}
		
		String durationString = "";
		if (negative) {
			durationString = durationString + NEGATIVE_MARKER;
		}
		
		durationString = durationString + PERIOD_MARKER;
		
		if (years > 0) {
			durationString = durationString + years + YEARS_MARKER;
		}
		
		if (months > 0) {
			durationString = durationString + months + MONTHS_MARKER;
		}
		
		if (days > 0) {
			durationString = durationString + days + DAYS_MARKER;
		}
		
		// Now we check if a time section begins.
		if (hours > 0 || minutes > 0 || seconds > 0) {
			durationString = durationString + TIME_MARKER;
			if (hours > 0) {
				durationString = durationString + hours + HOURS_MARKER;
			}
			
			if (minutes > 0) {
				durationString = durationString + minutes + MINUTES_MARKER;
			}
			
			if (seconds > 0) {
				durationString = durationString + seconds + SECONDS_MARKER;
			}
		}
		
		
		return durationString;
	}

	// ************* Getters ************
	@Override
	public int getDays() {
		return days;
	}

	@Override
	public int getHours() {
		return hours;
	}

	@Override
	public int getMinutes() {
		return minutes;
	}

	@Override
	public int getMonths() {
		return months;
	}

	@Override
	public boolean isNegativeDuration() {
		return negative;
	}

	@Override
	public int getSeconds() {
		return seconds;
	}
	
	@Override
	public int getYears() {
		return years;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + days;
		result = prime * result + hours;
		result = prime * result + minutes;
		result = prime * result + months;
		result = prime * result + (negative ? 1231 : 1237);
		result = prime * result + seconds;
		result = prime * result + years;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyDurationImpl other = (OntologyDurationImpl) obj;
		if (days != other.days)
			return false;
		if (hours != other.hours)
			return false;
		if (minutes != other.minutes)
			return false;
		if (months != other.months)
			return false;
		if (negative != other.negative)
			return false;
		if (seconds != other.seconds)
			return false;
		if (years != other.years)
			return false;
		return true;
	}
}
