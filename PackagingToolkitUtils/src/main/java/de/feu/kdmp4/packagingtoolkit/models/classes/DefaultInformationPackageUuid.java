package de.feu.kdmp4.packagingtoolkit.models.classes;

/**
 * This class represents a default Uuid. It is used as uuid of an information package if
 * the element does not belong to any information package. An element like an individual does
 * not belong to an information package if it is used in the base ontology. The elements in the
 * base ontology are not assigned to any information package.
 * <br />
 * It would be possible to save a null value in the element but the use of null values in the
 * application should be reduced as much as possible. This class extends Uuid in the case that
 * there is no uuid and it should avoid the use of null and reduce the danger of NullPointerExceptions.
 * <br />
 * This is a practial use of the null object pattern.
 * 
 * @author Christopher Olbertz
 *
 */
public class DefaultInformationPackageUuid extends Uuid {
	
}
