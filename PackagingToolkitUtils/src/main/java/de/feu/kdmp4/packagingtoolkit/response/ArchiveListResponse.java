package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.exceptions.response.ArchiveListResponseException;


/**
 * Encapsulates a list with archives that are send via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class ArchiveListResponse implements Serializable { 
	// *************** Constants ************************
	private static final long serialVersionUID = -4396824977427743225L;
	/**
	 * The list with the archives for sending via http.
	 */
	private List<ArchiveResponse> archives;
	
	/**
	 * Creates a new archive list response with an empty array list.
	 */
	public ArchiveListResponse() {
		archives = new ArrayList<>();
	}
	
	//**************** Public methods *******************
	/**
	 * Adds an archive to the list. With the help of the uuid is checked if
	 * the archive is already in the list.
	 * @param archive The archive that should be added to the list.
	 * @throws ArchiveListException Is thrown if the archive is null or if 
	 * an archive with the same uuid is already in the archive list.
	 */
	public void addArchiveToList(final ArchiveResponse archive) {
		UuidResponse uuid = new UuidResponse(archive.getUuid());
		checkUuidInArchiveList(uuid);
		
		try {
			archives.add(archive);
		} catch (ListException e) {
			throw ArchiveListResponseException.archiveMayNotBeNull();
		}
	}
	
	/**
	 * Gets an archive at a given index.
	 * @param index The index of the archive that should be determined.
	 * @return The found archive.
	 * @throws ListException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public ArchiveResponse getArchiveAt(final int index) {
		return archives.get(index);
	}
	
	/**
	 * Counts the archives in the list.
	 * @return The number of the archives in the list.
	 */
	public int getArchiveCount() {
		return archives.size();
	}
	
	public boolean isEmpty() {
		return archives.isEmpty();
	}
	
	public boolean isNotEmpty() {
		return !archives.isEmpty();
	}
	
	/**
	 * Removes an archive from the list. 
	 * @param archive The archive that should be removed.
	 * @throws ArchiveException Is thrown if the archive is null.
	 */
	public void removeArchive(final ArchiveResponse archive) {
		try {
			archives.remove(archive);
		} catch (ListException e) {
			throw ArchiveListResponseException.archiveMayNotBeNull();
		}
	}
	
	@XmlElement
	public List<ArchiveResponse> getArchives() {
		return archives;
	}
	
	public void setArchives(List<ArchiveResponse> archives) {
		this.archives = archives;
	}
	
	// ************* Private methods *****************************
	/**
	 * Checks, if an information package with a given uuid is
	 * already in the list. Throws an UuidAlreadyInArchiveListException
	 * when the uuid is found.
	 * @param uuid The uuid to look for.
	 * @throws ArchiveListException is thrown if the uuid is already in the 
	 * archive list.
	 * @throws ListException is thrown if there is an index used in the method
	 * that is not valid for this list.
	 */
	private void checkUuidInArchiveList(final UuidResponse uuid) {
		for (int i = 0; i < getArchiveCount(); i++) {
			ArchiveResponse archive = archives.get(i);
			if (archive.areUuidsEqual(uuid)) {
				throw ArchiveListResponseException.uuidAlreadyInArchive(uuid);
			}
		}
	}
}
