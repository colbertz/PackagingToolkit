package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with digitals objects for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class DigitalObjectListResponse implements Serializable { 
	// *************** Constants ***********************
	private static final long serialVersionUID = 6050213974271592449L;
	
	/**
	 * Contains the digital objects.
	 */
	private List<DigitalObjectResponse> digitalObjectList;
	
	/**
	 * Creates a new object and initializes the list as array list.
	 */
	public DigitalObjectListResponse() {
		digitalObjectList = new ArrayList<>();
	}
	
	/**
	 * Adds an digitalObject to the list. 
	 * @param digitalObject The digitalObject that should be added to the list.
	 */
	public void addDigitalObjectToList(final DigitalObjectResponse digitalObject) {
		digitalObjectList.add(digitalObject);
	}
	
	/**
	 * Gets an digitalObject at a given index.
	 * @param index The index of the digitalObject that should be determined.
	 * @return The found digitalObject.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public DigitalObjectResponse getDigitalObjectAt(final int index) {
		return digitalObjectList.get(index);
	}
	
	/**
	 * Counts the digitalObjects in the list.
	 * @return The number of the digitalObjects in the list.
	 */
	public int getDigitalObjectCount() {
		return digitalObjectList.size();
	}

	@XmlElement
	public List<DigitalObjectResponse> getDigitalObjectList() {
		return digitalObjectList;
	}

	public void setDigitalObjectList(List<DigitalObjectResponse> digitalObjectList) {
		this.digitalObjectList = digitalObjectList;
	}
}
