package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a data record that has been extracted by the mediator.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class MediatorDataResponse {
	/**
	 * The name of this record.
	 */
	private String name;
	/**
	 * The value of this record.
	 */
	private String value;
	
	public MediatorDataResponse() {
	}
	
	public MediatorDataResponse(final String name, final String value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Creates a string in the following form:
	 * <br />
	 * name: value
	 */
	@Override
	public String toString() {
		return name + ": " + value;
	}
}
