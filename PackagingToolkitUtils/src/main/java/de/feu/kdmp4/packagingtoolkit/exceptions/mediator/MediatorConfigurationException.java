package de.feu.kdmp4.packagingtoolkit.exceptions.mediator;

import java.io.IOException;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * Exception that is thrown if there is any issue with the configuration of the mediator. If this exception is been
 * triggered by catching another exception and this original exception contains any information of value it can
 * be stored in this exception too. This can be for example an IOException or an exception thrown by a data
 * source.
 * @author Christopher Olbertz
 *
 */
public class MediatorConfigurationException extends UserException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5847553940822290511L;
	/**
	 * The original exception. This can be for example an IOException or an exception thrown by a data
	 * source.
	 */
	private Exception originalException;
	
	/**
	 * Constructs a new exception.
	 * @param originalException The original exception that contains any information of value.
	 * @param theMessage The message that describes the exception.
	 */
	private MediatorConfigurationException(final Exception originalException, String theMessage) {
		super(theMessage);
		this.originalException = originalException;
	}
	
	/**
	 * Constructs a new exception.
	 * @param theMessage The message that describes the exception.
	 */
	private MediatorConfigurationException(final String theMessage) {
		super(theMessage);
	}

	/**
	 * Is thrown if the configuration file of the mediator does not exist.
	 * @param configurationFileName The name of the configuration file that does not exist.
	 * @return The exception.
	 */
	public static MediatorConfigurationException createDoesNotExistException(
			final String configurationFileName) {
		String message = I18nExceptionUtil.getConfigurationFileDoesNotExistString();
		message = String.format(message, configurationFileName);
		return new MediatorConfigurationException(message);
	}

	/**
	 * This exception is thrown if a general IOException has been thrown that cannot be 
	 * described with the other cases. 
	 * @param originalException The original exception is wrapped into this exception. 
	 * @param configurationFileName The name of the configuration file.
	 * @return The exception.
	 */
	public static MediatorConfigurationException createGeneralIOException(
			final IOException originalException, final String configurationFileName) {
		String message = I18nExceptionUtil.getGeneralIOExceptionString();
		message = String.format(message, configurationFileName);
		return new MediatorConfigurationException(originalException, message);
	}
	
	/**
	 * Is thrown if the configuration file was not readable.
	 * @param configurationFileName The name of the configuration file.
	 * @return The exception.
	 */
	public static MediatorConfigurationException createNotReadableException(
			final String configurationFileName) {
		String message = I18nExceptionUtil.getConfigurationFileNotReadableString();
		message = String.format(message, configurationFileName);
		return new MediatorConfigurationException(message);
	}

	/**
	 * Is thrown if the configuration file was not writable
	 * @param configurationFileName The name of the configuration file.
	 * @return The exception.
	 */
	public static MediatorConfigurationException createNotWritableException(
			final String configurationFileName) {
		String message = I18nExceptionUtil.getConfigurationFileNotWritableString();
		message = String.format(message, configurationFileName);
		return new MediatorConfigurationException(message);
	}
	
	/**
	 * Creates an exception that is thrown if a data source has not been configured correctly.
	 * @param dataSourceName The name of the data source.
	 * @param originalException The exception that has been thrown by the data source itself and must be converted
	 * into a general exception.
	 * @return The exception.
	 */
	public static MediatorConfigurationException createDataSourceConfigurationException(
			final String dataSourceName, final Exception originalException) {
		String originalMessage = originalException.getMessage();
		String message = I18nExceptionUtil.getDataSourceNotCorrectlyConfiguredString(dataSourceName, originalMessage);
		message = String.format(message, originalException);
		return new MediatorConfigurationException(originalException, message);
	}
	
	/**
	 * Creates an exception that is thrown if a data source has not been configured correctly.
	 * @param message The message that should describe the error.
	 * @return The exception.
	 */
	public static MediatorConfigurationException createDataSourceConfigurationException(
			final String message) {
		return new MediatorConfigurationException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the fits installation directory is not correct. The fits installation
	 * directory must contain the directories lib and xml.
	 * @param wrongFitsPath The name of wrong directory.
	 * @return The exception.
	 */
	public static MediatorConfigurationException createWrongFitsPathException(
			final String wrongFitsPath) {
		String message = I18nExceptionUtil.getFitsInstallationPathNotCorrect(wrongFitsPath);
		return new MediatorConfigurationException(message);
	}
	
	@Override
	public String getMessage() {
		String message = super.getMessage(); 
		return message;
	}

	public Exception getOriginalException() {
		return originalException;
	}

	@Override
	public String getSummary() {
		return I18nExceptionUtil.getSummaryMediatorConfigurationExceptionString();
	}
}