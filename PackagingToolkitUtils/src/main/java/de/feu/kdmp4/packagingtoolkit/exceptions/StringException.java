package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * <p>An exception that is thrown if there are general problems with strings. This
 * exception is thrown for the following causes:</p>
 * <ul>
 * 	<li>A string array is null.</li>
 * 	<li>A string is empty.</li>
 * </ul>
 * 
 * @author Christopher Olbertz
 *
 */
public final class StringException extends ProgrammingException {
	// *************** Constants *************
	private static final long serialVersionUID = -7562336262766206646L;

	// *************** Constructors **********
	/**
	 * Creates a StringException.
	 * @param message The message for this exception.
	 */
	private StringException(String message) {
		super(message);
	}

	// *************** Public methods ********
	/**
	 * Creates an exception if a string array is null.
	 * @return The exception.
	 */
	public static final StringException stringArrayMayNotBeNullException() {
		String message = I18nExceptionUtil.getArrayMayNotBeNullString();
		return new StringException(message);
	}
	
	/**
	 * Creates an exception if a string is null or empty.
	 * @return The exception.
	 */
	public static final StringException stringMayNotBeEmptyException() {
		String message = I18nExceptionUtil.getStringMayNotBeEmptyString();
		return new StringException(message);
	}
}
