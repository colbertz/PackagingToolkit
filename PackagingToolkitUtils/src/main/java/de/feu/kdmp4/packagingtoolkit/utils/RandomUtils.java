package de.feu.kdmp4.packagingtoolkit.utils;

import java.util.Random;

/**
 * Contains methods for creating random values and encapsulates the respective mechanism.
 * @author Christopher Olbertz
 *
 */
public abstract class RandomUtils {
	private static final Random random;
	
	static {
		random = new Random();
	}
	
	 /** 
	 * Creates a random double value starting with 0 to max.
	 * @param max The value the random number can upmost be.
	 * @return The created random number.
	 */
	public static double nextDouble(double max) {
		return random.nextDouble() * max;
	}
	
	/**
	 * Creates a random int value starting with 0 to max.
	 * @param max The value the random number can upmost be.
	 * @return The created random number.
	 */
	public static int nextInt(int max) {
		return random.nextInt(max);
	}
	
	/**
	 * Creates a random long value.
	 * @return The created random number.
	 */
	public static long nextLong() {
		return random.nextLong();
	}
}
