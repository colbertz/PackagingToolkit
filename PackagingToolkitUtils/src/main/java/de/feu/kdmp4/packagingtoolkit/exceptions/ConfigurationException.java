package de.feu.kdmp4.packagingtoolkit.exceptions;

import java.io.IOException;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * This exception is thrown if there are any problems with the configuration. The
 * message is shown to the user.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationException extends UserException {
	// *************** Constants ******************
	private static final long serialVersionUID = 9105369727995545817L;
	private static final String SUMMARY = I18nExceptionUtil.getSummaryConfigurationExceptionString();
	
	// *************** Attributes *****************
	/**
	 * Original exception if the cause was an IOException.
	 */
	private IOException originalIOException;
	
	// *************** Constructors ***************
	/**
	 * Constructs an exception.
	 * @param originalException The original exception that has caused this exception.
	 * @param message The message.
	 */
	private ConfigurationException(final IOException originalException,
									   final String theMessage) { 
		super(theMessage);
		this.originalIOException = originalException;
	}
	
	/**
	 * Constructs an exception.
	 * @param message The message.
	 */
	private ConfigurationException(final String theMessage) {
		super(theMessage);
	}

	// *********** Public methods *************
	/**
	 * Throws an exception if the base ontology on the server is not writable. 
	 * @param originalException The original exception.
	 * @return The new exception.
	 */
	public static ConfigurationException baseOntologyNotWritableException(
			final IOException originalException) {
		String message = I18nExceptionUtil.getBaseOntologyNotWritableString();
		return new ConfigurationException(originalException, message);
	}
	
	/**
	 * Throws an exception if the configuration file does not exist.
	 * @param configurationFileName The name of the configuration file that does not exist. 
	 * @return The new exception.
	 */
	public static ConfigurationException doesNotExistException(
			final String configurationFileName) {
		String message = I18nExceptionUtil.getConfigurationFileDoesNotExistString();
		message = String.format(message, configurationFileName);
		return new ConfigurationException(message);
	}
	
	/**
	 * Throws an exception. The cause is an IOException that can not be  assigned to
	 * a special cause.
	 * @param originalException The original exception.
	 * @param configurationFileName The name of the configuration file that caused the error. 
	 * @return The new exception.
	 */
	public static ConfigurationException generalIOException(
			final IOException originalException,
			final String configurationFileName) {
		String message = I18nExceptionUtil.getGeneralIOExceptionString();
		message = String.format(message, configurationFileName);
		return new ConfigurationException(originalException, message);
	}
	
	/**
	 * Throws an exception if the configuration file is not writable.
	 * @param configurationFileName The name of the configuration file that cannot be written. 
	 * @return The new exception.
	 */
	public static ConfigurationException notWritableException(
			final String configurationFileName) {
		String message = I18nExceptionUtil.getConfigurationFileNotWritableString();
		message = String.format(message, configurationFileName);
		return new ConfigurationException(message);
	}

	/**
	 * Throws an exception if the configuration file is not readable.
	 * @param configurationFileName The name of the configuration file that cannot be read. 
	 * @return The new exception.
	 */
	public static ConfigurationException notReadableException(
			final String configurationFileName) {
		String message = I18nExceptionUtil.getConfigurationFileNotReadableString();
		message = String.format(message, configurationFileName); 
		return new ConfigurationException(message);
	}
	
	/**
	 * Throws an exception if the rule ontology on the server is not writable. 
	 * @param originalException The original exception.
	 * @return The new exception.
	 */
	public static ConfigurationException ruleOntologyNotWritableException(
			final IOException originalException) {
		String message = I18nExceptionUtil.getRuleOntologyNotWritableString();
		return new ConfigurationException(originalException, message);
	}

	/**
	 * Gets the original exception that cause this exception.
	 * @return The original exception.
	 */
	public IOException getOriginalIOException() {
		return originalIOException;
	}

	/**
	 * The summary message for this exception.
	 * @return The summary message for this exception. 
	 */
	@Override
	public String getSummary() {
		return SUMMARY;
	}
}