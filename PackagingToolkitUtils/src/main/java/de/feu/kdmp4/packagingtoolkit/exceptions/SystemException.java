package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class SystemException extends ProgrammingException {
	private SystemException(String message) {
		super(message);
	}
	
	public static SystemException createAbsoluteFilepathNotSupportedUnderWindows() {
		String message = I18nExceptionUtil.getMethodNotSupportedUnderWindowsString();
		return new SystemException(message);
	}
}
