package de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class ViewException extends UserException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7213390698346545495L;
	private static final String SUMMARY = I18nExceptionUtil.getViewExceptionString();
	
	public ViewException(String message) {
		super(message);		
	}

	public static ViewException duplicateViewNameException(final String viewName) {
		String message = I18nExceptionUtil.getDuplicateViewNameString(viewName);
		return new ViewException(message);
	}
	
	@Override
	public String getSummary() {
		return SUMMARY;
	}
}
