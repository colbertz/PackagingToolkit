package de.feu.kdmp4.packagingtoolkit.validators;

import de.feu.kdmp4.packagingtoolkit.exceptions.ObjectException;

/**
 * Contains methods for validating objects. The class only contains methods
 * for die Superclass Object not for any other classes.
 * @author Christopher Olbertz
 */
public class ObjectValidator {
	/**
	 * Checks if an object is null.
	 * @param object The object to check.
	 * @throws ObjectException Is thrown if an object is null.
	 */
	public static final void checkIsNull(Object object) {
		if (!isNotNull(object)) {
			throw ObjectException.createObjectMayNotBeNullException();
		}
	}
	
	/**
	 * Checks if an object is not null.
	 * @param object The object to check.
	 * @return True, if the object is not null, false otherwise.
	 */
	public static final boolean isNotNull(Object object) {
		return object != null;
	}
}
