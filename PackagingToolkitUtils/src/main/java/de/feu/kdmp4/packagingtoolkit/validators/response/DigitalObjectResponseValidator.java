package de.feu.kdmp4.packagingtoolkit.validators.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.response.DigitalObjectResponseException;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * <p>Checks an object from the type {@link de.feu.kdmp4.packagingtoolkit.utils.response.DigitalObjectResponse}.
 * Every check has to methods. All methods beginning with the suffix is... return true or false and
 * all methods with the suffix check... throw an exception.</p>
 * <p>Contains the following checks:
 * <ul>
 * 	<li>If a UuidResponse object is null.</li>
 * </ul>
 * </p>
 * 
 * @author Christopher Olbertz
 *
 */
public abstract class DigitalObjectResponseValidator {
	// ************** Methods returning a boolean value **************
	/**
	 * Checks if a filename is valid. It is not valid if it is null.
	 * @param filename The file name that should be checked.
	 * @return True if filename is valid, false otherwise.
	 */
	public static final boolean isFilenameValid(String filename) {
		if (StringValidator.isNullOrEmpty(filename)) {
			return false;
		} else {
			return true;
		}
	}
	
	// ************ Methods throwing an exception *****************
	/**
	 * Checks if a filename is valid. It is not valid if it is null.
	 * @param filename The file name that should be checked.
	 * @throws DigitalObjectResponseException if filename is not valid.
	 */
	public static final void filenameValid(String filename) {
		if (StringValidator.isNullOrEmpty(filename)) {
			throw DigitalObjectResponseException.filenameMayNotBeEmpty();
		}
	}
}
