package de.feu.kdmp4.packagingtoolkit.utils;

import java.util.UUID;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.StringException;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.ListInterface;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Contains some constants and methods for working with strings.
 * <br />
 * Constants for:
 * <ul>
 * 	<li>empty string</li>
 *  <li>underscore</li>
 * </ul>
 * Methods:
 * <ul>
 * 	<li>Removing all spaces from a string.</li>
 * 	<li>Splitting a string at the underscores.</li>
 * </ul>
 * @author Christopher Olbertz
 *
 */
public class StringUtils {
	public static final String EMPTY_STRING = "";
	public static final String UNDERSCORE = "_";
	public static final String SHARP = "#";
	public static final String DOUBLE_SHARP = "##";
	public static final String SLASH = "/";
	public static final String LEFT_PARENTHESE = "(";
	public static final String RIGHT_PARENTHESE = ")";
	
	public static boolean containsSharp(final String theString) {
		return theString.contains(SHARP);
	}
	
	/**
	 * Gets a text that contains parentheses. All text between the parentheses and the
	 * parentheses themselves are deleted. If the text does not contain any parentheses it
	 * is not modified. Only recognizes a pair of parentheses.
	 * <br />
	 * Example
	 * <br />
	 * The parameter is: "This text contains some (parentheses)"
	 * <br />
	 * The result is:   "This text contains some"
	 * @param textWithParentheses The text that can contain parentheses.
	 * @return The text without the string in parentheses if there were any.
	 */
	public static String deleteTextInParentheses(final String textWithParentheses) {
		if (StringValidator.isNotNullOrEmpty(textWithParentheses)) {
			final int posOfLeftParenthese = textWithParentheses.indexOf(LEFT_PARENTHESE);
			String textWithoutParentheses = textWithParentheses;
			
			if (posOfLeftParenthese >= 0) {
				final int posOfRightParenthese = textWithParentheses.indexOf(RIGHT_PARENTHESE);
				if (posOfRightParenthese > posOfLeftParenthese) {
					textWithoutParentheses = textWithParentheses.substring(0, posOfLeftParenthese);
					textWithoutParentheses = textWithoutParentheses + textWithParentheses.substring(posOfRightParenthese + 1);
				}
			}
			
			return textWithoutParentheses.trim();
		} else {
			return StringUtils.EMPTY_STRING;
		}
	}
	
	/**
	 * If there are two sharp symbols in this string one after another, one of these two sharps 
	 * is deleted.
	 * @param theString The string that may contain two sharp symbols after another.
	 * @return The string with only one sharp symbol.
	 */
	public static String deleteDoubleSharps(final String theString) {
		String resultString = theString;
		if (StringValidator.isNotNullOrEmpty(resultString)) {
			if (resultString.contains(DOUBLE_SHARP)) {
				int positionFirstSharp = resultString.indexOf(DOUBLE_SHARP);
				resultString = resultString.substring(0, positionFirstSharp);
				resultString = resultString + theString.substring(positionFirstSharp + 1);
			}
		}
		
		return resultString;
	}
	
	/**
	 * Extracts the local name from an iri. An iri consists of a namespace and a local name. They are separated
	 * by a # symbol. The method returns the local name after the # symbol. If the parameter is null or empty
	 * a random long value is returned. 
	 * @param iri The iri.
	 * @return The local name in this iri. If is is not an iri with a # symbol, the parameter is simply returned.
	 */
	public static String extractLocalNameFromIri(final String iri) {
		if (iri != null) {
			if (iri.contains(PackagingToolkitConstants.NAMESPACE_SEPARATOR)) {
				final String[] splitted = iri.split(PackagingToolkitConstants.NAMESPACE_SEPARATOR);
				return splitted[1];
			} else {
				return iri;
			}
		} 
		return StringUtils.EMPTY_STRING;
	}
	
	/**
	 * Deletes the last character of a string.
	 * @param aString The string we want delete the last character from.
	 * @return The string without the last character.
	 */
	public static String deleteLastCharacter(final String aString) {
		final int charCount = aString.length();
		final String string = aString.substring(0, charCount - 1);
		return string;
	}
	
	/**
	 * Replaces the last occurence of a character by another character.
	 * @param aString The string that contains the character that should be replaced.
	 * @param characterToReplace The character that should be replaced.
	 * @param newCharacter The character that replaces characterToReplace.
	 * @return The string with the replaced character.
	 */
	public static String replaceLast(final String aString, final char characterToReplace, final char newCharacter) {
		final int lastIndexOfCharacter = aString.lastIndexOf(characterToReplace);
		if (lastIndexOfCharacter > 0) {
			final String lastPartOfString = aString.substring(lastIndexOfCharacter + 1);
			String string = aString.substring(0, lastIndexOfCharacter);
			string = string + newCharacter + lastPartOfString;
			return string;
		}
		return aString;
	}
	
	/**
	 * Extracts the name space from an iri. An iri consists of a namespace and a local name. They are separated
	 * by a # symbol. The method returns the name space before the # symbol.
	 * @param iri The iri.
	 * @return The name space in this iri. If is is not an iri with a # symbol, the parameter is simply returned.
	 */
	public static String extractNamespaceFromIri(final String iri) {
		if (iri != null) {
			if (iri.contains(PackagingToolkitConstants.NAMESPACE_SEPARATOR)) {
				final String[] splitted = iri.split(PackagingToolkitConstants.NAMESPACE_SEPARATOR);
				return splitted[0];
			} else {
				return iri;
			}
		}
		return StringUtils.EMPTY_STRING;
	}
	
	/**
	 * Removes all spaces from a string.
	 * @param theString The string whose spaces should be removed.
	 * @return The string with the spaces.
	 */
	public static String deleteSpaces(final String theString) {
		String aString = theString.trim();
		String[] strings = aString.split(" ");
		String stringWithoutSpaces = "";
		
		for (String string: strings) {
			stringWithoutSpaces = stringWithoutSpaces + string;
		}
		
		return stringWithoutSpaces;
	}
	
	public static String createCommaSeperatedList(final ListInterface theList) {
		final StringBuffer stringBuffer = new StringBuffer();
		
		for (int i = 0; i < theList.getSize(); i++) {
			final Object object = theList.getElement(i);
			stringBuffer.append(object.toString());
			stringBuffer.append(PackagingToolkitConstants.COMMA);
		}
		
		// Remove the last comma.
		if (stringBuffer.length() > 0) {
			stringBuffer.deleteCharAt(stringBuffer.length() - 1);
		}
		
		return stringBuffer.toString().trim();
	}
	
	public static StringList splitCommaSeparatedList(final String commaSeperatedString) {
		if (StringValidator.isNullOrEmpty(commaSeperatedString)) {
			throw StringException.stringMayNotBeEmptyException();
		}
		final String[] strings = commaSeperatedString.split(PackagingToolkitConstants.COMMA);
		return new StringList(strings);
	}
	
	public static StringList splitLineFeedSeparatedString(final String lineFeedSeperatedString) {
		if (StringValidator.isNullOrEmpty(lineFeedSeperatedString)) {
			throw StringException.stringMayNotBeEmptyException();
		}
		String[] strings = lineFeedSeperatedString.split(PackagingToolkitConstants.LINE_FEED);
		return new StringList(strings);
	}
	
	/**
	 * Checks if a string contains a valid uuid. Tries to create an UUID object from the string. If 
	 * an IllegalArgumentException is thrown, the string does not contain a valid uuid.
	 * @param theString The string to check.
	 * @return True, if theString contains a valid uuid, false otherwise.
	 */
	public static boolean isUuid(final String theString) {
		try {
			UUID.fromString(theString);
			return true;
		} catch (IllegalArgumentException ex) {
			return false;
		}
	}
	
	/**
	 * If the given string ends with a sharp symbol, the sharp is removed from the string.
	 * @param theString That possibly ends with a sharp.
	 * @return The string without the sharp. If it does not end with a sharp, the string is not
	 * modified.
	 */
	public static String trimLastSharp(final String theString) {
		if (theString.endsWith(PackagingToolkitConstants.NAMESPACE_SEPARATOR)) {
			String aString = theString.substring(0, theString.length() - 1);
			return aString;
		}
		
		return theString;
	}
	
	/**
	 * Splits a string at the underscores.
	 * @param theString The string to split.
	 * @return An array with the splitted strings.
	 */
	public static String[] splitAtUnderscore(final String theString) {
		if (StringValidator.isNullOrEmpty(theString)) {
			throw StringException.stringMayNotBeEmptyException();
		}
		
		return theString.split(UNDERSCORE);
	}
	
	/**
	 * Compares two strings. When you use this method for the comparison of two strings you cannot use by accident the 
	 * operator == instead of equals().
	 * @param string1 The first string for the comparison.
	 * @param string2 The string string1 is compared with.
	 * @return True, if the two strings are equal, false otherwise.
	 */
	public static boolean areStringsEqual(final String string1, final String string2) {
		if (string1.equals(string2)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Compares two strings if they are not equal. When you use this method for the comparison of two strings you cannot use 
	 * by accident the operator == instead of equals() and you cannot forget the ! in the condition.
	 * @param string1 The first string for the comparison.
	 * @param string2 The string string1 is compared with.
	 * @return True, if the two strings are not equal, false otherwise.
	 */
	public static boolean areStringsUnequal(final String string1, final String string2) {
		if (!string1.equals(string2)) {
			return true;
		} else {
			return false;
		}
	}
}
