package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains the configuration data of the server that should be send to the client.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement(name = "ServerConfigurationDataResponse")
public class ServerConfigurationDataResponse {
	/**
	 * The path of the base ontology on the server.
	 */
	private String configuredBaseOntologyPath;
	/**
	 * The path of the rule ontology on the server.
	 */
	private String configuredRuleOntologyPath;
	/**
	 * The path where the server saves the files that are needed for its work.
	 */
	private String workingDirectoryPath;
	/**
	 * The name of the class in the base ontology where the processing of the class hierarchy starts.
	 */
	private String baseOntologyClass;

	/**
	 * Creates a new object. May only be called by frameworks.
	 */
	public ServerConfigurationDataResponse() {
	}

	/**
	 * 
	 * @param configuredBaseOntologyPath The path of the base ontology on the server.
	 * @param configuredRuleOntologyPath The path of the rule ontology on the server.
	 * @param workingDirectoryPath The path where the server saves the files that are needed for its work.
	 * @param baseOntologyClass The name of the class in the base ontology where the processing of the class hierarchy starts.
	 * @param configuredPremisOntologyPath The path of the Premis ontology on the server.
	 * @param configuredSkosOntologyPath The path of the SKOS ontology on the server.
	 */
	public ServerConfigurationDataResponse(final String configuredBaseOntologyPath,
			final String configuredRuleOntologyPath, 
			final String workingDirectoryPath, final String baseOntologyClass) {
		this.configuredBaseOntologyPath = configuredBaseOntologyPath;
		this.configuredRuleOntologyPath = configuredRuleOntologyPath;
		this.workingDirectoryPath = workingDirectoryPath;
		this.baseOntologyClass = baseOntologyClass;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((workingDirectoryPath == null) ? 0 : workingDirectoryPath.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServerConfigurationDataResponse other = (ServerConfigurationDataResponse) obj;
		if (workingDirectoryPath == null) {
			if (other.workingDirectoryPath != null)
				return false;
		} else if (!workingDirectoryPath.equals(other.workingDirectoryPath))
			return false;
		return true;
	}

	public String getWorkingDirectoryPath() {
		return workingDirectoryPath;
	}

	public void setWorkingDirectoryPath(String workingDirectoryPath) {
		this.workingDirectoryPath = workingDirectoryPath;
	}

	public String getConfiguredRuleOntologyPath() {
		return configuredRuleOntologyPath;
	}

	public void setConfiguredRuleOntologyPath(String configuredRuleOntologyPath) {
		this.configuredRuleOntologyPath = configuredRuleOntologyPath;
	}

	public String getConfiguredBaseOntologyPath() {
		return configuredBaseOntologyPath;
	}

	public void setConfiguredBaseOntologyPath(String configuredBaseOntologyPath) {
		this.configuredBaseOntologyPath = configuredBaseOntologyPath;
	}

	public String getBaseOntologyClass() {
		return baseOntologyClass;
	}

	public void setBaseOntologyClass(String baseOntologyClass) {
		this.baseOntologyClass = baseOntologyClass;
	}
}
