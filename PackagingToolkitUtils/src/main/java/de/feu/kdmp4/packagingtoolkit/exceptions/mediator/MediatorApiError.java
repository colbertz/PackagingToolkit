package de.feu.kdmp4.packagingtoolkit.exceptions.mediator;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.http.HttpStatus;

/**
 * Is used for sending exceptions via REST to the client. It contains the important information
 * about the error. The receiver has to created an exception out of these information.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class MediatorApiError {
	/**
	 * The status code for the http error.
	 */
	private HttpStatus status;
	/**
	 * The message of the exception.
	 */
    private String message;
    /**
     * The classname of the original exception. This is necessary in order to create an exception on
     * client side.
     */
    private String exception;
    /**
     * An exception for the user contains a summary text.
     */
    private String summary;
 
    /**
     * Creates an empty object. Only necessary for the REST API and may only be called by the API.
     */
    public MediatorApiError() {
	}
    
    /**
     * Creates a api error.
     * @param status The http status code. 
     * @param message The message that should be shown on the screen.
     * @param exception The class of the original exception.
     * @param summary A summary of the error. 
     */
    public MediatorApiError(HttpStatus status, String message, String exception, String summary) {
        super();
        this.status = status;
        this.message = message;
        this.exception = exception;
        this.summary = summary;
    }
    
    public HttpStatus getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	public String getException() {
		return exception;
	}
	
	public void setException(String exception) {
		this.exception = exception;
	}
	
	public String getSummary() {
		return summary;
	}
	
	public void setSummary(String summary) {
		this.summary = summary;
	}
}
