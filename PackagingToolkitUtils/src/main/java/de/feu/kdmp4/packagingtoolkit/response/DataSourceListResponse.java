package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains a list with data sources that are used by the mediator for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class DataSourceListResponse {
	/**
	 * The list with the data sources.
	 */
	private List<DataSourceResponse> dataSources;

	/**
	 * Creates a new object with an array list.
	 */
	public DataSourceListResponse() {
		dataSources = new ArrayList<>();
	}
	
	public List<DataSourceResponse> getDataSources() {
		return dataSources;
	}

	public void setDataSources(List<DataSourceResponse> dataSources) {
		this.dataSources = dataSources;
	}
	
	public void addDataSource(DataSourceResponse dataSource) {
		dataSources.add(dataSource);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataSources == null) ? 0 : dataSources.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSourceListResponse other = (DataSourceListResponse) obj;
		if (dataSources == null) {
			if (other.dataSources != null)
				return false;
		} else if (!dataSources.equals(other.dataSources))
			return false;
		return true;
	}
}
