package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Represents an individual that is sendet via HTTP.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class IndividualResponse implements Serializable {
	// ************* Constants *****************
	private static final long serialVersionUID = 6599567279517938800L;

	/**
	 * A list with the properties of this individual. They contain
	 * the values entered by the user.
	 */
	private DatatypePropertyListResponse ontologyDatatypeProperties;
	
	private ObjectPropertyListResponse ontologyObjectProperties;
	/**
	 * The iri of the class this indiviudal is assigned to.
	 */
	private String classIri;
	/**
	 * The uuid that identifies this individual.
	 */
	private UuidResponse uuid;
	/**
	 * The uuid of the information package this individual is assigned to. 
	 */
	private UuidResponse uuidOfInformationPackage;
	/**
	 * The iri of this individual. Can contain the uuid as local name. 
	 */
	private IriResponse iriOfIndividual;
	
	/**
	 * The default constructor creates the lists contained in this object.  It is
	 * important for Framework like JAXB and has to exist in this class.
	 */
	public IndividualResponse() {
		uuid = ResponseModelFactory.getUuidResponse();
		ontologyDatatypeProperties = ResponseModelFactory.getOntologyPropertyListResponse();
		ontologyObjectProperties = ResponseModelFactory.getObjectPropertyListResponse();
	}
	
	public IndividualResponse(Uuid uuidOfIndividual, Uuid uuidOfInformationPackage) {
		this.uuid = ResponseModelFactory.getUuidResponse(uuidOfIndividual.toString());
		if (uuidOfInformationPackage != null) {
			this.uuidOfInformationPackage = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage.toString());
		}
		ontologyDatatypeProperties = ResponseModelFactory.getOntologyPropertyListResponse();
		ontologyObjectProperties = ResponseModelFactory.getObjectPropertyListResponse();
	}

	/**
	 * Adds an ontology property to this individual.
	 * @param ontologyProperty The ontology property.
	 */
	public void addOntologyProperty(final DatatypePropertyResponse ontologyProperty) {
		ontologyDatatypeProperties.addDatatypePropertyToList(ontologyProperty);
	}
	
	/**
	 * Adds an object property to this individual.
	 * @param objectProperty The object property.
	 */
	public void addObjectProperty(final ObjectPropertyResponse ontologyProperty) {
		ontologyObjectProperties.addObjectPropertyToList(ontologyProperty);
	}
	
	// ************ Getters and setters ********
	@XmlAttribute
	public String getClassIri() {
		return classIri;
	}

	public void setClassIri(String classIri) {
		this.classIri = classIri;
	}

	@XmlElement
	public UuidResponse getUuid() {
		return uuid;
	}

	public void setUuid(UuidResponse uuid) {
		this.uuid = uuid;
	}
	
	@XmlElement
	public UuidResponse getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}
	
	public void setUuidOfInformationPackage(UuidResponse uuidOfInformationPackage) {
		this.uuidOfInformationPackage = uuidOfInformationPackage;
	}

	@XmlElement
	public DatatypePropertyListResponse getOntologyDatatypeProperties() {
		return ontologyDatatypeProperties;
	}

	public void setOntologyDatatypeProperties(DatatypePropertyListResponse ontologyDatatypeProperties) {
		this.ontologyDatatypeProperties = ontologyDatatypeProperties;
	}

	@XmlElement
	public ObjectPropertyListResponse getOntologyObjectProperties() {
		return ontologyObjectProperties;
	}

	public void setOntologyObjectProperties(ObjectPropertyListResponse ontologyObjectProperties) {
		this.ontologyObjectProperties = ontologyObjectProperties;
	}
	
	@XmlElement
	public IriResponse getIriOfIndividual() {
		return iriOfIndividual;
	}
	
	public void setIriOfIndividual(IriResponse iriOfIndividual) {
		this.iriOfIndividual = iriOfIndividual;
	}
}
