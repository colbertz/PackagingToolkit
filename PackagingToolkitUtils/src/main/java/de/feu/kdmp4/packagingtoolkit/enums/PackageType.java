package de.feu.kdmp4.packagingtoolkit.enums;

/**
 * This enum contains the available package types and provides methods for access them. Is used for 
 * sending the type information via REST, because it is not possible to send implicit type information
 * within a class hierarchy. 
 * @author Christopher Olbertz
 *
 */
public enum PackageType {
	 SIP("SIP"), 
	SIU("SIU");

	/**
	 * A string that is assigned to a packageType.
	 */
	private final String packageType;
	
	/**
	 * The constructor is only called internally and gets a string. This string
	 * is assigned to a packageType.
	 * @param packageType
	 */
	private PackageType(final String packageType) {
		this.packageType = packageType;
	}
	
	/**
	 * Returns the string that is assigned to a package type.
	 * @return The string representation of the package type.
	 */
	@Override
	public String toString() {
		return packageType;
	}
	
	/**
	 * Checks if a string is a correct package type name. 
	 * @param packageType The string that has to be checked.
	 * @return True, if the string corresponds to a package
	 * type, else false.
	 */
	public static boolean isPackageType(final String packageType) {
		for (final PackageType myPackageType: values()) {
			if (myPackageType.toString().equals(packageType)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets a string and checks, if this string is a correct package
	 * type name. If a package type is found, this is returned. If the
	 * string is not a correct package type name, the return value is null.
	 * @param packageType The string with a package type name.
	 * @return The found package type or null.
	 */
	public static PackageType getPackageType(final String packageType) {
		for (final PackageType myPackageType: values()) {
			if (myPackageType.toString().equals(packageType)) {
				return myPackageType;
			}
		}
		
		return null;
	}
}
