package de.feu.kdmp4.packagingtoolkit.utils;

import static java.time.format.DateTimeFormatter.ofLocalizedDate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;

import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Contains following methods for working with date and time:
 * <ul>
 * 	<li>Returns the current time as string.</li>
 * 	<li>Gets the current date and time as string in German notation.</li>
 * 	<li>Convertion of a string in a DateTime object.</li>
 * 	<li>Convertion of a LocalDateTime object to a java.util.Date object.</li>
 * 	<li>Convertion of a LocalDate object to a java.util.Date object.</li>
 * 	<li>Convertion of a LocalTime object to a java.util.Date object.</li>
 * 	<li>Convertion of a java.util.Date object to a LocalDate object.</li>
 * 	<li>Convertion of a java.util.Date object to a LocalDateTime object.</li>
 *		<li>Convertion of a java.util.Date object to a LocalTime object.</li> 
 *  	<li>Determines the current date and time.</li>
 *  	<li>Determines the current time.</li>
 *  	<li>Determines the current date.</li>
 *  	<li>Convertion of a String to a LocalDate object.</li>
 *  	<li>Convertion of a String to a LocalDateTime object.</li>
 *  	<li>Convertion of a String to a LocalTime object.</li>
 *  	<li>Calculation of the difference between to times in milliseconds.</li>
 *  	<li>Convertion of a value in milliseconds to a value in seconds.</li>
 * </ul>
 * @author Christopher Olbertz
 *
 */
@SuppressWarnings("deprecation") // Because the old Date API is used.
public abstract class DateTimeUtils {
	private static final int MILLISECONDS_OF_A_SECOND = 1000;
	private static final String OWL_DATE_FORMAT = "yyyy-MM-dd";
	private static final String OWL_DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss";
	private static final String OWL_TIME_FORMAT = "hh:mm:ss";
	
	/**
	 * Returns the current date and time as a string.
	 * @return The current date and time as a string.
	 */
	public static final String getNowAsString() {
		final DateTimeFormatter dateTimeFormatter = ofLocalizedDate(FormatStyle.SHORT).
				withLocale(Locale.GERMAN);
		final LocalDateTime now = LocalDateTime.now();
		return now.format(dateTimeFormatter);
	}
	
	/**
	 * Formats the date as a string representing as a date in German notation.
	 * @param date The date we want to be written in German notation.
	 * @return The date as string in German notation.
	 */
	public static final String getDateAsGermanString(final LocalDate date) {
		final DateTimeFormatter dateTimeFormatter = ofLocalizedDate(FormatStyle.SHORT).
				withLocale(Locale.GERMAN);
		return date.format(dateTimeFormatter);
	}
	
	/**
	 * Converts a string that represents a datetime in an ontology in an object of LocalDateTime. The value in the ontology
	 * has the following format: YYYY-MM-ddThh:mm:ss. The T is required as separator between date and time.
	 * @param ontologyValue The value that was found in an ontology.
	 * @return The LocalDateTime object that represents ontologyValue.
	 * @throws DateTimeParseException if the string has the wrong format.
	 */
	public static final LocalDateTime getOntologyStringAsDateTime(final String ontologyValue) {
		return LocalDateTime.parse(ontologyValue);
	}
	
	/**
	 * Converts a LocalDateTime object from Java 8 in a java.util.Date object. Is sometimes needed for backward compatibility.
	 * @param localDateTime The LocalDateTime object we want to convert.
	 * @return The Date object with the values of localDateTime.
	 */
	public static final Date convertLocalDateTimeToDate(final LocalDateTime localDateTime) {
		final int year = localDateTime.getYear() - 1900;
		int month = localDateTime.getMonthValue();
		final int day = localDateTime.getDayOfMonth();
		final int hour = localDateTime.getHour();
		final int minute = localDateTime.getMinute();
		final int second = localDateTime.getSecond();
		
		month--;
		
		final Date date = new Date(year, month, day, hour, minute, second);
		return date;
	}

	/**
	 * Converts a LocalDate object from Java 8 in a java.util.Date object. Is sometimes needed for backward compatibility.
	 * @param localDate The LocalDate object we want to convert.
	 * @return The Date object with the values of localDate.
	 */
	public static final Date convertLocalDateToDate(final LocalDate localDate) {
		final int year = localDate.getYear() - 1900;
		int month = localDate.getMonthValue();
		final int day = localDate.getDayOfMonth();
		/*
		 * We have to decrement the month because Date starts with 1 as January
		 * and LocalDate with 0 as January. 
		 */
		month--;

		final Date date = new Date(year, month, day);
		return date;
	}
	
	/**
	 * Converts a LocalTime object from Java 8 in a java.util.Date object. Is sometimes needed for backward compatibility.
	 * @param localTime The LocalTime object we want to convert.
	 * @return The Date object with the values of localDate.
	 */
	public static final Date convertLocalTimeToDate(final LocalTime localTime) {
		final int hour = localTime.getHour();
		final int minute = localTime.getMinute();
		final int second = localTime.getSecond();
		
		final Date date = new Date();
		date.setHours(hour);
		date.setMinutes(minute);
		date.setSeconds(second);
		return date;
	}
	
	/**
	 * Converts a java.util.Date object to a LocalDate object from Java 8.
	 * @param date The object we want to convert to LocalDate.
	 * @return The converted object.
	 */
	public static final LocalDate convertDateToLocalDate(final Date date) {
		final int year = date.getYear() + 1900;
		int month = date.getMonth();
		final int day = date.getDate();
		/*
		 * We have to increment the month because Date starts with 0 as January
		 * and LocalDate with 1 as January. 
		 */
		month++;
		
		return LocalDate.of(year, month, day);
	}
	
	/**
	 * Converts a java.util.Date object to a LocalDateTime object from Java 8.
	 * @param date The object we want to convert to LocalDateTime.
	 * @return The converted object.
	 */
	public static final LocalDateTime convertDateToLocalDateTime(final Date date) {
		final int year = date.getYear() + 1900;
		final int month = date.getMonth() + 1;
		final int day = date.getDate();
		final int hours = date.getHours();
		final int minutes = date.getMinutes();
		final int seconds = date.getSeconds();
		
		return LocalDateTime.of(year, month, day, hours, minutes, seconds);
	}
	
	/**
	 * Converts a string that represents a date in an ontology in an object of LocalDate. The value in the ontology
	 * has the following format: YYYY-MM-dd. 
	 * @param ontologyValue The value that was found in an ontology.
	 * @return The LocalDate object that represents ontologyValue.
	 * @throws DateParseException if the string has the wrong format.
	 */
	public static final LocalDate getOntologyStringAsDate(final String ontologyValue) {
		return LocalDate.parse(ontologyValue);
	}
	
	/**
	 * Converts a local date to a string that correspondends to the date format of OWL.
	 * @param date The date we want to convert.
	 * @return The date as string in the owl date format.
	 */
	public static final String getLocalDateAsOntologyString(final LocalDate date) {
		final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(OWL_DATE_FORMAT);
		final String dateAsString = dateTimeFormatter.format(date);
		return dateAsString;
	}
	
	/**
	 * Converts a local time to a string that correspondends to the time format of OWL.
	 * @param time The time we want to convert.
	 * @return The time as string in the owl time format.
	 */
	public static final String getLocalTimeAsOntologyString(final LocalTime time) {
		final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(OWL_TIME_FORMAT);
		final String timeAsString = dateTimeFormatter.format(time);
		return timeAsString;
	}
	
	/**
	 * Converts a local datetime to a string that correspondends to the datetime format of OWL.
	 * @param dateTime The datetime we want to convert.
	 * @return The datetime as string in the owl datetime format.
	 */
	public static final String getLocalDateTimeAsOntologyString(final LocalDateTime dateTime) {
		final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(OWL_DATE_TIME_FORMAT);
		final String dateTimeAsString = dateTimeFormatter.format(dateTime);
		return dateTimeAsString;
	}
	
	/**
	 * Returns the current date.
	 * @return An object that contains the current date.
	 */
	public static final LocalDate getToday() {
		return LocalDate.now();
	}
	
	/**
	 * Determines the current time.
	 * @return An object that contains the current time. 
	 */
	public static final LocalTime getNowTime() {
		return LocalTime.now();
	}
	
	/**
	 * Determines the current date and time.
	 * @return An object that contains the current date and time. 
	 */
	public static final LocalDateTime getNow() {
		return LocalDateTime.now();
	}
	
	/**
	 * Parses a string to a LocalDate object. 
	 * @param value The string in the format yyyy-mm-dd
	 * @return The LocalDate object.
	 */
	public static LocalDate getValueAsDate(final String value) {
		if (StringValidator.isNotNullOrEmpty(value)) {
			final long epochDay = Long.parseLong(value);
			final LocalDate localDate = LocalDate.ofEpochDay(epochDay);
			return localDate;
		}
		
		return null;
	}
	
	/**
	 * Parses a string to a LocalDateTime object. 
	 * @param value The string in the format yyyy-mm-dd hh:mm:ss
	 * @return The LocalDateTime object.
	 */
	public static LocalDateTime getValueAsDateTime(final String value) {
		if (StringValidator.isNotNullOrEmpty(value)) {
			final long epochSeconds = Long.parseLong(value);
			final LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(epochSeconds, 0, ZoneOffset.UTC);
			return localDateTime;
		}
		
		return null;
	}
	
	/**
	 * Parses a string to a LocalTime object.
	 * @param value The string we want to parse.
	 * @return The LocalTime object.
	 */
	public static LocalTime getValueAsTime(final String value) {
		try {
			if (value != null) {
				return LocalTime.parse(value);
			}
		} catch (DateTimeParseException ex) {
			final long nanoOfDay = Long.parseLong(value);
			return LocalTime.ofNanoOfDay(nanoOfDay);
		}
		
		return null;
	}
	
	/**
	 * Gets to time values in milliseconds and calculates the difference
	 * between then. If the first value is smaller than the second one, the
	 * two values are swapped. Therefore a negative result value is not possible.
	 * @param time1 The first time value.
	 * @param time2 The second time value.
	 * @return The difference between the two values in milliseconds.
	 */
	public static long getDiffInMilliseconds(final long time1, final long time2) {
		if (time1 > time2) {
			return time1 - time2;
		} else {
			return time2 - time1;
		}
	}

	/**
	 * Converts a value in milliseconds to a value in seconds. 
	 * @param milliseconds The milliseconds to convert.
	 * @return The result in seconds.
	 */
	public static long convertMillisecondsToSeconds(final long milliseconds) {
		return milliseconds / MILLISECONDS_OF_A_SECOND;
	}
}
