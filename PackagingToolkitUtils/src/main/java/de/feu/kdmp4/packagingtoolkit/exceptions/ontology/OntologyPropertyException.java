package de.feu.kdmp4.packagingtoolkit.exceptions.ontology;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class OntologyPropertyException extends ProgrammingException {
	private static final long serialVersionUID = -560271585739566478L;
	
	private OntologyPropertyException(String message) {
		super(message);
	}
	
	public static OntologyPropertyException defaultLabelMayNotBeEmpty(String theClass, 
			String theMethod) {
		String message = I18nExceptionUtil.getDefaultLabelMayNotBeEmptyString();
		return new OntologyPropertyException(message);
	}
	
	public static OntologyPropertyException propertyMayNotBeNull(String theClass, 
			String theMethod) {
		String message = I18nExceptionUtil.getPropertyMayNotBeEmptyString();
		return new OntologyPropertyException(message);
	}
	
	public static OntologyPropertyException propertyIdMayNotBeNull(String theClass, 
			String theMethod) {
		String message = I18nExceptionUtil.getPropertyIdMayNotBeEmptyString();
		return new OntologyPropertyException(message);
	}
}
