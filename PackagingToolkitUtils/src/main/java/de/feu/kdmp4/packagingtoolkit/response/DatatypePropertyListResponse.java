package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with ontology properties for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class DatatypePropertyListResponse implements Serializable { 
	// *************** Constants **********************
	private static final long serialVersionUID = 9152272928881619738L;
	// *************** Attributes **********************
	/**
	 * A list with the properties.
	 */
	private List<DatatypePropertyResponse> ontologyPropertyList;
	
	//**************** Constructors *********************
	/**
	 * The default constructor is important for frameworks like JAXB. It
	 * initializes the list. 
	 */
	public DatatypePropertyListResponse() {
		ontologyPropertyList = new ArrayList<>();
	}
	
	//**************** Public methods *******************
	/**
	 * Adds a property to the list. 
	 * @param ontologyProperty The property that should be added to the list.
	 */
	public void addDatatypePropertyToList(final DatatypePropertyResponse ontologyProperty) {
		ontologyPropertyList.add(ontologyProperty);
	}
	
	/**
	 * Gets a property at a given index.
	 * @param index The index of the property that should be determined.
	 * @return The found property.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public DatatypePropertyResponse getOntologyPropertyAt(final int index) {
		return ontologyPropertyList.get(index);
	}
	
	/**
	 * Counts the ontology classes in the list.
	 * @return The number of the properties in the list.
	 */
	public int getOntologyPropertyCount() {
		return ontologyPropertyList.size();
	}

	// ************* Getters and Setters **************
	@XmlElement
	public List<DatatypePropertyResponse> getOntologyPropertyList() {
		return ontologyPropertyList;
	}

	public void setOntologyPropertyList(List<DatatypePropertyResponse> ontologyPropertyList) {
		this.ontologyPropertyList = ontologyPropertyList;
	}
}
