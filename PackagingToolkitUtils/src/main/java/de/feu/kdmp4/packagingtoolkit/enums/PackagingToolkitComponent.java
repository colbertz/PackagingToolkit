package de.feu.kdmp4.packagingtoolkit.enums;

/**
 * A enum for the three components of the packaging toolkit: client, server
 * and mediator.
 */
public enum PackagingToolkitComponent {
	CLIENT("Client"), SERVER("Server"), MEDIATOR("Mediator"), UTILS("Utils");
	
	private String componentName;
	
	/**
	 * Initializes an enum component.
	 * @param componentName The name of the component: Client, Server or Mediator.
	 */
	private PackagingToolkitComponent(final String componentName) {
		this.componentName = componentName;
	}
	
	/**
	 * Gets the textual name of an enum component.
	 * @return The name of the enum component as String.
	 */
	public String getComponentName() {
		return componentName;
	}
}
