package de.feu.kdmp4.packagingtoolkit.response.adapters.xml;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import de.feu.kdmp4.packagingtoolkit.validators.ObjectValidator;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * An adapter for marshalling a local date to a string for using in a
 * xml file and for unmarshalling a string that contains a local date.
 * The methods of this class may only be called by JAXB.
 * @author Christopher Olbertz
 *
 */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

	/**
	 * Umarshals a string to a local date.
	 * @param value The local date value as String.
	 * @return The local date.
	 */
	@Override
	public LocalDateTime unmarshal(String value) throws Exception {
		StringValidator.checkIsNotNullOrEmpty(value);
		return LocalDateTime.parse(value);
	}

	/**
	 * Marshals a local date to a string for a xml document.
	 * @param value The local date that should be marshalled to a string.
	 * @return A string that represents the local date.
	 */
	@Override
	public String marshal(LocalDateTime value) throws Exception {
		ObjectValidator.checkIsNull(value);
		return value.toString();
	}
}
