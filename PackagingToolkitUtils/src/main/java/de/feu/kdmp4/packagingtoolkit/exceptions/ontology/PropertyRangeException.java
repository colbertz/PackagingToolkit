package de.feu.kdmp4.packagingtoolkit.exceptions.ontology;

import java.math.BigInteger;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class PropertyRangeException extends UserException {
	private static final long serialVersionUID = -2347589291419693023L;
	private static final String SUMMARY = I18nExceptionUtil.getSummaryPropertyRangeExceptionString();
	
	private PropertyRangeException(String message) {
		super(message);
	}
	
	public static PropertyRangeException valueNotInByteRange(int value) {
		String message = I18nExceptionUtil.getValueNotInByteRangeString();
		message = String.format(message,value);
		return new PropertyRangeException(message);
	}
	
	public static PropertyRangeException valueNotInIntegerRange(long value) {
		String message = I18nExceptionUtil.getValueNotInIntegerRangeString();
		message = String.format(message,value);
		return new PropertyRangeException(message);
	}
	
	public static PropertyRangeException valueNotInLongRange(BigInteger value) {
		String message = I18nExceptionUtil.getValueNotInLongRangeString();
		message = String.format(message,value);
		return new PropertyRangeException(message);
	}
	
	public static PropertyRangeException valueNotInShortRange(int value) {
		String message = I18nExceptionUtil.getValueNotInLongRangeString();
		message = String.format(message,value);
		return new PropertyRangeException(message);
	}

	@Override
	public String getSummary() {
		return SUMMARY;
	}
}
