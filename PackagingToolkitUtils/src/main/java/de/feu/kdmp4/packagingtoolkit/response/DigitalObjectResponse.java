package de.feu.kdmp4.packagingtoolkit.response;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class contains the information about a digital object for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class DigitalObjectResponse  {
	/**
	 * The name of the digital object.
	 */
	private String filename;
	/**
	 * The uui of the information package this digital object is assigned to.
	 */
	private String informationPackageUuid;
	/**
	 * The md5 checksum of the file this digital object is referencing.
	 */
	private String md5Checksum;
	
	private Map<String, String> metadataMap;
	/**
	 * The url is necessary for the server to request the meta data from the mediator. But the url is not
	 * configured on the server itself; it is only configured on the client. The attribute is used for avoiding
	 * that the url of the mediator must be configured on the client and the server. 
	 */
	private String mediatorUrl;
	
	/**
	 * This constructor may only be used by frameworks like JAXB.
	 */
	public DigitalObjectResponse() {
		this.metadataMap = new HashMap<>();
	}
	
	/**
	 * This constructor gets the uuid of the information package that a digital object
	 * is added to and the filename.
	 * @param informationPackageUuid The uuid of the information package.
	 * @param filename The name of the file without the path.
	 */
	public DigitalObjectResponse(UuidResponse informationPackageUuid, String filename) {
		this();
		/*
		 * Here from the parameter a file object is created. Then the name of the
		 * file is used for further processing. This is because it is possible to
		 * give a full filename with the path to this method. This can cause an error.
		 * Only the filename may be given to the method. Therefore this steps are 
		 * necessary. Now we can be sure, that we only have the filename and not 
		 * the full path.  
		 */
		
		this.filename = filename;
		this.informationPackageUuid = informationPackageUuid.toString();
	}
	
	public String getFilename() {
		return filename;
	}
	
	public int getMetadataCount() {
		return metadataMap.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitalObjectResponse other = (DigitalObjectResponse) obj;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		return true;
	}

	public void addMetadata(String key, String value) {
		metadataMap.put(key, value);
	}
	
	// ****************** Getters and setters ****************
	public String getInformationPackageUuid() {
		return informationPackageUuid;
	}

	public void setInformationPackageUuid(String informationPackageUuid) {
		this.informationPackageUuid = informationPackageUuid;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public void setMd5Checksum(String md5Checksum) {
		this.md5Checksum = md5Checksum;
	}
	
	public String getMd5Checksum() {
		return md5Checksum;
	}
	
	public Map<String, String> getMetadataMap() {
		return metadataMap;
	}
	
	public void setMetadataMap(Map<String, String> metadataMap) {
		this.metadataMap = metadataMap;
	}
	
	public void setMediatorUrl(String mediatorUrl) {
		this.mediatorUrl = mediatorUrl;
	}
	
	public String getMediatorUrl() {
		return mediatorUrl;
	}
}
