package de.feu.kdmp4.packagingtoolkit.models.interfaces;

import java.util.UUID;

/**
 * An interface for an id. 
 * @author Christopher Olbertz.
 *
 */
public interface Id {
	public void setNewId(UUID uuid);
	public UUID getUuid();
}
