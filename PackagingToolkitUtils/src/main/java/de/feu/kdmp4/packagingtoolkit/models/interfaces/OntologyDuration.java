package de.feu.kdmp4.packagingtoolkit.models.interfaces;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;


public interface OntologyDuration {
	int getDays();
	int getHours();
	int getMinutes();
	int getSeconds();
	int getYears();
	boolean isNegativeDuration();
	int getMonths();
	
	public static OntologyDuration fromString(String theString) {
		OntologyDuration ontologyDuration = null;
		try {
			if (StringValidator.isNotNullOrEmpty(theString)) {
				DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
				Duration duration = datatypeFactory.newDuration(theString);
				int years = duration.getYears();
				int months = duration.getMonths();
				int days = duration.getDays();
				int hours = duration.getHours();
				int minutes = duration.getMinutes();
				int seconds = duration.getSeconds();
				
				if (duration.getSign() == -1) {
					ontologyDuration = PackagingToolkitModelFactory.getOntologyDuration(days, hours, minutes, months, seconds, years, true);
				} else {
					ontologyDuration = PackagingToolkitModelFactory.getOntologyDuration(days, hours, minutes, months, seconds, years);
				}
			}
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ontologyDuration;
	}
	
}
