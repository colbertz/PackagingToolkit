package de.feu.kdmp4.packagingtoolkit.factories;

import de.feu.kdmp4.packagingtoolkit.models.classes.DefaultInformationPackageUuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.OntologyDurationImpl;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.ExtractorToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ExtractorToolResponse;

/**
 * Implements the methods for object creation of jaxb objects.
 * @author Christopher Olbertz
 *
 */
public abstract class PackagingToolkitModelFactory {
	/*public static final DigitalObject getDigitalObject(File file) {
		return new DigitalObjectImpl(file.getAbsolutePath());
	}
	
	public static final DigitalObject getDigitalObject(String filename) {
		return new DigitalObjectImpl(filename);
	}*/
	
	public static final UuidList createEmptyUuidList() {
		return new UuidList();
	}
	
	public static final OntologyDuration getOntologyDuration(int days, int hours, 
			int minutes, int months, int seconds, int years) {
		return new OntologyDurationImpl(days, hours, minutes, months, seconds, years);
	}

	public static final OntologyDuration getOntologyDuration(int days, int hours, 
			int minutes, int months, int seconds, int years, boolean negative) {
		return new OntologyDurationImpl(days, hours, minutes, months, seconds, years, negative);
	}
	
	public static final ExtractorToolListResponse getExtractorToolList() {
		return new ExtractorToolListResponse();
	}
	
	public static final ExtractorToolResponse getExtractorTool(String toolName) {
		return new ExtractorToolResponse(toolName);
	}
	
	public static final ExtractorToolResponse getExtractorTool(String toolName, boolean activated) {
		return new ExtractorToolResponse(toolName, activated);
	}
	
	public static final ExtractorToolListResponse getExtractorToolList(StringList stringList) {
		return new ExtractorToolListResponse(stringList);
	}
	
	/*public static final OntologyLiteral getOntologyLiteral(String name, String value) {
		return new OntologyLiteralImpl(name, value);
	}
	
	public static final OntologyLiteralList getOntologyLiteralList() {
		return new OntologyLiteralListImpl();
	}*/
	
	public static final StringList getStringList() {
		return new StringList();
	}
	
	public static final Uuid getUuid() {
		return new Uuid();
	}
	
	public static final Uuid getUuid(String uuid) {
		return new Uuid(uuid);
	}
	
	public static final Uuid getDefaultInformationPackageUuid() {
		return new DefaultInformationPackageUuid();
	}
}
