package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.exceptions.response.ExtractorToolResponseException;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Describes a tool for extracting metadata. This class is used for configuration
 * of the mediator. The user can choose the tools he wants to use for extraction.
 * The metadata extractor FITS for example can aggregate several tools under one
 * roof.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class ExtractorToolResponse implements Comparable<ExtractorToolResponse>{
	// ******************* Constants ******************
	private static final String CONSTRUCTOR = "ExtractorTool(String toolName)";
	
	// ******************* Attributes *****************
	/**
	 * True if the tool is configured as activated, false otherwise.
	 */
	private boolean activated;
	/**
	 * The extractor that contains this tool.
	 */
	private Extractor extractor; 
	/**
	 * The name of the tool.
	 */
	private String toolName;	
	// ****************** Constructors ****************
	/**
	 * Constructs a tool with the name toolname. A default, FITS is used as extractor and the 
	 * tool is activated.
	 * @param toolName The name of the tool. May not be null or empty.
	 */
	public ExtractorToolResponse(String toolName) {
		if (StringValidator.isNullOrEmpty(toolName)) {
			throw ExtractorToolResponseException.extractorToolNameEmptyException();
		}
		
		this.toolName = toolName;
		activated = true;
		extractor = Extractor.FITS;
	}
	
	/**
	 * Constructs a tool with the name toolname. A default, FITS is used as extractor.
	 * @param toolName The name of the tool. May not be null or empty.
	 * @param activated True if the tool is activated for use, false otherwise.
	 */
	public ExtractorToolResponse(String toolName, boolean activated) {
		if (StringValidator.isNullOrEmpty(toolName)) {
			throw ExtractorToolResponseException.extractorToolNameEmptyException();
		}
		
		this.toolName = toolName;
		this.activated = activated;
		extractor = Extractor.FITS;
	}
	
	// *************** public methods ********************
	@Override
	public int compareTo(ExtractorToolResponse extractorTool) {
		return this.toolName.compareTo(extractorTool.getToolName());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((extractor == null) ? 0 : extractor.hashCode());
		result = prime * result + ((toolName == null) ? 0 : toolName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtractorToolResponse other = (ExtractorToolResponse) obj;
		if (extractor != other.extractor)
			return false;
		if (toolName == null) {
			if (other.toolName != null)
				return false;
		} else if (!toolName.equals(other.toolName))
			return false;
		return true;
	}
	
	// *************** Getters and setters ***************
	/**
	 * Sets the used tool to FITS.
	 */
	public void setFitsTool() {
		extractor = Extractor.FITS;
	}

	public boolean isFitsTool() {
		return extractor == Extractor.FITS;
	}
	
	public String getToolName() {
		return toolName;
	}
	
	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	// ******************** enums *******************
	/**
	 * A private enum for the metadata extractors. It is only used within the class
	 * ExtractorTool. It is hidden behind some access methods.
	 * @author Christopher Olbertz
	 *
	 */
	private enum Extractor {
		FITS
	}
}
