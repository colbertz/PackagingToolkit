package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaxonomyListResponse implements Serializable {
	private static final long serialVersionUID = -4488670706668747609L;
	private List<TaxonomyResponse> taxonomies;
	
	public TaxonomyListResponse() {
		taxonomies = new ArrayList<>();
	}
	
	public void addTaxonomy(final TaxonomyResponse taxonomyResponse) {
		taxonomies.add(taxonomyResponse);
	}
	
	@XmlElement
	public List<TaxonomyResponse> getTaxonomies() {
		return taxonomies;
	}
	
	public void setTaxonomies(List<TaxonomyResponse> taxonomies) {
		this.taxonomies = taxonomies;
	}
}
