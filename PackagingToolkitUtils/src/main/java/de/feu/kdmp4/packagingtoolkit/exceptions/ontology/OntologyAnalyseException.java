package de.feu.kdmp4.packagingtoolkit.exceptions.ontology;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * Is thrown if there was an exception while analysing the ontology and creating the 
 * classes for representing the ontology in the PackagingToolkit.
 * @author Christopher Olbertz
 *
 */
public class OntologyAnalyseException extends ProgrammingException {

	private OntologyAnalyseException(String message) {
		super(message);
	}

	public static OntologyAnalyseException ontologyClassNotInMapException(String className) {
		String message = I18nExceptionUtil.getOntologyClassNotInMapString(className);
		OntologyAnalyseException analyseException = new OntologyAnalyseException(message);
		return analyseException;
	}
}
