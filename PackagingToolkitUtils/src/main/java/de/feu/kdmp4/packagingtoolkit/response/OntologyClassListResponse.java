package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with ontology classes for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class OntologyClassListResponse implements Serializable { 
	// *************** Constants **********************
	private static final long serialVersionUID = 6050213974271592449L;
	
	/**
	 * Contains the classes.
	 */
	private List<OntologyClassResponse> ontologyClassList;
	
	/**
	 * Creates a new object and initializes it with an empty array list.
	 */
	public OntologyClassListResponse() {
		ontologyClassList = new ArrayList<>();
	}
	
	//**************** Public methods *******************
	/**
	 * Adds an ontologyClass to the list. 
	 * @param ontologyClass The ontologyClass that should be added to the list.
	 */
	public void addOntologyClassToList(final OntologyClassResponse ontologyClass) {
		ontologyClassList.add(ontologyClass);
	}
	
	/**
	 * Gets an ontology class at a given index.
	 * @param index The index of the ontology class that should be determined.
	 * @return The found ontology class.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public OntologyClassResponse getOntologyClassAt(final int index) {
		return ontologyClassList.get(index);
	}
	
	/**
	 * Counts the ontology classes in the list.
	 * @return The number of the ontology classes in the list.
	 */
	public int getOntologyClassCount() {
		return ontologyClassList.size();
	}

	// ************* Getters and Setters **************
	@XmlElement
	public List<OntologyClassResponse> getOntologyClassList() {
		return ontologyClassList;
	}

	public void setOntologyClassList(List<OntologyClassResponse> ontologyClassList) {
		this.ontologyClassList = ontologyClassList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ontologyClassList == null) ? 0 : ontologyClassList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyClassListResponse other = (OntologyClassListResponse) obj;
		if (ontologyClassList == null) {
			if (other.ontologyClassList != null)
				return false;
		} else if (!ontologyClassList.equals(other.ontologyClassList))
			return false;
		return true;
	}
}
