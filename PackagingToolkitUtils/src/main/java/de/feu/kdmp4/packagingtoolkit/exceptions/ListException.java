package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * This exception is thrown if there were any problems with any list.
 * @author Christopher Olbertz
 *
 */
public class ListException extends ProgrammingException {
	// ************* Constants ************
	private static final long serialVersionUID = -6557171572692533239L;

	// ************ Constructors **********
	/**
	 * Constructs an exception.
	 * @param message The message.
	 */
	private ListException(String message) {
		super(message);
	}
	
	// *********** Public methods *********
	/**
	 * Throws an exception if an index is not in the valid range of a list.
	 * @return The exception.
	 */
	public static final ListException indexNotInRangeException() {
		 String message = I18nExceptionUtil.getIndexNotInRangeString();
		 return new ListException(message);
	}
	
	/**
	 * Throws an exception if an element of a list is null.
	 * @return The exception.
	 */
	public static final ListException listElementMayNotBeNullException() {
		 String message = I18nExceptionUtil.getListElementMayNotBeNullString();
		 return new ListException(message);
	}
}
