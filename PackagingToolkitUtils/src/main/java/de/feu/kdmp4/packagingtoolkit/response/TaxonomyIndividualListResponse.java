package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaxonomyIndividualListResponse implements Serializable {
	private static final long serialVersionUID = -808869089482317046L;
	private List<TaxonomyIndividualResponse> individuals;
	private String iriOfTaxonomy;
	
	public TaxonomyIndividualListResponse() {
		individuals = new ArrayList<>();
	}
	
	public void addTaxonomyIndividual(final TaxonomyIndividualResponse taxonomyIndividualResponse) {
		individuals.add(taxonomyIndividualResponse);
	}
	
	public int getNarrowersCount() {
		return individuals.size();
	}
	
	@XmlElement
	public List<TaxonomyIndividualResponse> getIndividuals() {
		return individuals;
	}
	
	public void setIndividuals(List<TaxonomyIndividualResponse> individuals) {
		this.individuals = individuals;
	}
	
	public String getIriOfTaxonomy() {
		return iriOfTaxonomy;
	}
	
	public void setIriOfTaxonomy(String iriOfTaxonomy) {
		this.iriOfTaxonomy = iriOfTaxonomy;
	}
}
