package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with uuids for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class UuidListResponse implements Serializable { 
	private static final long serialVersionUID = 6050213974271592449L;
	/**
	 * A list with uuids that should be sended via http.
	 */
	private List<UuidResponse> uuidList;

	/**
	 * Initializes the list as an empty array list.
	 */
	public UuidListResponse() {
		uuidList = new ArrayList<>();
	}
	
	/**
	 * Adds an uuid to the list. 
	 * @param uuid The uuid that should be added to the list.
	 */
	public void addUuidToList(final UuidResponse uuid) {
		uuidList.add(uuid);
	}
	
	/**
	 * Gets an uuid at a given index.
	 * @param index The index of the uuid that should be determined.
	 * @return The found uuid.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public UuidResponse getUuidAt(final int index) {
		return uuidList.get(index);
	}
	
	/**
	 * Counts the uuids in the list.
	 * @return The number of the uuids in the list.
	 */
	public int getUuidCount() {
		return uuidList.size();
	}

	// ************* Getters and Setters **************
	@XmlElement
	public List<UuidResponse> getUuidList() {
		return uuidList;
	}

	public void setUuidList(List<UuidResponse> uuidList) {
		this.uuidList = uuidList;
	}
}
