package de.feu.kdmp4.packagingtoolkit.constants;

/**
 * Some contains for identifiying the data extracted by the mediator.
 * @author Christopher Olbertz
 *
 */
public class MediatorDataConstants {
	public static final String MD5_CHECKSUM = "md5checksum";
}
