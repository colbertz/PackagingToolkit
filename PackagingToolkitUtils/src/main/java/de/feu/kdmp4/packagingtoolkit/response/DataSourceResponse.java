package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains the information about a data source used by the mediator for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class DataSourceResponse {
	/**
	 * The name of the data source.
	 */
	private String dataSourceName;
	/**
	 * The tools the data source is working with.
	 */
	private DataSourceToolListResponse dataSourceTools;
	
	public DataSourceResponse() {
		dataSourceTools = new DataSourceToolListResponse();
	}
	
	public String getDataSourceName() {
		return dataSourceName;
	}
	
	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
	
	public DataSourceToolListResponse getDataSourceTools() {
		return dataSourceTools;
	}
	
	public void setDataSourceTools(DataSourceToolListResponse dataSources) {
		this.dataSourceTools = dataSources;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataSourceName == null) ? 0 : dataSourceName.hashCode());
		result = prime * result + ((dataSourceTools == null) ? 0 : dataSourceTools.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSourceResponse other = (DataSourceResponse) obj;
		if (dataSourceName == null) {
			if (other.dataSourceName != null)
				return false;
		} else if (!dataSourceName.equals(other.dataSourceName))
			return false;
		if (dataSourceTools == null) {
			if (other.dataSourceTools != null)
				return false;
		} else if (!dataSourceTools.equals(other.dataSourceTools))
			return false;
		return true;
	}
}
