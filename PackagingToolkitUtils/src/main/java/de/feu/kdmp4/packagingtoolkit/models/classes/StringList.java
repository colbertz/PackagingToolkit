package de.feu.kdmp4.packagingtoolkit.models.classes;

import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.exceptions.StringException;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;

/**
 * Encapsulates a list with Strings.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class StringList extends AbstractList {
	// *************** Constructors *********************
	/**
	 * This constructor is only for the use through frameworks like JAXB.
	 */
	public StringList() {
		
	}
	
	/**
	 * Constructs a list of a string array.
	 * @param strings An array with strings to construct the list of.
	 * @throws StringException is thrown if the array is null.
	 */
	public StringList(String[] strings) {
		super();
		if (strings == null) {
			StringException exception = StringException.stringArrayMayNotBeNullException();
			throw exception;
		}
		
		for (String aString: strings) {
			addStringToList(aString);
		}
	}
	
	//**************** Public methods *******************
	/**
	 * Adds an string to the list. 
	 * @param string The string that should be added to the list.
	 * @throws StringException Is thrown if the string is null or empty.
	 */
	public void addStringToList(final String string) {
		try {
			super.add(string);
		} catch (ListException e) {
			StringException exception = StringException.stringMayNotBeEmptyException();
			throw exception;
		}
	}
	
	/**
	 * Gets an string at a given index.
	 * @param index The index of the string that should be determined.
	 * @return The found string.
	 * @throws ListException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public String getStringAt(final int index) {
		return (String)super.getElement(index);
	}
	
	/**
	 * Counts the strings in the list.
	 * @return The number of the strings in the list.
	 */
	public int getStringCount() {
		return super.getSize();
	}
	
	/**
	 * Removes an string from the list. 
	 * @param string The string that should be removed.
	 * @throws StringException Is thrown if the string is null.
	 */
	public void removeString(final String string) {
		try {
			super.remove(string);
		} catch (ListException e) {
			StringException exception = StringException.stringMayNotBeEmptyException();
			throw exception;
		}
	}
	
	/**
	 * Converts the string list to a response object for sending via http.
	 * @return An object for sending via http.
	 */
	public StringListResponse toResponse() {
		StringListResponse response = ResponseModelFactory.getStringListResponse();
		
		for (int i = 0; i < this.getSize(); i++) {
			String myString = this.getStringAt(i);
			response.addString(myString);
		}
		
		return response;
	}
}
