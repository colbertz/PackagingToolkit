package de.feu.kdmp4.packagingtoolkit.models.classes;

import java.io.Serializable;
import java.util.UUID;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.Id;



/**
 * This class represents an id.
 * @author Christopher Olbertz
 *
 */
public abstract class IdImpl implements Serializable, Id {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9079534743606877437L;
	private UUID uuid;
	
	/**
	 * The constructor creates a new uuid. 
	 */
	public IdImpl() {
		uuid = UUID.randomUUID();
	}
	
	/**
	 * Sets a new UUID object.
	 * @param uuid The UUID object.
	 */
	public void setNewId(UUID uuid) {
		assert uuid != null: "uuid may not be null!";
		this.uuid = uuid;
	}
	
	/**
	 * Returns the current uuid.
	 * @return The current uuid.
	 */
	public UUID getUuid() {
		return uuid;
	}
}
