package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * Is thrown when there was an error while working with a file.
 * @author Christopher Olbertz
 *
 */
public class PackagingToolkitFileException extends UserException {
	// ******* Constants *********
	private static final long serialVersionUID = 1572707532354223771L;
	/**
	 * The summary for the error message in the gui.
	 */
	private static final String SUMMARY = I18nExceptionUtil.getSummaryFileExceptionString();
	
	// ******* Constructors ******
	public PackagingToolkitFileException(String message) {
		super(message);
	}

	// ****** Public methods *****
	/**
	 * Creates an exception that is thrown when a directory cannot be created.
	 * @return The exception.
	 */
	public static final PackagingToolkitFileException directoryCouldNotBeCreatedException(String directory) {
		String message = I18nExceptionUtil.getDirectoryCouldNotBeCreatedString();
		message = String.format(message, directory);
		return new PackagingToolkitFileException(message);
	}
	
	/**
	 * Creates an exception that is thrown when a directory exists that should not exist.
	 * @return The exception.
	 */
	public static final PackagingToolkitFileException directoryExistsException(String directory) {
		String message = I18nExceptionUtil.getDirectoryExistsString();
		message = String.format(message, directory);
		return new PackagingToolkitFileException(message);
	}
	
	/**
	 * Creates an exception that is thrown when a filename is empty.
	 * @return
	 */
	public static final PackagingToolkitFileException filenameMayNotBeEmptyException() {
		String message = I18nExceptionUtil.getFilenameMayNotBeEmptyString();
		return new PackagingToolkitFileException(message);
	}
	
	/**
	 * Creates an exception that is thrown if a filename is not a directory.
	 * @return
	 */
	public static final PackagingToolkitFileException notADirectoryException(String filename) {
		String message = I18nExceptionUtil.getNotADirectoryString(filename);
		return new PackagingToolkitFileException(message);
	}
	
	// ******* Getters *********
	@Override
	public String getSummary() {
		return SUMMARY;
	}
}
