package de.feu.kdmp4.packagingtoolkit.constants;

/**
 * Contains the paths for the web services as constants. Every constant begins with
 * CLIENT, MEDIATOR, SERVER. 
 * @author Christopher Olbertz
 *
 */
public class PathConstants {
	public static final String SERVER_CLIENT_INTERFACE = "serverClient";
	public static final String MEDIATOR_CLIENT_INTERFACE = "/mediatorClient";
	public static final String MEDIATOR_CHECK_INSTALLATION_DIRECTORIES = "/checkDataSourceInstallationDirectories";
	public static final String MEDIATOR_COLLECT_DATA_FROM_FILE = "/collectDataFromFile";
	public static final String MEDIATOR_CONFIGURE_DATA_SOURCES = "/configureDataSources";
	public static final String MEDIATOR_PING = "/mediatorPing";
	public static final String MEDIATOR_READ_DATASOURCES = "/getDataSources";
	public static final String SERVER_ADD_DIGITAL_OBJECT = "/addDigitalObject";
	public static final String SERVER_ADD_INDIVIDUAL_TO_INFORMATION_PACKAGE = "/addIndividualToInformationPackage";
	public static final String SERVER_ADD_HTTP_REFERENCE = "/addHttpReference";
	public static final String SERVER_ADD_FILE_REFERENCE = "/addFileReference";
	public static final String SERVER_CANCEL_INFORMATION_PACKAGE_CREATION = "/cancelInformationPackageCreation/{uuid}";
	public static final String SERVER_CREATE_SUBMISSION_INFORMATION_PACKAGE = "/createSubmissionInformationPackage";
	public static final String SERVER_CREATE_SUBMISSION_INFORMATION_UNIT = "/createSubmissionInformationUnit";
	public static final String SERVER_CONFIGURE_BASE_ONTOLOGY = "/configureBaseOntology";
	public static final String SERVER_CONFIGURE_RULE_ONTOLOGY = "configureRuleOntology";
	public static final String SERVER_CONFIGURE_SERVER = "/configureServer";
	public static final String SERVER_DELETE_ARCHIVE = "/deleteArchive/{uuid}";
	public static final String SERVER_DELETE_DIGITAL_OBJECT = "deleteDigitalObject";
	public static final String SERVER_DELETE_VIEW = "/deleteView/{viewId}";
	public static final String SERVER_DOWNLOAD_NOT_VIRTUAL_ARCHIVE = "/downloadNotVirtualArchive/{uuid}";
	public static final String SERVER_DOWNLOAD_VIRTUAL_ARCHIVE = "/downloadVirtualArchive/{uuid}";
	public static final String SERVER_FIND_INDIVIDUALS_OF_CLASS_AND_INFORMATION_PACKAGE = "/findIndividualsOfClassAndInformationPackage"; ///{uuid}/{classname}";
	public static final String SERVER_FIND_INDIVIDUALS_OF_CLASS = "/findIndividualsOfClass"; ///{uuid}/{classname}";
	public static final String SERVER_FIND_PREDEFINED_INDIVIDUALS_OF_CLASS = "/findPredefinedIndividualsOfClass";
	public static final String SERVER_FIND_ALL_ARCHIVES = "/findAllArchivesInStorage";
	public static final String SERVER_GET_ALL_VIEWS = "/getAllViews";
	public static final String SERVER_GET_CONFIGURATION_DATA = "/getConfigurationData";
	public static final String SERVER_GET_INDIVIDUALS_BY_CLASS = "/getIndividualsByClass";
	public static final String SERVER_GET_PROPERTIES_OF_CLASS = "/getPropertiesOfClass";
	public static final String SERVER_LOAD_INFORMATION_PACKAGE = "/loadInformationPackage/{uuid}";
	public static final String SERVER_SAVE_NEW_VIEW = "/saveNewView";
	public static final String SERVER_READ_HIERARCHY = "/readHierarchy";
	public static final String SERVER_READ_PREMIS = "/readPremis";
	public static final String SERVER_READ_SKOS = "/readSkos";
	public static final String SERVER_READ_HIERARCHY_WITH_CLASS_NAMES = "/readHierarchyWithClassNames";
	public static final String SERVER_RECEIVE_METADATA = "/receiveMetadata";
	public static final String SERVER_REMOVE_ARCHIVE = "removeArchive";
	public static final String SERVER_SAVE_INFORMATION_PACKAGE = "/saveInformationPackage";
	public static final String SERVER_UPDATE_INDIVIDUAL_IN_INFORMATION_PACKAGE = "/updateIndividualInInformationPackage";
	public static final String SERVER_UPLOAD_FILE = "/uploadFile";
	public static final String SERVER_CONFIGURATION_INTERFACE = "/serverConfiguration";
	public static final String SERVER_ONTOLOGY_INTERFACE = "/serverOntology";
	public static final String SERVER_PACKAGING_INTERFACE = "/serverPackaging";
	public static final String SERVER_PING = "/pingServer";
	public static final String SERVER_STORAGE_INTERFACE = "/serverStorage";
	public static final String SERVER_UPLOAD_PREMIS_ONTOLOGY = "/uploadPremisOntology";
	public static final String SERVER_UPLOAD_SKOS_ONTOLOGY = "/uploadSkosOntology";
	public static final String SERVER_UPLOAD_RULE_ONTOLOGY = "/uploadRuleOntology";
	public static final String SERVER_UPLOAD_BASE_ONTOLOGY = "/uploadBaseOntology";
	public static final String SERVER_VIEWS_INTERFACE = "/serverViews";
	public static final String MEDIATOR_INTERFACE = "/mediator";
	public static final String SERVER_FIND_ALL_TAXONOMIES = "/findAllTaxonomies";
	public static final String SERVER_GET_TAXONOMY_START_CLASS = "/getTaxonomyStartClass";
	public static final String SERVER_SAVE_SELECTED_TAXONOMY_INDIVIDUALS = "/saveSelectedTaxonomyIndividuals";
	public static final String SERVER_UPDATE_ONTOLOGY_CACHE = "/updateOntologyCache";
	public static final String SERVER_FIND_ALL_INDIVIDUALS_OF_INFORMATION_PACKAGE = "/findAllIndividualsOfInformationPackage";
	public static final String SERVER_ASSIGN_INDIVIDUALS_TO_INFORMATION_PACKAGE = "/assignIndividualsToInformationPackage";
	public static final String SERVER_UNASSIGN_INDIVIDUALS_FROM_INFORMATION_PACKAGE = "/unassignIndividualsFromInformationPackage";
	public static final String SERVER_ASSIGN_TAXONOMY_INDIVIDUALS = "/assignTaxonomyIndividualsToInformationPackage";
	public static final String SERVER_FIND_ALL_ASSIGNED_TAXONOMY_INDIVIDUALS = "/findAllAssignedTaxonomyIndividuals";
	public static final String SERVER_FIND_ALL_REFERENCES_OF_INFORMATION_PACKAGE = "/findAllReferencesOfInformationPackage/{uuid}";
}