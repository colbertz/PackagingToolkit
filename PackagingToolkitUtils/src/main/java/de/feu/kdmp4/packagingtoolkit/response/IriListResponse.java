package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IriListResponse {
	private List<IriResponse> iris;
	
	public IriListResponse() {
		iris = new ArrayList<>();
	}
	
	/**
	 * Adds an iri to the list. 
	 * @param uiri The iri that should be added to the list.
	 */
	public void addIriToList(final IriResponse iri) {
		iris.add(iri);
	}
	
	/**
	 * Gets an iri at a given index.
	 * @param index The index of the iri that should be determined.
	 * @return The found iri.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public IriResponse getIriAt(final int index) {
		return iris.get(index);
	}
	
	/**
	 * Counts the iris in the list.
	 * @return The number of the iris in the list.
	 */
	public int getIriCount() {
		return iris.size();
	}
	
	@XmlElement
	public List<IriResponse> getIris() {
		return iris;
	}
	
	public void setIris(List<IriResponse> iris) {
		this.iris = iris;
	}
}
