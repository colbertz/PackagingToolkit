package de.feu.kdmp4.packagingtoolkit.enums;

import javax.xml.bind.annotation.XmlEnum;

/**
 * An enum for the datatypes that can be used in an ontology.
 * This enum is only for the use in the response classes for sending
 * the ontology elements via http because there are no interfaces and
 * subclassing available. In the rest of the program this enum may
 * not be used.
 * @author Christopher Olbertz
 *
 */
@XmlEnum
public enum OntologyDatatypeResponse {
	BOOLEAN, BYTE, 
	DATE, DATETIME, DOUBLE, DURATION, 
	FLOAT, 
	INTEGER, 
	LONG,
	OBJECT,
	POSITIVE_INTEGER,
	SHORT, STRING,
	TIME;	
}
