package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with individuals for sending via http. The individuals can be assigend to an information
 * package.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class IndividualListResponse implements Serializable { 
	// *************** Constants ***********************
	private static final long serialVersionUID = 6050213974271592449L;
	
	/**
	 * The individuals that are contained in this object.
	 */
	private List<IndividualResponse> individualList;
	/**
	 * The uuid of the information package the individuals are assigned to.
	 */
	private UuidResponse uuidOfInformationPackage;
	
	/**
	 * Initializes the object with an array list.
	 */
	public IndividualListResponse() {
		individualList = new ArrayList<>();
	}
	
	/**
	 * Adds an individual to the list. 
	 * @param individual The individual that should be added to the list.
	 */
	public void addIndividualToList(final IndividualResponse individual) {
		individualList.add(individual);
	}
	
	/**
	 * Gets an individual at a given index.
	 * @param index The index of the individual that should be determined.
	 * @return The found individual.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public IndividualResponse getIndividualAt(final int index) {
		return individualList.get(index);
	}
	
	/**
	 * Counts the individuals in the list.
	 * @return The number of the individuals in the list.
	 */
	public int getIndividualCount() {
		return individualList.size();
	}

	// ************* Getters and Setters **************
	@XmlElement
	public List<IndividualResponse> getIndividualList() {
		return individualList;
	}

	public void setIndividualList(List<IndividualResponse> individualList) {
		this.individualList = individualList;
	}
	
	@XmlElement
	public UuidResponse getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}
	
	public void setUuidOfInformationPackage(UuidResponse uuidOfInformationPackage) {
		this.uuidOfInformationPackage = uuidOfInformationPackage;
	}
}
