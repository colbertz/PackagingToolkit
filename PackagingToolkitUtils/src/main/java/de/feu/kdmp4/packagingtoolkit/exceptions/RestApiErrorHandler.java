package de.feu.kdmp4.packagingtoolkit.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;

//@RestControllerAdvice
// DELETE_ME
public class RestApiErrorHandler  {
	@ExceptionHandler(OntologyReasonerException.class)
	//@ResponseStatus(HttpStatus.BAD_GATEWAY)
	public RestApiError handleOntologyReasonerException(OntologyReasonerException exception) {
		final String message = exception.getMessage();
		final RestApiError restApiError = new RestApiError(message, HttpStatus.BAD_GATEWAY);
		return restApiError;
	}
}
