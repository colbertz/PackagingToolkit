package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;
import de.feu.kdmp4.packagingtoolkit.exceptions.response.ExtractorToolListResponseException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.SortableAbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;


@XmlRootElement
public class ExtractorToolListResponse extends SortableAbstractList<ExtractorToolResponse> {
	// *************** Constants **********************
	private static final String METHOD_ADD_EXTRACTOR_TOOL_TO_LIST = "addExtractorToolToList(ExtractorTool extractorTool)";
	private static final String METHOD_GET_EXTRACTOR_TOOL_AT = "ExtractorTool getExtractorToolAt(int index)";
	private static final String METHODE_DELETE_EXTRACTOR_TOOL = "void deleteExtractorTool(ExtractorTool extractorTool)";
	
	// *************** Constructors *********************
	public ExtractorToolListResponse() {
	}

	/**
	 * Constructs a list with extractor tools of a string list with the names of the
	 * extractor tools.
	 * @param stringList A list with the names of the extractor tools.
	 */
	public ExtractorToolListResponse(StringList stringList) {
		for (int i = 0; i < stringList.getSize(); i++) {
			String aString = stringList.getStringAt(i);
			ExtractorToolResponse extractorTool = PackagingToolkitModelFactory.getExtractorTool(aString);
			addExtractorToolToList(extractorTool);
		}
	}
	
	//**************** Public methods *******************
	/**
	 * Adds an extractor tool to the list.
	 * @param extractorTool The extractorTool that should be added to the list.
	 * @throws ExtractorToolListResponseException Is thrown if the extractorTool is null.
	 */
	public void addExtractorToolToList(final ExtractorToolResponse extractorTool) {
		try {
			super.add(extractorTool);
		} catch (ListException e) {
			throw ExtractorToolListResponseException.extractorToolMayNotBeNull();
		}
	}
	
	/**
	 * Checks if the list contains a certain extractor tool. 
	 * @return True if the list contains the extractor tool, false otherwise.
	 */
	public boolean containsExtractorTool(ExtractorToolResponse extractorTool) {
		return super.contains(extractorTool); 
	}
	
	/**
	 * Gets an extractorTool at a given index.
	 * @param index The index of the extractorTool that should be determined.
	 * @return The found extractorTool.
	 * @throws ListException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public ExtractorToolResponse getExtractorToolAt(final int index) {
		ExtractorToolResponse extractorTool;
		extractorTool = (ExtractorToolResponse)super.getElement(index);
		return extractorTool;
		
	}
	
	/**
	 * Counts the extractorTools in the list.
	 * @return The number of the extractorTools in the list.
	 */
	public int getExtractorToolCount() {
		return super.getSize();
	}
	
	/**
	 * Removes an extractorTool from the list. 
	 * @param extractorTool The extractorTool that should be removed.
	 * @throws ExtractorToolMayNotBeNullException Is thrown if the extractorTool is null.
	 */
	public void removeExtractorTool(final ExtractorToolResponse extractorTool) {
		try {
			super.remove(extractorTool);
		} catch (ListException e) {
			throw ExtractorToolListResponseException.extractorToolMayNotBeNull();
		}
	}
}
