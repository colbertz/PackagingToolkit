package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Is used for transfering a text in a certain language to the client. The information in which
 * language the text is written in is important enough to send it to the client.
 * @author Christopher Olbertz 
 *
 */
@XmlRootElement
public class TextInLanguageResponse {
	/**
	 * The text that is written in a certain language.
	 */
	private String text;
	/**
	 * The language the text is written in.
	 */
	private String language;
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	/**
	 * Creates a string in the form:
	 * <br />
	 * language: text
	 */
	@Override
	public String toString() {
		return language + ": " + text;
	}
}
