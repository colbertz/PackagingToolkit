package de.feu.kdmp4.packagingtoolkit.i18n;

import java.util.ResourceBundle;

/**
 * Encapsulates the keys for internationalization. Java code is not allowed
 * to call the message bundles but it has to use this class and its static
 * methods. Therefore the other classes do not have to know the names of 
 * the keys. This class contains the keys only for messages that are not error
 * messages.
 * @author Christopher Olbertz
 *
 */
public class I18nMessageUtil {
	// ************ Constants ****************
	private static final String ADDITIONAL_INFORMATION = "additional-information";
	private static final String MESSAGE = "message";
	
	// ************ Attributes ***************
	private static ResourceBundle messagesResourceBundle;
	
	// *********** Initialization ************
	static {
		messagesResourceBundle = I18nUtil.getMessagesResourceBundle();
	}
	
	// ********** Public methods ************
	public static final String getAdditionalInformationString() {
		return messagesResourceBundle.getString(ADDITIONAL_INFORMATION);
	}
	
	public static final String getMessageString() {
		return messagesResourceBundle.getString(MESSAGE);
	}
}
