package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nMessageUtil;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

/**
 * The super class for all exceptions that are programming exceptions. These
 * are not shown to the user, but they are logged or they are handled otherwise.
 * They are indicators for programming errors that must be corrected.
 * @author Christopher Olbertz
 */
public abstract class ProgrammingException extends RuntimeException {
	// ************** Constants *************
	private static final long serialVersionUID = 3269517747938110547L;
	private static final String TIME_OF_OCCURENCE = "\nTime of occurence %s ";
	
	// ************* Attributes *************
	/**
	 * Some additional information for this exception. Every subclass
	 * can add another type of information. The additional information
	 * is set in the constructor of the respective subclass.
	 */
	protected Object additionalInformation;
	
	// *********** Constructors ************
	/**
	 * Creates an exception. 
	 * @param message The message for this exception.
	 */
	protected ProgrammingException(String message) {
		super(message);
	}
	
	// ********** Public methods ***********
	/**
	 * Creates the message that has to be logged with an exception.
	 * @return The message to log as String.
	 */
	public String getLogMessage(String message) {
		
		final String occurenceTime = DateTimeUtils.getNowAsString();
		StringBuffer logMessage = new StringBuffer("************************");
		logMessage.append(String.format(TIME_OF_OCCURENCE, occurenceTime));
		
		logMessage.append(I18nMessageUtil.getMessageString() + ": ").append(message);
		
		if (additionalInformation != null) {
			logMessage.append(I18nMessageUtil.getAdditionalInformationString() + ": " + additionalInformation);
		}
		
		logMessage.append("************************");
		return logMessage.toString();
	}
	
	// ************ Protected methods **********
	/**
	 * Returns some additional information exception. Should be overriden in the
	 * subclasses if there are any additional information. If there are no 
	 * additional information, the method should not be overriden. In this
	 * case, the method return always null. But this is no problem because
	 * it may only be called by getLogMessage(String). The method is not abstract
	 * because subclasses have not to add additional information.
	 * @return Null because the superclass has no additional information.
	 */
	protected String getAdditionalInformation() {
		return null;
	}
}
