package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with references for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class ReferenceListResponse implements Serializable { 
	private static final long serialVersionUID = 6050213974271592449L;
	
	/**
	 * The list with the references.
	 */
	private List<ReferenceResponse> referenceList;
	/**
	 * The uuid of the information package that contains the references.
	 */
	private UuidResponse uuidOfInformationPackage;
	
	/**
	 * Initializes the object with an array list.
	 */
	public ReferenceListResponse() {
		referenceList = new ArrayList<>();
	}
	
	/**
	 * Adds a reference to the list. 
	 * @param reference The reference that should be added to the list.
	 */
	public void addReferenceToList(final ReferenceResponse reference) {
		referenceList.add(reference);
	}
	
	/**
	 * Gets a reference at a given index.
	 * @param index The index of the reference that should be determined.
	 * @return The found reference.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public ReferenceResponse getReferenceAt(final int index) {
		return referenceList.get(index);
	}
	
	/**
	 * Counts the references in the list.
	 * @return The number of the references in the list.
	 */
	public int getReferenceCount() {
		return referenceList.size();
	}

	// ************* Getters and Setters **************
	@XmlElement
	public List<ReferenceResponse> getReferenceList() {
		return referenceList;
	}

	public void setReferenceList(List<ReferenceResponse> referenceList) {
		this.referenceList = referenceList;
	}
	
	@XmlElement
	public UuidResponse getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}
	
	public void setUuidOfInformationPackage(UuidResponse uuidOfInformationPackage) {
		this.uuidOfInformationPackage = uuidOfInformationPackage;
	}
}
