package de.feu.kdmp4.packagingtoolkit.exceptions.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * An exception that is thrown if the validation of an extractor tool response fails.
 * @author Christopher Olbertz
 *
 */
public final class ExtractorToolResponseException extends ProgrammingException {
	// *************** Aexceptions *****************
	private static final long serialVersionUID = 8346946449807899808L;

	// *************** Constructors ****************
	/**
	 * Constructs an exception.
	 * @param message The message of this exception.
	 */
	private ExtractorToolResponseException(String message) {
		super(message);
	}

	// ************** Public methods ***************
	/**
	 * Creates an exception that is thrown if the name of an extractor tool is empty.
	 * @return The exception.
	 */
	public static final ExtractorToolResponseException extractorToolNameEmptyException() {
		String message = I18nExceptionUtil.getExtractorToolNameMayNotBeEmptyString();
		return new ExtractorToolResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the name of an archive is invalid.
	 * @return The exception.
	 */
	public static final ExtractorToolResponseException archiveNameInvalidException() {
		String message = I18nExceptionUtil.getArchiveNameInvalidString();
		return new ExtractorToolResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the title of an archive is invalid.
	 * @return The exception.
	 */
	public static final ExtractorToolResponseException archiveTitleInvalidException() {
		String message = I18nExceptionUtil.getTitleInvalidMayNotBeEmptyString();
		return new ExtractorToolResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if a string is not a valid uuid.
	 * @param uuid The string that does not contain a valid uuid.
	 * @return The exception.
	 */
	public static final ExtractorToolResponseException doesNotContainValidUuidException(String uuid) {
		String message = I18nExceptionUtil.getDoesNotContainValidUuidString();
		message = String.format(message, uuid);
		return new ExtractorToolResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the package type of an archive is invalid.
	 * @return The exception.
	 */
	public static final ExtractorToolResponseException packageTypeInvalidException() {
		String message = I18nExceptionUtil.getPackageTypeMayNotBeEmptyString();
		return new ExtractorToolResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the serialization format of an archive is invalid.
	 * @return The exception.
	 */
	public static final ExtractorToolResponseException serializationFormatInvalidException() {
		String message = I18nExceptionUtil.getSerializationFormatMayNotBeEmptyString();
		return new ExtractorToolResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the uuid of an archive is invalid.
	 * @return The exception.
	 */
	public static final ExtractorToolResponseException uuidMayNotBeNullException() {
		String message = I18nExceptionUtil.getUuidMayNotBeEmptyString();
		return new ExtractorToolResponseException(message);
	}
}
