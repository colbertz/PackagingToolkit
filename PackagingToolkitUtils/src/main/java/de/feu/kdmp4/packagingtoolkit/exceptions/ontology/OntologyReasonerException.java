package de.feu.kdmp4.packagingtoolkit.exceptions.ontology;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.utils.HtmlUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class OntologyReasonerException extends UserException {
	private static final long serialVersionUID = -525497036438663256L;
	private StringList reasonerMessages;
	
	private OntologyReasonerException(String message) {
		super(message);
		reasonerMessages = PackagingToolkitModelFactory.getStringList();
	}

	/**
	 * Creates a new error with a list that contains the error message from the reasoner.
	 * @param reasonerMessages The messages that were created by the reasoner.
	 * @return An exception object that contains the reasoner messages.
	 */
	public static OntologyReasonerException createIndividualInvalidException(final StringList reasonerMessages) {
		final String message = I18nExceptionUtil.getIndividualInvalidString();
		final OntologyReasonerException ontologyReasonerException = new OntologyReasonerException(message);
		ontologyReasonerException.setMessages(reasonerMessages);
		
		return ontologyReasonerException;
	}
	
	/**
	 * Creates a new error with a list that contains the error message from the reasoner. The messages are separated by line feeds.
	 * @param reasonerMessages The messages that were created by the reasoner.
	 * @return An exception object that contains the reasoner messages.
	 */
	public static OntologyReasonerException createIndividualInvalidException(final String reasonerMessagesSeparatedByLineFeed) {
		final StringList messages = StringUtils.splitLineFeedSeparatedString(reasonerMessagesSeparatedByLineFeed);
		return createIndividualInvalidException(messages);
	}
	
	/**
	 * Returns an error message at a given index.
	 * @param index The index we are looking at.
	 * @return The found error message.
	 */
	public String getErrorMessageAt(final int index) {
		return reasonerMessages.getStringAt(index);
	}
	
	public int getMessagesCount() {
		return reasonerMessages.getSize();
	}
	
	@Override
	public String getSummary() {
		return I18nExceptionUtil.getSummaryIndividualInvalidString();
	}

	private void setMessages(final StringList reasonerMessages) {
		this.reasonerMessages = reasonerMessages;
	}
	
	
	public String getNotHtmlMessage() {
		final StringBuffer string = new StringBuffer();
		
		for (int i = 0; i < reasonerMessages.getStringCount(); i++) {
			final String aString = reasonerMessages.getStringAt(i);
			string.append(aString).append(PackagingToolkitConstants.LINE_FEED);
		}
		
		return string.toString();
	}
	
	@Override
	public String getMessage() {
		return HtmlUtils.createHtmlUnorderedList(reasonerMessages);
	}
}
