package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Represents an iri for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class IriResponse {
	/**
	 * The namespace of the iri.
	 */
	private String namespace;
	/**
	 * The local name of the iri.
	 */
	private String localName;
	
	public IriResponse() {
		
	}
	
	public IriResponse(final String namespace, final String localName) {
		this.namespace = namespace;
		this.localName = localName;
	}

	public String getNamespace() {
		return namespace;
	}
	
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	public String getLocalName() {
		return localName;
	}
	
	public void setLocalName(String localName) {
		this.localName = localName;
	}
	
	@Override
	public String toString() {
		if (StringValidator.isNullOrEmpty(localName)) {
			return namespace;
		} else {
			return namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localName;
		}
	}
}
