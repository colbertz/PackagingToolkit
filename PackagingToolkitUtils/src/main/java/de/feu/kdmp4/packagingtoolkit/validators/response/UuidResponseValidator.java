package de.feu.kdmp4.packagingtoolkit.validators.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.response.UuidResponseException;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;

/**
 * <p>Checks an object from the type {@link de.feu.kdmp4.packagingtoolkit.response.UuidResponse}.
 * Every check has to methods. All methods beginning with the suffix is... return true or false and
 * all methods with the suffix check... throw an exception.</p>
 * <p>Contains the following checks:
 * <ul>
 * 	<li>If a UuidResponse object is null.</li>
 * </ul>
 * </p>
 * 
 * @author Christopher Olbertz
 *
 */
public abstract class UuidResponseValidator {
	// ************** Methods returning a boolean value **************
	/**
	 * Checks if a UuidResponse object is valid. It is not valid if it is null.
	 * @param uuidResponse The object that should be checked.
	 * @return True if uuidResponse is valid, false otherwise.
	 */
	public static final boolean isUuidResponseValid(UuidResponse uuidResponse) {
		if (uuidResponse == null) {
			return false;
		} else {
			return true;
		}
	}
	
	// ************ Methods throwing an exception *****************
	/**
	 * Checks if a UuidResponse object is valid. It is not valid if it is null.
	 * @param uuidResponse The object that should be checked.
	 * @throws UuidResponseException if uuidResponse is not valid.
	 */
	public static final void checkUuidResponseValid(UuidResponse uuidResponse) {
		if (isUuidResponseValid(uuidResponse)) {
			throw UuidResponseException.uuidResponseMayNotBeNull();
		}
	}
}
