package de.feu.kdmp4.packagingtoolkit.models.classes;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.OaisEntity;

/**
 * The base class of all entities in OAIS.
 * @author Christopher Olbertz
 *
 */
public abstract class OaisEntityImpl implements OaisEntity {
	private Uuid uuid;

	public OaisEntityImpl(Uuid uuid) {
		if (uuid == null) {
			this.uuid = new Uuid();
		} else {
			this.uuid = uuid;
		}
	}
	
	public Uuid getUuid() {
		return uuid;
	}

	public void setUuid(Uuid uuid) {
		this.uuid = uuid;
	}
}
