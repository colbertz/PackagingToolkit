package de.feu.kdmp4.packagingtoolkit.validators;

import java.util.Locale;

import de.feu.kdmp4.packagingtoolkit.exceptions.LanguageException;

public class LanguageValidator {
	/**
	 * Throws a exception, if the languageDescription is empty.
	 * @param languageDescription
	 */
	public static void validateLanguageDescription(String languageDescription) {
		if (StringValidator.isNullOrEmpty(languageDescription)) {
			LanguageException languageException = LanguageException.createLanguageDescriptionMayNotBeEmptyException();
			throw languageException;
		}
	}
	
	/**
	 * Throws a exception, if the languageCode is empty.
	 * @param languageCode
	 */
	public static void validateLanguageCode(String languageCode) {
		if (StringValidator.isNullOrEmpty(languageCode)) {
			LanguageException languageException = LanguageException.createLanguageCodeMayNotBeEmptyException();
			throw languageException;
		}	
	}
	
	/**
	 * Checks, if the locale is null.
	 * @param locale
	 */
	public static void validateLocale(Locale locale) {
		if (locale == null) {
			LanguageException languageException = LanguageException.createLocaleMayNotBeNullException();
			throw languageException;
		}		
	}
}
