package de.feu.kdmp4.packagingtoolkit.models.classes;


import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;



@XmlRootElement
public class Uuid /*extends IdImpl*/ implements Comparable<Uuid> {
	private UUID uuid;
	
	public Uuid() {
		uuid = UUID.randomUUID();
	}
	
	public Uuid(final String uuid) {
		this.uuid = setUUIDfromString(uuid);
	}
	
	public Uuid(final UUID uuid) {
		if (uuid == null) {
			this.uuid = UUID.randomUUID();
		} else {
			this.uuid = uuid;
		}
	}

	/**
	 * Sets the UUID from a given string. If the string is null, a 
	 * random UUID is created.
	 * @param uuid The new UUID as string.
	 * @return A UUID object.
	 */
	public UUID setUUIDfromString(String uuid) {
		if (StringValidator.isNullOrEmpty(uuid)) {
			this.uuid = UUID.randomUUID();
		} else if (StringUtils.containsSharp(uuid)) {
			final String uuidAsString = StringUtils.extractLocalNameFromIri(uuid);
			this.uuid = UUID.fromString(uuidAsString);
		} else {
			this.uuid = UUID.fromString(uuid);
		} 
		
		return this.uuid;
	}
	
	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	@Override
	public String toString() {
		return uuid.toString();
	}

	@Override
	public int compareTo(Uuid theOtherUuid) {
		return uuid.compareTo(theOtherUuid.getUuid());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Uuid other = (Uuid) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
