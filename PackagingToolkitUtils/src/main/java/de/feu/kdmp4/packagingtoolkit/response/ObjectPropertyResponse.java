package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * This class encapsulates an object property in an ontology for sending via http. It does not
 * have an attribute for the value because the value are individuals and they are requested
 * separately. 
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class ObjectPropertyResponse implements Serializable {
	private static final long serialVersionUID = -6424338906613482110L;
	/**
	 * The default label as extracted from the ontology.
	 */
	private String label;
	/**
	 * The iri of this property.
	 */
	private String propertyName;
	/**
	 * The iri of the class that is the range of this property.
	 */
	private String range;
	/**
	 * Contains the iris of the domain classes of this property.
	 */
	private StringListResponse domains;
	/**
	 * The value of an object property consists of the uuid of individuals.
	 */
	private IriListResponse value;
	/**
	 * Contains labels extracted from the ontology and written in differez languages.
	 */
	private TextInLanguageListResponse labelTexts;
	
	// ****************** Constructors ***********
	/**
	 * The default constructor may only be used by frameworks
	 * like JAXB.
	 */
	public ObjectPropertyResponse() {
		value = new IriListResponse();
		domains = ResponseModelFactory.getStringListResponse();
		labelTexts = new TextInLanguageListResponse();
	}
	
	/**
	 * Creates a new object. 
	 * @param propertyName The iri of this property.
	 * @param rangeClassIri The iri of the class that is the range of this property.
	 * @param label The default label of this property.
	 */
	public ObjectPropertyResponse(final String propertyName, final String rangeClassIri, final String label) {
		this.propertyName = propertyName;
		if (StringValidator.isNullOrEmpty(label)) {
			this.label = propertyName;
		} else {
			this.label = label;
		}
		
		this.range = rangeClassIri;
		value = new IriListResponse();
		labelTexts = new TextInLanguageListResponse();
	}
	
	/**
	 * Creates a new object without a default label. 
	 * @param propertyName The iri of this property.
	 * @param rangeClassIri The iri of the class that is the range of this property.
	 */
	public ObjectPropertyResponse(final String propertyName, final String rangeClassName) {
		this(propertyName, rangeClassName, StringUtils.EMPTY_STRING);
	}
	
	/**
	 * Adds a new iri of a domain class to his object property.
	 * @param domain The iri of the domain class.
	 */
	public void addDomain(final String domain) {
		domains.addString(domain);
	}
	
	/**
	 * Counts the domain classes of this property.
	 * @return The number of domain classes of this property.
	 */
	public int getDomainCount() {
		return domains.getStringCount();
	}
	
	/**
	 * Determines the iri of a domain class at a certain position in the list. 
	 * @param index The position the list we want to look.
	 * @return The iri found at this position.
	 */
	public String getDomainAt(final int index) {
		return domains.getStringAtIndex(index);
	}
	
	// **************** Getters and setters ********
	@XmlElement
	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
		if (StringValidator.isNullOrEmpty(label)) {
			label = propertyName;
		}
	}

	@XmlElement
	public IriListResponse getValue() {
		return value;
	}

	public void setValue(IriListResponse value) {
		this.value = value;
	}

	@XmlElement
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@XmlElement
	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}
	
	@XmlElement
	public StringListResponse getDomains() {
		return domains;
	}
	
	public void setDomains(StringListResponse domains) {
		this.domains = domains;
	}
	
	@XmlElement
	public TextInLanguageListResponse getLabelTexts() {
		return labelTexts;
	}
	
	public void setLabelTexts(TextInLanguageListResponse labelTexts) {
		this.labelTexts = labelTexts;
	}
}
