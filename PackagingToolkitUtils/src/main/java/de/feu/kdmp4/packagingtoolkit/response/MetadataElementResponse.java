package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class MetadataElementResponse {
	private static final String CONSTRUCTOR = "MetadataElementImpl(String name, String value)";
	private static final String METHOD_GET_VALUE_AT = "String getValueAt(int index)";
	private static final String METHOD_GET_CONFLICTING_METADATA_ELEMENT_AT = "String getConflictingMetadataElementAt(int index)";
	
	// TODO Brauche ich die Uuid noch, wenn nicht mehr in HashMap und in equals verwendet?
	//private Uuid uuid;
	private String name;
	//private MetadataElementListResponse conflictingMetadataElements;
	private List<String> values;
	private boolean multipleValueElement;
	
	public MetadataElementResponse(String name, String value) {
		if (StringValidator.isNullOrEmpty(name)) {
			/*throw new MetadataElementNameMayNotBeEmptyException(this.getClass().getName(), CONSTRUCTOR,
				PackagingToolkitComponent.SERVER);*/
			//TODO Hier auch noch mal eine Exception?
		}
		this.name = name;
		
		if (StringValidator.isNullOrEmpty(value)) {
			/*throw new MetadataElementValueMayNotBeEmptyException(this.getClass().getName(), CONSTRUCTOR,
					PackagingToolkitComponent.SERVER);*/
			//TODO Hier auch noch mal eine Exception?
		}
		values = new ArrayList<>();
		values.add(value);
		//this.conflictingMetadataElements = ResponseModelFactory.getMetadataElementListResponse();
		//this.uuid = new Uuid();
		multipleValueElement = false;
	}
	
	public void addValue(String value) {
		 /*if (StringValidator.isNullOrEmpty(value)) {
			 throw new MetadataElementValueMayNotBeEmptyException(this.getClass().getName(), CONSTRUCTOR,
					 											  PackagingToolkitComponent.SERVER);
		 }*/
		 
		 /* Because there is already a value when creating the element, we know now that this
		 	this element can contain multiple values. */
		 multipleValueElement = true;
		 values.add(value);
		 Collections.sort(values);
	}
	
	public void addConflictingMetadata(MetadataElementResponse metadataElement) {
		// The conflicting metadata must have the same name like this metadata.
		if (metadataElement.getName().equals(this.name)) {
			//conflictingMetadataElements.addMetadata(metadataElement);
		} else {
			//TODO Vielleicht eine Exception werfen.
		}
	}
	
	public String getValueAt(int index) {
		if (multipleValueElement) {
			/*if (!ListValidator.isIndexInRange(index, values)) {
				throw new ListIndexInvalidException(index, this.getClass().getName(), 
												   METHOD_GET_VALUE_AT, 
												   PackagingToolkitComponent.MEDIATOR);
			}*/
			return values.get(index);
		} else {
			return values.get(0);
		}
	}

	/*public MetadataElementResponse getConflictingMetadataElementAt(int index) {
		if (!ListValidator.isIndexInRange(index, con)) {
			throw new IndexNotInRangeException(index, this.getClass().getName(), 
											   METHOD_GET_VALUE_AT, 
											   PackagingToolkitComponent.MEDIATOR);
		return conflictingMetadataElements.getMetadataAt(index);
	}
	
	public int getConflictingMetadataElementsCount() {
		return conflictingMetadataElements.getMetadataCount();
	}*/

	public int getValueCount()  {
		return values.size();
	}
	
	/*public Uuid getUuid() {
		return uuid;
	}*/

	public String getName() {
		return name;
	} 

	public String getValue() {
		return values.get(0);
	}

	public boolean isMultipleValueElement() {
		return multipleValueElement;
	}

	public int compareTo(MetadataElementResponse metadataElement) {
		return name.compareTo(metadataElement.getName());
	}
	
	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(name).append(": ");
		
		for (String myString: values) {
			stringBuffer.append(myString).append("\n");
		}
		return  stringBuffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetadataElementResponse other = (MetadataElementResponse) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}
}