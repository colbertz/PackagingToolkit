package de.feu.kdmp4.packagingtoolkit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulates a list with views.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class ViewListResponse { 
	/**
	 * The views contained in this list.
	 */
	private List<ViewResponse> viewList;
	
	/**
	 * Initializes the object with an empty array list.
	 */
	public ViewListResponse() {
		viewList = new ArrayList<>();
	}
	
	/**
	 * Adds an view to the list. .
	 * @param view The view that should be added to the list.
	 */
	public void addViewToList(final ViewResponse view) {
		viewList.add(view);
	}
	
	/**
	 * Gets an view at a given index.
	 * @param index The index of the view that should be determined.
	 * @return The found view.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	public ViewResponse getViewAt(final int index) {
		return viewList.get(index);
	}
	
	/**
	 * Counts the views in the list.
	 * @return The number of the views in the list.
	 */
	public int getViewCount() {
		return viewList.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((viewList == null) ? 0 : viewList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViewListResponse other = (ViewListResponse) obj;
		if (viewList == null) {
			if (other.viewList != null)
				return false;
		} else if (!viewList.equals(other.viewList))
			return false;
		return true;
	}

	// ************* Getters and Setters **************
	@XmlElement
	public List<ViewResponse> getViewList() {
		return viewList;
	}

	public void setViewList(List<ViewResponse> viewList) {
		this.viewList = viewList;
	}
}
