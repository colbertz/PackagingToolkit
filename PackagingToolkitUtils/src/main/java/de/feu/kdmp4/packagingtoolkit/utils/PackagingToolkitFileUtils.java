package de.feu.kdmp4.packagingtoolkit.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.commons.codec.digest.DigestUtils;

import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitFileException;
import de.feu.kdmp4.packagingtoolkit.exceptions.SystemException;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Some methods for working with files. The following functions are implemented:
 * <ul>
 * 	<li>Append a turtle suffix to a filename.</li>
 * 	<li>Check if a file is an owl file.</li>
 * 	<li>Check if a file is a rdf file.</li>
 * 	<li>Check if a file is a zip file.</li>
 *  	<li>Checks if a directory is empty.</li>
 *  	<li>Construction of a file path with several strings.</li>
 *  	<li>Creation of a directory.</li>
 *  	<li>Create a file from an input stream.</li>
 *    	<li>Deletes one file.</li>
 *    	<li>Determine the filename without the path and a suffix.</li>
 *    	<li>Determine the filename without the path.</li>
 *  	<li>Checks if a directory is not empty.</li>
 *  	<li>Deletes a directory and its files and subdirectories recursivly.</li>
 *  	<li>Determine the directory of the application.</li>
 *  	<li>Check if the content of a file has been changed and delete the old file.</li>
 *  	<li>Copy a file.</li>
 *  	<li>Move a file and replace an existing one.</li>
 *  	<li>Create a path object with the help of a string.</li>
 *  	<li>Calculate the MD5 checksum of a file.</li> 
 * </ul>
 * @author Christopher Olbertz
 *
 */
public abstract class PackagingToolkitFileUtils {
	private static final String DOT = ".";
	public static final String OWL_FILE_EXTENSION = ".owl";
	public static final String RDF_FILE_EXTENSION = ".rdf";
	public static final String TURTLE_FILE_EXTENSION = ".ttl";
	public static final String ZIP_FILE_EXTENSION = ".zip";
	
	/**
	 * Appends the file suffix for turtle.
	 * @param filenameWithoutExtension The filename without any extension.
	 * @return The filename with the extension of turtle.
	 */
	public static String createTurtleFileName(final String filenameWithoutExtension) {
		final String filename = filenameWithoutExtension + TURTLE_FILE_EXTENSION;
		return filename;
	}
	
	/**
	 * Checks with the help of the file extension, if a file is
	 * a owl file. 
	 * @param filename The file name to check.
	 * @return True, if the file name ends with .owl, false otherwise.
	 */
	public static final boolean isOwlFile(final String filename) {
		return filename.endsWith(OWL_FILE_EXTENSION);
	}
	
	/**
	 * Checks with the help of the file extension, if a file is
	 * a rdf file. 
	 * @param filename The file name to check.
	 * @return True, if the file name ends with .rdf, false otherwise.
	 */
	public static final boolean isRdfFile(final String filename) {
		return filename.endsWith(RDF_FILE_EXTENSION);
	}
	
	/**
	 * Checks with the help of the file extension, if a file is
	 * a zip file. 
	 * @param filename The file name to check.
	 * @return True, if the file name ends with .zip, false otherwise.
	 */
	public static final boolean isZipFile(final String filename) {
		return filename.endsWith(ZIP_FILE_EXTENSION);
	}
	
	/**
	 * Creates a path from a list of strings. The list contains the components of the 
	 * path. They are separated by the file separator of the system, / for linux and
	 * \ for windows. 
	 * <br />
	 * Example: From the list with the elements a, b and c the path a/b/c is created 
	 * under -nix systems and the path a\b\c under Windows.  
	 * @param pathComponents The list that contains the path components.
	 * @param absolute True if the path is absolute. Then it starts with a /. This does
	 * only work on -nix systems.
	 * @return The created path.
	 */
	public static String constructFilePath(final StringList pathComponents, final boolean absolute) {
		final StringBuffer path = new StringBuffer();
		
		if (absolute && !SystemUtils.isWindowsSystem()) {
			path.append(File.separator);
		} else if (absolute && SystemUtils.isWindowsSystem()) {
			throw SystemException.createAbsoluteFilepathNotSupportedUnderWindows();
		}
		
		for (int i = 0; i < pathComponents.getSize(); i++) {
			final String pathComponent = pathComponents.getStringAt(i);
			path.append(pathComponent);
			// Append not a / at the end of the path.
			if (i != pathComponents.getSize() - 1) {
				path.append(File.separator);
			}
		}
		
		return path.toString();
	}
	
	/**
	 * Creates a directory.
	 * @param directoryName The name of the directory. 
	 * @throws DirectoryExistsException if the directory already exists.
	 * @throws DirectoryCouldNotBeCreatedException if the directory could not
	 * be created for some reason. 
	 */
	public static final void createDirectory(final String directoryName) {
		File directory = new File(directoryName);
		if (directory.exists()) {
			throw PackagingToolkitFileException.directoryExistsException(directoryName);
		}
		
		if (!directory.mkdirs()) {
			throw PackagingToolkitFileException.directoryCouldNotBeCreatedException(directoryName);
		}
	}
	
	/**
	 * Takes an input stream and writes a file that contains the data of the input stream.
	 * @param file The file that contains the data of inputStream.
	 * @param inputStream Contains the data of file.
	 * @throws IOException Is thrown because the caller knows how to handle the
	 * exception.
	 */
	public static final void createFileFromInputStream(final File file, final InputStream inputStream) 
			throws IOException {
		try (final InputStreamReader reader = new InputStreamReader(inputStream);
				final BufferedReader bufferedReader = new BufferedReader(reader);
				final FileWriter fileWriter = new FileWriter(file);
				final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);) {
				file.createNewFile();
				String line = bufferedReader.readLine();
				
				while (line != null) {
					//TODO Ganze Zeile schreiben
					bufferedWriter.write(line);
					bufferedWriter.newLine();
					line = bufferedReader.readLine();
				}
		} 
		inputStream.close();
	}
	
	/**
	 * Writes the data contained in a byte array in a file.
	 * @param filepath The path of the file where the byte array should be saved in.
	 * @param dataOfFile The data of the file as byte array.
	 */
	public static void writeByteArrayToFile(final String filepath, final byte[] dataOfFile) {
		final File file = new File(filepath);
		writeByteArrayToFile(file, dataOfFile);
	}
	
	/**
	 * Writes the data contained in a byte array in a file.
	 * @param  The file where the byte array should be saved in.
	 * @param dataOfFile The data of the file as byte array.
	 */
	public static void writeByteArrayToFile(File file, byte[] dataOfFile) {
		try (OutputStream outputStream = new FileOutputStream(file)) {
			 outputStream.write(dataOfFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Deletes a file.
	 * @param filename The path of the file that should be deleted.
	 */
	public static final void deleteFile(final String filename) {
		final File file = new File(filename);
		file.delete();
	}
	
	/**
	 * Determines the filename without a suffix.
	 * @param absoluteFilePath The path of the file.
	 * @return The filename without suffix.
	 */
	public static final String getFileNameWithoutSuffix(final String absoluteFilePath) {
		String fileNameWithoutSuffix = "";
		// Exists for getting the file name from the absolute path.
		final File file = new File(absoluteFilePath);
		fileNameWithoutSuffix = file.getName();
		
		// Delete the suffix if existent.
		if (fileNameWithoutSuffix.contains(DOT)) {
			final int posDot = fileNameWithoutSuffix.lastIndexOf(DOT);
			fileNameWithoutSuffix = fileNameWithoutSuffix.substring(0, posDot);
		}
		
		return fileNameWithoutSuffix;
	}
	
	/**
	 * Gets a path of a file and returns only the file name without the path.
	 * @param absoluteFilePath The file path.
	 * @return Only the name of the file.
	 */
	public static final String extractFileName(final String absoluteFilePath) {
		if (StringValidator.isNotNullOrEmpty(absoluteFilePath)) {
			final File file = new File(absoluteFilePath);
			final String fileName = file.getName();
			return fileName;
		}
		return absoluteFilePath;
	}
	
	
	/**
	 * Checks if a directory is empty.
	 * @param directory The directory.
	 * @return False, if the directory is empty, true, if the directory
	 * is not empty, null, if the file object is not a directory.
	 */
	public static final Boolean isDirectoryEmpty(final File directory) {
		if (directory.isDirectory()) {
			final String files[] = directory.list();
			if (files.length > 0 == true) {
				return false;
			} else {
				return true;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Checks if a directory is not empty.
	 * @param directory The directory.
	 * @return True, if the directory is not empty, false, if the directory
	 * is empty, null, if the file object is not a directory.
	 */
	public static final Boolean isDirectoryNotEmpty(final File directory) {
		final Boolean result = isDirectoryEmpty(directory);
		if (result == null) {
			return result;
		} else {
			return !result;
		}
	}
	
	/**
	 * Deletes a directory and its files and subdirectories recursivly.
	 * @param deleteFile The file where to start. If it is a file object, only
	 * this file is deleted.
	 */
	public static final void deleteFilesAndDirectories(final File deleteFile){
		delete(deleteFile);
		deleteFile.delete();
	}
	
	/**
	 * Deletes a file and all subdirectories recursivly.
	 * @param deleteFile The file that should be deleted. 
	 */
	private static final void delete(final File deleteFile) {
		if (deleteFile != null) {
			if (deleteFile.isFile()) {
				deleteFile.delete();
			} else {
				if (isDirectoryEmpty(deleteFile)) {
					deleteFile.delete();
				} else {
					for (File nextFile: deleteFile.listFiles()) {
						deleteFilesAndDirectories(nextFile);
					}
				}
			}  
		}
	}
	
	/**
	 * Determines the path of the application on the file system.
	 * @return The path of the application.
	 */
	public static final String getApplicationDirectory() {
		return System.getProperty("user.dir");
	}
	
	/**
	 * Checks if there is a new file in a certain directory. If there is a new file, the old 
	 * one is deleted.
	 * @param oldFile The path of the old file.
	 * @param newFile The path of the new file.
	 * @param configDirectory The configuration directory that contains the files.
	 */
	public static void deleteOldFileIfChanged(final String oldFile, final String newFile, final File directory) {
		if (StringValidator.isNotNullOrEmpty(newFile)) {
			if (!oldFile.equals(newFile)) {
				final File old = new File(directory + File.separator + oldFile);
				old.delete();
			}
		}
	}
	
	/**
	 * Copies one file to another file. 
	 * @param sourceFile The file that is copied.
	 * @param destFile The file sourceFile is copied to.
	 * @throws CopyProcessNotPossibleException if there was an IOException while copying.
	 */
	public static final void copyFile(final File sourceFile, final File destFile) {
		try {
			Files.copy(sourceFile.toPath(), destFile.toPath());
		} catch (IOException ex) {
		}
	}
	
	/**
	 * Moves a file to another location. If the file is existing in that location, it is replaced.
	 * @param sourceFile The file that is moved.
	 * @param destFile The new location. An existing file is replaced.
	 * @return True if the move operation was successful, false otherwise.
	 */
	public static final boolean moveFileReplaceExisting(final Path sourceFile, final Path destFile) {
		try {
			Files.move(sourceFile, destFile, StandardCopyOption.REPLACE_EXISTING);
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	/**
	 * Creates a path object with the help of a string.
	 * @param directoryPath The String that contains a directory path.
	 * @return The object representing a pyhsical path on the disk.
	 * @throws InvalidPathException - if the path string cannot be converted to a Path.
	 */
	public static final Path getDirectoryPathByPathString(final String directoryPath) {
		return Paths.get(directoryPath);
	}
	
	/**
	 * Extracts from the directory represented by thePath the directory steps time above this
	 * directory. directoryName is appended at the determined path.
	 * <br />
	 * Example: Supposed we have a directory structure like the following:
	 * aaa
	 * 	bbb
	 * 	ccc
	 * 		ddd
	 * 			eee
	 * We call the method with steps = 3, thePath is the directory eee and directoryName is
	 * "bbb". The method return the file that represents the following path on the disk:
	 * aaa/bbb
	 * @param steps The count of directories we want to ascend the directory tree.
	 * @param thePath The path to start with.
	 * @param directoryName The name that is appended to the path.
	 * @return A file that represents the determined directory.
	 */
	public static final File determineDirectoryAboveThisDirectoryPath(final int steps, final Path thePath, final String directoryName)  {
		Path path = thePath;
		
		for (int i = 0; i < steps; i++) {
			path = path.getParent();
		}
		String newPath = path + File.separator + directoryName;
		File newDirectory = new File(newPath);
		return newDirectory;
	}

	/**
	 * Removes the extension of a file. Searches the point and removes all characters behind the point. If the file name
	 * has no extension, the file name is returned without any changes.
	 * @param fileName The file name with the extension. 
	 * @return The file name without the extension.
	 */
	public static String removeFileExtension(final String fileName) {
		String filename = fileName;
		final int indexOfPoint = fileName.indexOf(".");
		
		if (indexOfPoint > 0) {
			filename = filename.substring(0, indexOfPoint);
		} 
		return filename;
	}
	
	/**
	 * Calculates the MD5 checksum of a file. 
	 * @param file The file whose checksum is calculated.
	 * @return The calculated checksum.
	 */
	public static String calculateMD5(final File file) {
		String md5 = "";
		
		try (FileInputStream fileInputStream = new FileInputStream(file)) {
			md5 = DigestUtils.md5Hex(fileInputStream);
		} catch (FileNotFoundException ex) {
			
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		
		return md5;
	}
	
	public static String calculateMD5(final byte[] dataOfFile) {
		final String md5 = DigestUtils.md5Hex(dataOfFile);
		return md5;
	}
	
	public static String calculateMD5(final InputStream inputStream) {
		try {
			final String md5 = DigestUtils.md5Hex(inputStream);
			return md5;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
}
