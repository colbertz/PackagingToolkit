package de.feu.kdmp4.packagingtoolkit.factories;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Encapsulates the creation of objects of {@link java.util.Optional}. The reason for this class is to remove the need
 * of knowing how Optionals are created. 
 * @author Christopher Olbertz
 *
 */
public class OptionalFactory {
	/**
	 * Creates an optional with an uuid.
	 * @param uuid The uuid that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<Uuid> createOptionalWithUuid(final Uuid uuid) {
		return Optional.of(uuid);
	}
	
	/**
	 * Creates an empty optional for an uuid.
	 * @return The created empty Optional.
	 */
	public final static Optional<Uuid> createEmptyOptionalWithUuid() {
		return Optional.empty();
	}
}
