package de.feu.kdmp4.packagingtoolkit.exceptions.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * An exception that is thrown if the validation of an archive response fails.
 * @author Christopher Olbertz
 *
 */
public final class ArchiveResponseException extends ProgrammingException {
	// *************** Aexceptions *****************
	private static final long serialVersionUID = 8346946449807899808L;

	// *************** Constructors ****************
	/**
	 * Constructs an exception.
	 * @param message The message of this exception.
	 */
	private ArchiveResponseException(String message) {
		super(message);
	}

	// ************** Public methods ***************
	/**
	 * Creates an exception that is thrown if the id of an archive is empty.
	 * @return The exception.
	 */
	public static final ArchiveResponseException archiveIdEmptyException() {
		String message = I18nExceptionUtil.getArchiveIdMayNotBeEmptyString();
		return new ArchiveResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the name of an archive is invalid.
	 * @return The exception.
	 */
	public static final ArchiveResponseException archiveNameInvalidException() {
		String message = I18nExceptionUtil.getArchiveNameInvalidString();
		return new ArchiveResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the title of an archive is invalid.
	 * @return The exception.
	 */
	public static final ArchiveResponseException archiveTitleInvalidException() {
		String message = I18nExceptionUtil.getTitleInvalidMayNotBeEmptyString();
		return new ArchiveResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if a string is not a valid uuid.
	 * @param uuid The string that does not contain a valid uuid.
	 * @return The exception.
	 */
	public static final ArchiveResponseException doesNotContainValidUuidException(String uuid) {
		String message = I18nExceptionUtil.getDoesNotContainValidUuidString();
		message = String.format(message, uuid);
		return new ArchiveResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the package type of an archive is invalid.
	 * @return The exception.
	 */
	public static final ArchiveResponseException packageTypeInvalidException() {
		String message = I18nExceptionUtil.getPackageTypeMayNotBeEmptyString();
		return new ArchiveResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the serialization format of an archive is invalid.
	 * @return The exception.
	 */
	public static final ArchiveResponseException serializationFormatInvalidException() {
		String message = I18nExceptionUtil.getSerializationFormatMayNotBeEmptyString();
		return new ArchiveResponseException(message);
	}
	
	/**
	 * Creates an exception that is thrown if the uuid of an archive is invalid.
	 * @return The exception.
	 */
	public static final ArchiveResponseException uuidMayNotBeNullException() {
		String message = I18nExceptionUtil.getUuidMayNotBeEmptyString();
		return new ArchiveResponseException(message);
	}
}
