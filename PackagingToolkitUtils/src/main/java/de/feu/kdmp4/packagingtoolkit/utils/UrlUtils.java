package de.feu.kdmp4.packagingtoolkit.utils;

import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

/**
 * Contains some helper methods to work with urls:
 * <ul>
 * 	<li></li>
 * 	<li></li>
 * </ul>
 * @author christopher
 *
 */
public class UrlUtils {
	/**
	 * Replaces url parameters with their concrete values. The url contains placeholders for the parameters in the following form:
	 * url/{parameter1}/{parameter2}. The method replaces the placeholders with their values. The method gets two lists: One list
	 * contains the names of the parameters. The second list contains the corresponding values. It is important, that the both lists
	 * are correct. This means that the value of the parameter at index i in the parameter list ist replaced by the value at the index
	 * i in the value list.
	 * @param url The url that contains the placeholders for the parameters.
	 * @param parameterNames The names of the parameters.
	 * @param parameterValues The values that should replace the placeholders.
	 * @return A url that contains concrete values instead of the placeholders.
	 */
	public static final String replaceUrlParameters(String url, StringList parameterNames, StringList parameterValues) {
		for (int i = 0; i < parameterNames.getSize(); i++) {
			String parameterName = parameterNames.getStringAt(i);
			String parameterValue = parameterValues.getStringAt(i);
			url = url.replace("{" + parameterName + "}", parameterValue);
		}
		
		return url;
	}
	
	/**
	 * Replaces one parameter with its concrete value. The url contains a placeholder for the parameter in the following form:
	 * url/{parameter1}. The method replaces the placeholder with its value. This method is overload for only one parameter. 
	 * @param url The url that contains one placeholder for one parameter.
	 * @param parameterName The name of the parameter.
	 * @param parameterValue The value that should replace the placeholder.
	 * @return A url that contains concrete a value instead of the placeholder.
	 */
	public static final String replaceUrlParameters(String url, String parameterName, String parameterValue) {
		url = url.replace("{" + parameterName + "}", parameterValue);
		return url;
	}
}
