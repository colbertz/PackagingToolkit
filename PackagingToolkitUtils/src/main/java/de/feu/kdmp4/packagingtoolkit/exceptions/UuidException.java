package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * <p>An exception that is thrown if there are general problems with uuids. This
 * exception is thrown for the following causes:</p>
 * <ul>
 * 	<li>A strig is empty.</li>
 * </ul>
 * 
 * @author Christopher Olbertz
 *
 */
public final class UuidException extends ProgrammingException {
	// *************** Constants *************
	private static final long serialVersionUID = -7562336262766206646L;

	// *************** Constructors **********
	/**
	 * Creates a UuidException.
	 * @param message The message for this exception.
	 */
	private UuidException(String message) {
		super(message);
	}

	// *************** Public methods ********
	/**
	 * Creates an exception if a uuid is null or empty.
	 * @return The exception.
	 */
	public static final UuidException uuidMayNotBeEmptyException() {
		String message = I18nExceptionUtil.getUuidMayNotBeEmptyString();
		return new UuidException(message);
	}
}
