package de.feu.kdmp4.packagingtoolkit.exceptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;


/**
 * This is the super class for all exceptions in the packaging toolkit.
 * It defines some useful methods for the handling of errors.
 * An interface that marks the exceptions of the packaging toolkit. 
 * @author Christopher Olbertz
 *
 */
public abstract class PackagingToolkitException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3269517747938110547L;
	private static final String OCCURENCE_TEXT = "\nThe error occured in class %s "
			+ "and method %s";
	/**
	 * Some additional information for this exception. Every subclass
	 * can add another type of information. The additional information
	 * is set in the constructor of the respective subclass.
	 */
	protected Object additionalInformation;
	private String theClass;
	private String theMethod;
	private Calendar timeOfException;
	//protected MyLogger logger;
	private PackagingToolkitComponent packagingToolkitComponent;
	
	public PackagingToolkitException(final String message, final String theClass, 
			final String theMethod, final PackagingToolkitComponent packagingToolkitComponent) {
		super(message);
		//logger = MyLogger.getInstance();
		this.theClass = theClass;
		this.theMethod = theMethod;
		timeOfException = new GregorianCalendar();
		this.packagingToolkitComponent = packagingToolkitComponent;
	}

	public PackagingToolkitException(String message, Object additionalInformation, 
									 String theClass, String theMethod, 
									 PackagingToolkitComponent packagingToolkitComponent) {
		this(message, theClass, theMethod, packagingToolkitComponent);
		this.additionalInformation = additionalInformation;
	}
	
	public abstract Object getAdditionalInformation();
	/**
	 * Creates the message that has to be logged with an exception.
	 * @return The message to log as String.
	 */
	public String getLogMessage(String message) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		String time = simpleDateFormat.format(timeOfException.getTime());
		StringBuffer logMessage = new StringBuffer("************************");
		logMessage.append(time);
		logMessage.append(getAdditionalInformation());
		logMessage.append(String.format(OCCURENCE_TEXT, theClass, theMethod));
		logMessage.append("Message: ").append(message);
		return logMessage.toString();
	}

	public String getLogMessage() {
		String message = getMessage();
		return String.format(message, getAdditionalInformation());
	}
	
	public String getTheClass() {
		return theClass;
	}

	public String getTheMethod() {
		return theMethod;
	}
	
	public String getPackagingToolkitComponentName() {
		return packagingToolkitComponent.getComponentName();
	}
}
