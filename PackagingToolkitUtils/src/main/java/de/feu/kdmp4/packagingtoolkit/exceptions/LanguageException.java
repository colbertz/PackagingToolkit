package de.feu.kdmp4.packagingtoolkit.exceptions;

import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * An exception that is thrown if there were any problems with a language.
 * @author Christopher Olbertz
 *
 */
public class LanguageException extends ProgrammingException {
	// ************* Constants *************
	private static final long serialVersionUID = 3514197737642301592L;
	
	// ************* Constructors ***********
	/**
	 * Constructs an exception.
	 * @param message The message.
	 */
	private LanguageException(String message) {
		super(message);
	}

	// ************* Public methods *********
	/**
	 * Throws an exception if the description of a language is empty.
	 * @return The exception.
	 */
	public static LanguageException createLanguageDescriptionMayNotBeEmptyException() {
		String message = I18nExceptionUtil.getLanguageDescriptionMayNotBeEmptyString();
		return new LanguageException(message);
	}
	
	/**
	 * Throws an exception if the code of a language is empty.
	 * @return The exception.
	 */
	public static LanguageException createLanguageCodeMayNotBeEmptyException() {
		String message = I18nExceptionUtil.getLanguageCodeMayNotBeEmptyString();
		return new LanguageException(message);
	}
	
	/**
	 * Throws an exception if the locale is null.
	 * @return The exception.
	 */
	public static LanguageException createLocaleMayNotBeNullException() {
		String message = I18nExceptionUtil.getLocaleMayNotBeNullString();
		return new LanguageException(message);
	}
}
