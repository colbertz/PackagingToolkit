package de.feu.kdmp4.packagingtoolkit.constants;

/**
 * Contains constants that describe the names of parameters that are send
 * between the components. They are used in the methods in the rest 
 * interfaces.
 * @author Christopher Olbertz
 *
 */
public abstract class ParamConstants {
	/**
	 * This param contains one classname for sending via http.
	 */
	public static final String PARAM_CLASSNAME = "classname";
	/**
	 * This param contains a list with classnames for sending via http.
	 */
	public static final String PARAM_CLASSNAMES = "classnames";
	/**
	 * This param contains the name of a file for sending via http.
	 */
	public static final String PARAM_FILE = "file";
	/**
	 * This param contains one digital object for sending via http.
	 */
	public static final String PARAM_DIGITAL_OBJECT = "digitalObject";
	/**
	 * This param contains the metadata of a file for sending via http.
	 */
	public static final String PARAM_FILE_META_DATA = "fileMetaData";
	/**
	 * This param contains the title of an information package for sending via http.
	 */
	public static final String PARAM_TITLE = "title";
	/**
	 * This param contains an uuid for sending via http.
	 */
	public static final String PARAM_UUID = "uuid";
	/**
	 * This param contains the uuid of an information package for sending via http.
	 */
	public static final String PARAM_UUID_OF_INFORMATION_PACKAGE = "uuidOfInformationPackage";
	/**
	 * This param contains the uuid of a view for sending via http.
	 */
	public static final String PARAM_VIEW_ID = "viewId";
}
