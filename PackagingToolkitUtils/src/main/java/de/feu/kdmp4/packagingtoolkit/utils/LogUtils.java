package de.feu.kdmp4.packagingtoolkit.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Encapsulated the logging functions.
 * @author Christopher Olbertz
 *
 */
public class LogUtils {
	private static Log log = LogFactory.getLog(LogUtils.class);
	
	/**
	 * Logs an error text. The text contains the date and time the error
	 * occured.
	 * @param errorText The text describing the log entry. 
	 */
	public static void logError(final String errorText) {
		log.error(errorText);
	}
	
	/**
	 * Logs an info text. The text contains the date and time of the info.
	 * @param errorText The text describing the log entry. 
	 */
	public static void logInfo(final String infoText) {
		log.info(infoText);
	}
	
	/**
	 * Logs a debugtext. The text contains the date and time of the debug info.
	 * @param errorText The text describing the log entry. 
	 */
	public static void logDebug(final String debugText) {
		log.debug(debugText);
	}
}
