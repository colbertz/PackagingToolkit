package de.feu.kdmp4.packagingtoolkit.validators;

import de.feu.kdmp4.packagingtoolkit.exceptions.UrlException;

/**
 * Contains methods to check URLs. The following checks can be performed:
 * 
 * <ul>
 * 	<li>Checks, if an url is a file url.</li>
 * 	<li>Checks, if an url is a http url.</li>
 * </ul>
 * @author Christopher Olbertz
 *
 */
public class UrlValidator {
	/**
	 * The beginning of an uri of the http scheme.
	 */
	public static final String HTTP_URI_SCHEME = "http://";
	/**
	 * The beginning of an uri for downloading a file.
	 */
	public static final String FILE_URI_SCHEME = "file://";
	
	/**
	 * Checks if an url is valid. Every scheme is allowed. 
	 * @param url The url that should be checked.
	 * @return True if the url is valid, false otherwise.
	 */
	public static boolean isValidUrl(String url) {
		org.apache.commons.validator.routines.UrlValidator urlValidator = new org.apache.commons.validator.routines.UrlValidator();
		if (urlValidator.isValid(url)) {
			return true;
		} else {
			if (isFileUrl(url)) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * Checks if an url is valid. Every scheme is allowed. Throws an exception if the url is invalid. 
	 * @param url The url that should be checked.
	 * @throws UrlException if the url is not valid.
	 */
	public static void checkUrl(String url) {
		if (isValidUrl(url) == false) {
			UrlException urlException = UrlException.notAValidUrlException(url);
			throw urlException;
		}
	}

	/**
	 * Checks if a url is a file url.
	 * @param url The url to check.
	 * @return True, if the url is a file url, false otherwise.
	 */
	public static boolean isFileUrl(String url) {
		if (StringValidator.isNullOrEmpty(url)) {
			return false;
		}
		return url.startsWith(FILE_URI_SCHEME);
	}
	
	/**
	 * Checks if a url is a http url.
	 * @param url The url to check.
	 * @return True, if the url is a http url, false otherwise.
	 */
	public static boolean isHttpUrl(String url) {
		if (StringValidator.isNullOrEmpty(url)) {
			return false;
		}
		return url.startsWith(HTTP_URI_SCHEME);
	}
	
	/**
	 * Checks if a url is a file url.
	 * @param url The url to check.
	 * @throws UrlException if the url is not a file url.
	 */
	public static void checkFileUrl(String url) {
		if (!isFileUrl(url)) {
			//throw UrlException.notAFileReferenceException(url); 
		}
	}
	
	/**
	 * Checks if a url is a http url.
	 * @param url The url to check.
	 * @throws UrlException if the url is not a http url.
	 */
	public static void checkHttpUrl(String url) {
		if (!isHttpUrl(url)) {
			//throw UrlException.notAHttpReferenceException(url); 
		}
	}
}
