package de.feu.kdmp4.packagingtoolkit.exceptions.response;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

/**
 * This exception is thrown when there are problems with objects of the type
 * {@link de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse}.
 * @author Christopher Olbertz
 *
 */
public class DigitalObjectResponseException extends ProgrammingException {
	// ************** Constants ******************
	private static final long serialVersionUID = -5504472830906850000L;

	// ************** Constructors ***************
	private DigitalObjectResponseException(String message) {
		super(message);
	}
	
	// ************** Public methods *************
	/**
	 * Throws an exception if the filename is null or empty.
	 * @return A new exception.
	 */
	public static DigitalObjectResponseException filenameMayNotBeEmpty() {
		String message = I18nExceptionUtil.getFilenameMayNotBeEmptyString();
		return new DigitalObjectResponseException(message);
	}

}
