package de.feu.kdmp4.packagingtoolkit.utils;

/**
 * Contains some method that are related to the system the application
 * runs on. The following methods are contained:
 * <ul>
 * 	<li>Check if the operating system is windows.</li>
 * </ul>
 * @author christopher
 *
 */
public abstract class SystemUtils {
	private static final String PROPERTY_OS_NAME = "os.name";
	private static final String WINDOWS = "windows";
	
	/**
	 * Checks if the operating system is windows.
	 * @return True if the operating system is windows. False, if the operating
	 * system is a -nix system.
	 */
	public static boolean isWindowsSystem() {
		String system = System.getProperty(PROPERTY_OS_NAME);
		
		if (system.contains(WINDOWS)) {
			return true;
		} else {
			return false;
		}
	}
}
