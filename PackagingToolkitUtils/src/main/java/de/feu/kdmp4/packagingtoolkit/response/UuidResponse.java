package de.feu.kdmp4.packagingtoolkit.response;

import java.io.Serializable;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;
import de.feu.kdmp4.packagingtoolkit.validators.response.UuidResponseValidator;

/**
 * Represents an uuid for sending via http.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class UuidResponse implements Comparable<UuidResponse>, Serializable {
	private static final long serialVersionUID = 780728119535740205L;

	/**
	 * The uuid contained in this object.
	 */
	private UUID uuid;
	
	/**
	 * Creates an object with a random uuid.
	 */
	public UuidResponse() {
		uuid = UUID.randomUUID();
	}
	
	/**
	 * Creates a uuid from a string. If the string is null or empty, 
	 * a random uuid is created.
	 * @param uuid The uuid as string.
	 */
	public UuidResponse(final String uuid) {
		if (StringValidator.isNullOrEmpty(uuid)) {
			this.uuid = UUID.randomUUID();
		}
		
		this.uuid = UUID.fromString(uuid);
	}
	
	/**
	 * Creates a uuid. If the given uuid is null or empty, 
	 * a random uuid is created.
	 * @param uuid The uuid.
	 */	
	public UuidResponse(final UUID uuid) {
		if (uuid == null) {
			this.uuid = UUID.randomUUID();
		} else {
			this.uuid = uuid;
		}
	}

	// ************ Public methods *************
	/**
	 * Compares two uuids.
	 * @param theOtherUuid The uuid this uuid is compared with.
	 * @return A negative integer, zero, or a positive integer as this object 
	 * is less than, equal to, or greater than the specified object.
	 * @throws UuidResponseException is throw if theOtherUuid is null.
	 */
	@Override
	public int compareTo(final UuidResponse theOtherUuid) {
		UuidResponseValidator.checkUuidResponseValid(theOtherUuid);
		return uuid.compareTo(theOtherUuid.getUuid());
	}
	
	/**
	 * Checks if two objects are equal. 
	 * @param obj The object this object has to be compared with. Has to be
	 * an UuidResponse.
	 * @return True if the two objects are equal, false otherwise. 
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UuidResponse other = (UuidResponse) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	
	/**
	 * Returns a hash code value for the object. 
	 * @return A hash code value for this object.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}
	
	/**
	 * Sets the UUID from a given string. If the string is null, a 
	 * random UUID is created.
	 * @param uuid The new UUID as string.
	 * @return A UUID object.
	 */
	public UUID setIDfromString(final String uuid) {
		if (StringValidator.isNotNullOrEmpty(uuid)) {
			this.uuid = UUID.fromString(uuid);
		} else {
			this.uuid = UUID.fromString(UUID.randomUUID().toString());
		}
		
		return this.uuid;
	}
	
	/**
	 * Returns the encapsulated uuid as string.
	 * @return The uuid as string. 
	 */
	@Override
	public String toString() {
		return uuid.toString();
	}

	// *************** Getters and setters *************
	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		if (uuid == null) {
			this.uuid = UUID.randomUUID();
		} else {
			this.uuid = uuid;
		}
	}
}
