package de.feu.kdmp4.packagingtoolkit.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;

/**
 * Represents a view on an ontology that consists of several classes from a ontology.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
public class ViewResponse {
	/**
	 * The classes contained in this view.
	 */
	private OntologyClassListResponse ontologyClassList;
	/**
	 * The id that identifies the view.
	 */
	private int viewId;
	/**
	 * The name of this view.
	 */
	private String viewName;

	/**
	 * Creates a new view with an empty list with classes.
	 */
	public ViewResponse() {
		ontologyClassList = ResponseModelFactory.getOntologyClassListResponse();
	}

	/**
	 * Creates a new object.
	 * @param viewName The name of this view.
	 * @param viewId The id that identifies the view.
	 */
	public ViewResponse(final String viewName, final int viewId) {
		this();
		this.viewName = viewName;
		this.viewId = viewId;
	}
	
	public ViewResponse(String viewName) {
		this();
		this.viewName = viewName;
	}
	
	/**
	 * Adds a new class to this view.
	 * @param ontologyClass The class that should be added to the view. 
	 */
	public void addOntologyClass(final OntologyClassResponse ontologyClass) {
		ontologyClassList.addOntologyClassToList(ontologyClass);
	}

	/**
	 * Determines a class in the list at a given position.
	 * @param index The position we want to look at.
	 * @return The class at this position in the list.
	 */
	public OntologyClassResponse getOntologyClass(final int index) {
		return ontologyClassList.getOntologyClassAt(index);
	}
	
	/**
	 * Returns the number of classes in this view.
	 * @return The number of classes in this view.
	 */
	public int getOntologyClassesCount() {
		return ontologyClassList.getOntologyClassCount();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + viewId;
		result = prime * result + ((viewName == null) ? 0 : viewName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViewResponse other = (ViewResponse) obj;
		if (ontologyClassList == null) {
			if (other.ontologyClassList != null)
				return false;
		} else if (!ontologyClassList.equals(other.ontologyClassList))
			return false;
		if (viewId != other.viewId)
			return false;
		if (viewName == null) {
			if (other.viewName != null)
				return false;
		} else if (!viewName.equals(other.viewName))
			return false;
		return true;
	}

	// ********* Getters and Setters *********
	@XmlElement
	public OntologyClassListResponse getOntologyClassList() {
		return ontologyClassList;
	}

	public void setOntologyClassList(OntologyClassListResponse ontologyClassList) {
		this.ontologyClassList = ontologyClassList;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public int getViewId() {
		return viewId;
	}

	public void setViewId(int viewId) {
		this.viewId = viewId;
	}
}
