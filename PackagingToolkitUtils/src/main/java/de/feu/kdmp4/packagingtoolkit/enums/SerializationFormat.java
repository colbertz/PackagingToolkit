package de.feu.kdmp4.packagingtoolkit.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * An enum for the serialization formats.
 * At the moment, it consists only of one format: OAI-ORE. But it is possible to 
 * extend the application with other formats.
 * @author Christopher Olbertz
 *
 */
@XmlRootElement
@XmlEnum(Integer.class)
public enum SerializationFormat {
	OAIORE("OAI-ORE");
	
	/**
	 * A string that is assigned to a packageType.
	 */
	private final String serializationFormat;
	
	/**
	 * The constructor is only called internally and gets a string. This string
	 * is assigned to a serialization format.
	 * @param packageType
	 */
	private SerializationFormat(final String serializationFormat) {
		this.serializationFormat = serializationFormat;
	}
	
	/**
	 * Returns the string that is assigned to a serialization format.
	 * @return The string representation of the serialization format.
	 */
	@Override
	public String toString() {
		return serializationFormat;
	}
	
	/**
	 * Checks if a string is a correct serialization format name. 
	 * @param serializationFormat The string that has to be checked.
	 * @return True, if the string corresponds to a serialization format
	 * type, else false.
	 */
	public static boolean isSerializationFormat(final String serializationFormat) {
		for (SerializationFormat mySerializationFormat: values()) {
			if (mySerializationFormat.toString().equals(serializationFormat)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets a string and checks, if this string is a correct serialization format
	 * name. If a serialization format is found, this is returned. If the
	 * string is not a correct serialization formatname, the return value is null.
	 * @param serializationFormat The string with a serialization format name.
	 * @return The found serialization format or null.
	 */
	public static SerializationFormat getSerializationFormat(final String serializationFormat) {
		for (SerializationFormat mySerializationFormate: values()) {
			if (mySerializationFormate.toString().equals(serializationFormat)) {
				return mySerializationFormate;
			}
		}
		
		return null;
	}
}
