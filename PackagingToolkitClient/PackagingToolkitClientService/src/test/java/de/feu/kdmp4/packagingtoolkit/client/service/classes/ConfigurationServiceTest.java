package de.feu.kdmp4.packagingtoolkit.client.service.classes;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.client.operations.classes.ConfigurationOperationsPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ClientConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ConfigurationService;

public class ConfigurationServiceTest {
	private ConfigurationService configurationService;
	@Mock
	private ClientConfigurationOperations clientConfigurationOperations;
	@Mock
	private OperationsFacade operationsFacade;
	/**
	 * The name of the directory that contains the configuration files.
	 */
	private static final String CONFIGURATION_DIRECTORY = ".";
	/**
	 * The name of the logo file. 
	 */
	private static final String LOGO_FILE = "logo.png";
	
	private File logoFile;
	
	@Before
	public void setUp() {
		clientConfigurationOperations = mock(ConfigurationOperationsPropertyImpl.class);
		operationsFacade = mock(OperationsFacade.class);
		operationsFacade.setClientConfigurationOperations(clientConfigurationOperations);
		configurationService = new ConfigurationServiceImpl();
		configurationService.setOperationsFacade(operationsFacade);
		logoFile = new File(CONFIGURATION_DIRECTORY, LOGO_FILE);
		final File configurationDirectory = new File(CONFIGURATION_DIRECTORY);
		when(operationsFacade.getLogoPath(configurationDirectory.getAbsolutePath())).thenReturn(logoFile);
	}
	
	@Test
	public void testGetLogo() {
		final String logoPath = "./" + File.separator + LOGO_FILE;
		final File expectedLogo = new File(logoPath);
		final File actualLogo = configurationService.getLogo();
		assertEquals(actualLogo.getAbsolutePath(), expectedLogo.getAbsolutePath());
	}
}
