package de.feu.kdmp4.packagingtoolkit.client.service.classes;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.OntologyService;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;

/**
 * A service class for the access to 
 * {@link de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.OntologyOperations}. 
 * @author Christopher Olbertz
 *
 */
public class OntologyServiceImpl implements OntologyService {
	private InformationPackageManager informationPackageManager;
	private OperationsFacade operationsFacade;
	private ClientResponseFactory clientResponseFactory;
	private ClientModelFactory clientModelFactory;
	
	@Override
	public OntologyClass getHierarchyOfBaseOntology() {
		final String serverUrl = operationsFacade.getServerUrl();
		OntologyClassResponse ontologyClassResponse = operationsFacade.	getHierarchy(serverUrl);
		
		return clientModelFactory.createOntologyClass(ontologyClassResponse, informationPackageManager);
	}
	
	@Override
	public void saveSelectedTaxonomyIndividuals() {
		final String serverUrl = operationsFacade.getServerUrl();
		final List<TaxonomyIndividualCollection> selectedIndividuals = informationPackageManager.getSelectedTaxonomyIndividuals();
		
		for (final TaxonomyIndividualCollection taxonomyIndividualCollection: selectedIndividuals) {
			final TaxonomyIndividualListResponse taxonomyIndividualListResponse = clientResponseFactory.fromTaxonomyIndividualCollection(taxonomyIndividualCollection);
			operationsFacade.saveSelectedTaxonomyIndividuals(taxonomyIndividualListResponse, serverUrl);
		}
	}
	
	@Override
	public OntologyClass getHierarchyOfPremis() {
		final String serverUrl = operationsFacade.getServerUrl();
		final OntologyClassResponse premisRootClassResponse = operationsFacade.getAllPremisClasses(serverUrl);
		return clientModelFactory.createOntologyClass(premisRootClassResponse, informationPackageManager);
	}
	
	@Override
	public OntologyClass getHierarchyOfSkos() {
		final String serverUrl = operationsFacade.getServerUrl();
		final OntologyClassResponse skosRootClassResponse = operationsFacade.getAllSkosClasses(serverUrl);
		return clientModelFactory.createOntologyClass(skosRootClassResponse, informationPackageManager);
	}
	
	@Override
	public TaxonomyCollection findAllTaxonomies() {
		final String serverUrl = operationsFacade.getServerUrl();
		final TaxonomyListResponse taxonomyListResponse = operationsFacade.getAllTaxonomies(serverUrl);
		final TaxonomyCollection taxonomies = ClientModelFactory.createTaxonomyCollection(taxonomyListResponse);
		
		return taxonomies;
	}
	
	@Override
	public OntologyClass getHierarchyOfBaseOntology(final StringList ontologyClassNames) {
		final String serverUrl = operationsFacade.getServerUrl();
		final StringListResponse stringListResponse = ontologyClassNames.toResponse();
		final OntologyClassResponse ontologyClassResponse = operationsFacade.
				getHierarchy(serverUrl, stringListResponse);
		final OntologyClass ontologyClass = clientModelFactory.createOntologyClass(ontologyClassResponse, informationPackageManager); 
		return ontologyClass;
	}
	
	@Override
	public OntologyClass getPropertiesOfClass(final String classname) {
		final String serverUrl = operationsFacade.getServerUrl();
		final OntologyClassResponse ontologyClassResponse = operationsFacade.getPropertiesOfClass(serverUrl, classname);
		final OntologyClass ontologyClass = clientModelFactory.createOntologyClass(ontologyClassResponse, informationPackageManager); 
		return ontologyClass;
	}
	
	@Override
	public OntologyIndividualCollection getSavedIndividualsByClass(final OntologyClass ontologyClass) {
		final OntologyClassResponse ontologyClassResponse = clientResponseFactory.fromOntologyClass(ontologyClass);
		final String serverUrl = operationsFacade.getServerUrl();
		
		final IndividualListResponse individualListResponse = operationsFacade.findSavedIndividualsByClass(serverUrl, ontologyClassResponse);
		if (individualListResponse != null) {
			final OntologyIndividualCollection ontologyIndividualList = clientModelFactory.createOntologyIndividualList(individualListResponse, informationPackageManager);
			return ontologyIndividualList;
		} else {
			return ClientModelFactory.createEmptyOntologyIndividualList();
		}
	}
	
	@Override
	public OntologyIndividualCollection findPredefinedIndividualsByClass(final OntologyClass ontologyClass) {
		final String classname = ontologyClass.getFullName();
		final String serverUrl = operationsFacade.getServerUrl();
		
		final IndividualListResponse individualListResponse = operationsFacade.findPredefinedIndividualsByClass(serverUrl, classname);
		
		final OntologyIndividualCollection ontologyIndividualList = clientModelFactory.createOntologyIndividualList(individualListResponse, informationPackageManager);
		return ontologyIndividualList;
	}
	
	@Override
	public OntologyClass getTaxonomyStartClass() {
		final String serverUrl = operationsFacade.getServerUrl();
		final OntologyClassResponse startClassResponse = operationsFacade.getTaxonomyStartClass(serverUrl);
		final OntologyClass startClass = clientModelFactory.createOntologyClass(startClassResponse);
		return startClass;
	}
	
	@Override
	public void saveIndividual(final OntologyIndividual ontologyIndividual) {
		final String serverUrl = operationsFacade.getServerUrl();
		final IndividualResponse individualResponse = clientResponseFactory.fromOntologyIndividual(ontologyIndividual);
		final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
		final UuidResponse uuidOfInformationPackageResponse = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		individualResponse.setUuidOfInformationPackage(uuidOfInformationPackageResponse);
		operationsFacade.saveIndividual(serverUrl, individualResponse);
	}
	
	@Override
	public void updateIndividual(final OntologyIndividual ontologyIndividual) {
		final String serverUrl = operationsFacade.getServerUrl();
		final IndividualResponse individualResponse = clientResponseFactory.fromOntologyIndividual(ontologyIndividual);
		final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
		final UuidResponse uuidOfInformationPackageResponse = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		individualResponse.setUuidOfInformationPackage(uuidOfInformationPackageResponse);
		operationsFacade.updateIndividual(serverUrl, individualResponse);
	}
	
	@Override	
	public void setInformationPackageManager(final InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}

	@Override
	public void setOperationsFacade(final OperationsFacade operationsFacade) {
		this.operationsFacade = operationsFacade;
	}

	@Override
	public void setClientResponseFactory(final ClientResponseFactory clientResponseFactory) {
		this.clientResponseFactory = clientResponseFactory;
	}

	@Override
	public void setClientModelFactory(final ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}
}
