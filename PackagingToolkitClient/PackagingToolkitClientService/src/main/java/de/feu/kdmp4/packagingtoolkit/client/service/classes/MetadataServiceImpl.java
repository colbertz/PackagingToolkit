package de.feu.kdmp4.packagingtoolkit.client.service.classes;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElementList;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.MediatorCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.MetadataService;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementListResponse;

public class MetadataServiceImpl implements MetadataService {
	private MediatorCommunicationOperations metadataOperations;
	
	@Override
	public MetadataElementList getMetadataFromMediator() {
		MetadataElementListResponse metadataElementListResponse = metadataOperations.
				getMetadataFromMediator();
		return ClientModelFactory.createMetadataElementList(metadataElementListResponse);
	}

	@Override
	public void setMetadataOperations(MediatorCommunicationOperations metadataOperations) {
		this.metadataOperations = metadataOperations;
	}
}
