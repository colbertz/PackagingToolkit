package de.feu.kdmp4.packagingtoolkit.client.service.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;

public interface MediatorService {
	void setOperationsFacade(OperationsFacade operationsFacade);
	void setClientModelFactory(ClientModelFactory clientModelFactory);
	/**
	 * Determines all data sources that are registered on the mediator.
	 * @return All data sources registered on the mediator.
	 * @throws MediatorConfigurationException if the installation directory of a data source is not correct or if the mediator
	 * is not accessible.
	 */
	DataSourceCollection findAllDataSources();
	/**
	 * Sends a file to the mediator in order to collect the meta data of the file.
	 * @param file The file whose meta data we want to extract.
	 * @return The meta data found by the mediator in file.
	 */
	MediatorDataCollection collectMetadata(File file);
	/**
	 * Sends a signal to the mediator for testing if the mediator is accessible.
	 */
	void pingMediator();
	/**
	 * Checks the installation directories of all data sources.
	 * @throws MediatorConfigurationException if the installation directory of a data source is not correct.
	 */
	void checkDataSourceInstallations();
	void setClientResponseFactory(ClientResponseFactory clientResponseFactory);
	/**
	 * Saves the data source configured by the user on the mediator.
	 * @param dataSources The data sources.
	 */
	void saveDataSourceConfiguration(DataSourceCollection dataSources);
}