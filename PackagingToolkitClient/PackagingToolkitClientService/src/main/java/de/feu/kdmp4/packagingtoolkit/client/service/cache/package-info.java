/**
 * Contains classes that cache information from the server for avoiding 
 * to many requests over the network.
 */

package de.feu.kdmp4.packagingtoolkit.client.service.cache;