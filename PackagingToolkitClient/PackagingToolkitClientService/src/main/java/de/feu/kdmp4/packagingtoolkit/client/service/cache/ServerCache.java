package de.feu.kdmp4.packagingtoolkit.client.service.cache;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.OntologyIndividualsAndTheirUuidMap;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.PropertiesAndTheirClassesMap;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.TaxonomyIndividualsAndTheirIrisMap;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.DataStructureFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Caches the values received from the server after reading them from the ontologies. It is not 
 * necessary to send multiple request to the server.  
 * @author Christopher Olbertz
 *
 */
public class ServerCache {
	/**
	 * Contains the individuals in a map. The key is the uuid of the individual.
	 */
	private OntologyIndividualsAndTheirUuidMap individualsAndUuidsMap;
	/**
	 * Contains the taxonomy individuals in a map. The key is the iri of the individual.
	 */
	private TaxonomyIndividualsAndTheirIrisMap taxonomyIndividualsAndIrisMap;
	/**
	 * Contains the individuals of an ontology class.
	 */
	private Map<OntologyClass, CachedIndividualList> individualListMap;
	/**
	 * This map contains classnames as key and the data properties of the class as value.
	 */
	private Map<String, DatatypePropertyCollection> classesDatatypePropertiesMap;
	/**
	 * This map contains classnames as key and the object properties of the class as value.
	 */
	private Map<String, ObjectPropertyCollection> objectPropertiesOfClassMap;
	/**
	 * Contains the properties that were extracted from the ontologies and the classes they are assigned to.
	 */
	private PropertiesAndTheirClassesMap propertiesAndTheirClassesMap;
	/**
	 * Contains the properties that were extracted from the ontologies.
	 */
	private Map<String, OntologyProperty> propertiesMap;
	/**
	 * Contains information about the information package that is currently being created.
	 */
	@Autowired
	private InformationPackageManager informationPackageManager;
	/**
	 * Contains the classes found in the ontologies, idenfied by their iri. 
	 */
	private OntologyClassesMap ontologyClassesMap;
	/**
	 * A reference to the business logic of the client.
	 */
	@Autowired
	private ServiceFacade serviceFacade;
	/**
	 * The root class of the base ontology. 
	 */
	private OntologyClass rootClass;
	/**
	 * This class determines the points where a taxonomy is starting in the class hierarchy.
	 */
	private OntologyClass taxonomyStartClass;
	/**
	 * Contains the taxonomies that are defined in the base ontology.
	 */
	private TaxonomyCollection taxonomies;
	
	/**
	 * Initializes the empty data structures.
	 */
	public ServerCache() {

	}
	
	/**
	 * Initializes the data of the data structures. This may not be done in the constructor because the dependencies to other objects
	 * that are given by Spring are not available in the constructor.
	 */
	@PostConstruct
	public void initialize() {
		individualListMap = new HashMap<>();
		individualsAndUuidsMap = DataStructureFactory.createOntologyIndividualsAndTheirUuidHashMap();
		classesDatatypePropertiesMap = new HashMap<>();
		ontologyClassesMap = ClientModelFactory.createEmptyOntologyClassesMap();
		objectPropertiesOfClassMap = new HashMap<>();
		propertiesAndTheirClassesMap = ClientModelFactory.createEmptyPropertiesAndTheirClassesMap();
		propertiesMap = new HashMap<>();
		taxonomyIndividualsAndIrisMap = ClientModelFactory.createEmptyTaxonomyIndividualsAndTheirIrisMap();
		
		rootClass = serviceFacade.getHierarchyOfBaseOntology();
		putOntologyClassesIntoMap(rootClass);
		//final OntologyClass premisRootClass = serviceFacade.getHierarchyOfPremis();
		//putOntologyClassesIntoMap(premisRootClass);
		//fetchPropertiesOfOntologyHierarchy(premisRootClass);
		final OntologyClass skosRootClass = serviceFacade.getHierarchyOfSkos();
		putOntologyClassesIntoMap(skosRootClass);
		taxonomyStartClass = serviceFacade.getTaxonomyStartClass();
		taxonomies = serviceFacade.findAllTaxonomies();
		putAllTaxonomyIndividualsInMap();
	}
	
	/**
	 * Runs through the taxonomies and puts their individuals in the map.
	 */
	private void putAllTaxonomyIndividualsInMap() {
		for (int i = 0; i < taxonomies.getTaxonomyCount(); i++) {
			final Taxonomy taxonomy = taxonomies.getTaxonomy(i).get();
			final TaxonomyIndividual rootIndividual = taxonomy.getRootIndividual();
			putNarrowersInMap(rootIndividual);
		}
	}
	
	/**
	 * Determines a taxonomy individual with the help of its iri.
	 * @param iriOfTaxonomyIndividual The iri of the taxonomy individual we are looking for. 
	 * @return The taxonomy individual or an empty optional
	 */
	public Optional<TaxonomyIndividual> findTaxonomyIndividualByIri(final Iri iriOfTaxonomyIndividual) {
		final TaxonomyIndividual taxonomyIndividual = taxonomyIndividualsAndIrisMap.getIndividual(iriOfTaxonomyIndividual);
		return ClientOptionalFactory.createTaxonomyIndividualOptional(taxonomyIndividual);
	}
	
	/**
	 * Runs through the narrowers of a taxonomy individual and puts them into 
	 * the map. 
	 * @param taxonomyIndividual The individuals whose narrowers we want to put
	 * into the map.
	 */
	private void putNarrowersInMap(final TaxonomyIndividual taxonomyIndividual) {
		while (taxonomyIndividual.hasNextNarrower()) {
			final TaxonomyIndividual narrower = taxonomyIndividual.getNextNarrower().get();
			if (narrower.containsNarrowers()) {
				putNarrowersInMap(narrower);
			}
			taxonomyIndividualsAndIrisMap.addIndividual(narrower);
		}
	}
	
	// DELETE_ME
	private void fetchPropertiesOfOntologyHierarchy(final OntologyClass startClass) {
		for (int i = 0; i  < startClass.getSubclassCount(); i++) {
			final OntologyClass subClass = startClass.getSubClassAt(i);
			if (subClass.hasSubclasses()) {
				fetchPropertiesOfOntologyHierarchy(subClass);
			}
			getDatatypePropertiesListByClassname(subClass.getFullName());
		}
	}
	
	/**
	 * Finds an individual saved in the cache with the help of its uuid.
	 * @param uuidOfIndividual The uuid of the individual we are looking for.
	 * @return The individual that was found in the cache.
	 */
	public OntologyIndividual findOntologyIndividualByUuid(final Uuid uuidOfIndividual) {
		return individualsAndUuidsMap.getIndividual(uuidOfIndividual);
	}
	
	/**
	 * Finds the taxonomy in the collection that has a title. 
	 * @return The taxonomy with a title.  
	 */
	public Taxonomy findTaxonomyWithTitle() {
		return taxonomies.findTaxonomyWithTitle();
	}
	
	/**
	 * Puts the classes of a class hierarchy into the map. 
	 * @param startClass The root class of the hierarchy.
	 */
	private void putOntologyClassesIntoMap(final OntologyClass startClass) {
		for (int i = 0; i < startClass.getSubclassCount(); i++) {
			final OntologyClass ontologyClass = startClass.getSubClassAt(i);
			if (ontologyClass.hasSubclasses()) {
				putOntologyClassesIntoMap(ontologyClass);
			}
			ontologyClassesMap.addClass(ontologyClass);
		}
	}
	
	/**
	 * Returns the root class of the hierarchy of the base ontology. It contains already the classes of Premis and SKOS.
	 * @return The root class of the hierarchy.
	 */
	public OntologyClass getRootClassWithHierarchy() {
		return rootClass;
	}
	
	/**
	 * Determines the hierarchy of the base ontology, but starting with a set of given classes, not starting
	 * with the class Information_Package.
	 * @param classNames Contains the iris of the classes whose subclasses are requested.
	 * @return This class contains the classes of classNames and their subclasses.
	 */
	public OntologyClass getHierarchy(final StringList classNames) {
		final String namespace = rootClass.getNamespace();
		final String localName = rootClass.getLocalName();
		final OntologyClass ontologyClass = new OntologyClassImpl(namespace, localName);
		
		for (int i = 0; i < classNames.getStringCount(); i++) {
			final String classname = classNames.getStringAt(i);
			final OntologyClass foundClass = getClassByIri(classname);
			ontologyClass.addSubClass(foundClass);
		}
		
		return ontologyClass;
	}
	
	/**
	 * Determines a class by its iri.
	 * @param iri The iri of the class. 
	 * @return The found class. 
	 */
	public OntologyClass getClassByIri(final String iri) {
		return ontologyClassesMap.getOntologyClass(iri);
	}
	
	/**
	 * Determines the individuals of a class predefined in the base ontology. First, the cache is 
	 * checked if the individuals of this class have already been received from the server. If there have not been
	 * received, the server is asked for them. Then the individuals are saved in the cache.
	 * @param ontologyClass The class whose individuals shoud be retrieved.
	 * @return The list with the individual of ontologyClass that are defined in the base ontology and not created
	 * by the user.
	 */
	public OntologyIndividualCollection getIndividualsByClass(final OntologyClass ontologyClass) {
		CachedIndividualList cachedIndividualList = individualListMap.get(ontologyClass);
		final long updateTimeInMilliseconds = serviceFacade.getIndividualsUpdateTime();
		
		if (cachedIndividualList == null) {
			cachedIndividualList = new CachedIndividualList();
		}

		OntologyIndividualCollection individualList = cachedIndividualList.getOntologyIndividualList();
		final LocalTime now = DateTimeUtils.getNowTime();
		
		if (haveIndividualsToBeUpdated(cachedIndividualList, updateTimeInMilliseconds)) {
			individualList = updateIndividualsFromServer(ontologyClass);
			cachedIndividualList.setOntologyIndividualList(individualList);
			cachedIndividualList.setTimestamp(now);
			individualListMap.put(ontologyClass, cachedIndividualList);
		} 
		
		return individualList;
	}
	
	/**
	 * Creates a list with all classes in the ontology. 
	 * @return A list with all classes in the ontology.
	 */
	public OntologyClassList findAllOntologyClasses() {
		final OntologyClassList ontologyClasses = ontologyClassesMap.findAllOntologyClasses();
		return ontologyClasses;
	}
	
	/**
	 * Requests the individuals of an ontology class from the server.
	 * @param ontologyClass The class whose individuals we want to see.
	 * @return The individuals of the class that have been found on the server.
	 */
	private OntologyIndividualCollection updateIndividualsFromServer(final OntologyClass ontologyClass ) {
		final OntologyIndividualCollection individualList = new OntologyIndividualCollectionImpl();
		OntologyIndividualCollection individualsFromServer = serviceFacade.
				getPredefinedIndividualsByClass(ontologyClass);
		individualList.appendIndividualList(individualsFromServer);
		individualsFromServer = serviceFacade.getSavedIndividualsByClass(ontologyClass);
		individualList.appendIndividualList(individualsFromServer);
		
		return individualList;
	}
	
	
	/**
	 * Checks if the individuals have to be updated from the server. cachedIndividuals contains a time stamp that is 
	 * compared the updateTimeInMilliseconds.
	 * @param cachedIndividuals The individuals that eventually have to be updated.
	 * @param updateTimeInMilliseconds The time in milliseconds after that the individuals have to be updated.
	 * @return True, if updateTimeInMilliseconds == 0 or if the current time is after the time stamp of cachedIndividuals
	 * + updateTimeInMilliseconds. 
	 */
	private boolean haveIndividualsToBeUpdated(final CachedIndividualList cachedIndividuals, final long updateTimeInMilliseconds) {
		final LocalTime lastUpdatedTime = cachedIndividuals.getTimestamp();
		if (lastUpdatedTime == null) {
			return true;
		}
		
		if (updateTimeInMilliseconds == 0) {
			return true;
		}
		
		final LocalTime nextUpdateTime = lastUpdatedTime.plus(updateTimeInMilliseconds, ChronoUnit.MILLIS);
		final LocalTime currentTime = DateTimeUtils.getNowTime();
		
		if (currentTime.isBefore(nextUpdateTime)) {
			return false;
		} else {
			return true;
		}
	}
	
	public OntologyIndividualCollection getIndividualsByClass(final String iriOfOntologyClass) {
		final String iri = StringUtils.deleteDoubleSharps(iriOfOntologyClass);
		final OntologyClass ontologyClass = getClassByIri(iri);
		return getIndividualsByClass(ontologyClass);
	}
	
	/**
	 * Looks for a certain individual in the cache. The individual is determined by 
	 * its uuid and its class.
	 * @param uuid The uuid of the individual to look for.
	 * @param ontologyClass The class of the individual.
	 * @return The found individual.
	 */
	public OntologyIndividual getIndividual(final Uuid uuid, final OntologyClass ontologyClass) {
		final CachedIndividualList cachedIndividualList = individualListMap.get(ontologyClass);
		OntologyIndividual ontologyIndividual = null;
		boolean found = false;
		int i = 0;
		
		if (cachedIndividualList == null) {
			return ClientModelFactory.createOntologyIndividual(ontologyClass);
		} else {
			while (i < cachedIndividualList.getIndividualCount() && found == false) {
				ontologyIndividual = cachedIndividualList.getOntologyIndividual(i);
				Uuid individualsUuid = ontologyIndividual.getUuid();
				
				if (individualsUuid.equals(uuid)) {
					found = true;
				} else {
					i++;
				}
			}
		}
		
		return ontologyIndividual;
	}
	
	/**
	 * Looks for the data properties of a given class. If the properties have not been requested yet, they are
	 * now requested from the server.
	 * @param classname The class to look for.
	 * @return The data properties of this class.
	 */
	public DatatypePropertyCollection getDatatypePropertiesListByClassname(final String classname) {
		DatatypePropertyCollection datatypeProperties = classesDatatypePropertiesMap.get(classname);
		
		if (datatypeProperties == null) {
			datatypeProperties = ClientModelFactory.createEmptyDatatypePropertyList();
			final OntologyClass classWithDatatypeProperties = serviceFacade.getPropertiesOfClass(classname);
			for (int i = 0; i < classWithDatatypeProperties.getDatatypePropertiesCount();i++) {
				final OntologyDatatypeProperty ontologyDatatypeProperty = classWithDatatypeProperties.getDatatypeProperty(i);
				datatypeProperties.addDatatypeProperty(ontologyDatatypeProperty);
				propertiesMap.put(ontologyDatatypeProperty.getPropertyId(), ontologyDatatypeProperty);
			}
			
			classesDatatypePropertiesMap.put(classname, datatypeProperties);
			
			if (datatypeProperties != null) {
				final OntologyClass ontologyClass = getClassByIri(classname);
				
				for (int i = 0; i < datatypeProperties.getPropertiesCount(); i++) {
					final OntologyDatatypeProperty datatypeProperty = datatypeProperties.getDatatypeProperty(i);
					propertiesAndTheirClassesMap.addClassToProperty(datatypeProperty, ontologyClass);
				}
			}
		}
		return datatypeProperties;
	}
	
	/**
	 * Determine a property by its iri. 
	 * @param propertyIri The iri of the property.
	 * @return The found property. 
	 */
	public OntologyProperty getPropertyByIri(final String propertyIri) {
		return propertiesMap.get(propertyIri);
	}

	/**
	 * Looks for the object properties of a given class. If the properties have not been requested yet, they are
	 * now requested from the server.
	 * @param classname The class to look for.
	 * @return The object properties of this class.
	 */
	public ObjectPropertyCollection getObjectPropertiesByClassname(final String classname) {
		ObjectPropertyCollection objectProperties = objectPropertiesOfClassMap.get(classname);
		
		if (objectProperties == null) {
			objectProperties = ClientModelFactory.createObjectPropertyList();
			final OntologyClass classWithProperties = serviceFacade.getPropertiesOfClass(classname);
			for (int i = 0; i < classWithProperties.getObjectPropertiesCount();i++) {
				final OntologyObjectProperty ontologyObjectProperty = classWithProperties.getObjectProperty(i);
				objectProperties.addObjectProperty(ontologyObjectProperty);
			}
			
			objectPropertiesOfClassMap.put(classname, objectProperties);
		}
		return objectProperties;
		
	}
	
	/**
	 * Saves an individual in the information package on the server. The individual is also put in the 
	 * cache. 
	 * @param individual The individual that should be saved.
	 */
	public void saveIndividualInServerAndCache(final OntologyIndividual individual) {
		saveObjectProperties(individual);
		serviceFacade.saveIndividual(individual);
		
		// Put the new individual into the cache.
		final OntologyClass ontologyClass = individual.getOntologyClass();
		final OntologyIndividual copyOfOntologyIndividual = informationPackageManager.createCopyOfOntologyIndividual(individual);
		
		CachedIndividualList cachedIndividualList = individualListMap.get(ontologyClass);
		if (cachedIndividualList == null) {
			final OntologyIndividualCollection ontologyIndividualList = ClientModelFactory.createEmptyOntologyIndividualList();
			cachedIndividualList = new CachedIndividualList(ontologyIndividualList, null);
		} else {
			cachedIndividualList.getOntologyIndividualList().addIndividual(copyOfOntologyIndividual);
			individualListMap.put(ontologyClass, cachedIndividualList);
			individualsAndUuidsMap.addIndividual(copyOfOntologyIndividual);
		}
	}
	
	/**
	 * Checks if there are values of the object properties of an individual and saves them
	 * into the individual. 
	 * @param individual The individual that contains the object properties we want to
	 * assign some objects to. 
	 */
	private void saveObjectProperties(final OntologyIndividual individual) {
		for (int i = 0; i < individual.getObjectPropertiesCount(); i++) {
			final OntologyObjectProperty ontologyObjectProperty = individual.getObjectPropertyAt(i);
			final String iriOfObjectProperty = ontologyObjectProperty.getPropertyId();
			final Optional<OntologyIndividualCollection> optionalWithValueOfObjectProperty = informationPackageManager.
					getIndividualsOfObjectProperty(iriOfObjectProperty);
			optionalWithValueOfObjectProperty.ifPresent(ontologyIndividuals -> {
				ontologyObjectProperty.setPropertyValue(ontologyIndividuals);
			});
		}
	}
	
	/**
	 * Updates an individual with new values on the server and it's updated in the cache too. 
	 * @param individual The individual with the modified values.
	 */
	public void updateIndividualInServerAndCache(final OntologyIndividual individual) {
		serviceFacade.updateIndividual(individual);
		
		final OntologyClass ontologyClass = individual.getOntologyClass();
		final CachedIndividualList cachedIndividualList = individualListMap.get(ontologyClass);
		cachedIndividualList.replaceIndividual(individual);
		individualsAndUuidsMap.replaceIndividual(individual);
	}

	/**
	 * Determines the classes that are assigned to a certain property. 
	 * @param propertyIri The iri of the property.
	 * @return The classes that are assigned to this property or an empty Optional.
	 */
	public Optional<OntologyClassList> getClassesOfProperty(final String propertyIri) {
		return propertiesAndTheirClassesMap.getOntologyClassesOfProperty(propertyIri);
	}
	
	/**
	 * Finds a taxonomy in the collection with the help of its root individual. 
	 * @param rootIndividual The individual that is the root of the taxonomy we are looking for.
	 * @return The found taxonomy or an empty optional.
	 */
	public Optional<Taxonomy> getTaxonomyByRootIndividual(TaxonomyIndividual rootIndividual) {
		return taxonomies.getTaxonomyByRootIndividual(rootIndividual);
	}
	
	public ServiceFacade getServiceFacade() {
		return serviceFacade;
	}
	
	// ************** Inner classes ****************
	/**
	 * Represents a list with individuals. Contains a timestamp when the individuals were
	 * requested from the server the last time. Is used to determine if individuals must be
	 * requested from the server. 
	 * @author Christopher Olbertz
	 *
	 */
	private class CachedIndividualList {
		/**
		 * The list with the individuals.
		 */
		private OntologyIndividualCollection ontologyIndividualList;
		/**
		 * The imestamp when the individuals were
		 * requested from the server the last time. 
		 */
		private LocalTime timestamp;
		
		public CachedIndividualList() {
			super();
			this.ontologyIndividualList = ClientModelFactory.createEmptyOntologyIndividualList();
		}
		
		public CachedIndividualList(final OntologyIndividualCollection ontologyIndividualList, 
				final LocalTime timestamp) {
			super();
			this.ontologyIndividualList = ontologyIndividualList;
			this.timestamp = timestamp;
		}
		
		public void replaceIndividual(final OntologyIndividual ontologyIndividual) {
			ontologyIndividualList.replaceIndividual(ontologyIndividual);
		}
		
		public OntologyIndividual getOntologyIndividual(final int index) {
			return ontologyIndividualList.getIndividual(index);
		}
		
		public int getIndividualCount() {
			return ontologyIndividualList.getIndiviualsCount();
		}

		public OntologyIndividualCollection getOntologyIndividualList() {
			return ontologyIndividualList;
		}

		public LocalTime getTimestamp() {
			return timestamp;
		}

		public void setOntologyIndividualList(final OntologyIndividualCollection ontologyIndividualList) {
			this.ontologyIndividualList = ontologyIndividualList;
		}

		public void setTimestamp(final LocalTime timestamp) {
			this.timestamp = timestamp;
		}
	}

	/**
	 * Checks if a certain class is the class the taxonomies start with.
	 * @param ontologyClass The class we want to check.
	 * @return True, if ontologyClass is equal the class the taxonomies start with, false otherwise.
	 */
	public boolean isTaxonomyStartClass(final OntologyClass ontologyClass) {
		if (taxonomyStartClass.equals(ontologyClass)) {
			return true;
		} else {
			return false;
		}
	}
}