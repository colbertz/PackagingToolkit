package de.feu.kdmp4.packagingtoolkit.client.service.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.MediatorService;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;

public class MediatorServiceImpl implements MediatorService {
	private OperationsFacade operationsFacade;
	private ClientModelFactory clientModelFactory;
	private ClientResponseFactory clientResponseFactory;
	
	@Override
	public DataSourceCollection findAllDataSources() {
		ClientConfigurationData clientConfiguration = operationsFacade.getClientConfigurationData();
		String mediatorUrl = clientConfiguration.getMediatorUrl();
		operationsFacade.pingMediator(mediatorUrl);
		operationsFacade.checkDataSourceInstallations(mediatorUrl);
		DataSourceListResponse dataSourceListResponse = operationsFacade.readDataSources(mediatorUrl);
		DataSourceCollection dataSources = clientModelFactory.createDataSourceCollection(dataSourceListResponse);
		return dataSources;
	}
	
	@Override
	public void setOperationsFacade(OperationsFacade operationsFacade) {
		this.operationsFacade = operationsFacade;
	}
	
	@Override
	public void setClientModelFactory(ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}
	
	@Override
	public MediatorDataCollection collectMetadata(File file) {
		ClientConfigurationData clientConfiguration = operationsFacade.getClientConfigurationData();
		String mediatorUrl = clientConfiguration.getMediatorUrl();
		MediatorDataListResponse mediatorDataListResponse = operationsFacade.collectData(file, mediatorUrl);
		MediatorDataCollection mediatorDataCollection = clientModelFactory.createMediatorDataCollection(mediatorDataListResponse);
		return mediatorDataCollection;
	}
	
	@Override
	public void checkDataSourceInstallations() {
		ClientConfigurationData clientConfiguration = operationsFacade.getClientConfigurationData();
		String mediatorUrl = clientConfiguration.getMediatorUrl();
		operationsFacade.checkDataSourceInstallations(mediatorUrl);
	}
	
	@Override
	public void saveDataSourceConfiguration(DataSourceCollection dataSources) {
		ClientConfigurationData clientConfiguration = operationsFacade.getClientConfigurationData();
		String mediatorUrl = clientConfiguration.getMediatorUrl();
		DataSourceListResponse dataSourceListResponse = clientResponseFactory.toResponse(dataSources);
		operationsFacade.saveDataSourceConfiguration(mediatorUrl, dataSourceListResponse);
	}
	
	@Override
	public void pingMediator() {
		ClientConfigurationData clientConfiguration = operationsFacade.getClientConfigurationData();
		String mediatorUrl = clientConfiguration.getMediatorUrl();
		operationsFacade.pingMediator(mediatorUrl);
	}
	
	@Override
	public void setClientResponseFactory(ClientResponseFactory clientResponseFactory) {
		this.clientResponseFactory = clientResponseFactory;
	}
}
