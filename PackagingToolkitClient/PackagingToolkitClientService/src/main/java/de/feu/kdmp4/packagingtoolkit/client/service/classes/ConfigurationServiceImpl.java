package de.feu.kdmp4.packagingtoolkit.client.service.classes;

import java.io.File;
import java.io.IOException;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.MediatorConfigurationDataImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MediatorConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.ServiceMain;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ConfigurationService;
import de.feu.kdmp4.packagingtoolkit.client.utils.StandAloneData;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.ExtractorToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;

/**
 * An implementation of {@see de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ConfigurationService}
 * with property files.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationServiceImpl implements ConfigurationService {
	/**
	 * The file that contains the base ontology. It lies in the temporary
	 * directory of the client.
	 */
	private File baseOntologyFile;
	/**
	 * Contains the methods for accessing the operations layer.
	 */
	private OperationsFacade operationsFacade;
	
	@Override
	public ClientConfigurationData getClientConfigurationData() {
		return operationsFacade.getClientConfigurationData();
	}	
	
	@Override
	public File getLogo() {
		final File file = new File(".");
		return operationsFacade.getLogoPath(file.getAbsolutePath());
	}
	
	@Override
	public void saveLogo(final File temporaryLogoFile) {
		final File file = new File(".");
		operationsFacade.saveLogo(temporaryLogoFile, file.getAbsolutePath());
	}
	
	@Override 
	public MediatorConfigurationData getMediatorConfigurationData() {
		ExtractorToolListResponse extractorToolList = null;
		if (ServiceMain.isMockMediator()) {
			extractorToolList = StandAloneData.getExtractorToolList();
		} else {
			
		}
		
		final MediatorConfigurationData configurationData = new MediatorConfigurationDataImpl(extractorToolList);
		return configurationData;
	}
	
	@Override
	public ServerConfigurationData readServerConfiguration() {
		// Read the client configuration for getting the server url.
		final ClientConfigurationData clientConfigurationData = operationsFacade.
				getClientConfigurationData();
		final String serverUrl = clientConfigurationData.getServerUrl();
		// Read the configuration from the server.
		final ServerConfigurationDataResponse serverConfigurationResponse = operationsFacade.
				readServerConfiguration(serverUrl);
		
		final ServerConfigurationData serverConfigurationData = createServerConfigurationDataForDisplayingInGui(serverConfigurationResponse);
		return serverConfigurationData;
	}
	
	/**
	 * Creates the configuration data for the server with the data send by the server.
	 * @param serverConfigurationResponse The data send by the server.
	 * @return The data for being processed by the client.
	 */
	private ServerConfigurationData createServerConfigurationDataForDisplayingInGui(final ServerConfigurationDataResponse serverConfigurationResponse) {
		final String workingDirectoryPath = serverConfigurationResponse.getWorkingDirectoryPath();
		final String ruleOntologyPath = serverConfigurationResponse.getConfiguredRuleOntologyPath();
		final String baseOntologyPath = serverConfigurationResponse.getConfiguredBaseOntologyPath();
		
		final ServerConfigurationData serverConfigurationData = ClientModelFactory.
				createServerConfigurationData(workingDirectoryPath);
		serverConfigurationData.setConfiguredBaseOntologyPath(baseOntologyPath);
		serverConfigurationData.setConfiguredRuleOntologyPath(ruleOntologyPath);
		
		return serverConfigurationData;
	}
	
	@Override
	public void writeClientConfigurationData(final ClientConfigurationData 
			clientConfigurationData) {
		try {
			operationsFacade.writeClientConfigurationData(clientConfigurationData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public File getBaseOntologyFile() {
		// The base ontology file has not been downloaded from the server.
		if (baseOntologyFile == null || !baseOntologyFile.exists()) {
			final File temporaryDirectory = operationsFacade.getTemporaryDirectoryOnClient();
			final ClientConfigurationData clientConfigurationData = operationsFacade.getClientConfigurationData();
			final String serverUrl = clientConfigurationData.getServerUrl();
			baseOntologyFile = operationsFacade.downloadBaseOntology(serverUrl, temporaryDirectory); 
		}
		
		return baseOntologyFile; 
	}
	
	@Override 
	public void writeServerConfigurationData(final ServerConfigurationData configurationData) {
		// What was entered by the user?
		final String workingDirectoryPath = configurationData.getWorkingDirectoryPath();
		final String ruleOntology = configurationData.getConfiguredRuleOntologyPath();
		final String baseOntology = configurationData.getConfiguredBaseOntologyPath();
		final String baseOntologyClass = configurationData.getBaseOntologyClass();
		
		// We create an model object for sending to the server.
		final ServerConfigurationDataResponse serverConfigurationData = ResponseModelFactory.
				getServerConfigurationData(baseOntology, ruleOntology, workingDirectoryPath, baseOntologyClass);
		// Send the configuration to the server.
		final String serverUrl = getServerUrl();
		operationsFacade.writeServerConfiguration(serverConfigurationData, serverUrl);
		// Is there a base ontology to configure?
		final File baseOntologyFile = configurationData.getBaseOntology(); 
		if (baseOntologyFile != null) {
			operationsFacade.configureBaseOntology(baseOntologyFile, serverUrl);
			operationsFacade.updateOntologyCache(serverUrl);
			//operationsFacade.getHierarchy(serverl);
		}
		
		// Is there a rule ontology to configure?
		final File ruleOntologyFile = configurationData.getRuleOntology(); 
		if (ruleOntologyFile != null) {
			operationsFacade.configureRuleOntology(ruleOntologyFile, serverUrl);
		}
	}
	
	/**
	 * Determines the url the service is accessible with.
	 * @return The url of the server.
	 */
	private String getServerUrl() {
		final ClientConfigurationData clientConfigurationData = operationsFacade.
				getClientConfigurationData();
		return clientConfigurationData.getServerUrl();
	}

	@Override
	public void setOperationsFacade(final OperationsFacade operationsFacade) {
		this.operationsFacade = operationsFacade;
	}

	@Override
	public void pingServer() {
		final String serverUrl = operationsFacade.getServerUrl();
		operationsFacade.pingServer(serverUrl);
	}

	@Override
	public long getIndividualsUpdateTimePath() {
		final File file = new File(".");
		return operationsFacade.getIndividualsUpdateTimePath(file.getAbsolutePath());
	}
}
