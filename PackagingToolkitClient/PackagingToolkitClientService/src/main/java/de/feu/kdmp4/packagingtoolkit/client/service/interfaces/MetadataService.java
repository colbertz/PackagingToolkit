package de.feu.kdmp4.packagingtoolkit.client.service.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElementList;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.MediatorCommunicationOperations;

public interface MetadataService {
	/**
	 * Gets the metadata list from the mediator. Gets an object of  
	 * {@link de.feu.kdmp4.packagingtoolkit.output.classes.MetadataElementListOutput} and
	 * converts it to an object of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElementList}.
	 * @return
	 */
	MetadataElementList getMetadataFromMediator();

	void setMetadataOperations(MediatorCommunicationOperations metadataOperations);

}
