package de.feu.kdmp4.packagingtoolkit.client.service.interfaces;

import java.io.InputStream;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ViewList;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;

public interface ArchiveService {
	/**
	 * Deletes an information package from the server.
	 * @param archive The archive that we want to delete.
	 * @return The archives that remain on the server.
	 */
	ArchiveList deleteArchive(final Archive archive);
	/**
	 * Determines a collection with all archives saved on the server.
	 * @return A collection with all archives saved on the server.
	 */
	ArchiveList getArchiveList();
	/**
	 * Determines a collection with all views saved on the server.
	 * @return A collection with all views saved on the server.
	 */
	ViewList getAllViews();
	/**
	 * Saves a new view on the server. 
	 * @param view The view that should be saved.
	 */
	void saveNewView(final View view);
	/**
	 * Loads the archive the user wants to edit.
	 * @param archive The archive that should be loaded.
	 */
	void loadArchiveForEditing(final Archive archive);
	/**
	 * Loads the digital objects of the information package saved in the information package
	 * manager from the server.
	 * @return The list with the digital objects of the information package processed. 
	 */
	DigitalObjectCollection loadDigitalObjectsOfInformationPackage();
	/**
	 * Sets the reference to the object that contains important information about the information package that 
	 * is being created.
	 * @param informationPackageManager
	 */
	void setInformationPackageManager(final InformationPackageManager informationPackageManager);
	/**
	 * Sets a reference to the facade to the operations layer of the client. 
	 * @param operationsFacade A reference to the facade to the operations layer of the client.
	 */
	void setOperationsFacade(final OperationsFacade operationsFacade);
	/**
	 * Sets a reference to the object that creates the model objects of the client. 
	 * @param clientModelFactory A reference to the object that creates the model objects of the client.
	 */
	void setClientModelFactory(final ClientModelFactory clientModelFactory);
	/**
	 * A reference to the object that creates the object for sending via http.
	 * @param clientResponseFactory A reference to the object that creates the object for sending via http.
	 */
	void setClientResponseFactory(final ClientResponseFactory clientResponseFactory);
	/**
	 * Creates a new submission information package that is administrated by the information package manager.
	 * @param title The title of the submission information package.
	 */
	void createSubmissionInformationPackage(final String title);
	/**
	 * Creates a new submission information unit that is administrated by the information package manager.
	 * @param title The title of the submission information unit.
	 */
	void createSubmissionInformationUnit(final String title);
	/**
	 * Request the zip file of an archive for download.
	 * @param archive Describes the archive and the required information for building the zip
	 * file.
	 * @return An input stream that contains the data of the files for download.
	 */
	InputStream downloadArchiveFile(final Archive archive);
	/**
	 * Adds a reference to the information package that is currently in process.
	 * @param reference The reference that should be added to the information package.
	 */
	void addRemoteFileReference(final Reference reference);
	/**
	 * Adds a file reference to the information package that is currently in process.
	 * @param digitalObject The digital object representing the file the reference is pointing to.
	 */
	void addLocalFileReference(final DigitalObject digitalObject);
	/**
	 * Saves the information package the user is working with on the server.
	 */
	void saveCurrentInformationPackage();
	/**
	 * Finds all references of the information package that is currently in process.
	 * @return All references assigned to the information package currently in process or an empty list if the 
	 * information package does not contain any references.
	 */
	ReferenceCollection findReferencesOfInformationPackage();
	/**
	 * Is called if the creation process of the current information package has been cancelled. The server will be informed.
	 */
	void cancelInformationPackageCreation();
	/**
	 * Loads an information package so that the user can edit it.
	 * @param archive The archive representing the information package that should be load. 
	 */
	void loadInformationPackage(final Archive archive);
	/**
	 * Deletes a view from the server.
	 * @param viewId The id of the view to delete.
	 */
	void deleteView(final int viewId);
	void assignOrUnassignIndividualToInformationPackage(Uuid uuidOfIndividual, Uuid uuidOfInformationPackage);
	/**
	 * Determines all individuals that are assigned to a given information package.
	 * @param uuidOfInformationPackage The uuid of the information package whose individuals 
	 * we want to determine.
	 * @return All individuals that are assigned to this information package.
	 */
	OntologyIndividualCollection findAllIndividualsOfInformationPackage(Uuid uuidOfInformationPackage);
	/**
	 * Sends the uuids of some indidivuals to the server. These individuals should be assigned to
	 * a certain information package.
	 * @param uuids Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals.
	 */
	void assignIndividualsToInformationPackage(UuidList uuids);
	/**
	 * Sends the uuids of some indidivuals to the server. These individuals should be unassigned from
	 * a certain information package.
	 * @param uuids Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals.
	 */
	void unassignIndividualsFromInformationPackage(UuidList uuids);
	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param taxonomyIndividuals The individuals from the taxonomy that should be 
	 * assigned to the information package.
	 * @param iriOfInformationPackage The iri of the information package the individuals should be 
	 * assigned to. 
	 */
	void assignTaxonomyIndividuals(TaxonomyIndividualCollection taxonomyIndividuals, Iri iriOfInformationPackage);
	/**
	 * Requests all taxonomy individuals that are assigned to a certain information package from 
	 * the server.
	 * @param uuidOfInformationPackage The uuid of the information package whose taxonomy 
	 * individuals we want to request.
	 * @return The iris of the taxonomy individuals that are assigned to the information package.
	 */
	IriCollection findAllAssignedTaxonomyIndividuals(Uuid uuidOfInformationPackage);
}
