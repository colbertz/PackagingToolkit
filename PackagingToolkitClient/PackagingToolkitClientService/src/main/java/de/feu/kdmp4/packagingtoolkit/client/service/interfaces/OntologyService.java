package de.feu.kdmp4.packagingtoolkit.client.service.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

public interface OntologyService {
	/**
	 * Determines the class hierarchy of the base ontology. 
	 * @return The top class of the hierarchy. It contains subclasses and provides
	 * access to the entire hierarchy. 
	 */
	public OntologyClass getHierarchyOfBaseOntology();
	/**
	 * Determines the class hierarchy in the base ontology for some classes. 
	 * @param ontologyClassNames The names of the classes whose subclasses have to be 
	 * determined.
	 * @return The configured root class. Its subclasses are the classes whose names
	 * were given to this method. They contain their hierarchy.
	 */
	public OntologyClass getHierarchyOfBaseOntology(final StringList ontologyClassNames);
	/**
	 * Determines the properties of a given class.
	 * @param classname The name of the class.
	 * @return A list with the properties contained in this class.
	 */
	OntologyClass getPropertiesOfClass(final String classname);
	/**
	 * Saves an individual on the server. 
	 * @param ontologyIndividual The individual that should be saved.
	 */
	void saveIndividual(final OntologyIndividual ontologyIndividual);
	/**
	 * Determines all individuals that are predefined in the base ontology. 
	 * @param ontologyClass The ontology class we are interested in.
	 * @return All found individuals. 
	 */
	OntologyIndividualCollection findPredefinedIndividualsByClass(final OntologyClass ontologyClass);
	/**
	 * Sets the reference to the object that contains important information about the information package that 
	 * is being created.
	 * @param informationPackageManager
	 */
	void setInformationPackageManager(final InformationPackageManager informationPackageManager);
	/**
	 * Sets a reference to the facade to the operations layer of the client. 
	 * @param operationsFacade A reference to the facade to the operations layer of the client.
	 */
	void setOperationsFacade(final OperationsFacade operationsFacade);
	/**
	 * A reference to the object that creates the object for sending via http.
	 * @param clientResponseFactory A reference to the object that creates the object for sending via http.
	 */
	void setClientResponseFactory(final ClientResponseFactory clientResponseFactory);
	/**
	 * Sets a reference to the object that creates the model objects of the client. 
	 * @param clientModelFactory A reference to the object that creates the model objects of the client.
	 */
	void setClientModelFactory(final ClientModelFactory clientModelFactory);
	/**
	 * Updates an individual on the server. The individual already exists on the server and the user has entered
	 * new or updated data. 
	 * @param ontologyIndividual The individual with the new or updated data. 
	 */
	void updateIndividual(final OntologyIndividual ontologyIndividual);
	/**
	 * Determines the hierarchy of the Premis ontology.
	 * @return The root class of the Premis ontology. Contains all subclasses of Premis. 
	 */
	OntologyClass getHierarchyOfPremis();
	/**
	 * Determines the hierarchy of the SKOS ontology.
	 * @return The root class of the SKOS ontology. Contains all subclasses of SKOS. 
	 */
	OntologyClass getHierarchyOfSkos();
	/**
	 * Determines all individuals of a certain class on the server. 
	 * @param ontologyClass The class the individuals belong to. 
	 * @return All individuals on the server assigned to the class ontologyClass.
	 */
	OntologyIndividualCollection getSavedIndividualsByClass(final OntologyClass ontologyClass);
	/**
	 * Determines all taxonomies defined on the server. 
	 * @return All taxonomies found on the server.
	 */
	TaxonomyCollection findAllTaxonomies();
	/**
	 * Determines the class the taxonomies start with. This class has only one object property like hasTaxonomy. This object property
	 * references to the taxonomy.
	 * @return The class the taxonomies start with.
	 */
	OntologyClass getTaxonomyStartClass();
	/**
	 * Sends the taxonomy individuals the user has selected to the server for saving in the information package.
	 */
	void saveSelectedTaxonomyIndividuals();
}
