package de.feu.kdmp4.packagingtoolkit.client.service.classes;

import java.io.File;
import java.io.InputStream;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ViewList;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ArchiveService;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

public class ArchiveServiceImpl implements ArchiveService {
	/**
	 * A reference to the information package manager that contains the information about the information package
	 * in process.
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * Encapsulates the operations layer.
	 */
	private OperationsFacade operationsFacade;
	/**
	 * Creates response objects for the client.
	 */
	private ClientResponseFactory clientResponseFactory;
	/**
	 * Creates model objects for the client.
	 */
	private ClientModelFactory clientModelFactory;
		
	@Override
	public void cancelInformationPackageCreation() {
		final Uuid uuid = informationPackageManager.getUuid();
		if (uuid != null) {
			final String serverUrl = operationsFacade.getServerUrl();
			operationsFacade.cancelInformationPackageCreation(uuid, serverUrl);
			informationPackageManager.setTitle("");
		}
	}
	
	@Override
	public ReferenceCollection findReferencesOfInformationPackage() {
		final String serverUrl = operationsFacade.getServerUrl();
		final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
		final ReferenceListResponse referenceList = operationsFacade.findReferencesOfInformationPackage(serverUrl, uuidOfInformationPackage.toString());
		
		ReferenceCollection references = null;
		if (referenceList != null) {
			references = ClientModelFactory.createReferenceCollection(referenceList);
		} else {
			references = ClientModelFactory.createEmptyReferenceCollection();
		}
		
		return references;
	}
	
	@Override
	public void loadInformationPackage(final Archive archive) {
		final String serverUrl = operationsFacade.getServerUrl();
		final String uuidOfInformationPackage = archive.getUuid().toString();
		operationsFacade.loadInformationPackage(uuidOfInformationPackage, serverUrl);
		final OntologyIndividualCollection ontologyIndividuals = findAllIndividualsOfInformationPackage(archive.getUuid());
		informationPackageManager.setIndividualsOfInformationPackage(ontologyIndividuals);
	}
	
	@Override
	public void saveCurrentInformationPackage() {
		final String serverUrl = operationsFacade.getServerUrl();
		final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
		final UuidResponse uuidOfInformationPackageResponse = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		operationsFacade.saveInformationPackage(uuidOfInformationPackageResponse, serverUrl);
	}
	
	@Override
	public void addRemoteFileReference(final Reference reference) {
		final String serverUrl = operationsFacade.getServerUrl();
		final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
		
		final ReferenceResponse referenceResponse = clientResponseFactory.toResponse(reference);
		final UuidResponse uuidOfInformationPackageResponse = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		referenceResponse.setUuidOfInformationPackage(uuidOfInformationPackageResponse);
		operationsFacade.addHttpReference(referenceResponse, serverUrl);
	}
	
	@Override
	public void addLocalFileReference(final DigitalObject digitalObject) {
		final DigitalObjectResponse digitalObjectResponse = clientResponseFactory.toResponse(digitalObject);
		final String serverUrl = operationsFacade.getServerUrl();
		final ClientConfigurationData clientConfiguration = operationsFacade.getClientConfigurationData();
		final String mediatorUrl = clientConfiguration.getMediatorUrl();
		digitalObjectResponse.setMediatorUrl(mediatorUrl);
		
		boolean referenceCreated = operationsFacade.addFileReference(digitalObjectResponse, serverUrl);
		final String filepath = digitalObjectResponse.getFilename();
		final File file = new File(filepath);
		
		if (referenceCreated == false) {
			operationsFacade.uploadeFileToStorage(file, serverUrl);
			referenceCreated = operationsFacade.addFileReference(digitalObjectResponse, serverUrl);
		}
		
		/*final MediatorDataListResponse mediatorData = operationsFacade.collectData(file, mediatorUrl);
		final UuidResponse uuidOfDigitalObject = new UuidResponse(digitalObject.getUuidOfDigitalObject().getUuid().toString());
		final UuidResponse uuidOfInformationPackage = new UuidResponse(digitalObject.getUuidOfInformationPackage().getUuid().toString());
		mediatorData.setUuidOfFile(uuidOfDigitalObject);
		mediatorData.setUuidOfInformationPackage(uuidOfInformationPackage);
		
		final String filename = PackagingToolkitFileUtils.extractFileName(filepath);
		mediatorData.setOriginalFileName(filename);
		operationsFacade.addMetadataToInformationPackage(serverUrl, mediatorData);*/
	}
	
	@Override
	public void createSubmissionInformationPackage(final String title) {
		final String serverUrl = operationsFacade.getServerUrl();
		final PackageResponse theSip = operationsFacade.createSubmissionInformationPackage(serverUrl, title);
		final Uuid uuid = PackagingToolkitModelFactory.getUuid(theSip.getUuid());
		informationPackageManager.setUuid(uuid);
	}
	
	@Override
	public void createSubmissionInformationUnit(final String title) {
		final String serverUrl = operationsFacade.getServerUrl();
		final PackageResponse theSiu = operationsFacade.createSubmissionInformationUnit(serverUrl, title);
		final Uuid uuid = PackagingToolkitModelFactory.getUuid(theSiu.getUuid());
		informationPackageManager.setUuid(uuid);
	}
	
	@Override
	public ArchiveList deleteArchive(final Archive archive) {
		final String serverUrl = operationsFacade.getServerUrl();
		operationsFacade.deleteArchive(archive.getUuid(), serverUrl);
		final ArchiveListResponse archiveListResponse = operationsFacade.getArchiveList(serverUrl);
		return ClientModelFactory.getArchiveList(archiveListResponse);
	}
	
	@Override
	public ArchiveList getArchiveList() {
		final String serverUrl = operationsFacade.getServerUrl();
		final ArchiveListResponse archiveListResponse = operationsFacade.getArchiveList(serverUrl);
		return ClientModelFactory.getArchiveList(archiveListResponse);
	}

	@Override
	public ViewList getAllViews() {
		final String serverUrl = operationsFacade.getServerUrl();
		final ViewListResponse viewListResponse = operationsFacade.getAllViews(serverUrl);
		final ViewList viewList = clientModelFactory.createViewList(viewListResponse); 
		return viewList;
	}
	
	@Override
	public void deleteView(final int viewId) {
		final String serverUrl = operationsFacade.getServerUrl();
		operationsFacade.deleteView(viewId, serverUrl);
	}
	
	@Override
	// DELETE_ME
	public void loadArchiveForEditing(final Archive archive) {
		informationPackageManager.loadArchive(archive);
		final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
		final OntologyIndividualCollection ontologyIndividuals = findAllIndividualsOfInformationPackage(uuidOfInformationPackage);
		informationPackageManager.setIndividualsOfInformationPackage(ontologyIndividuals);
	}
	
	@Override
	public DigitalObjectCollection loadDigitalObjectsOfInformationPackage() {
		DigitalObjectCollection digitalObjectList = clientModelFactory.getEmptyDigitalObjectList();
		
		final Uuid uuid = informationPackageManager.getUuid();
		final UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(uuid);
		DigitalObjectListResponse digitalObjectListResponse = operationsFacade.getDigitalObjectsOfInformationPackage(uuidResponse);
		digitalObjectList = ClientModelFactory.getDigitalObjectList(digitalObjectListResponse);
		
		return digitalObjectList;
	}
	
	@Override
	public InputStream downloadArchiveFile(final Archive archive) {
		final String serverUrl = operationsFacade.getServerUrl();
		final UuidResponse uuid = ResponseModelFactory.getUuidResponse(archive.getUuid());
		final ArchiveResponse archiveResponse = ResponseModelFactory.getArchiveResponse(archive.getArchiveId(), 
				archive.getPackageType(), archive.getSerializationFormat(), archive.getTitle(), uuid,  archive.getCreationDate(),
				archive.getLastModificationDate());
		
		if (archive.isVirtual()) {
			return operationsFacade.downloadVirtualArchive(archiveResponse, serverUrl);
		} else {
			return operationsFacade.downloadNotVirtualArchive(archiveResponse, serverUrl);
		}
	}
	
	@Override
	public void saveNewView(final View view) {
		final String serverUrl = operationsFacade.getServerUrl();
		final ViewResponse viewResponse = clientResponseFactory.fromView(view);
		operationsFacade.saveNewView(serverUrl, viewResponse);
	}

	@Override
	public void setInformationPackageManager(InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}

	@Override
	public void setOperationsFacade(final OperationsFacade operationsFacade) {
		this.operationsFacade = operationsFacade;
	}

	@Override
	public void setClientResponseFactory(final ClientResponseFactory clientResponseFactory) {
		this.clientResponseFactory = clientResponseFactory;
	}

	@Override
	public void setClientModelFactory(final ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}

	// DELETE_ME
	@Override
	public void assignOrUnassignIndividualToInformationPackage(Uuid uuidOfIndividual, Uuid uuidOfInformationPackage) {
		/*final UuidResponse uuidOfIndividualResponse = ResponseModelFactory.getUuidResponse(uuidOfIndividual);
		final UuidResponse uuidOfInformationPackageResponse = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		operationsFacade.assignOrUnassignIndividualToInformationPackage(uuidOfIndividualResponse, 
				uuidOfInformationPackageResponse);*/
	}
	
	@Override
	public void assignIndividualsToInformationPackage(final UuidList uuids) {
		final String serverUrl = operationsFacade.getServerUrl();
		final UuidListResponse uuidsOfIndividualResponse = ResponseModelFactory.createUuidListResponse(uuids);
		operationsFacade.assignIndividualsToInformationPackage(uuidsOfIndividualResponse, serverUrl);
	}
	
	@Override
	public void unassignIndividualsFromInformationPackage(final UuidList uuids) {
		final String serverUrl = operationsFacade.getServerUrl();
		final UuidListResponse uuidsOfIndividualResponse = ResponseModelFactory.createUuidListResponse(uuids);
		operationsFacade.unassignIndividualsFromInformationPackage(uuidsOfIndividualResponse, serverUrl);
	}
	
	@Override
	public OntologyIndividualCollection findAllIndividualsOfInformationPackage(final Uuid uuidOfInformationPackage) {
		final UuidResponse uuidOfInformationPackageResponse = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		final String serverUrl = operationsFacade.getServerUrl();
		
		final IndividualListResponse individualListResponse = operationsFacade.findAllIndividualsOfInformationPackage(
				serverUrl, uuidOfInformationPackageResponse);
		if (individualListResponse != null) {
			final OntologyIndividualCollection ontologyIndividualList = clientModelFactory.createOntologyIndividualList(individualListResponse, informationPackageManager);
			return ontologyIndividualList;
		} else {
			return ClientModelFactory.createEmptyOntologyIndividualList();
		}
	}
	
	@Override
	public void assignTaxonomyIndividuals(final TaxonomyIndividualCollection taxonomyIndividuals, 
										  final Iri iriOfInformationPackage) {
		final String serverUrl = operationsFacade.getServerUrl();
		final IriListResponse irisOfTaxonomyIndividuals = ResponseModelFactory.createEmptyIriListResponse();
		final String namespaceOfInformationPackage = iriOfInformationPackage.getNamespace().toString();
		final String localNameOfInformationPackage = iriOfInformationPackage.getLocalName().toString();
		final IriResponse iriResponseOfInformationPackage = ResponseModelFactory.createIriResponse(
				namespaceOfInformationPackage, localNameOfInformationPackage);
		irisOfTaxonomyIndividuals.addIriToList(iriResponseOfInformationPackage); 
		
		for (int i = 0; i < taxonomyIndividuals.getSize(); i++) {
			final TaxonomyIndividual taxonomyIndividual = taxonomyIndividuals.getTaxonomyIndividual(i).get();
			final String namespace = taxonomyIndividual.getIri().getNamespace().toString();
			final String localName = taxonomyIndividual.getIri().getLocalName().toString();
			final IriResponse iriResponse = ResponseModelFactory.createIriResponse(namespace, localName);
			irisOfTaxonomyIndividuals.addIriToList(iriResponse);
		}
		
		operationsFacade.assignTaxonomyIndividuals(irisOfTaxonomyIndividuals, serverUrl);
	}

	@Override
	public IriCollection findAllAssignedTaxonomyIndividuals(final Uuid uuidOfInformationPackage) {
		final UuidResponse uuidOfInformationPackageResponse = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		final String serverUrl = operationsFacade.getServerUrl();
		final IriCollection iris = ClientModelFactory.createEmptyIriCollection();
		final IriListResponse individualListResponse = operationsFacade.findAllAssignedTaxonomyIndividuals(
				serverUrl, uuidOfInformationPackageResponse);
		if (individualListResponse != null) {
			for (int i = 0; i < individualListResponse.getIriCount(); i++) {
				final IriResponse iriOfIndividual = individualListResponse.getIriAt(i);
				final Iri iri = ClientModelFactory.createIri(iriOfIndividual.toString());
				iris.addIriToList(iri);
			}
		} 
		return iris;
	}
}
