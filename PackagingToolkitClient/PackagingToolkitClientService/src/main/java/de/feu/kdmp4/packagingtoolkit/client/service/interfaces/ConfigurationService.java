package de.feu.kdmp4.packagingtoolkit.client.service.interfaces;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MediatorConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;

/**
 * An interface for everything that has to do with saving and loading of the
 * configurations.
 * @author Christopher Olbertz
 *
 */
public interface ConfigurationService {
	/**
	 * Gets all configuration data for the client.
	 * @return All client configuration data.
	 */
	public ClientConfigurationData getClientConfigurationData();
	
	/**
	 * Gets all configuration data for the mediator.
	 * @return All client configuration data.
	 */
	public MediatorConfigurationData getMediatorConfigurationData();
	/**
	 * Persists configuration data.
	 * @param clientConfigurationData The configuration data to save.
	 * @throws FileNotFoundException Is thrown, when there is a problem with the
	 * configuration file. 
	 * @throws IOException 
	 */
	void writeClientConfigurationData(final ClientConfigurationData clientConfigurationData);
	/**
	 * Saves the configuration of the server.
	 * @param configurationData Contains the configuration data entered by the user.
	 */
	void writeServerConfigurationData(final ServerConfigurationData configurationData);
	/**
	 * Reads the configuration of the server.
	 * @return The configuration data of the server.
	 */
	ServerConfigurationData readServerConfiguration();
	/**
	 * Gets the file in the temporary directory that contains the base ontology.
	 * If the file does not exist in the temporary directory of the client, it
	 * is downloaded from the server. 
	 * @return The base ontology file in the temporary directory of the server.
	 */
	File getBaseOntologyFile();
	/**
	 * Sends a signal to the server for checking if the server is online. 
	 * @throws ConnectionException If the server is not accessible.
	 */
	void pingServer();
	/**
	 * Sets a reference to the object that contains the methods for accessing the operations layer.
	 * @param operationsFacade The facade for accessing the operations layer.
	 */
	void setOperationsFacade(final OperationsFacade operationsFacade);
	/**
	 * Determines the file that contains the logo.
	 * @return Contains the logo.
	 */
	File getLogo();
	/**
	 * Saves a new file for the logo.
	 * @param temporaryLogoFile The file with the logo in the temporary directory.
	 */
	void saveLogo(File temporaryLogoFile);
	/**
	 * Determines the time after that the individuals have to be updated from the server. 
	 * @return The time after that the individuals have to be updated from the server in milliseconds.
	 */
	long getIndividualsUpdateTimePath();
}
