package de.feu.kdmp4.packagingtoolkit.client.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceMain 
{
	/**
	 * Is used for deciding in the program if the communication via webservices is
	 * working or not. If set to true, the test data for the communication with
	 * the server are taken from static test lists. If set to false, the data 
	 * are accepted via web services. This constants
	 * and the respective get method are necessary for testing purposes.
	 */
	private static final boolean MOCK_SERVER = false;
	/**
	 * Is used for deciding in the program if the communication via webservices is
	 * working or not. If set to true, the test data for the communication with
	 * the mediator are taken from static test lists. If set to false, the data 
	 * are accepted via web services. This constants
	 * and the respective get method are necessary for testing purposes.
	 */
	private static final boolean MOCK_MEDIATOR = true;
	
	public static void main(String[] args) {
		SpringApplication.run(ServiceMain.class, args);
	}
	
	public static boolean isMockServer() {
		return MOCK_SERVER;
	}

	public static boolean isMockMediator() {
		return MOCK_MEDIATOR;
	}
}
