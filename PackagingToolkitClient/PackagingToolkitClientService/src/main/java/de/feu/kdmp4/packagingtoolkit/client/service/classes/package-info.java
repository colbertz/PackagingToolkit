/**
 * Contains the classes with the business logic of the client.
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.service.classes;