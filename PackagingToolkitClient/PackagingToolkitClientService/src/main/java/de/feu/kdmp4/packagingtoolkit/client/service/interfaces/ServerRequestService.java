package de.feu.kdmp4.packagingtoolkit.client.service.interfaces;

/**
 * An interface for the services that send requests to the server. 
 * @author Christopher Olbertz
 *
 */
public interface ServerRequestService {

	void sendCreateVirtualAipRequest(String title);

}
