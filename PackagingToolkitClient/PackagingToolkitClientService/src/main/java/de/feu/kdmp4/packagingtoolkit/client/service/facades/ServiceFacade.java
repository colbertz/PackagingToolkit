package de.feu.kdmp4.packagingtoolkit.client.service.facades;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ViewList;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ArchiveService;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ConfigurationService;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.MediatorService;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.OntologyService;
import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;

/**
 * Contains method to encapsulate the methods of the service classes. The caller
 * has only to use these methods and has not to know which class exactly process his
 * call. All methods only redirect the call to the service classes.
 * @author Christopher Olbertz
 *
 */
public class ServiceFacade {
	private ArchiveService archiveService;
	private ConfigurationService configurationService;
	private OntologyService ontologyService;
	private MediatorService mediatorService;
	
	/**
	 * Adds a reference to the information package that is currently in process.
	 * @param reference The reference that should be added to the information package.
	 */
	public void addRemoteFileReference(final Reference reference) {
		archiveService.addRemoteFileReference(reference);
	}
	
	/**
	 * Adds a file reference to the information package that is currently in process.
	 * @param digitalObject The digital object representing the file the reference is pointing to.
	 */
	public void addLocalFileReference(final DigitalObject digitalObject) {
		archiveService.addLocalFileReference(digitalObject);
	}
	
	/**
	 * Is called if the creation process of the current information package has been cancelled. The server will be informed.
	 */
	public void cancelInformationPackageCreation() {
		archiveService.cancelInformationPackageCreation();
	}
	
	/**
	 * Determines the file that contains the logo.
	 * @return Contains the logo.
	 */
	public File getLogo() {
		return configurationService.getLogo();
	}
	
	/**
	 * Saves a new file for the logo.
	 * @param temporaryLogoFile The file with the logo in the temporary directory.
	 */
	public void saveLogo(final File temporaryLogoFile) {
		configurationService.saveLogo(temporaryLogoFile);
	}
	
	/**
	 * Determines all individuals of a certain class on the server. 
	 * @param ontologyClass The class the individuals belong to. 
	 * @return All individuals on the server assigned to the class ontologyClass.
	 */
	public OntologyIndividualCollection getSavedIndividualsByClass(final OntologyClass ontologyClass) {
		if (ontologyClass == null) {
			return ClientModelFactory.createEmptyOntologyIndividualList();
		}
		return ontologyService.getSavedIndividualsByClass(ontologyClass);
	}
	
	/**
	 * Sends a file to the mediator in order to collect the meta data of the file.
	 * @param file The file whose meta data we want to extract.
	 * @return The meta data found by the mediator in file.
	 */
	public MediatorDataCollection collectMetadata(final File file) {
		return mediatorService.collectMetadata(file);
	}
	
	/**
	 * Determines all taxonomies defined on the server. 
	 * @return All taxonomies found on the server.
	 */
	public TaxonomyCollection findAllTaxonomies() {
		return ontologyService.findAllTaxonomies();
	}
	
	/**
	 * Determines the class the taxonomies start with. This class has only one object property like hasTaxonomy. This object property
	 * references to the taxonomy.
	 * @return The class the taxonomies start with.
	 */
	public OntologyClass getTaxonomyStartClass() {
		return ontologyService.getTaxonomyStartClass();
	}
	
	/**
	 * Determines the time after that the individuals have to be updated from the server. 
	 * @return The time after that the individuals have to be updated from the server in milliseconds.
	 */
	public long getIndividualsUpdateTime() {
		return configurationService.getIndividualsUpdateTimePath();
	}
	
	/**
	 * Loads an information package so that the user can edit it.
	 * @param archive The archive representing the information package that should be load. 
	 */
	public void loadInformationPackage(final Archive archive) {
		archiveService.loadInformationPackage(archive);
	}
	
	/**
	 * Updates an individual on the server. The individual already exists on the server and the user has entered
	 * new or updated data. 
	 * @param ontologyIndividual The individual with the new or updated data. 
	 */
	public void updateIndividual(final OntologyIndividual ontologyIndividual) {
		ontologyService.updateIndividual(ontologyIndividual);
	}
	
	/**
	 * Creates a new submission information package that is administrated by the information package manager.
	 * @param title The title of the submission information package.
	 */
	public void createSubmissionInformationPackage(final String title) {
		archiveService.createSubmissionInformationPackage(title);
	}
	
	/**
	 * Finds all references of the information package that is currently in process.
	 * @return All references assigned to the information package currently in process or an empty list if the 
	 * information package does not contain any references.
	 */
	public ReferenceCollection findReferencesOfInformationPackage() {
		return archiveService.findReferencesOfInformationPackage();
	}
	
	/**
	 * Creates a new submission information unit that is administrated by the information package manager.
	 * @param title The title of the submission information unit.
	 */
	public void createSubmissionInformationUnit(final String title) {
		archiveService.createSubmissionInformationUnit(title);
	}
	
	/**
	 * Deletes an information package from the server.
	 * @param archive The archive that we want to delete.
	 * @return The archives that remain on the server.
	 */
	public ArchiveList deleteArchive(final Archive archive) {
		return archiveService.deleteArchive(archive);
	}
	
	/**
	 * Request the zip file of an archive for download.
	 * @param archive Describes the archive and the required information for building the zip
	 * file.
	 * @return An input stream that contains the data of the files for download.
	 */
	public InputStream downloadArchiveFile(final Archive archive) {
		return archiveService.downloadArchiveFile(archive);
	}
	
	public void saveDataSourceConfiguration(final DataSourceCollection dataSources) {
		mediatorService.saveDataSourceConfiguration(dataSources);
	}
	
	/**
	 * Determines a collection with all views saved on the server.
	 * @return A collection with all views saved on the server.
	 */
	public ViewList getAllViews() {
		return archiveService.getAllViews();
	}
	
	/**
	 * Determines a collection with all archives saved on the server.
	 * @return A collection with all archives saved on the server.
	 */
	public ArchiveList getArchiveList() {
		return archiveService.getArchiveList();
	}
	
	/**
	 * Gets the file in the temporary directory that contains the base ontology.
	 * If the file does not exist in the temporary directory of the client, it
	 * is downloaded from the server. 
	 * @return The base ontology file in the temporary directory of the server.
	 */
	public File getBaseOntologyFile() {
		return configurationService.getBaseOntologyFile();
	}
	
	/**
	 * Gets all configuration data for the client.
	 * @return All client configuration data.
	 */
	public ClientConfigurationData getClientConfigurationData() {
		return configurationService.getClientConfigurationData();
	}
	
	/**
	 * Sends a signal to the server for checking if the server is online. 
	 * @throws ConnectionException If the server is not accessible.
	 */
	public void pingServer() {
		configurationService.pingServer();
	}

	/**
	 * Sends a signal to the mediator for testing if the mediator is accessible.
	 */
	public void pingMediator() {
		mediatorService.pingMediator();
	}
	
	/**
	 * Determines the name of the base ontology file.
	 * @return The name of the base ontology file.
	 */
	public String getBaseOntologyName() {
		ServerConfigurationData configurationData = readServerConfiguration();
		return configurationData.getConfiguredBaseOntologyPath();
	}
	
	/**
	 * Determines all data sources that are registered on the mediator.
	 * @return All data sources registered on the mediator.
	 * @throws MediatorConfigurationException if the installation directory of a data source is not correct or if the mediator
	 * is not accessible.
	 */
	public DataSourceCollection findAllDataSources() {
		return mediatorService.findAllDataSources();
	}
	
	/**
	 * Loads the digital objects of the information package saved in the information package
	 * manager from the server.
	 * @return The list with the digital objects of the information package processed. 
	 */
	public DigitalObjectCollection loadDigitalObjectsOfInformationPackage() {
		return archiveService.loadDigitalObjectsOfInformationPackage();
	}
	
	/**
	 * Determines the class hierarchy in the base ontology for some classes. 
	 * @param ontologyClassNames The names of the classes whose subclasses have to be 
	 * determined.
	 * @return The configured root class. Its subclasses are the classes whose names
	 * were given to this method. They contain their hierarchy.
	 */
	public OntologyClass getHierarchy(final StringList ontologyClassNames) {
		return ontologyService.getHierarchyOfBaseOntology(ontologyClassNames);
	}
	
	/**
	 * Determines the class hierarchy of the base ontology. 
	 * @return The top class of the hierarchy. It contains subclasses and provides
	 * access to the entire hierarchy. 
	 */
	public OntologyClass getHierarchyOfBaseOntology() {
		return ontologyService.getHierarchyOfBaseOntology();
	}
	
	/**
	 * Determines the hierarchy of the Premis ontology.
	 * @return The root class of the Premis ontology. Contains all subclasses of Premis. 
	 */
	public OntologyClass getHierarchyOfPremis() {
		return ontologyService.getHierarchyOfPremis();
	}
	
	/**
	 * Determines the hierarchy of the SKOS ontology.
	 * @return The root class of the SKOS ontology. Contains all subclasses of SKOS. 
	 */
	public OntologyClass getHierarchyOfSkos() {
		return ontologyService.getHierarchyOfSkos();
	}
	
	public void checkDataSourceInstallations() {
		mediatorService.checkDataSourceInstallations();
	}
	
	/**
	 * Determines all individuals that are predefined in the base ontology. 
	 * @param ontologyClass The ontology class we are interested in.
	 * @return All found individuals. 
	 */
	public OntologyIndividualCollection getPredefinedIndividualsByClass(final OntologyClass ontologyClass) {
		if (ontologyClass == null) {
			return ClientModelFactory.createEmptyOntologyIndividualList();
		}
		return ontologyService.findPredefinedIndividualsByClass(ontologyClass);
	}
	
	/**
	 * Determines the properties of a given class.
	 * @param classname The name of the class.
	 * @return A list with the properties contained in this class.
	 */
	public OntologyClass getPropertiesOfClass(final String classname)  {
		return ontologyService.getPropertiesOfClass(classname);
	}
	
	/**
	 * Loads the archive the user wants to edit.
	 * @param archive The archive that should be loaded.
	 */
	public void loadArchiveForEditing(final Archive archive) {
		archiveService.loadArchiveForEditing(archive);
	}
	
	/**
	 * Reads the configuration of the server.
	 * @return The configuration data of the server.
	 */
	public ServerConfigurationData readServerConfiguration() {
		return configurationService.readServerConfiguration();
	}   
	
	/**
	 * Determines all individuals that are assigned to a given information package.
	 * @param uuidOfInformationPackage The uuid of the information package whose individuals 
	 * we want to determine.
	 * @return All individuals that are assigned to this information package.
	 */
	public OntologyIndividualCollection findAllIndividualsOfInformationPackage(Uuid uuidOfInformationPackage) {
		return archiveService.findAllIndividualsOfInformationPackage(uuidOfInformationPackage);
	}
	
	/**
	 * Saves an individual on the server. 
	 * @param ontologyIndividual The individual that should be saved.
	 */
	public void saveIndividual(final OntologyIndividual ontologyIndividual) {
		ontologyService.saveIndividual(ontologyIndividual);
	}
	
	/**
	 * Deletes a view from the server.
	 * @param viewId The id of the view to delete.
	 */
	public void deleteView(final int viewId) {
		archiveService.deleteView(viewId);
	}
	
	/**
	 * Saves the information package the user is working with on the server.
	 */
	public void saveCurrentInformationPackage() {
		archiveService.saveCurrentInformationPackage();
	}
	
	/**
	 * Saves a new view on the server. 
	 * @param view The view that should be saved.
	 */
	public void saveNewView(final View view) {
		archiveService.saveNewView(view);
	}
	
	/**
	 * Persists configuration data.
	 * @param clientConfigurationData The configuration data to save.
	 * @throws FileNotFoundException Is thrown, when there is a problem with the
	 * configuration file. 
	 * @throws IOException 
	 */
	public void writeClientConfigurationData(final ClientConfigurationData 
			clientConfigurationData) {
		configurationService.writeClientConfigurationData(clientConfigurationData);
	}
	
	/**
	 * Saves the configuration of the server.
	 * @param configurationData Contains the configuration data entered by the user.
	 */
	public void writeServerConfigurationData(final ServerConfigurationData configurationData) {
		configurationService.writeServerConfigurationData(configurationData);
	}

	/**
	 * Sets a reference to an object of the archive service.
	 * @param archiveService A reference to an object of the archive service. 
	 */
	public void setArchiveService(final ArchiveService archiveService) {
		this.archiveService = archiveService;
	}

	/**
	 * Sets a reference to an object of the configuration service.
	 * @param archiveService A reference to an object of the configuration service. 
	 */
	public void setConfigurationService(final ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	/**
	 * Sets a reference to an object of the ontology service.
	 * @param archiveService A reference to an object of the ontology service. 
	 */
	public void setOntologyService(final OntologyService ontologyService) {
		this.ontologyService = ontologyService;
	}
	
	public void setMediatorService(final MediatorService mediatorService) {
		this.mediatorService = mediatorService;
	}

	public void assignIndividualsToInformationPackage(final UuidList uuids) {
		archiveService.assignIndividualsToInformationPackage(uuids);
	}
	
	public void unassignIndividualsFromInformationPackage(final UuidList uuids) {
		archiveService.unassignIndividualsFromInformationPackage(uuids);
	}
	
	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param taxonomyIndividuals The individuals from the taxonomy that should be 
	 * assigned to the information package.
	 * @param iriOfInformationPackage The iri of the information package the individuals should be 
	 * assigned to. 
	 */
	public void assignTaxonomyIndividuals(final TaxonomyIndividualCollection taxonomyIndividuals, 
			  final Iri iriOfInformationPackage) {
		archiveService.assignTaxonomyIndividuals(taxonomyIndividuals, iriOfInformationPackage);
	}

	/**
	 * Requests all taxonomy individuals that are assigned to a certain information package from 
	 * the server.
	 * @param uuidOfInformationPackage The uuid of the information package whose taxonomy 
	 * individuals we want to request.
	 * @return The iris of the taxonomy individuals that are assigned to the information package.
	 */
	public IriCollection findAllAssignedTaxonomyIndividuals(final Uuid uuidOfInformationPackage) {
		return archiveService.findAllAssignedTaxonomyIndividuals(uuidOfInformationPackage);
	}

	/**
	 * Sends that an individual should be assigned to an information package or
	 * unassigned. If the individual is already assigned to the information package,
	 * the relationship between the both will be removed. 
	 * @param uuidOfIndividual The uuid of the individual that should be assigned or
	 * unassigned.
	 * @param uuidOfInformationPackage The uuid of the information package we want to
	 * work with. 
	 */
	/*public void assignOrUnassignIndividualToInformationPackage(final Uuid uuidOfIndividual, 
													 		   final Uuid uuidOfInformationPackage) {
		archiveService.assignOrUnassignIndividualToInformationPackage(uuidOfIndividual, uuidOfInformationPackage);
	}*/
}
