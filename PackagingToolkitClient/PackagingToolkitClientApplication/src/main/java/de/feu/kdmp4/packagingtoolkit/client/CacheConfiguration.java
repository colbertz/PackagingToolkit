package de.feu.kdmp4.packagingtoolkit.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;

@Configuration
public class CacheConfiguration {
	/*@Autowired
	private InformationPackageManager informationPackageManager;
	@Autowired
	private ServiceFacade serviceFacade;*/
	
	@Bean(name = "serverCache")
	public ServerCache getServerCache() {
		ServerCache serverCache = new ServerCache();
		//serverCache.setServiceFacade(serviceFacade);
		//serverCache.setInformationPackageManager(informationPackageManager);
		return serverCache;
	}
}
