package de.feu.kdmp4.packagingtoolkit.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.operations.OperationsMain;
import de.feu.kdmp4.packagingtoolkit.client.operations.classes.ConfigurationOperationsPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.operations.classes.MediatorCommunicationOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.client.operations.classes.MockedMediatorCommunicationOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.client.operations.classes.MockedServerOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.client.operations.classes.ServerCommunicationOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ClientConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.MediatorCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ServerCommunicationOperations;


/**
 * Contains the spring configuration for the operations layer of the client.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class OperationsConfiguration {
	/**
	 * A reference to the factory that creates model objects for the client. 
	 */
	@Autowired
	private ClientModelFactory clientModelFactory;
	/**
	 * Creates an object for the configuration operations of the client. 
	 * @return The configuration operations object.
	 */
	@Bean
	public ClientConfigurationOperations clientConfigurationOperations() {
		ClientConfigurationOperations clientConfigurationOperations  = new ConfigurationOperationsPropertyImpl();
		return clientConfigurationOperations;
	}
	
	/**
	 * Creates an object for the server communication operations of the client. The application can create a mocked object 
	 * that means an object that does not communicate with the real server but creates some testing data. 
	 * @return The server communication operations object.
	 */
	@Bean
	public ServerCommunicationOperations serverCommunicationOperations() {
		ServerCommunicationOperations serverCommunicationOperations;
		
		if (OperationsMain.isMockServer()) {
			serverCommunicationOperations = new MockedServerOperationsImpl();
		} else {
			serverCommunicationOperations = new ServerCommunicationOperationsImpl(); 
		}
		
		serverCommunicationOperations.setClientModelFactory(clientModelFactory);
		return serverCommunicationOperations;
	}
	
	/**
	 * Creates an object for the mediator communication operations of the client. The application can create a mocked object 
	 * that means an object that does not communicate with the real mediator but creates some testing data. 
	 * @return The mediator communication operations object.
	 */
	@Bean
	public MediatorCommunicationOperations mediatorCommunicationOperations() {
		MediatorCommunicationOperations mediatorCommunicationOperations;
		
		if (OperationsMain.isMockMediator()) {
			mediatorCommunicationOperations = new MockedMediatorCommunicationOperationsImpl();
		} else {
			mediatorCommunicationOperations = new MediatorCommunicationOperationsImpl();
		}
		return mediatorCommunicationOperations;
	}
	
	/**
	 * Creates the object for the facade that encapsulates the operations layer and hides it from the calling classes.
	 * @return An facade object for the operations layer.
	 */
	@Bean
	public OperationsFacade getOperationsFacade() {
		OperationsFacade operationsFacade = new OperationsFacade();
		operationsFacade.setClientConfigurationOperations(clientConfigurationOperations());
		operationsFacade.setServerCommunicationOperations(serverCommunicationOperations());
		operationsFacade.setMediatorCommunicationOperations(mediatorCommunicationOperations());
		return operationsFacade;
	}
}
