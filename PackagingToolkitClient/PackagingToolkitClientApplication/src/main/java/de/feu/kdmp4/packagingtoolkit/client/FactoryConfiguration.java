package de.feu.kdmp4.packagingtoolkit.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;

@Configuration
public class FactoryConfiguration {
	@Autowired
	private InformationPackageManager informationPackageManager;
	
	@Bean
	public ClientResponseFactory clientResponseFactory() {
		return new ClientResponseFactory();
	}
	
	@Bean
	public ClientModelFactory clientModelFactory() {
		ClientModelFactory clientModelFactory = new ClientModelFactory(propertyFactory());
		clientModelFactory.setInformationPackageManager(informationPackageManager);
		//clientModelFactory.setPropertyFactory(propertyFactory());
		return clientModelFactory;
	}
	
	@Bean
	public PropertyFactory propertyFactory() {
		return new PropertyFactory(ontologyModelFactory());
	}
	
	@Bean
	public OntologyModelFactory ontologyModelFactory() {
		OntologyModelFactory ontologyModelFactory = new OntologyModelFactory();
		//ontologyModelFactory.setPropertyFactory(propertyFactory());
		return ontologyModelFactory;
	}
}
