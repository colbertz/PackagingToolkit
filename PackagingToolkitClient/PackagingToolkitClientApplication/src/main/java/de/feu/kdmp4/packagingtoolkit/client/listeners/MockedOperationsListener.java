package de.feu.kdmp4.packagingtoolkit.client.listeners;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;

import de.feu.kdmp4.packagingtoolkit.client.operations.OperationsMain;

/**
 * This listener reads the file application.properties and decides if the 
 * application is running in mocked state. If the property
 * packagingtoolkit.mockedServer is true then the class 
 * {@link de.feu.kdmp4.packagingtoolkit.client.operations.classes.MockedServerOperationsImpl}
 * is used, otherwise the class 
 * {@link de.feu.kdmp4.packagingtoolkit.client.operations.classes.ServerCommunicationOperationsImpl}.
 * The same is true for the mediator.
 * @author Christopher Olbertz
 *
 */
public class MockedOperationsListener implements 
			ApplicationListener<ApplicationStartingEvent> {

	private static final String APPLICATION_PROPERTIES = "src/main/resources/application.properties";
	private static final String PROPERTY_SERVER_PRODUCTIVE = "packagingtoolkit.mockedServer";
	private static final String PROPERTY_MEDIATOR_PRODUCTIVE = "packagingtoolkit.mockedMediator";
	
	@Override
	public void onApplicationEvent(ApplicationStartingEvent arg0) {
		boolean isServerProductive = readServerProductive();
		boolean isMediatorProductive = readMediatorProductive();
		OperationsMain.setMockMediator(isMediatorProductive);
		OperationsMain.setMockServer(isServerProductive);
	}
	
	private boolean readServerProductive() {
		File propertiesFile = new File(APPLICATION_PROPERTIES);
		Properties properties = new Properties();
		
		try (InputStream inputStream = new FileInputStream(propertiesFile)) {
			// Load the properties.
			properties.load(inputStream);
			// Get the properties and write them in an object.
			String productive = properties.getProperty(PROPERTY_SERVER_PRODUCTIVE);
			return Boolean.valueOf(productive);
		} catch (IOException ioex) {
			return false;
		}
	}
	
	private boolean readMediatorProductive() {
		File propertiesFile = new File(APPLICATION_PROPERTIES);
		Properties properties = new Properties();
		
		try (InputStream inputStream = new FileInputStream(propertiesFile)) {
			// Load the properties.
			properties.load(inputStream);
			// Get the properties and write them in an object.
			String productive = properties.getProperty(PROPERTY_MEDIATOR_PRODUCTIVE);
			return Boolean.valueOf(productive);
		} catch (IOException ioex) {
			return false;
		}
	}
}