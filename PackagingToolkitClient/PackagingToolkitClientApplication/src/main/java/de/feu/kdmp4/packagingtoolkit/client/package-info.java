/**
 * Contains the classes for the entire application. There are the classes for starting und configuring the whole client.
 * Contains the configuration classes for Spring too.
 */
/**
 * @author christopher
 *
 */
package de.feu.kdmp4.packagingtoolkit.client;