package de.feu.kdmp4.packagingtoolkit.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;

@Configuration
public class ManagerConfiguration {
	@Autowired
	private ClientModelFactory clientModelFactory;
	
	@Bean(name = "informationPackageManager")
	public InformationPackageManager getInformationPackageManager() {
		InformationPackageManager informationPackageManager = new InformationPackageManager();
		informationPackageManager.setClientModelFactory(clientModelFactory);
		return informationPackageManager;
	}
	
	@Bean
	public DynamicGuiManager dynamicGuiManager() {
		DynamicGuiManager dynamicGuiManager = new DynamicGuiManager();
		
		return dynamicGuiManager;
	}
	
	/*@Bean(name = "ontologyManager")
	public OntologyManager getOntologyManager() {
		return new OntologyManager();
	}*/
}
