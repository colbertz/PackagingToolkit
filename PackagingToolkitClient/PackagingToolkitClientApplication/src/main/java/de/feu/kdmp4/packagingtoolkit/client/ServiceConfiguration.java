package de.feu.kdmp4.packagingtoolkit.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientResponseFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.operations.facade.OperationsFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.classes.ArchiveServiceImpl;
import de.feu.kdmp4.packagingtoolkit.client.service.classes.ConfigurationServiceImpl;
import de.feu.kdmp4.packagingtoolkit.client.service.classes.MediatorServiceImpl;
import de.feu.kdmp4.packagingtoolkit.client.service.classes.OntologyServiceImpl;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ArchiveService;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.ConfigurationService;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.MediatorService;
import de.feu.kdmp4.packagingtoolkit.client.service.interfaces.OntologyService;

@Configuration
public class ServiceConfiguration {
	@Autowired
	private ClientModelFactory clientModelFactory;
	@Autowired
	private ClientResponseFactory clientResponseFactory;
	@Autowired 
	private InformationPackageManager informationPackageManager;
	@Autowired
	private OperationsFacade operationsFacade;
	
	@Bean(name = "archiveService")
	public ArchiveService getArchiveService() {
		ArchiveService archiveService = new ArchiveServiceImpl();
		archiveService.setInformationPackageManager(informationPackageManager);
		archiveService.setOperationsFacade(operationsFacade);
		archiveService.setClientModelFactory(clientModelFactory);
		archiveService.setClientResponseFactory(clientResponseFactory);
		
		return archiveService;
	}
	
	@Bean(name = "configurationService")
	public ConfigurationService getConfigurationService() {
		ConfigurationService configurationService = new ConfigurationServiceImpl();
		configurationService.setOperationsFacade(operationsFacade);
		
		return configurationService;
	}
	
	@Bean(name = "ontologyService")
	public OntologyService getOntologyService() {
		OntologyService ontologyService = new OntologyServiceImpl();
		ontologyService.setClientModelFactory(clientModelFactory);
		ontologyService.setClientResponseFactory(clientResponseFactory);
		ontologyService.setInformationPackageManager(informationPackageManager);
		ontologyService.setOperationsFacade(operationsFacade);
		
		return ontologyService;
	}
	
	@Bean
	public MediatorService mediatorService() {
		MediatorService mediatorService = new MediatorServiceImpl();
		mediatorService.setOperationsFacade(operationsFacade);
		mediatorService.setClientModelFactory(clientModelFactory);
		mediatorService.setClientResponseFactory(clientResponseFactory);
		
		return mediatorService;
	}
	
	/*@Bean(name = "metadataService")
	public MetadataService getMetadataService() {
		MetadataService metadataService = new MetadataServiceImpl();
		metadataService.setMetadataOperations(metadataOperations);
		
		return metadataService;
	}*/
	
	@Bean(name = "serviceFacade")
	public ServiceFacade getServiceFacade() {
		ServiceFacade serviceFacade = new ServiceFacade();
		serviceFacade.setArchiveService(getArchiveService());
		serviceFacade.setConfigurationService(getConfigurationService());
		serviceFacade.setOntologyService(getOntologyService());
		serviceFacade.setMediatorService(mediatorService());
		
		return serviceFacade;
	}
}
