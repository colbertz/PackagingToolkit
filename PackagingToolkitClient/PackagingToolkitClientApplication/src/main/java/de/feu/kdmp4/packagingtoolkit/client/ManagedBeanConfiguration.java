package de.feu.kdmp4.packagingtoolkit.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.ApplicationConfigurationBean;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.ConfigurationBean;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.InformationPackageCreationBean;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.OntologyNodesSelectionBean;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.SavedViewsBean;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.ShowArchiveListBean;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.OntologyTreeView;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.WorkspaceBean;

/**
 * Configures the managed beans for the view. Because the managed beans needs Spring's dependency 
 * injection, they are not managed by JSF but by Spring.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class ManagedBeanConfiguration {
	@Autowired 
	private InformationPackageManager informationPackageManager;
	@Autowired 
	private ServiceFacade serviceFacade;
	@Autowired
	private ClientModelFactory clientModelFactory;
	@Autowired
	private ServerCache serverCache;
	@Autowired
	private PropertyFactory propertyFactory;
	@Autowired
	private DynamicGuiManager dynamicGuiManager;
	
	@Bean
	@Scope("session")
	public ApplicationConfigurationBean applicationConfigurationBean() {
		ApplicationConfigurationBean applicationConfigurationBean = new ApplicationConfigurationBean();
		applicationConfigurationBean.setServiceFacade(serviceFacade);
		return applicationConfigurationBean;
	}
	
	@Bean
	@Scope("view")
	public InformationPackageCreationBean informationPackageCreationBean() {
		ReferenceCollection references = ClientModelFactory.createEmptyReferenceCollection();
		DigitalObjectCollection digitalObjects = clientModelFactory.getEmptyDigitalObjectList();
		InformationPackageCreationBean informationPackageCreationBean = new InformationPackageCreationBean(informationPackageManager, serviceFacade,
				references, digitalObjects);
		informationPackageCreationBean.setClientModelFactory(clientModelFactory);
		return informationPackageCreationBean;
	}
	
	@Bean
	@Scope("request")
	public ShowArchiveListBean showArchiveListBean() {
		ShowArchiveListBean showArchiveListBean = new ShowArchiveListBean(serviceFacade);
		showArchiveListBean.setInformationPackageManager(informationPackageManager);
		showArchiveListBean.setServerCache(serverCache);
		return showArchiveListBean;
	}
	
	@Bean
	@Scope("view")
	public SavedViewsBean savedViewsBean() {
		SavedViewsBean savedViewsBean = new SavedViewsBean();
		savedViewsBean.setInformationPackageManager(informationPackageManager);
		savedViewsBean.setServiceFacade(serviceFacade);
		savedViewsBean.setServerCache(serverCache);
		
		return savedViewsBean;
	}
	
	@Bean
	@Scope("view")
	public OntologyTreeView ontologyTreeView() {
		OntologyTreeView ontologyTreeView = new OntologyTreeView();
		ontologyTreeView.setInformationPackageManager(informationPackageManager);
		ontologyTreeView.setServiceFacade(serviceFacade);
		ontologyTreeView.setServerCache(serverCache);
		ontologyTreeView.setPropertyFactory(propertyFactory);
		ontologyTreeView.setDynamicGuiManager(dynamicGuiManager);
		
		return ontologyTreeView;
	}
	
	@Bean
	@Scope("view")
	public OntologyNodesSelectionBean ontologyNodesSelectionBean() {
		OntologyNodesSelectionBean ontologyNodesSelectionBean = new OntologyNodesSelectionBean();
		ontologyNodesSelectionBean.setInformationPackageManager(informationPackageManager);
		ontologyNodesSelectionBean.setServiceFacade(serviceFacade);
		ontologyNodesSelectionBean.setServerCache(serverCache);
		
		return ontologyNodesSelectionBean;
	}
	
	@Bean
	@Scope("view")
	public ConfigurationBean configurationBean() {
		ConfigurationBean configurationBean = new ConfigurationBean();
		configurationBean.setClientModelFactory(clientModelFactory);
		configurationBean.setServerCache(serverCache);
		//configurationBean.setConfigurationService(configurationService);
		configurationBean.setServiceFacade(serviceFacade);
		
		return configurationBean;
	}
	
	@Bean
	@Scope("session")
	public WorkspaceBean workspaceBean() {
		WorkspaceBean workspaceBean = new WorkspaceBean();
		workspaceBean.setServiceFacade(serviceFacade);
		workspaceBean.setInformationPackageManager(informationPackageManager);
		return workspaceBean;
	}
}