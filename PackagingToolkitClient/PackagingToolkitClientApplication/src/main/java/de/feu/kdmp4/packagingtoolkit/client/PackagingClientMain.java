package de.feu.kdmp4.packagingtoolkit.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.feu.kdmp4.packagingtoolkit.client.listeners.MockedOperationsListener;

@SpringBootApplication
public class PackagingClientMain 
{
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(PackagingClientMain.class);
		application.addListeners(new MockedOperationsListener());
		application.run(args);
	}
}
