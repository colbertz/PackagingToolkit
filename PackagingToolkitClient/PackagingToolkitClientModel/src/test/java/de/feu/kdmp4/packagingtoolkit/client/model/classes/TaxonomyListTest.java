package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.model.testapi.TaxonomyTestApi;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.exceptions.ListException;

public class TaxonomyListTest {
	/**
	 * Adds three taxonomies to the collection and tests after every insertion if the process was sucessful.
	 */
	@Test
	public void testAddTaxonomy() {
		final TaxonomyCollection taxonomyCollection = new TaxonomyListImpl();
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(0)));
		
		final Taxonomy taxonomy1 = TaxonomyTestApi.createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy1);
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(1)));
		final Optional<Taxonomy> optionalWithExpectedTaxonomy1 = taxonomyCollection.getTaxonomy(0);
		assertTrue(optionalWithExpectedTaxonomy1.isPresent());
		final Taxonomy expectedTaxonomy1 = optionalWithExpectedTaxonomy1.get();
		assertThat(expectedTaxonomy1, is(equalTo(taxonomy1)));
		
		final Taxonomy taxonomy2 = TaxonomyTestApi.createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy2);
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(2)));
		final Optional<Taxonomy> optionalWithExpectedTaxonomy2 = taxonomyCollection.getTaxonomy(1);
		assertTrue(optionalWithExpectedTaxonomy2.isPresent());
		final Taxonomy expectedTaxonomy2 = optionalWithExpectedTaxonomy2.get();
		assertThat(expectedTaxonomy2, is(equalTo(taxonomy2)));
		
		final Taxonomy taxonomy3 = TaxonomyTestApi.createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy3);
		assertThat(taxonomyCollection.getTaxonomyCount(), is(equalTo(3)));
		final Optional<Taxonomy> optionalWithExpectedTaxonomy3 = taxonomyCollection.getTaxonomy(2);
		assertTrue(optionalWithExpectedTaxonomy3.isPresent());
		final Taxonomy expectedTaxonomy3 = optionalWithExpectedTaxonomy3.get();
		assertThat(expectedTaxonomy3, is(equalTo(taxonomy3)));
	}
	
	/**
	 * Tests if the second taxonomy is determined from the collection correctly.
	 */
	@Test
	public void testGetTaxonomy_resultTaxonomyFound() {
		final TaxonomyCollection taxonomyCollection = ClientModelFactory.createEmptyTaxonomyCollection();
		final Taxonomy expectedTaxonomy = TaxonomyTestApi.createTaxonomyCollectionWithThreeTaxonomies(taxonomyCollection);
		final Optional<Taxonomy> optionalWithTaxonomy = taxonomyCollection.getTaxonomy(1);
		assertTrue(optionalWithTaxonomy.isPresent());
		final Taxonomy actualTaxonomy = optionalWithTaxonomy.get();
		assertThat(actualTaxonomy, is(equalTo(expectedTaxonomy)));
	}
	
	/**
	 * Tests if correct exception is thrown if the index does not exist in the list.
	 */
	@Test(expected = ListException.class)
	public void testGetTaxonomy_resultTaxonomyNotFound() {
		final TaxonomyCollection taxonomyCollection = ClientModelFactory.createEmptyTaxonomyCollection();
		TaxonomyTestApi.createTaxonomyCollectionWithThreeTaxonomies(taxonomyCollection);
		taxonomyCollection.getTaxonomy(11);
	}
	
	/**
	 * Tests if the collection can recognize that there is not another taxonomy with an empty list of taxonomies.
	 */
	@Test
	public void testHasNextTaxonomy_resultFalse() {
		final TaxonomyCollection taxonomyCollection = new TaxonomyListImpl();
		assertFalse(taxonomyCollection.hasNextTaxonomy());
	}

	/**
	 * Tests if the next taxonomy can be determined. The collection is initialized with three taxonomies. Then we try to determine the three taxonomies. 
	 * Every time we determine a taxonomy, we check if there is a next taxonomy and if the optional with the next taxonomy is not empty. The fourth 
	 * time we check if there is not a next taxonomy and the optional with the next taxonomy has to be empty.
	 */
	@Test
	public void testGetNextTaxonomy() {
		final TaxonomyCollection taxonomyCollection = new TaxonomyListImpl();
		TaxonomyTestApi.addThreeTaxonomiesToTaxonomyCollection(taxonomyCollection);
		
		assertTrue(taxonomyCollection.hasNextTaxonomy());
		Optional<Taxonomy> optionalWithTaxonomy = taxonomyCollection.nextTaxonomy();
		assertTrue(optionalWithTaxonomy.isPresent());
		
		assertTrue(taxonomyCollection.hasNextTaxonomy());
		optionalWithTaxonomy = taxonomyCollection.nextTaxonomy();
		assertTrue(optionalWithTaxonomy.isPresent());
		
		assertTrue(taxonomyCollection.hasNextTaxonomy());
		optionalWithTaxonomy = taxonomyCollection.nextTaxonomy();
		assertTrue(optionalWithTaxonomy.isPresent());
		
		assertFalse(taxonomyCollection.hasNextTaxonomy());
		optionalWithTaxonomy = taxonomyCollection.nextTaxonomy();
		assertFalse(optionalWithTaxonomy.isPresent());
	}
	
	/**
	 * Tests if the next narrower can be determined. The individual does not contain any narrowers.  We check if there is not a 
	 * next narrower and the optional with the next narrower has to be empty.
	 */
	@Test
	public void testGetNextTaxonomy_noTaxonomies() {
		final TaxonomyCollection taxonomies = new TaxonomyListImpl();
		
		assertFalse(taxonomies.hasNextTaxonomy());
		Optional<Taxonomy> optionalWithTaxonomy = taxonomies.nextTaxonomy();
		assertFalse(optionalWithTaxonomy.isPresent());
	}
	
	/**
	 * Tests the determination of a taxonomy with the help of a root individual with an empty collection.
	 */
	@Test
	public void testGetTaxonomyByRootIndividual_emptyTaxonomyCollection() {
		final TaxonomyCollection taxonomies = new TaxonomyListImpl();
		final TaxonomyIndividual rootIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Optional<Taxonomy> optionalWithFoundTaxonomy = taxonomies.getTaxonomyByRootIndividual(rootIndividual);
		assertFalse(optionalWithFoundTaxonomy.isPresent());
	}
	
	/**
	 * Tests the determination of a taxonomy with the help of a root individual. The root individual we are looking for
	 * is not a root individual of the taxonomies contained in the collection.
	 */
	@Test
	public void testGetTaxonomyByRootIndividual_individualNotContainedInTaxonomyCollection() {
		final TaxonomyCollection taxonomies = new TaxonomyListImpl();
		TaxonomyTestApi.addThreeTaxonomiesToTaxonomyCollection(taxonomies);
		final TaxonomyIndividual rootIndividual = TaxonomyTestApi.createTaxonomyIndividual();
		final Optional<Taxonomy> optionalWithFoundTaxonomy = taxonomies.getTaxonomyByRootIndividual(rootIndividual);
		assertFalse(optionalWithFoundTaxonomy.isPresent());
	}
	
	/**
	 * Tests the determination of a taxonomy with the help of a root individual.
	 */
	@Test
	public void testGetTaxonomyByRootIndividual_resultTaxonomyIsFound() {
		final TaxonomyCollection taxonomies = new TaxonomyListImpl();
		final Taxonomy expectedTaxonomy = TaxonomyTestApi.addThreeTaxonomiesToTaxonomyCollection(taxonomies);
		final TaxonomyIndividual rootIndividual = expectedTaxonomy.getRootIndividual();
		
		final Optional<Taxonomy> optionalWithFoundTaxonomy = taxonomies.getTaxonomyByRootIndividual(rootIndividual);
		
		assertTrue(optionalWithFoundTaxonomy.isPresent());
		final Taxonomy actualTaxonomy = optionalWithFoundTaxonomy.get();
		assertThat(actualTaxonomy, is(equalTo(expectedTaxonomy)));
	}
	
	/**
	 * Tests if a collection with some individuals can be added to another collection. The test checks if the number of contained individuals
	 * is higher than before and if the correct individuals have been added.
	 */
	@Test
	public void testAddIndividuals() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividualCollection);
		
		final TaxonomyIndividualCollection collectionWithNewIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(collectionWithNewIndividuals);
		
		taxonomyIndividualCollection.addTaxonomyIndividuals(collectionWithNewIndividuals);
		assertThat(taxonomyIndividualCollection.getSize(), is(equalTo(6)));
		
		for(int i = 3; i < taxonomyIndividualCollection.getSize(); i++) {
			final TaxonomyIndividual actualTaxonomyIndividual = taxonomyIndividualCollection.getTaxonomyIndividual(i).get();
			final TaxonomyIndividual expectedTaxonomyIndividual = collectionWithNewIndividuals.getTaxonomyIndividual(i - 3).get();
			assertThat(actualTaxonomyIndividual, is(equalTo(expectedTaxonomyIndividual)));
		}
	}
	
	/**
	 * Tests if a collection with some individuals can be added to another collection, but the target collection is empty. The individuals have
	 * to be inserted anyway.
	 */
	@Test
	public void testAddIndividuals_withEmptyTargetCollection() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		
		final TaxonomyIndividualCollection collectionWithNewIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		taxonomyIndividualCollection.addTaxonomyIndividuals(collectionWithNewIndividuals);
		
		assertThat(taxonomyIndividualCollection.getSize(), is(equalTo(3)));
		
		for(int i = 0; i < taxonomyIndividualCollection.getSize(); i++) {
			final TaxonomyIndividual actualTaxonomyIndividual = taxonomyIndividualCollection.getTaxonomyIndividual(i).get();
			final TaxonomyIndividual expectedTaxonomyIndividual = collectionWithNewIndividuals.getTaxonomyIndividual(i).get();
			assertThat(actualTaxonomyIndividual, is(equalTo(expectedTaxonomyIndividual)));
		}
	}
	
	/**
	 * Tests if a collection with some individuals can be added to another collection, but the second collection is empty.
	 */
	@Test
	public void testAddIndividuals_withEmptySourceCollection() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividualCollection);
		
		final TaxonomyIndividualCollection collectionWithNewIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		taxonomyIndividualCollection.addTaxonomyIndividuals(collectionWithNewIndividuals);
		
		assertThat(taxonomyIndividualCollection.getSize(), is(equalTo(3)));
	}
}
