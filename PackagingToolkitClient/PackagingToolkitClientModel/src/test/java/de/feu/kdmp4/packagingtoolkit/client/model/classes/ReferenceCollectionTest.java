package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

import java.util.Optional;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;


public class ReferenceCollectionTest {

	/**
	 * Tests if a reference can be removed by its url. The following conditions have to be assured:
	 * <ul>
	 * 	<li>The resulting Optional has to contain a value.</li>
	 * 	<li>The value of the Optional has to be equal to the reference we wanted to remove.</li>
	 * 	<li>The list with the references must contain only two values instead of three.</li>
	 * </ul>
	 */
	@Test
	public void testRemoveReference() {
		ReferenceCollection references = prepareTest_testRemoveReference_referenceList();
		Reference removedReference = references.getReference(1);
		String urlOfRemovedReference = removedReference.getUrl();
		
		Optional<Reference> removedReferenceAsOptional = references.removeReference(urlOfRemovedReference);
		assertTrue(removedReferenceAsOptional.isPresent());
		Reference actualRemovedReference = removedReferenceAsOptional.get();
		assertThat(actualRemovedReference, is(equalTo(removedReference)));
		assertThat(references.getReferencesCount(), is(equalTo(2)));
	}
	
	/**
	 * Creates three references for the test. 
	 * @return The list with the references.
	 */
	private ReferenceCollection prepareTest_testRemoveReference_referenceList() {
		ReferenceCollection referenceCollection = new ReferenceListCollectionImpl();
		Reference reference1 = ReferenceImpl.createRemoteUrlReference("http://reference1.de");
		Reference reference2 = ReferenceImpl.createRemoteUrlReference("http://reference2.de");
		Reference reference3 = ReferenceImpl.createRemoteUrlReference("http://reference3.de");
		
		referenceCollection.addReference(reference1);
		referenceCollection.addReference(reference2);
		referenceCollection.addReference(reference3);
		
		return referenceCollection;
	}

}
