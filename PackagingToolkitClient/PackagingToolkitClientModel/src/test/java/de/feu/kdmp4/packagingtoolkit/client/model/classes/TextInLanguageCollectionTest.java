package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.client.model.testapi.TextInLanguageTestApi;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguageList;

/**
 * Tests the methods of the collection for text in languages.
 * @author Christopher Olbertz
 *
 */
public class TextInLanguageCollectionTest {
	/**
	 * Tests the insertion of text in languages with three text in languages.
	 */
	@Test
	public void testAddTextInLanguage() {
		final List<TextInLanguage> textInLanguageList = TextInLanguageTestApi.getTextInLanguageList();
		int expectedTextInLanguageCount = 1;
		
		final TextInLanguageList textInLanguageCollection = new TextInLanguageListImpl();
		
		for (TextInLanguage textInLanguage: textInLanguageList) {
			textInLanguageCollection.addTextInLanguage(textInLanguage);
			int actualTextInLanguageCount = textInLanguageCollection.getTextsInLanguageCount();
			assertThat(actualTextInLanguageCount, is(equalTo(expectedTextInLanguageCount)));
			
			final int index = expectedTextInLanguageCount - 1;
			assertThat(textInLanguage, is(equalTo(textInLanguageList.get(index))));
			expectedTextInLanguageCount++;
		}
	}
	
	/**
	 * Tests if an empty list is recognized as empty.
	 */
	@Test
	public void testIsEmpty_resultTrue() {
		final TextInLanguageList textInLanguageCollection = new TextInLanguageListImpl();
		assertTrue(textInLanguageCollection.isEmpty());
	}
	
	/**
	 * Tests if a not empty list is recognized as not empty.
	 */
	@Test
	public void testIsEmpty_resultFalse() {
		final TextInLanguageList textInLanguageCollection = TextInLanguageTestApi.createTextInLanguageCollectionWithTestData();
		assertFalse(textInLanguageCollection.isEmpty());
	}
	
	/**
	 * Tests if the number of text in languages contained in the collection is correctly count.
	 */
	@Test
	public void testCountTextInLanguages() {
		final TextInLanguageList textInLanguageCollection = TextInLanguageTestApi.createTextInLanguageCollectionWithTestData();
		assertThat(textInLanguageCollection.getTextsInLanguageCount(), is(equalTo(3)));
	}
	
	/**
	 * Tests if a text in language at a given index is correctly returned.
	 */
	@Test
	public void testGetTextInLanguage() {
		final List<TextInLanguage> textInLanguageList = TextInLanguageTestApi.getTextInLanguageList();
		final TextInLanguageList textInLanguageCollection = TextInLanguageTestApi.createTextInLanguageCollectionWithTestData();
		final int index = 1;
		final TextInLanguage actualTextInLanguage = textInLanguageCollection.getTextInLanguage(index);
		final TextInLanguage expectedTextInLanguage = textInLanguageList.get(index);
		
		assertThat(actualTextInLanguage, is(equalTo(expectedTextInLanguage)));
	}
	
	@Test
	public void testGetTextByLanguage_resultFound() {
		final TextInLanguageList textInLanguageList = TextInLanguageTestApi.createTextInLanguageCollectionWithTestData();
		final Optional<TextInLanguage> optionalWithTextInLanguage = textInLanguageList.findTextByLanguage(Locale.ENGLISH);
		
		assertTrue(optionalWithTextInLanguage.isPresent());
		final TextInLanguage actualTextInLanguage = optionalWithTextInLanguage.get();
		final TextInLanguage expectedTextInLanguage = TextInLanguageTestApi.getEnglishText();
		assertThat(actualTextInLanguage, is(equalTo(expectedTextInLanguage)));
	}
	
	@Test
	public void testGetTextByLanguage_resultNotFound() {
		final TextInLanguageList textInLanguageList = TextInLanguageTestApi.createTextInLanguageCollectionWithTestData();
		final Optional<TextInLanguage> optionalWithTextInLanguage = textInLanguageList.findTextByLanguage(Locale.FRENCH);
		
		assertFalse(optionalWithTextInLanguage.isPresent());
	}
}
