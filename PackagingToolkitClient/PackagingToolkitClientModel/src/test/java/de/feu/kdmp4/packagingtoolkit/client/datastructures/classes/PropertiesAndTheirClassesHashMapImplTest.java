package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.PropertiesAndTheirClassesMap;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;

/**
 * Tests the class PropertiesAndTheirClassesHashMapImpl.
 * @author Christopher Olbertz
 *
 */
public class PropertiesAndTheirClassesHashMapImplTest {
	/**
	 * A map with test data.
	 */
	private PropertiesAndTheirClassesMap testMap;
	
	/**
	 * Sets up the test object with two properties. The first property has two classes, the second property has one class. The datatype of the properties
	 * is not relevant.
	 */
	@Before
	public void setUp() {
		testMap = new PropertiesAndTheirClassesHashMapImpl();
		OntologyDatatypeProperty property1 = new OntologyBooleanPropertyImpl("http://my.namespace.de/property1", "Property1", "http://www.w3.org/2001/XMLSchema#boolean");
		OntologyDatatypeProperty property2 = new OntologyBooleanPropertyImpl("http://my.namespace.de/property2", "Property1", "http://www.w3.org/2001/XMLSchema#boolean");
		OntologyClass ontologyClass1 = new OntologyClassImpl("http://my.namespace.de", "class1");
		OntologyClass ontologyClass2 = new OntologyClassImpl("http://my.namespace.de", "class2");
		testMap.addClassToProperty(property1, ontologyClass1);
		testMap.addClassToProperty(property1, ontologyClass2);
		testMap.addClassToProperty(property2, ontologyClass2);
	}
	
	/**
	 * Tests if a class can be added to the map if the property is already in the map.
	 */
	@Test
	public void testAddClassToProperty_propertyAlreadyInMap() {
		OntologyDatatypeProperty property = new OntologyBooleanPropertyImpl("http://my.namespace.de/property1", "Property1", "http://www.w3.org/2001/XMLSchema#boolean");
		OntologyClass ontologyClass = new OntologyClassImpl("http://my.namespace.de", "class3");
		
		testMap.addClassToProperty(property, ontologyClass);
		
		Optional<OntologyClassList> optionalWithOntologyClasses = testMap.getOntologyClassesOfProperty(property);
		assertTrue(optionalWithOntologyClasses.isPresent());
		OntologyClassList ontologyClasses = optionalWithOntologyClasses.get();
		final int classCount = ontologyClasses.getOntologyClassCount();
		assertThat(classCount, is(equalTo(3)));
	}
	
	/**
	 * Tests if a class can be added to the map if the property is not yet in the map.
	 */
	@Test
	public void testAddClassToProperty_propertyNotInMap() {
		final OntologyDatatypeProperty property = new OntologyBooleanPropertyImpl("http://my.namespace.de/property3", "Property3", "http://www.w3.org/2001/XMLSchema#boolean");
		final OntologyClass ontologyClass = new OntologyClassImpl("http://my.namespace.de", "class4");
		
		testMap.addClassToProperty(property, ontologyClass);
		
		final Optional<OntologyClassList> optionalWithOntologyClasses = testMap.getOntologyClassesOfProperty(property);
		assertTrue(optionalWithOntologyClasses.isPresent());
		final OntologyClassList ontologyClasses = optionalWithOntologyClasses.get();
		final int classCount = ontologyClasses.getOntologyClassCount();
		assertThat(classCount, is(equalTo(1)));
	}
	
	/**
	 * Tests if the correct classes are found if we look for the classes in the map.
	 */
	@Test
	public void testGetOntologyClassesOfProperty_resultSomeClasses() {
		final String propertyIri = "http://my.namespace.de/property1";
		final Optional<OntologyClassList> optionalWithOntologyClasses = testMap.getOntologyClassesOfProperty(propertyIri);
		assertTrue(optionalWithOntologyClasses.isPresent());
		final OntologyClassList ontologyClasses = optionalWithOntologyClasses.get();
		final int classCount = ontologyClasses.getOntologyClassCount();
		assertThat(classCount, is(equalTo(2)));
	}
}
