package de.feu.kdmp4.packagingtoolkit.client.ontology.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;

public class OntologyBytePropertyTest {
	@Test
	public void testIsUnsigned_resultTrue() {
		OntologyByteProperty ontologyByteProperty = new OntologyBytePropertyImpl("myByteProperty", true, "myByteProperty", "myBytePropertyRange");
		boolean isUnsigned = ontologyByteProperty.isUnsigned();
		assertTrue(isUnsigned);
	}
	
	@Test
	public void testIsUnsigned_resultFalse() {
		OntologyByteProperty ontologyByteProperty = new OntologyBytePropertyImpl("myByteProperty", false, "myByteProperty", "myBytePropertyRange");
		boolean isUnsigned = ontologyByteProperty.isUnsigned();
		assertFalse(isUnsigned);
	}
}
