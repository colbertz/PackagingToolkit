package de.feu.kdmp4.packagingtoolkit.client.ontology.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class DatatypePropertyListTest {
	private DatatypePropertyCollection datatypePropertyList;
	private List<OntologyDatatypeProperty> expectedList;
	
	/**
	 * Creates a property collection for the test and fills it with values.
	 */
	@Before
	public void setUp() {
		expectedList = new ArrayList<>();
		datatypePropertyList = new DatatypePropertyCollectionImpl();
		final OntologyDatatypeProperty booleanProperty = new OntologyBooleanPropertyImpl();
		booleanProperty.setPropertyId("booleanProperty");
		final OntologyDatatypeProperty doubleProperty = new OntologyDoublePropertyImpl();
		doubleProperty.setPropertyId("doubleProperty");
		final OntologyDatatypeProperty stringProperty = new OntologyStringPropertyImpl();
		stringProperty.setLabel("String-Property");
		stringProperty.setNamespace("http://de.feu.kdmp4");
		stringProperty.setPropertyId("stringProperty");
		final OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		shortProperty.setPropertyId("shortProperty");
		datatypePropertyList.addDatatypeProperty(booleanProperty);
		datatypePropertyList.addDatatypeProperty(doubleProperty);
		datatypePropertyList.addDatatypeProperty(stringProperty);
		datatypePropertyList.addDatatypeProperty(shortProperty);
		
		expectedList.add(booleanProperty);
		expectedList.add(doubleProperty);
		expectedList.add(stringProperty);
		expectedList.add(shortProperty);
	}
	
	/**
	 * Tests if a datatype property can be added to the collection. 
	 */
	@Test
	public void testAddDatatypeProperty() {
		final OntologyDatatypeProperty shortProperty = new OntologyShortPropertyImpl();
		expectedList.add(shortProperty);
		final int expectedSize = expectedList.size();
		
		datatypePropertyList.addDatatypeProperty(shortProperty);
		final int actualSize = datatypePropertyList.getPropertiesCount();
		assertThat(actualSize, is(equalTo(expectedSize)));
		for (int i = 0; i < expectedSize; i++) {
			final OntologyDatatypeProperty expectedDatatypeProperty = expectedList.get(i);
			final OntologyDatatypeProperty actualDatatypeProperty = datatypePropertyList.getDatatypeProperty(i);
			assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
		}
	}
	
	/**
	 * Tests if a property can be determined by its index in the collection.
	 */
	@Test
	public void testGetDatatypeProperty() {
		final OntologyDatatypeProperty expectedDatatypeProperty = expectedList.get(2);
		final OntologyDatatypeProperty actualDatatypeProperty = datatypePropertyList.getDatatypeProperty(2);
		final int expectedSize = expectedList.size();
		final int actualSize = datatypePropertyList.getPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		assertThat(actualDatatypeProperty, is(equalTo(expectedDatatypeProperty)));
	}
	
	/**
	 * Tests if the number of properties in the collection can be determined correctly.
	 */
	@Test
	public void testGetPropertiesCount() {
		final int expectedSize = expectedList.size();
		final int actualSize = datatypePropertyList.getPropertiesCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
	}
	
	/**
	 * Tests the replacement of the value of a datatype property by another one. The test
	 * takes the third property from the collection and tries to replace its value in the collection.  
	 */
	@Test
	public void testReplaceValueOfDatatypeProperty() {
		final OntologyDatatypeProperty thirdDatatypeProperty = datatypePropertyList.getDatatypeProperty(2);
		final OntologyDatatypeProperty propertyWithNewValue = new OntologyStringPropertyImpl(thirdDatatypeProperty.getPropertyId(), 
				thirdDatatypeProperty.getLabel(), thirdDatatypeProperty.getPropertyRange());
		propertyWithNewValue.setPropertyValue("Changed");
		propertyWithNewValue.setUuid(thirdDatatypeProperty.getUuid());
		datatypePropertyList.replaceValueOfDatatypeProperty(propertyWithNewValue);
		
		final OntologyDatatypeProperty changedDatatypeProperty = datatypePropertyList.getDatatypeProperty(2);
		final String changedValue = changedDatatypeProperty.getPropertyValue().toString();
		assertThat(changedValue, is(equalTo("Changed")));
	}
	
	/**
	 * Tests the search for a datatype property by its uuid. There is a datatype property found in the collection. The test
	 * takes the third property from the collection, creates a new one and searches by its uuid. The found property and
	 * the third property must be equal.
	 */
	@Test 
	public void testGetDatatypePropertyByUuid_resultFound() {
		final OntologyDatatypeProperty thirdDatatypeProperty = datatypePropertyList.getDatatypeProperty(2);
		final Uuid uuidOfThirdDatatypeProperty = thirdDatatypeProperty.getUuid();
		final Optional<OntologyDatatypeProperty> foundDatatypePropertyInOptional = datatypePropertyList.getDatatypeProperty(uuidOfThirdDatatypeProperty);
		final OntologyDatatypeProperty foundDatatypeProperty = foundDatatypePropertyInOptional.get();
		assertThat(foundDatatypeProperty, is(equalTo(thirdDatatypeProperty)));
	}
	
	/**
	 * Tests the search for a datatype property by its uuid. There is no datatype property found in the collection. The optional that
	 * contains the result of the search must be empty.
	 */
	@Test 
	public void testGetDatatypePropertyByUuid_resultNotFound() {
		final Uuid uuid = new Uuid();
		final Optional<OntologyDatatypeProperty> foundDatatypePropertyInOptional = datatypePropertyList.getDatatypeProperty(uuid);
		final boolean optionalValuePresent = foundDatatypePropertyInOptional.isPresent();
		assertFalse(optionalValuePresent);
	}
	
	/**
	 * Tests the search for a datatype property by its uuid. There is a datatype property found in the collection. The test
	 * takes the third property from the collection, creates a new one and searches by its uuid. The found property and
	 * the third property must be equal.
	 */
	@Test 
	public void testGetDatatypePropertyByIri_resultFound() {
		final OntologyDatatypeProperty thirdDatatypeProperty = datatypePropertyList.getDatatypeProperty(2);
		final String iriOfThirdDatatypeProperty = thirdDatatypeProperty.getPropertyId();
		final Optional<OntologyDatatypeProperty> foundDatatypePropertyInOptional = datatypePropertyList.getDatatypeProperty(iriOfThirdDatatypeProperty);
		final OntologyDatatypeProperty foundDatatypeProperty = foundDatatypePropertyInOptional.get();
		assertThat(foundDatatypeProperty, is(equalTo(thirdDatatypeProperty)));
	}
	
	/**
	 * Tests the search for a datatype property by its uuid. There is no datatype property found in the collection. The optional that
	 * contains the result of the search must be empty.
	 */
	@Test 
	public void testGetDatatypePropertyByIri_resultNotFound() {
		final String iri = "http://an.other/iri";
		final Optional<OntologyDatatypeProperty> foundDatatypePropertyInOptional = datatypePropertyList.getDatatypeProperty(iri);
		final boolean optionalValuePresent = foundDatatypePropertyInOptional.isPresent();
		assertFalse(optionalValuePresent);
	}
	
	/**
	 * Checks if it is possible to determine whether a property is contained in the collection or not. In this test, the 
	 * property has to be contained in the collection.
	 */
	@Test
	public void testContain_resultPropertyFound() {
		final OntologyDatatypeProperty thirdDatatypeProperty = datatypePropertyList.getDatatypeProperty(2);
		final boolean propertyFound = datatypePropertyList.containsDatatypeProperty(thirdDatatypeProperty);
		assertTrue(propertyFound);
	}
	
	/**
	 * Checks if it is possible to determine whether a property is contained in the collection or not. In this test, the 
	 * property has not to be contained in the collection.
	 */
	@Test
	public void testContain_resultPropertyNotFound() {
		final OntologyDatatypeProperty datatypeProperty = new OntologyStringPropertyImpl("anotherStringProperty", "String-Property", "string");
		final boolean propertyFound = datatypePropertyList.containsDatatypeProperty(datatypeProperty);
		assertFalse(propertyFound);
	}
}
