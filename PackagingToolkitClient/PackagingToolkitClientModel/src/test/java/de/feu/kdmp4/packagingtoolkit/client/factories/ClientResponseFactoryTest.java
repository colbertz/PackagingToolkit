package de.feu.kdmp4.packagingtoolkit.client.factories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyIndividualListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.model.testapi.TaxonomyTestApi;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.DigitalObjectImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;

public class ClientResponseFactoryTest {
	private ClientModelFactory clientModelFactory;
	private ClientResponseFactory clientResponseFactory;
	
	@Before
	public void setUp() {
		clientModelFactory = new ClientModelFactory(new PropertyFactory(new OntologyModelFactory()));
		clientResponseFactory = new ClientResponseFactory();
	}
	
	// ***********************************************
	// testCreateReferenceResponse_withHttpReference() 
	// ***********************************************
	/**
	 * Tests if a http reference can be converted into a reference response.
	 */
	@Test
	public void testCreateReferenceResponse_withHttpReference() {
		String expectedUrl = "http://www.stl.de";
		Reference reference = clientModelFactory.createRemoteReference(expectedUrl);
		ReferenceResponse referenceResponse = clientResponseFactory.toResponse(reference);
		
		String actualUrl = referenceResponse.getUrl();
		assertEquals(expectedUrl, actualUrl);
		assertNull(referenceResponse.getUuidOfFile());
	}
	
	// ***********************************************
	// testCreateReferenceResponse_withFileReference() 
	// ***********************************************
	/**
	 * Tests if a file reference can be converted into a reference response.
	 */
	@Test
	public void testCreateReferenceResponse_withFileReference() {
		Uuid expectedUuid = new Uuid();
		String expectedUrl = "/documents/file.txt";
		Reference reference = clientModelFactory.getLocalFileReference(expectedUrl, expectedUuid);
		ReferenceResponse referenceResponse = clientResponseFactory.toResponse(reference);
		
		String actualUrl = referenceResponse.getUrl();
		UuidResponse actualUuid = referenceResponse.getUuidOfFile();
		assertEquals(expectedUrl, actualUrl);
		assertNotNull(referenceResponse.getUuidOfFile());
		assertEquals(expectedUuid.toString(), actualUuid.toString());
	}
	
	// ***********************************************
	// testCreateDigitalObjectResponse() 
	// ***********************************************
	@Test
	public void testCreateDigitalObjectResponse() {
		Uuid uuidOfInformationPackage = new Uuid();
		String filename = "componentLabels_de.properties";
		DigitalObject digitalObject = new DigitalObjectImpl(filename, uuidOfInformationPackage);
		
		DigitalObjectResponse digitalObjectResponse = clientResponseFactory.toResponse(digitalObject);
		
		assertNotNull(digitalObjectResponse.getMd5Checksum());
		String uuidOfInformationPackageAsString = digitalObjectResponse.getInformationPackageUuid();
		assertEquals(uuidOfInformationPackage.toString(), uuidOfInformationPackageAsString);
		assertEquals(filename, digitalObjectResponse.getFilename());
	}
	
	@Test
	public void testCreateTaxonomyIndividualResponse()  {
		final TaxonomyIndividual taxonomyIndividual1 = TaxonomyTestApi.createTaxonomyIndividualTree();
		
		final TaxonomyIndividualResponse actualTaxonomyIndividual = clientResponseFactory.fromTaxonomyIndividual(taxonomyIndividual1);
		final boolean valuesEqual = TaxonomyTestApi.areValuesEqual(taxonomyIndividual1, actualTaxonomyIndividual);
		assertTrue(valuesEqual);
	}
	
	@Test
	public void testCreateTaxonomyIndividualListResponse() {
		final TaxonomyIndividualCollection taxonomyIndividualCollection = new TaxonomyIndividualListImpl();
		TaxonomyTestApi.addThreeTaxonomyIndividualTreesToTaxonomyIndividualCollection(taxonomyIndividualCollection);
		
		final TaxonomyIndividualListResponse actualIndividuals = clientResponseFactory.fromTaxonomyIndividualCollection(taxonomyIndividualCollection);
		boolean valuesEqual = TaxonomyTestApi.areValuesEqual(taxonomyIndividualCollection, actualIndividuals);
		assertTrue(valuesEqual);
	}
}
