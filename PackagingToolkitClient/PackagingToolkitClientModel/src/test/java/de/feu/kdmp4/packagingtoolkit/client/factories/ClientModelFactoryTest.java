package de.feu.kdmp4.packagingtoolkit.client.factories;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.model.testapi.TaxonomyTestApi;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

public class ClientModelFactoryTest {
	private static final int TEST_DATA_COUNT = 4;
	
	private ClientModelFactory clientModelFactory;
	
	@Mock
	private PropertyFactory propertyFactory;
	
	@Before
	public void setUp() {
		clientModelFactory = new ClientModelFactory(propertyFactory);
	}

	// **************************************************
	// **************** testGetArchive_fromArchiveResponse
	// **************************************************
	/**
	 * Tests the method getArchive(ArchiveResponse).
	 */
	@Test
	public void testGetArchive_fromArchiveResponse() {
		ArchiveResponse testArchiveResponse = prepareTestGetArchive_fromArchiveResponse_ArchiveResponse();
		Archive archive = ClientModelFactory.getArchive(testArchiveResponse);
		checkTestResults_testGetArchive_fromArchiveResponse(testArchiveResponse, archive);
	}
	
	/**
	 * Prepares the object of {@link de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse} 
	 * for the test.
	 * @return The object for the test.
	 */
	private ArchiveResponse prepareTestGetArchive_fromArchiveResponse_ArchiveResponse() {
		ArchiveResponse archiveResponse = new ArchiveResponse();
		archiveResponse.setArchiveId("1234");
		archiveResponse.setCreationDate(DateTimeUtils.getNow());
		archiveResponse.setLastModificationDate(DateTimeUtils.getNow());
		archiveResponse.setPackageType("SIP");
		archiveResponse.setSerializationFormat("OAI-ORE");
		archiveResponse.setTitle("An archive");
		Uuid uuid = new Uuid();
		archiveResponse.setUuid(uuid.toString());
		return archiveResponse;
	}

	/**
	 * Compares the archiveResponse with the created archive.
	 * @param archiveResponse The archive response that has been casted to an archive.
	 * @param archive The archive that has been created in the test.
	 */
	private void checkTestResults_testGetArchive_fromArchiveResponse(ArchiveResponse archiveResponse, Archive archive) {
		String actualArchiveId = archiveResponse.getArchiveId();
		String expectedArchiveId = String.valueOf(archive.getArchiveId());
		assertThat(actualArchiveId, is(equalTo(expectedArchiveId)));
		
		LocalDateTime actualCreationDate = archiveResponse.getCreationDate();
		LocalDateTime expectedCreationDate = archive.getCreationDate();
		assertThat(actualCreationDate, is(equalTo(expectedCreationDate)));
		
		LocalDateTime actualLastModificationDate = archiveResponse.getLastModificationDate();
		LocalDateTime expectedLastModificationDate = archive.getLastModificationDate();
		assertThat(actualLastModificationDate, is(equalTo(expectedLastModificationDate)));
		
		String actualPackageType = archiveResponse.getPackageType();
		String expectedPackageType = archive.getPackageType().toString();
		assertThat(actualPackageType, is(equalTo(expectedPackageType)));
		
		String actualSerializationFormat = archiveResponse.getSerializationFormat();
		String expectedSerializationFormat = archive.getSerializationFormat().toString();
		assertThat(actualSerializationFormat, is(equalTo(expectedSerializationFormat)));
		
		String actualArchiveTitle = archiveResponse.getTitle();
		String expectedArchiveTitle = archiveResponse.getTitle();
		assertThat(actualArchiveTitle, is(equalTo(expectedArchiveTitle)));
		
		String actualUuid = archive.getUuid().toString();
		String expectedUuid = archiveResponse.getUuid();
		assertThat(actualUuid, is(equalTo(expectedUuid)));
	}
	
	// *******************************************************
	// **************** testGetArchiveList_fromArchiveListResponse
	// *******************************************************
	/**
	 * Tests the method getArchiveList(ArchiveListResponse).
	 */
	@Test
	public void testGetArchiveList_fromArchiveListResponse() {
		ArchiveListResponse testArchiveListResponse = prepareTestGetArchive_fromArchiveListResponse_ArchiveListResponse();
		ArchiveList archiveList = ClientModelFactory.getArchiveList(testArchiveListResponse);
		checkTestResults_testGetArchiveList_fromArchiveListResponse(testArchiveListResponse, archiveList);
	}
	
	/**
	 * Creates an archive list response for the test.
	 * @return A archive list response.
	 */
	private ArchiveListResponse prepareTestGetArchive_fromArchiveListResponse_ArchiveListResponse() {
		ArchiveListResponse archiveListResponse = new ArchiveListResponse();
		
		for (int i = 0; i < TEST_DATA_COUNT; i++) {
			ArchiveResponse archiveResponse = prepareTestGetArchive_fromArchiveListResponse_ArchiveResponse(i);
			archiveListResponse.addArchiveToList(archiveResponse);
		}
		
		return archiveListResponse;
	}
	
	/**
	 * Creates an archive response for the list.
	 * @param index A number for the archive response.
	 * @return The created archive response.
	 */
	private ArchiveResponse prepareTestGetArchive_fromArchiveListResponse_ArchiveResponse(int index) {
		ArchiveResponse archiveResponse = new ArchiveResponse();
		archiveResponse.setArchiveId(String.valueOf(index));
		archiveResponse.setCreationDate(DateTimeUtils.getNow());
		archiveResponse.setLastModificationDate(DateTimeUtils.getNow());
		archiveResponse.setPackageType("SIP");
		archiveResponse.setSerializationFormat("OAI-ORE");
		archiveResponse.setTitle("An archive " + index);
		Uuid uuid = new Uuid();
		archiveResponse.setUuid(uuid.toString());
		return archiveResponse;
	}
	
	/**
	 * Compares an archive list with an archive list response.
	 * @param archiveListResponse
	 * @param archiveList
	 */
	private void checkTestResults_testGetArchiveList_fromArchiveListResponse(ArchiveListResponse archiveListResponse, ArchiveList archiveList) {
		assertThat(archiveListResponse.getArchiveCount(), is(equalTo(archiveList.getArchiveCount())));
		
		for (int i = 0; i < archiveListResponse.getArchiveCount(); i++) {
			ArchiveResponse expectedArchive = archiveListResponse.getArchiveAt(i);
			Archive actualArchive = archiveList.getArchiveAt(i);
			checkTestResults_testGetArchiveList_fromArchiveResponse(expectedArchive, actualArchive);
		}
	}
	
	/**
	 * Compares the archiveResponse with the created archive.
	 * @param archiveResponse The archive response that has been casted to an archive.
	 * @param archive The archive that has been created in the test.
	 */
	private void checkTestResults_testGetArchiveList_fromArchiveResponse(ArchiveResponse archiveResponse, Archive archive) {
		String actualArchiveId = archiveResponse.getArchiveId();
		String expectedArchiveId = String.valueOf(archive.getArchiveId());
		assertThat(actualArchiveId, is(equalTo(expectedArchiveId)));
		
		LocalDateTime actualCreationDate = archiveResponse.getCreationDate();
		LocalDateTime expectedCreationDate = archive.getCreationDate();
		assertThat(actualCreationDate, is(equalTo(expectedCreationDate)));
		
		LocalDateTime actualLastModificationDate = archiveResponse.getLastModificationDate();
		LocalDateTime expectedLastModificationDate = archive.getLastModificationDate();
		assertThat(actualLastModificationDate, is(equalTo(expectedLastModificationDate)));
		
		String actualPackageType = archiveResponse.getPackageType();
		String expectedPackageType = archive.getPackageType().toString();
		assertThat(actualPackageType, is(equalTo(expectedPackageType)));
		
		String actualSerializationFormat = archiveResponse.getSerializationFormat();
		String expectedSerializationFormat = archive.getSerializationFormat().toString();
		assertThat(actualSerializationFormat, is(equalTo(expectedSerializationFormat)));
		
		String actualArchiveTitle = archiveResponse.getTitle();
		String expectedArchiveTitle = archiveResponse.getTitle();
		assertThat(actualArchiveTitle, is(equalTo(expectedArchiveTitle)));
		
		String actualUuid = archive.getUuid().toString();
		String expectedUuid = archiveResponse.getUuid();
		assertThat(actualUuid, is(equalTo(expectedUuid)));
	}
	
	// *******************************************************
	// **************** testGetEmptyClientConfigurationData
	// *******************************************************
	/**
	 * Tests the method getEmptyClientConfigurationData().
	 */
	@Test
	public void testGetEmptyClientConfigurationData() {
		ClientConfigurationData clientConfigurationData = ClientModelFactory.getEmptyClientConfigurationData();
		
		String mediatorUrl = clientConfigurationData.getMediatorUrl();
		String serverUrl = clientConfigurationData.getServerUrl();
		
		assertThat(mediatorUrl, is(equalTo(" ")));
		assertThat(serverUrl, isEmptyString());
	}
	
	// *******************************************************
	// **************** testGetClientConfigurationData
	// *******************************************************
	/**
	 * Tests the method getClientConfigurationData().
	 */
	@Test   
	public void testGetClientConfigurationData() {
		String expectedServerUrl = "http://localhost:9191";
		String expectedMediatorUrl = "http://localhost:9292";
		
		ClientConfigurationData clientConfigurationData = clientModelFactory.getClientConfigurationData(expectedMediatorUrl, 
				expectedServerUrl);
		checkTestResults_testGetClientConfigurationData(clientConfigurationData, expectedServerUrl, expectedMediatorUrl);
	}
	
	/**
	 * Tests if clientConfigurationData contains the correct values.
	 * @param clientConfigurationData The data to be checked.
	 * @param expectedServerUrl The server url that is expected.
	 * @param expectedMediatorUrl The mediator url that is expected.
	 */
	private void checkTestResults_testGetClientConfigurationData(ClientConfigurationData clientConfigurationData, String expectedServerUrl,
			String expectedMediatorUrl) {
		String actualServerUrl = clientConfigurationData.getServerUrl();
		String actualMediatorUrl = clientConfigurationData.getMediatorUrl();
		
		assertThat(actualMediatorUrl, is(equalTo(expectedMediatorUrl)));
		assertThat(actualServerUrl, is(equalTo(expectedServerUrl)));
	}
	
	// *******************************************************
	// **************** testGetDigitalObject(DigitalObjectResponse)
	// *******************************************************
	@Test
	public void testGetDigitalObject_fromDigitalObjectResponse() {
		DigitalObjectResponse expectedDigitalObjectResponse = prepareTestGetDigitalObject_DigitalObjectResponse();
		DigitalObject actualDigitalObject = ClientModelFactory.getDigitalObject(expectedDigitalObjectResponse);
		checkTestResults_testGetDigitalObject_DigitalObjectResponse(expectedDigitalObjectResponse, actualDigitalObject);
	}
	
	/**
	 * Prepares the object of {@link de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse} 
	 * for the test.
	 * @return The object for the test.
	 */
	private DigitalObjectResponse prepareTestGetDigitalObject_DigitalObjectResponse() {
		DigitalObjectResponse digitalObjectResponse = new DigitalObjectResponse();
		digitalObjectResponse.setFilename("filename.txt");
		Uuid uuid = new Uuid();
		String informationPackageUuid = uuid.toString();
		digitalObjectResponse.setInformationPackageUuid(informationPackageUuid);
		return digitalObjectResponse;
	}
	
	/**
	 * Compares the digitalObjectResponse with the created digital object.
	 * @param digitalObjectResponse The digital objectresponse that has been casted to an digitalo bject.
	 * @param digitalObject The digitalObject that has been created in the test.
	 */
	private void checkTestResults_testGetDigitalObject_DigitalObjectResponse(DigitalObjectResponse digitalObjectResponse, DigitalObject digitalObject) {
		String actualFilename = digitalObject.getAbsolutePath();
		String expectedFilename = digitalObjectResponse.getFilename();
		assertThat(actualFilename, is(equalTo(expectedFilename)));
	}
	
	// *******************************************************
	// **************** testGetDigitalObject_fromFilename()
	// *******************************************************
	public void testGetDigitalObject_fromFilename() {
		String expectedFilename = "myFilename.sh";
		Uuid uuidOfInformationPackage = new Uuid();
		DigitalObject actualDigitalObject = clientModelFactory.getDigitalObject(expectedFilename, uuidOfInformationPackage);
		
		String actualFilename = actualDigitalObject.getAbsolutePath();
		assertThat(actualDigitalObject, is(equalTo(actualFilename)));
	}
	
	// *******************************************************
	// **************** testGetEmptyDigitalObject()
	// *******************************************************
	@Test
	public void testGetEmptyDigitalObjectList() {
		DigitalObjectCollection digitalObjectList = clientModelFactory.getEmptyDigitalObjectList();
		
		int actualSize = digitalObjectList.getDigitalObjectCount();
		assertThat(actualSize, is(equalTo(0)));		
	}
	
	// *******************************************************
	// **************** testGetDigitalObjectList_fromDigitalObjectListResponse
	// *******************************************************
	/**
	 * Tests the method getDigitalObjectList(DigitalObjectListResponse).
	 */
	@Test
	public void testGetDigitalObjectList_fromDigitalObjectListResponse() {
		DigitalObjectListResponse testDigitalObjectListResponse = prepareTestGetDigitalObject_fromDigitalObjectListResponse_DigitalObjectListResponse();
		DigitalObjectCollection digitalObjectList = ClientModelFactory.getDigitalObjectList(testDigitalObjectListResponse);
		checkTestResults_testGetDigitalObjectList_fromDigitalObjectListResponse(testDigitalObjectListResponse, digitalObjectList);
	}
	
	/**
	 * Creates an digital object list response for the test.
	 * @return A digital object list response.
	 */
	private DigitalObjectListResponse prepareTestGetDigitalObject_fromDigitalObjectListResponse_DigitalObjectListResponse() {
		DigitalObjectListResponse digitalObjectListResponse = new DigitalObjectListResponse();
		
		for (int i = 0; i < TEST_DATA_COUNT; i++) {
			DigitalObjectResponse digitalObjectResponse = prepareTestGetDigitalObject_fromDigitalObjectListResponse_DigitalObjectResponse(i);
			digitalObjectListResponse.addDigitalObjectToList(digitalObjectResponse);
		}
		
		return digitalObjectListResponse;
	}
	
	/**
	 * Creates an digitalObject response for the list.
	 * @param index A number for the digitalObject response.
	 * @return The created digitalObject response.
	 */
	private DigitalObjectResponse prepareTestGetDigitalObject_fromDigitalObjectListResponse_DigitalObjectResponse(int index) {
		DigitalObjectResponse digitalObjectResponse = new DigitalObjectResponse();
		digitalObjectResponse.setFilename("filename" + index + ".txt");
		Uuid uuid = new Uuid();
		digitalObjectResponse.setInformationPackageUuid(uuid.toString());
		return digitalObjectResponse;
	}
	
	/**
	 * Compares an digitalObject list with an digitalObject list response.
	 * @param digitalObjectListResponse
	 * @param digitalObjectList
	 */
	private void checkTestResults_testGetDigitalObjectList_fromDigitalObjectListResponse(DigitalObjectListResponse digitalObjectListResponse, DigitalObjectCollection digitalObjectList) {
		assertThat(digitalObjectListResponse.getDigitalObjectCount(), is(equalTo(digitalObjectList.getDigitalObjectCount())));
		
		for (int i = 0; i < digitalObjectListResponse.getDigitalObjectCount(); i++) {
			DigitalObjectResponse expectedDigitalObject = digitalObjectListResponse.getDigitalObjectAt(i);
			DigitalObject actualDigitalObject = digitalObjectList.getDigitalObjectAt(i);
			checkTestResults_testGetDigitalObjectList_fromDigitalObjectResponse(expectedDigitalObject, actualDigitalObject);
		}
	}
	
	/**
	 * Compares the digitalObjectResponse with the created digitalObject.
	 * @param digitalObjectResponse The digitalObject response that has been casted to an digitalObject.
	 * @param digitalObject The digitalObject that has been created in the test.
	 */
	private void checkTestResults_testGetDigitalObjectList_fromDigitalObjectResponse(DigitalObjectResponse digitalObjectResponse, DigitalObject digitalObject) {
		String actualFilename = digitalObject.getAbsolutePath();
		String expectedFilename = digitalObjectResponse.getFilename();
		assertThat(actualFilename, is(equalTo(expectedFilename)));
	}
	
	// *******************************************************
	// **************** testGetDigitalObjectList_fromDigitalObjectListResponse
	// *******************************************************
	/**
	 * Tests the method getOntologyClass(OntologyClassResponse).
	 */
	@Test
	public void testGetOntologyClass_fromOntologyClassResponse() {
		OntologyClassResponse testOntologyClassResponse = prepareTestGetOntologyClass_fromOntologyClassResponse_OntologyClassResponse();
		OntologyClass ontologyClass = clientModelFactory.createOntologyClass(testOntologyClassResponse);
		checkResults_testGetOntologyClass_fromOntologyClassResponse(ontologyClass, testOntologyClassResponse);
	}
	
	/**
	 * Prepares a ontology class for the test.
	 * @return The ontology class.
	 */
	private OntologyClassResponse prepareTestGetOntologyClass_fromOntologyClassResponse_OntologyClassResponse() {
		OntologyClassResponse ontologyClassResponse = new OntologyClassResponse();
		ontologyClassResponse.setIri("anIri");
		ontologyClassResponse.setLocalName("theLocalname");
		ontologyClassResponse.setNamespace("theNamespace");
		ontologyClassResponse.setOntologyClassId(555);
		ontologyClassResponse = prepareTestGetOntologyClass_fromOntologyClassResponse_addDatatypeProperties(ontologyClassResponse);
		ontologyClassResponse = prepareTestGetOntologyClass_fromOntologyClassResponse_addSubclasses(ontologyClassResponse);
		return ontologyClassResponse;
	}
	
	/**
	 * Adds some data properties to the ontology class for the test.
	 * @param ontologyClassResponse The ontology class without the datatype properties.
	 * @return The ontology class with the datatype properties.
	 */
	private OntologyClassResponse prepareTestGetOntologyClass_fromOntologyClassResponse_addDatatypeProperties(OntologyClassResponse ontologyClassResponse) {
		DatatypePropertyResponse ontologyBooleanProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyBooleanProperty.setPropertyName("booleanProperty");
		ontologyBooleanProperty.setDatatype(OntologyDatatypeResponse.BOOLEAN);
		ontologyBooleanProperty.setValue("true");
		
		DatatypePropertyResponse ontologyByteProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyByteProperty.setPropertyName("byteProperty");
		ontologyByteProperty.setDatatype(OntologyDatatypeResponse.BYTE);
		DatatypePropertyListResponse datatypePropertyListResponse = new DatatypePropertyListResponse();
		datatypePropertyListResponse.addDatatypePropertyToList(ontologyByteProperty);
		datatypePropertyListResponse.addDatatypePropertyToList(ontologyBooleanProperty);
		ontologyClassResponse.setProperties(datatypePropertyListResponse);
		
		return ontologyClassResponse;
	}
	
	/**
	 * Adds some sub classes to the ontology class for the test.
	 * @param ontologyClassResponse The ontology class without the sub classes.
	 * @return The ontology class with the sub classes.
	 */
	private OntologyClassResponse prepareTestGetOntologyClass_fromOntologyClassResponse_addSubclasses(OntologyClassResponse ontologyClassResponse) {
		OntologyClassResponse ontologySubClassResponse1 = new OntologyClassResponse();
		ontologySubClassResponse1.setIri("subclass1");
		ontologySubClassResponse1.setLocalName("thelocalname1");
		ontologySubClassResponse1.setNamespace("namespace1");
		ontologySubClassResponse1.setOntologyClassId(333);
		
		OntologyClassResponse ontologySubClassResponse2 = new OntologyClassResponse();
		ontologySubClassResponse2.setIri("subclass2");
		ontologySubClassResponse2.setLocalName("thelocalname2");
		ontologySubClassResponse2.setNamespace("namespace2");
		ontologySubClassResponse2.setOntologyClassId(444);
		
		return ontologyClassResponse;
	}
	
	private void checkResults_testGetOntologyClass_fromOntologyClassResponse(OntologyClass ontologyClass, OntologyClassResponse ontologyClassResponse) {
		String actualLocalName = ontologyClass.getLocalName();
		String expectedLocalName = ontologyClassResponse.getLocalName();
		assertThat(actualLocalName, is(equalTo(expectedLocalName)));
		
		String actualNamespace = ontologyClass.getNamespace();
		String expectedNamespace = ontologyClassResponse.getNamespace();
		assertThat(actualNamespace, is(equalTo(expectedNamespace)));
		
		int expectedDatatypePropertySize = 2;
		int actualDatatypePropertySize = ontologyClass.getDatatypePropertiesCount();
		assertThat(actualDatatypePropertySize, is(equalTo(expectedDatatypePropertySize)));
		
		int expectedSubclassesSize = 2;
		int actualSubclassesSize = ontologyClass.getSubclassCount();
		assertThat(actualSubclassesSize, is(equalTo(expectedSubclassesSize)));
	}
	
	// *******************************************************
	// **************** testGetDataSourceTool_fromDataSourceToolResponse()
	// *******************************************************
	@Test
	public void testGetDataSourceTool_fromDataSourceToolResponse() {
		DataSourceToolResponse dataSourceToolResponse = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceToolResponse.setDataSourceToolName("abc");
		dataSourceToolResponse.setActivated(true);
		
		DataSourceTool dataSourceTool = clientModelFactory.createDataSourceTool(dataSourceToolResponse);
		
		assertEquals(dataSourceToolResponse.getDataSourceToolName(), dataSourceTool.getToolName());
		assertEquals(dataSourceToolResponse.isActivated(), dataSourceTool.isActivated());
	}
	
	// *******************************************************
	// **************** testGetDataSourceToolCollection_fromDataSourceToolListResponse()
	// *******************************************************
	@Test
	public void testGetDataSourceToolCollection_fromDataSourceToolListResponse() {
		DataSourceToolListResponse dataSourceToolListResponse = prepareTest_testGetDataSourceToolCollection_fromDataSourceToolListResponse();
		
		DataSourceToolCollection dataSourceTools = clientModelFactory.createDataSourceToolCollection(dataSourceToolListResponse);
		checkResults_testGetDataSourceToolCollection_fromDataSourceToolListResponse(dataSourceTools, dataSourceToolListResponse);
	}
	
	private DataSourceToolListResponse prepareTest_testGetDataSourceToolCollection_fromDataSourceToolListResponse() {
		DataSourceToolResponse dataSourceToolResponse1 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceToolResponse1.setDataSourceToolName("abc");
		dataSourceToolResponse1.setActivated(true);
		
		DataSourceToolResponse dataSourceToolResponse2 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceToolResponse2.setDataSourceToolName("xyz");
		dataSourceToolResponse2.setActivated(false);
		
		DataSourceToolListResponse dataSourceToolList = ResponseModelFactory.getDataSourceToolListResponse();
		dataSourceToolList.addDataSourceTool(dataSourceToolResponse1);
		dataSourceToolList.addDataSourceTool(dataSourceToolResponse2);
		
		return dataSourceToolList;
	}
	
	private void checkResults_testGetDataSourceToolCollection_fromDataSourceToolListResponse(DataSourceToolCollection 
			dataSourceTools, DataSourceToolListResponse dataSourceToolListResponse) {
		assertEquals(dataSourceTools.getDataSourceToolCount(), dataSourceToolListResponse.getDataSourceToolsCount());
		
		for (int i = 0; i < dataSourceTools.getDataSourceToolCount(); i++) {
			DataSourceTool dataSourceTool = dataSourceTools.getDataSourceToolAt(i);
			DataSourceToolResponse dataSourceToolResponse = dataSourceToolListResponse.getDataSourceTool(i);
			String actualToolName =  dataSourceToolResponse.getDataSourceToolName();
			String expectedToolName = dataSourceTool.getToolName();
			assertEquals(expectedToolName, actualToolName);
			
			assertEquals(dataSourceToolResponse.isActivated(), dataSourceTool.isActivated());
		}
	}
	
	// *******************************************************
	// **************** testGetDataSource_fromDataSourceResponse()
	// *******************************************************
	@Test
	public void testGetDataSource_fromDataSourceResponse() {
		DataSourceResponse dataSourceResponse = prepareTest_testGetDataSource_fromDataSourceResponse();
		DataSource dataSource = clientModelFactory.createDataSource(dataSourceResponse);
		
		checkResults_testGetDataSource_fromDataSourceResponse(dataSourceResponse, dataSource);
	}
	
	private DataSourceResponse prepareTest_testGetDataSource_fromDataSourceResponse() {
		DataSourceResponse dataSourceResponse = ResponseModelFactory.getDataSourceResponse();
		dataSourceResponse.setDataSourceName("abcxyz");
		
		DataSourceToolResponse dataSourceTool1 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceTool1.setActivated(true);
		dataSourceTool1.setDataSourceToolName("qwertz");
		DataSourceToolResponse dataSourceTool2 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceTool2.setActivated(false);
		dataSourceTool2.setDataSourceToolName("qwertz qwertz");
		
		DataSourceToolListResponse dataSourceToolListResponse = ResponseModelFactory.getDataSourceToolListResponse();
		dataSourceToolListResponse.addDataSourceTool(dataSourceTool1);
		dataSourceToolListResponse.addDataSourceTool(dataSourceTool2);
		
		dataSourceResponse.setDataSourceTools(dataSourceToolListResponse);
		
		return dataSourceResponse;
	}
	
	private void checkResults_testGetDataSource_fromDataSourceResponse(DataSourceResponse dataSourceResponse, 
			DataSource dataSource) {
		assertEquals(dataSourceResponse.getDataSourceName(), dataSource.getDataSourceName());
		DataSourceToolListResponse dataSourceToolListResponse = dataSourceResponse.getDataSourceTools();
		DataSourceToolCollection dataSourceToolCollection = dataSource.getDataSourceTools();
		
		for (int i = 0; i < dataSourceToolCollection.getDataSourceToolCount(); i++) {
			DataSourceTool dataSourceTool = dataSourceToolCollection.getDataSourceToolAt(i);
			DataSourceToolResponse dataSourceToolResponse = dataSourceToolListResponse.getDataSourceTool(i);
			String actualToolName =  dataSourceToolResponse.getDataSourceToolName();
			String expectedToolName = dataSourceTool.getToolName();
			assertEquals(expectedToolName, actualToolName);
			
			assertEquals(dataSourceToolResponse.isActivated(), dataSourceTool.isActivated());
		}
	}
	
	// *******************************************************
	// **************** testGetDataSourceCollection_fromDataSourceResponseList()
	// *******************************************************
	@Test
	public void testGetDataSourceCollection_fromDataSourceResponseList() {
		DataSourceListResponse dataSourceListResponse = prepareTest_testGetDataSourceCollection_fromDataSourceResponseList();
		DataSourceCollection dataSourceCollection = clientModelFactory.createDataSourceCollection(dataSourceListResponse);
		checkResults_testGetDataSourceCollection_fromDataSourceResponseList(dataSourceListResponse, dataSourceCollection);
	}
	
	private DataSourceListResponse prepareTest_testGetDataSourceCollection_fromDataSourceResponseList() {
		DataSourceResponse dataSourceResponse1 = ResponseModelFactory.getDataSourceResponse();
		dataSourceResponse1.setDataSourceName("asdf");
		DataSourceResponse dataSourceResponse2 = ResponseModelFactory.getDataSourceResponse();
		dataSourceResponse2.setDataSourceName("yxcv");
		
		DataSourceListResponse dataSourceListResponse = ResponseModelFactory.getDataSourceListResponse();
		dataSourceListResponse.addDataSource(dataSourceResponse1);
		dataSourceListResponse.addDataSource(dataSourceResponse2);
		
		return dataSourceListResponse;
	}
	
	private void checkResults_testGetDataSourceCollection_fromDataSourceResponseList(DataSourceListResponse 
			dataSourceListResponse, DataSourceCollection dataSourceCollection) {
		assertEquals(dataSourceListResponse.getDataSources().size(), dataSourceCollection.getDataSourceCount());
		
		for (int i = 0; i < dataSourceCollection.getDataSourceCount(); i++) {
			DataSource dataSource = dataSourceCollection.getDataSourceAt(i);
			DataSourceResponse dataSourceResponse = dataSourceListResponse.getDataSources().get(i);
			assertEquals(dataSource.getDataSourceName(), dataSourceResponse.getDataSourceName());
		}
	}
	
	// *******************************************************
	// **************** testCreateCopyOfOntologyIndividual()
	// *******************************************************
	/**
	 * Tests if a copy of an individual can be created. The test first checks if the properties of the copy of the individual
	 * have the correct values. Then the values of the copy are modified. Now the test checks if the values of the 
	 * original individual and the copied individual are not the same.
	 */
	@Test
	public void testCreateCopyOfOntologyIndividual() {
		PropertyFactory spiedPropertyFactory = spy(new PropertyFactory(new OntologyModelFactory()));
		clientModelFactory = new ClientModelFactory(spiedPropertyFactory);
		OntologyIndividual originalOntologyIndividual = prepareTest_testCreateCopyOfOntologyIndividual_createIndividual(spiedPropertyFactory);
		
		OntologyIndividual copyOfOntologyIndividual = clientModelFactory.createCopyOfOntologyIndividual(originalOntologyIndividual);
		
		checkResults_testCreateCopyOfOntologyIndividual_copyOfIndividual(copyOfOntologyIndividual);
		copyOfOntologyIndividual = testCreateCopyOfOntologyIndividual_modifyIndividual(copyOfOntologyIndividual);
		checkResults_testCreateCopyOfOntologyIndividual_individualNotEqual(originalOntologyIndividual, copyOfOntologyIndividual);
	}
	
	/**
	 * Creates an individual that can be copied during the test.
	 * @param spiedPropertyFactory The property factory used for creating the properties of this individual.
	 * @return The individual used for the test.
	 */
	private OntologyIndividual prepareTest_testCreateCopyOfOntologyIndividual_createIndividual(PropertyFactory spiedPropertyFactory) {
		OntologyClass ontologyClass = new OntologyClassImpl("my", "class");
		OntologyIndividual originalOntologyIndividual = new OntologyIndividualImpl(ontologyClass);
		OntologyDatatypeProperty integerProperty = spiedPropertyFactory.getProperty("integer", "integerProperty", "integerProperty");
		OntologyDatatypeProperty stringProperty = spiedPropertyFactory.getProperty("string", "stringProperty", "stringProperty");
		integerProperty.setPropertyValue(45);
		stringProperty.setPropertyValue("Hello World");
		
		originalOntologyIndividual.addDatatypeProperty(integerProperty);
		originalOntologyIndividual.addDatatypeProperty(stringProperty);
		
		return originalOntologyIndividual;
	}
	
	/**
	 * Checks if the copy of the individual has the correct values.
	 * @param copyOfOntologyIndividual  The individual that is a copy of the original individual.
	 */
	private void checkResults_testCreateCopyOfOntologyIndividual_copyOfIndividual(OntologyIndividual copyOfOntologyIndividual) {
		OntologyDatatypeProperty copyOfIntegerProperty = copyOfOntologyIndividual.getDatatypePropertyAt(0);
		assertEquals(45l, copyOfIntegerProperty.getPropertyValue());
		OntologyDatatypeProperty copyOfStringProperty = copyOfOntologyIndividual.getDatatypePropertyAt(1);
		assertEquals("Hello World", copyOfStringProperty.getPropertyValue());
	}
	
	/**
	 * Modifies the values of an individual for the test.
	 * @param ontologyIndividual The individual whose values have to be modified.
	 * @return The individual with the modified values.
	 */
	private OntologyIndividual testCreateCopyOfOntologyIndividual_modifyIndividual(OntologyIndividual ontologyIndividual) {
		OntologyDatatypeProperty integerProperty = ontologyIndividual.getDatatypePropertyAt(0);
		OntologyDatatypeProperty stringProperty = ontologyIndividual.getDatatypePropertyAt(1);
		integerProperty.setPropertyValue(100l);
		stringProperty.setPropertyValue("And another World");
		return ontologyIndividual;
	}
	
	/**
	 * Checks if the original individual and its copy are not equal after the values of the copy have been modified.
	 * @param originalOntologyIndividual The original unmodified individual.
	 * @param copyOfOntologyIndividual The copy which has been modified.
	 */
	private void checkResults_testCreateCopyOfOntologyIndividual_individualNotEqual(OntologyIndividual originalOntologyIndividual, 
			OntologyIndividual copyOfOntologyIndividual) {
		OntologyDatatypeProperty copyOfIntegerProperty = copyOfOntologyIndividual.getDatatypePropertyAt(0);
		OntologyDatatypeProperty copyOfStringProperty = copyOfOntologyIndividual.getDatatypePropertyAt(1);
		OntologyDatatypeProperty originalIntegerProperty = originalOntologyIndividual.getDatatypePropertyAt(0);
		assertNotEquals(originalIntegerProperty.getPropertyValue(), copyOfIntegerProperty.getPropertyValue());
		OntologyDatatypeProperty originalStringProperty = originalOntologyIndividual.getDatatypePropertyAt(1);
		assertNotEquals(originalStringProperty.getPropertyValue(), copyOfStringProperty.getPropertyValue());
	}
	
	/**
	 * Tests the creation of an individual in a taxonomy.
	 */
	@Test
	public void testCreateTaxonomyIndividual() {
		final String iriAsString = "http://mynamespace#individual";
		final String preferredLabel = "myLabel";
		final TaxonomyIndividual taxonomyIndividual = ClientModelFactory.createTaxonomyIndividual(iriAsString, preferredLabel);
		
		assertThat(taxonomyIndividual.getPreferredLabel(), is(equalTo(preferredLabel)));
		assertThat(taxonomyIndividual.getIri().toString(), is(equalTo(iriAsString)));
	}
	
	/**
	 * Tests the creation of an individual in a taxonomy. The taxonomy object has the values of a response object.
	 */
	@Test
	public void testCreateTaxonomyIndividual_withTaxonomyIndividualResponse() {
		final TaxonomyIndividualResponse taxonomyIndividual = TaxonomyTestApi.createTaxonomyIndividualResponseTree();
		final TaxonomyIndividual actualRootIndividual = ClientModelFactory.createTaxonomyIndividual(taxonomyIndividual);
	
		final boolean valuesEqual = TaxonomyTestApi.areValuesEqual(actualRootIndividual, taxonomyIndividual);
		assertTrue(valuesEqual);
	}
	
	/**
	 * Tests the creation of a collection with individual of a taxonomy. The individuals in a response object should be transformed
	 * into 
	 */
	@Test
	public void testCreateTaxonomyCollection() {
		final TaxonomyListResponse taxonomyResponses = TaxonomyTestApi.createTaxonomyListResponseWithTwoTaxonomies();
		final TaxonomyCollection taxonomies = ClientModelFactory.createTaxonomyCollection(taxonomyResponses);
		boolean valuesEqual = TaxonomyTestApi.areValuesEqual(taxonomies, taxonomyResponses);
		assertTrue(valuesEqual);
	}
}
