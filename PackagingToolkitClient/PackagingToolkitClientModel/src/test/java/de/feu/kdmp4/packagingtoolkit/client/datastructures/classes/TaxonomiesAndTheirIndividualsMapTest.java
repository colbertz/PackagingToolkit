package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;

import static org.junit.Assert.assertTrue; 
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.TaxonomiesAndTheirIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.model.testapi.TaxonomyTestApi;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;

public class TaxonomiesAndTheirIndividualsMapTest {
	private TaxonomiesAndTheirIndividualsMap map;
	
	@Before
	public void setUp() {
		map = ClientModelFactory.createEmptyTaxonomiesAndTheirIndividualsMap();
	}
	
	/**
	 * Tests if an individual can be added to a taxonomy in the map. Six individuals are added to the map: three individuals are in one collection.
	 * First the individual of the first collection are added to the map. The the individuals of this taxonomy is determined from the map and then
	 * the number of individuals is checked. The same with the next collection with three individuals. 
	 */
	@Test
	public void testAddIndividual() {
		final TaxonomyIndividualCollection taxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividuals);
		final Taxonomy taxonomy = TaxonomyTestApi.createTaxonomy();
		
		for (int i = 0; i < taxonomyIndividuals.getTaxonomyIndividualCount(); i++) {
			final TaxonomyIndividual taxonomyIndividual = taxonomyIndividuals.getTaxonomyIndividual(i).get();
			map.addIndividualToTaxonomy(taxonomy.getIri(), taxonomyIndividual);
		}
		
		final Optional<TaxonomyIndividualCollection> optionalWithFirstTaxonomyIndividuals = map.findIndividualsOfTaxonomy(taxonomy.getIri());
		assertTrue(optionalWithFirstTaxonomyIndividuals.isPresent());
		final TaxonomyIndividualCollection firstTaxonomyIndividuals = optionalWithFirstTaxonomyIndividuals.get();
		assertThat(firstTaxonomyIndividuals.getSize(), is(equalTo(3)));
		
		final TaxonomyIndividualCollection anotherTaxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(anotherTaxonomyIndividuals);
		
		for (int i = 0; i < anotherTaxonomyIndividuals.getTaxonomyIndividualCount(); i++) {
			final TaxonomyIndividual taxonomyIndividual = taxonomyIndividuals.getTaxonomyIndividual(i).get();
			map.addIndividualToTaxonomy(taxonomy.getIri(), taxonomyIndividual);
		}
		
		final Optional<TaxonomyIndividualCollection> pptionalWithFSecondTaxonomyIndividuals = map.findIndividualsOfTaxonomy(taxonomy.getIri());
		assertTrue(pptionalWithFSecondTaxonomyIndividuals.isPresent());
		final TaxonomyIndividualCollection secondTaxonomyIndividuals = optionalWithFirstTaxonomyIndividuals.get();
		assertThat(secondTaxonomyIndividuals.getSize(), is(equalTo(6)));
	}
	
	/**
	 * Tests if the method for adding several individual to the map is working. Two collection with three individuals are created and 
	 * added to the map. After every addition the number of individuals assigned to the taxonomy is checked. 
	 */
	@Test
	public void testAddIndividuals() {
		final TaxonomyIndividualCollection taxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividuals);
		final Taxonomy taxonomy = TaxonomyTestApi.createTaxonomy();
		
		map.addIndividualsToTaxonomy(taxonomy.getIri(), taxonomyIndividuals);
		
		final Optional<TaxonomyIndividualCollection> optionalWithFirstTaxonomyIndividuals = map.findIndividualsOfTaxonomy(taxonomy.getIri());
		assertTrue(optionalWithFirstTaxonomyIndividuals.isPresent());
		final TaxonomyIndividualCollection firstTaxonomyIndividuals = optionalWithFirstTaxonomyIndividuals.get();
		assertThat(firstTaxonomyIndividuals.getSize(), is(equalTo(3)));
		
		final TaxonomyIndividualCollection anotherTaxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(anotherTaxonomyIndividuals);
		
		map.addIndividualsToTaxonomy(taxonomy.getIri(), anotherTaxonomyIndividuals);
		final Optional<TaxonomyIndividualCollection> optionalWithSecondTaxonomyIndividuals = map.findIndividualsOfTaxonomy(taxonomy.getIri());
		assertTrue(optionalWithSecondTaxonomyIndividuals.isPresent());
		final TaxonomyIndividualCollection secondTaxonomyIndividuals = optionalWithFirstTaxonomyIndividuals.get();
		assertThat(secondTaxonomyIndividuals.getTaxonomyIndividualCount(), is(equalTo(6)));
	}
	
	/**
	 * Tests if the individuals assigned to a certain taxonomy can be found.
	 */
	@Test
	public void testFindIndividualOfTaxonomy() {
		final TaxonomyIndividualCollection taxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividuals);
		final Taxonomy taxonomy = TaxonomyTestApi.createTaxonomy();
		map.addIndividualsToTaxonomy(taxonomy.getIri(), taxonomyIndividuals);
		
		final Optional<TaxonomyIndividualCollection> optionalWithFirstTaxonomyIndividuals = map.findIndividualsOfTaxonomy(taxonomy.getIri());
		assertTrue(optionalWithFirstTaxonomyIndividuals.isPresent());
		final TaxonomyIndividualCollection firstTaxonomyIndividuals = optionalWithFirstTaxonomyIndividuals.get();
		assertThat(firstTaxonomyIndividuals.getSize(), is(equalTo(3)));
	}
	
	/**
	 * Tests the method that writes the elements contained in the map in a list. Two taxonomies and lists with three individuals per taxonomy 
	 * are inserted into the map. Then the resulting list must contain two elements. 
	 */
	@Test
	public void testAsList() {
		final TaxonomyIndividualCollection taxonomyIndividuals1 = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividuals1);
		final Taxonomy taxonomy1 = TaxonomyTestApi.createTaxonomy();
		
		final TaxonomyIndividualCollection taxonomyIndividuals2 = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		TaxonomyTestApi.createTaxonomyIndividualCollectionWithThreeIndividuals(taxonomyIndividuals2);
		final Taxonomy taxonomy2 = TaxonomyTestApi.createTaxonomy();
		
		map.addIndividualsToTaxonomy(taxonomy1.getIri(), taxonomyIndividuals1);
		map.addIndividualsToTaxonomy(taxonomy2.getIri(), taxonomyIndividuals2);
		
		taxonomyIndividuals1.setIriOfTaxonomy(taxonomy1.getIri());
		taxonomyIndividuals2.setIriOfTaxonomy(taxonomy2.getIri());
		
		final List<TaxonomyIndividualCollection> allIndividuals = map.asList();
		assertThat(allIndividuals.size(), is(equalTo(2)));
		assertThat(allIndividuals, containsInAnyOrder(taxonomyIndividuals1, taxonomyIndividuals2));
	}
}
