package de.feu.kdmp4.packagingtoolkit.client.model.testapi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TextInLanguageListImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguageList;

/**
 * Contain some method for supporting the tests of textInLanguages.
 * @author Christopher Olbertz
 *
 */
public class TextInLanguageTestApi {
	/**
	 * A list with textInLanguages for the test.
	 */
	private static List<TextInLanguage> textInLanguageList;
	
	/**
	 * Initializes the test list with three textInLanguages.
	 */
	static {
		textInLanguageList = createThreeTextInLanguages();
	}
	
	/**
	 * Returns the list with the test objects.
	 * @return The list with the test objects.
	 */
	public static List<TextInLanguage> getTextInLanguageList() {
		return textInLanguageList;
	}

	/**
	 * Returns the value with the german text from the test list. 
	 * @return The value with the german text.
	 */
	public static TextInLanguage getGermanText() {
		return textInLanguageList.get(0);
	}
	
	/**
	 * Returns the value with the english text from the test list. 
	 * @return The value with the english text.
	 */
	public static TextInLanguage getEnglishText() {
		return textInLanguageList.get(1);
	}
	
	/**
	 * Creates a list with three textInLanguages.
	 * @return A list with three textInLanguages.
	 */
	private static final List<TextInLanguage> createThreeTextInLanguages() {
		final TextInLanguage textInLanguage1 = ClientModelFactory.createTextInLanguage("text 1", Locale.GERMAN);
		final TextInLanguage textInLanguage2 = ClientModelFactory.createTextInLanguage("text 2", Locale.ENGLISH);
		final TextInLanguage textInLanguage3 = ClientModelFactory.createTextInLanguage("text 3", Locale.CHINESE);
		
		final List<TextInLanguage> textInLanguageList = new ArrayList<>();
		textInLanguageList.add(textInLanguage1);
		textInLanguageList.add(textInLanguage2);
		textInLanguageList.add(textInLanguage3);
		
		return textInLanguageList;
	}
	
	/**
	 * Creates a collection with three textInLanguages.
	 * @return A collection with three textInLanguages.
	 */
	public static final Collection<TextInLanguage> createCollectionWithThreeTextInLanguages() {
		final Collection<TextInLanguage> textInLanguages = new ArrayList<>(textInLanguageList);
		return textInLanguages;
	}
	
	/**
	 * Compares a collection as an result of a test with the list that contains the test data.
	 * @param textInLanguageCollection The collection we want to test.
	 * @return True if the collection contains the correct value false otherwise.
	 */
	public static final boolean compareWithTestList(TextInLanguageList textInLanguageCollection) {
		if (textInLanguageCollection.getTextsInLanguageCount() != textInLanguageList.size()) {
			return false;
		}
		
		for (int i = 0; i < textInLanguageCollection.getTextsInLanguageCount(); i++) {
			TextInLanguage actualTextInLanguage = textInLanguageCollection.getTextInLanguage(i);
			TextInLanguage expectedTextInLanguage = textInLanguageList.get(i);
			
			if (!actualTextInLanguage.equals(expectedTextInLanguage)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Creates a collection with some test data.
	 * @return The newly created collection.
	 */
	public static final TextInLanguageList createTextInLanguageCollectionWithTestData() {
		TextInLanguageList inLanguageCollection = new TextInLanguageListImpl();
		
		for (TextInLanguage textInLanguage: textInLanguageList) {
			inLanguageCollection.addTextInLanguage(textInLanguage);
		}
		
		return inLanguageCollection;
	}
}
