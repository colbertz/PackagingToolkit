package de.feu.kdmp4.packagingtoolkit.client.model.testapi;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.model.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyResponse;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Creates some testdate for the tests of the taxonomy and the related classes.
 * @author C-hristopher Olbertz
 *
 */
public class TaxonomyTestApi {
	/**
	 * Creates an individual with a random uuid as localname.
	 * @return The individual.
	 */
	public static TaxonomyIndividual createTaxonomyIndividual() {
		final Uuid uuid = new Uuid();
		final Namespace namespace = new NamespaceImpl("http://www.mynamespace.de");
		final LocalName localName = new LocalNameImpl(uuid.toString());
		final Iri iriOfIndividual = new IriImpl(namespace, localName);
		final String title = uuid.toString();
		final TaxonomyIndividual taxonomyIndividual = new TaxonomyIndividualImpl(iriOfIndividual, title);
		return taxonomyIndividual;
	}
	
	/**
	 * Adds three individuals as narrowers to the individual.
	 * @param taxonomyIndividual The individual we want to add three narrowers to.
	 * @return The second of the three narrowers.
	 */
	public static TaxonomyIndividual addThreeNarrowersToTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual ) {
		final TaxonomyIndividual narrower1 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower1);
		
		final TaxonomyIndividual narrower2 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower2);
		
		final TaxonomyIndividual narrower3 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower3);
		
		return narrower2;
	}
	
	/**
	 * Adds three taxonomies to the Collection.
	 * @param taxonomies The collection we want to add three taxonomies to.
	 * @return The second of the three taxonomies.
	 */
	public static Taxonomy addThreeTaxonomiesToTaxonomyCollection(final TaxonomyCollection taxonomies) {
		final Taxonomy taxonomy1 = TaxonomyTestApi.createTaxonomy();
		taxonomies.addTaxonomy(taxonomy1);
		
		final Taxonomy taxonomy2 = TaxonomyTestApi.createTaxonomy();
		taxonomies.addTaxonomy(taxonomy2);
		
		final Taxonomy taxonomy3 = TaxonomyTestApi.createTaxonomy();
		taxonomies.addTaxonomy(taxonomy3);
		
		return taxonomy2;
	}
	
	
	/**
	 * Adds three taxonomy individuals to the Collection. Every individuals has a narrower tree.
	 * @param taxonomyIndividuals The collection we want to add three taxonomy individuals to.
	 * @return The second of the three taxonomy individuals.
	 */
	public static TaxonomyIndividual addThreeTaxonomyIndividualTreesToTaxonomyIndividualCollection(final TaxonomyIndividualCollection taxonomyIndividuals) {
		final TaxonomyIndividual taxonomyIndividual1 = createTaxonomyIndividualTree();
		final TaxonomyIndividual taxonomyIndividual2 = createTaxonomyIndividualTree();
		final TaxonomyIndividual taxonomyIndividual3 = createTaxonomyIndividualTree();
		
		taxonomyIndividuals.addTaxonomyIndividual(taxonomyIndividual1);
		taxonomyIndividuals.addTaxonomyIndividual(taxonomyIndividual2);
		taxonomyIndividuals.addTaxonomyIndividual(taxonomyIndividual3);
		
		return taxonomyIndividual2;
	}
	
	/**
	 * Adds one individual as narrower to the individual.
	 * @param taxonomyIndividual The individual we want to add a narrower to.
	 * @return The narrower that has been added.
	 */
	public static TaxonomyIndividual addOneNarrowerToTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual ) {
		final TaxonomyIndividual narrower = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower);
		
		return narrower;
	}
	
	/**
	 * Creates a taxonomy with only a root individual.
	 * @return The new taxonomy.
	 */
	public static Taxonomy createTaxonomy() {
		final TaxonomyIndividual rootIndividual = createTaxonomyIndividual();
		final String title = rootIndividual.getIri().getLocalName().toString();
		final Namespace namespace = new NamespaceImpl(title);
		final Taxonomy taxonomy = new TaxonomyImpl(rootIndividual,  title,  namespace);
		return taxonomy;
	}
	
	/**
	 * Creates a collection with three taxonomies for the tests.
	 * @return The second taxonomy in the collection.
	 */
	public static Taxonomy createTaxonomyCollectionWithThreeTaxonomies(final TaxonomyCollection taxonomyCollection) {
		final Taxonomy taxonomy1 = createTaxonomy();
		final Taxonomy taxonomy2 = createTaxonomy();
		final Taxonomy taxonomy3 = createTaxonomy();
		taxonomyCollection.addTaxonomy(taxonomy1);
		taxonomyCollection.addTaxonomy(taxonomy2);
		taxonomyCollection.addTaxonomy(taxonomy3);
		
		return taxonomy2;
	}

	/**
	 * Creates a collection with three taxonomy individuals for the tests.
	 * @return The second taxonomy individual in the collection.
	 */
	public static TaxonomyIndividual createTaxonomyIndividualCollectionWithThreeIndividuals(
			TaxonomyIndividualCollection taxonomyIndividualCollection) {
		final TaxonomyIndividual taxonomyIndividual1 = createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual2 = createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual3 = createTaxonomyIndividual();
		
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual1);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual2);
		taxonomyIndividualCollection.addTaxonomyIndividual(taxonomyIndividual3);
		
		return taxonomyIndividual2;
	}

	/**
	 * Compares the values of a taxonomy individual with the values of a response object.
	 * @param taxonomyIndividual The individual that contains the value the response has to contain.
	 * @param taxonomyIndividualResponse The response object we want to check.
	 * @return True if the both objects contain the same values, false otherwise.
	 */
	public static boolean areValuesEqual(final TaxonomyIndividual taxonomyIndividual, final TaxonomyIndividualResponse taxonomyIndividualResponse) {
		final String expectedIri = taxonomyIndividual.getIri().toString();
		final String actualIri = taxonomyIndividualResponse.getIri();
		if (StringUtils.areStringsUnequal(expectedIri, actualIri)) {
			return false;
		}
		
		final String expectedPreferredLabel = taxonomyIndividual.getPreferredLabel();
		final String actualPreferredLabel = taxonomyIndividualResponse.getPreferredLabel();
		if (StringUtils.areStringsUnequal(expectedPreferredLabel, actualPreferredLabel)) {
			return false;
		}
		
		final int expectedNarrowerCount = taxonomyIndividual.getNarrowerCount();
		final int actualNarrowerCount = taxonomyIndividualResponse.getNarrowers().getNarrowersCount(); 
		if (expectedNarrowerCount != actualNarrowerCount) {
			return false;
		}
		
		int counter = 0;
		while(taxonomyIndividual.hasNextNarrower()) {
			final TaxonomyIndividual narrower = taxonomyIndividual.getNextNarrower().get();
			final TaxonomyIndividualResponse narrowerResponse = taxonomyIndividualResponse.getNarrowers().getIndividuals().get(counter);
			counter++;
			if (areValuesEqual(narrower, narrowerResponse) == false) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Creates a little taxonomy tree with response objects.
	 * @return The root individual of the tree.
	 */
	public static TaxonomyIndividualResponse createTaxonomyIndividualResponseTree() {
		final TaxonomyIndividualResponse taxonomyIndividual1 = createTaxonomyIndividualResponseWithoutNarrowers();
		final TaxonomyIndividualResponse taxonomyIndividual2 = createTaxonomyIndividualResponseWithoutNarrowers();
		addThreeNarrowersToTaxonomyIndividualResponse(taxonomyIndividual1);
		addThreeNarrowersToTaxonomyIndividualResponse(taxonomyIndividual2);
		final TaxonomyIndividualResponse taxonomyIndividual11 = taxonomyIndividual1.getNarrowers().getIndividuals().get(0);
		addThreeNarrowersToTaxonomyIndividualResponse(taxonomyIndividual11);
		
		final TaxonomyIndividualResponse taxonomyIndividual0 = createTaxonomyIndividualResponseWithoutNarrowers();
		taxonomyIndividual0.addNarrower(taxonomyIndividual1);
		taxonomyIndividual0.addNarrower(taxonomyIndividual2);
		return taxonomyIndividual0;
	}
	
	/**
	 * Creates a little taxonomy tree with TaxonomyIndividual objects.
	 * @return The root individual of the tree.
	 */
	public static TaxonomyIndividual createTaxonomyIndividualTree() {
		final TaxonomyIndividual taxonomyIndividual1 = createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual2 = createTaxonomyIndividual();
		addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual1);
		addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual2);
		if (taxonomyIndividual1.hasNextNarrower()) { 
			final TaxonomyIndividual taxonomyIndividual11 = taxonomyIndividual1.getNextNarrower().get();
			addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual11);
			taxonomyIndividual1.resetIerator();
		}
		
		final TaxonomyIndividual taxonomyIndividual0 = createTaxonomyIndividual();
		taxonomyIndividual0.addNarrower(taxonomyIndividual1);
		taxonomyIndividual0.addNarrower(taxonomyIndividual2);
		return taxonomyIndividual0;
	}
	
	/**
	 * Creates an individual in a taxonomy without any narrowers. The local name of this individual is a random uuid.  
	 * @return
	 */
	public static TaxonomyIndividualResponse createTaxonomyIndividualResponseWithoutNarrowers() {
		String iriAsString = "http://mynamespace#";
		final Uuid uuid = new Uuid();
		iriAsString = iriAsString + uuid.toString();
		final String preferredLabel = "myLabel";
		final TaxonomyIndividualResponse taxonomyIndividualResponse = new TaxonomyIndividualResponse();
		taxonomyIndividualResponse.setIri(iriAsString);
		taxonomyIndividualResponse.setPreferredLabel(preferredLabel);
		
		return taxonomyIndividualResponse;
	}
	
	/**
	 * Adds three narrowers to an individual response object.
	 * @param taxonomyIndividualResponse
	 */
	public static void addThreeNarrowersToTaxonomyIndividualResponse(final TaxonomyIndividualResponse taxonomyIndividualResponse) {
		final TaxonomyIndividualResponse narrower1 = createTaxonomyIndividualResponseWithoutNarrowers();
		final TaxonomyIndividualResponse narrower2 = createTaxonomyIndividualResponseWithoutNarrowers();
		final TaxonomyIndividualResponse narrower3 = createTaxonomyIndividualResponseWithoutNarrowers();
		
		taxonomyIndividualResponse.addNarrower(narrower1);
		taxonomyIndividualResponse.addNarrower(narrower2);
		taxonomyIndividualResponse.addNarrower(narrower3);
	}
	
	/**
	 * Creates a list with two taxonomies. These taxonomies contain a hierarchy of individuals.
	 * @return The two taxonomies. 
	 */
	public static TaxonomyListResponse createTaxonomyListResponseWithTwoTaxonomies() {
		final TaxonomyListResponse taxonomies = new TaxonomyListResponse();
		taxonomies.addTaxonomy(createTaxonomyResponse_withIndividuals());
		taxonomies.addTaxonomy(createTaxonomyResponse_withIndividuals());
		
		return taxonomies;
	}
	
	/**
	 * Creates a taxonomy with one root individual.
	 * @return The created taxonomy.
	 */
	public static TaxonomyResponse createTaxonomyResponse_withIndividuals() {
		final TaxonomyResponse taxonomy = new TaxonomyResponse();
		taxonomy.setNamespace("http://mynamespace");
		taxonomy.setTitle(new Uuid().toString());
		final TaxonomyIndividualResponse rootIndividual = createTaxonomyIndividualResponseWithoutNarrowers();
		final TaxonomyIndividualResponse individual1 = createTaxonomyIndividualResponseTree();
		final TaxonomyIndividualResponse individual2 = createTaxonomyIndividualResponseTree();
		rootIndividual.addNarrower(individual1);
		rootIndividual.addNarrower(individual2);
		taxonomy.setRootIndividual(rootIndividual);
		
		return taxonomy;
	}
	
	/**
	 * Creates a taxonomy with one root individual.
	 * @return The created taxonomy.
	 */
	public static Taxonomy createTaxonomy_withIndividuals() {
		final String title = new Uuid().toString();
		final Namespace namespace = new NamespaceImpl("http://mynamespace");
		final TaxonomyIndividual rootIndividual = createTaxonomyIndividual();
		final TaxonomyIndividual individual1 = createTaxonomyIndividualTree();
		final TaxonomyIndividual individual2 = createTaxonomyIndividualTree();
		rootIndividual.addNarrower(individual1);
		rootIndividual.addNarrower(individual2);

		final Taxonomy taxonomy = new TaxonomyImpl(rootIndividual, title, namespace);
		
		return taxonomy;
	}
	
	/**
	 * Compares the values of a taxonomy with the values of a response object.
	 * @param taxonomy The individual that contains the value the response has to contain.
	 * @param taxonomyResponse The response object we want to check.
	 * @return True if the both objects contain the same values, false otherwise.
	 */
	public static boolean areValuesEqual(final Taxonomy taxonomy, final TaxonomyResponse taxonomyResponse) {
		if (StringUtils.areStringsUnequal(taxonomy.getTitle(), taxonomyResponse.getTitle())) {
			return false;
		}
		
		if (StringUtils.areStringsUnequal(taxonomy.getNamespace().toString(), taxonomyResponse.getNamespace())) {
			return false;
		}

		if (areValuesEqual(taxonomy.getRootIndividual(), taxonomyResponse.getRootIndividual()));
		
		return true;
	}
	
	/**
	 * Compares the values of a collection with taxonomies with the values of a response object.
	 * @param taxonomyCollection The individual that contains the taxonomies the response has to contain.
	 * @param taxonomyResponses The response object we want to check.
	 * @return True if the both objects contain the same values, false otherwise.
	 */
	public static boolean areValuesEqual(final TaxonomyCollection taxonomies, final TaxonomyListResponse taxonomyResponses) {
		if (taxonomies.getTaxonomyCount() != taxonomyResponses.getTaxonomies().size()) {
			return false;
		}
		
		int counter = 0;
		while (taxonomies.hasNextTaxonomy()) {
			final Optional<Taxonomy> optionalWithTaxonomy = taxonomies.nextTaxonomy();
			final Taxonomy taxonomy = optionalWithTaxonomy.get();
			final TaxonomyResponse taxonomyResponse = taxonomyResponses.getTaxonomies().get(counter);
			counter++;
			if (!areValuesEqual(taxonomy, taxonomyResponse)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Compares the values of a collection with individuals in a taxonomy with the values of a response object.
	 * @param taxonomyIndividualCollection The collection that contains the individuals the response has to contain.
	 * @param taxonomyIndividualListResponses The response object we want to check.
	 * @return True if the both objects contain the same values, false otherwise.
	 */
	public static boolean areValuesEqual(final TaxonomyIndividualCollection taxonomyIndividuals, final TaxonomyIndividualListResponse taxonomyIndividualsResponses) {
		if (taxonomyIndividuals.getTaxonomyIndividualCount() != taxonomyIndividualsResponses.getIndividuals().size()) {
			return false;
		}
		
		for (int i = 0; i < taxonomyIndividuals.getSize(); i++) {
			final TaxonomyIndividual taxonomyIndividual = taxonomyIndividuals.getTaxonomyIndividual(i).get();
			final TaxonomyIndividualResponse taxonomyIndividualResponse = taxonomyIndividualsResponses.getIndividuals().get(i);
			final boolean valuesEqual = areValuesEqual(taxonomyIndividual, taxonomyIndividualResponse);
			if (!valuesEqual) {
				return false;
			}
		}
		
		return true;
	}
}
	
