package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

public interface MediatorConfigurationData {

	String getExtractorToolNameAt(int index);

	boolean getExtractorToolActivatedAt(int index);

	boolean getFitsToolAt(int index);

	int getExtractorToolsCount();

}
