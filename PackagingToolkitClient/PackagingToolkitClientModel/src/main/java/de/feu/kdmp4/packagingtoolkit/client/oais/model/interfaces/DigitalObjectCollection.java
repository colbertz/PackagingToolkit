package de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces;

import java.util.List;

public interface DigitalObjectCollection {
	public void addDigitalObject(DigitalObject digitalObject);
	int getDigitalObjectCount();
	DigitalObject getDigitalObjectAt(int index);
	void deleteDigitalObject(DigitalObject digitalObject);
	boolean isEmpty();
	boolean isNotEmpty();
	List<DigitalObject> asList();
}
