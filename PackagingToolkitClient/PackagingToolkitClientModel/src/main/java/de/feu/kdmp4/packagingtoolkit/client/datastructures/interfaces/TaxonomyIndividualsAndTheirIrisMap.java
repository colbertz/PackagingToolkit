package de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;

/**
 * Contains taxonomy individuals. Their keys are their iris.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomyIndividualsAndTheirIrisMap {
	/**
	 * Adds an individual to the map. 
	 * @param taxonomyIndividual The individual to add to the map. Its iri is used as its key.
	 */
	void addIndividual(TaxonomyIndividual taxonomyIndividual);
	/**
	 * Gets an individual from the map with the help of the uuid.
	 * @param iriOfIndividual The iri of the individual we are looking for.
	 * @return The individual with the iri iriOfIndividual.
	 */
	TaxonomyIndividual getIndividual(Iri iriOfIndividual);

}
