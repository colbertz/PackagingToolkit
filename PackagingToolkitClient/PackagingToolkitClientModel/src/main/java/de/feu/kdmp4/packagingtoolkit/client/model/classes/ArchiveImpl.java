package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;

/**
 * Represents a zip archive on the server's file system. It contains all
 * information needed by the user to browse through the archives on
 * client side and to choose the archives he wants to delete or to edit.
 * @author Christopher Olbertz
 *
 */
public class ArchiveImpl implements Archive {
	/**
	 * The archiveId is the uuid as String.
	 */
	private long archiveId;
	/**
	 * The type of the package. 
	 */
	private PackageType packageType;
	/**
	 * The serialization format of the package.
	 */
	private SerializationFormat serializationFormat;
	/**
	 * The title of the package. 
	 */
	private String title;
	/**
	 * The uuid for identifying the archive on the server. 
	 */
	private Uuid uuid;
	/**
	 * Contains the digital objects of this information package.
	 */
	private DigitalObjectCollection digitalObjects;

	private LocalDateTime creationDate;
	private LocalDateTime lastModificationDate;
	private boolean virtual;
	
	public ArchiveImpl(final long archiveId, final PackageType packageType, final String title, 
			final LocalDateTime lastModificationDate, final LocalDateTime creationDate, final Uuid uuid, 
			final SerializationFormat serializationFormat) {
		this.archiveId = archiveId;
		this.packageType = packageType;
		this.title = title;
		this.uuid = uuid;
		this.creationDate = creationDate;
		this.lastModificationDate = lastModificationDate;
		this.serializationFormat = serializationFormat;
	}
	
	/**
	 * Checks if a given uuid is equal to the uuid of this archive.
	 * @param uuid The uuid that should be checked.
	 * @return True, if uuid is equal to the uuid of this archive, false
	 * otherwise.
	 */
	@Override
	public boolean areUuidsEqual(final Uuid uuid) {
		final int compareTo = this.uuid.compareTo(uuid); 
		if (compareTo == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int compareTo(final Archive archive) {
		final String myUuid = this.uuid.toString();
		final String theOtherUud = archive.getUuid().toString();
		return myUuid.compareTo(theOtherUud);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArchiveImpl other = (ArchiveImpl) obj;
		if (packageType != other.packageType)
			return false;
		if (serializationFormat != other.serializationFormat)
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((packageType == null) ? 0 : packageType.hashCode());
		result = prime * result + ((serializationFormat == null) ? 0 : serializationFormat.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}
	
	@Override
	public ArchiveResponse toResponse() {
		final UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(uuid);
		return ResponseModelFactory.getArchiveResponse(archiveId, packageType, 
				serializationFormat, title, uuidResponse, creationDate, lastModificationDate);
	}
	
	@Override
	public PackageType getPackageType() {
		return packageType;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public Uuid getUuid() {
		return uuid;
	}

	@Override
	public SerializationFormat getSerializationFormat() {
		return serializationFormat;
	}

	@Override
	public long getArchiveId() {
		return archiveId;
	}
	
	@Override
	public void setUuid(final Uuid uuid) {
		this.uuid = uuid;
	}

	@Override
	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	@Override
	public LocalDateTime getLastModificationDate() {
		return lastModificationDate;
	}

	@Override
	public void setSerializationFormat(String serializationFormat) {
		this.serializationFormat = SerializationFormat.getSerializationFormat(serializationFormat);
	}
	
	@Override
	public boolean isVirtual() {
		return virtual;
	}
	
	@Override
	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}
	
	@Override 
	public DigitalObjectCollection getDigitalObjects() {
		return digitalObjects;
	}
	
	@Override
	public void setDigitalObjects(DigitalObjectCollection digitalObjects) {
		this.digitalObjects = digitalObjects;
	}
}
