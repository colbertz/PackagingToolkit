package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;

public class ClientConfigurationDataImpl implements ClientConfigurationData {
	/**
	 * The url the server is reachable at.
	 */
	private String serverUrl;
	/**
	 * The url the mediator is reachable at.
	 */
	private String mediatorUrl;

	/**
	 * Construct a new object with the configuration of the client.
	 * @param serverUrl The url the server is reachable at.
	 * @param mediatorUrl The url the mediator is reachable at.
	 */
	public ClientConfigurationDataImpl(final String serverUrl, final String mediatorUrl) {
		super();
		this.serverUrl = serverUrl;
		this.mediatorUrl = mediatorUrl;
	}

	@Override
	public String getServerUrl() {
		return serverUrl;
	}
	
	@Override
	public String getMediatorUrl() {
		return mediatorUrl;
	}
}
