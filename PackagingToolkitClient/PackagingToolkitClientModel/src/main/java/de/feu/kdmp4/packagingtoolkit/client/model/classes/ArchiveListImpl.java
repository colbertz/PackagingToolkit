package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Encapsulates a list with archives.
 * @author Christopher Olbertz
 *
 */
public class ArchiveListImpl extends AbstractList implements ArchiveList { 
	
	/**
	 * Is needed because of PrimeFaces.
	 */
	public ArchiveListImpl() {
	}

	/**
	 * Adds an archive to the list. With the help of the uuid is checked if
	 * the archive is already in the list.
	 * @param archive The archive that should be added to the list.
	 * @throws ArchiveMayNotBeNullException Is thrown if the archive is null.
	 * @throws UuidAlreadyInArchiveListException Is thrown if an archive
	 * with the same uuid is already in the archive.
	 */
	@Override
	public void addArchiveToList(final Archive archive) {
		checkUuidInArchiveList(archive.getUuid());
		super.add(archive);
	}
	
	/**
	 * Gets an archive at a given index.
	 * @param index The index of the archive that should be determined.
	 * @return The found archive.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	@Override
	public Archive getArchiveAt(final int index) {
		Archive archive = (Archive)super.getElement(index);
		return archive;
	}
	
	/**
	 * Counts the archives in the list.
	 * @return The number of the archives in the list.
	 */
	@Override
	public int getArchiveCount() {
		return super.getSize();
	}
	
	/**
	 * Removes an archive from the list. 
	 * @param archive The archive that should be removed.
	 * @throws ArchiveMayNotBeNullException Is thrown if the archive is null.
	 */
	@Override
	public void removeArchive(final Archive archive) {
			super.remove(archive);
	}
	
	/**
	 * Checks, if an information package with a given uuid is
	 * already in the list. Throws an UuidAlreadyInArchiveListException
	 * when the uuid is found.
	 * @param uuid The uuid to look for.
	 */
	private void checkUuidInArchiveList(final Uuid uuid) {
		for (int i = 0; i < getArchiveCount(); i++) {
			ArchiveImpl archive;
			//try {
				archive = (ArchiveImpl)super.getElement(i);
				if (archive.areUuidsEqual(uuid)) {
					/*throw new UuidAlreadyInArchiveListException(uuid, 
								this.getClass().getName(), 
								METHOD_CHECK_UUID_IN_ARCHIVE_LIST, 
								PackagingToolkitComponent.SERVER);*/
				}
			/*} catch (IndexNotInRangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}
}
