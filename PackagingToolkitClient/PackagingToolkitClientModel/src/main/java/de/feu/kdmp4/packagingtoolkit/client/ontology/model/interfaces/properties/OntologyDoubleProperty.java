package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;


public interface OntologyDoubleProperty extends OntologyDatatypeProperty {

	void setPropertyValue(Double value);
	Double getPropertyValue();
}
