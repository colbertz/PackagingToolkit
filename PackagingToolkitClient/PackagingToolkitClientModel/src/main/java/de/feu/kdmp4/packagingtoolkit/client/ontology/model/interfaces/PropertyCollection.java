package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;

/**
 * A list with properties.
 * @author Christopher Olbertz
 *
 */
public interface PropertyCollection {
	/**
	 * Gets a property at a given index of the list.
	 * @param index The index.
	 * @return The property found at the index.
	 */
	OntologyProperty getProperty(int index);
	/**
	 * Adds a property to the list.
	 * @param property The property that should be appended to the list.
	 */
	void addProperty(OntologyProperty property);
	int getPropertiesCount();
	boolean isEmpty();
}
