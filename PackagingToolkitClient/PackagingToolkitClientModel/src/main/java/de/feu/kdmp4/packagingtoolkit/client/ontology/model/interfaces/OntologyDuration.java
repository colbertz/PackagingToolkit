package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

public interface OntologyDuration {
	int getDays();
	int getHours();
	int getMinutes();
	int getSeconds();
	int getYears();
	boolean isNegative();
	int getMonths();
	void setDays(int days);
	void setHours(int hours);
	void setMinutes(int minutes);
	void setMonths(int months);
	void setNegative(boolean negative);
	void setSeconds(int seconds);
	void setYears(int years);
}
