package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import java.time.LocalDate;

public interface OntologyDateProperty extends OntologyDatatypeProperty {
	public LocalDate getPropertyValue();

	void setPropertyValue(LocalDate value);
}
