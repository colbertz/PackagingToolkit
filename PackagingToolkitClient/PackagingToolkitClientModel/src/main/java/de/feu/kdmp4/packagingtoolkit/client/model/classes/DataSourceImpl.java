package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceToolCollection;

public class DataSourceImpl implements DataSource {
	private String dataSourceName;
	private DataSourceToolCollection dataSourceTools;
	
	public DataSourceImpl(final String dataSourceName, final DataSourceToolCollection dataSourceTools) {
		this.dataSourceName = dataSourceName;
		this.dataSourceTools = dataSourceTools;
	}
	
	@Override
	public String getDataSourceName() {
		return dataSourceName;
	}
	
	@Override
	public void addDataSourceTool(final DataSourceTool dataSourceTool) {
		dataSourceTools.addDataSourceToolToList(dataSourceTool);
	}

	@Override
	public DataSourceTool getDataSourceToolAt(final int index) {
		return dataSourceTools.getDataSourceToolAt(index);
	}

	@Override
	public int getDataSourceToolCount() {
		return dataSourceTools.getDataSourceToolCount();
	}
	
	@Override
	public DataSourceToolCollection getDataSourceTools() {
		return dataSourceTools;
	}
}
