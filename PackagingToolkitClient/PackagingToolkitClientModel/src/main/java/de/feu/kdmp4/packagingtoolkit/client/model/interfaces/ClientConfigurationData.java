package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

/**
 * This interface represents the configuration data that has to be entered by the user
 * like the url of the server and the mediator.
 * @author Christopher Olbertz
 *
 */
public interface ClientConfigurationData {
	/**
	 * Gets the url of the server read from the configuration.
	 * @return The url of the server.
	 */
	public String getServerUrl();
	/**
	 * Gets the url of the mediator read from the configuration.
	 * @return The url of the mediator.
	 */
	public String getMediatorUrl();
}
