package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import java.math.BigInteger;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyLongPropertyImpl;

public interface OntologyLongProperty extends OntologyDatatypeProperty {
	void addMaxInclusiveRestriction(BigInteger value);
	void addMinInclusiveRestriction(BigInteger value);
	public BigInteger getPropertyValue();
	
	public static OntologyLongProperty createUnsigned(String propertyId, String label, String range) {
		return new OntologyLongPropertyImpl(propertyId, true, label, range); 
	}
	
	public static OntologyLongProperty createSigned(String propertyId, String label, String range) {
		return new OntologyLongPropertyImpl(propertyId, false, label, range); 
	}
	void setPropertyValue(BigInteger value);
	boolean isUnsigned();
}
