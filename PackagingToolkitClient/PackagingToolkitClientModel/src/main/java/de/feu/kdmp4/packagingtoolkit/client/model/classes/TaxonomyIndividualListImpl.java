package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.util.Optional;
import java.util.function.Consumer;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.utils.HtmlUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class TaxonomyIndividualListImpl extends AbstractList implements TaxonomyIndividualCollection {
	/**
	 * The iri of the taxonomy the individuals belong to if this information is important.
	 */
	private Iri iriOfTaxonomy;
	
	@Override
	public void addTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual) {
		super.add(taxonomyIndividual);
	}
	
	@Override
	public Optional<TaxonomyIndividual> getTaxonomyIndividual(final int index) {
		TaxonomyIndividual taxonomyIndividual = (TaxonomyIndividual)super.getElement(index);
		return ClientOptionalFactory.createOptionalWithTaxonomyIndividual(taxonomyIndividual);
	}
	
	@Override
	public int getTaxonomyIndividualCount() {
		return super.getSize();
	}
	
	@Override
	public void  doWithEveryIndividual(Consumer<TaxonomyIndividual> consumer) {
		for (int i = 0; i < getSize(); i++) {
			final TaxonomyIndividual taxonomyIndividual = (TaxonomyIndividual)getElement(i);
			consumer.accept(taxonomyIndividual);
		}
	}

	@Override
	public void addTaxonomyIndividuals(final TaxonomyIndividualCollection taxonomyIndividuals) {
		super.addCollection(taxonomyIndividuals);
	}
	
	@Override
	public Iri getIriOfTaxonomy() {
		return iriOfTaxonomy;
	}
	
	@Override
	public void setIriOfTaxonomy(final Iri iriOfTaxonomy) {
		this.iriOfTaxonomy = iriOfTaxonomy;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((iriOfTaxonomy == null) ? 0 : iriOfTaxonomy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxonomyIndividualListImpl other = (TaxonomyIndividualListImpl) obj;
		if (iriOfTaxonomy == null) {
			if (other.iriOfTaxonomy != null)
				return false;
		} else if (!iriOfTaxonomy.equals(other.iriOfTaxonomy))
			return false;
		return true;
	}

	@Override
	public String getAsHtmlUnorderedList() {
		if (isNotEmpty()) {
			final StringList taxonomyIndividualsAsStrings = PackagingToolkitModelFactory.getStringList();
			for (int i = 0; i < getSize(); i++) {
				final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = getTaxonomyIndividual(i);
				optionalWithTaxonomyIndividual.ifPresent(taxonomyIndividual -> 
					taxonomyIndividualsAsStrings.addStringToList(taxonomyIndividual.getPreferredLabel()));
			}
			return HtmlUtils.createHtmlUnorderedList(taxonomyIndividualsAsStrings);
		}
		
		return StringUtils.EMPTY_STRING;
	}
}
