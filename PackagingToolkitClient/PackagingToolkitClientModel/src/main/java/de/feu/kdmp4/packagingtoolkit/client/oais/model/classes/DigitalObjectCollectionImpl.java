package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import java.util.ArrayList;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.SortableAbstractList;
 
public class DigitalObjectCollectionImpl extends SortableAbstractList<DigitalObject>
								   implements DigitalObjectCollection {
	
	@Override
	public void addDigitalObject(DigitalObject digitalObject) {
		//try {
			super.add(digitalObject);
		/*} catch (ListElementMayNotBeNullException e) {
			throw new DigitalObjectMayNotBeNullException(this.getClass().getName(), 
					METHOD_ADD_DIGITAL_OBJECT, PackagingToolkitComponent.CLIENT);
		}*/
	}
	
	@Override
	public void deleteDigitalObject(DigitalObject digitalObject) {
		//try {
			super.remove(digitalObject);
		/*} catch (ListElementMayNotBeNullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	@Override
	public boolean isEmpty() {
		if (getDigitalObjectCount() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public boolean isNotEmpty() {
		return !isEmpty();
	}
	
	@Override
	public DigitalObject getDigitalObjectAt(int index) {
		return super.getElement(index);
	}
	
	@Override
	public int getDigitalObjectCount() {
		return super.getSize();
	}
	
	@Override
	public List<DigitalObject> asList() {
		final List<DigitalObject> digitalObjectList = new ArrayList<>();
		
		for (int i = 0; i < getSize(); i++) {
			final DigitalObject digitalObject = getDigitalObjectAt(i);
			digitalObjectList.add(digitalObject);
		}
		
		return digitalObjectList;
	}
}
