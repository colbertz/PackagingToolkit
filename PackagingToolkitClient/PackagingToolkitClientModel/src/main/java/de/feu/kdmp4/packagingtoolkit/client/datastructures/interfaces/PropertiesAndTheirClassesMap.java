package de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;

/**
 * Contains the iris of properties as keys and the classes that contain this properties as values.
 * @author Christopher Olbertz
 *
 */
public interface PropertiesAndTheirClassesMap {
	/**
	 * Adds a class to a datatype 
	 * @param propertyIri The iri of the property the class is assigned to.
	 * @param ontologyClass The class we want to add to the map.
	 */
	void addClassToProperty(final String propertyIri, final OntologyClass ontologyClass);
	/**
	 * Adds a class to a datatype 
	 * @param property The property the class is assigned to.
	 * @param ontologyClass The class we want to add to the map.
	 */
	void addClassToProperty(final OntologyProperty property, final OntologyClass ontologyClass);
	/**
	 * Returns the classes that are assigned to a certain property.
	 * @param propertyIri The iri of the property whose classes we want to find.
	 * @return The classes that are assigned to a the property.
	 */
	Optional<OntologyClassList> getOntologyClassesOfProperty(final String propertyIri);
	/**
	 * Returns the classes that are assigned to a certain property.
	 * @param property The property whose classes we want to find.
	 * @return The classes that are assigned to a the property.
	 */
	Optional<OntologyClassList> getOntologyClassesOfProperty(final OntologyProperty property);
}
