package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class OntologyIndividualImpl implements OntologyIndividual {
	/**
	 * 
	 */
	private static final long serialVersionUID = -457859572134184882L;
	// *********** Attributes ***********
	transient private OntologyClass ontologyClass;
	transient private DatatypePropertyCollection datatypeProperties;
	transient private ObjectPropertyCollection objectProperties;
	private Uuid uuid;
	@Autowired
	transient private OntologyModelFactory ontologyModelFactory;
	private Iri iriOfIndividual;
	
	// ********** Constructors *********
	public OntologyIndividualImpl() {
		this.uuid = new Uuid();
		initialize();
	}
	  
	public OntologyIndividualImpl(OntologyClass ontologyClass) {
		initialize();
		this.ontologyClass = ontologyClass;		
		this.uuid = new Uuid();
	}
	
	public OntologyIndividualImpl(final Iri iriOfIndividual) {
		this.uuid = new Uuid();
		this.iriOfIndividual = iriOfIndividual;
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		this.uuid = new Uuid();
		datatypeProperties = new DatatypePropertyCollectionImpl();
		objectProperties = new ObjectPropertyCollectionImpl();
	}
	
	@Override
	public Optional<OntologyDatatypeProperty> findDatatypePropertyByLabel(final String label) {
		boolean propertyFound = false;
		int counter = 0;
		
		while (!propertyFound && counter < datatypeProperties.getPropertiesCount()) {
			final OntologyDatatypeProperty datatypeProperty = datatypeProperties.getDatatypeProperty(counter);
			final String labelOfCurrentProperty = datatypeProperty.getLabel();
			if (StringUtils.areStringsEqual(label, labelOfCurrentProperty)) {
				propertyFound = true;
			} else  {
				counter++;
			}
		}
		
		if (propertyFound == true) {
			final OntologyDatatypeProperty foundProperty = datatypeProperties.getDatatypeProperty(counter);
			return ClientOptionalFactory.createDatatypePropertyOptional(foundProperty);
		} else {
			return ClientOptionalFactory.createEmptyDatatypePropertyOptional();
		}
	}
	
	@Override
	public Optional<OntologyObjectProperty> findObjectPropertyByLabel(final String label) {
		boolean propertyFound = false;
		int counter = 0;
		
		while (!propertyFound && counter < objectProperties.getPropertiesCount()) {
			final OntologyObjectProperty objectProperty = objectProperties.getObjectProperty(counter);
			final String labelOfCurrentProperty = objectProperty.getLabel();
			if (StringUtils.areStringsEqual(label, labelOfCurrentProperty)) {
				propertyFound = true;
			} else  {
				counter++;
			}
		}
		
		if (propertyFound == true) {
			final OntologyObjectProperty foundProperty = objectProperties.getObjectProperty(counter);
			return ClientOptionalFactory.createObjectPropertyOptional(foundProperty);
		} else {
			return ClientOptionalFactory.createEmptyObjectPropertyOptional();
		}
	}
	
	public OntologyIndividualImpl(OntologyClass ontologyClass, DatatypePropertyCollection propertyList, Uuid uuid) {
		initialize();
		if (propertyList == null) {
			datatypeProperties = new DatatypePropertyCollectionImpl();
		}
		datatypeProperties = propertyList;
		this.ontologyClass = ontologyClass;
		this.uuid = uuid;
	}
	
	public OntologyIndividualImpl(OntologyClass ontologyClass, DatatypePropertyCollection propertyList, ObjectPropertyCollection objectPropertyList) {
		initialize();
		if (propertyList == null) {
			datatypeProperties = new DatatypePropertyCollectionImpl();
		}
		datatypeProperties = propertyList;
		this.ontologyClass = ontologyClass;
		this.objectProperties = objectPropertyList;
		this.uuid = new Uuid();
	}
	
	// ******** Public methods *********
	@Override
	public OntologyIndividual clone() throws CloneNotSupportedException {
		return ontologyModelFactory.createOntologyIndividual(getOntologyClass());
	}
	
	@Override
	public void addDatatypeProperty(OntologyDatatypeProperty ontologyProperty) {
		if (datatypeProperties.containsDatatypeProperty(ontologyProperty)) {
			datatypeProperties.replaceValueOfDatatypeProperty(ontologyProperty);
		} else {
			datatypeProperties.addDatatypeProperty(ontologyProperty);
		}
	}
	
	@Override 
	public Optional<OntologyDatatypeProperty> getDatatypeProperty(final String propertyName) {
		OntologyDatatypeProperty ontologyProperty = null;
		int i = 0;
		boolean found = false;
		
		while (found == false && i < datatypeProperties.getPropertiesCount()) {
			ontologyProperty = datatypeProperties.getDatatypeProperty(i);
			
			if (ontologyProperty.getPropertyId().equals(propertyName)) {
				found = true;
			}
			i++;
		}
		
		if (found) {
			return ClientOptionalFactory.createDatatypePropertyOptional(ontologyProperty);
		} else {
			return ClientOptionalFactory.createEmptyDatatypePropertyOptional();
		}
	}
	
	@Override
	public void addObjectProperty(OntologyObjectProperty ontologyProperty) {
		if (objectProperties.containsObjectProperty(ontologyProperty)) {
			objectProperties.removeObjectProperty(ontologyProperty);
		}
		objectProperties.addObjectProperty(ontologyProperty);
	}
	
	@Override
	public OntologyDatatypeProperty getDatatypePropertyAt(int index) {
		return datatypeProperties.getDatatypeProperty(index);
	}
	
	@Override
	public int getDatatypePropertiesCount() {
		return datatypeProperties.getPropertiesCount();
	}
	
	@Override
	public OntologyObjectProperty getObjectPropertyAt(int index) {
		return objectProperties.getObjectProperty(index);
	}

	@Override
	public int getObjectPropertiesCount() {
		return objectProperties.getPropertiesCount();
	}
	
	@Override
	public OntologyClass getOntologyClass() {
		return ontologyClass;
	}
	
	@Override
	public Uuid getUuid() {
		return uuid;
	}
	
	@Override
	public Iri getIriOfIndividual() {
		return iriOfIndividual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyIndividualImpl other = (OntologyIndividualImpl) obj;
		if (uuid == null) {
			if (other.uuid != null) {
				return false;
			}
		} else if (!uuid.equals(other.uuid)) {
			if (other.iriOfIndividual.equals(this.iriOfIndividual)) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	public String getNamespace() {
		return ontologyClass.getNamespace();
	}

	@Override
	public void setUuid(Uuid uuid) {
		this.uuid = uuid;
	}
	
	@Override
	public void setObjectProperties(ObjectPropertyCollection objectProperties) {
		this.objectProperties = objectProperties;
	}
	
	@Override
	public void setIriOfIndividual(Iri iriOfIndividual) {
		this.iriOfIndividual = iriOfIndividual;
	}
	
	@Override
	public String toString() {
		return iriOfIndividual.toString();
	}
}
