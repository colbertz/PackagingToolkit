package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;


public interface MetadataExtractorList {
	/**
	 * Determines the metadata extractor at a given index.
	 * @param index The index of the metadata extractor in 
	 * the list.
	 * @return The found metadata extractor.
	 */
	//MetadataExtractor getMetadataExtractor(int index);
	/**
	 * Adds a new metadata extractor to the list.
	 * @param metadataExtractor The metadata extractor to be
	 * added to the list.
	 */
	//void addMetadataExtractor(MetadataExtractor metadataExtractor);
	/**
	 * Counts the metadata extractors in the list.
	 * @return The number of metadata extractors in the list.
	 */
	int getMetadataExtractorCount();
}
