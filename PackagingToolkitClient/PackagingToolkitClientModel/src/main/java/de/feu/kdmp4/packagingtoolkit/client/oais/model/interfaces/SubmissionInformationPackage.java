package de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces;


/**
 * A submission information package. If it is not virtual it can contain only
 * one digital object.
 * @author Christopher Olbertz
 *
 */
public interface SubmissionInformationPackage extends BasicInformationPackage {
	/**
	 * Removes the digital object from the SIP.
	 * @return The removed digital Object.
	 */
	public DigitalObject removeDigitalObject();
	/**
	 * Sets a digital object in die archival information package. Because there is only one 
	 * digital object allowed in an AIP, an exception is thrown if there is already a 
	 * digital object in this AIP.
	 * @param digitalObject The digital object to set.
	 * @throws DigitalObjectException Is thrown if the digital object is null or if
	 * there is already a digital object in the SIP.
	 */
	public void addDigitalObject(DigitalObject digitalObject);
}
