package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;


public interface OntologyFloatProperty extends OntologyDatatypeProperty {
	void addMaxInclusiveRestriction(final float value);
	void addMinInclusiveRestriction(final float value);
	Float getPropertyValue();
	void setPropertyValue(Float value);
}
