package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import java.time.LocalTime;
import java.util.Date;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyTimeProperty;

public class OntologyTimePropertyImpl extends OntologyDatatypePropertyImpl 
									  implements OntologyTimeProperty {

	// ******* Constants *********
	private static final long serialVersionUID = 1748033123603000410L;

	// ******* Constructors *******
	public OntologyTimePropertyImpl() {
	}
	
	/**
	 * Gets the label and the value of this property.
	 * 
	 * @param label
	 *            The label.
	 * @param value
	 *            The value.
	 */
	public OntologyTimePropertyImpl(String propertyId, LocalTime value, String defaultLabel, String range) {
		super(propertyId, value, defaultLabel, range);
	}

	public OntologyTimePropertyImpl(String propertyId, String label, String range) {
		super(propertyId, null, label, range);
	}
	
	public OntologyTimePropertyImpl(String propertyId, String range) {
		super(propertyId, null, null, range);
	}
	
	@Override
	public void setPropertyValue(LocalTime value) {
		super.setValue(value);
	}
	
	@Override
	public void setPropertyValue(Date value) {
		LocalTime inputAsLocalTime = null;
		Date date = (Date)value;
		inputAsLocalTime = LocalTime.of(date.getHours(), date.getMinutes(), date.getSeconds());
		super.setValue(inputAsLocalTime);
	}
	
	// ******** Getters and setters ********
	@Override
	public LocalTime getPropertyValue() {
		return (LocalTime) getValue();
	}

	@Override
	public boolean isTimeProperty() {
		return true;
	}
}
