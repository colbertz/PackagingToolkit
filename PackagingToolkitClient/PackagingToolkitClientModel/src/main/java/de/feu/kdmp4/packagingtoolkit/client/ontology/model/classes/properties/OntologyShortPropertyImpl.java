package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;


import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.PropertyRangeException;

public class OntologyShortPropertyImpl extends OntologyDatatypePropertyImpl 
									   implements OntologyShortProperty {
	// ********* Constants **********
	private static final int DEFAULT_VALUE = 0;
	private static final long serialVersionUID = 106001928244230493L;
	private static final int SIGNED_MAX = Short.MAX_VALUE;
	private static final int SIGNED_MIN = Short.MIN_VALUE;
	private static final int UNSIGNED_MAX = 65535;
	private static final int UNSIGNED_MIN = 0;
	
	// ********* Attributes ***********
	private boolean unsigned;

	// ********* Constructors *********
	public OntologyShortPropertyImpl() {
	}
	
	@Override
	public boolean isUnsigned() {
		return unsigned;
	}
	
	@Override
	public void setPropertyValue(int value) {
		super.setValue(value);
	}
	
	public OntologyShortPropertyImpl(String propertyId, boolean unsigned, 
			String defaultLabel, String range) {
		super(propertyId, DEFAULT_VALUE, defaultLabel, range);
		this.unsigned = unsigned;
	}

	@PostConstruct
	public void initialize() {
		if (unsigned) {
			addMaxInclusiveRestriction(UNSIGNED_MAX);
			addMinInclusiveRestriction(UNSIGNED_MIN);
		} else {
			addMaxInclusiveRestriction(SIGNED_MAX);
			addMinInclusiveRestriction(SIGNED_MIN);
		}		
	}
	
	// ********* Public methods ******
	@Override
	public void addMaxInclusiveRestriction(int value) {
		if (!checkShortRange(value)) {
			PropertyRangeException exception = PropertyRangeException.
					valueNotInShortRange(value);
			throw exception;
		}
		RestrictionValue<Integer> restrictionValue = new RestrictionValue<>(value);
		MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}
	
	@Override
	public void addMinInclusiveRestriction(int  value) {
		if (!checkShortRange(value)) {
			PropertyRangeException exception = PropertyRangeException.
					valueNotInShortRange(value);
			throw exception;
		}
		RestrictionValue<Integer> restrictionValue = new RestrictionValue<>(value);
		MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	// ******* Private methods *********
	private boolean checkShortRange(int value) {
		if (unsigned) {
			if (value >= UNSIGNED_MIN && value <= UNSIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		} else {
			if (value >= SIGNED_MIN && value <= SIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	// ******* Getters and setters *******
	@Override
	public Integer getPropertyValue() {
		return (Integer)getValue();
	}

	@Override
	public boolean isShortProperty() {
		return true;
	}
}
