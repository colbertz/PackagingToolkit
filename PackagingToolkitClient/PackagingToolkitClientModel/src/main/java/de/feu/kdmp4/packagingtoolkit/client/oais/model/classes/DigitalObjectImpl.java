package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.client.exceptions.informationpackages.FilenameMayNotBeEmptyException;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataMap;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class DigitalObjectImpl implements DigitalObject {
	/**
	 * The name of the digital object.
	 */
	private String filename;
	private Uuid uuidOfInformationPackage;
	private Uuid uuidOfDigitalObject;
	private MetadataMap metadataMap;
	
	private static final String CONSTRUCTOR = "DigitalObjectImpl(String filename)";
	
	public DigitalObjectImpl(String filename, Uuid uuidOfInformationPackage, Uuid uuidOfDigitalObject) {
		if (!StringValidator.isNotNullOrEmpty(filename)) {
			throw new FilenameMayNotBeEmptyException(this.getClass().getName(), 
													 CONSTRUCTOR, PackagingToolkitComponent.CLIENT);
		}
		this.uuidOfInformationPackage = uuidOfInformationPackage;
		this.filename = filename;
		if (uuidOfDigitalObject == null) {
			this.uuidOfDigitalObject = new Uuid();
		} else {
			this.uuidOfDigitalObject = uuidOfDigitalObject;
		}
		metadataMap = ClientModelFactory.createEmptyMetadataMap();
	}
	
	public DigitalObjectImpl(String filename, Uuid uuidOfInformationPackage) {
		this(filename, uuidOfInformationPackage, null);
	}
	
	public DigitalObjectImpl(String filename) {
		this(filename, null);
	}
	
	@Override
	public void addMetadata(String key, String value) {
		metadataMap.addMetadata(key, value);
	}
	
	@Override
	public int getMetadataCount() {
		return metadataMap.getMetadataCount();
	}
	
	@Override
	public String getAbsolutePath() {
		return filename;
	}
	
	@Override
	public String getFilename() {
		return PackagingToolkitFileUtils.extractFileName(filename);
	}
	
	@Override
	public StringList getKeysOfMetadata() {
		return metadataMap.getListWithKeys();
	}
	
	@Override
	public String getMetadataValue(String key) {
		return metadataMap.getMetadataValue(key);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuidOfDigitalObject == null) ? 0 : uuidOfDigitalObject.hashCode());
		result = prime * result + ((uuidOfInformationPackage == null) ? 0 : uuidOfInformationPackage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitalObjectImpl other = (DigitalObjectImpl) obj;
		if (uuidOfDigitalObject == null) {
			if (other.uuidOfDigitalObject != null)
				return false;
		} else if (!uuidOfDigitalObject.equals(other.uuidOfDigitalObject))
			return false;
		if (uuidOfInformationPackage == null) {
			if (other.uuidOfInformationPackage != null)
				return false;
		} else if (!uuidOfInformationPackage.equals(other.uuidOfInformationPackage))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(DigitalObject digitalObject) {
		String anotherFilename = digitalObject.getAbsolutePath();
		return this.filename.compareTo(anotherFilename);
	}
	
	@Override
	public Uuid getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}
	
	@Override
	public void setUuidOfInformationPackage(Uuid uuidOfInformationPackage) {
		this.uuidOfInformationPackage = uuidOfInformationPackage;
	}
	
	@Override
	public Uuid getUuidOfDigitalObject() {
		return uuidOfDigitalObject;
	}
	
	@Override
	public MetadataMap getMetadataMap() {
		return metadataMap;
	}
}
