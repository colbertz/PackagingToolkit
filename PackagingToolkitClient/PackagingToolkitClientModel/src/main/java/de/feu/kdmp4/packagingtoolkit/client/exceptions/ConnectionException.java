package de.feu.kdmp4.packagingtoolkit.client.exceptions;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;

/**
 * Is thrown if the client was not possible to connect with the server or the mediator.
 * @author Christopher Olbertz
 *
 */
public class ConnectionException extends UserException {
	private static final long serialVersionUID = -4847239875394633301L;

	private ConnectionException(String message) {
		super(message);
	}
	
	public static ConnectionException createMediatorConnectionError(String mediatorUrl) {
		String message = I18nExceptionUtil.getMediatorConnectionNotPossibleString(mediatorUrl);
		return new ConnectionException(message);
	}
	
	public static ConnectionException createServerConnectionError(String serverUrl) {
		String message = I18nExceptionUtil.getServerConnectionNotPossibleString(serverUrl);
		return new ConnectionException(message);
	}

	@Override
	public String getSummary() {
		return I18nExceptionUtil.getSummaryConnectionExceptionString();
	}
}
