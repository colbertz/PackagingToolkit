/**
 * Contains interfaces for the data structures that are used in the client. These data structures
 * encapsulate maps, lists and so on. 
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces;