package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

public interface ViewList {
	void addViewToList(View view);
	View getViewAt(int index);
	int getViewCount();
	void removeView(View view);
	View getViewByName(String viewName);
}
