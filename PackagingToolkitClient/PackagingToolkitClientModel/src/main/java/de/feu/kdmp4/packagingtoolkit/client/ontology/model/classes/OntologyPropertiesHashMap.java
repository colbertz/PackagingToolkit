package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;

/**
 * Contains the properties that have been read from the ontology. The map is needed 
 * because we need to find the properties with the help of their local name 
 * while processing the data the user has entered in the gui and creating an
 * information package.
 * @author Christopher Olbertz
 *
 */
public class OntologyPropertiesHashMap implements OntologyPropertiesMap {
	private Map<String, OntologyProperty> propertiesMap;
	
	public OntologyPropertiesHashMap() {
		propertiesMap = new HashMap<>();
	}
	
	@Override
	public void addProperty(OntologyProperty ontologyProperty) {
		String propertyName = ontologyProperty.getPropertyId();
		propertiesMap.put(propertyName, ontologyProperty);
	}
	
	@Override
	public OntologyProperty getOntologyProperty(String propertyName) {
		return propertiesMap.get(propertyName);
	}
}
