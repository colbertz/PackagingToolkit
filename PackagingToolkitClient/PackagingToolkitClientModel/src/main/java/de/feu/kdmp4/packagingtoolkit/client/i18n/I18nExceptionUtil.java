package de.feu.kdmp4.packagingtoolkit.client.i18n;

import java.util.ResourceBundle;

/**
 * Encapsulates the keys for internationalization. Java code is not allowed
 * to call the message bundles but it has to use this class and its static
 * methods. Therefore the other classes do not have to know the names of 
 * the keys. This class contains the keys only for exceptions.
 * @author Christopher Olbertz
 *
 */
public class I18nExceptionUtil {
	private static final String DIGITAL_OBJECT_MAY_NOT_BE_NULL = "digital-object-may-not-be-null";
	private static final String ERROR = "error";
	private static final String FILENAME_MAY_NOT_BE_NULL = "filename-may-not-be-null";
	private static final String LOGO_FILE_NOT_FOUND = "logo-file-not-found";
	private static final String MEDIATOR_CONNECTION_NOT_POSSIBLE = "mediator-connection-not-possible";
	private static final String MEDIATOR_URL_MAY_NOT_BE_EMTPY = "mediator-url-may-not-be-empty";
	private static final String NOT_A_VALID_PACKAGE_TYPE = "not-a-valid-package-type";
	private static final String PATH_MAY_NOT_BE_NULL = "path-may-not-be-null";
	private static final String PROPERTY_VALUE_MAY_NOT_BE_NULL = "property-value-may-not-be-null";
	private static final String REFERENCE_MAY_NOT_BE_NULL = "reference-may-not-be-null";
	private static final String RESULT_DIR_PATH_URL_MAY_NOT_BE_EMTPY = "result-dir-path-may-not-be-empty";
	private static final String RULE_ONTOLOGY_DOES_NOT_EXIST = "rule-ontology-does-not-exist";
	private static final String SERVER_URL_MAY_NOT_BE_EMTPY = "server-url-may-not-be-empty";
	private static final String SERVER_CONNECTION_NOT_POSSIBLE = "server-connection-not-possible";
	private static final String SIP_MAY_CONTAIN_ONLY_ONE_DIGITAL_OBJECT = "sip-may-contain-only-one-digital-object";
	private static final String SUMMARY_CONNECTION_EXCEPTION = "summary-connection-exception";
	private static final String UI_PANEL_MAY_NOT_BE_NULL = "uipanel-may-not-be-null";
	private static final String WORKING_DIRECTORY_MAY_NOT_BE_EMPTY = "working-directory-may-not-be-empty";
	
	private static ResourceBundle messagesResourceBundle;
	
	static {
		messagesResourceBundle = I18nUtil.getExceptionsResourceBundle();
	}
	
	public static String getDigitalObjectMayNotBeNullString() {
		return messagesResourceBundle.getString(DIGITAL_OBJECT_MAY_NOT_BE_NULL);
	}

	public static String getErrorString() {
		return messagesResourceBundle.getString(ERROR);
	}
	
	public static String getLogoFIleNotFoundString() {
		return messagesResourceBundle.getString(LOGO_FILE_NOT_FOUND);
	}
	
	public static String getFilenameMayNotBeNullString() {
		return messagesResourceBundle.getString(FILENAME_MAY_NOT_BE_NULL);
	}

	public static String getMediatorConnectionNotPossibleString(String mediatorUrl) {
		String message = messagesResourceBundle.getString(MEDIATOR_CONNECTION_NOT_POSSIBLE);
		message = String.format(message, mediatorUrl);
		return message;
	}
	
	public static String getPathMayNotBeNullString() {
		return messagesResourceBundle.getString(PATH_MAY_NOT_BE_NULL);
	}
	
	public static String getReferenceMayNotBeNullString() {
		return messagesResourceBundle.getString(REFERENCE_MAY_NOT_BE_NULL);
	}
	
	public static String getOntologyDoesNotExistString() {
		return messagesResourceBundle.getString(RULE_ONTOLOGY_DOES_NOT_EXIST);
	}

	public static String getMediatorUrlMayNotBeEmptyString() {
		return messagesResourceBundle.getString(MEDIATOR_URL_MAY_NOT_BE_EMTPY);
	}
	
	public static String getNotAValidPackageTypeString() {
		return messagesResourceBundle.getString(NOT_A_VALID_PACKAGE_TYPE);
	}

	public static String getPropertyValueMayNotBeEmptyString() {
		return messagesResourceBundle.getString(PROPERTY_VALUE_MAY_NOT_BE_NULL);
	}
	
	public static String getResultDirPathMayNotBeEmptyString() {
		return messagesResourceBundle.getString(RESULT_DIR_PATH_URL_MAY_NOT_BE_EMTPY);
	}
	
	public static String getServerConnectionNotPossibleString(String serverUrl) {
		String message = messagesResourceBundle.getString(SERVER_CONNECTION_NOT_POSSIBLE);
		message = String.format(message, serverUrl);
		return message;
	}
	
	public static String getServerUrlMayNotBeEmptyString() {
		return messagesResourceBundle.getString(SERVER_URL_MAY_NOT_BE_EMTPY);
	}
	
	public static String getSummaryConnectionExceptionString() {
		return messagesResourceBundle.getString(SUMMARY_CONNECTION_EXCEPTION);
	}
	
	public static String getSipMayContainOnlyOneDigitalObjectString() {
		return messagesResourceBundle.getString(SIP_MAY_CONTAIN_ONLY_ONE_DIGITAL_OBJECT);
	}
	
	public static String getWorkingDirectoryMayNotBeEmptyString() {
		return messagesResourceBundle.getString(WORKING_DIRECTORY_MAY_NOT_BE_EMPTY);
	}
	
	public static String getUIPanelMayNotBeNullString() {
		return messagesResourceBundle.getString(UI_PANEL_MAY_NOT_BE_NULL);
	}
}
