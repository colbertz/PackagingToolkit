package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class OntologyIndividualCollectionImpl extends AbstractList implements OntologyIndividualCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;

	@Override
	public void addIndividual(OntologyIndividual individual) {
//		try {
			super.add(individual);
	/*	} catch (ListElementMayNotBeNullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	@Override
	public OntologyIndividual getIndividual(int index) {
		return (OntologyIndividual)super.getElement(index);
	}
	
	@Override
	public Optional<OntologyIndividual> getIndividualByUuid(Uuid uuid) {
		OntologyIndividual individual = null;
		int i = 0;
		boolean found = false;
		
		while (found == false && i < getIndiviualsCount()) {
			individual = getIndividual(i);
			Uuid individualUuid = individual.getUuid();
			
			if (individualUuid.equals(uuid)) {
				found = true;
			}
			
			i++;
		}
				
		return ClientOptionalFactory.createOntologyIndividualOptional(individual);
	}
	
	@Override
	public int getIndiviualsCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}

	@Override
	public boolean isNotEmpty() {
		return !super.isEmpty();
	}

	@Override
	public void removeIndividual(int index) {
		OntologyIndividual individual = getIndividual(index);
		super.remove(individual);
	}

	@Override
	public void appendIndividualList(OntologyIndividualCollection individualList) {
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			OntologyIndividual individual = individualList.getIndividual(i);
			addIndividual(individual);
		}
	}
	
	@Override
	public void replaceIndividual(OntologyIndividual newOntologyIndividual) {
		Uuid uuidOfNewIndividual = newOntologyIndividual.getUuid();
		
		int counter = 0;
		boolean individualFound = false;
		
		while(counter < getIndiviualsCount() && individualFound == false) {
			OntologyIndividual ontologyIndividual = getIndividual(counter);
			if (ontologyIndividual.getUuid().equals(uuidOfNewIndividual)) {
				remove(counter);
				addObjectAt(counter, newOntologyIndividual);
				individualFound = true;
			}
			counter++;
		}
	}

	@Override
	public List<OntologyIndividual> toList() {
		List<OntologyIndividual> individualList = new ArrayList<>();
		
		for (int i = 0; i < getIndiviualsCount(); i++) {
			OntologyIndividual ontologyIndividual = getIndividual(i);
			individualList.add(ontologyIndividual);
		}
		
		return individualList;
	}
	
	@Override
	public boolean containsIndividual(final OntologyIndividual ontologyIndividual) {
		return super.contains(ontologyIndividual);
	}

	@Override
	public void removeIndividual(final OntologyIndividual ontologyIndividual) {
		super.remove(ontologyIndividual);
	}

	@Override
	public boolean containsNotIndividual(OntologyIndividual ontologyIndividual) {
		return !containsIndividual(ontologyIndividual);
	}
}
