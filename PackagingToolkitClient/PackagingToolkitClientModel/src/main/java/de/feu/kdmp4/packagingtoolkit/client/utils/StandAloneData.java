package de.feu.kdmp4.packagingtoolkit.client.utils;

import de.feu.kdmp4.packagingtoolkit.response.ExtractorToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ExtractorToolResponse;

/**
 * Contains some data that are used for testing the gui in the stand alone mode.
 * @author Christopher Olbertz
 *
 */
public abstract class StandAloneData {
	private static ExtractorToolListResponse extractorToolList;
	
	static {
		initializeFitsTools();
	}
	
	public static ExtractorToolListResponse getExtractorToolList() {
		return extractorToolList;
	}
	
	private static void initializeFitsTools() {
		extractorToolList = new ExtractorToolListResponse();
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("MediaInfo", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("OIS Audio Information", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("ADL Tool"));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("VTT Too", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("Droid", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("Jhove", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("File Utility", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("Exiftool", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("NLNZ Metadata Extractor", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("OIS File Information", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("OIS XML Metadata", false));
		extractorToolList.addExtractorToolToList(new ExtractorToolResponse("Tika", false));
	}
}
