package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface MetadataElement extends Comparable<MetadataElement>{
	/**
	 * Adds a list with conflictingMetadataElements to the list of conflicting metadata elements.
	 * @param metadataElementList a list with conflictingMetadataElements.
	 */
	void addConflictingMetadataElement(List<MetadataElement> metadataElementList);
	public Uuid getUuid();
	public String getName();
	public String getValue();
	/**
	 * Determines the number of metadata elements in the list.
	 * @return The number of metadata elements in the list.
	 */
	int getConflictingMetadataElementsCount();
	/**
	 * Returns the value at the given index. If the metadata element contains only one value,
	 * the first value is returned. 
	 * @param index The index of the element in the list.
	 * @return The value.
	 * @throws 
	 */
	public String getValueAt(int index);
	/**
	 * Adds a value to this element. Because there is already a value when creating the element, we know after 
	 * calling this method, that this element can contain multiple values.
	 * @param value The value that should be added. 
	 */
	void addValue(String value);
	boolean isMultipleValueElement();
	int getValueCount();
	MetadataElement getConflictingMetadataElementAt(int index);
	
}
