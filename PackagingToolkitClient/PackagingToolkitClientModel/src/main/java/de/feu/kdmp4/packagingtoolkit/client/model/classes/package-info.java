/**
 * Contains the implementations of the interfaces in 
 * {@link de.feu.kdmp4.packagingtoolkit.client.model.interfaces}.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.client.model.classes;