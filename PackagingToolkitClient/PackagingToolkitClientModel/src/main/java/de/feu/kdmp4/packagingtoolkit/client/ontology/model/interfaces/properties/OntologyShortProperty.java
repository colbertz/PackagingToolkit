package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyShortPropertyImpl;

public interface OntologyShortProperty extends OntologyDatatypeProperty {
	void addMaxInclusiveRestriction(int value);
	void addMinInclusiveRestriction(int value);
	public Integer getPropertyValue();
	
	public static OntologyShortProperty createUnsigned(String propertyId, String label, String range) {
		return new OntologyShortPropertyImpl(propertyId, true, label, range); 
	}
	
	public static OntologyShortProperty createSigned(String propertyId, String label, String range) {
		return new OntologyShortPropertyImpl(propertyId, false, label, range); 
	}
	void setPropertyValue(int value);
	boolean isUnsigned();
}
