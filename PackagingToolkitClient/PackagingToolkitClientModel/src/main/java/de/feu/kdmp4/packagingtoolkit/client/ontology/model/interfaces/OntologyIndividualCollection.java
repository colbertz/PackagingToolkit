package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import java.util.List;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface OntologyIndividualCollection extends Cloneable {
	void addIndividual(OntologyIndividual individual);
	void appendIndividualList(OntologyIndividualCollection individualList);
	OntologyIndividual getIndividual(int index);
	int getIndiviualsCount();
	boolean isEmpty();
	boolean isNotEmpty();
	Optional<OntologyIndividual> getIndividualByUuid(Uuid uuid);
	void removeIndividual(int index);
	void removeIndividual(OntologyIndividual ontologyIndividual);
	void replaceIndividual(OntologyIndividual newOntologyIndividual);
	List<OntologyIndividual> toList();
	boolean containsIndividual(OntologyIndividual ontologyIndividual);
	boolean containsNotIndividual(OntologyIndividual ontologyIndividual);
}
