package de.feu.kdmp4.packagingtoolkit.client.model.decorators.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.OntologyIndividualFinishedDecorator;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;

public class OntologyIndividualFinishedDecoratorImpl implements OntologyIndividualFinishedDecorator {
	/**
	 * The individual that is decorated.
	 */
	private OntologyIndividual ontologyIndividual;
	/**
	 * True if the creation process of this individual has been finished.
	 */
	private boolean finished;
	
	public OntologyIndividualFinishedDecoratorImpl(OntologyIndividual ontologyIndividual) {
		this.ontologyIndividual = ontologyIndividual;
		finished = false;
	}
	
	@Override
	public void finishIndividualCreation() {
		finished = true;
	}
	
	@Override
	public OntologyIndividual getOntologyIndividual() {
		return ontologyIndividual;
	}
	
	@Override
	public boolean isFinished() {
		return finished;
	}
	
	@Override
	public void addDatatypeProperty(OntologyDatatypeProperty ontologyDatatypeProperty) {
		ontologyIndividual.addDatatypeProperty(ontologyDatatypeProperty);
	}
}
