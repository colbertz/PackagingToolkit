package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

public class DataSourceToolCollectionListImpl extends AbstractList implements DataSourceToolCollection {
	public DataSourceToolCollectionListImpl() {
		
	}

	/**
	 * Constructs a list with data source tools of a string list with the names of the
	 * data source tools.
	 * @param dataSourceToolNamesCollection A list with the names of the data source tools.
	 */
	public DataSourceToolCollectionListImpl(final StringList dataSourceToolNamesCollection) {
		for (int i = 0; i < dataSourceToolNamesCollection.getSize(); i++) {
			final String dataSourceToolName = dataSourceToolNamesCollection.getStringAt(i);
			final DataSourceTool dataSourceTool = ClientModelFactory.createDataSourceTool(dataSourceToolName);
			addDataSourceToolToList(dataSourceTool);
		}
	}
	
	@Override
	public void addDataSourceToolToList(final DataSourceTool dataSourceTool) {
			super.add(dataSourceTool);
	}
	
	@Override
	public boolean containsDataSourceTool(DataSourceTool dataSourceTool) {
		return super.contains(dataSourceTool); 
	}
	
	@Override
	public DataSourceTool getDataSourceToolAt(final int index) {
			DataSourceTool dataSourceTool = (DataSourceToolImpl)super.getElement(index);
			return dataSourceTool;
	}
	
	@Override
	public int getDataSourceToolCount() {
		return super.getSize();
	}
	
	@Override
	public void removeDataSourceTool(final DataSourceTool dataSourceTool) {
			super.remove(dataSourceTool);
	}
}