package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.validators.UrlValidator;

/**
 * A reference to a file. At the moment there are two types of reference:
 * <ul>
 * 	<li>A reference to a file on the local file system of the server. These files are stored
 * in the storage directory.</li>
 * 	<li>A reference that points to a remote file in the internet./<li>
 * </ul>
 * @author Christopher Olbertz
 *
 */
public class ReferenceImpl implements Reference {
	private String url;
	private ReferenceType referenceType;
	
	/**
	 * The uuid of the file this reference belongs to. The reference type is determined
	 * by the url. If the url starts with http the reference is a http reference to a remote
	 * file, if the url starts with file the reference references a file in the storage directory
	 * of the server.
	 */
	private Uuid uuidOfFile;
	
	protected ReferenceImpl(final String url, final Uuid uuidOfFile) {
		super();
		this.url = url;
		this.uuidOfFile = uuidOfFile;
		referenceType = ReferenceType.LOCAL_FILE;
	}
	
	/**
	 * Creates a new reference to a remote file. 
	 * @param url The url.
	 * @throws UrlException if url is not a valid url.
	 */
	protected ReferenceImpl(final String url) {
		super();
		UrlValidator.checkUrl(url);
		this.url = url;
		referenceType = ReferenceType.REMOTE_FILE;
	}
	
	public static Reference createRemoteUrlReference(final String url) {
		return new ReferenceImpl(url);
	}

	public static Reference createLocalFileReference(final String url, final Uuid uuidOfFile) {
		return new ReferenceImpl(url, uuidOfFile);
	}
	
	@Override
	public boolean isLocalFileReference() {
		if (referenceType == ReferenceType.LOCAL_FILE) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isRemoteFileReference() {
		if (referenceType == ReferenceType.REMOTE_FILE) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String getUrl() {
		return url;
	}
	
	@Override
	public Uuid getUuidOfFile() {
		return uuidOfFile;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((referenceType == null) ? 0 : referenceType.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((uuidOfFile == null) ? 0 : uuidOfFile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReferenceImpl other = (ReferenceImpl) obj;
		if (referenceType != other.referenceType)
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		if (uuidOfFile == null) {
			if (other.uuidOfFile != null)
				return false;
		} else if (!uuidOfFile.equals(other.uuidOfFile))
			return false;
		return true;
	}



	/**
	 * An enum for the type of the reference. It can be a reference to a file that is saved in the storage
	 * or a reference to a remote file via http. An enum instead a boolean value is used because there may be
	 * an extension in the future. 
	 * @author Christopher Olbertz
	 *
	 */
	private enum ReferenceType {
		LOCAL_FILE, REMOTE_FILE;
	}
}