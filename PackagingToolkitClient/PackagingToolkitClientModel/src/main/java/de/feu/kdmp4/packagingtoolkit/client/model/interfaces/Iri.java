package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

/**
 * Encapsulates an Iri that is indepenendant from any framework.
 * @author Christopher Olbertz
 *
 */
public interface Iri extends Comparable<Iri> {
	/**
	 * Returns the local name of the iri. This is part after the # symbol.
	 * @return The local name.
	 */
	LocalName getLocalName();
	/**
	 * Returns the namespace of the iri. This is part before the # symbol.
	 * @return The namespace.
	 */
	Namespace getNamespace();
}
