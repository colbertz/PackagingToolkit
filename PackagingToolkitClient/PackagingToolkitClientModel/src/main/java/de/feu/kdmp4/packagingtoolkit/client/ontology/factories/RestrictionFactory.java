package de.feu.kdmp4.packagingtoolkit.client.ontology.factories;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.MaxInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.MinInclusiveImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.RestrictionCollection;

public class RestrictionFactory extends AbstractFactory {
	public MaxInclusive createMaxInclusive(RestrictionValue value) {
		MaxInclusive maxInclusive = new MaxInclusiveImpl(value);
		//super.initBean(maxInclusive);
		
		return maxInclusive; 
	}
	
	public MinInclusive createMinInclusive(RestrictionValue value) {
		MinInclusive minInclusive = new MinInclusiveImpl(value);
		//super.initBean(minInclusive);
		
		return minInclusive; 
	}
	
	public RestrictionCollection createRestrictionList() {
		return new RestrictionCollectionImpl();
	}
}
