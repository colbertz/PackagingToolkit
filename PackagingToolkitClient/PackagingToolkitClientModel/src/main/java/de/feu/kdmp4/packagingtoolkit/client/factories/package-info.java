/**
 * Contains classes that create objects used in the client. They are used for holding the
 * implementations independant from the interfaces. In the whole application only 
 * interfaces are used. These factories are the only classes that know the real
 * implementations. 
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.factories;