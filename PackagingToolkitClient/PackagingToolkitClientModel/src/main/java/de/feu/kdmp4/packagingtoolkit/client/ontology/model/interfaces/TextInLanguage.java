package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import java.util.Locale;

/**
 * Represents a text in a specific language. It contains the text 
 * itself and the language the text is written in. Is used for the 
 * property labels that can be written in different languages. 
 * @author Christopher Olbertz
 *
 */
public interface TextInLanguage {
	String getLabelText();
	void setLabelText(final String labelText);
	Locale getLanguage();
	void setLanguage(final Locale language);
	
}
