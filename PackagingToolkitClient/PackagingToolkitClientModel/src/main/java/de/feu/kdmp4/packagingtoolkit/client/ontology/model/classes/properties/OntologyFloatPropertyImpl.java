package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MinInclusive;


public class OntologyFloatPropertyImpl extends OntologyDatatypePropertyImpl implements OntologyFloatProperty {
	// ******* Constants ******
	private static final float DEFAULT_VALUE = 0.0f;
	private static final long serialVersionUID = -6417668866515143729L;

	public OntologyFloatPropertyImpl() {
	}
	
	public OntologyFloatPropertyImpl(final String propertyId, final String defaultLabel, final String range) {
		super(propertyId, DEFAULT_VALUE, defaultLabel, range);
	}

	public OntologyFloatPropertyImpl(final String propertyId, final double value, final String defaultLabel, final String range) {
		super(propertyId, value, defaultLabel, range);
	}
	
	@Override
	public void setPropertyValue(Float value) {
		super.setValue(value);
	}
	
	@PostConstruct
	public void initialize() {
		addMaxInclusiveRestriction(Float.MAX_VALUE);
		addMinInclusiveRestriction(Float.MIN_VALUE);
	}

	@Override
	public void addMaxInclusiveRestriction(final float value) {
		final RestrictionValue<Float> restrictionValue = new RestrictionValue<>(value);
		final MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public void addMinInclusiveRestriction(final float value) {
		final RestrictionValue<Float> restrictionValue = new RestrictionValue<>(value);
		final MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public Float getPropertyValue() {
		return (Float) getValue();
	}

	@Override
	public boolean isFloatProperty() {
		return true;
	}
	
	/*@Override
	public void setPropertyValue(final Object value) {
		String valueAsString = value.toString();
		if (valueAsString.contains(DECIMAL_SEPARATOR_DE)) {
			valueAsString = valueAsString.replaceAll(DECIMAL_SEPARATOR_DE, DECIMAL_SEPARATOR_EN);
		}
		super.setPropertyValue(valueAsString);
	}*/
}
