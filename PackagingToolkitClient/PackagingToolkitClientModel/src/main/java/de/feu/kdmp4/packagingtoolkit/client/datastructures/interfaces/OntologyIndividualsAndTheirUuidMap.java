package de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Contains individuals. Their keys are their uuids.
 * @author Christopher Olbertz
 *
 */
public interface OntologyIndividualsAndTheirUuidMap {
	/**
	 * Adds an individual to the map. 
	 * @param ontologyIndividual The individual to add to the map. Its uuid is used as its key.
	 */
	void addIndividual(OntologyIndividual ontologyIndividual);
	/**
	 * Gets an individual from the map with the help of the uuid.
	 * @param uuidOfIndividual The uuid of the individual we are looking for.
	 * @return The individual with the uuid uuidOfIndividual.
	 */
	OntologyIndividual getIndividual(Uuid uuidOfIndividual);
	/**
	 * Replaces an individual with the help of its uuid.
	 * @param newOntologyIndividual The individual that should replace the one with the
	 * same uuid.
	 */
	void replaceIndividual(OntologyIndividual newOntologyIndividual);
}
