package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;


/**
 * Represents an property in an ontology that is a String.
 * @author Christopher Olbertz
 *
 */
public interface OntologyStringProperty extends OntologyDatatypeProperty {
	public String getPropertyValue();
	int compareTo(OntologyStringProperty ontologyProperty);
	void setPropertyValue(String value);
}
