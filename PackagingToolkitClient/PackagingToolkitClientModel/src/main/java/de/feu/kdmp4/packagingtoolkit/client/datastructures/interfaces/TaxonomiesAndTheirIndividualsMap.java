package de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces;

import java.util.List;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;

/**
 * Contains the individuals that are assigned to a taxonomy. The key is the iri of the taxonomy and the
 * value is a collection with TaxonomyIndividuals.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomiesAndTheirIndividualsMap {
	/**
	 * Assigns an individual to a taxonomy. 
	 * @param iriOfTaxonomy The iri of the taxonomy the individuals should be added to.  
	 * @param taxonomyIndividual The individual that should be added to the map.
	 */
	void addIndividualToTaxonomy(Iri iriOfTaxonomy, TaxonomyIndividual taxonomyIndividual);
	/**
	 * Finds the individuals of a certain taxonomy. 
	 * @param iriOfTaxonomy The iri of the taxonomy whose individuals we want to find.
	 * @return The found individuals or an empty optional.
	 */
	Optional<TaxonomyIndividualCollection> findIndividualsOfTaxonomy(Iri iriOfTaxonomy);
	/**
	 * Assigns some individuals to a taxonomy.
	 * @param taxonomyIndividual The individual that should be added to the map.
	 * @param newTaxonomyIndividuals The individuals that should be added to the map.
	 */
	void addIndividualsToTaxonomy(Iri iriOfTaxonomy, TaxonomyIndividualCollection newTaxonomyIndividuals);
	/**
	 * Writes all values contained in the map in a list with TaxonomyIndividualCollection objects. Every collection object knows the 
	 * taxonomy the contained individuals belong to.
	 * @return Contains all values in the map.
	 */
	List<TaxonomyIndividualCollection> asList();

}
