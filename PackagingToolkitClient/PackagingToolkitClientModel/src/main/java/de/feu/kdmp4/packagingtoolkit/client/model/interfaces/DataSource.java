package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

/**
 * Represents a data source. On the mediator, every data source is encapsulated by a wrapper. This 
 * interface and its implementation are used to describe the data sources independant from their
 * wrapper. For example they can be used for sending information about the available data sources
 * and their tools to the client.
 * @author Christopher Olbertz
 *
 */
public interface DataSource {
	/**
	 * Adds a new data source tool to this data source.
	 * @param dataSourceTool The data source tool that should be added.
	 */
	void addDataSourceTool(DataSourceTool dataSourceTool);
	/**
	 * Determines a tool at a certain index.
	 * @param index The index of the tool in the collection.
	 * @return The tool found at index.
	 */
	DataSourceTool getDataSourceToolAt(int index);
	/**
	 * Determines the number of tools in this data source.
	 * @return The number of tools.
	 */
	int getDataSourceToolCount();
	String getDataSourceName();
	DataSourceToolCollection getDataSourceTools();
}
