package de.feu.kdmp4.packagingtoolkit.client.factories;

import java.io.File;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * Creates the response objects out of the model objects and vice versa. The response objects are used for
 * sending between server, mediator and clients. This class only works with the model objects of the server.
 * @author Christopher Olbertz
 *
 */
public class ClientResponseFactory {
	/**
	 * Creates an reponse object out of an object of {@link de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse}.
	 * @param dataSource The list with the data sources that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public DataSourceListResponse toResponse(final DataSourceCollection dataSourceCollection) {
		final DataSourceListResponse dataSourceListResponse = new DataSourceListResponse();
		
		for (int i = 0; i < dataSourceCollection.getDataSourceCount(); i++) {
			final DataSource dataSource = dataSourceCollection.getDataSourceAt(i);
			final DataSourceResponse dataSourceResponse = toResponse(dataSource);
			dataSourceListResponse.addDataSource(dataSourceResponse);
		}
		
		return dataSourceListResponse;
	}
	
	
	/**
	 * Creates an reponse object out of an object of {@link de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse}.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public DataSourceResponse toResponse(final DataSource dataSource) {
		final	DataSourceResponse dataSourceResponse = new DataSourceResponse();
		dataSourceResponse.setDataSourceName(dataSource.getDataSourceName());
		
		for (int i = 0; i < dataSource.getDataSourceToolCount(); i++) {
			final DataSourceTool dataSourceTool = dataSource.getDataSourceToolAt(i);
			final DataSourceToolResponse dataSourceToolResponse = toResponse(dataSourceTool);
			dataSourceResponse.getDataSourceTools().addDataSourceTool(dataSourceToolResponse);
		}
		
		return dataSourceResponse;
	}
	
	/**
	 * Creates an reponse object out of an object of {@link de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse}.
	 * @param dataSourceTool The data source tool that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public DataSourceToolResponse toResponse(final DataSourceTool dataSourceTool) {
		final DataSourceToolResponse dataSourceToolResponse = new DataSourceToolResponse();
		dataSourceToolResponse.setActivated(dataSourceTool.isActivated());
		dataSourceToolResponse.setDataSourceToolName(dataSourceTool.getToolName());
		return dataSourceToolResponse;
	}
	
	/**
	 * Creates an reponse object out of an object of {@link de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse}.
	 * @param datatypePropertyList The property list that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public DatatypePropertyListResponse toResponse(final DatatypePropertyCollection datatypePropertyList) {
		final DatatypePropertyListResponse listResponse = ResponseModelFactory.getOntologyPropertyListResponse();
		
		for (int i = 0; i < datatypePropertyList.getPropertiesCount(); i++) {
			final OntologyDatatypeProperty ontologyProperty = datatypePropertyList.getDatatypeProperty(i);
			final DatatypePropertyResponse ontologyPropertyResponse = toResponse(ontologyProperty); 
			listResponse.addDatatypePropertyToList(ontologyPropertyResponse);
		}
		return listResponse;
	}
	
	/**
	 * Uses a digital object to create a response object. During this process the md5 checksum of the
	 * file that is represented by this digital object is calculated and stored into the response object.
	 * @param digitalObject The digital object that should be send to the server.
	 * @return The response object representing the digital object.
	 */
	public DigitalObjectResponse toResponse(final DigitalObject digitalObject) {
		final Uuid informationPackageUuid = digitalObject.getUuidOfInformationPackage();
		final UuidResponse uuid = ResponseModelFactory.getUuidResponse(informationPackageUuid);
		final String filename = digitalObject.getAbsolutePath();
		final File fileOfDigitalObject = new File(filename);
		final String md5Checksum = PackagingToolkitFileUtils.calculateMD5(fileOfDigitalObject);
		
		final DigitalObjectResponse digitalObjectResponse = ResponseModelFactory.getDigitalObjectResponse(filename, uuid);
		digitalObjectResponse.setMd5Checksum(md5Checksum);
		
		return digitalObjectResponse;
	}
	
	/**
	 * Creates an reponse object out of an object of {@link de.feu.kdmp4.packagingtoolkit.model.interfaces.DataSourceResponse}.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public ReferenceResponse toResponse(final Reference reference) {
		final ReferenceResponse referenceResponse = ResponseModelFactory.getReferenceResponse();
		referenceResponse.setUrl(reference.getUrl());
		final Uuid uuid = reference.getUuidOfFile();
		
		if (uuid != null) {
			final UuidResponse uuidOfFile = ResponseModelFactory.getUuidResponse(uuid);
				referenceResponse.setUuidOfFile(uuidOfFile);
		}
		
		return referenceResponse;
	}
	
	/**
	 * Creates a response object with the values of an object property.  
	 * @param ontologyObjectProperty The object property we want to send via http.
	 * @return The response object for sending via http.
	 */
	public ObjectPropertyResponse toResponse(final OntologyObjectProperty ontologyObjectProperty) {
		final ObjectPropertyResponse objectPropertyResponse = ResponseModelFactory.getObjectPropertyResponse();
		
		objectPropertyResponse.setLabel(ontologyObjectProperty.getLabel());
		objectPropertyResponse.setRange(ontologyObjectProperty.getPropertyRange());
		objectPropertyResponse.setPropertyName(ontologyObjectProperty.getPropertyId());
		final OntologyIndividualCollection valuesOfObjectProperty = ontologyObjectProperty.getPropertyValue();
		
		final IriListResponse iriOfIndividualsList = toIriListResponse(valuesOfObjectProperty);
		objectPropertyResponse.setValue(iriOfIndividualsList);
		
		return objectPropertyResponse;
	}
	
	/**
	 * Creates a list with the iris of individuals for sending via http.
	 * @param ontologyIndividualList The list that contain the individuals whose iris we want to send.
	 * @return The list with the iris for sending via http.
	 */
	private IriListResponse toIriListResponse(final OntologyIndividualCollection ontologyIndividualList) {
		final IriListResponse iriListResponse = new IriListResponse();
		
		for (int i = 0; i < ontologyIndividualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = ontologyIndividualList.getIndividual(i);
			final Iri iriOfIndividual = ontologyIndividual.getIriOfIndividual();
			final String namespace = iriOfIndividual.getNamespace().toString();
			final String localName = iriOfIndividual.getLocalName().toString();
			final IriResponse iriResponseOfIndividual = new IriResponse(namespace, localName);
			iriListResponse.addIriToList(iriResponseOfIndividual);
		}
		
		return iriListResponse;
	}
	
	/**
	 * Creates an reponse object out of an object of 
	 * {@link de.feu.kdmp4.packagingtoolkit.model.interfaces.properties.OntologyDatatyeProperty}.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public DatatypePropertyResponse toResponse(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		DatatypePropertyResponse propertyResponse = null;
		
		if (ontologyDatatypeProperty instanceof OntologyBooleanProperty) {
			propertyResponse = fromOntologyBooleanProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyByteProperty) {
			propertyResponse = fromOntologyByteProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDateProperty) {
			propertyResponse = fromOntologyDateProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDateTimeProperty) {
			propertyResponse = fromOntologyDateTimeProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDoubleProperty) {
			propertyResponse = fromOntologyDoubleProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyDurationProperty) {
			propertyResponse = fromOntologyDurationProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyFloatProperty) {
			propertyResponse = fromOntologyFloatProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyIntegerProperty) {
			propertyResponse = fromOntologyIntegerProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyLongProperty) {
			propertyResponse = fromOntologyLongProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyShortProperty) {
			propertyResponse = fromOntologyShortProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyStringProperty) {
			propertyResponse = fromOntologyStringProperty(ontologyDatatypeProperty);
		} else if (ontologyDatatypeProperty instanceof OntologyTimeProperty) {
			propertyResponse = fromOntologyTimeProperty(ontologyDatatypeProperty);
		}
		
		propertyResponse.setLabel(ontologyDatatypeProperty.getLabel());
		propertyResponse.setPropertyName(ontologyDatatypeProperty.getPropertyId());
		return propertyResponse;		
	}
	
	/**
	 * Creates an reponse object out of a boolean property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyBooleanProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyBooleanProperty ontologyBooleanProperty = (OntologyBooleanProperty)ontologyDatatypeProperty;
		final Boolean value = (Boolean)ontologyBooleanProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Boolean.toString(value));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.BOOLEAN);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a byte property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyByteProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyByteProperty ontologyByteProperty = (OntologyByteProperty)ontologyDatatypeProperty;
		final Short value = (Short)ontologyByteProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Short.toString(value));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.BYTE);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a date property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyDateProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDateProperty ontologyDateProperty = (OntologyDateProperty)ontologyDatatypeProperty;
		final LocalDate value = (LocalDate)ontologyDateProperty.getPropertyValue();		
		if (value != null) {
			final long epochDay = value.toEpochDay();
			propertyResponse.setValue(Long.toString(epochDay));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DATE);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a datetime property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyDateTimeProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDateTimeProperty ontologyDateTimeProperty = (OntologyDateTimeProperty)ontologyDatatypeProperty;
		final LocalDateTime value = (LocalDateTime)ontologyDateTimeProperty.getPropertyValue();		
		if (value != null) {
			final long epochSecond = value.toEpochSecond(ZoneOffset.UTC);
			propertyResponse.setValue(Long.toString(epochSecond));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DATETIME);
		return propertyResponse;
	}
	
	private DatatypePropertyResponse fromOntologyDoubleProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDoubleProperty ontologyDoubleProperty = (OntologyDoubleProperty)ontologyDatatypeProperty;
		final Double value = (Double)ontologyDoubleProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Double.toString(value));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DOUBLE);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a duration property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyDurationProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyDurationProperty ontologyDurationProperty = (OntologyDurationProperty)ontologyDatatypeProperty;
		final OntologyDuration value = (OntologyDuration)ontologyDurationProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(value.toString());
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.DURATION);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a float property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyFloatProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyFloatProperty ontologyFloatProperty = (OntologyFloatProperty)ontologyDatatypeProperty;
		final Float value = (Float)ontologyFloatProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Float.toString(value));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.FLOAT);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of an integer property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyIntegerProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyIntegerProperty ontologyIntegerProperty = (OntologyIntegerProperty)ontologyDatatypeProperty;
		final Long value = (Long)ontologyIntegerProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Long.toString(value));
		}
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.INTEGER);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a long property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyLongProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyLongProperty ontologyLongProperty = (OntologyLongProperty)ontologyDatatypeProperty;
		final BigInteger value = (BigInteger)ontologyLongProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(value.toString());
		}
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.LONG);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a short property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyShortProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyShortProperty ontologyShortProperty = (OntologyShortProperty)ontologyDatatypeProperty;
		final Integer value = (Integer)ontologyShortProperty.getPropertyValue();
		if (value != null) {
			propertyResponse.setValue(Integer.toString(value));
		}
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.SHORT);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a string property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyStringProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyStringProperty ontologyStringProperty = (OntologyStringProperty)ontologyDatatypeProperty;
		final String value = (String)ontologyStringProperty.getPropertyValue();
		propertyResponse.setValue(value);
		
		propertyResponse.setDatatype(OntologyDatatypeResponse.STRING);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of a time property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	private DatatypePropertyResponse fromOntologyTimeProperty(final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final DatatypePropertyResponse propertyResponse = ResponseModelFactory.getOntologyPropertyResponse();
		final OntologyTimeProperty ontologyTimeProperty = (OntologyTimeProperty)ontologyDatatypeProperty;
		final LocalTime value = (LocalTime)ontologyTimeProperty.getPropertyValue();		
		if (value != null) {
			long nanoOfDay = value.toNanoOfDay();
			propertyResponse.setValue(Long.toString(nanoOfDay));
		}
		propertyResponse.setDatatype(OntologyDatatypeResponse.TIME);
		return propertyResponse;
	}
	
	/**
	 * Creates an reponse object out of an object property.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public ObjectPropertyResponse fromObjectProperty(final OntologyObjectProperty ontologyObjectProperty) {
		final ObjectPropertyResponse propertyResponse = ResponseModelFactory.getObjectPropertyResponse();
		propertyResponse.setPropertyName(ontologyObjectProperty.getPropertyId());
		propertyResponse.setLabel(ontologyObjectProperty.getLabel());
		final OntologyClass range = ontologyObjectProperty.getRange();
		propertyResponse.setRange(range.getFullName());
		return propertyResponse;		
	}
	
	/**
	 * Creates an reponse object with the values of an individual.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public IndividualResponse fromOntologyIndividual(final OntologyIndividual ontologyIndividual) {
		final IndividualResponse individualResponse = ResponseModelFactory.getIndividualResponse();
		final UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(ontologyIndividual.getUuid().toString());
		individualResponse.setUuid(uuidResponse);
		final int datatypePropertiesCount = ontologyIndividual.getDatatypePropertiesCount();
		
		for (int i = 0; i < datatypePropertiesCount; i++) {
			final OntologyDatatypeProperty ontologyProperty = ontologyIndividual.getDatatypePropertyAt(i);
			final DatatypePropertyResponse propertyResponse = toResponse(ontologyProperty);
			individualResponse.addOntologyProperty(propertyResponse);
		}
		
		final int objectPropertyCount = ontologyIndividual.getObjectPropertiesCount();
		for (int i = 0; i < objectPropertyCount; i++) { 
			final OntologyObjectProperty ontologyObjectProperty = ontologyIndividual.getObjectPropertyAt(i);
			final ObjectPropertyResponse objectPropertyResponse = toResponse(ontologyObjectProperty);
			individualResponse.addObjectProperty(objectPropertyResponse);
		}
		
		final OntologyClass ontologyClass = ontologyIndividual.getOntologyClass();
		final String classIri = ontologyClass.getFullName();
		individualResponse.setClassIri(classIri);  
		
		return individualResponse;
	}
	
	/**
	 * Creates a reponse object with the values of a list with individuals.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public IndividualListResponse fromOntologyIndividualList(final OntologyIndividualCollection individualList) {
		final IndividualListResponse individualListResponse = ResponseModelFactory.getIndividualListResponse();
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
			final IndividualResponse individualResponse = fromOntologyIndividual(ontologyIndividual);
			individualListResponse.addIndividualToList(individualResponse);
		}
		
		return individualListResponse;
	}
	
	/**
	 * Creates a reponse object with the values of a list with individuals of a taxonomy.
	 * @param taxonomyIndividuals The individuals that are  converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public TaxonomyIndividualListResponse fromTaxonomyIndividualCollection(final TaxonomyIndividualCollection taxonomyIndividuals) {
		final TaxonomyIndividualListResponse taxonomyIndividualsResponse = ResponseModelFactory.createEmptyTaxonomyIndividualListResponse();
		
		for (int i = 0; i < taxonomyIndividuals.getSize(); i++) {
			final TaxonomyIndividual taxonomyIndividual = taxonomyIndividuals.getTaxonomyIndividual(i).get();
			final TaxonomyIndividualResponse individualResponse = fromTaxonomyIndividual(taxonomyIndividual);
			taxonomyIndividualsResponse.addTaxonomyIndividual(individualResponse);
			
			if (taxonomyIndividuals.getIriOfTaxonomy() != null) {
						final String iriOfTaxonomy = taxonomyIndividuals.getIriOfTaxonomy().toString();
						taxonomyIndividualsResponse.setIriOfTaxonomy(iriOfTaxonomy);
			}
		}
		
		return taxonomyIndividualsResponse;
	}
	
	/**
	 * Creates a reponse object with the values of a individual in a taxonomy.
	 * @param taxonomyIndividual The individuals that are  converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public TaxonomyIndividualResponse fromTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual) {
		final TaxonomyIndividualResponse individualResponse = ResponseModelFactory.createTaxonomyIndividualResponse();
		
		individualResponse.setIri(taxonomyIndividual.getIri().toString());
		individualResponse.setPreferredLabel(taxonomyIndividual.getPreferredLabel());
		
		while(taxonomyIndividual.hasNextNarrower()) {
			final TaxonomyIndividual narrower = taxonomyIndividual.getNextNarrower().get();
			final TaxonomyIndividualResponse narrowerResponse = fromTaxonomyIndividual(narrower);
			individualResponse.addNarrower(narrowerResponse);
		}
		
		return individualResponse;
	}
	
	/**
	 * Creates an reponse object with the values of an ontology class.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public OntologyClassResponse fromOntologyClass(final OntologyClass ontologyClass) {
		final String namespace = ontologyClass.getNamespace();
		final String localName = ontologyClass.getLocalName();
		
		final OntologyClassResponse ontologyClassResponse = ResponseModelFactory.getOntologyClassResponse(namespace, localName);
		
		if (ontologyClass.hasDatatypeProperties()) {
			final DatatypePropertyListResponse datatypeProperties = new DatatypePropertyListResponse();
			for (int i = 0; i < ontologyClass.getDatatypePropertiesCount(); i++) {
				final OntologyDatatypeProperty  ontologyDatatypeProperty = ontologyClass.getDatatypeProperty(i);
				final DatatypePropertyResponse datatypePropertyResponse = toResponse(ontologyDatatypeProperty);
				datatypeProperties.addDatatypePropertyToList(datatypePropertyResponse);
			}
			ontologyClassResponse.setProperties(datatypeProperties);
		}
				
		return ontologyClassResponse;
	}

	/**
	 * Creates an reponse object with the values of a view.
	 * @param dataSource The data source that is converted into a response object.
	 * @return The converted response object ready for beeing send over http to the client.
	 */
	public ViewResponse fromView(final View view) {
		final String viewName = view.getViewname();
		final int viewId = view.getViewId();
		final ViewResponse viewResponse = ResponseModelFactory.getViewResponse(viewName, viewId);
		final OntologyClassListResponse ontologyClassListResponse = ResponseModelFactory.getOntologyClassListResponse();
		
		for (int i = 0; i < view.getClassesCount(); i++) {
			final OntologyClass ontologyClass = view.getOntologyClassAt(i);
			final OntologyClassResponse ontologyClassResponse = fromOntologyClass(ontologyClass);
			ontologyClassListResponse.addOntologyClassToList(ontologyClassResponse);
		}
		
		viewResponse.setOntologyClassList(ontologyClassListResponse);
		return viewResponse;
	}
}