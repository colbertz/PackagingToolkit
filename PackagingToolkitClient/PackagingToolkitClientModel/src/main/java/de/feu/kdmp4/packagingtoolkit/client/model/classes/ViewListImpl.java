package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ViewList;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

/**
 * Encapsulates a list with views.
 * @author Christopher Olbertz
 *
 */
public class ViewListImpl extends AbstractList 
							 implements ViewList { 
	
	/**
	 * Is needed because of PrimeFaces.
	 */
	public ViewListImpl() {
	}
	
	/**
	 * Adds a view to the list.
	 * @param view The view that should be added to the list.
	 */
	@Override
	public void addViewToList(final View view) {
		super.add(view);
	}
	
	/**
	 * Gets a view at a given index.
	 * @param index The index of the view that should be determined.
	 * @return The found view.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	@Override
	public View getViewAt(final int index) {
		View view = null;
		view = (View)super.getElement(index);
		return view;
	}
	
	@Override 
	public View getViewByName(final String viewName) {
		for (int i = 0; i < getSize(); i++) {
			View view = getViewAt(i);
			if (view.getViewname().equals(viewName)) {
				return view;
			}
		}
		
		return null;
	}
	
	/**
	 * Counts the views in the list.
	 * @return The number of the views in the list.
	 */
	@Override
	public int getViewCount() {
		return super.getSize();
	}
	
	/**
	 * Removes an view from the list. 
	 * @param view The view that should be removed.
	 * @throws ViewMayNotBeNullException Is thrown if the view is null.
	 */
	@Override
	public void removeView(final View view) {
		super.remove(view);
	}
}
