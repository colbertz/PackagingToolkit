package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyElementImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyPropertyException;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * This class is the implementation of an ontology property. It is an abstract class 
 * and has to be implemented for each type of property, e.g. a boolean property or
 *  an integer property.
 * @author Christopher Olbertz
 *
 */  
public abstract class OntologyPropertyImpl extends OntologyElementImpl implements OntologyProperty {
	// ************* Constants ***********
	private static final long serialVersionUID = 8440424325812921650L;
	private static final String CONSTRUCTOR_STRING_OBJECT = "OntologyPropertyImpl(String label, Object value)";
	protected static final String DECIMAL_SEPARATOR_DE = ",";
	protected static final String DECIMAL_SEPARATOR_EN = ".";
	// ************* Attributes **********
	/**
	 * The label that is shown in the gui.
	 */
	private String label;
	/**
	 * Contains the identifier of this property from the ontology. If the property does
	 * not have a label, propertyId is shown in the gui as output text.
	 */
	private String propertyId;
	/**
	 * The range of this property as defined in the ontology. Is needed for cloning
	 * this property. The cloning is easier if the range can be used. 
	 */
	private String propertyRange;
	/**
	 * The value of the property. The subclasses control the range of this attribute.
	 */
	private Object value;
	@Autowired
	protected PropertyFactory propertyFactory;
	
	private Uuid uuid;
	/* TODO
	 *  Namespace ermitteln. Generell steht er als Praefix vor dem lokalen Namen. Kein Praefix oder Namespace-Angabe
	 *  heisst, dass der Standard-Namespace verwendet wird. Implementiert werden muss also noch eine Moeglichkeit,
	 *  die Namespaces zu ermitteln und die Praefixe den Namensraeumen zuzuordnen. 
	 */
	private String namespace;

	// ********* Constructors ***********
	public OntologyPropertyImpl() {
		super();
		uuid = new Uuid();
	}
	
	public OntologyPropertyImpl(String propertyId, Object value,  
			String label, String range) {
		this();
		if (StringValidator.isNullOrEmpty(propertyId)) {
			OntologyPropertyException exception = OntologyPropertyException.
					propertyIdMayNotBeNull(this.getClass().getName(), 
							CONSTRUCTOR_STRING_OBJECT);
			throw exception;
		}
		this.propertyId = propertyId;
		if (StringValidator.isNullOrEmpty(label)) {
			this.label = StringUtils.extractLocalNameFromIri(propertyId);
		} else {
			this.label = StringUtils.extractLocalNameFromIri(label);
		}
		
		this.propertyRange = range;
		this.value = value;
	}

	// ********* Public methods *********
	@Override
	public boolean isHasTaxonomyProperty() {
		return false;
	}

	
	@Override
	public boolean hasValue() {
		if (value != null) {
			return true;
		} else {
			return false;
		}
	}
	
	protected Object getValue() {
		return value;
	}
	
	protected void setValue(Object value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((propertyId == null) ? 0 : propertyId.hashCode());
		result = prime * result + ((propertyRange == null) ? 0 : propertyRange.hashCode());
		//result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyPropertyImpl other = (OntologyPropertyImpl) obj;
		if (propertyId == null) {
			if (other.propertyId != null)
				return false;
		} else if (!propertyId.equals(other.propertyId))
			return false;
		if (propertyRange == null) {
			if (other.propertyRange != null)
				return false;
		} else if (!propertyRange.equals(other.propertyRange))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return propertyId + ": " + value.toString();
	}
	
	// ******* Getters and setters *********
	public static String getConstructorStringObject() {
		return CONSTRUCTOR_STRING_OBJECT;
	}

	@Override
	public String getPropertyId() {
		return propertyId;
	}
	
	public abstract Object getPropertyValue();
	
	@Override
	public String getLabel() {
		return label;
	}
	
	@Override
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public boolean isObjectProperty() {
		return false;
	}

	@Override
	public String getPropertyRange() {
		return propertyRange;
	}
	
	@Override
	public Uuid getUuid() {
		return uuid;
	}
	
	@Override
	public String getNamespace() {
		return namespace;
	}

	@Override
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	
	@Override
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	@Override
	public void setUuid(Uuid uuid) {
		this.uuid = uuid;
	}
}