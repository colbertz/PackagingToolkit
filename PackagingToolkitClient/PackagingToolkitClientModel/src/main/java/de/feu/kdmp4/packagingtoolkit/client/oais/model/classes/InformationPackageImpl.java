package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackage;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * An abstract base class of information packages. It does not contains any 
 * digital objects because there is a difference betweens SIPs and SIUs 
 * related to the digital objects. 
 * @author Christopher Olbertz
 *
 */
public abstract class InformationPackageImpl extends BasicInformationPackageImpl 
											 implements InformationPackage {
	
	public InformationPackageImpl(String title) {
		super(title);
	}
	
	public InformationPackageImpl(String title, Uuid uuid) {
		super(title, uuid);
	}
}
