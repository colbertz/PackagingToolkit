package de.feu.kdmp4.packagingtoolkit.client.factories;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.classes.OntologyIndividualsAndTheirUuidHashMap;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.OntologyIndividualsAndTheirUuidMap;

/**
 * Creates object with classes that implement the interfaces for 
 * data structures.
 * @author Christopher Olbertz
 *
 */
public class DataStructureFactory {
	/**
	 * Creates an object that contains individuals in a map. The key of the map are the uuids
	 * of the individuals.
	 * @return An implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.OntologyIndividualsAndTheirUuidMap}.
 	 */
	public static final OntologyIndividualsAndTheirUuidMap createOntologyIndividualsAndTheirUuidHashMap() {
		return new OntologyIndividualsAndTheirUuidHashMap();
	}
}
