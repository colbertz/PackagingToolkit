package de.feu.kdmp4.packagingtoolkit.client.ontology.factories;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyBooleanPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyBytePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyDatePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyDateTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyDoublePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyDurationPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyFloatPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyLongPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyShortPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyStringPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyTimePropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatype;
import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * A factory for ontology properties.
 * @author Christopher Olbertz
 *
 */
public class PropertyFactory extends AbstractFactory {
	/**
	 * The factory that builds ontology model objects.
	 */
	private OntologyModelFactory ontologyModelFactory;
	
	public PropertyFactory(OntologyModelFactory ontologyModelFactory) {
		this.ontologyModelFactory = ontologyModelFactory;
	}
	
	public OntologyObjectProperty createHasTaxonomyProperty() {
		final String propertyId = "http://www.KDMP4.eu/ontologies/KDMP4-OAIS-IP#hasTaxonomie";
		final String label = "hasTaxonomy";
		final OntologyObjectProperty hasTaxonomyProperty = new OntologyObjectPropertyImpl(propertyId, label, null, null);
		return hasTaxonomyProperty;
	}

/*	public OntologyDatatypeProperty cloneDataTypeProperty(OntologyDatatypeProperty ontologyProperty) {
		String propertyRange = ontologyProperty.getPropertyRange();
		String propertyId = ontologyProperty.getPropertyId();
		String label = ontologyProperty.getLabel();
		OntologyDatatypeProperty datatypeProperty = getProperty(propertyRange, propertyId, label);
		
		return datatypeProperty;
	}*/
	
	/**
	 * Initializes the properties of the object after the wired properties have been injected by Spring.
	 * It is only an empty stub at the moment.
	 */
	@PostConstruct
	public void initialize() {
		System.out.println("Bla");
	}
	
	/**
	 * Clones an ontology property and write its values in a new ontology property object.
	 * @param ontologyProperty The ontology property that has be cloned.
	 * @return A new ontology property object that is identical to the parameter object.
	 */
	/*public OntologyProperty cloneOntologyProperty(OntologyProperty originalProperty) {
		String propertyRange = originalProperty.getPropertyRange();
		String label = originalProperty.getLabel();
		String propertyId = originalProperty.getPropertyId();
		//Object value = originalProperty.getPropertyValue();
		OntologyProperty copy = getProperty(propertyRange, propertyId, label);
		//Object clonedValue = value.
		super.initBean(copy);
		//copy.setPropertyValue(value);
		return copy;
	}*/
	
	public OntologyDatatypeProperty copyOntologyDatatypeProperty(OntologyDatatypeProperty ontologyProperty) {
		String propertyIri = ontologyProperty.getPropertyId();
		String label = ontologyProperty.getLabel();
		//if (ontologyProperty.getPropertyValue() != null) {
			Object value = ontologyProperty.getPropertyValue();
			String propertyRange = ontologyProperty.getPropertyRange();
			OntologyDatatypeProperty datatypeProperty = null;
			
			if (ontologyProperty.isBooleanProperty()) {
				datatypeProperty = new OntologyBooleanPropertyImpl(propertyIri, label, propertyRange);
			} else if (ontologyProperty.isByteProperty()) {
				OntologyByteProperty ontologyByteProperty = (OntologyByteProperty)ontologyProperty;
				datatypeProperty = new OntologyBytePropertyImpl(propertyIri, ontologyByteProperty.isUnsigned(), label, propertyRange);
			} else if (ontologyProperty.isDateProperty()) {
				datatypeProperty = new OntologyDatePropertyImpl(propertyIri, label, propertyRange);
			} else if (ontologyProperty.isDateTimeProperty()) {
				datatypeProperty = new OntologyDateTimePropertyImpl(propertyIri, label, propertyRange);
			} else if (ontologyProperty.isDoubleProperty()) {
				datatypeProperty = new OntologyDoublePropertyImpl(propertyIri, label, propertyRange);
			} else if (ontologyProperty.isDurationProperty()) {
				datatypeProperty = new OntologyDurationPropertyImpl(propertyIri, label, propertyRange);
			} else if (ontologyProperty.isFloatProperty()) {
				datatypeProperty = new OntologyFloatPropertyImpl(propertyIri, label, propertyRange);
			} else if (ontologyProperty.isIntegerProperty()) {
				OntologyIntegerProperty ontologyIntegerProperty = (OntologyIntegerProperty)ontologyProperty;
				datatypeProperty = new OntologyIntegerPropertyImpl(propertyIri, ontologyIntegerProperty.isUnsigned(), 
						ontologyIntegerProperty.getSign(), ontologyIntegerProperty.isInclusiveZero(), label, propertyRange);
			} else if (ontologyProperty.isLongProperty()) {
				OntologyLongProperty ontologyLongProperty = (OntologyLongProperty)ontologyProperty;
				datatypeProperty = new OntologyLongPropertyImpl(propertyIri, ontologyLongProperty.isUnsigned(), label, propertyRange);
			} else if (ontologyProperty.isShortProperty()) {
				OntologyShortProperty ontologyShortProperty = (OntologyShortProperty)ontologyProperty;
				datatypeProperty = new OntologyShortPropertyImpl(propertyIri, ontologyShortProperty.isUnsigned(), label, propertyRange);
			} else if (ontologyProperty.isStringProperty()) {
				datatypeProperty = new OntologyStringPropertyImpl(propertyIri, label, propertyRange);
			} else  {
				datatypeProperty = new OntologyTimePropertyImpl(propertyIri, label, propertyRange);
			}
			
			datatypeProperty.setPropertyValue(value);
			Uuid uuid = ClientModelFactory.createUuid(ontologyProperty.getUuid().toString());
			datatypeProperty.setUuid(uuid);
			return datatypeProperty;
		/*} else {
			 if (ontologyProperty.isDateProperty()) {
				return createDateProperty(propertyIri, label, null);
			} else if (ontologyProperty.isDateTimeProperty()) {
				return createDateTimeProperty(propertyIri, label, null);
			}  else {
				return  createTimeProperty(propertyIri, label, null);
			}
		}*/
	}
	
	public OntologyObjectProperty copyOntologyObjectProperty(OntologyObjectProperty originalProperty) {
		OntologyObjectProperty copyOfProperty = new OntologyObjectPropertyImpl(originalProperty.getPropertyId(), 
				originalProperty.getLabel(), originalProperty.getRange(), originalProperty.getPropertyRange());
		
		return copyOfProperty;
	}
	
	/**
	 * Creates an datatype property out of an response object that has been send from the client. The object is registered with Spring.
	 * @param ontologyDatatype The response object.
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property. 
	 * @param unsigned True if the property is unsigned, false otherwise. It can be false if there is no
	 * signed or unsigned with this datatype.
	 * @param sign The sign of the property.
	 * @param inclusiveZero True if the property range contains zero, false otherwise.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created ontology property object.
	 */
	private OntologyProperty getDatatypeProperty(OntologyDatatypeResponse ontologyDatatype, 
			String propertyId, String label, Object value, boolean unsigned, Sign sign,
			boolean inclusiveZero, String propertyRange) {
		String valueAsString = getValueAsString(value);
		OntologyDatatypeProperty ontologyDatatypeProperty = null;
		
		switch(ontologyDatatype) {
		case BOOLEAN:			
			ontologyDatatypeProperty = createBooleanProperty(propertyId, label, valueAsString, propertyRange);
			break;
		case BYTE:
			ontologyDatatypeProperty = createByteProperty(propertyRange, propertyId, label, valueAsString);
			break;
		case DATE:
			ontologyDatatypeProperty = createDateProperty(propertyId, label, valueAsString);
			break;
		case DATETIME:
			ontologyDatatypeProperty = createDateTimeProperty(propertyId, label, valueAsString);
			break;
		case DOUBLE:
			ontologyDatatypeProperty = createDoubleProperty(propertyId, label, valueAsString, propertyRange);
			break;
		case DURATION:
			ontologyDatatypeProperty = createDurationProperty(propertyId, label, valueAsString, propertyRange);
			break;
		case FLOAT: 
			ontologyDatatypeProperty = createFloatProperty(propertyRange, propertyId, label, valueAsString);
			break;
		case INTEGER:
			ontologyDatatypeProperty = createIntegerProperty(propertyRange, propertyId, label, unsigned, sign, inclusiveZero, valueAsString);
			break;
		case LONG:
			ontologyDatatypeProperty = createLongProperty(propertyId, label, valueAsString, unsigned, propertyRange);
			break;
		case SHORT:
			ontologyDatatypeProperty = createShortProperty(propertyId, label, valueAsString, unsigned, propertyRange);
			break;
		case STRING:
			ontologyDatatypeProperty = createStringProperty(propertyId, label, valueAsString, propertyRange);
			break;
		case TIME:
			ontologyDatatypeProperty = createTimeProperty(propertyId, label, valueAsString);
			break;
		} 
		//super.initBean(ontologyDatatypeProperty);
		
		return ontologyDatatypeProperty;
	}
	
	/**
	 * Creates an object property object.
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created object property object.
	 */
	/*public OntologyObjectProperty getObjectProperty(String propertyId, String label, 
			String propertyRange/*, OntologyClassesMap ontologyClassesMap) {
		OntologyClass ontologyClass = ontologyCache.getOntologyClassByPropertyRange(propertyRange);
		OntologyObjectProperty ontologyObjectProperty = null;
		
		if (ontologyClass != null) {
			ontologyObjectProperty = new OntologyObjectPropertyImpl(propertyId, label, ontologyClass, propertyRange);			
		}
		return ontologyObjectProperty;
		}
	}*/
	
	/**
	 * Creates a new ontology property object out of an response object that has been send by the client.
	 * @param propertyResponse The response object.
	 * @return The newly created ontology property object.
	 */
	public OntologyProperty getProperty(final DatatypePropertyResponse propertyResponse) {
		OntologyDatatypeResponse datatype = propertyResponse.getDatatype();
		OntologyProperty ontologyProperty = null;
		String propertyId = propertyResponse.getPropertyName();
		String label = propertyResponse.getLabel();
		label = StringUtils.extractLocalNameFromIri(label);
		
		Sign sign = propertyResponse.getSign();
		boolean unsigned = propertyResponse.isUnsigned();
		boolean inclusiveZero = propertyResponse.isInclusiveZero();
		String propertyRange = "";
		String value = propertyResponse.getValue();

		ontologyProperty = getDatatypeProperty(datatype, propertyId, label, value, unsigned, sign, inclusiveZero, propertyRange);
		return ontologyProperty;
	}

	/**
	 * Converts the value of a property into a String.
	 * @param value The value.
	 * @return The value as String.
	 */
	private String getValueAsString(Object value) {
		String valueAsString = "";
		if (value != null) {
			valueAsString = value.toString();
		}
		
		return valueAsString;
	}
	
	/**
	 * Creates a new property object of the datatype boolean. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype boolean.
	 */
	private OntologyBooleanProperty createBooleanProperty(final String propertyId,
			   final String label, final String value, final String propertyRange) {
		OntologyBooleanProperty ontologyProperty = new OntologyBooleanPropertyImpl(propertyId, label, propertyRange);
		if (StringValidator.isNotNullOrEmpty(value)) {
			Boolean booleanValue = Boolean.valueOf(value);
			ontologyProperty.setPropertyValue(booleanValue);
		}
		
		return ontologyProperty;
	}

	/**
	 * Creates a new property object of the datatype byte. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype byte.
	 */
	private OntologyByteProperty createByteProperty(final String propertyRange, final String propertyId,
			   final String label, final String value) {
		OntologyByteProperty ontologyProperty = null; //new OntologyBytePropertyImpl(propertyId, unsigned, label);
		
		if (OntologyDatatype.isUnsignedByte(propertyRange)) {
			ontologyProperty = OntologyByteProperty.createUnsigned(propertyId, label, propertyRange);
		} else {
			ontologyProperty = OntologyByteProperty.createSigned(propertyId, label, propertyRange);
		}
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			Short byteValue = Short.valueOf(value);
			ontologyProperty.setPropertyValue(byteValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype date. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype date.
	 */
	private OntologyDateProperty createDateProperty(final String propertyId,
			   final String label,
			   final String value) {
		OntologyDateProperty ontologyProperty = new OntologyDatePropertyImpl(propertyId, label);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			LocalDate dateValue = null;
			try {
				dateValue = DateTimeUtils.getOntologyStringAsDate(value);
			} catch (DateTimeParseException ex) {
				dateValue = DateTimeUtils.getValueAsDate(value);
			}
			
			ontologyProperty.setPropertyValue(dateValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype double. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype double.
	 */
	private OntologyDoubleProperty createDoubleProperty(final String propertyId,
			   final String label,
			   final String value, final String propertyRange) {
		OntologyDoubleProperty ontologyProperty = new OntologyDoublePropertyImpl(propertyId, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			Double doubleValue = Double.valueOf(value);
			ontologyProperty.setPropertyValue(doubleValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype datetime. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype datetime.
	 */
	private OntologyDateTimeProperty createDateTimeProperty(final String propertyId,
			   final String label,
			   final String value) {
		OntologyDateTimeProperty ontologyProperty = new OntologyDateTimePropertyImpl(propertyId, label);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			LocalDateTime dateTimeValue = null;
			try {
				dateTimeValue = DateTimeUtils.getOntologyStringAsDateTime(value);
			} catch (DateTimeParseException ex) {
				dateTimeValue = DateTimeUtils.getValueAsDateTime(value);
			}
			ontologyProperty.setPropertyValue(dateTimeValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype duration. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype duration.
	 */
	private OntologyDatatypeProperty createDurationProperty(final String propertyId, final  String label, final String value,
			final String propertyRange) {
		OntologyDurationProperty ontologyProperty = new OntologyDurationPropertyImpl(propertyId, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			OntologyDuration durationValue = OntologyDuration.fromString(value);
			ontologyProperty.setPropertyValue(durationValue);
		}
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype float. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype float.
	 */
	private OntologyFloatProperty createFloatProperty(final String propertyRange, 
			   final String propertyId,
			   final String label,
			   final String value) {
		OntologyFloatProperty ontologyProperty = new OntologyFloatPropertyImpl(propertyId, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			Float floatValue = Float.valueOf(value);
			ontologyProperty.setPropertyValue(floatValue);
		}
		//ontologyProperty.addMaxInclusiveRestriction(Float.MAX_VALUE);
		//ontologyProperty.addMinInclusiveRestriction(Float.MIN_VALUE);
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype integer. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype integer.
	 */
	private OntologyIntegerProperty createIntegerProperty(final String propertyRange, 
			   final String propertyId,
			   final String label,
			   final boolean unsigned,
			   final Sign sign,
			   final boolean inclusiveZero,
			   final String value) {
		OntologyIntegerProperty ontologyProperty = null;
		
		if (OntologyDatatype.isNonNegativeInteger(propertyRange)) {
			ontologyProperty = ontologyModelFactory.createOntologyIntegerProperty(propertyId, label, 
					true, Sign.PLUS_SIGN, true, propertyRange);
		} else if (OntologyDatatype.isNegativeInteger(propertyRange)) {
			ontologyProperty = ontologyModelFactory.createOntologyIntegerProperty(propertyId, label, 
					false, Sign.MINUS_SIGN, false, propertyRange);
		} else if (OntologyDatatype.isNonPositiveInteger(propertyRange)) {
			ontologyProperty = ontologyModelFactory.createOntologyIntegerProperty(propertyId, label, 
					false, Sign.MINUS_SIGN, true, propertyRange);
		} else if (OntologyDatatype.isPositiveInteger(propertyRange)) {
			ontologyProperty = ontologyModelFactory.createOntologyIntegerProperty(propertyId, label, 
					false, Sign.PLUS_SIGN, false, propertyRange);
		} else if (OntologyDatatype.isUnsignedInt(propertyRange)) {
			ontologyProperty = ontologyModelFactory.createOntologyIntegerProperty(propertyId, label, 
					true, Sign.MINUS_SIGN, false, propertyRange);
		} else {
			ontologyProperty = ontologyModelFactory.createOntologyIntegerProperty(propertyId, label, 
					false, Sign.BOTH, false, propertyRange);
		}
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			Long integerValue = Long.valueOf(value);
			ontologyProperty.setPropertyValue(integerValue);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype long. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype long.
	 */
	private OntologyDatatypeProperty createLongProperty(final String propertyId, final String label, 
			final String value, final boolean unsigned, final String propertyRange) {
		OntologyLongProperty ontologyProperty = new OntologyLongPropertyImpl(propertyId, unsigned, label, propertyRange);
		
		if (StringValidator.isNotNullOrEmpty(value)) {
			BigInteger longValue = new BigInteger(value);
			ontologyProperty.setPropertyValue(longValue);
		}
		return ontologyProperty;
	}
	
	/*public static OntologyProperty getObjectProperty(final String propertyId, final String label, 
			final String propertyRange, final OntologyClass range) {
		
		OntologyProperty ontologyProperty = new OntologyObjectPropertyImpl(propertyId, label, range, propertyRange);
		return ontologyProperty;
	}*/
	
	/**
	 * Creates a new property object of the datatype short. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype short.
	 */
	private OntologyDatatypeProperty createShortProperty(final String propertyId, final String label, 
			final String value, final boolean unsigned, final String propertyRange) {
		OntologyShortProperty ontologyProperty = new OntologyShortPropertyImpl(propertyId, unsigned, label, propertyRange);
		if (StringValidator.isNotNullOrEmpty(value)) {
			Integer shortValue = Integer.valueOf(value);
			ontologyProperty.setPropertyValue(shortValue);
		}
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype string. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype string.
	 */
	private static OntologyDatatypeProperty createStringProperty(final String propertyId, 
			final String label, 
			final String value, 
			final String propertyRange) {
		OntologyStringProperty ontologyProperty = new OntologyStringPropertyImpl(propertyId, label, propertyRange);
		ontologyProperty.setPropertyValue(value);
		return ontologyProperty;
	}
	
	/**
	 * Creates a new property object of the datatype time. 
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param value The value of the property.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created property with the datatype time.
	 */
	private OntologyDatatypeProperty createTimeProperty(final String propertyId, final String label, 
			final String value) {
		OntologyTimeProperty ontologyProperty = new OntologyTimePropertyImpl(propertyId, label);
		if (StringValidator.isNotNullOrEmpty(value)) {
			LocalTime timeValue = DateTimeUtils.getValueAsTime(value);
			ontologyProperty.setPropertyValue(timeValue);
		}
		return ontologyProperty;
	}

	/**
	 * Creates a datatype property object. The object is registered with Spring.
	 * @param propertyId The id of the property that has been defined in the ontology.
	 * @param label The label of the property that has been defined in the ontology.
	 * @param propertyRange The property range that has been defined in the ontology.
	 * @return The newly created datatype property object.
	 */
	public OntologyDatatypeProperty getProperty(final String propertyRange, 
											   final String propertyId,
											   final String label) {		
		boolean unsigned = OntologyDatatype.isUnsigned(propertyRange);
		OntologyDatatypeProperty ontologyDatatypeProperty = null;
		
		if (OntologyDatatype.isBoolean(propertyRange)) {
			ontologyDatatypeProperty = new OntologyBooleanPropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isByte(propertyRange)) {
			ontologyDatatypeProperty = createByteProperty(propertyRange, propertyId, label, null);
		} else if (OntologyDatatype.isDateTime(propertyRange)) {
			ontologyDatatypeProperty = new OntologyDateTimePropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isDate(propertyRange)) {
			ontologyDatatypeProperty = new OntologyDatePropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isDouble(propertyRange)) {
			ontologyDatatypeProperty = new OntologyDoublePropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isDuration(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyDurationPropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isFloat(propertyRange)) {
			ontologyDatatypeProperty =  createFloatProperty(propertyRange, propertyId, label, null);
		} else if (OntologyDatatype.isInteger(propertyRange)) {
			ontologyDatatypeProperty =  createIntegerProperty(propertyRange, propertyId, label, unsigned, Sign.BOTH, true, null);
		} else if (OntologyDatatype.isLong(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyLongPropertyImpl(propertyId, unsigned, label, propertyRange);
		} else if (OntologyDatatype.isShort(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyShortPropertyImpl(propertyId, unsigned, label, propertyRange);
		} else if (OntologyDatatype.isString(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyStringPropertyImpl(propertyId, label, propertyRange);
		} else if (OntologyDatatype.isTime(propertyRange)) {
			ontologyDatatypeProperty =  new OntologyTimePropertyImpl(propertyId, label, propertyRange);
		}  else {
			ontologyDatatypeProperty =  new OntologyStringPropertyImpl(propertyId, label, propertyRange);
		}
		if (applicationContext != null) {
			super.initBean(ontologyDatatypeProperty);
		}
		return ontologyDatatypeProperty;
	}
}
