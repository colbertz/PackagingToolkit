package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.Restriction;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.RestrictionCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

public class RestrictionCollectionImpl extends AbstractList implements RestrictionCollection {
	@Override
	public Restriction getRestriction(int index) {
		return (Restriction)super.getElement(index);
	}
	
	@Override
	public void addRestriction(Restriction restriction) {
		super.add(restriction);
	}

	@Override
	public int getRestrictionsCount() {
		return super.getSize();
	}
}
