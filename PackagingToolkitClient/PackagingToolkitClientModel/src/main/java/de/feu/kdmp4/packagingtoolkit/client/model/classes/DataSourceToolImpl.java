package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceTool;

public class DataSourceToolImpl implements Comparable<DataSourceToolImpl>, DataSourceTool {
	/**
	 * True if the tool is configured as activated, false otherwise.
	 */
	private boolean activated;
	/**
	 * The name of the tool.
	 */
	private String toolName;	

	/**
	 * Constructs a tool with the name toolname. As default the tool is activated.
	 * @param toolName The name of the tool. May not be null or empty.
	 */
	public DataSourceToolImpl(final String toolName) {
		this.toolName = toolName;
		activated = true;
	}
	
	/**
	 * Constructs a tool with the name toolname. Aa default, FITS is used as extractor.
	 * @param toolName The name of the tool. May not be null or empty.
	 * @param activated True if the tool is activated for use, false otherwise.
	 */
	public DataSourceToolImpl(final String toolName, final boolean activated) {
		this.toolName = toolName;
		this.activated = activated;
	}
	
	@Override
	public int compareTo(DataSourceToolImpl dataSourceTool) {
		return this.toolName.compareTo(dataSourceTool.getToolName());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activated ? 1231 : 1237);
		result = prime * result + ((toolName == null) ? 0 : toolName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSourceToolImpl other = (DataSourceToolImpl) obj;
		if (activated != other.activated)
			return false;
		if (toolName == null) {
			if (other.toolName != null)
				return false;
		} else if (!toolName.equals(other.toolName))
			return false;
		return true;
	}
	
	@Override
	public String getToolName() {
		return toolName;
	}
	
	@Override
	public boolean isActivated() {
		return activated;
	}

	@Override
	public void activateTool() {
		this.activated = true;
	}
	
	@Override
	public void deactivateTool() {
		this.activated = false;
	}
	
	@Override
	public String toString() {
		return toolName;
	}
}