package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.BasicInformationPackage;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.SubmissionInformationPackage;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.SubmissionInformationUnit;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.OaisEntityImpl;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * An abstract base class for information packages with the elements that all 
 * information packages have. 
 * @author Christopher Olbertz
 *
 */
public abstract class BasicInformationPackageImpl extends OaisEntityImpl 
												  implements BasicInformationPackage {
	
	/**
	 * The title of this information package.
	 */
	private String title;
	
	/**
	 * A constructor that does not get an uuid for the information package. The
	 * uuid is created automatically.
	 * @param title The title of the information package.
	 */
	public BasicInformationPackageImpl(final String title) {
		this(title, null);
	}

	/**
	 * Constructs an information package.
	 * @param title The title of the information package. May not be empty.
	 * @param uuid The uuid of the information package. May not be null.
	 * @throws InformationPackageTitleMayNotBeNullException Is thrown if the title
	 * parameter is empty.
	 */
	public BasicInformationPackageImpl(final String title, final Uuid uuid) {
		super(uuid);
		if (StringValidator.isNotNullOrEmpty(title)) {
			this.title = title;
		} 
	}
	
	/**
	 * Creates an object of the type {@link de.feu.kdmp4.packagingtoolkit.response.PackageResponse} out of the information package.
	 * @return The created PackageResponse object.
	 */
	@Override
	public PackageResponse toPackageResponse() {
		PackageType packageType = null;
		
		if (this instanceof SubmissionInformationPackage) {
			packageType = PackageType.SIP;
		} else if (this instanceof SubmissionInformationUnit) {
			packageType = PackageType.SIU;
		} 
		
		final UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(getUuid());
		
		final PackageResponse packageOutput = new PackageResponse(packageType.toString(), 
				uuidResponse, title);
		
		return packageOutput;
	}
	
	/**
	 * Returns the type of the information package. Every subclass has to override this
	 * method and to return its package type. This method is used if querying the hierarchy
	 * with instance of is not suitable. For example, this method is used for creation of the names
	 * of the zip files. 
	 * @return The type of the information package.
	 */
	@Override
	public abstract PackageType getPackageType();
	
	@Override
	public String getTitle() {
		return title;
	}
}
