package de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
 
public interface BasicInformationPackage { 
	String getTitle();
	PackageResponse toPackageResponse();
	PackageType getPackageType();
	void addDigitalObject(final DigitalObject digitalObject);
	Uuid getUuid();
	void setUuid(final Uuid uuid);
}
