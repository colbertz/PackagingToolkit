package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface ObjectPropertyCollection {
	void addObjectProperty(OntologyObjectProperty property);
	OntologyObjectProperty getObjectProperty(int index);
	OntologyObjectProperty getObjectProperty(Uuid uuid);
	int getPropertiesCount();
	boolean isEmpty();
	boolean containsObjectProperty(OntologyObjectProperty ontologyObjectProperty);
	void removeObjectProperty(OntologyObjectProperty ontologyObjectProperty);
	OntologyObjectProperty getObjectProperty(String iri); 
}
