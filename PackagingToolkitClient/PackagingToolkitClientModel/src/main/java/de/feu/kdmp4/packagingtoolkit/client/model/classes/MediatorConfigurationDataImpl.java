package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MediatorConfigurationData;
import de.feu.kdmp4.packagingtoolkit.response.ExtractorToolListResponse;

public class MediatorConfigurationDataImpl implements MediatorConfigurationData {
	private ExtractorToolListResponse extractorToolList;

	public MediatorConfigurationDataImpl(ExtractorToolListResponse extractorToolList) {
		this.extractorToolList = extractorToolList;
	}
	
	@Override
	public String getExtractorToolNameAt(int index) {
		return extractorToolList.getExtractorToolAt(index).getToolName();
	}
	
	@Override
	public boolean getExtractorToolActivatedAt(int index) {
		return extractorToolList.getExtractorToolAt(index).isActivated();
	}
	
	@Override
	public boolean getFitsToolAt(int index) {
		return false;
	}
	
	@Override
	public int getExtractorToolsCount() {
		return extractorToolList.getExtractorToolCount();
	}
}