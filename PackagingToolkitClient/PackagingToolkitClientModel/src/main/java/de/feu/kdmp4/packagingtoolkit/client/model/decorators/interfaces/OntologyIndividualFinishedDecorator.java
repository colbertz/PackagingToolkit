package de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;

/**
 * This is a decorator for ontology individuals. It extends individuals by a flag that indicates that the 
 * creation process of an individual has been finished. If theis flag is true, then there are no more values
 * changed in the individual.
 * @author Christopher Olbertz
 *
 */
public interface OntologyIndividualFinishedDecorator {
	/**
	 * Finishes the creation process of the decorated individual. It is only possible to finish the process and not to
	 * start it again.
	 */
	void finishIndividualCreation();
	/**
	 * Returns the individual that is decorated. The individual can only be set in the constructor of the implementing
	 * class.
	 * @return The decorated individual.
	 */
	OntologyIndividual getOntologyIndividual();
	/**
	 * Checks if the creation process of the individual has been finished.
	 * @return True if the creation process of the individual has been finished, false otherwise.
	 */
	boolean isFinished();
	/**
	 * Adds a datatype property to the decorated individual.
	 * @param ontologyDatatypeProperty The datatype property that should be added.
	 */
	void addDatatypeProperty(OntologyDatatypeProperty ontologyDatatypeProperty);
}
