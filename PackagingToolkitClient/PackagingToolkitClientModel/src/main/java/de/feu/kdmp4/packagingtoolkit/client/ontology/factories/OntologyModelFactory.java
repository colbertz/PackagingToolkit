package de.feu.kdmp4.packagingtoolkit.client.ontology.factories;

import de.feu.kdmp4.packagingtoolkit.client.model.classes.OntologyClassListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyIndividualListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.InformationPackagePathImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackagePath;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyIntegerPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.models.classes.OntologyDurationImpl;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;

/**
 * Creates the objects of the model classes for ontology elements.
 * @author Christopher Olbertz
 *
 */
@SuppressWarnings("rawtypes")
public class OntologyModelFactory extends AbstractFactory {
	private PropertyFactory propertyFactory;
	//private int ontologyClassCounter;
	
	public OntologyModelFactory() {
		//ontologyClassCounter = 0;
	}
	
	/**
	 * Creates a new object of an implementation of 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackagePath}.
	 * @return The newly created object.
	 */
	public static InformationPackagePath createInformationPackagePath() {
		return new InformationPackagePathImpl();
	}
	
	/**
	 * Creates an empty collection that can contain ontology classes.
	 * @return The newly created object.
	 */
	public static OntologyClassList createEmptyOntologyClassCollection() {
		return new OntologyClassListImpl();
	}
	
	/**
	 * Creates an empty ontology class. 
	 * @param namespace The namespace of the class.
	 * @param localname The name of the class in the ontology without the namespace.
	 * @return The new ontology class with the given namespace and the given local name.
	 */
	public OntologyClass createOntologyClass(final String namespace, 
														  final String localname) {
		OntologyClass ontologyClass = new OntologyClassImpl(namespace, localname);
		return ontologyClass;
	}
	
	public OntologyDuration createOntologyDuration(int years, int months, int days,
			int hours, int minutes, int seconds) {
		return new OntologyDurationImpl(days, hours, minutes, months, seconds, years);
	}
	
	public static final OntologyDuration createOntologyDuration(int years, int months, int days,
			int hours, int minutes, int seconds, boolean negative) {
		return new OntologyDurationImpl(days, hours, minutes, months, seconds, years, negative);
	}
	
	public OntologyIntegerProperty createOntologyIntegerProperty(String propertyId, 
			String label, boolean unsigned, Sign sign, boolean inclusiveZero, String propertyRange) {
		return new OntologyIntegerPropertyImpl(propertyId, unsigned, sign, inclusiveZero, label, propertyRange);
	}
	
	public OntologyIndividual createOntologyIndividual(OntologyClass ontologyClass) {
		OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass);
		return ontologyIndividual; 
	}
	
	public OntologyIndividualCollection createOntologyIndividualList() {
		return new OntologyIndividualCollectionImpl();
	}
	
	public OntologyClass createOntologyClass(String nameWithNamespace) {
		int posSharp = nameWithNamespace.indexOf("#");
		String namespace = nameWithNamespace.substring(0, posSharp);
		String localname = nameWithNamespace.substring(posSharp + 1);
		OntologyClass ontologyClass =  new OntologyClassImpl(namespace, localname);
		
		return ontologyClass;
	}
	
	public DatatypePropertyCollection createPropertyList() {
		DatatypePropertyCollection datatypePropertyList = new DatatypePropertyCollectionImpl();
		return  datatypePropertyList;
	}
	
	public OntologyIndividualCollection createOntologyindividualList(IndividualListResponse individualListResponse) {
		OntologyIndividualCollection ontologyIndividualList = createOntologyIndividualList();
		
		for (int i = 0; i < individualListResponse.getIndividualCount(); i++) {
			IndividualResponse individualResponse = individualListResponse.getIndividualAt(i);
			OntologyIndividual ontologyIndividual = createOntologyIndividual(individualResponse);
			ontologyIndividualList.addIndividual(ontologyIndividual);
		}
		
		return ontologyIndividualList;
	}
	
	public OntologyIndividual createOntologyIndividual(IndividualResponse individualResponse) {
		OntologyIndividual ontologyIndividual = createOntologyIndividual();
		
		 DatatypePropertyListResponse datatypePropertyListResponse =  individualResponse.getOntologyDatatypeProperties();
		 for (int i = 0; i < datatypePropertyListResponse.getOntologyPropertyCount(); i++) {
			 DatatypePropertyResponse datatypePropertyResponse = datatypePropertyListResponse.getOntologyPropertyAt(i);
			 OntologyDatatypeProperty ontologyDatatypeProperty = createOntologyProperty(datatypePropertyResponse);
			 ontologyIndividual.addDatatypeProperty(ontologyDatatypeProperty);
		 }
		
		return ontologyIndividual;
	}
	
	public OntologyDatatypeProperty createOntologyProperty(DatatypePropertyResponse datatypePropertyResponse) {
		return (OntologyDatatypeProperty)propertyFactory.getProperty(datatypePropertyResponse);
	}
	
	private OntologyIndividual createOntologyIndividual() {
		return new OntologyIndividualImpl();
	}
	
	/*public static final PropertyList createPropertyList(DatatypePropertyListResponse propertyListResponse) {
		PropertyList propertyList = new PropertyListImpl();
		
		if (propertyListResponse != null) {
			for (int i = 0; i < propertyListResponse.getOntologyPropertyCount(); i++) {
				OntologyPropertyResponse propertyResponse = propertyListResponse.getOntologyPropertyAt(i);
				OntologyProperty ontologyProperty = createOntologyProperty(propertyResponse);
				propertyList.addProperty(ontologyProperty);
			}
		}
		
		return propertyList;
	}*/
	
	public static final ObjectPropertyCollection createObjectPropertyList() {
		return new ObjectPropertyCollectionImpl();
	}
	
	public static final DatatypePropertyCollection createDatatypePropertyList() {
		return new DatatypePropertyCollectionImpl();
	}

	public PropertyFactory getPropertyFactory() {
		return propertyFactory;
	}

	public void setPropertyFactory(PropertyFactory propertyFactory) {
		this.propertyFactory = propertyFactory;
	}

	public static TaxonomyIndividualCollection createEmptyTaxonomyIndividualCollection() {
		return new TaxonomyIndividualListImpl();
	}
}
