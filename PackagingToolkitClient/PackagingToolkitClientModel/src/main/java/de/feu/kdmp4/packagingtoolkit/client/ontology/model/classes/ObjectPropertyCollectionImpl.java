package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.io.Serializable;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class ObjectPropertyCollectionImpl extends AbstractList implements ObjectPropertyCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;

	@Override
	public void addObjectProperty(OntologyObjectProperty property) {
		//try {
			super.add(property);
		/*} catch (ListElementMayNotBeNullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	@Override
	public boolean containsObjectProperty(final OntologyObjectProperty ontologyObjectProperty) {
		final OntologyObjectProperty foundObjectProperty = getObjectProperty(ontologyObjectProperty.getPropertyId());
		if (foundObjectProperty == null) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public void removeObjectProperty(final OntologyObjectProperty ontologyObjectProperty) {
		super.remove(ontologyObjectProperty);
	}
	
	@Override
	public OntologyObjectProperty getObjectProperty(final int index) {
		return (OntologyObjectProperty)super.getElement(index);
	}
	
	@Override
	public OntologyObjectProperty getObjectProperty(final Uuid uuid) {
		for (int i = 0; i < getPropertiesCount(); i++) {
			final OntologyObjectProperty ontologyObjectProperty = (OntologyObjectProperty)super.getElement(i);
			final Uuid theOtherUuid = ontologyObjectProperty.getUuid();
			if (uuid.equals(theOtherUuid)) {
				return ontologyObjectProperty;
			}
			
		}
		return null;
	}
	
	@Override
	public OntologyObjectProperty getObjectProperty(final String iri) {
		for (int i = 0; i < getPropertiesCount(); i++) {
			final OntologyObjectProperty ontologyObjectProperty = (OntologyObjectProperty)super.getElement(i);
			final String theOtherIri = ontologyObjectProperty.getPropertyId();
			if (iri.equals(theOtherIri)) {
				return ontologyObjectProperty;
			}
			
		}
		return null;
	}
	
	@Override
	public int getPropertiesCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}
}
