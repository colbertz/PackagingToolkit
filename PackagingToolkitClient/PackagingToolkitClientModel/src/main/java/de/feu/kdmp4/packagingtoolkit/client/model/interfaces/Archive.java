package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;


public interface Archive extends Comparable<Archive> {
	/**
	 * Checks if an uuid is equal to the uuid of this archive.
	 * @param uuid The uuid we want to compare with the uuid of this archive.
	 * @return True, if the two uuids are equal, false otherwise.
	 */
	boolean areUuidsEqual(final Uuid uuid);
	/**
	 * Sets the uuid of this archive.
	 * @param uuid The uuid to set.
	 */
	void setUuid(final Uuid uuid);
	/**
	 * Sets the serialization format of this archive. At the moment only OAI-ORE is supported.
	 * @return The serialization format of this archive.
	 */
	SerializationFormat getSerializationFormat();
	/**
	 * Determines the uuid of this archive.
	 * @return The uuid of this archive.
	 */
	Uuid getUuid();
	/**
	 * Determines the title of this archive.
	 * @return The title of this archive.
	 */
	String getTitle();
	/**
	 * Determines the package type of this archive.
	 * @return The package type  of this archive.
	 */
	PackageType getPackageType();
	/**
	 * Determines the date and time this archive was created on the server.
	 * @return The creation date and time of this archive.
	 */
	LocalDateTime getCreationDate();
	/**
	 * Determines the date and time this archive was modified the last time on the server.
	 * @return The time and date this archive was modified the last time on the server.
	 */
	LocalDateTime getLastModificationDate();
	/**
	 * Creates a response object with the data of this archive. 
	 * @return The response object for sending via rest. 
	 */
	ArchiveResponse toResponse();
	/**
	 * Sets the serialization format .
	 * @param serializationFormat
	 */
	void setSerializationFormat(final String serializationFormat);
	/**
	 * Gets the id of this archive.
	 * @return The id of this archive.
	 */
	long getArchiveId();
	boolean isVirtual();
	void setVirtual(boolean virtual);
	DigitalObjectCollection getDigitalObjects();
	void setDigitalObjects(DigitalObjectCollection digitalObjects);
}