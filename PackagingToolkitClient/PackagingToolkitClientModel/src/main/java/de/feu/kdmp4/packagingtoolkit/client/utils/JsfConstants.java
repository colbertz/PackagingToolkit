package de.feu.kdmp4.packagingtoolkit.client.utils;

public class JsfConstants {
	public static final String AJAX_ALL = "@all";
	public static final String AJAX_EVENT_CHANGE = "change";
	public static final String AJAX_EVENT_CLICK = "click";
	public static final String AJAX_EVENT_KEY_UP = "keyup";
	public static final String AJAX_EVENT_VALUE_CHANGE = "valuechange";
	public static final String AJAX_FORM = "@form";
	
	public static final String BEAN_I18N = "i18nBean";
	
	public static final String FACET_HEADER = "header";
	public static final String FILE_UPLOAD_MODE_ADVANCED = "advanced";
	public static final String FILE_UPLOAD_MODE_SIMPLE = "simple";
	
	public static final String PROPERTY_SELECTED_LANGUAGE = "selectedLanguage";
	public static final String PROPERTY_VALUE = "value";
	public static final String PRIME_RADIO_GRID = "grid";
	
	public static final String SELECTION_MODE_MULTIPLE = "multiple";
	public static final String SELECTION_MODE_SINGLE = "single";
	
	// Icons in PrimeFaces.
	public static final String ICON_CLOSE = "ui-icon-close";
	public static final String ICON_CHECK = "ui-icon-check";
}
