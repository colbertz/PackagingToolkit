package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import java.time.LocalDateTime;

public interface OntologyDateTimeProperty extends OntologyDatatypeProperty {
	public LocalDateTime getPropertyValue();

	void setPropertyValue(LocalDateTime value);
}
