package de.feu.kdmp4.packagingtoolkit.client.factories;

import de.feu.kdmp4.packagingtoolkit.client.model.decorators.classes.OntologyIndividualFinishedDecoratorImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.OntologyIndividualFinishedDecorator;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;

/**
 * Creates decorator objects.
 * @author Christopher Olbertz
 *
 */
public class DecoratorFactory {
	/**
	 * Creates a new object that decorates an individual with the finished flag.
	 * @param ontologyIndividual The individual that should be decorated.
	 * @return The newly created object.
	 */
	public static final OntologyIndividualFinishedDecorator createOntologyIndividualFinishedDecorator(
			final OntologyIndividual ontologyIndividual) {
		return new OntologyIndividualFinishedDecoratorImpl(ontologyIndividual);
	}
}
