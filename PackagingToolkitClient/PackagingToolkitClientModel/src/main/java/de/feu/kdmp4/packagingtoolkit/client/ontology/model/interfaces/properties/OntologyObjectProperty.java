package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;

/**
 * Represents object properties in an ontology. The value of an object property is a list with 
 * individual of a class that is the range of this property. 
 * @author Christopher Olbertz
 *
 */
public interface OntologyObjectProperty extends OntologyProperty {
	//void setValue(OntologyIndividual individual);
	/**
	 * Gets the class that is the range of this property. All individuals in the value of this
	 * property are idnviduals of this class.
	 * @return The range of this property.
	 */
	OntologyClass getRange();
	/**
	 * Adds an individual to the value of this property.
	 * @param ontologyIndividual The individual that is added to the list with individuals of this
	 * property.
	 */
	void addOntologyIndividual(OntologyIndividual ontologyIndividual);
	/**
	 * Returns the value of this object property. The value is a list with the individuals of this 
	 * object property.
	 * @return A list with the individuals of this property.
	 */
	OntologyIndividualCollection getPropertyValue();
	void setPropertyValue(OntologyIndividualCollection ontologyIndividuals);
	void setPropertyId(String propertyId);
	/**
	 * Checks if the range of this object property is a class of Premis.
	 * @return True if the range is a Premis class false otherwise.
	 */
	boolean hasPremisRange();
	/**
	 * Deletes all values from this object property.
	 */
	void clearValues();
}
