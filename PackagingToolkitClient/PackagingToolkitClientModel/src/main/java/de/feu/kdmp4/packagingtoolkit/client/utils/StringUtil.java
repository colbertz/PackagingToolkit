package de.feu.kdmp4.packagingtoolkit.client.utils;

/**
 * A class with some helpful methods for working with strings.
 * <ul>
 * 	<li>Replacing spaces with underscores.
 * </ul>
 * @author christopher
 *
 */
public class StringUtil {
	public static final String EMPTY_STRING = " ";
	public static final String UNDERSCORE = "_";
	
	/**
	 * Replace space with underscore.
	 * @param theString The string with spaces.
	 * @return String The string with underscores instead of spaces.
	 */
	public static String escapeSpaces(String theString) {

		if (theString != null) {
			if (theString.contains(" ")) {
				theString = theString.replaceAll(EMPTY_STRING, UNDERSCORE);			
			}
			return theString;
		} else {
			return EMPTY_STRING;
		}
	}
}
