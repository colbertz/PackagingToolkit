package de.feu.kdmp4.packagingtoolkit.client.model.classes;

/**
 * This class represents the user's decision not to use a existing view but to create
 * a new view. This class is necessary to avoid the use of a null-value for this case and
 * to avoid NullPointerExceptions and the checks for null. The attributes are filled with
 * default values.
 * @author Christopher Olbertz
 *
 */
public class NewViewImpl extends ViewImpl {
	private static final int NEW_VIEW_ID = -1;
	private static final String NEW_VIEW_NAME = "newView";
	
	public NewViewImpl() {
		super(NEW_VIEW_NAME, NEW_VIEW_ID);
	}
	
	@Override
	public boolean isNewView() {
		return true;
	}
}
