package de.feu.kdmp4.packagingtoolkit.client.validators;

import de.feu.kdmp4.packagingtoolkit.client.utils.FileUtils;

/**
 * Processes validations about files. Every check has to methods. One methods throws an
 * exception (method name begins with check) and the other method returns a boolean 
 * (method name begins with is or are).
 * <ul>
 * 	<li>Represents a filename a file with an ontology?</li>
 * </ul>
 * @author Christopher Olbertz
 *
 */
public abstract class FileValidator {
	private static final String CLASSNAME = "FileValidator";
	private static final String METHOD_CHECK_ONTOLOGY_FILE = "void checkOntologyFile(String fileName)";
	
	/**
	 * Checks if the file is an owl or a rdf file. Uses the file suffix for the check.
	 * @param filename The name of the file to check.
	 * @throws NotAnOntologyFileException if the file is not an ontology file.
	 */
	public static final void checkOntologyFile(final String filename) {
		if (!isOntologyFile(filename)) {
			/*throw new NotAnOntologyFileException(filename, CLASSNAME, 
												 METHOD_CHECK_ONTOLOGY_FILE, 
												 PackagingToolkitComponent.CLIENT);*/
		}
	}
	
	/**
	 * Checks if a file is an owl or a rdf file. Uses the file suffix for the check.
	 * @param filename The name of the file to check.
	 * @return True if the file is an ontology file, false otherwise.
	 */
	public static final boolean isOntologyFile(final String filename) {
		if (!FileUtils.isOwlFile(filename) && 
				!FileUtils.isRdfFile(filename)) {
			return false;
		} else {
			return true;
		}
	}
}
