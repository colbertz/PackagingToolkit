package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDoubleProperty;

public class OntologyDoublePropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDoubleProperty {
	// ******** Constants ***********
	private static final double DEFAULT_VALUE = 0.0;
	private static final long serialVersionUID = -6417668866515143729L;

	// ******* Constructors ***********
	public OntologyDoublePropertyImpl() {
	}
	
	public OntologyDoublePropertyImpl(String propertyId, String defaultLabel, String range) {
		super(propertyId, DEFAULT_VALUE, defaultLabel, range);
	}

	public OntologyDoublePropertyImpl(String propertyId, double value, String defaultLabel, String range) {
		super(propertyId, value, defaultLabel, range);
	}

	// ****** Getters and setters ********
	@Override
	public void setPropertyValue(Double value) {
		super.setValue(value);
	}
	
	@Override
	public Double getPropertyValue() {
		return (Double) getValue();
	}

	@Override
	public boolean isDoubleProperty() {
		return true;
	}
	
	/*@Override
	public void setPropertyValue(final Object value) {
		String valueAsString = value.toString();
		if (valueAsString.contains(DECIMAL_SEPARATOR_DE)) {
			valueAsString = valueAsString.replaceAll(DECIMAL_SEPARATOR_DE, DECIMAL_SEPARATOR_EN);
		}
		super.setPropertyValue(valueAsString);
	}*/
}