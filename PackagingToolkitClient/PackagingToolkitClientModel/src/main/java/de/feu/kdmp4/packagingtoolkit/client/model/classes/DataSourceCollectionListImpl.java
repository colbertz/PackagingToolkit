package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

public class DataSourceCollectionListImpl extends AbstractList implements DataSourceCollection {
	@Override
	public DataSource getDataSourceAt(final int index) {
		return (DataSource)getElement(index);
	}
	
	@Override
	public void addDataSource(final DataSource dataSource) {
		super.add(dataSource);
	}
	
	@Override
	public int getDataSourceCount() {
		return super.getSize();
	}
}
