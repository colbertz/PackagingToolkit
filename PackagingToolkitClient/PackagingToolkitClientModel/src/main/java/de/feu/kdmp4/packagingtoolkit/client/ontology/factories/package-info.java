/**
 * Contains all factories for the client objects.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.client.ontology.factories;