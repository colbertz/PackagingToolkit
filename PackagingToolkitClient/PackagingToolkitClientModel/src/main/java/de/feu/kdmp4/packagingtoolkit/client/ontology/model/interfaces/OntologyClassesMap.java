package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;

public interface OntologyClassesMap {
	//TODO, Kommentare korrigieren, wenn sich diese Klasse bewaehrt.
	/**
	 * Adds a property to the property list of an ontology class.
	 * @param className The class where the property is added to.
	 * @param property The property that should be added to a class.
	 */
	public void addClass(OntologyClass ontologyClass);
	/**
	 * Gets the property list of a given class.
	 * @param className The name of the class. This is the key in the map.
	 * @return The property list of the class. If there are no properties 
	 * the return value is an empty property list.
	 */
	OntologyClass getOntologyClass(String className);
	/**
	 * Creates an ontology class in the map with an empty property list.
	 * @param className The name of the class and the key for the map. 
	 */
	void addDatatypeProperty(String className, OntologyDatatypeProperty ontologyDatatypeProperty);
	/**
	 * Creates a list with all ontology classes in this map.
	 * @return A list with all ontology classes in this map.
	 */
	OntologyClassList findAllOntologyClasses();

}
