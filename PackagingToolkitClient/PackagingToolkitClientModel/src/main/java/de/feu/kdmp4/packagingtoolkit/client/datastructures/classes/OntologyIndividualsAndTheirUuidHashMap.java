package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.OntologyIndividualsAndTheirUuidMap;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class OntologyIndividualsAndTheirUuidHashMap implements OntologyIndividualsAndTheirUuidMap {
	/**
	 * Contains the uuid of an individual as key and the individual object itself as value. 
	 */
	private Map<Uuid, OntologyIndividual> ontologyIndividuals;
	
	public OntologyIndividualsAndTheirUuidHashMap() {
		ontologyIndividuals = new HashMap<>();
	}
	
	@Override
	public void addIndividual(final OntologyIndividual ontologyIndividual) {
		if (ontologyIndividual != null && ontologyIndividual.getUuid() != null) {
			ontologyIndividuals.put(ontologyIndividual.getUuid(), ontologyIndividual);
		}
	}
	
	@Override
	public OntologyIndividual getIndividual(final Uuid uuidOfIndividual) {
		return ontologyIndividuals.get(uuidOfIndividual);
	}
	
	@Override
	public void replaceIndividual(final OntologyIndividual newOntologyIndividual) {
		final Uuid uuidOfNewIndividual = newOntologyIndividual.getUuid();
		ontologyIndividuals.remove(uuidOfNewIndividual);
		ontologyIndividuals.put(uuidOfNewIndividual, newOntologyIndividual);
	}
}
