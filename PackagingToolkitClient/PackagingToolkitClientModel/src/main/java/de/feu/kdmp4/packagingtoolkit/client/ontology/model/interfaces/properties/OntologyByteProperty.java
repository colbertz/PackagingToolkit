package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyBytePropertyImpl;

public interface OntologyByteProperty extends OntologyDatatypeProperty {

	void addMaxInclusiveRestriction(short value);
	void addMinInclusiveRestriction(short value);
	
	public static OntologyByteProperty createUnsigned(String propertyId, String label, String range) {
		return new OntologyBytePropertyImpl(propertyId, true, label, range); 
	}
	
	public static OntologyByteProperty createSigned(String propertyId, String label, String range) {
		return new OntologyBytePropertyImpl(propertyId, false, label, range); 
	}
	
	public Short getPropertyValue();
	void setPropertyValue(Short value);
	boolean isUnsigned();
}
