package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;

@SuppressWarnings("rawtypes")
public interface Restriction {
	boolean isSatisfied(RestrictionValue value);
	void checkRestriction(RestrictionValue value);
	RestrictionValue getRestrictionValue();
}
