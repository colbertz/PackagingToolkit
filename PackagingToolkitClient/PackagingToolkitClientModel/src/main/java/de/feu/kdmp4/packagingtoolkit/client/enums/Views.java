package de.feu.kdmp4.packagingtoolkit.client.enums;

/**
 * An enum for the views of the client. Is used in the class 
 * {@link de.feu.kdmp4.packagingtoolkit.client.utils.ClientLogUtil} for 
 * logging the navigation.
 * @author Christopher Olbertz
 *
 */
public enum Views {
	CONFIGURATION("configuration"), CREATE_INFORMATION_PACKAGE("createInformationPackage"),
	ONTOLOGY_TREE_VIEW("ontologyTreeView"), SAVED_VIEWS("savedViews"), 
	SELECT_ONTOLOGY_NODES("selectOntologyNodes"), SHOW_ARCHIVE_LIST("showArchiveList"), 
	STARTPAGE("index");
	
	/**
	 * The default suffix of jsf files.
	 */
	private static final String FACELETS_SUFFIX = "";
	/**
	 * The relative path where the xhtml files are stored. 
	 */
	private static final String PATH_XHTML_FILES = "";
	
	private String url;
	
	private Views(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return PATH_XHTML_FILES + url + FACELETS_SUFFIX;
	}
}