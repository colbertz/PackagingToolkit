package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

/**
 * Contains a collection with tools of the data sources on the mediator. 
 * @author Christopher Olbertz
 *
 */
public interface DataSourceToolCollection {
	/**
	 * Adds a tool to the collection.
	 * @param dataSourceTool The tool that should be added.
	 */
	void addDataSourceToolToList(final DataSourceTool dataSourceTool);
	/**
	 * Determines if a certain tool is contained in the collection.
	 * @param dataSourceTool The tool that we want to check. 
	 * @return True if the tool is contained in the collection, false otherwise. 
	 */
	boolean containsDataSourceTool(final DataSourceTool dataSourceTool);
	/**
	 * Counts the tools in the collection.
	 * @return The number of tools.
	 */
	int getDataSourceToolCount();
	/**
	 * Removes a tool from the collection. 
	 * @param dataSourceTool The tool that should be removed. 
	 */
	void removeDataSourceTool(final DataSourceTool dataSourceTool);
	/**
	 * Determines a tool at a given position in the collection. 
	 * @param index The position we are interested in, starting with 0.
	 * @return The found tool. 
	 */
	DataSourceTool getDataSourceToolAt(final int index);
	/**
	 * Checks if the collection contains any tools. 
	 * @return True if the collection is empty, false otherwise. 
	 */
	boolean isEmpty();
}
