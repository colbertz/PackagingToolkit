/**
 * Contains classes that manage some information that have to be reachable in the whole application. 
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.managers;