package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

/**
 * Describes the tools a data source can contain of. If a data source contains of several tools,
 * the tools not needed can be deactivated.
 * @author Christopher Olbertz
 *
 */
public interface DataSourceTool {
	/**
	 * Determines if the tool is activated.
	 * @return True if the tool is activated, false otherwise. 
	 */
	boolean isActivated();
	/**
	 * Returns the name of the tool.
	 * @return The name of the tool.
	 */
	String getToolName();
	/**
	 * Activates a tool.
	 */
	void activateTool();
	/**
	 * Deactivates the tool.
	 */
	void deactivateTool();
}