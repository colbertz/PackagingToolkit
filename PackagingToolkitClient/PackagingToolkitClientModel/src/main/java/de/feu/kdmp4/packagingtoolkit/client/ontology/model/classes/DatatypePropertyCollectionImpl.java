package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.io.Serializable;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

public class DatatypePropertyCollectionImpl extends AbstractList implements DatatypePropertyCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;

	public DatatypePropertyCollectionImpl() {
	}
	
	@Override
	public void addDatatypeProperty(final OntologyDatatypeProperty property) {
		super.add(property);
	}
	
	@Override
	public OntologyDatatypeProperty getDatatypeProperty(final int index) {
		return (OntologyDatatypeProperty)super.getElement(index);
	}
	
	@Override
	public Optional<OntologyDatatypeProperty> getDatatypeProperty(final Uuid uuidOfDatatypeProperty) {
		for (int i = 0; i < getPropertiesCount(); i++) {
			OntologyDatatypeProperty ontologyDatatypeProperty = getDatatypeProperty(i);
			Uuid uuid = ontologyDatatypeProperty.getUuid();
			if (uuid.equals(uuidOfDatatypeProperty)) {
				Optional<OntologyDatatypeProperty> optional = ClientOptionalFactory.createDatatypePropertyOptional(ontologyDatatypeProperty);
				return optional;
			}
		}
		
		return ClientOptionalFactory.createEmptyDatatypePropertyOptional();
	}

	@Override
	public Optional<OntologyDatatypeProperty> getDatatypeProperty(final String iriOfDatatypeProperty) {
		for (int i = 0; i < getPropertiesCount(); i++) {
			OntologyDatatypeProperty ontologyDatatypeProperty = getDatatypeProperty(i);
			String iri = ontologyDatatypeProperty.getPropertyId();
			if (areStringsEqual(iri, iriOfDatatypeProperty)) {
				Optional<OntologyDatatypeProperty> optional = ClientOptionalFactory.createDatatypePropertyOptional(ontologyDatatypeProperty);
				return optional;
			}
		}
		
		return ClientOptionalFactory.createEmptyDatatypePropertyOptional();
	}
	 
	
	@Override
	public int getPropertiesCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}

	@Override
	public boolean containsDatatypeProperty(OntologyDatatypeProperty ontologyDatatypeProperty) {
		return super.contains(ontologyDatatypeProperty);
	}

	@Override
	public void replaceValueOfDatatypeProperty(OntologyDatatypeProperty ontologyDatatypeProperty) {
		String iriOfDatatypeProperty = ontologyDatatypeProperty.getPropertyId();
		Object value = ontologyDatatypeProperty.getPropertyValue();
		
		Optional<OntologyDatatypeProperty> optionalWithOntologyDatatypeProperty = getDatatypeProperty(iriOfDatatypeProperty);
		if (optionalWithOntologyDatatypeProperty.isPresent()) {
			OntologyDatatypeProperty originalDatatypeProperty = optionalWithOntologyDatatypeProperty.get();
			originalDatatypeProperty.setPropertyValue(value);
		}
	}
}
