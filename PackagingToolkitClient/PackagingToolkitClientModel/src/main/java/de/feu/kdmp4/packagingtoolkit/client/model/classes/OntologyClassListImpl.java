package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

/**
 * Encapsulates a list with ontologyClasss.
 * @author Christopher Olbertz
 *
 */
public class OntologyClassListImpl extends AbstractList 
							 implements OntologyClassList { 
	
	/**
	 * Is needed because of PrimeFaces.
	 */
	public OntologyClassListImpl() {
	}
	
	@Override
	public void addOntologyClassToList(final OntologyClass ontologyClass) {
		if (ontologyClass == null) {
		}
		
		super.add(ontologyClass);
	}
	
	@Override
	public OntologyClass getOntologyClassAt(final int index) {
		OntologyClass ontologyClass = null;
		ontologyClass = (OntologyClass)super.getElement(index);
		return ontologyClass;
	}
	
	@Override
	public int getOntologyClassCount() {
		return super.getSize();
	}
	
	@Override
	public void removeOntologyClass(final OntologyClass ontologyClass) {
		super.remove(ontologyClass);
	}
}
