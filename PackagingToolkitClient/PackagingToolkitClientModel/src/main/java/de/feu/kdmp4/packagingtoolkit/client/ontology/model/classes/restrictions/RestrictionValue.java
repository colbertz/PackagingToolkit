package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions;

/**
 * Represents values that are used in restrictions. The class encapsulates the 
 * values that are comparable. A value can be an object of different datatypes.
 * This class hides the real datatype. This class exists in order that the
 * methods of AbstractRestriction are more flexible.
 * @author Christopher Olbertz
 *
 */
public class RestrictionValue<T extends Comparable<T>> {
	/**
	 * The value that is used in a restriction.
	 */
	private T value;

	public RestrictionValue() {
	}
	
	public RestrictionValue(T value) {
		this.value = value;
	}
	
	/*public static void main(String[] args) {
		RestrictionValue<Integer> r = new RestrictionValue<>(3);
		RestrictionValue<Integer> y = new RestrictionValue<>(4);
		System.out.println(r.compare(y));
	}*/
	
	/*public static RestrictionValue fromDecimal(BigDecimal value) {
		return new RestrictionValue(value);
	}
	
	public static RestrictionValue fromInteger(Integer value) {
		return new RestrictionValue(value);
	}
	
	public void fromString(String value) {
		this.value = value;
	}*/
	
	/*public BigDecimal getDecimal() {
		if (value instanceof BigDecimal) {
			return (BigDecimal)value;
		} else {
			return null;
		}
	}

	public Integer getInteger() {
		if (value instanceof Integer) {
			return (Integer)value;
		} else {
			return null;
		}
	}
	
	public String getString() {
		if (value instanceof String) {
			return (String)value;
		} else {
			return null;
		}
	}*/
	
	public int compare(RestrictionValue<T> restrictionValue) {
		return value.compareTo(restrictionValue.getValue());
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
	public T getValue() {
		return value;
	}
}