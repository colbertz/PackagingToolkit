package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyBooleanProperty;

public class OntologyBooleanPropertyImpl extends OntologyDatatypePropertyImpl implements OntologyBooleanProperty {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5032266957060852923L;
	private static final boolean BOOLEAN_DEFAULT_VALUE = false;
	
	public OntologyBooleanPropertyImpl() {
	}
	
	/**
	 * Creates a new boolean property with an id and a label. 
	 * @param label The label.
	 * @param propertyId The id.
	 */
	public OntologyBooleanPropertyImpl(String propertyId, String label, String range) {
		super(propertyId, BOOLEAN_DEFAULT_VALUE, label, range);
	}
	
	@Override
	public void setPropertyValue(boolean value) {
		super.setValue(value);
	}
	
	@Override
	public Boolean getPropertyValue() {
		return (Boolean)getValue();
	}
	
	@Override
	public String toString() {  
		return getLabel();
	}
	
	@Override
	public boolean isBooleanProperty() {
		return true;
	}
}
