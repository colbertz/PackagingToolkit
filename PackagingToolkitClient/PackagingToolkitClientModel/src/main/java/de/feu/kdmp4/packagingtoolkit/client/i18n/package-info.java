/**
 * Contains classes that help with the internationalization. 
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.i18n;