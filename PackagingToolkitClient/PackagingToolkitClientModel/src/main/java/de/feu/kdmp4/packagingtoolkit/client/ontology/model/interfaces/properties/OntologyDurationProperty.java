package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;

public interface OntologyDurationProperty extends OntologyDatatypeProperty {
	public OntologyDuration getPropertyValue();

	void setPropertyValue(OntologyDuration value);
}
