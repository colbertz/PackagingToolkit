package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;

/**
 * Represents a view on a set of ontology classes.
 * @author Christopher Olbertz
 *
 */
public interface View {
	OntologyClass getOntologyClassAt(final int index);
	String getViewname();
	int getClassesCount();
	void addOntologyClass(final OntologyClass ontologyClass);
	int getViewId();
	void addOntologyClasses(final OntologyClassList ontologyClassList);
	boolean isNewView();
}
