package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.io.Serializable;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.PropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

public class PropertyCollectionImpl extends AbstractList/*<OntologyProperty>*/ implements PropertyCollection, Serializable {
	private static final long serialVersionUID = -2983927994943573999L;

	@Override
	public void addProperty(OntologyProperty property) {
		//try {
			super.add(property);
		/*} catch (ListElementMayNotBeNullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	@Override
	public OntologyProperty getProperty(int index) {
		return (OntologyProperty)super.getElement(index);
	}
	
	@Override
	public int getPropertiesCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}
}
