package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.util.Collection;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

public class ReferenceListCollectionImpl extends AbstractList implements ReferenceCollection {
	
	public ReferenceListCollectionImpl() {
	}
	
	public ReferenceListCollectionImpl(final Collection<Reference> referenceCollection) {
		for (final Reference reference: referenceCollection) {
			addReference(reference);
		}
	}
	
	@Override
	public void addReference(final Reference reference) {
		super.add(reference);

	}
	
	@Override
	public Reference getReference(final int index) {
		return (Reference)super.getElement(index);
	}
	
	@Override
	public Optional<Reference> removeReference(final String url) {
		boolean referenceNotFound = true;
		int counter = 0;
		
		while (referenceNotFound && counter < getReferencesCount()) {
			final Reference reference = getReference(counter);
			if (areStringsEqual(reference.getUrl(), url)) {
				final Reference deletedReference = (Reference)super.remove(counter);
				final Optional<Reference> referenceOptional = ClientOptionalFactory.createReferenceOptional(deletedReference); 
				return referenceOptional;
			}
			counter++;
		}
		
		return ClientOptionalFactory.createEmptyReferenceOptional();
	}
	
	@Override
	public int getReferencesCount() {
		return super.getSize();
	}
}
