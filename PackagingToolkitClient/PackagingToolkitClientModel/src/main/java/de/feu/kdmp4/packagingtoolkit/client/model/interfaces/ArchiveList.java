package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

public interface ArchiveList {
	void removeArchive(Archive archive);
	int getArchiveCount();
	Archive getArchiveAt(int index);
	void addArchiveToList(Archive archive);
}
