package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

/**
 * Contains data received from the mediator. The data are name-value-pairs. 
 * @author Christopher Olbertz
 *
 */
public interface MediatorData {
	/**
	 * Returns the name assigned to this data element.
	 * @return The name of the element. 
	 */
	String getName();
	/**
	 * Returns the value assigned to this data element.
	 * @return The value of the element. 
	 */
	String getValue();

}
