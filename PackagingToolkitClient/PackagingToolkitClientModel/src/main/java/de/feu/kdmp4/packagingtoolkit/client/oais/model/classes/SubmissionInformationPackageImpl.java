package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.SubmissionInformationPackage;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * The implementation of a submission information package.
 * @author Christopher Olbertz
 *
 */
public class SubmissionInformationPackageImpl extends InformationPackageImpl 
										    implements SubmissionInformationPackage {
	
	// ********** Attributes ***********
	/**
	 * The digital object that is contained in this SIP.
	 */
	private DigitalObject digitalObject;
	
	// ********** Constructors *********
	public SubmissionInformationPackageImpl(String title, Uuid uuid) {
		super(title, uuid);
	}
	
	// ********** Public methods *******
	@Override
	public DigitalObject removeDigitalObject() {
		digitalObject = null;
		return digitalObject;
	}
	
	@Override
	public void addDigitalObject(DigitalObject digitalObject) {
		if (digitalObject == null) {
			/*DigitalObjectException exception = DigitalObjectException.
					digitalObjectMayNotBeNullException(this.getClass().getName(),  
							METHOD_SET_DIGITAL_OBJECT);*/
			//throw exception;
		}
		
		/* There is already a digital object in this SIP. Therefore the new digital
		 * cannot be set.
		 */
		if (this.digitalObject != null) {
			/*DigitalObjectException exception = DigitalObjectException.
					digitalObjectAlreadySetInSIPException(this.getClass().getName(),  
							METHOD_SET_DIGITAL_OBJECT, digitalObject.getFilename(),
							this.getUuid().toString(), this.digitalObject.getFilename());*/
			//throw exception;
		}
		
		this.digitalObject = digitalObject;
	}
	
	// ********** Getters and setters *********
	@Override
	public PackageType getPackageType() {
		return PackageType.SIP;
	}
}
