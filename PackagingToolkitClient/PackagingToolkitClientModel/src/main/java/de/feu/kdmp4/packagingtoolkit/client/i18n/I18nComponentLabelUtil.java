package de.feu.kdmp4.packagingtoolkit.client.i18n;

import java.util.ResourceBundle;

/**
 * Encapsulates the keys for internationalization. Java code is not allowed
 * to call the message bundles but it has to use this class and its static
 * methods. Therefore the other classes do not have to know the names of 
 * the keys. This class contains the keys only for the labels of components.
 * @author Christopher Olbertz
 *
 */
public class I18nComponentLabelUtil {
	private static final String BUTTON_ADD = "button-add";
	private static final String BUTTON_BACK = "button-back";
	private static final String BUTTON_CANCEL = "button-cancel";
	private static final String BUTTON_CHOOSE = "button-choose";
	private static final String BUTTON_DELETE = "button-delete";
	private static final String BUTTON_EDIT = "button-edit";
	private static final String BUTTON_NEW_INDIVIDUAL = "button-new-individual";
	private static final String BUTTON_NEXT = "button-next";
	private static final String BUTTON_RESET = "button-reset";
	private static final String BUTTON_SAVE = "button-save";
	private static final String BUTTON_SAVE_INDIVIDUAL = "button-save-new-individual";
	private static final String BUTTON_SHOW_REFERENCES = "button-show-references";
	private static final String COLUMN_REFERENCE = "column-reference";
	private static final String LABEL_COLUMNS = "label-columns";
	private static final String LABEL_DAYS = "label-days";
	private static final String LABEL_DIGITAL_OBJECT = "label-digital-object";
	private static final String LABEL_DIGITAL_OBJECTS = "label-digital-objects";
	private static final String LABEL_FITS = "label-fits";
	private static final String LABEL_HOURS = "label-hours";
	private static final String LABEL_LITERALS = "label-literals";
	private static final String LABEL_METADATA = "label-metadata";
	private static final String LABEL_METADATA_WITH_CONFLICTS = "label-metadata-with-conflicts";
	private static final String LABEL_MINUTES = "label-minutes";
	private static final String LABEL_MONTHS = "label-months";
	private static final String LABEL_NEGATIVE = "label-negative";
	private static final String LABEL_NEW_VALUE = "label-new-value";
	private static final String LABEL_NO = "label-no";
	private static final String LABEL_NO_INDIVIDUAL_FOUND = "label-no-individuals-found";
	private static final String LABEL_NO_DIGITAL_OBJECTS_FOUND = "label-no-digital-objects";
	private static final String LABEL_NO_VALUES = "label-no-values";
	private static final String LABEL_NOTHING_CONFIGURED = "label-nothing-configured";
	private static final String LABEL_REFERENCES = "label-references";
	private static final String LABEL_SECONDS = "label-seconds";
	private static final String LABEL_YEARS = "label-years";
	private static final String LABEL_YES = "label-yes";
	private static final String LABEL_YOU_ARE_CREATING_NEW_INDIVIDUAL = "label-you-are-creating-new-individual";
	private static final String LABEL_YOU_ARE_EDITING_INDIVIDUAL = "label-you-are-editing-individual";
	private static final String RADIO_BUTTON_NEW_VIEW = "radio-button-new-view";
	
	private static ResourceBundle resourceBundleComponentLabels;
	
	static {
		resourceBundleComponentLabels = I18nUtil.getComponentLabelsResourceBundle();
	}
	
	public static String getButtonAddString() {
		return resourceBundleComponentLabels.getString(BUTTON_ADD);
	}

	public static String getButtonBackString() {
		return resourceBundleComponentLabels.getString(BUTTON_BACK);
	}	
	
	public static String getButtonCancelString() {
		return resourceBundleComponentLabels.getString(BUTTON_CANCEL);
	}
	
	public static String getButtonChooseString() {
		return resourceBundleComponentLabels.getString(BUTTON_CHOOSE);
	}
	
	public static String getButtonDeleteString() {
		return resourceBundleComponentLabels.getString(BUTTON_DELETE);
	}
	
	public static String getButtonEditString() {
		return resourceBundleComponentLabels.getString(BUTTON_EDIT);
	}
	
	public static String getButtonResetString() {
		return resourceBundleComponentLabels.getString(BUTTON_RESET);
	}

	public static String getButtonShowReferencesString() {
		return resourceBundleComponentLabels.getString(BUTTON_SHOW_REFERENCES);
	}
	
	public static String getButtonNewIndividualString() {
		return resourceBundleComponentLabels.getString(BUTTON_NEW_INDIVIDUAL);
	}
	
	public static String getLabelColumnsString() {
		return resourceBundleComponentLabels.getString(LABEL_COLUMNS);
	}
	
	public static String getLabelDaysString() {
		return resourceBundleComponentLabels.getString(LABEL_DAYS);
	}
	
	public static String getFitsString() {
		return resourceBundleComponentLabels.getString(LABEL_FITS);
	}
	
	public static String getLabelHoursString() {
		return resourceBundleComponentLabels.getString(LABEL_HOURS);
	}

	public static String getLabelLiteralsString() {
		return resourceBundleComponentLabels.getString(LABEL_LITERALS);
	}
	
	public static String getLabelMinutesString() {
		return resourceBundleComponentLabels.getString(LABEL_MINUTES);
	}
	
	public static String getLabelMonthsString() {
		return resourceBundleComponentLabels.getString(LABEL_MONTHS);
	}
	
	public static String getLabelMetadataString() {
		return resourceBundleComponentLabels.getString(LABEL_METADATA);
	}
	
	public static String getLabelMetadataWithConflictsString() {
		return resourceBundleComponentLabels.getString(LABEL_METADATA_WITH_CONFLICTS);
	}
	
	public static String getLabelNegativeString() {
		return resourceBundleComponentLabels.getString(LABEL_NEGATIVE);
	}
	
	public static String getLabelNoString() {
		return resourceBundleComponentLabels.getString(LABEL_NO);
	}
	
	public static String getLabelNoValues() {
		return resourceBundleComponentLabels.getString(LABEL_NO_VALUES);
	}
	
	public static String getLabelNoIndividualsFoundString() {
		return resourceBundleComponentLabels.getString(LABEL_NO_INDIVIDUAL_FOUND);
	}
	
	public static String getLabelNothingConfiguredString() {
		return resourceBundleComponentLabels.getString(LABEL_NOTHING_CONFIGURED);
	}
	
	public static String getLabelSecondsString() {
		return resourceBundleComponentLabels.getString(LABEL_SECONDS);
	}
	
	public static String getLabelYearsString() {
		return resourceBundleComponentLabels.getString(LABEL_YEARS);
	}
	
	public static String getLabelYesString() {
		return resourceBundleComponentLabels.getString(LABEL_YES);
	}
	
	public static String getLabelYouAreCreatingNewIndividualString() {
		return resourceBundleComponentLabels.getString(LABEL_YOU_ARE_CREATING_NEW_INDIVIDUAL);
	}
	
	public static String getLabelYouAreEditingIndividualString(String uuidOfIndividual) {
		String message = resourceBundleComponentLabels.getString(LABEL_YOU_ARE_EDITING_INDIVIDUAL);
		message = String.format(message, uuidOfIndividual);
		
		return message;
	}
	
	public static String getButtonNextString() {
		return resourceBundleComponentLabels.getString(BUTTON_NEXT);
	}
	
	public static String getButtonSaveString() {
		return resourceBundleComponentLabels.getString(BUTTON_SAVE);
	}
	
	public static String getColumnReferenceString() {
		return resourceBundleComponentLabels.getString(COLUMN_REFERENCE);
	}

	public static String getLabelDigitalObjectString() {
		return resourceBundleComponentLabels.getString(LABEL_DIGITAL_OBJECT);
	}
	
	public static String getLabelDigitalObjectsString() {
		return resourceBundleComponentLabels.getString(LABEL_DIGITAL_OBJECTS);
	}
	
	public static String getLabelNewValueString() {
		return resourceBundleComponentLabels.getString(LABEL_NEW_VALUE);
	}
	
	public static String getLabelReferencesString() {
		return resourceBundleComponentLabels.getString(LABEL_REFERENCES);
	}
	
	public static String getRadioButtonNewViewString() {
		return resourceBundleComponentLabels.getString(RADIO_BUTTON_NEW_VIEW);
	}

	public static Object getLabelNoDigitalObjects() {
		return resourceBundleComponentLabels.getString(LABEL_NO_DIGITAL_OBJECTS_FOUND);
	}

	public static Object getButtonSaveIndividualString() {
		return resourceBundleComponentLabels.getString(BUTTON_SAVE_INDIVIDUAL);
	}
}