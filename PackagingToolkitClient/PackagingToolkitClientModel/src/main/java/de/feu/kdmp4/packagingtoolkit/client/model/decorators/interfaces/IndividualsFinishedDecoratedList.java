package de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;

/**
 * Contains individuals that are decorated in this way that they contain an additional finished flag.
 * This class administrates the individuals in that way that only one individual can be unfinished
 * at the same time. 
 * @author Christopher OIbertz
 *
 */
public interface IndividualsFinishedDecoratedList {
	/**
	 * Adds an new individual if it is either a finished individual or an unfinished individual in only and only this case there
	 * is not another unfinished individual in the list.
	 * @param individual The individual that should be added to the list.
	 */
	void addUnfinishedIndividual(final OntologyIndividualFinishedDecorator individual);
	/**
	 * Checks if there are any unfinished individuals in the list.
	 * @return True if the list contains an unfinished individual.
	 */
	boolean containsUnfinishedIndividual();
	/**
	 * Returns the unfinished individual in the list.
	 * @return The unfinished individual in the list or an empty optional if there is no unfinished individual.
	 */
	Optional<OntologyIndividualFinishedDecorator> getUnfinishedIndividual();
	/**
	 * Adds an new individual if it is either a finished individual or an unfinished individual in only and only this case there
	 * is not another unfinished individual in the list.
	 * @param individual The individual that should be added to the list.
	 */
	void addUnfinishedIndividual(OntologyIndividual individual);
}
