package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions;

/**
 * A list with restrictions.
 * @author Christopher Olbertz
 *
 */
public interface RestrictionCollection {
	/**
	 * Gets a restriction at a given index of the list.
	 * @param index The index.
	 * @return The restriction found at the index.
	 */
	Restriction getRestriction(int index);
	/**
	 * Adds a restriction to the list.
	 * @param restriction The restriction that should be appended to the list.
	 */
	void addRestriction(Restriction restriction);
	
	int getRestrictionsCount();

}
