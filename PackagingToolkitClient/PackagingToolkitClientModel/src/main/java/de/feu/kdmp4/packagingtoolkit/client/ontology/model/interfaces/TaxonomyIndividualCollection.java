package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import java.util.Optional;
import java.util.function.Consumer;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.ListInterface;

/**
 * A collection that contains individuals in a taxonomy.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomyIndividualCollection extends ListInterface {
	/**
	 * Adds an individual to the collection.
	 * @param taxonomyIndividual The individual that should be added.
	 */
	void addTaxonomyIndividual(TaxonomyIndividual taxonomyIndividual);
	/**
	 * Adds individuals to the collection.
	 * @param taxonomyIndividuals The individuals that should be added.
	 */
	void addTaxonomyIndividuals(TaxonomyIndividualCollection taxonomyIndividuals);
	/**
	 * Determines an individual in the collection at a certain position.
	 * @param index The position we are looking at.
	 * @return The found individual or an empty optional.
	 */
	Optional<TaxonomyIndividual> getTaxonomyIndividual(int index);
	/**
	 * Determines the number of individuals in this collection.
	 * @return The number of individuals in this collection.
	 */
	int getTaxonomyIndividualCount();
	/**
	 * Processes an action on every individual in this collection. The modified individuals are written in a new collection
	 * object. This means that the original individuals are not modified.
	 * @param operator Contains the operation that should be done on every individual.
	 * @return A new collection object with the modifications.
	 */
	void doWithEveryIndividual(Consumer<TaxonomyIndividual> consumer);
	/**
	 * Determines the iri of the taxonomy the individuals belong to.
	 * @return The iri of the taxonomy the individuals belong to or null if this information has not been set.
	 */
	Iri getIriOfTaxonomy();
	/**
	 * Sets the iri of the taxonomy the individuals belong to.
	 * @param iriOfTaxonomy The iri of the taxonomy the individuals belong to.
	 */
	void setIriOfTaxonomy(Iri iriOfTaxonomy);
	/**
	 * Creates a string that represents the elements in the list as an unordered list in HTML.
	 * @return
	 */
	String getAsHtmlUnorderedList();
}