package de.feu.kdmp4.packagingtoolkit.client.factories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.classes.PropertiesAndTheirClassesHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.classes.TaxonomiesAndTheirIndividualsHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.classes.TaxonomyIndividualsAndTheirIrisHashMap;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.PropertiesAndTheirClassesMap;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.TaxonomiesAndTheirIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.TaxonomyIndividualsAndTheirIrisMap;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ArchiveImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ArchiveListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ClientConfigurationDataImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.DataSourceCollectionListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.DataSourceImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.DataSourceToolCollectionListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.DataSourceToolImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.IriCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.MediatorDataCollectionHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.MediatorDataImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.MetadataElementImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.MetadataElementListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.MetadataHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.NewViewImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.OntologyClassListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ReferenceImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ReferenceListCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ServerConfigurationDataImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyIndividualListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TextInLanguageImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TextInLanguageListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ViewImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ViewListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.classes.IndividualsFinishedDecoratedListImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.classes.OntologyIndividualFinishedDecoratorImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.IndividualsFinishedDecoratedList;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.OntologyIndividualFinishedDecorator;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElement;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElementList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataMap;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ViewList;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.DigitalObjectImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.DigitalObjectCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.SubmissionInformationPackageImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.SubmissionInformationUnitImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.BasicInformationPackage;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.AbstractFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.DatatypePropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyClassImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyClassesHashMap;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyDurationImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.PropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties.OntologyObjectPropertyImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.PropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguageList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.utils.StringUtil;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyResponse;
import de.feu.kdmp4.packagingtoolkit.response.TextInLanguageListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TextInLanguageResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

/**
 * Creates the model elements of the client component.  All implementations of model elements should be
 * created in this class.
 * @author Christopher Olbertz
 *
 */
public class ClientModelFactory extends AbstractFactory {
	/**
	 * A reference to the object that creates property objects.
	 */
	private PropertyFactory propertyFactory;
	/**
	 * A reference to the object that contain important information about the information package that is
	 * currently being created. 
	 */
	private InformationPackageManager informationPackageManager;
	
	public static final IriCollection createEmptyIriCollection() {
		return new IriCollectionImpl();
	}
	
	/**
	 * Creates an individual that is decorated with the ability to be marked as finished. 
	 * @return The created object.
	 */
	public static final IndividualsFinishedDecoratedList createEmptyIndividualsFinishedDecoratedList() {
		return new IndividualsFinishedDecoratedListImpl();
	}
	
	public static final TaxonomiesAndTheirIndividualsMap createEmptyTaxonomiesAndTheirIndividualsMap() {
		return new TaxonomiesAndTheirIndividualsHashMapImpl();
	}
	
	/**
	 * Creates an empty collection object that can contai different texts in different languages.
	 * @return The newly created empty collection.
	 */
	public static final TextInLanguageList createEmptyTextInLanguageCollection() {
		return new TextInLanguageListImpl();
	}
	
	/**
	 * Creates an individual that is decorated with the ability to be marked as finished.
	 * @param ontologyIndividual The individual that should be contained in the new object.  
	 * @return The created object.
	 */
	public static final OntologyIndividualFinishedDecorator createOntologyIndividualFinishedDecorator(final OntologyIndividual ontologyIndividual) {
		return new OntologyIndividualFinishedDecoratorImpl(ontologyIndividual);
	}
	
	/**
	 * Creates an empty collection with ontology classes.
	 * @return The created object.
	 */
	public static final OntologyClassList createEmptyOntologyClassList() {
		return new OntologyClassListImpl();
	}

	/**
	 * Creates an empty map with ontology classes and their iris. 
	 * @return The created object.
	 */
	public static final PropertiesAndTheirClassesMap createEmptyPropertiesAndTheirClassesMap() {
		return new PropertiesAndTheirClassesHashMapImpl();
	}
	
	/**
	 * Creates an object of this factory. 
	 * @param propertyFactory A reference on a property factory.
	 */
	public ClientModelFactory(final PropertyFactory propertyFactory) {
		this.propertyFactory = propertyFactory;
	}
	
	/**
	 * Creates a data source tool.
	 * @param toolName A name for the tool.
	 * @return The created object.
	 */
	public static DataSourceTool createDataSourceTool(final String toolName) {
		return new DataSourceToolImpl(toolName);
	}
	
	/**
	 * Creates an empty map for metadata received from the mediator. 
	 * @return The created object.
	 */
	public static final MetadataMap createEmptyMetadataMap() {
		return new MetadataHashMapImpl();
	}
	
	/**
	 * Creates an empty map for ontology individuals. 
	 * @return The created object.
	 */
	public static final OntologyIndividualCollection createEmptyOntologyIndividualCollection() {
		return new OntologyIndividualCollectionImpl();
	}
	
	/**
	 * Creates an empty map for taxonomy individuals.
	 * @return The created object.
	 */
	public static final TaxonomyIndividualCollection createEmptyTaxonomyIndividualCollection() {
		return new TaxonomyIndividualListImpl();
	}
	
	/**
	 * Creates an empty collection with references.
	 * @return The empty collection.
	 */
	public static final ReferenceCollection createEmptyReferenceCollection() {
		return new ReferenceListCollectionImpl();
	}
	
	/**
	 * Creates an archive with the data of an archive response send by the server.
	 * @param archiveResponse The archive response that contains the data to use.
	 * @return The archive with the data from the archive response.
	 */
	public static final Archive getArchive(final ArchiveResponse archiveResponse) {
		long archiveId = Long.parseLong(archiveResponse.getArchiveId()); 
		final String title = archiveResponse.getTitle();
		final String uuidAsString = archiveResponse.getUuid();
		final Uuid uuid = PackagingToolkitModelFactory.getUuid(uuidAsString);
		final String packageTypeAsString = archiveResponse.getPackageType();
		final PackageType packageType = PackageType.getPackageType(packageTypeAsString);
		final String serializationFormatAsString = archiveResponse.getSerializationFormat();
		final SerializationFormat serializationFormat = SerializationFormat.getSerializationFormat(serializationFormatAsString);
		final LocalDateTime creationDate = archiveResponse.getCreationDate();
		final LocalDateTime lastModificationDate = archiveResponse.getLastModificationDate();
		final Archive archive = new ArchiveImpl(archiveId, packageType, title, lastModificationDate, creationDate, uuid, serializationFormat);
		final DigitalObjectCollection digitalObjects = getDigitalObjectList(archiveResponse.getDigitalObjects());
		archive.setDigitalObjects(digitalObjects);
		return archive;
	}
	
	/**
	 * Creates reference with the values received from the server. 
	 * @param referenceResponse The values received from the server.
	 * @return The created object.
	 */	
	public static final Reference createReference(final ReferenceResponse referenceResponse)  {
		if (referenceResponse.isLocalFileReference()) {
			final String url = referenceResponse.getUrl();
			final UuidResponse uuidOfFileResponse = referenceResponse.getUuidOfFile();
			final Uuid uuidOfFile = new Uuid(uuidOfFileResponse.toString());
			return getLocalFileReference(url, uuidOfFile);
		} else {
			String url = referenceResponse.getUrl();
			return getRemoteFileReference(url);
		}
	}

	/**
	 * Creates a collection with the data of a reference responses send by the server.
	 * @param archiveResponse The list that contains the data to use.
	 * @return The archive with the data from the archive response.
	 */
	public static final ReferenceCollection createReferenceCollection(final ReferenceListResponse referenceListResponse) {
		final ReferenceCollection references = new ReferenceListCollectionImpl();
		
		for (int i = 0; i < referenceListResponse.getReferenceCount(); i++) {
			final ReferenceResponse referenceResponse = referenceListResponse.getReferenceAt(i);
			final Reference reference = createReference(referenceResponse);
			references.addReference(reference);
		}
		
		return references;
	}
	
	/**
	 * Creates an empty collection with data sources. 
	 * @return The empty collection with data sources.
	 */
	public final DataSourceCollection createDataSourceCollection() {
		return new DataSourceCollectionListImpl();
	}
	
	/**
	 * Creates a copy of an individual. The values of this individual are written in a whole new object. 
	 * @param ontologyIndividual Contains the values that should be copied. 
	 * @return The copy of ontologyIndividual.
	 */
	public final OntologyIndividual createCopyOfOntologyIndividual(final OntologyIndividual ontologyIndividual) {
		final OntologyIndividual copyOfOntologyIndividual = new OntologyIndividualImpl(ontologyIndividual.getOntologyClass());
		copyOfOntologyIndividual.setUuid(ontologyIndividual.getUuid());
		
		for (int i = 0; i < ontologyIndividual.getDatatypePropertiesCount(); i++) {
			final OntologyDatatypeProperty originalDatatypeProperty = ontologyIndividual.getDatatypePropertyAt(i);
			final OntologyDatatypeProperty copyOfDatatypeProperty = propertyFactory.copyOntologyDatatypeProperty(originalDatatypeProperty);
			copyOfOntologyIndividual.addDatatypeProperty(copyOfDatatypeProperty);
		}
		
		return copyOfOntologyIndividual;
	}
	
	/**
	 * Creates a new uuid. 
	 * @param uuid The uuid as string. Must be a valid uuid. 
	 * @return The new uuid object. 
	 */
	public static final Uuid createUuid(final String uuid) {
		return new Uuid(uuid);
	}
	
	/**
	 * Creates a collection with the data of a collection with data source responses send by the server.
	 * @param dataSourceListResponse The archive response that contains the data to use.
	 * @return The data source with the data from the list response.
	 */
	public final DataSourceCollection createDataSourceCollection(final DataSourceListResponse dataSourceListResponse) {
		DataSourceCollection dataSourceCollection = createDataSourceCollection();
		
		for (int i = 0; i < dataSourceListResponse.getDataSources().size(); i++) {
			final DataSourceResponse dataSourceResponse = dataSourceListResponse.getDataSources().get(i);
			final DataSource dataSource = createDataSource(dataSourceResponse);
			dataSourceCollection.addDataSource(dataSource);
		}
		
		return dataSourceCollection;
	}
	
	/**
	 * Creates mediator data with the data of an object send by the server.
	 * @param mediatorDataResponse The response that contains the data to use.
	 * @return The data source with the data from the response.
	 */
	public final MediatorData createMediatorData(final MediatorDataResponse mediatorDataResponse) {
		final String name = mediatorDataResponse.getName();
		final String value = mediatorDataResponse.getValue();
		final MediatorData mediatorData = new MediatorDataImpl(name, value);
		return mediatorData;
	}
	
	/**
	 * Creates a collection with the data of a collection with data source responses send by the server.
	 * @param archiveResponse The archive response that contains the data to use.
	 * @return The data source with the data from the list response.
	 */
	public final MediatorDataCollection createMediatorDataCollection(final MediatorDataListResponse mediatorDataListResponse) {
		final MediatorDataCollection mediatorDataCollection = new MediatorDataCollectionHashMapImpl();
		final List<MediatorDataResponse> mediatorDataResponseList = mediatorDataListResponse.getMediatorData();
		
		for (final MediatorDataResponse mediatorDataResponse: mediatorDataResponseList) {
			final MediatorData mediatorData = createMediatorData(mediatorDataResponse);
			mediatorDataCollection.addMediatorData(mediatorData);
		}
		
		return mediatorDataCollection;
	}
	
	/**
	 * Creates a data source with the data of a list send by the server.
	 * @param dataSourceResponse The data source response that contains the data to use.
	 * @return The data source with the data from the list response.
	 */
	public final DataSource createDataSource(final DataSourceResponse dataSourceResponse) {
		final DataSourceToolListResponse dataSourceToolListResponse = dataSourceResponse.getDataSourceTools();
		final DataSourceToolCollection dataSourceTools = createDataSourceToolCollection(dataSourceToolListResponse);
		final String dataSourceName = dataSourceResponse.getDataSourceName();
		
		return new DataSourceImpl(dataSourceName, dataSourceTools);
	}
	
	/**
	 * Creates a new data source object. 
	 * @param dataSourceName The name of the data source. 
	 * @param dataSourceTools The tools that are contained in the data source. 
	 * @return The new object. 
	 */
	public final DataSource createDataSource(String dataSourceName, DataSourceToolCollection dataSourceTools) {
		return new DataSourceImpl(dataSourceName, dataSourceTools);
	}
	
	/**
	 * Creates a new data source object. 
	 * @param dataSourceName The name of the data source. 
	 * @param dataSourceTools The tools that are contained in the data source. 
	 * @return The new object. 
	 */
	public final DataSourceToolCollection createDataSourceToolCollection(final DataSourceToolListResponse dataSourceToolList) {
		final DataSourceToolCollection dataSourceTools = createEmptyDataSourceToolCollection();
		
		for (int i = 0; i < dataSourceToolList.getDataSourceToolsCount(); i++) {
			final DataSourceToolResponse dataSourceToolResponse = dataSourceToolList.getDataSourceTool(i);
			final DataSourceTool dataSourceTool = createDataSourceTool(dataSourceToolResponse);
			dataSourceTools.addDataSourceToolToList(dataSourceTool);
		}
		
		return dataSourceTools;
	}
	
	/**
	 * Creates an empty collection for data source tools.
	 * @return The collection.
	 */
	public final DataSourceToolCollection createEmptyDataSourceToolCollection() {
		return new DataSourceToolCollectionListImpl();
	}
	
	/**
	 * Creates a data source tool with the data of a response object send by the server.
	 * @param dataSourceToolResponseThe data source tool response that contains the data to use.
	 * @return The data source with the data from the response.
	 */
	public final DataSourceTool createDataSourceTool(final DataSourceToolResponse dataSourceToolResponse) {
		final String toolName = dataSourceToolResponse.getDataSourceToolName();
		final boolean activated = dataSourceToolResponse.isActivated();
		
		DataSourceTool dataSourceTool = createDataSourceTool(toolName, activated);
		
		return dataSourceTool;
	}
	
	/**
	 * Creates a new data source tool object. 
	 * @param toolName The name of the data source tool. 
	 * @param activated True if the tool is activated, false otherwise. 
	 */
	public final DataSourceTool createDataSourceTool(final String toolName, final boolean activated) {
		final DataSourceTool dataSourceTool = new DataSourceToolImpl(toolName, activated);
		return dataSourceTool;
	}
	
	/**
	 * Creates an archive list with the data of an archive list response send by the server.
	 * @param archiveLIstResponse The archive list response that contains the data to use.
	 * @return The archive list with the data from the archive list response.
	 */
	public static final ArchiveList getArchiveList(final ArchiveListResponse archiveListResponse) {
		final ArchiveList archiveList = new ArchiveListImpl();
		
		for (int i = 0; i < archiveListResponse.getArchiveCount(); i++) {
			final ArchiveResponse archiveResponse = archiveListResponse.getArchiveAt(i);
			final Archive archive = getArchive(archiveResponse);
			archiveList.addArchiveToList(archive);
		}
		
		return archiveList;
	}
	
	/**
	 * Creates configuration data with no values. Is used if there was not 
	 * configuration file when reading the configuration and a new 
	 * configuration files without any values was created.
	 * @return Configuration without any data.
	 */
	public static final ClientConfigurationData getEmptyClientConfigurationData() {
		return new ClientConfigurationDataImpl(StringUtil.EMPTY_STRING, StringUtil.EMPTY_STRING);
	}

	/**
	 * Creates configuration data with the given values.
	 * @param mediatorUrl The url of the mediator.
	 * @param serverUrl The url of the server.
	 * @param resultDir The directory that contains the files downloaded from
	 * the server.
	 * @return The new object with the configuration data.
	 */
	public final ClientConfigurationData getClientConfigurationData (
			final String mediatorUrl, final String serverUrl) {
		return new ClientConfigurationDataImpl(serverUrl, mediatorUrl);
	}
	
	/**
	 * Creates a digital object with the data of a digital object response send by the server.
	 * @param digitalObjectResponse The digital object response that contains the data to use.
	 * @return The digital object with the data from the digital object response.
	 */
	public static final DigitalObject getDigitalObject(final DigitalObjectResponse digitalObjectResponse) {
		final String filename = digitalObjectResponse.getFilename();
		final Uuid uuidOfInformationPackage = new Uuid(digitalObjectResponse.getInformationPackageUuid());
		final DigitalObject digitalObject = new DigitalObjectImpl(filename, uuidOfInformationPackage);
		final Set<String> metadataKeySet = digitalObjectResponse.getMetadataMap().keySet();
		for (final String key: metadataKeySet) {
			final String value = digitalObjectResponse.getMetadataMap().get(key);
			digitalObject.addMetadata(key, value);
		}
		
		return digitalObject;
	}
	
	/**
	 * Creates a reference object that points to a local file on the server. 
	 * @param url The path of the file in the storage directory of the server. 
	 * @param uuidOfFile The uuid of the file. 
	 * @return The newly created reference. 
	 */
	public static final Reference getLocalFileReference(final String url,final  Uuid uuidOfFile) {
		final Reference reference = ReferenceImpl.createLocalFileReference(url, uuidOfFile);
		return reference;
	}

	/**
	 * Creates a reference object that points to a file on a remote server. 
	 * @param url The url where the file can be found.  
	 * @return The newly created reference. 
	 */
	public static final Reference getRemoteFileReference(final String url) {
		Reference reference = ReferenceImpl.createRemoteUrlReference(url);
		return reference;
	}
	
	/**
	 * Creates a new digital object with a filename and an uuid of the information package the digital object
	 * belongs to.
	 * @param filename The name of the file that is described by the digital object.
	 * @param uuidOfInformationPackage The uuid of the information package the digital object
	 * belongs to.
	 * @return The new digital object.
	 */
	public final DigitalObject getDigitalObject(final String filename, final Uuid uuidOfInformationPackage) {
		return new DigitalObjectImpl(filename, uuidOfInformationPackage); 
	}
	
	/**
	 * Creates a new digital object with a filename.
	 * @param filename The name of the file that is described by the digital object.
	 * @param uuidOfInformationPackage The uuid of the information package the digital object
	 * belongs to.
	 * @param uuidOfDigitalObject The uuid of the digital object if the uuid is already existing.
	 * @return The new digital object.
	 */
	public static final DigitalObject getDigitalObject(final String filename, final Uuid uuidOfInformationPackage,
			final Uuid uuidOfDigitalObject) {
		return new DigitalObjectImpl(filename, uuidOfInformationPackage, uuidOfDigitalObject); 
	}
	
	/**
	 * Creates a new digital object with a filename.
	 * @param filename The name of the file that is described by the digital object.
	 * @return The new digital object.
	 */
	public static final DigitalObject getDigitalObject(final String filename) {
		return new DigitalObjectImpl(filename); 
	}
	
	/**
	 * Creates an empty list for digital objects.
	 * @return The empty list.
	 */
	public final DigitalObjectCollection getEmptyDigitalObjectList() {
		return new DigitalObjectCollectionImpl();
	}
	
	/**
	 * Creates a digital object with the elements of a digital object response. The digital object contains
	 * all objects that are contained in digitalObjectListResponse.  
	 * @param digitalObjectListResponse Contains the elements to use.
	 * @return The list with all digital objects in digitalObjectListResponse.
	 */
	public static final DigitalObjectCollection getDigitalObjectList(final DigitalObjectListResponse digitalObjectListResponse) {
		final DigitalObjectCollection digitalObjectList = new DigitalObjectCollectionImpl();
		
		for(int i = 0; i < digitalObjectListResponse.getDigitalObjectCount(); i++) {
			final DigitalObjectResponse digitalObjectResponse = digitalObjectListResponse.getDigitalObjectAt(i);
			final DigitalObject digitalObject = getDigitalObject(digitalObjectResponse);
			digitalObjectList.addDigitalObject(digitalObject);
		}
		
		return digitalObjectList;
	}
	
	/**
	 * Creates a metadata element out of a metadata element output. 
	 * @param metadataElementResponse The element that has been received over a webservice.
	 * @return The metadata element.
	 */
	public static final MetadataElement createMetadataElement(
			final MetadataElementResponse metadataElementResponse) {
		return new MetadataElementImpl(metadataElementResponse);
	}
	
	/**
	 * Creates an empty metadata element list.. 
	 * @return The empty metadata element list.
	 */
	public static final MetadataElementList createMetadataElementList() {
		return new MetadataElementListImpl();
	}
	
	/**
	 * Creates a metadata element list out of a metadata element list output. 
	 * @param metadataList The element list that has been received over a webservice.
	 * @return The metadata element list that contains the values of the list that has been
	 * received over a webservice.
	 */
	public static final MetadataElementList createMetadataElementList(
			final MetadataElementListResponse metadataList) {
		return new MetadataElementListImpl(metadataList);
	}
	
	/**
	 * Creates a new reference to a remote file in the internet accessible.
	 * @param url The url.
	 * @return The new reference.
	 */
	public final Reference createRemoteReference(final String url) {
		return ReferenceImpl.createRemoteUrlReference(url);
	}
	
	/**
	 * Creates an ontology class with the properties and the other data of an ontology class response sent by
	 * the server.
	 * @param ontologyClassResponse The data sent by the server.
	 * @return The ontology class containing the same data as ontologyClassResponse.
	 */
	public final OntologyClass createOntologyClass(final OntologyClassResponse ontologyClassResponse) {
		final String namespace = ontologyClassResponse.getNamespace();
		final String localname = ontologyClassResponse.getLocalName();
		final OntologyClass ontologyClass = new OntologyClassImpl(namespace, localname);
		
		for (int i = 0; i < ontologyClassResponse.getSubclassCount(); i++) {
			final OntologyClassResponse subclassResponse = ontologyClassResponse.getSubclassAt(i);
			final OntologyClass subclass = createOntologyClass(subclassResponse, informationPackageManager);
			ontologyClass.addSubClass(subclass);
			informationPackageManager.addOntologyClass(subclass);
		}

		final DatatypePropertyCollection datatypePropertyList = createEmptyDatatypePropertyList();
		for (int i = 0; i < ontologyClassResponse.getPropertiesCount(); i++) {
			final DatatypePropertyResponse propertyResponse = ontologyClassResponse.getPropertyAt(i);
			final OntologyDatatypeProperty datatypeProperty = createDatatypeProperty(propertyResponse);
			datatypePropertyList.addDatatypeProperty(datatypeProperty);
		}
		ontologyClass.addDatatypeProperties(datatypePropertyList);
		
		final TextInLanguageListResponse textInLanguageListResponse = ontologyClassResponse.getLabelTexts();
		
		for (final TextInLanguageResponse textInLanguageResponse: textInLanguageListResponse.getTexts()) {
			final TextInLanguage textInLanguage = createTextInLanguage(textInLanguageResponse);
			ontologyClass.addLabel(textInLanguage);
		}
		   
		// TODO ObjectProperties muessen noch in OntologyClassResponse eingebaut werden.
		/*for (int i = 0; i < ontologyClassResponse.getObjectPropertiesCount(); i++) {
			ObjectPropertyResponse objectPropertyResponse = ontologyClassResponse.getObjectPropertyAt(i);
			OntologyObjectProperty objectProperty = createOntologyObjectProperty(objectPropertyResponse, informationPackageManager);
			ontologyClass.addProperty(objectProperty);
		}*/
		
		return ontologyClass;
	}
	
	/**
	 * Creates an empty collection with datatype properties. 
	 * @return The newly created collection. 
	 */
	public static final DatatypePropertyCollection createEmptyDatatypePropertyList() {
		return new DatatypePropertyCollectionImpl();
	}
	
	/**
	 * Creates an object for an object property.
	 * @param propertyId The iri of the property. 
	 * @param label The label for describing the property in the user interface. If there is no label, the iri is used. 
	 * @param range The range of the property as class. 
	 * @param propertyRange The range of the property as specified in the ontology.
	 * @return
	 */
	public static final OntologyObjectProperty createOntologyObjectProperty(final String propertyId,
			final String label, final OntologyClass range, final String propertyRange) {
		return new OntologyObjectPropertyImpl(propertyId, label, range, propertyRange);
	}
	
	/**
	 * Creates an empty collection with object properties. 
	 * @return The newly created collection. 
	 */
	public static final ObjectPropertyCollection createObjectPropertyList() {
		return new ObjectPropertyCollectionImpl();
	}
	
	/**
	 * Creates a collection with ontology classes with the values received from the server. 
	 * @param ontologyClassListResponse The values received from the server.
	 * @return The created collection. 
	 */
	public final OntologyClassList createOntologyClassList(final OntologyClassListResponse 
			ontologyClassListResponse) {
		final OntologyClassList ontologyClassList = new OntologyClassListImpl();
		
		for (int i = 0; i < ontologyClassListResponse.getOntologyClassCount(); i++) {
			final OntologyClassResponse ontologyClassResponse = ontologyClassListResponse.getOntologyClassAt(i);
			final OntologyClass ontologyClass = createOntologyClass(ontologyClassResponse);
			ontologyClassList.addOntologyClassToList(ontologyClass);
		}
		
		return ontologyClassList;
	}
	
	/**
	 * Creates an empty map for ontology classes
	 * @return The newly created map. 
	 */
	public static final OntologyClassesMap createEmptyOntologyClassesMap() {
		return new OntologyClassesHashMap();
	}
	
	/**
	 * Creates an ontology class with the values received from the server. 
	 * @param ontologyClassResponse Contains the values received from the server.
	 * @param informationPackageManager Contains information about the information package that is currently
	 * being created. 
	 * @return The created object.
	 */
	public final OntologyClass createOntologyClass(final OntologyClassResponse ontologyClassResponse, 
			final InformationPackageManager informationPackageManager) {
		final String namespace = ontologyClassResponse.getNamespace();
		final String localname = ontologyClassResponse.getLocalName();
		final OntologyClass ontologyClass = new OntologyClassImpl(namespace, localname);
		
		for (int i = 0; i < ontologyClassResponse.getSubclassCount(); i++) {
			final OntologyClassResponse subclassResponse = ontologyClassResponse.getSubclassAt(i);
			final OntologyClass subclass = createOntologyClass(subclassResponse, informationPackageManager);
			ontologyClass.addSubClass(subclass);
			informationPackageManager.addOntologyClass(subclass);
		}

		for (int i = 0; i < ontologyClassResponse.getPropertiesCount(); i++) {
			final DatatypePropertyResponse propertyResponse = ontologyClassResponse.getPropertyAt(i);
			final OntologyDatatypeProperty datatypeProperty = (OntologyDatatypeProperty)createOntologyDatatypeProperty(propertyResponse);
			ontologyClass.addDatatypeProperty(datatypeProperty);
		}
		   
		for (int i = 0; i < ontologyClassResponse.getObjectPropertiesCount(); i++) {
			final ObjectPropertyResponse objectPropertyResponse = ontologyClassResponse.getObjectPropertyAt(i);
			final String rangeClassName = objectPropertyResponse.getRange();
			final OntologyClass propertyRange = informationPackageManager.findOntologyClassByIri(rangeClassName);
			final OntologyObjectProperty objectProperty = createOntologyObjectProperty(objectPropertyResponse.getPropertyName(),
					objectPropertyResponse.getLabel(), propertyRange, objectPropertyResponse.getRange());
			ontologyClass.addObjectProperty(objectProperty);
		}
		
		final TextInLanguageListResponse textInLanguageListResponse = ontologyClassResponse.getLabelTexts();
		for (final TextInLanguageResponse textInLanguageResponse: textInLanguageListResponse.getTexts()) {
			final TextInLanguage textInLanguage = createTextInLanguage(textInLanguageResponse);
			ontologyClass.addLabel(textInLanguage);
		}

		return ontologyClass;
	}
	
	/**
	 * Creates an object that represents a duration in an ontology. 
	 * @return The newly created object. 
	 */
	public static final OntologyDuration createOntologyDuration() {
		return new OntologyDurationImpl();
	}
	
	/**
	 * Creates an object that represents an individual in an ontology.
	 * @param ontologyClass The class the property is assigned to. 
	 * @return The newly created object. 
	 */
	public static final OntologyIndividual createOntologyIndividual(final OntologyClass ontologyClass) {
		return new OntologyIndividualImpl(ontologyClass);
	}
	
	/**
	 * Creates an object that represents an individual in an ontology.
	 * @param iri The iri of the individual. 
	 * @return The newly created object. 
	 */
	public static final OntologyIndividual createOntologyIndividual(final Iri iri) {
		return new OntologyIndividualImpl(iri);
	}
	
	/**
	 * Creates an object that represents an individual in an ontology with the values received from the server.  
	 * @param individualResponse  Contains the values received from the server. 
	 * @param informationPackageManager Contains information about the information package that is currently
	 * being created. 
	 * @return The newly created individual. 
	 */
	public final OntologyIndividual createOntologyIndividual(IndividualResponse individualResponse, 
			InformationPackageManager informationPackageManager) {
		final String classIri = individualResponse.getClassIri();
		final OntologyClass ontologyClass = informationPackageManager.findOntologyClassByIri(classIri);
		final UuidResponse uuidResponse = individualResponse.getUuid();
		final Uuid uuid = new Uuid(uuidResponse.getUuid());
		final DatatypePropertyListResponse propertyListResponse = individualResponse.getOntologyDatatypeProperties();

		final DatatypePropertyCollection datatypeProperties = createDatatypePropertyList(propertyListResponse);
		final ObjectPropertyListResponse objectPropertyListResponse = individualResponse.getOntologyObjectProperties();
		final ObjectPropertyCollection objectProperties = createObjectPropertyList(objectPropertyListResponse, informationPackageManager);
		final OntologyIndividual ontologyIndividual = new OntologyIndividualImpl(ontologyClass, datatypeProperties, uuid);
		ontologyIndividual.setObjectProperties(objectProperties);
		
		final IriResponse iriOfIndividualResponse = individualResponse.getIriOfIndividual();
		if (iriOfIndividualResponse != null) {
			final Iri iriOfIndividual = createIri(iriOfIndividualResponse.toString());
			ontologyIndividual.setIriOfIndividual(iriOfIndividual);
		}
		return ontologyIndividual;
	}
	
	/**
	 * Creates an empty collection with individuals. 
	 * @return The newly created collection. 
	 */
	public static final OntologyIndividualCollection createEmptyOntologyIndividualList() {
		return new OntologyIndividualCollectionImpl();
	}

	/**
	 * Creates a collection with individuals with the values received from the server. 
	 * @param individualListResponse  The values received from the server.
	 * @return The newly created collection. 
	 */
	public final OntologyIndividualCollection createOntologyIndividualList(final IndividualListResponse individualListResponse,
			final InformationPackageManager informationPackageManager) {
		final OntologyIndividualCollection individualList = new OntologyIndividualCollectionImpl();
		
		for (int i = 0; i < individualListResponse.getIndividualCount(); i++) {
			final IndividualResponse individualResponse = individualListResponse.getIndividualAt(i);
			final OntologyIndividual ontologyIndividual = createOntologyIndividual(individualResponse, informationPackageManager);
			individualList.addIndividual(ontologyIndividual);
		}
		
		return individualList;
	}
	
	/**
	 * Creates a collection with taxonomy individuals with the values received from the server. 
	 * @param individualListResponse  The values received from the server.
	 * @return The newly created collection. 
	 */
	public final TaxonomyIndividualCollection createTaxonomyIndividualCollection(final TaxonomyIndividualListResponse individualListResponse) {
		final TaxonomyIndividualCollection individualList = new TaxonomyIndividualListImpl();
		
		for (int i = 0; i < individualListResponse.getIndividuals().size(); i++) {
			final TaxonomyIndividualResponse taxonomyIndividualResponse = individualListResponse.getIndividuals().get(i);
			final TaxonomyIndividual taxonomyIndividual = createTaxonomyIndividual(taxonomyIndividualResponse);
			individualList.addTaxonomyIndividual(taxonomyIndividual);
		}
		
		return individualList;
	}
	
	public static final TaxonomyIndividualsAndTheirIrisMap createEmptyTaxonomyIndividualsAndTheirIrisMap() {
		return new TaxonomyIndividualsAndTheirIrisHashMap();
	}
	
	/**
	 * Creates an empty collection with properties. 
	 * @return The newly created collection. 
	 */
	public static final PropertyCollection createPropertyList() {
		return new PropertyCollectionImpl();
	}
	
	/**
	 * Creates a collection with datatype properties with the values received from the server. 
	 * @param propertyListResponse  The values received from the server.
	 * @return The newly created collection. 
	 */
	public final DatatypePropertyCollection createDatatypePropertyList(final DatatypePropertyListResponse propertyListResponse) {
		final DatatypePropertyCollection propertyList = new DatatypePropertyCollectionImpl();
		
		if (propertyListResponse != null) {
			for (int i = 0; i < propertyListResponse.getOntologyPropertyCount(); i++) {
				final DatatypePropertyResponse propertyResponse = propertyListResponse.getOntologyPropertyAt(i);
				final OntologyDatatypeProperty ontologyProperty = (OntologyDatatypeProperty)createOntologyDatatypeProperty(propertyResponse);
				propertyList.addDatatypeProperty(ontologyProperty);
			}
		}
		
		return propertyList;
	}
	
	/**
	 * Creates a collection with object properties with the values received from the server. 
	 * @param propertyListResponse  The values received from the server.
	 * @param  informationPackageManager Contains information about the information package that is currently
	 * being created. 
	 * @return The newly created collection. 
	 */
	public final ObjectPropertyCollection createObjectPropertyList(final ObjectPropertyListResponse propertyListResponse, 
			final InformationPackageManager informationPackageManager) {
		final ObjectPropertyCollection propertyList = new ObjectPropertyCollectionImpl();
		
		if (propertyListResponse != null) {
			for (int i = 0; i < propertyListResponse.getObjectPropertyCount(); i++) {
				final ObjectPropertyResponse propertyResponse = propertyListResponse.getObjectPropertyAt(i);
				final OntologyObjectProperty ontologyProperty = createObjectProperty(propertyResponse, informationPackageManager);
				propertyList.addObjectProperty(ontologyProperty);
			}
		}
		
		return propertyList;
	}
	
	/**
	 * Creates a new object that represents an object property. Only the iris and the class of the individuals that
	 * are contained as values in the object property are saved in the resulting object.
	 * @param objectPropertyResponse The values received from the server.
	 * @param  informationPackageManager Contains information about the information package that is currently
	 * being created. 
	 * @return The new object.
	 */
	public final OntologyObjectProperty createObjectProperty(final ObjectPropertyResponse objectPropertyResponse, 
			final InformationPackageManager informationPackageManager) {
		final String propertyId = objectPropertyResponse.getPropertyName();
		final String label = objectPropertyResponse.getLabel();
		final String propertyRange = objectPropertyResponse.getRange();
		//final OntologyClass range = informationPackageManager.findOntologyClassByIri(propertyRange);
		final OntologyObjectProperty ontologyObjectProperty = new OntologyObjectPropertyImpl(propertyId, label, null, propertyRange);
		
		final IriListResponse irisOfValuesResponse = objectPropertyResponse.getValue();
		for (final IriResponse iriResponse: irisOfValuesResponse.getIris()) {
			final Iri iri = ClientModelFactory.createIri(iriResponse.toString());
			final OntologyIndividual ontologyIndividual = ClientModelFactory.createOntologyIndividual(iri);
			ontologyIndividual.setIriOfIndividual(iri);
			ontologyObjectProperty.addOntologyIndividual(ontologyIndividual);
		}
		
		return ontologyObjectProperty;
	}
	
	/**
	 * Creates a datatype property with the values received from the server. 
	 * @param propertyResponse The values received from the server.
	 * @return The new object.
	 */
	public final OntologyProperty createOntologyDatatypeProperty(final DatatypePropertyResponse propertyResponse) {
		final OntologyProperty ontologyProperty =  propertyFactory.getProperty(propertyResponse);
		final TextInLanguageListResponse textInLanguageListResponse = propertyResponse.getLabelTexts();
		
		for (final TextInLanguageResponse textInLanguageResponse: textInLanguageListResponse.getTexts()) {
			final TextInLanguage textInLanguage = createTextInLanguage(textInLanguageResponse);
			ontologyProperty.addLabel(textInLanguage);
		}
		
		return ontologyProperty;
	}
	
	/**
	 * Creates a datatype property with the values received from the server. 
	 * @param datatypePropertyResponse The values received from the server.
	 * @return The new object.
	 */
	public final OntologyDatatypeProperty createDatatypeProperty(final DatatypePropertyResponse datatypePropertyResponse) {
		return (OntologyDatatypeProperty)propertyFactory.getProperty(datatypePropertyResponse);
	}
	
	/**
	 * Creates an object with the configuration data of the server.
	 * @param workingDirectoryPath The path of the working directory on the server.
	 * @return An object with the configuration data for the server. 
	 */
	public static final ServerConfigurationData createServerConfigurationData(
			final String workingDirectoryPath) {
		return new ServerConfigurationDataImpl(workingDirectoryPath);
	}
	
	/**
	 * Creates an information package object that represents either a submission information package or
	 * a submission information unit. 
	 * @param title The title of the information package.
	 * @param packageType The type of the information package: is it a submission information package or
	 * a submission information unit?
	 * @return The object that represents the information package.
	 */
	public static final BasicInformationPackage createInformationPackage(final String title, 
			final PackageType packageType) {
		final Uuid uuid = PackagingToolkitModelFactory.getUuid();
		if (packageType.equals(PackageType.SIP)) {
			return new SubmissionInformationPackageImpl(title, uuid);
		} else {
			return new SubmissionInformationUnitImpl(title, uuid, new DigitalObjectCollectionImpl());
		}
	}
	
	/**
	 * Creates a view that represents the user's decision to create a new view. This is necessary 
	 * to avoid the use of a null value.
	 * @return The new view that contains default values as id and name.
	 */
	public static final View createNewView() {
		return new NewViewImpl();
	}
	
	/**
	 * Creates a view object.
	 * @param viewName The name of the view.
	 * @param viewId The id of the view.
	 * @return The created view object.
	 */
	public static final View createView(final String viewName, final int viewId) {
		return new ViewImpl(viewName, viewId);
	}
	
	/**
	 * Creates a view object without an id. 
	 * @param viewName The name of the view.
	 * @return The created view object.
	 */
	public static final View createView(final String viewName) {
		return new ViewImpl(viewName);
	}
	
	/**
	 * Creates a view object with the data that are contained in a view response object that
	 * has been send by the server.
	 * @param viewResponse The object that has been send by the server.
	 * @return The created view object.
	 */
	public final View createView(ViewResponse viewResponse) {
		final OntologyClassListResponse ontologyClassListResponse = viewResponse.getOntologyClassList();
		final OntologyClassList containedOntologyClasses = createOntologyClassList(ontologyClassListResponse);
		final String viewName = viewResponse.getViewName();
		final int viewId = viewResponse.getViewId();
		final View view = new ViewImpl(viewName, viewId);
		view.addOntologyClasses(containedOntologyClasses);
		return view;
	}
	
	/**
	 * Creates a view list object with the data that are contained in a view list response object that
	 * has been send by the server.
	 * @param viewListResponse The object that has been send by the server.
	 * @return The created view list object.
	 */
	public final ViewList createViewList(final ViewListResponse viewListResponse) {
		final ViewList viewList = new ViewListImpl();
		
		for (int i = 0; i < viewListResponse.getViewCount(); i++) {
			final ViewResponse viewResponse = viewListResponse.getViewAt(i);
			final View view = createView(viewResponse);
			viewList.addViewToList(view);
		}
		
		return viewList;
	}

	/**
	 * Sets a reference to the object that creates properties. 
	 * @param propertyFactory A reference to the object that creates properties. 
	 */
	public void setPropertyFactory(final PropertyFactory propertyFactory) {
		this.propertyFactory = propertyFactory;
	}

	/**
	 * Sets a reference to the object that contains important information about the information package that is
	 * being created. 
	 * @param informationPackageManager A reference to the object that contains important information about the information package that is
	 * being created.
	 */
	public void setInformationPackageManager(final InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}

	/**
	 * Creates an object that contains a text in a certain language.
	 * @param text The text that is contained in this object.
	 * @param language The language text is written in.
	 * @return The created object.
	 */
	public static TextInLanguage createTextInLanguage(final String text, final Locale language) {
		final TextInLanguage textInLanguage = new TextInLanguageImpl(text, language);
		return textInLanguage;
	}
	
	/**
	 * Creates an object that contains a text in a certain language.
	 * @param textInLanguageResponse An object received from the server.
	 * @return The created object.
	 */
	public static TextInLanguage createTextInLanguage(final TextInLanguageResponse textInLanguageResponse) {
		final String text = textInLanguageResponse.getText();
		final String language = textInLanguageResponse.getLanguage();
		final Locale locale = Locale.forLanguageTag(language);
		final TextInLanguage textInLanguage = new TextInLanguageImpl(text, locale);
		return textInLanguage;
	}
	
	/**
	 * Creates an object that contains a collection with taxonomies. 
	 * @param taxonomyList An object received from the server.
	 * @return The created object.
	 */
	public static TaxonomyCollection createTaxonomyCollection(final TaxonomyListResponse taxonomyList) {
		final TaxonomyCollection taxonomies = createEmptyTaxonomyCollection();
		final List<TaxonomyResponse> taxonomyResponses = taxonomyList.getTaxonomies();
		
		for(int i = 0; i < taxonomyResponses.size(); i++) {
			final  TaxonomyResponse taxonomyResponse = taxonomyResponses.get(i);
			final String title = taxonomyResponse.getTitle();
			final String namespaceAsString = taxonomyResponse.getNamespace();
			final TaxonomyIndividualResponse rootIndividualResponse = taxonomyResponse.getRootIndividual();
			final TaxonomyIndividual rootIndividual = createTaxonomyIndividual(rootIndividualResponse);
			final Taxonomy taxonomy = createTaxonomy(rootIndividual, title, namespaceAsString);
			taxonomies.addTaxonomy(taxonomy);
		}
		
		return taxonomies;
	}

	/**
	 * Creates an empty collection for taxonomies. 
	 * @return The created object.
	 */
	public static TaxonomyCollection createEmptyTaxonomyCollection() {
		return new TaxonomyListImpl();
	}

	/**
	 * Creates an object that contains a taxonomy. 
	 * @param rootIndividual The individual that represents the root individual for the taxonomy.
	 * @param title The title of the taxonomy.
	 * @param namespaceAsString The namespace of the taxonomy.
	 * @return The created object.
	 */
	public static Taxonomy createTaxonomy(final TaxonomyIndividual rootIndividual, final String title, final String namespaceAsString) {
		final Namespace namespace = createNamespace(namespaceAsString);
		return new TaxonomyImpl(rootIndividual, title, namespace);
	}
	
	/**
	 * Creates an object that contains an individual in a taxonomy. 
	 * @param iriAsString The iri of this individual.
	 * @param preferredLabel The preferred label of this individual.
	 * @return The created object.
	 */
	public static TaxonomyIndividual createTaxonomyIndividual(final String iriAsString, final String preferredLabel) {
		final Iri iri = createIri(iriAsString);
		return new TaxonomyIndividualImpl(iri, preferredLabel);
	}
	
	/**
	 * Creates an object that contains an individual in a taxonomy. 
	 * @param iriAsString The iri of this individual.
	 * @return The created object.
	 */
	public static TaxonomyIndividual createTaxonomyIndividual(final String iriAsString) {
		final Iri iri = createIri(iriAsString);
		return new TaxonomyIndividualImpl(iri);
	}
	
	/**
	 * Creates an object that contains an individual in a taxonomy. 
	 * @param iri The iri of this individual.
	 * @return The created object.
	 */
	public static TaxonomyIndividual createTaxonomyIndividual(final Iri iri) {
		return new TaxonomyIndividualImpl(iri);
	}
	
	/**
	 * Creates an object that contains an individual in a taxonomy. 
	 * @param taxonomyIndividualResponse An object received from the server.
	 * @return The created object.
	 */
	public static TaxonomyIndividual createTaxonomyIndividual(final TaxonomyIndividualResponse taxonomyIndividualResponse) {
		final TaxonomyIndividual taxonomyIndividual = createTaxonomyIndividual(taxonomyIndividualResponse.getIri(), 
				taxonomyIndividualResponse.getPreferredLabel());
		final TaxonomyIndividualListResponse narrowersResponse = taxonomyIndividualResponse.getNarrowers();
		final List<TaxonomyIndividualResponse> narrowersList = narrowersResponse.getIndividuals();
		
		for (final TaxonomyIndividualResponse narrowerResponse: narrowersList) {
			final TaxonomyIndividual narrower = createTaxonomyIndividual(narrowerResponse);
			taxonomyIndividual.addNarrower(narrower);
		}
		
		return taxonomyIndividual;
	}
	
	/**
	 * Creates an object that contains a namespace. 
	 * @param namespace A string representing the namespace.
	 * @return The created object.
	 */
	public static Namespace createNamespace(final String namespace) {
		return new NamespaceImpl(namespace);
	}
	
	/**
	 * Creates an object that contains an iri. 
	 * @param namespace A string representing the iri.
	 * @return The created object.
	 */
	public static Iri createIri(final String iriAsString) {
		return new IriImpl(iriAsString);
	}
	
	/**
	 * Creates an object that contains an iri. 
	 * @param namespace The namespace contained in the iri.
	 * @param localName The local name contained in the iri.
	 * @return The created object.
	 */
	public static Iri createIri(final Namespace namespace, final LocalName localName) {
		return new IriImpl(namespace, localName);
	}

	/**
	 * Creates an object that contains a local name. 
	 * @param namespace A string representing the local name.
	 * @return The created object.
	 */
	public static LocalName createLocalName(String localnameAsString) {
		return new LocalNameImpl(localnameAsString);
	}

	/**
	 * Gets the namespace that is used as default in the PackagingToolkit.
	 * @return The default namespace.
	 */
	public static final Namespace getPackagingToolkitDefaultNamespace() {
		return createNamespace(PackagingToolkitConstants.NAMESPACE_PACKACKING_TOOLKIT);
	}
}