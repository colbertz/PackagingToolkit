package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElement;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElementList;
import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementResponse;

public class MetadataElementListImpl extends AbstractList implements MetadataElementList {
	private static final String METHOD_GET_METADATA_ELEMENT_AT = 
			"MetadataElement getMetadataElement(int index)";
	private static final String METHOD_ADD_METADATA = "void addMetadata(MetadataElement metadata)";
	
	public MetadataElementListImpl() {
	
	}
	
	public MetadataElementListImpl(MetadataElementListResponse metadataElementList) {
		for (int i = 0; i < metadataElementList.getMetadataCount(); i++) {
			MetadataElementResponse metadata = metadataElementList.getMetadataAt(i);
			MetadataElement metadataElement = ClientModelFactory.createMetadataElement(metadata);
			
			//try {
				super.add(metadataElement);
			/*} catch (ListElementMayNotBeNullException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}

	@Override
	public void addMetadata(MetadataElement metadata) {
		//try {
			super.add(metadata);
		/*} catch (ListElementMayNotBeNullException e) {
			//TODO wie reagieren?
		}*/
	}
	
	@Override
	public MetadataElement getMetadataElementAt(int index) {
		//try {
			return (MetadataElement)super.getElement(index);
		/*} catch (IndexNotInRangeException e) {
			throw new ListIndexInvalidException(index, this.getClass().getName(), 
					   METHOD_GET_METADATA_ELEMENT_AT, 
					   PackagingToolkitComponent.CLIENT);
		}*/
	}
	
	@Override
	public int getMetadataCount() {
		return super.getSize();
	} 
}
