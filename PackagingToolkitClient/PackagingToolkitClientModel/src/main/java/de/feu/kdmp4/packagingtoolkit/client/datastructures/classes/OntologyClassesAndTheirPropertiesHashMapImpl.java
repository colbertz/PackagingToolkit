package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.OntologyClassesAndTheirPropertiesMap;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.ObjectPropertyCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;

public class OntologyClassesAndTheirPropertiesHashMapImpl implements OntologyClassesAndTheirPropertiesMap {
	/**
	 * Contains the datatype properties. 
	 */
	private Map<String, DatatypePropertyCollection> datatypePropertiesMap;
	/**
	 * Contains the object properties. 
	 */
	private Map<String, ObjectPropertyCollection> objectPropertiesMap;
	
	/**
	 * Creates a new object and initializes the maps.
	 */
	public OntologyClassesAndTheirPropertiesHashMapImpl() {
		datatypePropertiesMap = new HashMap<>();
		objectPropertiesMap = new HashMap<>();
	}
	
	@Override
	public void addClass(final OntologyClass ontologyClass) {
		addDatatypeProperties(ontologyClass);
		addObjectProperties(ontologyClass);
	}
	
	/**
	 * Inserts the datatype properties of a class in the map. 
	 * @param ontologyClass The class whose datatype properties should be inserted in the map.
	 */
	private void addDatatypeProperties(final OntologyClass ontologyClass) {
		final String className = ontologyClass.getFullName();
		final DatatypePropertyCollection datatypeProperties = OntologyModelFactory.createDatatypePropertyList();
		
		if (ontologyClass.hasDatatypeProperties()) {
			for (int i = 0; i < ontologyClass.getDatatypePropertiesCount(); i++) {
				final OntologyDatatypeProperty ontologyProperty = ontologyClass.getDatatypeProperty(i);
				datatypeProperties.addDatatypeProperty(ontologyProperty);
			}
		}
		datatypePropertiesMap.put(className, datatypeProperties);
	}
	
	/**
	 * Inserts the object properties of a class in the map. 
	 * @param ontologyClass The class whose object properties should be inserted in the map.
	 */
	private void addObjectProperties(final OntologyClass ontologyClass) {
		final String className = ontologyClass.getFullName();
		final ObjectPropertyCollection objectProperties = OntologyModelFactory.createObjectPropertyList();
		
		if (ontologyClass.hasObjectProperties()) {
			for (int i = 0; i < ontologyClass.getObjectPropertiesCount(); i++) {
				final OntologyObjectProperty ontologyProperty = ontologyClass.getObjectProperty(i);
				objectProperties.addObjectProperty(ontologyProperty);
			}
		}
		objectPropertiesMap.put(className, objectProperties);
	}
	
	@Override
	public void addProperty(final String className, final OntologyProperty ontologyProperty) {
		if (ontologyProperty instanceof OntologyDatatypeProperty) {
			DatatypePropertyCollection datatypePropertyList = datatypePropertiesMap.get(className); 
			if (datatypePropertyList == null) {
				datatypePropertyList = OntologyModelFactory.createDatatypePropertyList();
			}
			
			final OntologyDatatypeProperty ontologyDatatypeProperty = (OntologyDatatypeProperty)ontologyProperty;
			datatypePropertyList.addDatatypeProperty(ontologyDatatypeProperty);
			datatypePropertiesMap.put(className, datatypePropertyList);
		} else if (ontologyProperty instanceof OntologyObjectProperty) {
			ObjectPropertyCollection objectProperties = objectPropertiesMap.get(className);
			if (objectProperties == null) {
				objectProperties = new ObjectPropertyCollectionImpl();
			}
			
			final OntologyObjectProperty ontologyObjectProperty = (OntologyObjectProperty)ontologyProperty;
			objectProperties.addObjectProperty(ontologyObjectProperty);
			objectPropertiesMap.put(className, objectProperties);
		}
	}

	@Override
	public DatatypePropertyCollection getDatatypeProperties(final String className) {
		return datatypePropertiesMap.get(className);
	}
	
	@Override
	public ObjectPropertyCollection getObjectProperties(final String className) {
		return objectPropertiesMap.get(className);
	}
	
	@Override
	public int getDatatypePropertyListsCount() {
		return datatypePropertiesMap.size();
	}
	
	@Override
	public int getObjectPropertyListsCount() {
		return objectPropertiesMap.size();
	}
}
