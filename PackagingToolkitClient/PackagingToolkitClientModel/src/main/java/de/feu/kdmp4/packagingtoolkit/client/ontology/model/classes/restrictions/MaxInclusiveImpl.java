package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;

@SuppressWarnings("rawtypes")
public class MaxInclusiveImpl extends AbstractRestriction implements MaxInclusive {
	private RestrictionValue restrictionValue;
	
	public MaxInclusiveImpl() {
	}
	
	public MaxInclusiveImpl(RestrictionValue restrictionValue) {
		this.restrictionValue = restrictionValue;
	}

	/*@Override
	public void checkRestriction(BigDecimal value) {
		
	}*/
	
	@Override
	public void checkRestriction(RestrictionValue value) {
		if (!isSatisfied(value)) {
			RestrictionNotSatisfiedException exception = RestrictionNotSatisfiedException.
					maxRestrictionNotSatisfied(value.toString(), 
							restrictionValue.toString());
			throw exception;
		}
	}

	@Override
	public RestrictionValue getRestrictionValue() {
		return restrictionValue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isSatisfied(RestrictionValue value) {
		if (restrictionValue.compare(value) == 1) {
			return true;
		} else if (restrictionValue.compare(value) == 0) {
			return true;
		} else {
			return false;
		}
	}
}
