package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.io.Serializable;
import java.util.Locale;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguageList;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

public class TextInLanguageListImpl extends AbstractList implements TextInLanguageList, Serializable {
	private static final long serialVersionUID = -2774754858876620076L;

	@Override
	public void addTextInLanguage(final TextInLanguage textInLanguage) {
		super.add(textInLanguage);
	}
	
	@Override
	public void addTextInLanguage(final String text, final Locale language) {
		final TextInLanguage textInLanguage = ClientModelFactory.createTextInLanguage(text, language);
		super.add(textInLanguage);
	}
	
	@Override
	public TextInLanguage getTextInLanguage(final int index) {
		return (TextInLanguage)super.getElement(index);
	}
	
	@Override
	public Optional<TextInLanguage> findTextByLanguage(final Locale locale) {
		int i = 0;
		boolean found = false;
		TextInLanguage foundTextInLanguage = null;
		
		while (i < getSize() && found == false) {
			final TextInLanguage textInLanguage = getTextInLanguage(i);
			final Locale otherLocale = textInLanguage.getLanguage();
			final String language = locale.getLanguage();
			final String otherLanguage = otherLocale.getLanguage();
			
			if (otherLanguage.equals(language)) {
				found = true;
				foundTextInLanguage = textInLanguage;
			}
			i++;
		}
		
		if (foundTextInLanguage == null) {
			return ClientOptionalFactory.createEmptyOptionalWithTextInLanguage();
		} else {
			return ClientOptionalFactory.createOptionalWithTextInLanguage(foundTextInLanguage);
		}
	}
	
	@Override
	public int getTextsInLanguageCount() {
		return super.getSize();
	}
	
	@Override
	public boolean isEmpty() {
		if (getTextsInLanguageCount() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
