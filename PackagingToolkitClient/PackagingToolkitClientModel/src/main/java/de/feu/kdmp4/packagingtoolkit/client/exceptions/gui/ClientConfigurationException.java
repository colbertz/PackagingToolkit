package de.feu.kdmp4.packagingtoolkit.client.exceptions.gui;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.client.utils.StringUtil;
import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;

/**
 * Is thrown if there are errors during the configuration process on
 * the client. The reason are user errors.
 * @author Christopher Olbertz
 *
 */
public class ClientConfigurationException extends UserException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8907743231901678006L;

	public ClientConfigurationException(String message) {
		super(message);
	}

	public static ClientConfigurationException createOntologyDoesNotExistException(
			String filename) {
		String message = I18nExceptionUtil.getOntologyDoesNotExistString();
		message = String.format(message, filename);
		return new ClientConfigurationException(message);
	}
	
	public static ClientConfigurationException createWorkingDirectoryMayNotBeEmptyException(
			String theClass, String theMethod) {
		String message = I18nExceptionUtil.getWorkingDirectoryMayNotBeEmptyString();
		return new ClientConfigurationException(message);
	}
	
	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

}
