package de.feu.kdmp4.packagingtoolkit.client.exceptions.informationpackages;

import de.feu.kdmp4.packagingtoolkit.client.utils.StringUtil;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;

public class InformationPackageCreationException extends UserException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9105369727995545817L;
	
	private InformationPackageCreationException(final String theMessage) {
		super(theMessage);
	}
	
	public static InformationPackageCreationException createDigitalObjectCountSipException() {
		String message = I18nExceptionUtil.getConfigurationFileDoesNotExistString();
		return new InformationPackageCreationException(message);
	}
	
	public static InformationPackageCreationException createFilenameMayNotBeEmptyException() {
		String message = I18nExceptionUtil.getFilenameMayNotBeEmptyString();
		return new InformationPackageCreationException(message);
	}

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*public static InformationPackageCreationException createDigitalObjectAlreadyUploadedException(
			String filename) {
		String message = I18nExceptionUtil.getFileAlreadyUploadedString();
		return new InformationPackageCreationException(message, filename);
	}*/

}