package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import java.util.ArrayList;
import java.util.List;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackagePath;

public class InformationPackagePathImpl implements InformationPackagePath {
	private List<String> pathComponents;
	
	public InformationPackagePathImpl() {
		pathComponents = new ArrayList<>();
	}
	
	@Override
	public void addPathComponent(String pathComponent) {
		pathComponents.add(pathComponent);
	}
	
	@Override
	public void deleteLastPathComponent() {
		pathComponents.remove(pathComponents.size());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pathComponents == null) ? 0 : pathComponents.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InformationPackagePathImpl other = (InformationPackagePathImpl) obj;
		if (pathComponents == null) {
			if (other.pathComponents != null)
				return false;
		} else if (!pathComponents.equals(other.pathComponents))
			return false;
		return true;
	}
}
