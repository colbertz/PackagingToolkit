/**
 * Contains the implementations of the interfaces for the data structures.
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;