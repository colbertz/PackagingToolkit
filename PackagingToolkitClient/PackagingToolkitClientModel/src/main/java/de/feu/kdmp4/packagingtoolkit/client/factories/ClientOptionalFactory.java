package de.feu.kdmp4.packagingtoolkit.client.factories;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.OntologyIndividualFinishedDecorator;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;

/**
 * Encapsulates the creation of objects of {@link java.util.Optional}. The reason for this class is to remove the need
 * of knowing how Optionals are created. 
 * @author Christopher Olbertz
 *
 */
public class ClientOptionalFactory {
	/**
	 * Creates an optional with a digital object.
	 * @param datatypeProperty The digital object that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<DigitalObject> createDigitalObjectOptional(final DigitalObject digitalObject) {
		return Optional.of(digitalObject);
	}
	
	/**
	 * Creates an empty optional for a digital object.
	 * @return The created empty Optional.
	 */
	public final static Optional<DigitalObject> createEmptyDigitalObjectOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a taxonomy individual.
	 * @param datatypeProperty The taxonomy individual that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TaxonomyIndividual> createTaxonomyIndividualOptional(final TaxonomyIndividual taxonomyIndividual) {
		return Optional.of(taxonomyIndividual);
	}
	
	/**
	 * Creates an empty optional for a taxonomy individual.
	 * @return The created empty Optional.
	 */
	public final static Optional<TaxonomyIndividual> createEmptyTaxonomyIndividualOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a datatype property.
	 * @param datatypeProperty The datatype property that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyDatatypeProperty> createDatatypePropertyOptional(final OntologyDatatypeProperty datatypeProperty) {
		return Optional.of(datatypeProperty);
	}
	
	/**
	 * Creates an empty optional for a datatype property.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyDatatypeProperty> createEmptyDatatypePropertyOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an individual list.
	 * @param datatypeProperty The individual list that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyIndividualCollection> createOntologyIndividualListOptional(
			final OntologyIndividualCollection ontologyIndividualList) {
		return Optional.of(ontologyIndividualList);
	}
	
	/**
	 * Creates an empty optional for an individual list property.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyIndividualCollection> createEmptyOntologyIndividualListOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an object property.
	 * @param datatypeProperty The object property that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyObjectProperty> createObjectPropertyOptional(final OntologyObjectProperty objectProperty) {
		return Optional.of(objectProperty);
	}
	
	/**
	 * Creates an empty optional for an object property.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyObjectProperty> createEmptyObjectPropertyOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an exchange object for labels.
	 * @param textInLanguage The object that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TextInLanguage> createOptionalWithTextInLanguage(final TextInLanguage textInLanguage) {
		return Optional.of(textInLanguage);
	}
	
	/**
	 * Creates an empty optional for an exchange object for labels.
	 * @return The created empty Optional.
	 */
	public final static Optional<TextInLanguage> createEmptyOptionalWithTextInLanguage() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a collection with ontology classes.
	 * @param ontologyClasses The collection that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyClassList> createOntologyClassListOptional(final OntologyClassList ontologyClassList) {
		return Optional.of(ontologyClassList);
	}
	
	/**
	 * Creates an empty optional for a collection with ontology classes.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyClassList> createEmptyOntologyClassListOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a reference.
	 * @param reference The reference that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<Reference> createReferenceOptional(final Reference reference) {
		return Optional.of(reference);
	}
	
	/**
	 * Creates an empty optional for a reference.
	 * @return The created empty Optional.
	 */
	public final static Optional<Reference> createEmptyReferenceOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a OntologyIndividualFinishedDecorator.
	 * @param ontologyIndividualFinishedDecorator The OntologyIndividualFinishedDecorator that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<OntologyIndividualFinishedDecorator> createOntologyIndividualFinishedDecoratorOptional(final 
			OntologyIndividualFinishedDecorator ontologyIndividualFinishedDecorator) {
		return Optional.of(ontologyIndividualFinishedDecorator);
	}
	
	/**
	 * Creates an empty optional for a OntologyIndividualFinishedDecorator.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyIndividualFinishedDecorator> createEmptyOntologyIndividualFinishedDecoratorOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a taxonomy.
	 * @param iri The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<Taxonomy> createOptionalWithTaxonomy(final Taxonomy taxonomy) {
		if (taxonomy != null) {
			return Optional.of(taxonomy);
		}
		return createEmptyOptionalWithTaxonomy();
	}
	
	/**
	 * Creates an empty optional for a taxonomy.
	 * @return The created empty Optional.
	 */
	public final static Optional<Taxonomy> createEmptyOptionalWithTaxonomy() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an individual in a taxonomy.
	 * @param iri The object that should be contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TaxonomyIndividual> createOptionalWithTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual) {
		if (taxonomyIndividual != null) {
			return Optional.of(taxonomyIndividual);
		}
		return createEmptyOptionalWithTaxonomyIndividual();
	}
	
	/**
	 * Creates an empty optional for an individual in a taxonomy.
	 * @return The created empty Optional.
	 */
	public final static Optional<TaxonomyIndividual> createEmptyOptionalWithTaxonomyIndividual() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with a map with a collection with taxonomy individuals.
	 * @param taxonomyIndividuals The collection that is contained in the Optional.
	 * @return The created Optional.
	 */
	public final static Optional<TaxonomyIndividualCollection> createTaxonomyIndividualCollectionOptional(
			final TaxonomyIndividualCollection taxonomyIndividuals) {
		if (taxonomyIndividuals == null) {
			return createEmptyTaxonomyIndividualCollectionOptional();
		} else {
			return Optional.of(taxonomyIndividuals);
		}
	}
	
	/**
	 * Creates an empty optional with a map with a collection with taxonomy individuals.
	 * @return The created empty Optional.
	 */
	public final static Optional<TaxonomyIndividualCollection> createEmptyTaxonomyIndividualCollectionOptional() {
		return Optional.empty();
	}
	
	/**
	 * Creates an optional with an individual.
	 * @param ontologyIndividual The individual that is contained in the Optional.
	 * @return The created Optional or an empty optional if ontologyIndividual is null.
	 */
	public final static Optional<OntologyIndividual> createOntologyIndividualOptional(
			final OntologyIndividual ontologyIndividual) {
		if (ontologyIndividual == null) {
			return createEmptyOntologyIndividualOptional();
		} else {
			return Optional.of(ontologyIndividual);
		}
	}
	
	/**
	 * Creates an empty optional for an individual.
	 * @return The created empty Optional.
	 */
	public final static Optional<OntologyIndividual> createEmptyOntologyIndividualOptional() {
		return Optional.empty();
	}
}
