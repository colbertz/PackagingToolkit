package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.util.Locale;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguage;

public class TextInLanguageImpl implements TextInLanguage {
	/**
	 * The text of the label.
	 */
	private String labelText;
	/**
	 * The language the text is written in.
	 */
	private Locale language;
	
	/**
	 * Creates an object with a text and the language the
	 * text is written in.
	 * @param labelText The text.
	 * @param language The language the text is written in.
	 */
	public TextInLanguageImpl(final String labelText, final Locale language) {
		super();
		this.labelText = labelText;
		this.language = language;
	}
	
	/**
	 * Creates an object with a text. There are no information
	 * about the language indicated and therefore a default 
	 * language can be used.
	 * @param labelText The text.
	 * @param language The language the text is written in.
	 */
	public TextInLanguageImpl(final String labelText) {
		this(labelText, null);
	}
	
	@Override
	public String getLabelText() {
		return labelText;
	}
	
	@Override
	public void setLabelText(final String labelText) {
		this.labelText = labelText;
	}
	
	@Override
	public Locale getLanguage() {
		return language;
	}
	
	@Override
	public void setLanguage(final Locale language) {
		this.language = language;
	}
}
