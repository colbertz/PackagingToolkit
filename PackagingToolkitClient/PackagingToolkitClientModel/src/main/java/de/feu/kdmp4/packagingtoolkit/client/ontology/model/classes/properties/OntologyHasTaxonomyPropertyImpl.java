package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyHasTaxonomyProperty;

public class OntologyHasTaxonomyPropertyImpl extends OntologyObjectPropertyImpl implements OntologyHasTaxonomyProperty {
	public OntologyHasTaxonomyPropertyImpl(String propertyId, String label, OntologyClass range, String propertyRange) {
		super(propertyId, label, range, propertyRange);
	}

	@Override
	public boolean isHasTaxonomyProperty() {
		return true;
	}
}
