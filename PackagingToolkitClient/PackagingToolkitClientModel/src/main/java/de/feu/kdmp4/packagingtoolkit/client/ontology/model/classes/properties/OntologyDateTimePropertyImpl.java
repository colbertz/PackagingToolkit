package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import java.time.LocalDateTime;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateTimeProperty;

public class OntologyDateTimePropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDateTimeProperty {

	// ******** Constants *********
	private static final long serialVersionUID = 1748033123603000410L;

	// ******** Constructors ******
	public OntologyDateTimePropertyImpl() {
	}
	
	/**
	 * Gets the label and the value of this property.
	 * 
	 * @param label
	 *            The label.
	 * @param value
	 *            The value.
	 */
	public OntologyDateTimePropertyImpl(String propertyId, LocalDateTime value, String defaultLabel, String range) {
		super(propertyId, value, defaultLabel, range);
	}

	public OntologyDateTimePropertyImpl(String propertyId, String range) {
		super(propertyId, null, null, range);
	}

	public OntologyDateTimePropertyImpl(String propertyId, String label, String range) {
		super(propertyId, null, label, range);
	}
	
	// ****** Getters and setters *******
	@Override
	public void setPropertyValue(LocalDateTime value) {
		super.setValue(value);
	}
	
	@Override
	public LocalDateTime getPropertyValue() {
		return (LocalDateTime) getValue();
	}

	@Override
	public boolean isDateTimeProperty() {
		return true;
	}
}
