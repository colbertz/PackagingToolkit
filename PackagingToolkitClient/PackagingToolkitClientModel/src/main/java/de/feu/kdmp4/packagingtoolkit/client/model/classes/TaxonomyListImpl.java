package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.util.Iterator;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class TaxonomyListImpl extends AbstractList implements TaxonomyCollection {
	private TaxonomyIterator taxonomyIterator;
	
	@Override
	public void addTaxonomy(final Taxonomy taxonomy) {
		super.add(taxonomy);
	}
	
	@Override
	public Optional<Taxonomy> getTaxonomy(final int index) {
		Taxonomy taxonomy = (Taxonomy)super.getElement(index);
		return ClientOptionalFactory.createOptionalWithTaxonomy(taxonomy);
	}
	
	@Override
	public int getTaxonomyCount() {
		return super.getSize();
	}
	
	@Override
	public Taxonomy findTaxonomyWithTitle() {
		for (int i = 0; i < getSize(); i++) {
			final Taxonomy taxonomy = (Taxonomy)getElement(i);
			final String title = taxonomy.getTitle();
			if (StringValidator.isNotNull(title)) {
				return taxonomy;
			}
		}
		
		return null;
	}
	
	@Override
	public boolean containsTaxonomies() {
		if (getTaxonomyCount() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean hasNextTaxonomy() {
		if (taxonomyIterator == null) {
			taxonomyIterator = new TaxonomyIterator();
		}
		
		return taxonomyIterator.hasNext();
	}
	
	@Override
	public Optional<Taxonomy> nextTaxonomy() {
		if (taxonomyIterator == null) {
			taxonomyIterator = new TaxonomyIterator();
		}
		
		return taxonomyIterator.next();
	}
	
	@Override
	public Optional<Taxonomy> getTaxonomyByRootIndividual(final TaxonomyIndividual rootIndividual) {
		final TaxonomyIterator taxonomyIterator = new TaxonomyIterator();
		Taxonomy foundTaxonomy = null;
		
		while (taxonomyIterator.hasNext() && foundTaxonomy == null) {
			final Taxonomy currentTaxonomy = taxonomyIterator.next().get();
			final TaxonomyIndividual currentRootIndividual = currentTaxonomy.getRootIndividual();
			if (currentRootIndividual.equals(rootIndividual)) {
				foundTaxonomy = currentTaxonomy;
			}
		}
		
		if (foundTaxonomy != null) {
			return ClientOptionalFactory.createOptionalWithTaxonomy(foundTaxonomy);
		} else {
			return ClientOptionalFactory.createEmptyOptionalWithTaxonomy();
		}
	}
	
	/**
	 * Iterates through all narrowers.
	 * @author Christopher Olbertz
	 *
	 */
	private class TaxonomyIterator implements Iterator<Optional<Taxonomy>> {
		private int counter;
		private Optional<Taxonomy> currentTaxonomy;
		
		public TaxonomyIterator() {
			counter = 0;
			if (getTaxonomyCount() > 0) {
				currentTaxonomy = getTaxonomy(counter);
			} else {
				currentTaxonomy = ClientOptionalFactory.createEmptyOptionalWithTaxonomy();
			}
		}
		
		@Override
		public boolean hasNext() {
			if (!currentTaxonomy.isPresent()) {
				return false;
			} else {
				if (counter < getSize() ) {
					return true;
				}
			}
			
			return false;
		}

		@Override
		public Optional<Taxonomy> next() {
			if (hasNext()) {
				final Optional<Taxonomy> taxonomy = getTaxonomy(counter);
				counter++;
				return taxonomy;
			} else {
				counter = 0;
				return ClientOptionalFactory.createEmptyOptionalWithTaxonomy();
			}
		}
	}
}
