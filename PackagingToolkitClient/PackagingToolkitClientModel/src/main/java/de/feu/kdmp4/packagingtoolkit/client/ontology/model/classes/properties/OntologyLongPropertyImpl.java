package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import java.math.BigInteger;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.PropertyRangeException;

public class OntologyLongPropertyImpl extends OntologyDatatypePropertyImpl 
									  implements OntologyLongProperty {
	// ********* Constants *********
	private static final long DEFAULT_VALUE = 0L;
	private static final long serialVersionUID = 106001928244230493L;
	private static final BigInteger SIGNED_MAX = new BigInteger(String.valueOf(Long.MAX_VALUE));
	private static final BigInteger SIGNED_MIN = new BigInteger(String.valueOf(Long.MIN_VALUE));
	private static final BigInteger UNSIGNED_MAX = BigInteger.valueOf(Long.MAX_VALUE).multiply(BigInteger.valueOf(2));
	private static final BigInteger UNSIGNED_MIN = new BigInteger("0");

	// ******** Attributes ********
	private boolean unsigned;

	// ******** Constructors ******
	public OntologyLongPropertyImpl() {
	}
	
	public OntologyLongPropertyImpl(String propertyId, boolean unsigned, String defaultLabel, String range) {
		super(propertyId, BigInteger.valueOf(DEFAULT_VALUE), defaultLabel, range);
		this.unsigned = unsigned;
	}
	
	@Override
	public void setPropertyValue(BigInteger value) {
		super.setValue(value);
	}
	
	@PostConstruct
	public void initialize() {
		if (unsigned) {
			addMaxInclusiveRestriction(UNSIGNED_MAX);
			addMinInclusiveRestriction(UNSIGNED_MIN);
		} else {
			addMaxInclusiveRestriction(SIGNED_MAX);
			addMinInclusiveRestriction(SIGNED_MIN);
		}		
	}

	// ******* Public methods *******
	@Override
	public void addMaxInclusiveRestriction(BigInteger value) {
		if (!checkLongRange(value)) {
			PropertyRangeException exception = PropertyRangeException.valueNotInLongRange(value);
			throw exception;
		}
		RestrictionValue<BigInteger> restrictionValue = new RestrictionValue<>(value);
		MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public void addMinInclusiveRestriction(BigInteger value) {
		if (!checkLongRange(value)) {
			PropertyRangeException exception = PropertyRangeException.valueNotInLongRange(value);
			throw exception;
		}
		RestrictionValue<BigInteger> restrictionValue = new RestrictionValue<>(value);
		MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public BigInteger getPropertyValue() {
		return (BigInteger) getValue();
	}

	@Override
	public boolean isLongProperty() {
		return true;
	}
	
	@Override
	public boolean isUnsigned() {
		return unsigned;
	}

	// ******** Private methods ********
	/**
	 * Checks if a given value is in the range of long. It considers if the
	 * property is signed or unsigned.
	 * 
	 * @param value
	 *            The value to check.
	 * @return True if the value is in the range of long, false otherwise.
	 */
	private boolean checkLongRange(BigInteger value) {
		if (unsigned) {
			int compareMin = value.compareTo(UNSIGNED_MIN);
			int compareMax = value.compareTo(UNSIGNED_MAX);
			if (compareMin != -1 && compareMax != 1) {
				return true;
			} else {
				return false;
			}
		} else {
			int compareMin = value.compareTo(SIGNED_MIN);
			int compareMax = value.compareTo(SIGNED_MAX);
			if (compareMin != -1 && compareMax != 1) {
				return true;
			} else {
				return false;
			}
		}
	}
}