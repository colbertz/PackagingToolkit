package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackagePath;

public class OaisModelFactory {
	public static InformationPackagePath createInformationPackagePath() {
		return new InformationPackagePathImpl();
	}
}
