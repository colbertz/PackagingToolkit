package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;

@SuppressWarnings("rawtypes")
public class MinInclusiveImpl extends AbstractRestriction implements MinInclusive {
	private RestrictionValue restrictionValue;
	
	public MinInclusiveImpl() {
	}
	
	public MinInclusiveImpl(RestrictionValue restrictionValue) {
		this.restrictionValue = restrictionValue;
	}

	/*@Override
	public void checkRestriction(BigDecimal value) {
		
	}*/
	
	@Override
	public RestrictionValue getRestrictionValue() {
		return restrictionValue;
	}
	
	@Override
	public void checkRestriction(RestrictionValue value) {
		if (!isSatisfied(value)) {
			RestrictionNotSatisfiedException exception = RestrictionNotSatisfiedException.
					minRestrictionNotSatisfied(value.toString(), 
							restrictionValue.toString());
			throw exception;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isSatisfied(RestrictionValue value) {
		if (restrictionValue.compare(value) == 1) {
			return false;
		} else if (restrictionValue.compare(value) == 0) {
			return true;
		} else {
			return true;
		}
	}
	
	/*@Override   
	public boolean isSatisfied(BigDecimal value) {
		RestrictionValue restrictionValue = RestrictionValue.fromDecimal(value);
		return this.isSatisfied(restrictionValue);
	}*/
}
