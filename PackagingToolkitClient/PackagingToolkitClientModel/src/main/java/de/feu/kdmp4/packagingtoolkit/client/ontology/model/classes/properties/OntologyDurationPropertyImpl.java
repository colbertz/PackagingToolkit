package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;

public class OntologyDurationPropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDurationProperty {

	// ******* Constants **********
	private static final long serialVersionUID = 1748033123603000410L;

	// ******* Constructors *******
	public OntologyDurationPropertyImpl() {
	}
	
	/**
	 * Gets the label and the value of this property.
	 * 
	 * @param label
	 *            The label.
	 * @param value
	 *            The value.
	 */
	public OntologyDurationPropertyImpl(String propertyId, 
			OntologyDuration value, String label, String range) {
		super(propertyId, value, label, range);
	}

	public OntologyDurationPropertyImpl(String propertyId, String label, String propertyRange) {
		super(propertyId, null, label, propertyRange);
	}
	
	// ********** Getters and setters **********
	@Override
	public void setPropertyValue(OntologyDuration value) {
		super.setValue(value);
	}
	
	@Override
	public OntologyDuration getPropertyValue() {
		return (OntologyDuration) getValue();
	}

	@Override
	public boolean isDurationProperty() {
		return true;
	}
}
