package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;


import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIRI;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class OntologyIRIImpl implements OntologyIRI {
	private String iri;
	
	public OntologyIRIImpl(String iri) {
		assert StringValidator.isNotNullOrEmpty(iri): "The IRI may not be empty!";
		this.iri = iri;
	}
	
	@Override
	public String getIri() {
		return iri;
	}
	
	@Override
	public String toString() {
		return iri;
	}
}
