package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

/**
 * An interface that provides access methods to a map containing meta data.
 * @author Christopher Olbertz
 *
 */
public interface MetadataMap {
	/**
	 * Adds a metadata element to the map.
	 * @param key The key of the metadata element.
	 * @param value The value of the metadata element.
	 */
	void addMetadata(String key, String value);
	/**
	 * Returns a metadata element from the map.
	 * @param key The key to look for.
	 * @return The found metadata value.
	 */
	String getMetadataValue(String key);
	/**
	 * Determines the number of entries in the map. 
	 * @return The number of entries in the map.
	 */
	int getMetadataCount();
	/**
	 * Creates a list with all keys found in the map with the metadata.
	 * @return A list with all keys found in the map with the metadata. 
	 */
	StringList getListWithKeys();
}
