package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

public interface IriCollection {
	/**
	 * Adds an iri to the list. With the help of the uuid is checked if
	 * the iri is already in the list.
	 * @param iri The iri that should be added to the list.
	 * @throws IriMayNotBeNullException Is thrown if the iri is null.
	 * @throws UuidAlreadyInIriListException Is thrown if an iri
	 * with the same uuid is already in the iri.
	 */
	void addIriToList(Iri iri);
	/**
	 * Gets an iri at a given index.
	 * @param index The index of the iri that should be determined.
	 * @return The found iri.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	Iri getIriAt(int index);
	/**
	 * Counts the iris in the list.
	 * @return The number of the iris in the list.
	 */
	int getIriCount();
	/**
	 * Removes an iri from the list. 
	 * @param iri The iri that should be removed.
	 * @throws IriMayNotBeNullException Is thrown if the iri is null.
	 */
	void removeIri(Iri iri);
	/**
	 * Checks if the list is empty.
	 * @return True if the list is empty, false otherwise.
	 */
	boolean isEmpty();
	/**
	 * Checks if the list is not empty.
	 * @return True if the list is not empty, false otherwise.
	 */
	boolean isNotEmpty();
}
