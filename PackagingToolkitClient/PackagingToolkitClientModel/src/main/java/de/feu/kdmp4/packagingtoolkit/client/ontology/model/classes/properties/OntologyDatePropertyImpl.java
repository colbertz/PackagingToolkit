package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import java.time.LocalDate;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

public class OntologyDatePropertyImpl extends OntologyDatatypePropertyImpl implements OntologyDateProperty {

	/**
	* 
	*/
	private static final long serialVersionUID = 1748033123603000410L;

	public OntologyDatePropertyImpl() {
	}
	
	/**
	 * Gets the label and the value of this property.
	 * 
	 * @param label
	 *            The label.
	 * @param value
	 *            The value.
	 */
	public OntologyDatePropertyImpl(String propertyId, LocalDate value, String label, String range) {
		super(propertyId, value, label, range);
	}

	public OntologyDatePropertyImpl(String propertyId, String range) {
		super(propertyId, null, null, range);
	}
	
	public OntologyDatePropertyImpl(String propertyId, String label, String range) {
		super(propertyId, null, label, range);
	}
	
	@Override
	public LocalDate getPropertyValue() {
		return (LocalDate) getValue();
	}

	@Override
	public void setPropertyValue(LocalDate value) {
		super.setValue(value);
	}
	
	@Override
	public boolean isDateProperty() {
		return true;
	}
	
	@Override
	public String toString() {
		return DateTimeUtils.getDateAsGermanString(getPropertyValue());
	}
}
