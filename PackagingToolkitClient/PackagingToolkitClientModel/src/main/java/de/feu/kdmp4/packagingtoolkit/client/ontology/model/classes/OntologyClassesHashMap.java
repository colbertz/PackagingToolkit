package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClassesMap;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;

/**
 * Contains the information which properties the ontology classes have. In order to
 * store these information, this class contains a map. The key of the map are the names
 * of the ontology classes. The values are objects of {@link de.feu.kdmp4.packagingtoolkit.
 * client.ontology.model.interfaces.PropertyCollection}. They contain the properties of the ontology
 * class
 * @author Christopher Olbertz
 *
 */
public class OntologyClassesHashMap implements OntologyClassesMap {
	private Map<String, OntologyClass> ontologyMap;
	
	public OntologyClassesHashMap() {
		ontologyMap = new HashMap<>();
	}
	
	@Override
	public void addClass(OntologyClass ontologyClass) {
		String className = ontologyClass.getFullName();
		ontologyMap.put(className, ontologyClass);
	}
	
	@Override
	public void addDatatypeProperty(String className, OntologyDatatypeProperty ontologyDatatypeProperty) {
		OntologyClass ontologyClass = ontologyMap.get(className); 
		/* Insert the property if the associated class was found in the map.
		   If the associated class was not found in the map, the class is
		   not a sub class of Information_Package and then it has not to be
		   regarded in this process. */
		if (ontologyClass != null) {
			ontologyClass.addDatatypeProperty(ontologyDatatypeProperty);
			ontologyMap.put(className, ontologyClass);
		}
	}
	
	@Override
	public OntologyClass getOntologyClass(String className) {
		return ontologyMap.get(className);
	}

	@Override
	public OntologyClassList findAllOntologyClasses() {
		final OntologyClassList ontologyClasses = ClientModelFactory.createEmptyOntologyClassList();
		
		final Collection<OntologyClass> ontologyClassCollection = ontologyMap.values();
		for (final OntologyClass ontologyClass: ontologyClassCollection) {
			ontologyClasses.addOntologyClassToList(ontologyClass);
		}
		
		return ontologyClasses;
	}
}
