package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.ListValidator;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * An implementation of {@see de.feu.kdmp4.packagingtoolkit.server.ontology.model.interfaces.OntologyClass}. 
 * @author Christopher Olbertz
 *
 */
public class OntologyClassImpl extends OntologyElementImpl implements OntologyClass, Serializable {
	private static final long serialVersionUID = -4248591790948061732L;
	
	/**
	 * The namespace the class in the ontology.
	 */
	private String namespace;
	/**
	 * The label is the name of the class in the ontology without its namespace.
	 */
	private String localName;
	private List<OntologyClass> subclasses;
	@Autowired
	private DatatypePropertyCollection datatypePropertyList;
	@Autowired
	private ObjectPropertyCollection objectPropertyList;
	
	public OntologyClassImpl(final DatatypePropertyCollection datatypePropertyList, final ObjectPropertyCollection objectPropertyList) {
		this.datatypePropertyList = datatypePropertyList;
		this.objectPropertyList = objectPropertyList;
		subclasses = new ArrayList<>();
	}
	
	public OntologyClassImpl(final DatatypePropertyCollection datatypePropertyList, final ObjectPropertyCollection objectPropertyList,
			final String namespace, final String localName) {
		this.datatypePropertyList = datatypePropertyList;
		this.objectPropertyList = objectPropertyList;
		subclasses = new ArrayList<>();
		this.namespace = namespace;
		this.localName = localName;
	}
	
	/**
	 * Constructs an ontology class. The classLabel may not be null or empty. The list
	 * with the subclasses is instantiated as an array list.
	 * @param classname A label for the class. May not be null or empty.
	 */
	public OntologyClassImpl(final String namespace, final String localname) {
		assert StringValidator.isNotNullOrEmpty(namespace): "namespace may"
				+ " not be empty!";
		this.namespace = namespace;
		this.localName = localname;
		subclasses = new ArrayList<>();
		datatypePropertyList = new DatatypePropertyCollectionImpl();
		objectPropertyList = new ObjectPropertyCollectionImpl();
	}
	
	@Override  
	public void addDatatypeProperties(final DatatypePropertyCollection propertyList) {
		if (propertyList != null) {
			for (int i = 0; i < propertyList.getPropertiesCount(); i++) {
				OntologyDatatypeProperty property = propertyList.getDatatypeProperty(i);
				this.datatypePropertyList.addDatatypeProperty(property);
			}
		}
	}
	
	@Override  
	public void addObjectProperties(final ObjectPropertyCollection propertyList) {
		if (propertyList != null) {
			for (int i = 0; i < propertyList.getPropertiesCount(); i++) {
				OntologyObjectProperty property = propertyList.getObjectProperty(i);
				this.objectPropertyList.addObjectProperty(property);
			}
		}
	}
	
	@Override
	public void addDatatypeProperty(final OntologyDatatypeProperty ontologyProperty) {
		datatypePropertyList.addDatatypeProperty(ontologyProperty);
	}
	
	@Override
	public void addObjectProperty(final OntologyObjectProperty ontologyObjectProperty) {
		objectPropertyList.addObjectProperty(ontologyObjectProperty);
	}
	
	@Override
	public OntologyObjectProperty getObjectProperty(final int index) {
		return objectPropertyList.getObjectProperty(index);
	}
	
	@Override
	public OntologyDatatypeProperty getDatatypeProperty(final int index) {
		return datatypePropertyList.getDatatypeProperty(index);
	}
	
	@Override 
	public OntologyDatatypeProperty getDatatypeProperty(final String propertyName) {
		OntologyDatatypeProperty ontologyProperty = null;
		int i = 0;
		boolean found = false;
		
		while (found == false && i < datatypePropertyList.getPropertiesCount()) {
			ontologyProperty = datatypePropertyList.getDatatypeProperty(i);
			
			if (ontologyProperty.getPropertyId().equals(propertyName)) {
				found = true;
			}
			i++;
		}
		
		if (found) {
			return ontologyProperty;
		} else {
			return null;
		}
	}
	
	@Override 
	public OntologyObjectProperty getObjectProperty(final String propertyName) {
		OntologyObjectProperty ontologyProperty = null;
		int i = 0;
		boolean found = false;
		
		while (found == false && i < objectPropertyList.getPropertiesCount()) {
			ontologyProperty = objectPropertyList.getObjectProperty(i);
			
			if(StringUtils.areStringsEqual(ontologyProperty.getPropertyId(), propertyName)) {
				found = true;
			}
			i++;
		}
		
		if (found) {
			return ontologyProperty;
		} else {
			return null;
		}
	}
	
	@Override
	public int getDatatypePropertiesCount() {
		return datatypePropertyList.getPropertiesCount();
	}
	
	@Override
	public int getObjectPropertiesCount() {
		return objectPropertyList.getPropertiesCount();
	}
	
	@Override
	public void addSubClass(OntologyClass subClass) {
		assert subClass != null: "subClass may not be null!";
		subclasses.add(subClass);
	}
	
	@Override
	public OntologyClass getSubClassAt(final int index) {
		assert ListValidator.isIndexInRange(index, subclasses): "The index " + index +
			" is invalid!";
		return subclasses.get(index);
	}
	
	@Override
	public String getNamespace(){
		return namespace;
	}
	
	@Override
	public String getLocalName() {
		return localName;
	}

	@Override
	public int getSubclassCount() {
		return subclasses.size();
	}
	
	@Override
	public List<OntologyDatatypeProperty> getDatatypePropertiesAsList() {
		final List<OntologyDatatypeProperty> datatypeProperties = new ArrayList<>();
		
		for (int i = 0; i < datatypePropertyList.getPropertiesCount(); i++) {
			final OntologyDatatypeProperty datatypeProperty = datatypePropertyList.getDatatypeProperty(i);
			datatypeProperties.add(datatypeProperty);
		}
		
		return datatypeProperties;
	}
	
	@Override
	public List<OntologyObjectProperty> getObjectPropertiesAsList() {
		final List<OntologyObjectProperty> objectProperties = new ArrayList<>();
		
		for (int i = 0; i < objectPropertyList.getPropertiesCount(); i++) {
			final OntologyObjectProperty objectProperty = objectPropertyList.getObjectProperty(i);
			objectProperties.add(objectProperty);
		}
		
		return objectProperties;
	}
	
	@Override
	public boolean hasSubclasses() {
		// There are no subclasses.
		if (subclasses.size() == 0) {
			return false;
		} else if (subclasses.size() == 1){
			// There is one subclass and this subclass is Nothing.
			if (subclasses.get(0).isNothing()) {
				return false;
			} else {
				// There is one subclass that is not Nothing.
				return true;
			}
 		} else {
 			// There are multiple subclasses.
			return true;
		}
	}
	
	@Override 
	public int indexOfSubclass(final String subclassName) {
		for (int i = 0; i <= subclasses.size(); i++) {
			OntologyClass ontologyClass = subclasses.get(i);
			if (ontologyClass.classnameContains(subclassName)) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public String toString() {
		return getFullName();
		//return getLocalName();
	}   
	
	@Override
	public boolean classnameContains(final String text) {
		return localName.contains(text);
	}
	
	@Override
	public String getLabel() {
		return localName;
	}

	@Override
	public boolean isNothing() {
		return localName.contains("Nothing");
	}
	
	@Override
	public boolean isThing() {
		return localName.contains("Thing");
	}
	
	@Override
	public String getFullName() {
		return namespace + PackagingToolkitConstants.NAMESPACE_SEPARATOR + localName;
	}
	
	@Override
	public boolean isDigitalObjectClass() {
		if (StringUtils.areStringsEqual(PackagingToolkitConstants.DIGITAL_OBJECT_CLASS_NAME, localName)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean hasDatatypeProperties() {
		if (datatypePropertyList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public boolean hasObjectProperties() {
		if (objectPropertyList == null || objectPropertyList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localName == null) ? 0 : localName.hashCode());
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OntologyClassImpl other = (OntologyClassImpl) obj;
		if (localName == null) {
			if (other.localName != null)
				return false;
		} else if (!localName.equals(other.localName))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		return true;
	}
}

