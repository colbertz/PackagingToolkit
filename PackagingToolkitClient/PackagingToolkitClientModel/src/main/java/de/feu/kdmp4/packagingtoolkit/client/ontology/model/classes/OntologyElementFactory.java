package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.PropertyCollection;

public class OntologyElementFactory {
	private static final String THING_LABEL = "Thing";
	
	public static OntologyClass createOntologyClass(String namespace, String classname) {
		return new OntologyClassImpl(namespace, classname);
	}
	
	public static PropertyCollection createPropertyList() {
		return new PropertyCollectionImpl();
	}
	
	public static OntologyClass createThingClass() {
		return new OntologyClassImpl(THING_LABEL, THING_LABEL);
	}
}
