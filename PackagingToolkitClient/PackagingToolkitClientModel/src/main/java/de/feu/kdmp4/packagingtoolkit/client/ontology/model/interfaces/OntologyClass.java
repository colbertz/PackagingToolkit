package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;


/**
 * Represents a class in a owl file. It can contain subclasses. This
 * class is a tool for building a tree describing an owl file.
 * @author Christopher Olbertz
 */
public interface OntologyClass  extends OntologyElement {
	/**
	 * Adds a OntologyClass to the list of subclasses. 
	 * @param subClass The subclass of this class. May not be null.
	 */
	public void addSubClass(OntologyClass subClass);

	/**
	 * Returns a subclass at a given index.
	 * @param index The index.
	 * @return The found subclass.
	 */
	public OntologyClass getSubClassAt(int index);
	/**
	 * Gets the number of subclasses.
	 * @return The number of subclasses.
	 */
	public int getSubclassCount();
	/**
	 * Checks, if the class has any subclasses. This is the case if die count of subclasses
	 * is zero or if the only subclass is the class Nothing.
	 * @return True, if the class has subclasses expect of Nothing, false otherwise.
	 */
	public boolean hasSubclasses();
	/**
	 * Returns the index of an class in the list of subclasses. 
	 * @param subclassName The name of the class to look for.
	 * @return The index of the class in the list of subclasses.
	 */
	public int indexOfSubclass(String subclassName);
	/**
	 * Checks if the name of the class contains a certain text.
	 * @param text The text.
	 * @return True if the name of the class contains the text, false otherwise.
	 */
	boolean classnameContains(String text);
	/**
	 * Checks if the class is the ontology class Nothing.
	 * @return True, if the class ist the class Nothing, false otherwise.
	 */
	public boolean isNothing();
	
	/**
	 * Checks if the class is the ontology class Thing.
	 * @return True, if the class ist the class Thing, false otherwise.
	 */
	public boolean isThing();
	/**
	 * Adds a datatype property to this class.
	 * @param datatypeProperty The datatype property that should be added.
	 */
	void addDatatypeProperty(OntologyDatatypeProperty datatypeProperty);
	/**
	 * Determines the datatype property at a given index.
	 * @param index The index we are looking at. 
	 * @return The datatype property that has been found.
	 */
	OntologyDatatypeProperty getDatatypeProperty(int index);
	/**
	 * Determines the number of datatype properties defined in this class.
	 */
	int getDatatypePropertiesCount();
	/**
	 * Determines the number of object properties defined in this class.
	 */
	int getObjectPropertiesCount();
	/**
	 * Returns the namespace of this class.
	 * @return The namespace of this class.
	 */
	String getNamespace();
	/**
	 * Returns the local name of this class.
	 * @return The local name of this class.
	 */
	String getLocalName();
	/**
	 * Returns the full name of this class in the form namespace#localName.
	 * @return The full name of this class.
	 */
	String getFullName();
	/**
	 * Adds several datatype properties to this class. 
	 * @param propertyList Contains the datatype properties that should be added.
	 */
	public void addDatatypeProperties(DatatypePropertyCollection propertyList);
	/**
	 * Checks if the class has datatype properties.
	 * @return True of the class has datatype properties, false otherwise.
	 */
	boolean hasDatatypeProperties();
	/**
	 * Checks if the class has object properties.
	 * @return True of the class has object properties, false otherwise.
	 */
	boolean hasObjectProperties();
	/**
	 * Adds several object properties to this class. 
	 * @param propertyList Contains the object properties that should be added.
	 */
	void addObjectProperties(ObjectPropertyCollection propertyList);
	/**
	 * Determines a datatype property by its name.
	 * @param propertyName The name of the property we are looking for.
	 * @return The datatype property that has been found. 
 	 */
	OntologyDatatypeProperty getDatatypeProperty(String propertyName);
	/**
	 * Determines an object property by its name.
	 * @param propertyName The name of the property we are looking for.
	 * @return The object property that has been found. 
 	 */
	OntologyObjectProperty getObjectProperty(String propertyName);
	/**
	 * Determines an object property by its index in the list.
	 * @param index The index of the property we are looking for.
	 * @return The object property that has been found. 
 	 */
	OntologyObjectProperty getObjectProperty(int index);
	/**
	 * Adds an object property to this class.
	 * @param datatypeProperty The object property that should be added.
	 */
	void addObjectProperty(OntologyObjectProperty ontologyObjectProperty);
	/**
	 * Creates a java.util.List with the contained data properties. 
	 * @return A .util.List with the contained data properties. 
	 */
	List<OntologyDatatypeProperty> getDatatypePropertiesAsList();
	/**
	 * Creates a java.util.List with the contained object properties. 
	 * @return A .util.List with the contained object properties. 
	 */
	List<OntologyObjectProperty> getObjectPropertiesAsList();
	/**
	 * Determines if this class is the class that contains the digital objects. 
	 * @return True if this class contains the digital objects, false otherwise. 
	 */
	boolean isDigitalObjectClass();
}
