package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.client.exceptions.gui.ClientConfigurationException;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class ServerConfigurationDataImpl implements ServerConfigurationData {
	// *********** Constants **********
	/**
	 * Is fix at the moment, but can be configurable in later versions.
	 */
	private static final String BASE_ONTOLOGY_CLASS = "Information_Package";
	private static final String CONSTRUCTOR = "ServerConfigurationDataImpl(String workingDirectoryPath)";
	/**
	 * The base ontology that is uploaded to the server. Can be null. In this case,
	 * there is no new rule ontology uploaded to the server but the already 
	 * configured base ontology is used. 
	 */
	private File baseOntology;
	
	private String baseOntologyClass;
	/**
	 * The path to the base ontology that is configured on the server.
	 */
	private String configuredBaseOntologyPath;
	/**
	 * The path to the rule ontology that is configured on the server.
	 */
	private String configuredRuleOntologyPath;
	/**
	 * The rule ontology that is uploaded to the server. Can be null. In this case,
	 * there is no new rule ontology uploaded to the server but the already 
	 * configured rule ontology is used. 
	 */
	private File ruleOntology;
	/**
	 * The path to the working directory on the server.
	 */
	private String workingDirectoryPath;
	
	public ServerConfigurationDataImpl(final String workingDirectoryPath) {
		if (StringValidator.isNullOrEmpty(workingDirectoryPath)) {
			ClientConfigurationException exception = ClientConfigurationException.
					createWorkingDirectoryMayNotBeEmptyException(this.getClass().getName(), CONSTRUCTOR);
			throw exception;
		}
		this.workingDirectoryPath = workingDirectoryPath;
		this.baseOntologyClass = BASE_ONTOLOGY_CLASS;
	}

	@Override
	public File getRuleOntology() {
		return ruleOntology;
	}
	
	@Override
	public String getConfiguredRuleOntologyPath() {
		return configuredRuleOntologyPath;
	}

	@Override
	public String getWorkingDirectoryPath() {
		return workingDirectoryPath;
	}

	@Override
	public File getBaseOntology() {
		return baseOntology;
	}

	@Override
	public void setBaseOntology(final File baseOntology) {
		if (!baseOntology.exists()) {
			ClientConfigurationException exception = ClientConfigurationException.
					createOntologyDoesNotExistException(baseOntology.getAbsolutePath());
			throw exception;
		}
		this.baseOntology = baseOntology;
	}

	@Override
	public String getConfiguredBaseOntologyPath() {
		String baseOntologyName = PackagingToolkitFileUtils.extractFileName(configuredBaseOntologyPath);
		return baseOntologyName;
	}

	@Override
	public void setConfiguredBaseOntologyPath(final String configuredBaseOntologyPath) {
		this.configuredBaseOntologyPath = configuredBaseOntologyPath;
	}

	@Override
	public void setConfiguredRuleOntologyPath(final String configuredRuleOntologyPath) {
		this.configuredRuleOntologyPath = configuredRuleOntologyPath;
	}

	@Override
	public void setRuleOntology(final File ruleOntology) {
		if (!ruleOntology.exists()) {
			ClientConfigurationException exception = ClientConfigurationException.
					createOntologyDoesNotExistException(ruleOntology.getAbsolutePath());
			throw exception;
		}
		this.ruleOntology = ruleOntology;
	}

	@Override
	public void setWorkingDirectoryPath(final String workingDirectoryPath) {
		this.workingDirectoryPath = workingDirectoryPath;
	}

	@Override
	public String getBaseOntologyClass() {
		return baseOntologyClass;
	}

	@Override
	public void setBaseOntologyClass(final String baseOntologyClass) {
		this.baseOntologyClass = baseOntologyClass;
	}
}