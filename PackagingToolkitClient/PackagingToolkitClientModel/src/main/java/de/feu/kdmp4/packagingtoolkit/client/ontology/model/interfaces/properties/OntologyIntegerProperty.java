package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import de.feu.kdmp4.packagingtoolkit.enums.Sign;

public interface OntologyIntegerProperty extends OntologyDatatypeProperty {
	Long getPropertyValue();
	void addMaxInclusiveRestriction(long value);
	void addMinInclusiveRestriction(long value);
	void setPropertyValue(long value);
	boolean isUnsigned();
	boolean isInclusiveZero();
	Sign getSign();
	long getMinValue();
	long getMaxValue();
}
