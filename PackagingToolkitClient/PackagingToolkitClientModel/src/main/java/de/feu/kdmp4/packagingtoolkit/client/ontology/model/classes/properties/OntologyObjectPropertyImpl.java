package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.utils.OntologyUtils;

public class OntologyObjectPropertyImpl extends OntologyPropertyImpl
										implements OntologyObjectProperty {

	private static final long serialVersionUID = 5950765286646166012L;

	private OntologyClass range;
	
	public OntologyObjectPropertyImpl(String propertyId, String label, OntologyClass range, String propertyRange) {
		super(propertyId, null, label, propertyRange);
		this.range = range;
		setPropertyValue(new OntologyIndividualCollectionImpl());
	}
	
	@Override
	public void addOntologyIndividual(OntologyIndividual ontologyIndividual) {
		OntologyClass individualClass = ontologyIndividual.getOntologyClass();
		if (individualClass == null) {
			OntologyIndividualCollection ontologyIndividualList = (OntologyIndividualCollection)super.getValue();
			ontologyIndividualList.addIndividual(ontologyIndividual);
		} else if (individualClass.equals(range)) {
			OntologyIndividualCollection ontologyIndividualList = (OntologyIndividualCollection)super.getValue();
			ontologyIndividualList.addIndividual(ontologyIndividual);
		} else {
			// TODO Exception werfen.
		}
	}
	
	@Override
	public void setPropertyValue(OntologyIndividualCollection ontologyIndividuals) {
		super.setValue(ontologyIndividuals);
	}
	
	@Override
	public boolean isObjectProperty() {
		return true;
	}

	@Override
	public OntologyIndividualCollection getPropertyValue() {
		return (OntologyIndividualCollection)getValue();
	}
	
	@Override
	public OntologyClass getRange() {
		return range;
	}

	@Override
	public boolean isBooleanProperty() {
		return false;
	}

	@Override
	public boolean isByteProperty() {
		return false;
	}

	@Override
	public boolean isDateProperty() {
		return false;
	}

	@Override
	public boolean isDateTimeProperty() {
		return false;
	}

	@Override
	public boolean isDoubleProperty() {
		return false;
	}

	@Override
	public boolean isDurationProperty() {
		return false;
	}

	@Override
	public boolean isFloatProperty() {
		return false;
	}

	@Override
	public boolean isIntegerProperty() {
		return false;
	}

	@Override
	public boolean isLongProperty() {
		return false;
	}

	@Override
	public boolean isShortProperty() {
		return false;
	}

	@Override
	public boolean isStringProperty() {
		return false;
	}

	@Override
	public boolean isTimeProperty() {
		return false;
	}

	@Override
	public void setPropertyId(String propertyId) {
		super.setPropertyId(propertyId);
	}
	
	@Override
	public boolean hasPremisRange() {
		String iriOfRange = range.getFullName();
		return OntologyUtils.isPremisIri(iriOfRange);
	}

	@Override
	public void clearValues() {
		final OntologyIndividualCollection individuals = getPropertyValue();
		for (int i = individuals.getIndiviualsCount() - 1; i >= 0; i--) {
			individuals.removeIndividual(i);
		}
	}
}
