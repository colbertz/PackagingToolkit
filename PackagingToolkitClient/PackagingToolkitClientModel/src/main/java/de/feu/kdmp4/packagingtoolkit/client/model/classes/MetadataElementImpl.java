package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElement;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataElementList;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementResponse;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class MetadataElementImpl implements MetadataElement {
	private static final String CONSTRUCTOR = "MetadataElementImpl(String name, String value)";
	private static final String METHOD_GET_VALUE_AT = "String getValueAt(int index)";
	private static final String METHOD_GET_CONFLICTING_METADATA_ELEMENT_AT = "String getConflictingMetadataElementAt(int index)";
	
	// TODO Brauche ich die Uuid noch, wenn nicht mehr in HashMap und in equals verwendet?
	private Uuid uuid;
	private String name;
	private MetadataElementList conflictingMetadataElements;
	private StringList values;
	private boolean multipleValueElement;
	
	public MetadataElementImpl(MetadataElementResponse metadataElementResponse) {
		this.name = metadataElementResponse.getName();
		//values = new ArrayList<>();
		values = PackagingToolkitModelFactory.getStringList();
		conflictingMetadataElements = ClientModelFactory.createMetadataElementList(); 
		
		// Copy the values.
		for (int i = 0; i < metadataElementResponse.getValueCount(); i++) {
			String value = metadataElementResponse.getValueAt(i); 
			values.addStringToList(value);
		}
		
		// Copy the conflicting metadata.
		/*for (int i = 0; i < metadataElementResponse.getConflictingMetadataElementsCount(); i++) {
			MetadataElementResponse conflict = metadataElementResponse.getConflictingMetadataElementAt(i);
			MetadataElement conflictingElement = ClientModelFactory.createMetadataElement(conflict);
			this.addConflictingMetadataElement(conflictingElement);
		}*/
		
	}
	
	@Override
	public void addValue(String value) {
		 /*if (StringValidator.isNullOrEmpty(value)) {
			 throw new MetadataElementValueMayNotBeEmptyException(this.getClass().getName(), CONSTRUCTOR,
					 											  PackagingToolkitComponent.SERVER);
		 }*/
		 
		 /* Because there is already a value when creating the element, we know now that this
		 	this element can contain multiple values. */
		 //multipleValueElement = true;
		 //values.add(value);
		 //Collections.sort(values);
	}
	
	@Override
	public String getValueAt(int index) {
		if (multipleValueElement) {
			/*if (!ListValidator.isIndexInRange(index, values)) {
				throw new ListIndexInvalidException(index, this.getClass().getName(), 
												   METHOD_GET_VALUE_AT, 
												   PackagingToolkitComponent.MEDIATOR);
			}*/
			return values.getStringAt(index);
		} else {
			return values.getStringAt(0);
		}
	}

	@Override
	public MetadataElement getConflictingMetadataElementAt(int index) {
		/*if (!ListValidator.isIndexInRange(index, con)) {
			throw new IndexNotInRangeException(index, this.getClass().getName(), 
											   METHOD_GET_VALUE_AT, 
											   PackagingToolkitComponent.MEDIATOR);*/
		return conflictingMetadataElements.getMetadataElementAt(index);
	}
	
	private void addConflictingMetadataElement(MetadataElement metadataElement) {
		conflictingMetadataElements.addMetadata(metadataElement);
	}
	
	@Override
	public void addConflictingMetadataElement(List<MetadataElement> metadataElementList) {
		/*for (MetadataElement metadataElement: metadataElementList) {
			this.addConflictingMetadataElement(metadataElement);
		}*/
	}
	
	@Override
	public int getConflictingMetadataElementsCount() {
		return conflictingMetadataElements.getMetadataCount();
	}

	@Override
	public int getValueCount()  {
		return values.getSize();
	}
	
	@Override
	public Uuid getUuid() {
		return uuid;
	}

	@Override
	public String getName() {
		return name;
	} 

	@Override
	public String getValue() {
		return values.getStringAt(0);
	}

	@Override
	public boolean isMultipleValueElement() {
		return multipleValueElement;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetadataElementImpl other = (MetadataElementImpl) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(MetadataElement metadataElement) {
		return name.compareTo(metadataElement.getName());
	}
	
	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(name).append(": ");
		stringBuffer.append(StringUtils.createCommaSeperatedList(values));
		return  stringBuffer.toString();
	}
}
