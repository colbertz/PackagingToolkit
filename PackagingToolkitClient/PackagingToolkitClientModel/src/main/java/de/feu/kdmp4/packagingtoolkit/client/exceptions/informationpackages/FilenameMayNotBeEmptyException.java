package de.feu.kdmp4.packagingtoolkit.client.exceptions.informationpackages;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.client.utils.StringUtil;
import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;
import de.feu.kdmp4.packagingtoolkit.exceptions.PackagingToolkitException;

public class FilenameMayNotBeEmptyException extends PackagingToolkitException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7969082242897952770L;
	/**
	 * The message that is sended.
	 */
	private static final String MESSAGE = I18nExceptionUtil.getFilenameMayNotBeNullString();
	
	/**
	 * The constructor.
	 * @param theClass The class in that the exception occured.
	 * @param theMethod The method in that the exception occured.
	 */
	public FilenameMayNotBeEmptyException(final String theClass, 
			final String theMethod, final PackagingToolkitComponent packagingToolkitComponent) {
		super(MESSAGE, theClass, theClass, theMethod, packagingToolkitComponent);
	}

	/**
	 * Returns the additional information of this exception.
	 * @return There is no additional information.
	 */
	@Override
	public Object getAdditionalInformation() {
		return StringUtil.EMPTY_STRING;
	}

	public String getLogErrorMessage() {
		return super.getLogMessage(MESSAGE); 
	}
}
