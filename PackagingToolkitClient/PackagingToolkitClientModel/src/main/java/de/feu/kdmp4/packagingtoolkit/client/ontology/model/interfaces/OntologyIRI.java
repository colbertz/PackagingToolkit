package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

public interface OntologyIRI {
	public String getIri();
}
