package de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces;

public interface SubmissionInformationUnit extends InformationPackage {
	/**
	 * Adds a new digital object to the AIU.
	 * @param digitalObject The digital object to add.
	 */
	public void addDigitalObject(DigitalObject digitalObject);
	/**
	 * Removes a digital object from an AIU.
	 * @param uuid The Uuid of the digital object to look for. 
	 * @return The removed digital object.
	 */
	//public DigitalObject removeDigitalObject(Uuid uuid);
}
