package de.feu.kdmp4.packagingtoolkit.client.oais.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.SubmissionInformationUnit;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.PackagingToolkitComponent;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Represents an archival information unit that can contain multiple digital objects.
 * @author Christopher Olbertz
 *
 */
public class SubmissionInformationUnitImpl extends InformationPackageImpl 
									 implements SubmissionInformationUnit {
	
	private DigitalObjectCollection digitalObjects;
	
	public SubmissionInformationUnitImpl(String title, Uuid uuid, DigitalObjectCollection digitalObjects) {
		super(title,  uuid);
		this.digitalObjects = digitalObjects;
	}

	@Override
	public void addDigitalObject(DigitalObject digitalObject) {
		if (digitalObject == null) {
			/*throw new DigitalObjectMayNotBeNullException(this.getClass().getName(), 
														 METHOD_ADD_DIGITAL_OBJECT, PackagingToolkitComponent.SERVER);*/
		}
		
		digitalObjects.addDigitalObject(digitalObject);
	}
	
	/*@Override
	public DigitalObject removeDigitalObject(Uuid uuid) {
		if (uuid == null) {
			throw new UuidMayNotBeNullException(this.getClass().getName(), METHOD_REMOVE_DIGITAL_OBJECT, 
												PackagingToolkitComponent.SERVER);	
		}
		
		DigitalObject removedDigitalObject = digitalObjects.remove(uuid); 
		
		if (removedDigitalObject == null) {
			throw new DigitalObjectNotFoundByUuidException(uuid, this.getClass().getName(), METHOD_REMOVE_DIGITAL_OBJECT, 
												PackagingToolkitComponent.SERVER);
		}
		
		return removedDigitalObject;
	}*/
	
	@Override
	public PackageType getPackageType() {
		return PackageType.SIU;
	}
}
