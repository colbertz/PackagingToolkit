package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import java.io.Serializable;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Represents an individual that can be saved in the triple store or can be found in an ontology.
 * @author Christopher Olbertz
 *
 */
public interface OntologyIndividual extends Serializable, Cloneable {
	OntologyClass getOntologyClass();
	void addDatatypeProperty(OntologyDatatypeProperty ontologyProperty);
	OntologyDatatypeProperty getDatatypePropertyAt(int index);
	void addObjectProperty(OntologyObjectProperty ontologyProperty);
	OntologyObjectProperty getObjectPropertyAt(int index);
	Uuid getUuid();
	int getDatatypePropertiesCount();
	int getObjectPropertiesCount();
	String getNamespace();
	void setUuid(Uuid uuid);
	void setObjectProperties(ObjectPropertyCollection objectProperties);
	/**
	 * Searches for a datatype property by its iri.
	 * @param propertyName The iri of the property we are looking got. 
	 * @return The found property or an empty optional.
	 */
	Optional<OntologyDatatypeProperty> getDatatypeProperty(String propertyName);
	Iri getIriOfIndividual();
	void setIriOfIndividual(Iri iriOfIndividual);
	/**
	 * Determines a datatype property with the help of the label. 
	 * @param label The label of the property we are looking for. 
	 * @return The datatype property that has been found or an empty optional. 
	 */
	Optional<OntologyDatatypeProperty> findDatatypePropertyByLabel(String label);
	/**
	 * Determines an object property with the help of the label. 
	 * @param label The label of the property we are looking for. 
	 * @return The object property that has been found or an empty optional. 
	 */
	Optional<OntologyObjectProperty> findObjectPropertyByLabel(String label);
}
