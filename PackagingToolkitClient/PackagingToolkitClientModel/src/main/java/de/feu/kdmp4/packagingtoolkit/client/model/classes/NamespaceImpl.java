package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

public class NamespaceImpl implements Namespace {
	/**
	 * The string that contains the namespace and is encapsulated by this class.
	 */
	private String namespace;
	
	public NamespaceImpl(final String namespace) {
		this.namespace = namespace;
	}
	
	@Override
	public String getNamespace() {
		return namespace;
	}
	
	@Override
	public String toString() {
		return namespace;
	}
	
	@Override
	public boolean isNotEmpty() {
		if (StringValidator.isNotNullOrEmpty(namespace)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NamespaceImpl other = (NamespaceImpl) obj;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		return true;
	}
}
