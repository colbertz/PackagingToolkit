package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import java.util.Locale;

/**
 * An element that is extracted from a ontology. Here, the compositum pattern is
 * used. The class, that is implementing OntologyElement, is the superclass of 
 * all classes that are involved into compositum. The interface 
 * {@link de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.OntologyClass}
 * represents the class that contains other objects of subclasses of OntologyElement.
 * @author Christopher Olbertz
 *
 */
public interface OntologyElement {
	public String getLabel();
	/**
	 * Adds a label in a certain language.
	 * @param label The label that is added to this element.
	 */
	void addLabel(TextInLanguage label);
	/**
	 * Looks for the text in a given language. 
	 * @param language The language of the text to look for.
	 * @return The found text or an empty String if no label in the given language was found.
	 */
	String findLabelByLanguage(Locale locale);
}
