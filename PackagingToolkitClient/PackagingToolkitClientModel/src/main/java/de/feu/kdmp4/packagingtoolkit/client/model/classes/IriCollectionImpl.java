package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;

/**
 * Encapsulates a list with iris.
 * @author Christopher Olbertz
 *
 */
public class IriCollectionImpl extends AbstractList	implements IriCollection { 
	
	@Override
	public void addIriToList(final Iri iri) {
		super.add(iri);
	}
	
	@Override
	public Iri getIriAt(final int index) {
		Iri iri = (Iri)super.getElement(index);
		return iri;
	}
	
	@Override
	public int getIriCount() {
		return super.getSize();
	}
	
	@Override
	public void removeIri(final Iri iri) {
			super.remove(iri);
	}
}
