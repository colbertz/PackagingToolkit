package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MaxInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.Restriction;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.PropertyRangeException;

public class OntologyBytePropertyImpl extends OntologyDatatypePropertyImpl 
									  implements OntologyByteProperty {
	// ********* Constants ***************
	private static final long serialVersionUID = 106001928244230493L;
	private static final short SIGNED_MAX = 127;
	private static final short SIGNED_MIN = -128;
	private static final short UNSIGNED_MAX = 255;
	private static final short UNSIGNED_MIN = 0;
	
	// ************ Attributes **************
	private boolean unsigned;
	
	// ************ Constructors ************
	public OntologyBytePropertyImpl() { }
	
	public OntologyBytePropertyImpl(String propertyId, boolean unsigned, 
			String defaultLabel, String range) {
		super(propertyId, (short)0, defaultLabel, range);
		this.unsigned = unsigned;
	}

	@PostConstruct
	public void initialize() {
		super.initialize();
		if (unsigned) {
			addMaxInclusiveRestriction(UNSIGNED_MAX);
			addMinInclusiveRestriction(UNSIGNED_MIN);
		} else {
			addMaxInclusiveRestriction(SIGNED_MAX);
			addMinInclusiveRestriction(SIGNED_MIN);
		}
	}
	
	// *********** Public methods ***********
	@Override
	public void addMaxInclusiveRestriction(short value) {
		if (!checkByteRange(value)) {
			PropertyRangeException exception = PropertyRangeException.valueNotInByteRange(value);
			throw exception;
		}
		RestrictionValue<Short> restrictionValue = new RestrictionValue<>(value);
		MaxInclusive restriction = restrictionFactory.createMaxInclusive(restrictionValue);
		addRestriction(restriction);
	}
	
	@Override
	public void addMinInclusiveRestriction(short value) {
		if (!checkByteRange(value)) {
			PropertyRangeException exception = PropertyRangeException.valueNotInByteRange(value);
			throw exception;
		}
		RestrictionValue<Short> restrictionValue = new RestrictionValue<>(value);
		MinInclusive restriction = restrictionFactory.createMinInclusive(restrictionValue);
		addRestriction(restriction);
	}

	@Override
	public void setPropertyValue(Short value) {
		super.setValue(value);
	}
	
	@Override
	public Short getPropertyValue() {
		return (Short)getValue();
	}

	@Override
	public boolean isUnsigned() {
		return unsigned;
	}
	
	@Override
	public boolean isByteProperty() {
		return true;
	}
	
	private boolean checkByteRange(short value) {
		if (unsigned) {
			if (value >= UNSIGNED_MIN && value <= UNSIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		} else {
			if (value >= SIGNED_MIN && value <= SIGNED_MAX) {
				return true;
			} else {
				return false;
			}
		}
	}
}
