package de.feu.kdmp4.packagingtoolkit.client.ontology.factories;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class AbstractFactory  implements ApplicationContextAware{
	@Autowired
	protected ApplicationContext applicationContext;
	
	protected void initBean(Object myBean) {
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(myBean);
		String beanName = myBean.getClass().getSimpleName();
		beanFactory.configureBean(myBean, beanName);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}