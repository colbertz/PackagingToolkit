package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyDuration;

public class OntologyDurationImpl implements OntologyDuration {
	// ************** Constants **************
	private static final String DAYS_MARKER = "D";
	private static final String HOURS_MARKER = "H";
	private static final String MINUTES_MARKER = "M";
	private static final String MONTHS_MARKER = "M";
	private static final String NEGATIVE_MARKER = "-";
	private static final String PERIOD_MARKER = "P";
	private static final String SECONDS_MARKER = "S";
	private static final String TIME_MARKER = "T";
	private static final String YEARS_MARKER = "Y";
	
	// ************* Attributes **************
	private int days;
	private int hours;
	private int minutes;
	private int months;
	private boolean negative;
	private int seconds;
	private int years;
	
	// ************ Constructors *************
	public OntologyDurationImpl() {
		
	}
	
	public OntologyDurationImpl(int days, int hours, int minutes, int months, 
			int seconds, int years) {
		this(days, hours, minutes, months, seconds, years, false);
	}
	
	public OntologyDurationImpl(int days, int hours, int minutes, int months, 
			int seconds, int years, boolean negative) {
		super();
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
		this.months = months;
		this.seconds = seconds;
		this.years = years;
		this.negative = negative;
	}
	
	// ******** Public methods **********
	@Override
	public String toString() {
		String durationString = "";
		if (negative) {
			durationString = durationString + NEGATIVE_MARKER;
		}
		
		durationString = durationString + PERIOD_MARKER;
		
		if (years > 0) {
			durationString = durationString + years + YEARS_MARKER;
		}
		
		if (months > 0) {
			durationString = durationString + months + MONTHS_MARKER;
		}
		
		if (days > 0) {
			durationString = durationString + days + DAYS_MARKER;
		}
		
		// Now we check if a time section begins.
		if (hours > 0 || minutes > 0 || seconds > 0) {
			durationString = durationString + TIME_MARKER;
			if (hours > 0) {
				durationString = durationString + hours + HOURS_MARKER;
			}
			
			if (minutes > 0) {
				durationString = durationString + minutes + MINUTES_MARKER;
			}
			
			if (seconds > 0) {
				durationString = durationString + seconds + SECONDS_MARKER;
			}
		}
		
		
		return durationString;
	}

	// ************* Getters ************
	@Override
	public int getDays() {
		return days;
	}

	@Override
	public int getHours() {
		return hours;
	}

	@Override
	public int getMinutes() {
		return minutes;
	}

	@Override
	public int getMonths() {
		return months;
	}

	@Override
	public boolean isNegative() {
		return negative;
	}

	@Override
	public int getSeconds() {
		return seconds;
	}
	
	@Override
	public int getYears() {
		return years;
	}

	@Override
	public void setDays(int days) {
		this.days = days;
	}

	@Override
	public void setHours(int hours) {
		this.hours = hours;
	}

	@Override
	public void setMinutes(int minutes) {  
		this.minutes = minutes;
	}

	@Override
	public void setMonths(int months) {
		this.months = months;
	}

	@Override
	public void setNegative(boolean negative) {
		this.negative = negative;
	}

	@Override
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	@Override
	public void setYears(int years) {
		this.years = years;
	}
}
