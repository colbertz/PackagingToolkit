package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface Reference {
	String getUrl();
	Uuid getUuidOfFile();
	boolean isLocalFileReference();
	boolean isRemoteFileReference();
}