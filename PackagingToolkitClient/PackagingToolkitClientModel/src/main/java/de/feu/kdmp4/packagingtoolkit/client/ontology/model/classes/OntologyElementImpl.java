package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes;

import java.util.Locale;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyElement;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguage;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TextInLanguageList;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public abstract class OntologyElementImpl implements OntologyElement {
	private TextInLanguageList labels;
	
	public OntologyElementImpl() {
		labels = ClientModelFactory.createEmptyTextInLanguageCollection();
	}
	
	@Override
	public void addLabel(final TextInLanguage label) {
		labels.addTextInLanguage(label);
	}
	
	@Override
	public String findLabelByLanguage(final Locale locale) {
		final Optional<TextInLanguage> textInLanguage = labels.findTextByLanguage(locale);
		
		if(textInLanguage.isPresent()) {
			return textInLanguage.get().getLabelText();
		} else {
			return StringUtils.EMPTY_STRING;
		}
	}
}
