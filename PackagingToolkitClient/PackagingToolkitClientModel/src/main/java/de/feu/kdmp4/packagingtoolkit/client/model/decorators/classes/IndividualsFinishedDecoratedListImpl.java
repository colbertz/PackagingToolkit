package de.feu.kdmp4.packagingtoolkit.client.model.decorators.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.DecoratorFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.IndividualsFinishedDecoratedList;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.OntologyIndividualFinishedDecorator;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;

public class IndividualsFinishedDecoratedListImpl implements IndividualsFinishedDecoratedList {
	/**
	 * Contains the individuals this class is administrating.
	 */
	private List<OntologyIndividualFinishedDecorator> individuals;
	
	/**
	 * Initiliazes an empty list with individuals.
	 */
	public IndividualsFinishedDecoratedListImpl() {
		individuals = new ArrayList<>();
	}
	
	@Override
	public void addUnfinishedIndividual(final OntologyIndividualFinishedDecorator individual) {
		if (individual.isFinished() == true) {
			individuals.add(individual);
		} else if (!containsUnfinishedIndividual()) {
			individuals.add(individual);
		}
	}
	
	@Override
	public void addUnfinishedIndividual(final OntologyIndividual individual) {
		final OntologyIndividualFinishedDecorator unfinishedIndividual = DecoratorFactory.createOntologyIndividualFinishedDecorator(individual);
		addUnfinishedIndividual(unfinishedIndividual);
	}
	
	@Override
	public boolean containsUnfinishedIndividual() {
		for (OntologyIndividualFinishedDecorator individual: individuals) {
			if (individual.isFinished() == false) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public Optional<OntologyIndividualFinishedDecorator> getUnfinishedIndividual() {
		for (OntologyIndividualFinishedDecorator individual: individuals) {
			if (individual.isFinished() == false) {
				return ClientOptionalFactory.createOntologyIndividualFinishedDecoratorOptional(individual);
			}
		}
		
		return ClientOptionalFactory.createEmptyOntologyIndividualFinishedDecoratorOptional();
	}
}
