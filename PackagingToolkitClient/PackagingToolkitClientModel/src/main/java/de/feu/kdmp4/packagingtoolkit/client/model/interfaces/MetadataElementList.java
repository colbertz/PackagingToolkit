package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;


public interface MetadataElementList {
	void addMetadata(MetadataElement metadata);
	MetadataElement getMetadataElementAt(int index);
	int getMetadataCount();
}
