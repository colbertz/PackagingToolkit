package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.TaxonomiesAndTheirIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;

public class TaxonomiesAndTheirIndividualsHashMapImpl implements TaxonomiesAndTheirIndividualsMap {
	private Map<Iri, TaxonomyIndividualCollection> mapWithIndividuals;
	
	public TaxonomiesAndTheirIndividualsHashMapImpl() {
		mapWithIndividuals = new HashMap<>();
	}
	
	@Override
	public void addIndividualToTaxonomy(final Iri iriOfTaxonomy, final TaxonomyIndividual taxonomyIndividual) {
		TaxonomyIndividualCollection taxonomyIndividuals = mapWithIndividuals.get(iriOfTaxonomy);
		
		if (taxonomyIndividuals == null) {
			taxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		}
		
		taxonomyIndividuals.addTaxonomyIndividual(taxonomyIndividual);
		mapWithIndividuals.put(iriOfTaxonomy, taxonomyIndividuals);
	}
	
	@Override
	public void addIndividualsToTaxonomy(final Iri iriOfTaxonomy, final TaxonomyIndividualCollection newTaxonomyIndividuals) {
		TaxonomyIndividualCollection taxonomyIndividuals = mapWithIndividuals.get(iriOfTaxonomy);
		
		if (taxonomyIndividuals == null) {
			taxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		}
		taxonomyIndividuals.addTaxonomyIndividuals(newTaxonomyIndividuals);
		mapWithIndividuals.put(iriOfTaxonomy, taxonomyIndividuals);
	}
	
	@Override
	public Optional<TaxonomyIndividualCollection> findIndividualsOfTaxonomy(final Iri iriOfTaxonomy) {
		final TaxonomyIndividualCollection individuals = mapWithIndividuals.get(iriOfTaxonomy);
		final Optional<TaxonomyIndividualCollection> optionalWithIndividuals = ClientOptionalFactory.createTaxonomyIndividualCollectionOptional(individuals);
		return optionalWithIndividuals;
	}
	
	@Override
	public List<TaxonomyIndividualCollection> asList() {
		final List<TaxonomyIndividualCollection> allIndividuals = new ArrayList<>();
		
		final Set<Iri> irisInMap = mapWithIndividuals.keySet();
		for (final Iri iri: irisInMap) {
			final TaxonomyIndividualCollection individuals = mapWithIndividuals.get(iri);
			individuals.setIriOfTaxonomy(iri);
			allIndividuals.add(individuals);
		}
		
		return allIndividuals;
	}
}
