package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;

public interface OntologyPropertiesMap {

	void addProperty(OntologyProperty ontologyProperty);

	OntologyProperty getOntologyProperty(String propertyName);

}
