package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.Restriction;

/**
 * An abstract superclass for restrictions.
 * @author Christopher Olbertz
 *
 */
public abstract class AbstractRestriction implements Restriction {
	
}