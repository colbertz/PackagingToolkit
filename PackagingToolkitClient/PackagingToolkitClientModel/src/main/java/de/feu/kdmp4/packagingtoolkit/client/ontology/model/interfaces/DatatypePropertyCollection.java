package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces;

import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * A list with properties.
 * @author Christopher Olbertz
 *
 */
public interface DatatypePropertyCollection {
	/**
	 * Gets a property at a given index of the list.
	 * @param index The index.
	 * @return The property found at the index.
	 */
	OntologyDatatypeProperty getDatatypeProperty(final int index);
	/**
	 * Adds a property to the list.
	 * @param property The property that should be appended to the list.
	 */
	void addDatatypeProperty(final OntologyDatatypeProperty property);
	/**
	 * Determines the number of properties in the collection.
	 * @return The number of properties.
	 */
	int getPropertiesCount();
	/**
	 * Determines whether the collection is empty or not.
	 * @return True if the collection is empty false otherwise.
	 */
	boolean isEmpty();
	/**
	 * Determines if the collection contain a given property.
	 * @param ontologyDatatypeProperty The property we want know from if it is contained in the collection.
	 * @return True if ontologyDatatypeProperty is contained in the collection, false otherwise.
	 */
	boolean containsDatatypeProperty(final OntologyDatatypeProperty ontologyDatatypeProperty);
	/**
	 * Replaces a value of a property contained in the collection by another value.
	 * @param ontologyDatatypeProperty Contains the value that should be replaced. Furthermore this object is
	 * used to determine the property whose value should be replaced. 
	 */
	void replaceValueOfDatatypeProperty(final OntologyDatatypeProperty ontologyDatatypeProperty);
	/**
	 * Looks for a datatype property with a certain uuid.
	 * @param uuidOfDatatypeProperty The uuid of the datatype property we are looking for.
	 * @return An optional that contains the found datatype property or an empty optional.
	 */
	Optional<OntologyDatatypeProperty> getDatatypeProperty(Uuid uuidOfDatatypeProperty);
	/**
	 * Looks for a datatype property with a certain iri.
	 * @param iriOfDatatypeProperty The iri of the datatype property we are looking for.
	 * @return An optional that contains the found datatype property or an empty optional.
	 */
	Optional<OntologyDatatypeProperty> getDatatypeProperty(String iriOfDatatypeProperty);
}
