package de.feu.kdmp4.packagingtoolkit.client.exceptions.gui;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;

public class InformationPackageException extends ProgrammingException {
	private static final long serialVersionUID = -8518766136371113813L;

	private InformationPackageException(String message) {
		super(message);
	}

	public static final InformationPackageException informationPackagePathMayNotBeNullException() {
		String message = I18nExceptionUtil.getPathMayNotBeNullString();
		return new InformationPackageException(message);
	}
	
}
