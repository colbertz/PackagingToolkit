package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.PropertiesAndTheirClassesMap;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;

public class PropertiesAndTheirClassesHashMapImpl implements PropertiesAndTheirClassesMap {
	/**
	 * Contains the iris of the properties as strings and the classes that contain these properties as value;
	 */
	private Map<String, OntologyClassList> propertiesAndTheirClassesMap;
	
	/**
	 * Creates a new object and initializes the map.
	 */
	public PropertiesAndTheirClassesHashMapImpl() {
		propertiesAndTheirClassesMap = new HashMap<>();
	}
	
	@Override
	public void addClassToProperty(final OntologyProperty property, final OntologyClass ontologyClass) {
		addClassToProperty(property.getPropertyId(), ontologyClass);
	}
	
	@Override
	public void addClassToProperty(final String datatypePropertyIri, final OntologyClass ontologyClass) {
		OntologyClassList ontologyClasses = propertiesAndTheirClassesMap.get(datatypePropertyIri);
		
		if (ontologyClasses == null) {
			ontologyClasses = ClientModelFactory.createEmptyOntologyClassList();
		}
		
		ontologyClasses.addOntologyClassToList(ontologyClass);
		propertiesAndTheirClassesMap.put(datatypePropertyIri, ontologyClasses);
	}
	
	@Override
	public Optional<OntologyClassList> getOntologyClassesOfProperty(final String propertyIri) {
		OntologyClassList ontologyClasses = propertiesAndTheirClassesMap.get(propertyIri);
		if (ontologyClasses != null) {
			return ClientOptionalFactory.createOntologyClassListOptional(ontologyClasses);
		} else {
			return ClientOptionalFactory.createEmptyOntologyClassListOptional();
		}
	}
	
	@Override
	public Optional<OntologyClassList> getOntologyClassesOfProperty(final OntologyProperty property) {
		return getOntologyClassesOfProperty(property.getPropertyId());
	}
}
