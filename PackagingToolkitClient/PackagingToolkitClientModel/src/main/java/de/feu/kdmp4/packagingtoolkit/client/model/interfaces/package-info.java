/**
 * The interfaces for model classes that are used within the client and do not
 * belong to an other package.
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;