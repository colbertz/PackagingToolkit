package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import java.io.File;

/**
 * Represents the data used for configuring the server. 
 * @author Christopher Olbertz
 *
 */
public interface ServerConfigurationData {
	File getRuleOntology();
	String getWorkingDirectoryPath();
	String getConfiguredRuleOntologyPath();
	void setWorkingDirectoryPath(final String workingDirectoryPath);
	void setRuleOntology(File ruleOntology);
	void setConfiguredRuleOntologyPath(final String configuredRuleOntologyPath);
	void setConfiguredBaseOntologyPath(final String configuredBaseOntologyPath);
	String getConfiguredBaseOntologyPath();
	void setBaseOntology(final File baseOntology);
	File getBaseOntology();
	String getBaseOntologyClass();
	void setBaseOntologyClass(final String baseOntologyClass);
}
