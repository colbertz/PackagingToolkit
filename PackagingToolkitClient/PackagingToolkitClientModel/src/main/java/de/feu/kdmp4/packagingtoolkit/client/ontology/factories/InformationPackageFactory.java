package de.feu.kdmp4.packagingtoolkit.client.ontology.factories;

import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.DigitalObjectCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.SubmissionInformationPackageImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.classes.SubmissionInformationUnitImpl;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.SubmissionInformationPackage;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.SubmissionInformationUnit;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * A factory class for creating information packages. For every type of information
 * package, there is an own creation method. This class is the only place where 
 * it is allowed to call the constructor of the classes that represent information
 * packages.  
 * @author Christopher Olbertz
 *
 */
public abstract class InformationPackageFactory {
	/**
	 * Creates a new submission information package. 
	 * @param title The title of the new sip.
	 * @param uuid The uuid of the new sip. If the uuid is null, there is a uuid created.
	 * @return The newly created sip.
	 */
	public static final SubmissionInformationPackage createSubmissionInformationPackage(
																			  String title, 
																			  Uuid uuid) {
		return new SubmissionInformationPackageImpl(title, uuid);
	}
	
	/**
	 * Creates a new submission information unit. 
	 * @param title The title of the new siu.
	 * @param uuid The uuid of the new siu. If the uuid is null, there is a uuid created.
	 * @return The newly created siu.
	 */
	public static final SubmissionInformationUnit createSubmissionInformationUnit(
																			  String title, 
																			  Uuid uuid) {
		return new SubmissionInformationUnitImpl(title, uuid, new DigitalObjectCollectionImpl());
	}
	
	/**
	 * Creates a new submission information package. The uuid is generated. 
	 * @param title The title of the new sip.
	 * @return The newly created sip.
	 */
	public static final SubmissionInformationPackage createSubmissionInformationPackage(
																			  String title) {
		return new SubmissionInformationPackageImpl(title, new Uuid());
	}
	
	/**
	 * Creates a new submission information unit. The uuid is generated.
	 * @param title The title of the new siu.
	 * @return The newly created siu.
	 */
	public static final SubmissionInformationUnit createSubmissionInformationUnit(
																			  String title) {
		return new SubmissionInformationUnitImpl(title, new Uuid(), new DigitalObjectCollectionImpl());
	}
}
