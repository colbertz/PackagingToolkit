package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataMap;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

public class MetadataHashMapImpl implements MetadataMap {
	private Map<String, String> metadataMap;
	
	public MetadataHashMapImpl() {
		metadataMap = new HashMap<>();
	}
	
	@Override
	public void addMetadata(String key, String value) {
		metadataMap.put(key, value);
	}
	
	@Override
	public String getMetadataValue(String key) {
		return metadataMap.get(key);
	}

	@Override
	public int getMetadataCount() {
		return metadataMap.size();
	}

	@Override
	public StringList getListWithKeys() {
		Set<String> keySet = metadataMap.keySet();
		StringList keyList = PackagingToolkitModelFactory.getStringList(); 
		
		for (String aKey: keySet) {
			keyList.addStringToList(aKey);
		}
		
		return keyList;
	}
}
