package de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.properties;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.RestrictionFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.MinInclusive;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.Restriction;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.RestrictionCollection;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;
import static de.feu.kdmp4.packagingtoolkit.validators.StringValidator.isNotEmpty;

public abstract class OntologyDatatypePropertyImpl extends OntologyPropertyImpl implements OntologyDatatypeProperty {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7678084367113086933L;
	/**
	 * The restrictions that are valid for this property.
	 */
	@Autowired
	private RestrictionCollection restrictionList;
	//@Autowired
	protected RestrictionFactory restrictionFactory;
	
	public OntologyDatatypePropertyImpl() {
	}
	
	public OntologyDatatypePropertyImpl(String propertyId, Object value, 
			String label, String range) {
		super(propertyId, value, label, range);
	}
	
	/*@Override
	public Object clone() throws CloneNotSupportedException {
		OntologyDatatypeProperty newproperty = (OntologyDatatypeProperty)super.clone();
		Object value = newproperty.getPropertyValue();
		Object clonedValue = value.
		
		return newproperty;
	}*/
	
	@PostConstruct
	public void initialize() {
		restrictionList = restrictionFactory.createRestrictionList();
	}
	
	@Override
	public void checkRestrictions(RestrictionValue value) {
		int i = 0;
		
		while (i < restrictionList.getRestrictionsCount()) {
			Restriction restriction = restrictionList.getRestriction(i);
			restriction.checkRestriction(value);
			i++;
		}
	}
	
	@Override
	public boolean areRestrictionsSatisfied(RestrictionValue value) {
		boolean isValid = true;
		int i = 0;
		
		while (i < restrictionList.getRestrictionsCount() && isValid == true) {
			Restriction restriction = restrictionList.getRestriction(i);
			isValid = restriction.isSatisfied(value);
			i++;
		}
		
		return isValid;
	}
	
	@Override
	public Restriction getRestrictionAt(int index) {
		return restrictionList.getRestriction(index);
	}
	
	@Override
	public void setPropertyValue(final Object value) {
		if (value != null) {
			String valueAsString = value.toString();
			
			if (isBooleanProperty()) { 
				Boolean inputAsBoolean = Boolean.parseBoolean(valueAsString);
				setValue(inputAsBoolean);
			} else if (isByteProperty()) {
				Short inputAsShort = 0;
				if (isNotEmpty(valueAsString)) {
					inputAsShort = Short.parseShort(valueAsString);
				}
				setValue(inputAsShort);
			} else if (isDateProperty()) { 
				if (value instanceof LocalDate) {
					LocalDate localDate = (LocalDate)value;
					setValue(localDate);
				} else {
					Date date = (Date)value;
					LocalDate inputAsLocalDate = null; 
					if(isNotEmpty(valueAsString)) {
						inputAsLocalDate = DateTimeUtils.convertDateToLocalDate(date);
					}
					setValue(inputAsLocalDate);
				}
			} else if (isDateTimeProperty()) {
				if (value instanceof LocalDateTime) {
					LocalDateTime localDateTime = (LocalDateTime)value;
					setValue(localDateTime);
				} else {
					Date date = (Date)value;
					LocalDateTime inputAsLocalDateTime = null;
					if(isNotEmpty(valueAsString)) {
						inputAsLocalDateTime = DateTimeUtils.convertDateToLocalDateTime(date);
					}
					setValue(inputAsLocalDateTime);
				}
			} else if (isDoubleProperty()){
				Double inputAsDouble = 0.0;
				if (isNotEmpty(valueAsString)) {
					inputAsDouble = Double.parseDouble(valueAsString);
				}
				setValue(inputAsDouble);
			} else if (isDurationProperty()) {
				OntologyDuration valueAsDuration = OntologyDuration.fromString(valueAsString);
				setValue(valueAsDuration);
			} else if (isIntegerProperty()) {
				Long inputAsLong = 0l;
				if (isNotEmpty(valueAsString)) {
					inputAsLong = Long.parseLong(valueAsString);
				}
				setValue(inputAsLong);
			} else if (isFloatProperty()){
				Float inputAsFloat = 0.0f;
				
				if(isNotEmpty(valueAsString)) {
					inputAsFloat = Float.parseFloat(valueAsString);
				}
				setValue(inputAsFloat);
			} else if (isLongProperty()){
				BigInteger inputAsBigInteger = new BigInteger("0");
				if(isNotEmpty(valueAsString)) {
					inputAsBigInteger = new BigInteger(valueAsString);
				}
				setValue(inputAsBigInteger);
			} else if (isShortProperty()){
				Integer inputAsShort = 0;
				
				if(isNotEmpty(valueAsString)) {
					inputAsShort = Integer.parseInt(valueAsString);
				}
				setValue(inputAsShort);
			} else if (isStringProperty()){
				setValue(valueAsString);
			} else if (isTimeProperty()) { 
				LocalTime inputAsLocalTime = null;
				if (value instanceof Date) {
					Date date = (Date)value;
					inputAsLocalTime = LocalTime.of(date.getHours(), date.getMinutes(), date.getSeconds());
				} else {
					inputAsLocalTime = (LocalTime)value;
				}
				setValue(inputAsLocalTime);
			}
		} else {
			if (isDateProperty() || isDateTimeProperty() || isTimeProperty()) {
				setValue(null);
			}
		}
	}
	
	@Override
	public boolean isBooleanProperty() {
		return false;
	}
	
	@Override
	public boolean isByteProperty() {
		return false;
	}

	@Override
	public boolean isDateProperty() {
		return false;
	}

	@Override
	public boolean isDateTimeProperty() {
		return false;
	}
	
	@Override
	public boolean isDoubleProperty() {
		return false;
	}
	
	@Override
	public boolean isDurationProperty() {
		return false;
	}
	
	@Override
	public boolean isFloatProperty() {
		return false;
	}

	@Override
	public boolean isIntegerProperty() {
		return false;
	}
	
	@Override
	public boolean isLongProperty() {
		return false;
	}
	
	@Override
	public boolean isShortProperty() {
		return false;
	}
	
	@Override
	public boolean isStringProperty() {
		return false;
	}
	
	@Override
	public boolean isTimeProperty() {
		return false;
	}
	
	@Override
	public int getRestrictionCount() {
		if (restrictionList == null) {
			return 0;
		}
		return restrictionList.getRestrictionsCount();
	}
	
	// ********* Protected methods **********
	/**
	 * Adds a restriction to the property. Is only be used by the subclasses.
	 * Every subclass defines add methods for every restriction type. This
	 * methods call the method addRestriction(). Therefore, the subclasses
	 * do not need access to restrictionList. 
	 * @param restriction The restriction to add. 
	 */
	protected void addRestriction(Restriction restriction) {
		restrictionList.addRestriction(restriction);
	}
}
