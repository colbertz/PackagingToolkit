package de.feu.kdmp4.packagingtoolkit.client.utils;

/**
 * Some methods for working with files. The following functions are implemented:
 * <ul>
 * 	<li>Check if a file is an owl file.
 * 	<li>Check if a file is an rdf file.
 * </ul>
 * @author Christopher Olbertz
 *
 */
public abstract class FileUtils {
	// TODO Brauche ich das ueberhaupt?
	private static final String UPLOADED_FILES_DIRECTORY = "upload";
	private static final String OWL_FILE_EXTENSION = ".owl";
	private static final String RDF_FILE_EXTENSION = ".rdf";
	
	/**
	 * Checks with the help of the file extension, if a file is
	 * a owl file. 
	 * @param filename The file name to check.
	 * @return True, if the file name ends with .owl, false otherwise.
	 */
	public static final boolean isOwlFile(final String filename) {
		return filename.endsWith(OWL_FILE_EXTENSION);
	}
	
	/**
	 * Checks with the help of the file extension, if a file is
	 * a rdf file. 
	 * @param filename The file name to check.
	 * @return True, if the file name ends with .rdf, false otherwise.
	 */
	public static final boolean isRdfFile(final String filename) {
		return filename.endsWith(RDF_FILE_EXTENSION);
	}
}
