package de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces;

public interface InformationPackagePath {
	public void addPathComponent(String pathComponent);
	public void deleteLastPathComponent();
}
