/**
 * Contains enums used in the client.
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.enums;