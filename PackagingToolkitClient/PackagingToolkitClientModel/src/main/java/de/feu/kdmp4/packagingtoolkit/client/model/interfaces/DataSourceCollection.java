package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

/**
 * Represents a collection with data sources from the mediator. 
 * @author Christopher Olbertz
 *
 */
public interface DataSourceCollection {
	/**
	 * Gets a data source at a certain index from the collection.
	 * @param index The index of the data source in the collection.
	 * @return The found data source.
	 */
	DataSource getDataSourceAt(final int index);
	/**
	 * Adds a data source to the collection.
	 * @param dataSource The data source that should be added to the collection.
	 */
	void addDataSource(final DataSource dataSource);
	/**
	 * Returns the number of data sources in the collection.
	 * @return The number of data sources in the collection.
	 */
	int getDataSourceCount();

}
