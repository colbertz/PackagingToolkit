package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import java.time.LocalTime;
import java.util.Date;

public interface OntologyTimeProperty  extends OntologyDatatypeProperty {
	public LocalTime getPropertyValue();

	void setPropertyValue(Date value);

	void setPropertyValue(LocalTime value);
}
