package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.restrictions.RestrictionValue;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.restrictions.Restriction;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface OntologyDatatypeProperty extends OntologyProperty {
	public String getLabel();
	boolean isBooleanProperty();
	boolean isByteProperty();
	boolean isDateProperty();
	boolean isDateTimeProperty();
	boolean isDoubleProperty();
	boolean isDurationProperty();
	boolean isFloatProperty();
	boolean isIntegerProperty();
	boolean isLongProperty();
	boolean isShortProperty();
	boolean isStringProperty();
	boolean isTimeProperty();
	void checkRestrictions(RestrictionValue value);
	boolean areRestrictionsSatisfied(RestrictionValue value);
	Restriction getRestrictionAt(int index);
	void setPropertyValue(Object value);
	int getRestrictionCount();
}
