package de.feu.kdmp4.packagingtoolkit.client.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.OntologyIndividualsAndTheirUuidMap;
import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.TaxonomiesAndTheirIndividualsMap;
import de.feu.kdmp4.packagingtoolkit.client.exceptions.gui.InformationPackageException;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientOptionalFactory;
import de.feu.kdmp4.packagingtoolkit.client.factories.DataStructureFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackagePath;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.classes.OntologyIndividualCollectionImpl;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.PropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/* FORTHESIS Diese Klasse sorgt dafuer, dass die Erstellung eines Informationspakets
 * in der GUI unabhaengig ist von der spaeteren Implementierung. Hier werden die 
 * vom Anwender eingegebenen Daten so verwaltet, dass man sie spaeter ohne Probleme
 * einer beliebigen Struktur uebergeben und speichern kann.  
 */
/**
 * This class collects the data for an information package while the
 * user creates the packages with the help of the gui. All information, 
 * entered by the user, are saved in an object of this class. When the
 * user finishes the creation, this class finally creates the information
 * package. 
 * <br />
 * Because there is an attribute, that holds the information if a SIP or a SIU
 * is created, this class is also used to control the presentation of the gui. 
 * <br />
 * For example: A SIP can contain only one data object. If there is already one
 * data object in the InformationPackageManager, the control elements for 
 * uploading data objects are not displayed. This is done with the help
 * of the information in InformationPackageManager. 
 * @author Christopher Olbertz
 *
 */
public class InformationPackageManager {
	/**
	 * The type of the information package that is created. 
	 * This attribute helps with the creation of the gui.
	 */
	private PackageType informationPackageType;
	/**
	 * Contains the individuals that are assigned to this information package.
	 */
	private OntologyIndividualsAndTheirUuidMap containedIndividuals;
	private Map<InformationPackagePath, InformationPackageComponent> informationPackageComponents;
	/**
	 * Contains the uris of objects properties and the individuals that are assigned to the
	 * properties as value. This map is used for the creation of one individual. If the 
	 * individual is saved, this map has to be cleared.
	 * <br />
	 * Example: The class Content_Information contains one object property with the name hasDataObjects
	 * of the class Data_Object. Some individuals of Data_Object are chosen by the user as values
	 * of this property. Now in the map there is an entry that contains the String namespace#Content_Information
	 * as key and a list with the chosen individuals as value.
	 */
	private Map<String, OntologyIndividualCollection> objectPropertyValuesMap;
	/**
	 * Contains the ontology classes identified by their iris. 
	 */
	private Map<String, OntologyClass> ontologyClassMap;
	/**
	 * A title for the information package.
	 */
	private String title;
	/**
	 * The uuid of the information packages the user wants to create.
	 */
	private Uuid uuid;	
	/**
	 * The ontology classes the user has selected for further
	 * processing. 
	 */
	private OntologyClassList selectedOntologyClasses;
	/**
	 * The view the user has selected in the step with the view 
	 * selection. In the next step, the classes in this view are
	 * displayed in the mindmap structure.
	 */
	private View selectedView;
	/**
	 * Creates model objects for the client.
	 */
	private ClientModelFactory clientModelFactory;
	/**
	 * The serialization format of the information package. At the moment, only OAI-ORE is supported.
	 */
	private SerializationFormat serializationFormat;
	/**
	 * Contains the references that are assigned to this information package. 
	 */
	private ReferenceCollection references;
	/**
	 * True if the information package the user is working on is being edited. False, if the user is
	 * creating a new information package.
	 */
	private boolean editMode;
	/**
	 * Contains the digital object that has been assigned to this information package.
	 */
	private DigitalObjectCollection digitalObjects;
	/**
	 * The iri of the class that contains the file reference. This attribute is necessary for a simple
	 * access from the user interface.
	 */
	private String iriOfPropertyWithFileReference;
	/**
	 * True if a new information package is created, false otherwise. Is used for avoiding that the values from an old
	 * information package are entered into the gui. If this value is true all input components are initialized with 
	 * empty values. This is necessary because the view scope cannot be used.
	 */
	private boolean newInformationPackage;
	/**
	 * The taxonomy the user wants to see in the dialog. The dynamically created button contains the listener for open the
	 * dialog. The button sets the current taxonomy and the managed bean uses this value for rendering the tree
	 * in the dialog. 
	 */
	private Taxonomy currentTaxonomy;
	/**
	 * Contains the taxonomy individuals the user has selected to be inserted into the information package.
	 */
	private TaxonomiesAndTheirIndividualsMap selectedTaxonomyIndividuals;
	
	/**
	 * Initializes the necessary objects. 
	 */
	@PostConstruct
	public void initialize() {
		informationPackageComponents = new HashMap<>();
		ontologyClassMap = new HashMap<>();
		references = ClientModelFactory.createEmptyReferenceCollection();
		objectPropertyValuesMap = new HashMap<>();
		digitalObjects = clientModelFactory.getEmptyDigitalObjectList();
		selectedTaxonomyIndividuals = ClientModelFactory.createEmptyTaxonomiesAndTheirIndividualsMap();
		containedIndividuals = DataStructureFactory.createOntologyIndividualsAndTheirUuidHashMap();
	}
	
	/**
	 * Checks if the information package contains a certain individual. Uses the uuid for
	 * the comparison.
	 * @param uuidOfIndividual The uuid of the individual we want to know if it is contained in 
	 * the information package. 
	 * @return True if there was an individual found, false otherwise.
	 */
	public boolean containsIndividual(final Uuid uuidOfIndividual) {
		if (containedIndividuals.getIndividual(uuidOfIndividual) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Removes a reference. 
	 * @param urlOfReference The url of the reference. 
	 */
	public void removeReference(final String urlOfReference) {
		references.removeReference(urlOfReference);
	}
	
	/**
	 * Adds a digital object to the information package.
	 * @param digitalObject The digital object that should be added.
	 */
	public void addDigitalObject(final DigitalObject digitalObject) {
		digitalObjects.addDigitalObject(digitalObject);
	}
	
	/**
	 * Determines the taxonomy individuals the user has selected.
	 * @return A list with collections that contain the selected taxonomy individuals. 
	 */
	public List<TaxonomyIndividualCollection> getSelectedTaxonomyIndividuals() {
		return selectedTaxonomyIndividuals.asList();
	}
	
	/**
	 * Creates a new individual with the values of an existing one. 
	 * @param ontologyIndividual The individual whose values should be used for the new one. 
	 * @return The new individual. 
	 */
	public OntologyIndividual createCopyOfOntologyIndividual(final OntologyIndividual ontologyIndividual) {
		return clientModelFactory.createCopyOfOntologyIndividual(ontologyIndividual);
	}
	
	/**
	 * Deletes all data that are saved in the information package manager. This method should be called
	 * if an information package has been saved on the server or the creation process has been cancelled.
	 */
	public void clearData() {
		references = ClientModelFactory.createEmptyReferenceCollection();
		selectedOntologyClasses = null;
		containedIndividuals = DataStructureFactory.createOntologyIndividualsAndTheirUuidHashMap();
		uuid = null;
		title = StringUtils.EMPTY_STRING;
		iriOfPropertyWithFileReference = StringUtils.EMPTY_STRING;
		ontologyClassMap.clear();
		objectPropertyValuesMap.clear();
		informationPackageComponents.clear();
		clearDigitalObjects();
		selectedTaxonomyIndividuals = ClientModelFactory.createEmptyTaxonomiesAndTheirIndividualsMap();
		selectedView = null;
		currentTaxonomy = null;
	}
	
	/**
	 * Deletes all digital objects from the information package manager.
	 */
	public void clearDigitalObjects() {
		for (int i = 0; i < digitalObjects.getDigitalObjectCount(); i++) {
			final DigitalObject digitalObject = digitalObjects.getDigitalObjectAt(i);
			digitalObjects.deleteDigitalObject(digitalObject);
		}
	}
	
	/**
	 * Adds a value of an object property to this information package. The value of object properties is a collection
	 * with individuals.  
	 * @param objectPropertyIri The iri of the object property. 
	 * @param ontologyIndividual The individual that should be added to the value of this property. 
	 */
	public void addObjectPropertyValue(final String objectPropertyIri, final OntologyIndividual ontologyIndividual) {
		OntologyIndividualCollection ontologyIndividuals = objectPropertyValuesMap.get(objectPropertyIri);
		if (ontologyIndividuals == null) {
			ontologyIndividuals = new OntologyIndividualCollectionImpl();
		}
		
		if (ontologyIndividuals.containsNotIndividual(ontologyIndividual)) {
			ontologyIndividuals.addIndividual(ontologyIndividual);
		}
		objectPropertyValuesMap.put(objectPropertyIri, ontologyIndividuals);
	}
	
	/**
	 * Removes an individual from the values of an object property.
	 * @param objectPropertyIri The iri of the object property we want the individual remove from.
	 * @param ontologyIndividual The individual we want to remove from this object property.
	 */
	public void removeObjectPropertyValue(final String objectPropertyIri, final OntologyIndividual ontologyIndividual) {
		OntologyIndividualCollection ontologyIndividuals = objectPropertyValuesMap.get(objectPropertyIri);
		if (ontologyIndividuals != null) {
			ontologyIndividuals.removeIndividual(ontologyIndividual);
			//objectPropertyValuesMap.put(objectPropertyIri, ontologyIndividuals);
		}
	}
	
	/**
	 * Checks if a certain individual is contained in the list with the values of a certain object property.
	 * @param objectPropertyIri The iri of the object property we want to check if it contains the individual
	 * as value.
	 * @param ontologyIndividual The individual we want to check.
	 * @return True if the object property with the iri objectPropertyIri contains the individual ontologyIndividual
	 * as value, false otherwise.
	 */
	public boolean containsObjectPropertyValue(final String objectPropertyIri, final OntologyIndividual ontologyIndividual) {
		OntologyIndividualCollection ontologyIndividuals = objectPropertyValuesMap.get(objectPropertyIri);
		if (ontologyIndividuals != null) {
			if (ontologyIndividuals.containsIndividual(ontologyIndividual)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if a certain individual is not contained in the list with the values of a certain object property.
	 * @param objectPropertyIri The iri of the object property we want to check if it does not contain the individual
	 * as value.
	 * @param ontologyIndividual The individual we want to check.
	 * @return True if the object property with the iri objectPropertyIri does not contain the individual ontologyIndividual
	 * as value, false otherwise.
	 */
	public boolean containsNotObjectPropertyValue(final String objectPropertyIri, final OntologyIndividual ontologyIndividual) {
		return !containsObjectPropertyValue(objectPropertyIri, ontologyIndividual);
	}
	
	/**
	 * Checks if there are references, local or remote, for the information package.
	 * @return True if the information package contains any references, false otherwise.
	 */
	public boolean hasReferences() {
		if (references == null) {
			return false;
		} else if (references.getReferencesCount() == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Adds individuals to an information package. 
	 * @param ontologyIndividuals The individuals that should be added. 
	 * @param informationPackagePath Describes the path through the tree in the user interface. 
	 */
	public void addIndividualsToInformationPackageComponent(final OntologyIndividualCollection ontologyIndividuals, 
			final InformationPackagePath informationPackagePath) {
		final InformationPackageComponent informationPackageComponent = getComponent(informationPackagePath);
		if (informationPackageComponent == null) {
			addInformationPackageComponent(informationPackagePath);
		}
		informationPackageComponent.ontologyIndividuals = ontologyIndividuals;
	}
	
	/**
	 * Determines the individuals that are the value of an object property. 
	 * @param iriOfObjectProperty The iri of the object property. 
	 * @return The found individuals.
	 */
	public Optional<OntologyIndividualCollection> getIndividualsOfObjectProperty(final String iriOfObjectProperty) {
		final OntologyIndividualCollection ontologyIndividuals = objectPropertyValuesMap.get(iriOfObjectProperty);
		if (ontologyIndividuals != null) {
			return ClientOptionalFactory.createOntologyIndividualListOptional(ontologyIndividuals);
		}
		return ClientOptionalFactory.createEmptyOntologyIndividualListOptional();
		
	}
	
	/**
	 * Can be called if the current taxonomy is not longer needed.
	 */
	public void deselectedCurrentTaxonomy() {
		currentTaxonomy = null;
	}
	
	/**
	 * Creates a new information package component and uses the given path to identify the
	 * component in the map.
	 * @param informationPackagePath The path to the component in the gui tree and the identifier
	 * for this component.
	 */
	public void addInformationPackageComponent(InformationPackagePath informationPackagePath) {
		if (informationPackageComponents.get(informationPackagePath) == null) {
			InformationPackageComponent informationPackageComponent = new 
					InformationPackageComponent(informationPackagePath);
			informationPackageComponents.put(informationPackagePath, 
											 informationPackageComponent);
		}
	}
	
	/**
	 * Adds  a reference to a local file on the server.  
	 * @param filename The name of the file. 
	 * @return The reference that has been added to the information package. 
	 */
	public Reference addLocalFileReference(final String filename) {
		final Uuid uuidOfFile = new Uuid();
		final Reference localFileReference = ClientModelFactory.getLocalFileReference(filename, uuidOfFile);
		references.addReference(localFileReference);
		return localFileReference;
	}
	
	/**
	 * Adds  a reference to a file on a remote server.  
	 * @param filename The url where the file can be found.  
	 * @return The reference that has been added to the information package. 
	 */
	public Reference addRemoteFileReference(String url) {
		Reference remoteFileReference = ClientModelFactory.getRemoteFileReference(url);
		references.addReference(remoteFileReference);
		return remoteFileReference;
	}
	
	/**
	 * Counts references in the information package. 
	 * @return The number of references in the information package.
	 */
	public int getReferencesCount() {
		return references.getReferencesCount();
	}
	
	/**
	 * Detemines a reference at a given index. 
	 * @param index The position we are looking at. 
	 * @return The found reference. 
	 */
	public Reference getReferenceAt(final int index) {
		return references.getReference(index);
	}
	
	/**
	 * Loads an archive the user has selected in the gui into the information package
	 * manager for further processing. When the archive is loaded, the user can
	 * edit the data of this archive and update it on the server.
	 * @param archive The archive that should be loaded.
	 */
	public void loadArchive(final Archive archive) {
		this.title = archive.getTitle();
		this.uuid = archive.getUuid();
		this.informationPackageType = archive.getPackageType();
		//final OntologyIndividualList ontologyIndividuals = service
	}
	
	/**
	 * Checks if the individuals have to be requested from the server. This is the case if the information package is in edit mode and
	 * the list with the individuals is null. 
	 * @param informationPackagePath
	 * @return True if the individuals have to be requested from the server, false otherwise.
	 */
	public boolean haveIndividualsToBeRequested(final InformationPackagePath informationPackagePath) {
		final InformationPackageComponent component = informationPackageComponents.get(informationPackagePath);
		if (component == null) {
			return true;
		}
		if (component.ontologyIndividuals == null) {
			return true;
		} else if (editMode) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if the information package manager contains information about an information
	 * package. Is necessary for 
	 * {@link de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.InformationPackageCreationBean}
	 * to decide if the user wants to edit an information package. If so, the data of this information package 
	 * are loaded into the bean.
	 * @return True if there are data of an information package, false otherwise.
	 */
	public boolean containsInformationPackage() {
		if (uuid == null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Adds an ontology class to the information package manager. This means the class has been processed 
	 * during the process of creating or editing an information package.
	 * @param ontologyClass The class that should be added to the information package manager.
	 */
	public void addOntologyClass(final OntologyClass ontologyClass) {
		final String className = ontologyClass.getFullName();
		final OntologyClass classInMap = ontologyClassMap.get(className);
		
		if (classInMap == null) {
			ontologyClassMap.put(className, ontologyClass);
		}
	}
	
	/**
	 * Determines if the information package can contain only one digital
	 * object or multiple digital objects. Returns true, if the information
	 * package is a non-virtual AIU. 
	 * @return True, if the information package may contain multiple digital
	 * objects, false otherwise.
	 */
	public boolean containsMultipleDigitalObject() {
		if (informationPackageType == PackageType.SIU) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Gets a list with all literals contained in this information package.
	 * @return A list with all literals contained in this information package.
	 */
	public PropertyCollection getAllLiterals() {
		final PropertyCollection propertyList = ClientModelFactory.createPropertyList();
		
		for (InformationPackageComponent component: informationPackageComponents.values()) {
			for (int i = 0; i < component.containedProperties.getPropertiesCount(); i++) {
				final OntologyProperty ontologyProperty = component.getPropertyAt(i);
				propertyList.addProperty(ontologyProperty);
			}
		}
		
		return propertyList;
	}
	
	public OntologyClass findOntologyClassByIri(final String classname) {
		/*String iriOfClass = classname;
		if (StringValidator.isNotNullOrEmpty(classname)) {
			iriOfClass = classname.toLowerCase();
		}*/
		return ontologyClassMap.get(classname);
	}
	
	Heruakriegen , ob es Individuen der Klasse X im Informationspaket gibt. Dadurch
	könnten die fehlerhaften Tabellen dann ausgeblendet werden.
	
	/**
	 * Some individuals of the current taxonomy are marked as selected. They are later inserted into the information package.
	 * @param taxonomyIndividuals The individuals of the current taxonomy that should be selected.
	 */
	public void addSelectIndividualsOfCurrentTaxonomy(final TaxonomyIndividualCollection taxonomyIndividuals) {
		if (currentTaxonomy != null) {
			selectedTaxonomyIndividuals.addIndividualsToTaxonomy(currentTaxonomy.getIri(), taxonomyIndividuals);
		}
	}
	
	/**
	 * Adds a property to the information package component at a given path. If there is no
	 * component with this path, is it created.
	 * @param property The proeprty to add.
	 * @param informationPackagePath The path to look for a information package component.
	 */
	public void addProperty(final OntologyProperty property, InformationPackagePath informationPackagePath) {
		if (informationPackagePath == null) {  
			final InformationPackageException exception = InformationPackageException.
					informationPackagePathMayNotBeNullException();
			throw exception;
		}
		if (property == null) {
			//OntologyPropertyException exception = OntologyPropertyException.propertyMayNotBeNull
			//		(this.getClass().getName(), METHOD_ADD_LITERAL);
			//throw exception;
		} 
		final InformationPackageComponent informationPackageComponent = getComponent(informationPackagePath);
		informationPackageComponent.addProperty(property);
	}
	
	/**
	 * Checks if this information package manager contains a SIP.
	 * @return True, if this information package manager contains a SIP, false otherwise.
	 */
	public boolean isSip() {
		if (informationPackageType == PackageType.SIP) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if this information package manager contains a SIU.
	 * @return True, if this information package manager contains a SIU, false otherwise.
	 */
	public boolean isSiu() {
		if (informationPackageType == PackageType.SIU) {
			return true;
		} else {
			return false;
		}
	}
	
	public void cancelInformationPackageCreation() {
		newInformationPackage = true;
	}
	
	public Optional<TaxonomyIndividualCollection> findSelectedIndividualsByTaxonomy(final Taxonomy taxonomy) {
		final Iri iriOfTaxonomy = taxonomy.getIri();
		return selectedTaxonomyIndividuals.findIndividualsOfTaxonomy(iriOfTaxonomy);
	}
	
	/**
	 * Creates an iri for the information package with the uuid of the information
	 * package as local name.
	 * @return The created iri.
	 */
	public Iri getIriOfInformationPackage() {
		return ClientModelFactory.createIri(uuid.toString());
	}
	
	/**
	 * Sets the classes the user has selected for working with in
	 * the step with the mindmap structure. 
	 * @param ontologyClassList A list with the selected classes.
	 */
	public void setFilterClasses(final OntologyClassList ontologyClassList) {
		selectedOntologyClasses = ontologyClassList;
		final OntologyClass ontologyClass = ontologyClassList.getOntologyClassAt(0);
	}
	
	public OntologyClass getFilterClassAt(final int index) {
		return selectedOntologyClasses.getOntologyClassAt(index);
	}
	
	public int getFilterClassesCount() {
		return selectedOntologyClasses.getOntologyClassCount();
	}
	
	/**
	 * Gets a information package component from the map.
	 * @param informationPackagePath The path to look for in the map.
	 * @return The information package component, that is found at the given path, or null, if there
	 * was no component found at this path.
	 */
	private InformationPackageComponent getComponent(final InformationPackagePath informationPackagePath) {
		return informationPackageComponents.get(informationPackagePath);
	}
	
	public PackageType getInformationPackageType() {
		return informationPackageType;
	}
	
	public void setInformationPackageType(final PackageType informationPackageType) {
		this.informationPackageType = informationPackageType;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Uuid getUuid() {
		return uuid;
	}

	public void setUuid(Uuid uuid) {
		this.uuid = uuid;
	}

	// ********** private inner class ***************
	private class InformationPackageComponent {
		/**
		 * A list with the properties that are contained in the information
		 * package path. 
		 */
		private PropertyCollection containedProperties;
		private InformationPackagePath informationPackagePath;
		/**
		 * Contains the individuals of this component. In an information package that is in creation process, this
		 * list is initialized as an empty list. If the information package should be modified, the list is null until the
		 * individuals have been requested from the server.
		 */
		private OntologyIndividualCollection ontologyIndividuals;
		/**
		 * True if the values have not to be requested from the server again because they have been
		 * already send from the server to the client.
		 */
		private boolean alreadyRequested;

		public InformationPackageComponent(final InformationPackagePath informationPackagePath) {
			this.informationPackagePath = informationPackagePath;
			containedProperties = ClientModelFactory.createPropertyList();		
			ontologyIndividuals = ClientModelFactory.createEmptyOntologyIndividualList(); 
		}
		
		/**
		 * Adds a new property to the information package component.
		 * @param property The new property.
		 */	
		public void addProperty(final OntologyProperty property) {
			containedProperties.addProperty(property);
		}
		
		/**
		 * Gets a property at a given index within this component.
		 * @param index The index.
		 * @return The property found at the index.
		 */
		public OntologyProperty getPropertyAt(final int index) {
			return containedProperties.getProperty(index);
		}
		
		/**
		 * Counts the properties contained in this component.
		 * @return The number of properties.
		 */
		public int getPropertiesCount() {
			return containedProperties.getPropertiesCount();
		}
	}

	public View getSelectedView() {
		return selectedView;
	}

	public void setSelectedView(View selectedView) {
		this.selectedView = selectedView;
	}
	
	public void setClientModelFactory(ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}
	
	public void setSerializationFormat(SerializationFormat serializationFormat) {
		this.serializationFormat = serializationFormat;
	}
	
	public SerializationFormat getSerializationFormat() {
		return serializationFormat;
	}

	public void activateEditMode() {
		editMode = true;
	}
	
	public void deativateEditMode() {
		editMode = false;
	}
	
	public boolean isEditMode() {
		return editMode;
	}
	
	public void setReferencesOfInformationPackage(ReferenceCollection references) {
		this.references = references;
	}
	
	public void setIriOfPropertyWithFileReference(String iriOfPropertyWithFileReference) {
		this.iriOfPropertyWithFileReference = iriOfPropertyWithFileReference;
	}
	
	public String getIriOfPropertyWithFileReference() {
		return iriOfPropertyWithFileReference;
	}
	
	public boolean isCancelled() {
		return newInformationPackage;
	}
	
	public void setNewInformationPackage(boolean newInformationPackage) {
		this.newInformationPackage = newInformationPackage;
	}
	
	public void setCurrentTaxonomy(Taxonomy currentTaxonomy) {
		this.currentTaxonomy = currentTaxonomy;
	}
	
	public Taxonomy getCurrentTaxonomy() {
		return currentTaxonomy;
	}

	/**
	 * The individuals that are given to the method are written into the map with the individuals
	 * that are contained in this information package. With other words, this method sets the 
	 * individuals assigned to this information package. 
	 * @param ontologyIndividuals The individuals that are assigned to this information package.
	 */
	public void setIndividualsOfInformationPackage(final OntologyIndividualCollection ontologyIndividuals) {
		for (int i = 0; i < ontologyIndividuals.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = ontologyIndividuals.getIndividual(i);
			containedIndividuals.addIndividual(ontologyIndividual);
		}
	}
	
	/**
	 * Determines the number of digital objects in this information package.
	 * @return The number of digital objects in this information package.
	 */
	public int getDigitalObjectCount() {
		if (digitalObjects == null) {
			return 0;
		} else {
			return digitalObjects.getDigitalObjectCount();
		}
	}
	
	/**
	 * Checks if there are no digital objects contained in this information package.
	 * @return True if there are no digital objects contained in this information package.
	 */
	public boolean noDigitalObjectContained() {
		if (digitalObjects == null || digitalObjects.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if there are digital objects contained in this information package.
	 * @return True if there are digital objects contained in this information package.
	 */
	public boolean digitalObjectContained() {
		return !noDigitalObjectContained();
	}
	
	/**
	 * Determines the digital object at a given index.
	 * @param index The index we are looking at.
	 * @return The digital object found at index or an empty optional.
	 */
	public Optional<DigitalObject> getDigitalObjectAt(int index) {
		if (digitalObjects == null) {
			return ClientOptionalFactory.createEmptyDigitalObjectOptional();
		} else {
			final DigitalObject digitalObject = digitalObjects.getDigitalObjectAt(index);
			return ClientOptionalFactory.createDigitalObjectOptional(digitalObject);
		}
	}
}