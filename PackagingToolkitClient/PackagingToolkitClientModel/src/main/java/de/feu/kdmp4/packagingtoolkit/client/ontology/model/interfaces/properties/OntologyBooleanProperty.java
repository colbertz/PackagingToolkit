package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;


/**
 * A ontology property with a boolean value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyBooleanProperty extends OntologyDatatypeProperty {
	public Boolean getPropertyValue();

	void setPropertyValue(boolean value);
}
