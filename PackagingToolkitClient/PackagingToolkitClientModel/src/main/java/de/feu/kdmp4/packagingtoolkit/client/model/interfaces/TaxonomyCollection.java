package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import java.util.Optional;

/**
 * A collection that contains taxonomies.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomyCollection {
	/**
	 * Adds a taxonomy to the collection.
	 * @param taxonomy The taxonomy that should be added.
	 */
	void addTaxonomy(Taxonomy taxonomy);
	/**
	 * Determines a taxonomy in the collection at a certain position.
	 * @param index The position we are looking at.
	 * @return The found taxonomy or an empty optional.
	 */
	Optional<Taxonomy> getTaxonomy(int index);
	/**
	 * Determines the number of taxonomies in this collection.
	 * @return The number of taxonomies in this collection.
	 */
	int getTaxonomyCount();
	/**
	 * Determines if there are any taxonomies in this collection.
	 * @return True if there are any taxonomies in this collection, false otherwise.
	 */
	boolean containsTaxonomies();
	/**
	 * Determines if there is another taxonomy in this collection. If this method return true
	 * it is safe to call the method nextTaxonomy().
	 * @return True if there is another taxonomy in this collection, false otherwise.
	 */
	boolean hasNextTaxonomy();
	/**
	 * Returns the next taxonomy in this collection.
	 * @return The next taxonomy in this collection if any or an empty optional.
	 */
	Optional<Taxonomy> nextTaxonomy();
	/**
	 * Finds a taxonomy in the collection with the help of its root individual. 
	 * @param rootIndividual The individual that is the root of the taxonomy we are looking for.
	 * @return The found taxonomy or an empty optional.
	 */
	Optional<Taxonomy> getTaxonomyByRootIndividual(TaxonomyIndividual rootIndividual);
	/**
	 * Finds the taxonomy in the collection that has a title. 
	 * @return The taxonomy with a title.  
	 */
	Taxonomy findTaxonomyWithTitle();
}
