package de.feu.kdmp4.packagingtoolkit.client.i18n;

import java.util.ResourceBundle;

/**
 * Encapsulates the keys for internationalization. Java code is not allowed
 * to call the message bundles but it has to use this class and its static
 * methods. Therefore the other classes do not have to know the names of 
 * the keys. This class contains the keys only for messages.
 * @author Christopher Olbertz
 *
 */
public class I18nMessagesUtil {
	private static final String CONFIGURATION_SAVED = "configuration-saved";
	private static final String CONFIGURATION_SAVING_ERROR = "configuration-saving-error";
	private static final String EMPTY_REFERENCES_LIST = "empty-references-list";
	private static final String ERROR = "error";
	private static final String ERROR_UPLOADING_ONTOLOGY = "error-uploading-ontology";
	private static final String INFORMATION = "information";
	private static final String INFORMATION_PACKAGE_SAVED = "information-package-saved";
	private static final String INVALID_URL = "invalid-url";
	private static final String NOT_ALLOWED_ONTOLOGY_FILE_TYPE = "not-allowed-ontology-file-type";
	private static final String LOGO_UPLOADED = "logo-uploaded";
	private static final String ONTOLOGY_UPLOADED = "ontology-uploaded";
	private static final String SELECT_VIEW_FOR_DELETING = "select-view-for-deleting";
	private static final String SUCCESS = "success";
	private static final String VIEW_DELETED = "view-deleted";
	private static final String VIEW_MUST_CONTAIN_CLASSES = "view-must-contain-classes";
	private static final String VIEW_NAME_MAY_NOT_BE_EMPTY = "view-name-may-not-be-empty";
	private static final String VIEW_SAVED = "view-saved";
	
	private static ResourceBundle messagesResourceBundle;
	
	static {
		messagesResourceBundle = I18nUtil.getMessagesResourceBundle();
	}
	
	public static String getConfigurationSavedString() {
		return messagesResourceBundle.getString(CONFIGURATION_SAVED);
	}
	
	public static String getConfigurationSavingErrorString() {
		return messagesResourceBundle.getString(CONFIGURATION_SAVING_ERROR);
	}

	public static String getEmptyReferencesListString() {
		return messagesResourceBundle.getString(EMPTY_REFERENCES_LIST);
	}
	
	public static String getErrorString() {
		return messagesResourceBundle.getString(ERROR);
	}
	
	public static String getInformationString() {
		return messagesResourceBundle.getString(INFORMATION);
	}
	
	public static String getInformationPackageSavedString() {
		return messagesResourceBundle.getString(INFORMATION_PACKAGE_SAVED);
	}

	public static String getErrorUploadingOntologyString() {
		return messagesResourceBundle.getString(ERROR_UPLOADING_ONTOLOGY);
	}

	public static String getSelectViewForDeletingString() {
		return messagesResourceBundle.getString(SELECT_VIEW_FOR_DELETING);
	}
	
	public static String getInvalidUrlString() {
		return messagesResourceBundle.getString(INVALID_URL);
	}

	public static String getNotAllowedOntologyFileTypeString() {
		return messagesResourceBundle.getString(NOT_ALLOWED_ONTOLOGY_FILE_TYPE);
	}
	
	public static String getViewNameMayNotBeEmptyString() {
		return messagesResourceBundle.getString(VIEW_NAME_MAY_NOT_BE_EMPTY);
	}
	
	public static String getOntologyUploadedString() {
		return messagesResourceBundle.getString(ONTOLOGY_UPLOADED);
	}
	
	public static String getLogoUploadedString() {
		return messagesResourceBundle.getString(LOGO_UPLOADED);
	}
	
	public static String getSuccessString() {
		return messagesResourceBundle.getString(SUCCESS);
	}
	
	public static String getViewMustContainClassesString() {
		return messagesResourceBundle.getString(VIEW_MUST_CONTAIN_CLASSES);
	}
	
	public static String getViewSavedString() {
		return messagesResourceBundle.getString(VIEW_SAVED);
	}
	
	public static String getViewDeletedString() {
		return messagesResourceBundle.getString(VIEW_DELETED);
	}
}
