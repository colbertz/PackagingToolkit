package de.feu.kdmp4.packagingtoolkit.client.datastructures.classes;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces.TaxonomyIndividualsAndTheirIrisMap;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;

public class TaxonomyIndividualsAndTheirIrisHashMap implements TaxonomyIndividualsAndTheirIrisMap {
	/**
	 * Contains the iri of an individual as key and the individual object itself as value. 
	 */
	private Map<Iri, TaxonomyIndividual> taxonomyIndividuals;
	
	public TaxonomyIndividualsAndTheirIrisHashMap() {
		taxonomyIndividuals = new HashMap<>();
	}
	
	@Override
	public void addIndividual(final TaxonomyIndividual taxonomyIndividual) {
		if (taxonomyIndividual != null && taxonomyIndividual.getIri() != null) {
			taxonomyIndividuals.put(taxonomyIndividual.getIri(), taxonomyIndividual);
		}
	}
	
	@Override
	public TaxonomyIndividual getIndividual(final Iri iriOfIndividual) {
		return taxonomyIndividuals.get(iriOfIndividual);
	}
	
	// DELETE_ME
	/*@Override
	public void replaceIndividual(final TaxonomyIndividual newTaxonomyIndividual) {
		final Iri iriOfNewIndividual = newTaxonomyIndividual.getIri();
		taxonomyIndividuals.remove(iriOfNewIndividual);
		taxonomyIndividuals.put(iriOfNewIndividual, newTaxonomyIndividual);
	}*/
}
