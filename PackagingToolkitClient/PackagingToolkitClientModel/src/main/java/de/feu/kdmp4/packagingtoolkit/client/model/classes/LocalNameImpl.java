package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Represents a local name. The local name is the part after the # symbol in an iri. This object 
 * encapsulates a String and exists for the type safety.
 * @author Christopher Olbertz
 *
 */
public class LocalNameImpl implements LocalName {
	/**
	 * The string that contains the local name and is encapsulated by this class.
	 */
	private String localName;
	
	public LocalNameImpl(final String localName) {
		this.localName = localName;
	}
	
	@Override
	public String getLocalName() {
		return localName;
	}
	
	@Override
	public String toString() {
		return localName;
	}
	
	@Override
	public boolean isNotEmpty() {
		if (StringValidator.isNotNullOrEmpty(localName)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localName == null) ? 0 : localName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalNameImpl other = (LocalNameImpl) obj;
		if (localName == null) {
			if (other.localName != null)
				return false;
		} else if (!localName.equals(other.localName))
			return false;
		return true;
	}
}
