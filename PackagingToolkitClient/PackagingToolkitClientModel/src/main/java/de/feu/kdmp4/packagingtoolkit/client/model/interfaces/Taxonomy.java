package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;


/**
 * A taxonomy is a scheme for classification of objects. It is a hierarchical classification. The taxonomy contains
 * individuals and starts with a root individual. This individual contains so called narrowers. These are individuals
 * that follow their parent individual in the taxonomy.  
 * @author Christopher Olbertz
 *
 */
public interface Taxonomy {
	/**
	 * Returns the individual the taxonomy starts with. This individual contains the top level concepts as
	 * narrower.
	 * @return The individual the taxonomy starts with.
	 */
	TaxonomyIndividual getRootIndividual();
	String getTitle();
	Namespace getNamespace();
	Iri getIri();
}
