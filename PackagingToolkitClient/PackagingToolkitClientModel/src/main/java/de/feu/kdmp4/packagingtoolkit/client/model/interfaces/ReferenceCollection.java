package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import java.util.Optional;

public interface ReferenceCollection {
	void addReference(final Reference reference);
	/**
	 * Gets the reference at a certain index.
	 * @param index The index of the element to determine.
	 * @return The reference that we are looking for.
	 */
	Reference getReference(final int index);
	/**
	 * Determines the number of references in this collection.
	 * @return The number of references in this collection.
	 */
	int getReferencesCount();
	/**
	 * Removes the reference with a certain url from the collection.
	 * @param url The url of the reference to remove.
	 * @return An optional with the removed reference or an empty optional if the reference was not found in the 
	 * collection.
	 */
	Optional<Reference> removeReference(final String url);
}
