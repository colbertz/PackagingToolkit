package de.feu.kdmp4.packagingtoolkit.client.datastructures.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;

/**
 * Assigns classes from an ontology to their properties. Offers quick access to a 
 * list with the properties of a class. 
 * @author Christopher Olbertz
 *
 */
public interface OntologyClassesAndTheirPropertiesMap {
	/**
	 * Adds a property to the property list of an ontology class. The property can either be a datatype or
	 * an object property.
	 * @param className The class where the property is added to.
	 * @param property The property that should be added to a class.
	 */
	public void addProperty(final String className, final OntologyProperty ontologyProperty);
	/**
	 * Gets the datatype property list of a given class.
	 * @param className The name of the class. This is the key in the map.
	 * @return The datatype property list of the class. If there are no datatype properties 
	 * the return value is an empty datatype property list.
	 */
	DatatypePropertyCollection getDatatypeProperties(final String className);
	/**
	 * Creates an ontology class in the map with empty property lists.
	 * @param className The name of the class and the key for the map. 
	 */
	void addClass(final OntologyClass ontologyClass);
	/**
	 * Returns the number of datatype property entries in the map.
	 * @return
	 */
	int getDatatypePropertyListsCount();
	/**
	 * Returns the number of object property entries in the map.
	 * @return
	 */
	int getObjectPropertyListsCount();
	/**
	 * Gets the object property list of a given class.
	 * @param className The name of the class. This is the key in the map.
	 * @return The object property list of the class. If there are no properties 
	 * the return value is an empty object property list.
	 */
	ObjectPropertyCollection getObjectProperties(final String className);
}
