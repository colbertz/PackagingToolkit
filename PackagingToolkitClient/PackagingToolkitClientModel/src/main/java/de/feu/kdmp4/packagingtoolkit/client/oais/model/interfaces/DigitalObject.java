package de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.MetadataMap;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface DigitalObject extends Comparable<DigitalObject> {
	public String getAbsolutePath();
	Uuid getUuidOfDigitalObject();
	Uuid getUuidOfInformationPackage();

	void setUuidOfInformationPackage(Uuid uuidOfInformationPackage);
	void addMetadata(String key, String value);
	int getMetadataCount();
	StringList getKeysOfMetadata();
	String getMetadataValue(String key);
	MetadataMap getMetadataMap();
	/**
	 * Determines only the file name without any path information.
	 * @return The filename and the extension.
	 */
	String getFilename();
}
