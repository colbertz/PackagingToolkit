package de.feu.kdmp4.packagingtoolkit.client.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.feu.kdmp4.packagingtoolkit.client.enums.Views;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;

/**
 * Encapsulates the most frequent statements for logging. Allows that the other classes
 * do not need code for logging.
 * @author Christopher Olbertz
 *
 */
public class ClientLogUtil {
	private static Log log = LogFactory.getLog(ClientLogUtil.class);
	
	private ClientLogUtil() { }
	
	public static void logNavigation(Views theView) {
		log.debug("Navigation to: " + theView.getUrl());
	}
	
	public static void logUserException(UserException userException) {
		log.debug("UserException: " + userException.getMessage());
	}
}
