package de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties;

import java.io.Serializable;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyElement;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Represents a property in an ontology. A property has a value.
 * @author Christopher Olbertz
 *
 */
public interface OntologyProperty extends OntologyElement, Serializable, Cloneable {
	String getPropertyRange();
	boolean isObjectProperty();
	String getPropertyId();
	boolean hasValue();
	//void setPropertyValue(final Object value);
	Object getPropertyValue();
	String getNamespace();
	Uuid getUuid();
	void setUuid(Uuid uuid);
	String getLabel();
	boolean isBooleanProperty();
	boolean isByteProperty();
	boolean isDateProperty();
	boolean isDateTimeProperty();
	boolean isDoubleProperty();
	boolean isDurationProperty();
	boolean isFloatProperty();
	boolean isHasTaxonomyProperty();
	boolean isIntegerProperty();
	boolean isLongProperty();
	boolean isShortProperty();
	boolean isStringProperty();
	boolean isTimeProperty();
	void setPropertyId(String propertyId);
	void setNamespace(String namespace);
	void setLabel(String label);
}
