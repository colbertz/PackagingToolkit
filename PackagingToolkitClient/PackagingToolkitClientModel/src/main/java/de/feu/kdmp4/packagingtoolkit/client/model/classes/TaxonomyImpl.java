package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;


public class TaxonomyImpl implements Taxonomy {
	/**
	 * This is the individual the taxonomy starts with. 
	 */
	private TaxonomyIndividual rootIndividual;
	/**
	 * A title for the taxonomy that can for example be displayed in the user interface.
	 */
	private String title;
	/**
	 * A namespace for the taxonomy. The taxonomy can be identified uniquely by this namespace.
	 */
	private Namespace namespace;
	
	public TaxonomyImpl(final TaxonomyIndividual rootIndividual, final String title, final Namespace namespace) {
		this.rootIndividual = rootIndividual;
		this.title = title;
		this.namespace = namespace;
	}
	
	@Override
	public TaxonomyIndividual getRootIndividual() {
		return rootIndividual;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public Namespace getNamespace() {
		return namespace;
	}

	@Override
	public Iri getIri() {
		final LocalName localName = ClientModelFactory.createLocalName(StringUtils.EMPTY_STRING);
		return ClientModelFactory.createIri(namespace, localName);
	}
}
