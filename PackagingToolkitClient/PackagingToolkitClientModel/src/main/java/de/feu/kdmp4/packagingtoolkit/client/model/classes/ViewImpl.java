package de.feu.kdmp4.packagingtoolkit.client.model.classes;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;

public class ViewImpl implements View {
	private OntologyClassList containedOntologyClasses;
	private String viewName;
	private int viewId;
	
	public ViewImpl() {
	
	}
	
	public ViewImpl(final String viewName, final int viewId) {
		this.viewName = viewName;
		this.viewId = viewId;
		containedOntologyClasses = ClientModelFactory.createEmptyOntologyClassList();
	}
	
	public ViewImpl(final String viewName) {
		this.viewName = viewName;
		containedOntologyClasses = ClientModelFactory.createEmptyOntologyClassList();
	}
	
	@Override
	public void addOntologyClass(final OntologyClass ontologyClass) {
		containedOntologyClasses.addOntologyClassToList(ontologyClass);
	}
	
	@Override
	public OntologyClass getOntologyClassAt(final int index) {
		return containedOntologyClasses.getOntologyClassAt(index);
	}
	
	@Override
	public int getClassesCount() {
		return containedOntologyClasses.getOntologyClassCount();
	}
	
	@Override
	public String getViewname() {
		return viewName;
	}
	
	@Override
	public int getViewId() {
		return viewId;
	}

	@Override
	public void addOntologyClasses(final OntologyClassList ontologyClassList) {
		this.containedOntologyClasses = ontologyClassList;
	}

	@Override
	public boolean isNewView() {
		return false;
	}
}
