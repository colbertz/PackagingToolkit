package de.feu.kdmp4.packagingtoolkit.client.model.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;

public interface OntologyClassList {
	/**
	 * Adds an ontology class to the list.
	 * @param ontologyClass The ontology class that should be added to the list.
	 */
	void addOntologyClassToList(final OntologyClass ontologyClass);
	/**
	 * Gets an ontology class at a given index.
	 * @param index The index of the ontology class that should be determined.
	 * @return The found ontologyClass.
	 * @throws ListIndexInvalidException Is thrown if the index is not 
	 * a valid index in the list.
	 */
	OntologyClass getOntologyClassAt(final int index);
	/**
	 * Counts the ontologyClasss in the list.
	 * @return The number of the ontologyClasss in the list.
	 */
	int getOntologyClassCount();
	/**
	 * Removes an ontologyClass from the list. 
	 * @param ontologyClass The ontologyClass that should be removed.
	 * @throws OntologyClassMayNotBeNullException Is thrown if the ontologyClass is null.
	 */
	void removeOntologyClass(final OntologyClass ontologyClass);
}
