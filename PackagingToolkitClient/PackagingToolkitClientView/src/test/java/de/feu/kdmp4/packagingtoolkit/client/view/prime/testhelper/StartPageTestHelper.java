package de.feu.kdmp4.packagingtoolkit.client.view.prime.testhelper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import de.feu.kdmp4.packagingtoolkit.client.view.prime.AbstractSeleniumTest;

public class StartPageTestHelper extends AbstractSeleniumTest {
	private static final String ID_FORM_STARTPAGE = "frmStartpage";
	private static final String ID_LINK_SHOW_ARCHIVE_LIST = ID_FORM_STARTPAGE  + ":linkShowArchiveList";
	private static final String ID_LINK_CREATE_INFORMRATION_PACKAGE = ID_FORM_STARTPAGE  + ":linkCreateInformationPackage";
	private static final String ID_LINK_CONFIGURATION = ID_FORM_STARTPAGE  + ":linkConfiguration";
	
	private static final String URL_START_PAGE = "http://localhost:9090/index.jsf"; 
	
	static {
		setUpClass();
		gotoStartPage();
	}
	
	public static void gotoStartPage() {
		firefoxDriver.get(URL_START_PAGE);
	}
	
	/**
	 * Checks if we are on the start page by checking if the link that leads to the configuration is existent. It
	 * exists only on the start page.
	 * @return True if we are on the start page false otherwise.
	 */
	public static boolean isStartPage() {
		WebElement linkConfiguration = firefoxDriver.findElement(By.id(ID_LINK_CONFIGURATION));
		if (linkConfiguration == null) {
			return false;
		} else {
			return true;
		}
	}
}
