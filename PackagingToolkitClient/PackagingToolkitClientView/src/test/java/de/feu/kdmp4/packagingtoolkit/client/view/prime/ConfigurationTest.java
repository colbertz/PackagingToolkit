package de.feu.kdmp4.packagingtoolkit.client.view.prime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.crypto.CipherInputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.client.operations.classes.MockedServerOperationsImpl;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ServerCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.testhelper.ConfigurationTestHelper;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.testhelper.StartPageTestHelper;

/**
 * Tests the page for entering the configuration data for client, server and mediator.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationTest {
	private static final String SERVER_URL = "http://localhost:9292";
	private static final String MEDIATOR_URL = "http://localhost:9191";
	private static final String RESULT_DIR = "result";
	private static final String BASE_ONTOLOGY = "baseOntology";
	private static final String RULE_ONTOLOGY = "ruleOntology";

	private ServerCommunicationOperations serverCommunicationOperations;
	
	@Before
	public void setUp() {
		serverCommunicationOperations = new MockedServerOperationsImpl();
	}

	/**
	 * Enters the correct data for the client configuration and saves them. This is important because the data
	 * could have been changed during the tests.
	 */
	@After 
	public void tearDown() {
		ConfigurationTestHelper.setValueOfTxtMediatorUrl(MEDIATOR_URL);
		ConfigurationTestHelper.setValueOfTxtServerUrl(SERVER_URL);
		ConfigurationTestHelper.setValueOfTxtResultDir(RESULT_DIR);
		ConfigurationTestHelper.hitBtnSaveClientConfiguration();
	}
	
	/**
	 * Tests the initialization of the text field for the result directory. 
	 */
	@Test
	public void testInitializationTxtResultDir() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		String actualResultDir = ConfigurationTestHelper.getValueOfTxtResultDir();
		assertEquals(RESULT_DIR, actualResultDir);
	}
	
	/**
	 * Tests the initialization of the text field for the server url. 
	 */
	@Test
	public void testInitializationTxtServerUrl() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		String actualServerUrl = ConfigurationTestHelper.getValueOfTxtServerUrl();
		assertEquals(SERVER_URL, actualServerUrl);
	}
	
	/**
	 * Tests the initialization of the text field for the mediator url. 
	 */
	@Test
	public void testInitializationTxtMediatorUrl() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		String actualMediatorUrl = ConfigurationTestHelper.getValueOfTxtMediatorUrl();
		assertEquals(MEDIATOR_URL, actualMediatorUrl);
	}
	
	/**
	 * Tests the initialization of the text field for the working directory path. 
	 */
	@Test
	public void testInitializationTxtWorkingDirectoryPath() {
		ConfigurationTestHelper.hitTabConfigurationOfServer();
		String actualBaseOntology = ConfigurationTestHelper.getValueOfLabelBaseOntology();
		assertEquals(BASE_ONTOLOGY, actualBaseOntology);
	}
	
	/**
	 * Tests the initialization of the label field with the base ontology path. 
	 */
	@Test
	public void testInitializationLblBaseOntology() {
		ConfigurationTestHelper.hitTabConfigurationOfServer();
		String actualBaseOntology = ConfigurationTestHelper.getValueOfLabelBaseOntology();
		assertEquals(BASE_ONTOLOGY, actualBaseOntology);
	}
	
	/**
	 * Tests the initialization of the label field with the rule ontology path. 
	 */
	@Test
	public void testInitializationLblRuleOntology() {
		ConfigurationTestHelper.hitTabConfigurationOfServer();
		String actualRuleOntology = ConfigurationTestHelper.getValueOfLabelRuleOntology();
		assertEquals(RULE_ONTOLOGY, actualRuleOntology);
	}
	
	/**
	 * Changes the text in the input field for the result directory and clicks the submit button. The value in the
	 * input field may not be changed.
	 */
	@Test
	public void testChangeResultDir() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		final String newResultDir = "resultDir";
		ConfigurationTestHelper.setValueOfTxtResultDir(newResultDir);
		ConfigurationTestHelper.hitBtnSaveClientConfiguration();
		String actualResultDir = ConfigurationTestHelper.getValueOfTxtResultDir();
		assertEquals(newResultDir, actualResultDir);
	}
	
	/**
	 * Changes the text in the input field for the working directory and clicks the submit button. The value in the
	 * input field may not be changed.
	 */
	@Test
	public void testWorkingDirectory() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		final String newWorkingDirectory = "workingDir";
		ConfigurationTestHelper.setValueOfTxtWorkingDirectory(newWorkingDirectory);
		ConfigurationTestHelper.hitBtnSaveServerConfiguration();
		String actualWorkingDirectory = ConfigurationTestHelper.getValueOfTxtResultDir();
		assertEquals(newWorkingDirectory, actualWorkingDirectory);
	}
	
	/**
	 * Changes the text in the input field for the server url and clicks the submit button. The value in the
	 * input field may not be changed.
	 */
	@Test
	public void testChangeServerUrl() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		final String newServerUrl = "http://localhost:7070";
		ConfigurationTestHelper.setValueOfTxtServerUrl(newServerUrl);
		ConfigurationTestHelper.hitBtnSaveClientConfiguration();
		
		String actualServerUrl = ConfigurationTestHelper.getValueOfTxtServerUrl();
		assertEquals(newServerUrl, actualServerUrl);
	}
	
	/**
	 * Changes the text in the input field for the mediator url and clicks the submit button. The value in the
	 * input field may not be changed.
	 */
	@Test
	public void testChangeMediatorUrl() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		final String newMediatorUrl = "http://localhost:7171";
		ConfigurationTestHelper.setValueOfTxtMediatorUrl(newMediatorUrl);
		ConfigurationTestHelper.hitBtnSaveClientConfiguration();
		
		String actualMediatorUrl = ConfigurationTestHelper.getValueOfTxtMediatorUrl();
		assertEquals(newMediatorUrl, actualMediatorUrl);
	}
	
	/**
	 * Tests the back button. It works if we go back to the start page.
	 */
	@Test
	public void testBackButtonClient() {
		ConfigurationTestHelper.hitTabConfigurationOfClient();
		ConfigurationTestHelper.hitBtnBackClient();
		boolean weAreOnStartPage = StartPageTestHelper.isStartPage();
		assertTrue(weAreOnStartPage);
		ConfigurationTestHelper.goToConfigurationPage();
	}
	
	@Test
	public void testDataSourcesPanelInitialization() {
		ConfigurationTestHelper.hitTabConfigurationOfMediator();
		boolean complete = ConfigurationTestHelper.isDataSourcePanelComplete();
		assertTrue(complete);
	}
}
