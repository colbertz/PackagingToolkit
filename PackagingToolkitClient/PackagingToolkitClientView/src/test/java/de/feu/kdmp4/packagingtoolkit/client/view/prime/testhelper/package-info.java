/**
 * Contains some helper classes for the tests with Selenium.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.client.view.prime.testhelper;