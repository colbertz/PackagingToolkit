package de.feu.kdmp4.packagingtoolkit.client.view.prime.testhelper;

import static de.feu.kdmp4.packagingtoolkit.client.view.prime.AbstractSeleniumTest.waitUntilElementIsVisible;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.feu.kdmp4.packagingtoolkit.client.view.prime.AbstractSeleniumTest;

public class CreateInformationPackageTestHelper extends AbstractSeleniumTest  {
	private static final String ID_FORM_CREATE_INFORMATION_PACKAGE = "frmCreateInformationPackage";
	//private static final String ID_PANEL_GRID = ID_FORM_CREATE_INFORMATION_PACKAGE + ":panelGrid";
	private static final String ID_PANEL_DIGITAL_OBJECTS_TABLE = ID_FORM_CREATE_INFORMATION_PACKAGE + ":pnlDigitalObjectsTable";
	private static final String ID_PANEL_REFERENCE_TABLE = ID_FORM_CREATE_INFORMATION_PACKAGE + ":pnlReferencesTable";
	
	private static final String ID_BUTTON_ADD_REFERENCE = ID_FORM_CREATE_INFORMATION_PACKAGE + ":btnAddReference";
	private static final String ID_BUTTON_NEXT_STEP = ID_FORM_CREATE_INFORMATION_PACKAGE + ":btnNextStep";
	private static final String ID_BUTTON_CANCEL = ID_FORM_CREATE_INFORMATION_PACKAGE + ":btnCancel";
	private static final String ID_BUTTON_CHOOSE = ID_FORM_CREATE_INFORMATION_PACKAGE + ":digitalObjectUpload_label";
	
	private static final String ID_TEXT_FIELD_PACKAGE_TITLE = ID_FORM_CREATE_INFORMATION_PACKAGE + ":txtPackageTitle";
	private static final String ID_TEXT_FIELD_NEW_REFERENCE = ID_FORM_CREATE_INFORMATION_PACKAGE + ":txtNewReference";
	
	private static final String ID_LABEL_PACKAGE_TYPE = ID_FORM_CREATE_INFORMATION_PACKAGE + ":lblPackageType";
	private static final String ID_LABEL_BASE_ONTOLOGY = ID_FORM_CREATE_INFORMATION_PACKAGE + ":lblBaseOntology";
	
	private static final String ID_DIGITAL_OBJECT_UPLOAD = ID_FORM_CREATE_INFORMATION_PACKAGE + ":digitalObjectUpload";
	
	private static final String URL_CREATE_INFORMATION_PACKAGE_PAGE = "http://localhost:9090/createInformationPackage.jsf";
	
	//private static final String XPATH_CHOOSE_BUTTON = "//a[@href='#tabViewMain:tabConfigurationServer']";
	
	static {
		setUpClass();
		goToCreateInformationPackagePage();
	}
	
	public static void waitUntilDigitalObjectUploadIsVisible() {
		waitUntilElementIsVisible(ID_DIGITAL_OBJECT_UPLOAD);
	}

	public static void waitUntilBtnNextStepVisible() {
		waitUntilElementIsVisible(ID_BUTTON_NEXT_STEP);
	}
	
	public static void waitUntilTxtNewReferenceVisible() {
		waitUntilElementIsVisible(ID_TEXT_FIELD_NEW_REFERENCE);
	}
	
	public static void waitUntilTableDigitalObjectsVisible() {
		waitUntilElementIsVisible(ID_PANEL_DIGITAL_OBJECTS_TABLE);
	}
	
	public static void waitUntilTableReferencesVisible() {
		waitUntilElementIsVisible(ID_PANEL_REFERENCE_TABLE);
	}
	
	public static void waitUntilBtnAddReferenceVisible() {
		waitUntilElementIsVisible(ID_BUTTON_ADD_REFERENCE);
	}
	
	public static void goToCreateInformationPackagePage() {
		firefoxDriver.get(URL_CREATE_INFORMATION_PACKAGE_PAGE);
	}
	
	public static String getValueOfTxtInformationPackageTitle() {
		return getValueOfWebElementById(ID_TEXT_FIELD_PACKAGE_TITLE);
	}
	
	public static String getValueOfTxtNewReference() {
		return getValueOfWebElementById(ID_TEXT_FIELD_NEW_REFERENCE);
	}
	
	public static String getTextOfLblPackageType() {
		return getTextOfWebElementById(ID_LABEL_PACKAGE_TYPE);
	}
	
	public static String getTextOfLblBaseOntology() {
		return getTextOfWebElementById(ID_LABEL_BASE_ONTOLOGY);
	}
	
	public static void setValueOfTxtInformationPackageTitle(String newValue) {
		setValueOfWebElementById(newValue, ID_TEXT_FIELD_PACKAGE_TITLE);
	}
	
	public static void setValueOfTxtNewReference(String newValue) {
		setValueOfWebElementById(newValue, ID_TEXT_FIELD_NEW_REFERENCE);
	}
	
	public static boolean isBtnNextStepEnabled() {
		return isWebElementEnabledById(ID_BUTTON_NEXT_STEP);
	}
	
	public static boolean isBtnCancelEnabled() {
		return isWebElementEnabledById(ID_BUTTON_CANCEL);
	}
	
	public static boolean isBtnAddReferenceEnabled() {
		return isWebElementEnabledById(ID_BUTTON_ADD_REFERENCE);
	}
	
	public static boolean isTableReferencesVisible() {
		return isWebElementVisibleById(ID_PANEL_REFERENCE_TABLE);
	}
	
	public static boolean isTableDigitalObjectsVisible() {
		return isWebElementVisibleById(ID_PANEL_DIGITAL_OBJECTS_TABLE);
	}
	
	public static boolean isTableDigitalObjectsEnabled() {
		return isWebElementEnabledById(ID_PANEL_DIGITAL_OBJECTS_TABLE);
	}
	
	public static boolean isDigitalObjectUploadEnabled() {
		WebElement chooseButton = firefoxDriver.findElement(By.className("ui-fileupload-choose"));
		String cssClasses = chooseButton.getAttribute("class");
		if (cssClasses.contains("ui-state-disabled")) {
			return false;
		} else {
			return true;
		}
		//return isWebElementEnabledById(ID_BUTTON_CHOOSE);
	}
	
	public static boolean isTxtNewReferenceEnabled() {
		return isWebElementEnabledById(ID_TEXT_FIELD_NEW_REFERENCE);
	}
	
	public static void hitAddNewReferenceButton() {
		hitButton(ID_BUTTON_ADD_REFERENCE);
	}
	
	public static void uploadDigitalObject(File uploadFile) {
		WebElement chooseButton = firefoxDriver.findElement(By.className("ui-fileupload-choose"));
		chooseButton.sendKeys(uploadFile.getAbsolutePath());
	}
}
