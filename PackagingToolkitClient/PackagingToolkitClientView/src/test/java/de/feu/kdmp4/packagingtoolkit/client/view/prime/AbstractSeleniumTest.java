package de.feu.kdmp4.packagingtoolkit.client.view.prime;

import java.io.File;

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractSeleniumTest {
	protected static FirefoxDriver firefoxDriver;
	
	public static void setUpClass() {
		System.setProperty("webdriver.gecko.driver", "/opt/geckodriver/geckodriver");
		firefoxDriver = new FirefoxDriver();
	}
	
	public static void waitUntilElementIsVisible(String id) {
		new WebDriverWait(firefoxDriver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
	}
	
	public static String getValueOfWebElementById(String id) {
		WebElement webElement = firefoxDriver.findElement(By.id(id));
		return webElement.getAttribute("value");
	}
	
	public static String getTextOfWebElementById(String id) {
		WebElement webElement = firefoxDriver.findElement(By.id(id));
		return webElement.getText();
	}
	
	
	public static void setValueOfWebElementById(String newValue, String id) {
		WebElement webElement = firefoxDriver.findElement(By.id(id));
		webElement.clear();  
		webElement.sendKeys(newValue);
	}
	
	public static boolean isWebElementEnabledById(String id) {
		WebElement webElement = firefoxDriver.findElement(By.id(id)); 
		return webElement.isEnabled();
	}
	
	public static boolean isWebElementVisibleById(String id) {
		WebElement webElement = firefoxDriver.findElement(By.id(id)); 
		return webElement.isDisplayed();
	}
	
	public static void hitButton(String id) {
		WebElement button = firefoxDriver.findElement(By.id(id)); 
		button.submit();
	}
	
	public static void startUpload(String idOfUploadComponent, File uploadFile) {
		firefoxDriver.findElement(By.id(idOfUploadComponent)).sendKeys(uploadFile.getAbsolutePath());
	}
}
