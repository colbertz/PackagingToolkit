package de.feu.kdmp4.packagingtoolkit.client.view.prime.testhelper;

import org.apache.bcel.generic.IFNONNULL;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import de.feu.kdmp4.packagingtoolkit.client.view.prime.AbstractSeleniumTest;

public class ConfigurationTestHelper extends AbstractSeleniumTest {
	private static final String ID_TAB_VIEW = "tabViewMain";
	private static final String ID_TAB_CONFIGURATION_CLIENT = "tabConfigurationClient";
	private static final String ID_TAB_CONFIGURATION_SERVER = "tabConfigurationServer";
	private static final String ID_TAB_CONFIGURATION_MEDIATOR = "tabConfigurationMediator";
	private static final String ID_FORM_CLIENT_CONFIGURATION = ID_TAB_VIEW + ":frmClientConfiguration";
	private static final String ID_FORM_SERVER_CONFIGURATION = ":frmServerConfiguration";
	private static final String ID_CC_SERVER_CONFIGURATION = ID_TAB_VIEW + ":ccServerConfiguration";
	
	private static final String ID_BTN_SAVE_CLIENT_CONFIGURATION = ID_FORM_CLIENT_CONFIGURATION + ":btnSaveClientConfiguration";
	private static final String ID_BTN_SAVE_SERVER_CONFIGURATION = ID_FORM_CLIENT_CONFIGURATION + ":btnSaveServerConfiguration";
	private static final String ID_BTN_BACK_SERVER = ID_FORM_CLIENT_CONFIGURATION + ":btnBackServer";
	private static final String ID_BTN_BACK_CLIENT = ID_FORM_CLIENT_CONFIGURATION + ":btnBackClient";
	
	private static final String ID_TXT_RESULT_DIR = ID_FORM_CLIENT_CONFIGURATION + ":txtResultDir";
	private static final String ID_TXT_SERVER_URL = ID_FORM_CLIENT_CONFIGURATION + ":txtServerUrl";
	private static final String ID_TXT_WORKING_DIRECTORY_PATH = ID_CC_SERVER_CONFIGURATION + ID_FORM_SERVER_CONFIGURATION + ":txtWorkingDirectoryPath";
	private static final String ID_TXT_MEDIATOR_URL = ID_FORM_CLIENT_CONFIGURATION + ":txtMediatorUrl";
	private static final String ID_LBL_BASE_ONTOLOGY = ID_FORM_SERVER_CONFIGURATION + ":lblBaseOntology";
	private static final String ID_LBL_RULE_ONTOLOGY = ID_FORM_SERVER_CONFIGURATION + ":lblRuleOntology";
	
	private static final String ID_PANEL_DATASOURCES = ID_FORM_SERVER_CONFIGURATION + ":pnlDataSources";
	
	private static final String URL_CONFIGURATION_PAGE = "http://localhost:9090/configuration.jsf"; 
	
	static {
		setUpClass();
		goToConfigurationPage();
	}
	
	public static void goToConfigurationPage() {
		firefoxDriver.get(URL_CONFIGURATION_PAGE);
	}
	
	public static String getValueOfTxtResultDir() {
		WebElement txtResultDir = firefoxDriver.findElement(By.id(ID_TXT_RESULT_DIR));
		return txtResultDir.getAttribute("value");
	}
	
	public static String getValueOfTxtWorkingDirectory() {
		WebElement txtResultDir = firefoxDriver.findElement(By.id(ID_TXT_WORKING_DIRECTORY_PATH));
		return txtResultDir.getAttribute("value");
	}
	
	public static String getValueOfTxtServerUrl() {
		WebElement txtServerUrl = firefoxDriver.findElement(By.id(ID_TXT_SERVER_URL));
		return txtServerUrl.getAttribute("value");
	}
	
	public static String getValueOfLabelBaseOntology() {
		WebElement lblBaseOntology = firefoxDriver.findElement(By.id(ID_LBL_BASE_ONTOLOGY));
		return lblBaseOntology.getAttribute("value");
	}
	
	public static String getValueOfLabelRuleOntology() {
		WebElement lblRuleOntology = firefoxDriver.findElement(By.id(ID_LBL_RULE_ONTOLOGY));
		return lblRuleOntology.getAttribute("value");
	}
	
	public static String getValueOfTxtMediatorUrl() {
		WebElement txtMediatorUrl = firefoxDriver.findElement(By.id(ID_TXT_MEDIATOR_URL));
		return txtMediatorUrl.getAttribute("value");
	}
	
	public static void setValueOfTxtResultDir(String newValue) {
		WebElement txtResultDir = firefoxDriver.findElement(By.id(ID_TXT_RESULT_DIR));
		txtResultDir.clear();
		txtResultDir.sendKeys(newValue);
	}
	
	public static void setValueOfTxtServerUrl(String newValue) {
		WebElement txtServerUrl = firefoxDriver.findElement(By.id(ID_TXT_SERVER_URL));
		txtServerUrl.clear();
		txtServerUrl.sendKeys(newValue);
	}
	
	public static void setValueOfTxtMediatorUrl(String newValue) {
		WebElement txtMediatorUrl = firefoxDriver.findElement(By.id(ID_TXT_MEDIATOR_URL));
		txtMediatorUrl.clear();
		txtMediatorUrl.sendKeys(newValue);
	}
	
	public static void setValueOfTxtWorkingDirectory(String newValue) {
		WebElement txtMediatorUrl = firefoxDriver.findElement(By.id(ID_TXT_WORKING_DIRECTORY_PATH));
		txtMediatorUrl.clear();
		txtMediatorUrl.sendKeys(newValue);
	}
	
	public static void hitBtnSaveClientConfiguration() {
		WebElement btnSaveClientConfiguration = firefoxDriver.findElement(By.id(ID_BTN_SAVE_CLIENT_CONFIGURATION )); 
		btnSaveClientConfiguration.submit();
	}
	
	public static void hitBtnSaveServerConfiguration() {
		WebElement btnSaveServerConfiguration = firefoxDriver.findElement(By.id(ID_BTN_SAVE_SERVER_CONFIGURATION )); 
		btnSaveServerConfiguration.submit();
	}
	
	public static void hitBtnBackClient() {
		WebElement btnBackClient = firefoxDriver.findElement(By.id(ID_BTN_BACK_CLIENT)); 
		btnBackClient.submit();
	}
	
	public static void hitTabConfigurationOfServer() {
		firefoxDriver.findElement(By.xpath("//a[@href='#tabViewMain:tabConfigurationServer']")).click();
	}
	
	public static void hitTabConfigurationOfClient() {
		firefoxDriver.findElement(By.xpath("//a[@href='#tabViewMain:tabConfigurationClient']")).click();
	}
	
	public static void hitTabConfigurationOfMediator() {
		firefoxDriver.findElement(By.xpath("//a[@href='#tabViewMain:tabConfigurationMediator']")).click();
	}
	
	public static boolean isDataSourcePanelComplete() {
		int dataSourceCount = 2;
		int toolsPerDataSourceCount = 5;
		
		for (int i = 1; i <= dataSourceCount; i++) {
			String idOfPanel = ID_PANEL_DATASOURCES + ":pnlDataSource" + i;
			WebElement pnlDataSources = firefoxDriver.findElement(By.id(ID_PANEL_DATASOURCES));
			if (pnlDataSources == null) {
				return false;
			}
			
			for (int j = i * toolsPerDataSourceCount + 1 ; j <= toolsPerDataSourceCount; j++) {
				String idOfLabel = idOfPanel + ":lblTool" + j;
				WebElement labelTool = firefoxDriver.findElement(By.id(idOfLabel));
				if (labelTool == null) {
					return false;
				}
				String idOfCheckbox = idOfPanel = ":btnTool" + j;
				WebElement checkBoxTool = firefoxDriver.findElement(By.id(idOfCheckbox));
				
				if (checkBoxTool == null) {
					return false;
				}
			}
		}
		return true;
	}
}
