package de.feu.kdmp4.packagingtoolkit.client.dynamiccomponents.primefaces;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.tree.Tree;
import org.primefaces.model.TreeNode;

import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.testapi.TaxonomyTestApi;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.TaxonomyElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.TaxonomyElementPrimeFacesImpl;


/**
 * Tests the element that represents a taxonomy in the graphical user interface.
 * @author Christopher Olbertz
 *
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PrimeFacesComponentFactory.class})
public class TaxonomyElementTest {
	  @Mock
	  private PrimeFacesComponentFactory primeFacesComponentFactory;
	  @Mock
	  private InformationPackageManager informationPackageManager;
	  private Taxonomy taxonomy;

	  @Before
	  public void setUp() throws Exception {
	    PowerMockito.mockStatic(PrimeFacesComponentFactory.class);
	    final Tree tree = new Tree();
	    final OutputLabel outputLabel = new OutputLabel();
	    final HtmlPanelGroup panelGroup = new HtmlPanelGroup();
	    final InputText inputText = new InputText();
	    final HtmlInputHidden inputHidden = new HtmlInputHidden();
	    
	    when(PrimeFacesComponentFactory.createTree()).thenReturn(tree);
	    when(PrimeFacesComponentFactory.createOutputLabel()).thenReturn(outputLabel);
	    when(PrimeFacesComponentFactory.createPanelGroup()).thenReturn(panelGroup);
	    when(PrimeFacesComponentFactory.createInputText()).thenReturn(inputText);
	    when(PrimeFacesComponentFactory.createInputHidden()).thenReturn(inputHidden);
	  }
	  
	@Test
	public void testCreateTaxonomyElement() {
		taxonomy = TaxonomyTestApi.createTaxonomy_withIndividuals();
		final TaxonomyElement taxonomyElement = new TaxonomyElementPrimeFacesImpl(taxonomy, informationPackageManager, new ServerCache());
		final HtmlPanelGroup panelGroup = (HtmlPanelGroup)taxonomyElement.getGuiElement();
		checkOutputLabel(panelGroup, taxonomy);
		checkHiddenText(panelGroup, taxonomy);
		checkTaxonomyTree(panelGroup, taxonomy);
	}
	
	/**
	 * Checks if the label is correct. 
	 * @param panelGroup Should contains the label as first child.
	 * @param taxonomy The title of this taxonomy should be the text of the label.
	 */
	private void checkOutputLabel(final HtmlPanelGroup panelGroup, final Taxonomy taxonomy) {
		final OutputLabel outputLabel = (OutputLabel)panelGroup.getChildren().get(0);
		final String labelText = outputLabel.getValue().toString();
		assertThat(labelText, is(equalTo(taxonomy.getTitle())));
	}
	
	private void checkTaxonomyTree(final HtmlPanelGroup panelGroup, final Taxonomy taxonomy) {
		final Tree tree = (Tree)panelGroup.getChildren().get(1);
		final TreeNode rootNode = tree.getValue();
		
		final TaxonomyIndividual rootIndividual = (TaxonomyIndividual)rootNode.getData();
		//assertThat(rootIndividual, is(equalTo(taxonomy.getRootIndividual())));
		checkTaxonomyTree(rootNode, rootIndividual);
	}
	
	private void checkTaxonomyTree(final TreeNode treeNode, final TaxonomyIndividual taxonomyIndividual) {
		int counter = 0;
		assertThat(taxonomyIndividual.getNarrowerCount(), is(equalTo(treeNode.getChildCount())));
		assertThat(taxonomyIndividual, is(equalTo(treeNode.getData())));
		
		while(taxonomyIndividual.hasNextNarrower()) {
			final TaxonomyIndividual narrowerIndividual = taxonomyIndividual.getNextNarrower().get();
			final TreeNode narrowerNode = treeNode.getChildren().get(counter);
			checkTaxonomyTree(narrowerNode, narrowerIndividual);
			//assertThat(narrowerIndividual, is(equalTo(narrowerNode.getData())));
			counter++;
		}
	}
	
	/**
	 * Checks if the id is correct. 
	 * @param panelGroup Should contains the hidden input.
	 * @param taxonomy The title of this taxonomy should be the text of the hidden input.
	 */
	private void checkHiddenText(final HtmlPanelGroup panelGroup, final Taxonomy taxonomy) {
		final HtmlInputHidden inputHidden = (HtmlInputHidden)panelGroup.getChildren().get(2);
		final String idText = inputHidden.getValue().toString();
		assertThat(idText, is(equalTo(taxonomy.getTitle())));
	}
}
