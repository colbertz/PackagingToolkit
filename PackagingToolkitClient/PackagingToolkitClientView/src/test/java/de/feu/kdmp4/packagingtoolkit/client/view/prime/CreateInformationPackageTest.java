package de.feu.kdmp4.packagingtoolkit.client.view.prime;

import static org.junit.Assert.*;

import java.io.File;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.is;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.testhelper.CreateInformationPackageTestHelper.*;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class CreateInformationPackageTest {
	private static final String BASE_ONTOLOGY = "KDMP4-OAIS.owl";
	private static final String SIP = "SIP";
	private static final String NEW_REFERENCE = "http://newReference.de";
	private static final String NEW_TITLE = "new information package";
	
	/**
	 * Tests if the intialization of the page was processed correctly. The following conditions must be achieved:
	 * <ul>
	 * 	<li>The button for the next step must be disabled.</li>
	 * 	<li>The package type SIP must be visible in the output label.</li>
	 * 	<li>The components for uploading a digital object must be disabled.</li>
	 * 	<li>The components for entering a reference must be disabled.</li>
	 * 	<li>The text box for entering a title for the information package must be enabled.</li>
	 * 	<li>The cancel button must be enabled.</li>
	 * 	<li>The label for the base ontology must contain the name of the base ontology.</li>
	 * 	<li>The text field for the title must be empty.</li>
	 * 	<li>The text field for the new reference must be empty.</li>
	 * </ul>
	 */
	@Test
	public void testInitialization() {
		waitUntilBtnNextStepVisible();
		assertFalse(isBtnNextStepEnabled());
		String packageType = getTextOfLblPackageType();
		assertEquals(SIP, packageType);
		waitUntilDigitalObjectUploadIsVisible();
		assertFalse(isDigitalObjectUploadEnabled());
		waitUntilTxtNewReferenceVisible();
		assertFalse(isTxtNewReferenceEnabled());
		waitUntilBtnAddReferenceVisible();
		assertFalse(isBtnAddReferenceEnabled());
		assertTrue(isBtnCancelEnabled());
		String baseOntology = getTextOfLblBaseOntology();
		assertEquals(baseOntology, BASE_ONTOLOGY);
		assertEquals("", getValueOfTxtInformationPackageTitle());
		assertEquals("", getValueOfTxtNewReference());
	}

	/**
	 * Tests if the components are correctly enabled if a title is been entered. The following conditions must
	 * be achieved:
	 * <ul>
	 * 	<li>The components for uploading a digital object must be enabled.</li>
	 * 	<li>The components for entering a reference must be enabled.</li>
	 * 	<li>The button for the next step must be enabled.</li>
	 * </ul>
	 */
	@Test
	public void testEnterTitle() {
		setValueOfTxtInformationPackageTitle(NEW_TITLE);
		//waitUntilDigitalObjectUploadIsVisible();
		//assertTrue(isDigitalObjectUploadEnabled());
		waitUntilBtnAddReferenceVisible();
		assertTrue(isBtnAddReferenceEnabled());
		waitUntilTxtNewReferenceVisible();
		assertTrue(isTxtNewReferenceEnabled());
		waitUntilBtnNextStepVisible();
		assertTrue(isBtnNextStepEnabled());
	}
	
	/**
	 * Tests if the components are correctly enabled if the text field with the title is been cleared. The following conditions 
	 * must be achieved:
	 * <ul>
	 * 	<li>The components for uploading a digital object must be disabled.</li>
	 * 	<li>The components for entering a reference must be disabled.</li>
	 * 	<li>The button for the next step must be disabled.</li>
	 * </ul>
	 */
	@Test
	public void testClearTitle() {
		setValueOfTxtInformationPackageTitle(NEW_TITLE);
		setValueOfTxtInformationPackageTitle(StringUtils.EMPTY_STRING);
		waitUntilDigitalObjectUploadIsVisible();
		assertFalse(isDigitalObjectUploadEnabled());
		waitUntilBtnAddReferenceVisible();
		assertFalse(isBtnAddReferenceEnabled());
		waitUntilTxtNewReferenceVisible();
		assertFalse(isTxtNewReferenceEnabled());
		waitUntilBtnNextStepVisible();
		assertFalse(isBtnNextStepEnabled());
	}
	
	/**
	 * Tests if the components are correctly enabled if the text field with the title is been cleared. The following conditions
	 * must be achieved:
	 * <ul>
	 * 	<li>The components for uploading a digital object must be disabled.</li>
	 * 	<li>The components for entering a reference must be disabled.</li>
	 * 	<li>The table with the reference is displayed.</li>
	 * </ul>
	 * First the test enters a title. Then it enters a new reference and presses the button. 
	 */
	@Test
	public void testAddNewReference() {
		setValueOfTxtInformationPackageTitle(NEW_TITLE);
		waitUntilTxtNewReferenceVisible();
		setValueOfTxtNewReference(NEW_REFERENCE);
		waitUntilBtnAddReferenceVisible();
		hitAddNewReferenceButton();
		waitUntilDigitalObjectUploadIsVisible();
		assertFalse(isDigitalObjectUploadEnabled());
		waitUntilBtnAddReferenceVisible();
		assertFalse(isBtnAddReferenceEnabled());
		waitUntilTxtNewReferenceVisible();
		assertFalse(isTxtNewReferenceEnabled());
		waitUntilTableDigitalObjectsVisible();
		assertFalse(isTableDigitalObjectsVisible());
		waitUntilTableReferencesVisible();
		assertTrue(isTableReferencesVisible());
	}
	
	/*@Test
	public void testUploadFile() {
		setValueOfTxtInformationPackageTitle(NEW_TITLE);
		File uploadFile = new File("/serverConfig.properties");
		waitUntilDigitalObjectUploadIsVisible();
		uploadDigitalObject(uploadFile);
		setValueOfTxtInformationPackageTitle(NEW_TITLE);
	}*/
}
