package de.feu.kdmp4.packagingtoolkit.client.testapi;

import de.feu.kdmp4.packagingtoolkit.client.model.classes.IriImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.LocalNameImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.NamespaceImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.TaxonomyIndividualImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.LocalName;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Namespace;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class TaxonomyTestApi {
	/**
	 * Creates a taxonomy with one root individual.
	 * @return The created taxonomy.
	 */
	public static Taxonomy createTaxonomy_withIndividuals() {
		final String title = new Uuid().toString();
		final Namespace namespace = new NamespaceImpl("http://mynamespace");
		final TaxonomyIndividual rootIndividual = createTaxonomyIndividual();
		final TaxonomyIndividual individual1 = createTaxonomyIndividualTree();
		final TaxonomyIndividual individual2 = createTaxonomyIndividualTree();
		rootIndividual.addNarrower(individual1);
		rootIndividual.addNarrower(individual2);

		final Taxonomy taxonomy = new TaxonomyImpl(rootIndividual, title, namespace);
		
		return taxonomy;
	}
	
	/**
	 * Creates an individual with a random uuid as localname.
	 * @return The individual.
	 */
	public static TaxonomyIndividual createTaxonomyIndividual() {
		final Uuid uuid = new Uuid();
		final Namespace namespace = new NamespaceImpl("http://www.mynamespace.de");
		final LocalName localName = new LocalNameImpl(uuid.toString());
		final Iri iriOfIndividual = new IriImpl(namespace, localName);
		final String title = uuid.toString();
		final TaxonomyIndividual taxonomyIndividual = new TaxonomyIndividualImpl(iriOfIndividual, title);
		return taxonomyIndividual;
	}
	
	/**
	 * Creates a little taxonomy tree with TaxonomyIndividual objects.
	 * @return The root individual of the tree.
	 */
	public static TaxonomyIndividual createTaxonomyIndividualTree() {
		final TaxonomyIndividual taxonomyIndividual1 = createTaxonomyIndividual();
		final TaxonomyIndividual taxonomyIndividual2 = createTaxonomyIndividual();
		addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual1);
		addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual2);
		final TaxonomyIndividual taxonomyIndividual11 = taxonomyIndividual1.getNextNarrower().get();
		taxonomyIndividual1.resetIerator();
		addThreeNarrowersToTaxonomyIndividual(taxonomyIndividual11);
		
		final TaxonomyIndividual taxonomyIndividual0 = createTaxonomyIndividual();
		taxonomyIndividual0.addNarrower(taxonomyIndividual1);
		taxonomyIndividual0.addNarrower(taxonomyIndividual2);
		return taxonomyIndividual0;
	}
	
	/**
	 * Adds three individuals as narrowers to the individual.
	 * @param taxonomyIndividual The individual we want to add three narrowers to.
	 * @return The second of the three narrowers.
	 */
	public static TaxonomyIndividual addThreeNarrowersToTaxonomyIndividual(final TaxonomyIndividual taxonomyIndividual ) {
		final TaxonomyIndividual narrower1 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower1);
		
		final TaxonomyIndividual narrower2 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower2);
		
		final TaxonomyIndividual narrower3 = TaxonomyTestApi.createTaxonomyIndividual();
		taxonomyIndividual.addNarrower(narrower3);
		
		return narrower2;
	}
}
