package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;

/**
 * Contains some constants related to web applications.
 * @author Christopher Olbertz
 *
 */
public class WebConstants {
	public static final String PROTOCOL_HTTP = "http://";
	public static final String PROTOCOL_HTTPS = "https://";
}
