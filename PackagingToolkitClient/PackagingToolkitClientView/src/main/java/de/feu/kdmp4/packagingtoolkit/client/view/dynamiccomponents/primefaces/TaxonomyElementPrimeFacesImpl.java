package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import java.util.Optional;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.tree.Tree;
import org.primefaces.model.DefaultTreeNode;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.TaxonomyElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class TaxonomyElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements TaxonomyElement {
	private static final String WIDGET_VAR_TAXONOMY = "dlgTaxonomy";
	/**
	 * The taxonomy that is represented by this gui element.
	 */
	private Taxonomy taxonomy;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	/**
	 * Contains all individuals in the taxonomy.
	 */
	private Tree taxonomyTree;
	/**
	 * Contains the information about the information package that is currently created.
	 */
	private InformationPackageManager informationPackageManager;
	
	private ServerCache serverCache;
	
	private TaxonomyIndividualCollection selectedTaxonomyIndividuals;
	
	public TaxonomyElementPrimeFacesImpl(final Taxonomy taxonomy, final InformationPackageManager informationPackageManager,
			final ServerCache serverCache) {
		super(taxonomy.getTitle(), taxonomy.getTitle()); 
		this.serverCache = serverCache;
		taxonomyTree = PrimeFacesComponentFactory.createTree();
		this.informationPackageManager = informationPackageManager;
		this.taxonomy = taxonomy;
		initializeTaxonomyTree();
		initializePanelGroup();
		initializeSelectedTaxonomyIndividuals();
	}
	
	private void initializeSelectedTaxonomyIndividuals() {
		selectedTaxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		initializeSelectedTaxonomyNodes();
		
	}
	
	private void initializeSelectedTaxonomyNodes() {
		if (informationPackageManager.isEditMode()) {
			final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
			final IriCollection irisOfselectedTaxonomyIndividuals = serverCache.getServiceFacade().
					findAllAssignedTaxonomyIndividuals(uuidOfInformationPackage);
			final TaxonomyIndividualCollection taxonomyIndividuals = determineTaxonomyIndividualsFromIris(
					irisOfselectedTaxonomyIndividuals);
			selectedTaxonomyIndividuals.addTaxonomyIndividuals(taxonomyIndividuals);
			informationPackageManager.setCurrentTaxonomy(taxonomy);
			informationPackageManager.addSelectIndividualsOfCurrentTaxonomy(selectedTaxonomyIndividuals);
		}
	}
	
	private TaxonomyIndividualCollection determineTaxonomyIndividualsFromIris(final IriCollection iris) {
		final TaxonomyIndividualCollection taxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		
		for (int i = 0; i < iris.getIriCount(); i++) {
			final Iri iri = iris.getIriAt(i);
			final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = serverCache.findTaxonomyIndividualByIri(iri);
			optionalWithTaxonomyIndividual.ifPresent(taxonomyIndividual -> 
						selectedTaxonomyIndividuals.addTaxonomyIndividual(taxonomyIndividual));
		}
		
		return taxonomyIndividuals;
	}
	
	/*private void selectTaxonomyIndividuals(final TaxonomyIndividualCollection taxonomyIndividuals) {
		for (int i = 0; i < taxonomyIndividuals.getSize(); i++) {
			final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = taxonomyIndividuals.getTaxonomyIndividual(i);
			optionalWithTaxonomyIndividual.ifPresent(taxonomyIndividual -> {
				final TreeNode treeNode = new DefaultTreeNode(taxonomyIndividual);
				sele
			});
			
		}
	}*/
	
	/**
	 * Initializes the tree in the graphical user interface. 
	 */
	private void initializeTaxonomyTree() {
		final TaxonomyIndividual rootIndividual = taxonomy.getRootIndividual();
		final DefaultTreeNode rootNode = new DefaultTreeNode(null, null);//  (rootIndividual, null);
		rootNode.setData(rootIndividual);

		while (rootIndividual.hasNextNarrower()) {
			final Optional<TaxonomyIndividual> optionalWithNarrower = rootIndividual.getNextNarrower();
			if (optionalWithNarrower.isPresent()) {
				final TaxonomyIndividual narrower = optionalWithNarrower.get();
				addNarrowerToNode(narrower, rootNode);
			}
		}
		
		final String idTree = UniqueIdentifierUtil.createTreeIdentifier(taxonomy.getTitle());
		taxonomyTree.setValue(rootNode);
		taxonomyTree.setId(idTree);
	}
	
	/**
	 * Adds a narrower of an individual to the taxonomy tree.
	 * @param narrowerIndividual The individual that should be inserted as child of treeNode.
	 * @param broaderNode The node that contains the individual narrowerIndividual is a narrower of. 
	 */
	private void addNarrowerToNode(final TaxonomyIndividual narrowerIndividual, final DefaultTreeNode broaderNode) {
		final DefaultTreeNode newTreeNode = new DefaultTreeNode(null, null);//  (rootIndividual, null);
		newTreeNode.setData(narrowerIndividual);
		broaderNode.getChildren().add(newTreeNode);
		//final TreeNode newTreeNode = new DefaultTreeNode("default", narrowerIndividual, broaderNode);
		//broaderNode.getChildren().add(newTreeNode);
		
		while (narrowerIndividual.hasNextNarrower()) {
			final Optional<TaxonomyIndividual> optionalWithNarrower = narrowerIndividual.getNextNarrower();
			if (optionalWithNarrower.isPresent()) {
				final TaxonomyIndividual narrower = optionalWithNarrower.get();
				addNarrowerToNode(narrower, newTreeNode);
			}
		}
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		final CommandButton btnOpenTaxonomyDialog = PrimeFacesComponentFactory.createCommandButton();
		btnOpenTaxonomyDialog.setValue("Taxonomie oeffnen!");
		btnOpenTaxonomyDialog.addActionListener(new ActionListener() {
			
			@Override
			public void processAction(ActionEvent event) throws AbortProcessingException {
				PrimeFacesUtils.showDialog(WIDGET_VAR_TAXONOMY);
				informationPackageManager.setCurrentTaxonomy(taxonomy);
			}
		});
		btnOpenTaxonomyDialog.setUpdate("treeTaxonomy");
		panelGroup.getChildren().add(btnOpenTaxonomyDialog);
		//panelGroup.getChildren().add(getOutputLabel());
		//panelGroup.getChildren().add(taxonomyTree);
		//panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	@Override
	public Object getGuiElement() {
		return panelGroup;
	}

	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}

	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
