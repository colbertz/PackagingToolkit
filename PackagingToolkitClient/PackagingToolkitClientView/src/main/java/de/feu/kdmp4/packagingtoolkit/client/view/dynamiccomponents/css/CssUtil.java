package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.css;

/**
 * A helper class for creating css styles. If you use this class it is not necessary to litter the css
 * style in the whole application. This class contains conventient private methods create the css
 * styles and public methods for creating the styles for the components which need css styles. 
 * So the styles can be adjusted in one place and you cannot have any typos in them. 
 * @author Christopher Olbertz
 *
 */
public class CssUtil {
	/**
	 * A constant for the css property bold.
	 */
	private static final String BOLD = "bold";
	/**
	 * A constant for the css property font-size.
	 */
	private static final String FONT_SIZE = "font-size";
	/**
	 * A constant for the css property font-weight.
	 */
	private static final String FONT_WEIGHT = "font-weight";
	
	private static final String FLOAT_RIGHT = "float:right";
	
	/**
	 * Creates a css property with the font size. 
	 * @param cssFontSize The font size that should be used.
	 * @return A css property with the font size.
	 */
	private static final String createFontSize(final CssFontSizes cssFontSize) {
		return FONT_SIZE + ":" + cssFontSize.getCssName() + ";";
	}
	
	/**
	 * Creates a css property for a bold text.
	 * @return A css property for a bold text.
	 */
	private static final String createBoldFont() {
		return FONT_WEIGHT + ":" + BOLD + ";";
	}
	
	/**
	 * Creates a style that can be used to create a status label in the windows for entering
	 * the properties. The status label contains the information which individual the 
	 * user is editing.
	 * @return A style for the status label. 
	 */
	public static String createStyleForStatusLabel() {
		String style = createFontSize(CssFontSizes.X_LARGE) + createBoldFont();
		return style;
	}
	
	/**
	 * Creates a CSS style that a text floats right.
	 * @return The created style.
	 */
	public static String createStyleFloatRight() {
		return FLOAT_RIGHT;
	}
}
