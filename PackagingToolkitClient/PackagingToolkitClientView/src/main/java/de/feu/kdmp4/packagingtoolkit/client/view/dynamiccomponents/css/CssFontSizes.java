package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.css;

/**
 * This enum contains the font sizes of CSS. Is used for avoiding typos.
 * @author christopher
 *
 */
public enum CssFontSizes {
	XX_SMALL("xx-small"), X_SMALL("x-small"), SMALL("small"), SMALLER("smaller"), MEDIUM("medium"), LARGE("large"), 
	X_LARGE("x-large"), XX_LARGE("xx-large"), LARGER("larger");
	
	/**
	 * Contains the name of the font size in CSS.
	 */
	private String cssName;
	
	/**
	 * Constructs an enum object. 
	 * @param cssName The name of the css font. 
	 */
	private CssFontSizes(final String cssName) {
		this.cssName = cssName;
	}
	
	/**
	 * Determines the css name of the font size. 
	 * @return The css name of the font size.
	 */
	public String getCssName() {
		return cssName;
	}
}
