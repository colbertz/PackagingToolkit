package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;

import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Creates some unique identifiers for the components for the GUI. The components are created by
 * using a prefix for the component and an increasing number. 
 * @author Christopher Olbertz
 *
 */
public class UniqueIdentifierUtil {
	/**
	 * The counter for creating an unique identifier.
	 */
	private static int counter;
	/**
	 * This prefix for button components.
	 */
	private static final String PREFIX_BUTTON = "btn";
	/**
	 * This prefix for calendar components.
	 */
	private static final String PREFIX_CALENDAR = "cld";
	/**
	 * This prefix for checkboxes.
	 */
	private static final String PREFIX_CHECKBOX = "chk";
	/**
	 * This prefix for data lists.
	 */
	private static final String PREFIX_DATA_LIST = "lst";
	/**
	 * This prefix for tables.
	 */
	private static final String PREFIX_DATA_TABLE = "tbl";
	/**
	 * This prefix for button dialogs.
	 */
	private static final String PREFIX_DIALOG = "dlg";
	/**
	 * This prefix for file upload components.
	 */
	private static final String PREFIX_FILEUPLOAD = "fileUpload";
	/**
	 * This prefix for the growl components of PrimeFaces.
	 */
	private static final String PREFIX_GROWL = "growl";
	/**
	 * This prefix for number input components.
	 */
	private static final String PREFIX_INPUT_NUMBER = "inNmb";
	/**
	 * This prefix for labels.
	 */
	private static final String PREFIX_LABEL = "lbl";
	/**
	 * This prefix for list boxes.
	 */
	private static final String PREFIX_LISTBOX = "lb";
	/**
	 * This prefix for list panels.
	 */
	private static final String PREFIX_PANEL = "pnl";
	/**
	 * This prefix for radio buttons.
	 */
	private static final String PREFIX_RADIO = "rd";
	/**
	 * This prefix for sliders.
	 */
	private static final String PREFIX_SLIDER = "sld";
	/**
	 * This prefix for spinner components.
	 */
	private static final String PREFIX_SPINNER = "spn";
	/**
	 * This prefix for text input components.
	 */
	private static final String PREFIX_TEXT_INPUT = "txt";
	/**
	 * This prefix for tree components.
	 */
	private static final String PREFIX_TREE = "tree";
	
	/**
	 * Creates an identifier for a button.
	 * @param buttonName The name for the button that is used to create an unique identifier.
	 * @return The unique identifier for the button.
	 */
	public static String createButtonIdentifier(final String buttonName) {
		final String buttonLabel = StringUtils.deleteSpaces(buttonName);
		return String.valueOf(PREFIX_BUTTON + getIdentifier(buttonLabel));
	}

	/**
	 * Creates an identifier for a calendar.
	 * @param calendarName The name for the calendar that is used to create an unique identifier.
	 * @return The unique identifier for the calendar.
	 */
	public static String createCalendarIdentifier(final String calendarName) {
		final String calendarLabel = StringUtils.deleteSpaces(calendarName);
		return String.valueOf(PREFIX_CALENDAR + getIdentifier(calendarLabel));
	}
	
	/**
	 * Creates an identifier for a checkbox.
	 * @param checkboxName The name for the checkbox that is used to create an unique identifier.
	 * @return The unique identifier for the checkbox.
	 */
	public static String createCheckboxIdentifier(final String checkboxName) {
		final String checkboxLabel = StringUtils.deleteSpaces(checkboxName);
		return String.valueOf(PREFIX_CHECKBOX + getIdentifier(checkboxLabel));
	}

	/**
	 * Creates an identifier for a dialog.
	 * @param dialogName The name for the dialog that is used to create an unique identifier.
	 * @return The unique identifier for the dialog.
	 */
	public static String createDialogIdentifier(final String dialogName) {
		final String dialogLabel = StringUtils.deleteSpaces(dialogName);
		return String.valueOf(PREFIX_DIALOG + getIdentifier(dialogLabel));
	}	

	/**
	 * Creates an identifier for a file upload.
	 * @param fileUploadName The name for the file upload that is used to create an unique identifier.
	 * @return The unique identifier for the file upload.
	 */
	public static String createFileUploadIdentifier(final String fileUploadName) {
		final String fileUploadLabel = StringUtils.deleteSpaces(fileUploadName);
		return String.valueOf(PREFIX_FILEUPLOAD + getIdentifier(fileUploadLabel));
	}	
	
	/**
	 * Creates an identifier for a growl.
	 * @param growlName The name for the growl that is used to create an unique identifier.
	 * @return The unique identifier for the growl.
	 */
	public static String createGrowlIdentifier(final String growlName) {
		final String growlLabel = StringUtils.deleteSpaces(growlName);
		return String.valueOf(PREFIX_GROWL + getIdentifier(growlLabel));
	}	

	/**
	 * Creates an identifier for a number input.
	 * @param inputNumberName The name for the number input that is used to create an unique identifier.
	 * @return The unique identifier for the number input.
	 */
	public static String createInputNumberIdentifier(final String inputNumberName) {
		final String inputNumberLabel = StringUtils.deleteSpaces(inputNumberName);
		return String.valueOf(PREFIX_INPUT_NUMBER + getIdentifier(inputNumberLabel));
	}
	
	/**
	 * Creates an identifier for a data list.
	 * @param dataListName The name for the data list that is used to create an unique identifier.
	 * @return The unique identifier for the data list.
	 */
	public static String createDataListIdentifier(final String dataListName) {
		final String dataListLabel = StringUtils.deleteSpaces(dataListName);
		return String.valueOf(PREFIX_DATA_LIST + getIdentifier(dataListLabel));
	}
	
	/**
	 * Creates an identifier for a text field.
	 * @param inputTextName The name for the textFieldthat is used to create an unique identifier.
	 * @return The unique identifier for the text field.
	 */
	public static String createInputTextIdentifier(final String inputTextName) {
		final String inputTextLabel = StringUtils.deleteSpaces(inputTextName);
		return String.valueOf(PREFIX_TEXT_INPUT + getIdentifier(inputTextLabel));
	}
	
	/**
	 * Creates an identifier for a label.
	 * @param labelName The name for the label that is used to create an unique identifier.
	 * @return The unique identifier for the label.
	 */
	public static String createLabelIdentifier(final String labelName) {
		final String label = StringUtils.deleteSpaces(labelName);
		return String.valueOf(PREFIX_LABEL + getIdentifier(label));
	}

	/**
	 * Creates an identifier for a listbox.
	 * @param listboxName The name for the listbox that is used to create an unique identifier.
	 * @return The unique identifier for the listbox.
	 */
	public static String createListboxIdentifier(final String listboxName) {
		final String listboxLabel = StringUtils.deleteSpaces(listboxName);
		return String.valueOf(PREFIX_LISTBOX + getIdentifier(listboxLabel));
	}
	
	/**
	 * Creates an identifier for a panel.
	 * @param buttonName The name for the panel that is used to create an unique identifier.
	 * @return The unique identifier for the panel.
	 */
	public static String createPanelIdentifier(final String panelName) {
		final String panelLabel = StringUtils.deleteSpaces(panelName);
		return String.valueOf(PREFIX_PANEL + getIdentifier(panelLabel));
	}
	
	/**
	 * Creates an identifier for a radio button.
	 * @param radioName The name for the radio button that is used to create an unique identifier.
	 * @return The unique identifier for the radio button.
	 */
	public static String createRadioIdentifier(final String radioName) {
		final String radioLabel = StringUtils.deleteSpaces(radioName);
		return String.valueOf(PREFIX_RADIO + getIdentifier(radioLabel));
	}
	
	/**
	 * Creates an identifier for a slider.
	 * @param sliderName The name for the slider that is used to create an unique identifier.
	 * @return The unique identifier for the slider.
	 */
	public static String createSliderIdentifier(final String sliderName) {
		final String sliderLabel = StringUtils.deleteSpaces(sliderName);
		return String.valueOf(PREFIX_SLIDER + getIdentifier(sliderLabel));
	}
	
	/**
	 * Creates an identifier for a spinner.
	 * @param spinnerName The name for the spinner that is used to create an unique identifier.
	 * @return The unique identifier for the spinner.
	 */
	public static String createSpinnerIdentifier(final String spinnerName) {
		final String spinnerLabel = StringUtils.deleteSpaces(spinnerName);
		return String.valueOf(PREFIX_SPINNER + getIdentifier(spinnerLabel));
	}

	/**
	 * Creates an identifier for a tree.
	 * @param treeName The name for the tree that is used to create an unique identifier.
	 * @return The unique identifier for the tree.
	 */
	public static String createTreeIdentifier(final String treeName) {
		String treeLabel = StringUtils.deleteTextInParentheses(treeName); 
		treeLabel =	StringUtils.deleteSpaces(treeLabel);
		return String.valueOf(PREFIX_TREE + getIdentifier(treeLabel));
	}
	
	/**
	 * Creates an identifier for a table.
	 * @param tableName The name for the table that is used to create an unique identifier.
	 * @return The unique identifier for the table.
	 */
	public static String createTableIdentifier(final String tableName) {
		final String tableLabel = StringUtils.deleteSpaces(tableName);
		return String.valueOf(PREFIX_DATA_TABLE + getIdentifier(tableLabel));
	}
	
	/**
	 * Creates an identifier by incrementing a counter and hanging the number at the end of the name 
	 * of the component.
	 * @param componentName The name for the component that is used to create an unique identifier.
	 * @return The unique identifier for the component.
	 */
	private static String getIdentifier(final String componentName) {
		counter++;
		return componentName + String.valueOf(counter);
	}
}
