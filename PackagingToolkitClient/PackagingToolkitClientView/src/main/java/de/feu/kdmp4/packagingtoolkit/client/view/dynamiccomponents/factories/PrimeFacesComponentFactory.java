package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories;

import javax.faces.component.UIOutput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.ajaxexceptionhandler.AjaxExceptionHandler;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.column.Column;
import org.primefaces.component.columntoggler.ColumnToggler;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datalist.DataList;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.dialog.Dialog;
import org.primefaces.component.fieldset.Fieldset;
import org.primefaces.component.fileupload.FileUpload;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.layout.Layout;
import org.primefaces.component.layout.LayoutUnit;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.picklist.PickList;
import org.primefaces.component.row.Row;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonelistbox.SelectOneListbox;
import org.primefaces.component.selectoneradio.SelectOneRadio;
import org.primefaces.component.slider.Slider;
import org.primefaces.component.tree.Tree;

import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;

/**
 * Contains some methods that create implementations of PrimeFaces components. It is not a good idea to call the 
 * constructors of the components directly. Therefore the code for creating them is encapsulated in this factory. 
 * @author Christopher Olbertz
 *
 */
public class PrimeFacesComponentFactory {
	/**
	 * Creates a component for an output that the user cannot use.
	 * @return The created component.
	 */
	public static UIOutput createUIOutput() {
		return (UIOutput)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(UIOutput.COMPONENT_TYPE);
	}
	
	/**
	 * Creates a component for a data list.
	 * @return The created component.
	 */
	public static DataList createDataList() {
		return (DataList)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(DataList.COMPONENT_TYPE);
	}

	/**
	 * Creates a component for a output text.
	 * @return The created component.
	 */
	public static HtmlOutputText createHtmlOutputText() {
		return (HtmlOutputText)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(HtmlOutputText.COMPONENT_TYPE);
	}
	
	/**
	 * Creates a component for a column component for a table.
	 * @return The created component.
	 */
	public static Column createColumn() {
		return (Column)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Column.COMPONENT_TYPE);
	}

	/**
	 * Creates a calendar component .
	 * @return The created component.
	 */
	public static Calendar createCalendar() {
		return (Calendar)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Calendar.COMPONENT_TYPE);
	}

	/**
	 * Creates a button component .
	 * @return The created component.
	 */
	public static CommandButton createCommandButton() {
		return (CommandButton)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(CommandButton.COMPONENT_TYPE);
	}

	/**
	 * Creates a column toggler component. This makes it possible for the user make columns in a table visible or invisible.
	 * @return The created component.
	 */
	public static ColumnToggler createColumnToggler() {
		return (ColumnToggler)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(ColumnToggler.COMPONENT_TYPE);
	}
	
	/**
	 * Creates an ajax exception component.
	 * @return The created component.
	 */
	public static AjaxExceptionHandler createAjaxExceptionHandler() {
		return (AjaxExceptionHandler)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(AjaxExceptionHandler.COMPONENT_TYPE);
	}
	
	/**
	 * Creates a table component.
	 * @return The created component.
	 */
	public static DataTable createDataTable() {
		return (DataTable)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(DataTable.COMPONENT_TYPE);
	}

	/**
	 * Creates a dialog component.
	 * @return The created component.
	 */
	public static Dialog createDialog() {
		return (Dialog)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Dialog.COMPONENT_TYPE);
	}

	/**
	 * Creates a file upload component.
	 * @return The created component.
	 */
	public static FileUpload createFileUpload() {
		return (FileUpload)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(FileUpload.COMPONENT_TYPE);
	}

	/**
	 * Creates a fieldset component. It is a container component for other components.
	 * @return The created component.
	 */
	public static Fieldset createFieldset() {
		return (Fieldset)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Fieldset.COMPONENT_TYPE);
	}

	/**
	 * Creates a panel grid component.
	 * @return The created component.
	 */
	public static HtmlPanelGrid createHtmlPanelGrid() {
		return (HtmlPanelGrid)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(HtmlPanelGrid.COMPONENT_TYPE);
	}
	
	/**
	 * Creates a html panel group component.
	 * @return The created component.
	 */
	public static HtmlPanelGroup createHtmlPanelGroup() {
		return (HtmlPanelGroup)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(HtmlPanelGroup.COMPONENT_TYPE);
	}
	
	/**
	 * Creates a hidden input field. The user cannot see it. 
	 * @return The created component.
	 */
	public static HtmlInputHidden createInputHidden() {
		return (HtmlInputHidden)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(HtmlInputHidden.COMPONENT_TYPE);
	}

	/**
	 * Creates a component where the user can enter values in.
	 * @return The created component.
	 */
	public static InputText createInputText() {
		return (InputText)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(InputText.COMPONENT_TYPE);
	}

	/**
	 * Creates a layout component. It contains several layout unit components.
	 * @return The created component.
	 */
	public static Layout createLayout() {
		return (Layout)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Layout.COMPONENT_TYPE);
	}

	/**
	 * Creates a layout unit component. These are contained in layout components.
	 * @return The created component.
	 */
	public static LayoutUnit createLayoutUnit() {
		return (LayoutUnit)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(LayoutUnit.COMPONENT_TYPE);
	}

	/**
	 * Creates a panel component.
	 * @return The created component.
	 */
	public static Panel createPanel() {
		return (Panel)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Panel.COMPONENT_TYPE);
	}

	/**
	 * Creates a panel group component.
	 * @return The created component.
	 */
	public static HtmlPanelGroup createPanelGroup() {
		return (HtmlPanelGroup)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(HtmlPanelGroup.COMPONENT_TYPE);
	}

	/**
	 * Creates a panel grid component.
	 * @return The created component.
	 */
	public static PanelGrid createPanelGrid() {
		return (PanelGrid)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(PanelGrid.COMPONENT_TYPE);
	}

	/**
	 * Creates a picklist component.
	 * @return The created component.
	 */
	public static PickList createPickList() {
		return (PickList)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(PickList.COMPONENT_TYPE);
	}

	/**
	 * Creates a boolean checkbox component.
	 * @return The created component.
	 */
	public static SelectBooleanCheckbox createSelectBooleanCheckbox() {
		return (SelectBooleanCheckbox)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(SelectBooleanCheckbox.COMPONENT_TYPE);
	}

	/**
	 * Creates a  component where the user can select one item.
	 * @return The created component.
	 */
	public static SelectOneListbox createSelectOneListbox() {
		return (SelectOneListbox)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(SelectOneListbox.COMPONENT_TYPE);
	}

	/**
	 * Creates a radio button component.
	 * @return The created component.
	 */
	public static SelectOneRadio createSelectOneRadio() {
		return (SelectOneRadio)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(SelectOneRadio.COMPONENT_TYPE);
	}

	/**
	 * Creates a slider component where the user can select an integer value.
	 * @return The created component.
	 */
	public static Slider createSlider() {
		return (Slider)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Slider.COMPONENT_TYPE);
	}

	/**
	 * Creates a tree component.
	 * @return The created component.
	 */
	public static Tree createTree() {
		return (Tree)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Tree.COMPONENT_TYPE);
	}	
	
	/**
	 * Creates a row component.
	 * @return The created component.
	 */
	public static Row createRow() {
		return (Row)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(Row.COMPONENT_TYPE);
	}

	/**
	 * Creates an output component.
	 * @return The created component.
	 */
	public static OutputLabel createOutputLabel() {
		return (OutputLabel)JsfUtil.getApplication(JsfUtil.getFacesContext()).createComponent(OutputLabel.COMPONENT_TYPE);
	}

	/**
	 * Creates a ui select component. These are contained in lists.
	 * @return The created component.
	 */
	public static UISelectItems createUISelectItems() {
		return new UISelectItems();
	}
}