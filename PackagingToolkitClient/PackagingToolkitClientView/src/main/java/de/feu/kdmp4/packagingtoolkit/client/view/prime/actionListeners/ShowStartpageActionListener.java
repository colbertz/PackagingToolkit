package de.feu.kdmp4.packagingtoolkit.client.view.prime.actionListeners;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

import de.feu.kdmp4.packagingtoolkit.client.enums.Views;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;

/**
 * This listener navigates to the startpage of the application.  This is used for example in the save button
 * on the page ontologyTreeView.xhtml because there is already an other action listener assigned to this
 * button and a second one has to be defined in its own class.
 * @author Christopher Olbertz
 *
 */
public class ShowStartpageActionListener implements ActionListener {

	@Override
	public void processAction(ActionEvent actionEvent) throws AbortProcessingException {
		Views currentView = Views.STARTPAGE;
		JsfUtil.setValueExpression("workspaceBean", "page", String.class, 
				currentView.getUrl());
	}

}
