package de.feu.kdmp4.packagingtoolkit.client.view.prime.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

//import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nMessagesUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.WebConstants;


/**
 * A simple validator for URLs. Checks if the url starts with http:// or
 * https://.
 * @author Christopher Olbertz
 */

@Component
@Scope("request")
public class FacesUrlValidator implements Validator {
	private int value;

	public FacesUrlValidator() {
	}
	
	public FacesUrlValidator(int value) {
		this.value = value;
	}
	
	/**
	 * Validates an url. 
	 */
	@Override
	public void validate(FacesContext facesContext, UIComponent component, 
			Object object) throws ValidatorException {
		final String url = (String)object;

		if (!isValidUrl(url)) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
					I18nMessagesUtil.getErrorString(), 
					I18nMessagesUtil.getInvalidUrlString())); 
		}
	}
	
	/**
	 * Checks if an url is a valid http or https url.
	 * @param url The url we want to check. 
	 * @return True, if the url is valid, false otherwise.  
	 */
	private boolean isValidUrl(final String url) {
		if (url.startsWith(WebConstants.PROTOCOL_HTTP)) {
			return true;
		} else if (url.startsWith(WebConstants.PROTOCOL_HTTPS))  {
			return true;
		} else {
			return false;
		}
	}
}
