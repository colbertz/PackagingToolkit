package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import java.util.Locale;

import javax.faces.component.html.HtmlInputHidden;

import org.primefaces.component.outputlabel.OutputLabel;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Contains the methods and atttributes all PrimeFaces components need. All classes that create
 * PrimeFaces components dynamically have to extend this class.
 * @author Christopher Olbertz
 *
 */
public abstract class AbstractPrimeFacesComponent {
	/**
	 * This element contains the description of the component for the user. 
	 */
	private OutputLabel outputLabel;
	/**
	 * Contains the iri of the property that is assigned to this component.
	 */
	private HtmlInputHidden txtPropertyId;
	
	/**
	 * Initializes the output label with the description and the hidden field with the
	 * iri of the property. 
	 * @param ontologyProperty The property that is assigned to this component.
	 */
	public AbstractPrimeFacesComponent(final OntologyProperty ontologyProperty) {
		initializeLabel(ontologyProperty);
		initializeInputHidden(ontologyProperty);
	}
	
	/**
	 * Initializes the output label with labelText and the hidden field with the
	 * hiddenText
	 * @param labelText The text that the label should contain.
	 * @param hiddenText The text that the hidden field should contain.
	 */
	public AbstractPrimeFacesComponent(final String labelText, final String hiddenText) {
		initializeLabel(labelText);
		initializeInputHidden(hiddenText);
	}
	
	/**
	 * Determines the label for the component for a property for the current language. If there is no
	 * label for the current language, the default label is used. If there is no default label, the iri of 
	 * the property is used as label.  
	 * @param ontologyProperty The property whose label we want to determine. 
	 */
	private String determineLabel(final OntologyProperty ontologyProperty) {
		final Locale locale = PrimeFacesUtils.getCurrentLocale();
		String labelText = ontologyProperty.findLabelByLanguage(locale);
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyProperty.getLabel();
		}
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyProperty.getPropertyId();
		}
		
		return labelText;
	}
	
	/**
	 * Initializes the hidden field with the iri of the property.
	 * @param ontologyProperty The property that contains the iri. 
	 */
	private void initializeInputHidden(final OntologyProperty ontologyProperty) {
		final String propertyId = ontologyProperty.getPropertyId();
		initializeInputHidden(propertyId);
	}
	
	/**
	 * Initializes the hidden field with a text. In the most cases this is the iri of the property.
	 * @param hiddenText The text that is contained in the hidden text field.
	 */
	private void initializeInputHidden(final String hiddenText) {
		txtPropertyId = PrimeFacesComponentFactory.createInputHidden();
		final String text = StringUtils.deleteTextInParentheses(hiddenText);
		txtPropertyId.setValue(text);
	}
	
	/**
	 * Initializes the label with the description for the user.   If there is no text for
	 * the label in the property, the iri is used as description. 
	 * @param ontologyProperty Contains the description string.
	 */
	private void initializeLabel(final OntologyProperty ontologyProperty) {
		final String labelText = determineLabel(ontologyProperty);
		initializeLabel(labelText);
	}
	
	/**
	 * Initializes the label with the description for the user.   
	 * @param labelText Contains the description string.
	 */
	private void initializeLabel(final String labelText) {
		outputLabel = PrimeFacesComponentFactory.createOutputLabel();
		final String text = StringUtils.deleteTextInParentheses(labelText);
		final String idLabel = UniqueIdentifierUtil.createLabelIdentifier(text);
		outputLabel.setId(idLabel);
		outputLabel.setValue(labelText);
	}
	
	/**
	 * Returns the label that contains the description for the user. 
	 * @return The label that contains the description for the user. 
	 */
	protected OutputLabel getOutputLabel() {
		return outputLabel;
	}
	
	/**
	 * Returns the hidden field that contains the iri of the property. 
	 * @return The hidden field that contains the iri of the property. 
	 */
	protected HtmlInputHidden getTxtPropertyIri() {
		return txtPropertyId;
	}
	
	/**
	 * Checks if the label of this component contains a given text.
	 * @param aString The text that we want to check. 
	 * @return True if the label contains aString, false otherwise. 
	 */
	public boolean labelContains(final String aString) {
		final String outputLabelValue = getOutputLabel().getValue().toString();
		return outputLabelValue.contains(aString);
	}
}	
