package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesConstants.ATTRIBUTE_VALUE;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesConstants.FACET_FOOTER;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesConstants.FACET_HEADER;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesConstants.ICON_CALCULATOR;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesConstants.TYPE_BUTTON;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

import org.primefaces.component.column.Column;
import org.primefaces.component.columntoggler.ColumnToggler;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nComponentLabelUtil;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.css.CssUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.IndividualGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRow;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRowTableModel;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * The element that contains the individuals for displaying in the gui. This implementation
 * displays a PrimeFaces data table. The columns are determined  by the individuals class. 
 * Every property of the class gets one column. Then the rows are determined by the individuals. For
 * every individual of the class there is a row created. The cells contain the property values of the
 * individuals.
 * @author Christopher Olbertz
 *
 */
public class IndividualGuiElementPrimeFacesImpl implements IndividualGuiElement {
	/**
	 * The title of the column with the uuid.
	 */
	private static final String TITLE_COLUMN_UUID = "Uuid";
	/**
	 * The expression value used for access the selected attribute of an individual.
	 */
	private static final String VALUE_EXPRESSION_SELECTED = "#{individual.selected}";
	/**
	 * The expression value used for access the uuid attribute of an individual.
	 */
	private static final String VALUE_EXPRESSION_UUID = "#{individual.uuidOfIndividual}";
	/**
	 * The name of the variable that represents the current individual in the table.
	 */
	private static final String VAR_INDIVIDUAL = "individual";
	/**
	 * The individuals that are shown in the table.
	 */
	private OntologyIndividualCollection individualList;
	/**
	 * The class the individuals in the table are assigned to.
	 */
	private OntologyClass ontologyClass;
	/**
	 * The dynamically created columns of the table. Every property of the individuals are represented by one
	 * column.
	 */
	private List<ColumnModel> columns;
	/**
	 * The data table that shows the individuals.
	 */
	private DataTable dataTable;
	/**
	 * The model of the table. It contains the data of the individuals.
	 */
	private DataRowTableModel dataModel;
	/**
	 * The panel that contains the table. 
	 */
	private HtmlPanelGroup panel;
	/**
	 * The rows of the table are created of the individuals. Every individual is represented
	 * by one row.
	 */
	private List<DataRow> dataRows;
	/**
	 * The label contains the label of the class whose individuals are shown in the table.
	 */
	private OutputLabel label;
	/**
	 * The individuals that are selected by the user.
	 */
	private List<DataRow> selectedDataRows;
	/**
	 * Contains the information that is the base for the creation of the information package. Here the
	 * individuals the user has selected are written into the manager. So the individuals can be known
	 * in the whole client application.
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * Contains the iri of the class whose individuals are shown in the table.
	 */
	private HtmlInputHidden txtPropertyId;
	/**
	 * A reference to an object that contains some important information about the gui. 
	 */
	private DynamicGuiManager dynamicGuiManager;
	/**
	 * The iri of the property that is represented by this input component.
	 */
	private String propertyIri;
	
	/**
	 * Creates a gui element with the given individual. Determines the ontology class for displaying the
	 * table from the first individual of the list. 
	 * @param individualList Contains the individuals that should be displayed.
	 * @param ontologyClass The class the individuals are assigned to.
	 * @param informationPackageManager Contains information about the information package that the user is
	 * creating.
	 * @param dynamicGuiManager Contains some important information about the user interface.
	 */
	public IndividualGuiElementPrimeFacesImpl(final OntologyIndividualCollection individualList, final OntologyClass ontologyClass, 
			final InformationPackageManager informationPackageManager, final DynamicGuiManager dynamicGuiManager,
			final String propertyIri) {
		panel = PrimeFacesComponentFactory.createPanelGroup();
		this.individualList = individualList;
		this.informationPackageManager = informationPackageManager;
		this.dynamicGuiManager = dynamicGuiManager;
		this.propertyIri = propertyIri;
		this.ontologyClass = ontologyClass;
		label = PrimeFacesComponentFactory.createOutputLabel();
		final String labelText = determineLabel(ontologyClass);
		label.setValue(labelText);
		columns = new ArrayList<>();
		configureDataTable();
		createDynamicColumns(ontologyClass);
		createDynamicData();
		selectedDataRows = new ArrayList<>();
		initializeInputHidden(propertyIri);
	}
	
	/**
	 * This constructor is only for be used by the subclass NullIndividualGuiElementPrimeFacesImpl.
	 */
	// DELETE_ME
	protected IndividualGuiElementPrimeFacesImpl() {
		
	}
	
	/**
	 * Determines the label for the component for a property for the current language. If there is no
	 * label for the current language, the default label is used. If there is no default label, the iri of 
	 * the property is used as label.  
	 * @param ontologyProperty The property whose label we want to determine. 
	 */
	private String determineLabel(final OntologyClass ontologyClass) {
		final Locale locale = PrimeFacesUtils.getCurrentLocale();
		String labelText = ontologyClass.findLabelByLanguage(locale);
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyClass.getLabel();
		}
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyClass.getFullName();
		}
		
		return labelText;
	}
	
	/**
	 * Configures the toggler. That is the component that make it possible to fade out the columns
	 * of the table. It consists of the toggler component and a button for choosing the columns.
	 */
	private void configureToggler() {
		PanelGrid panelGrid = PrimeFacesComponentFactory.createPanelGrid();
		CommandButton toggleButton = PrimeFacesComponentFactory.createCommandButton();
		toggleButton.setValue(I18nComponentLabelUtil.getLabelColumnsString());
		toggleButton.setIcon(ICON_CALCULATOR);
		toggleButton.setStyle(CssUtil.createStyleFloatRight());
		toggleButton.setType(TYPE_BUTTON);
		ColumnToggler columnToggler = PrimeFacesComponentFactory.createColumnToggler();
		columnToggler.setDatasource(dataTable.getId());
		columnToggler.setTrigger(toggleButton.getId());
		panelGrid.getChildren().add(columnToggler);
		panelGrid.getChildren().add(toggleButton);
		dataTable.getFacets().put(FACET_HEADER, toggleButton);
	}
	
	/**
	 * Creates the button for selecting the individuals.
	 */
	private void configureSelectButton()  {
		CommandButton btnSelect = PrimeFacesComponentFactory.createCommandButton();
		btnSelect.setValue(I18nComponentLabelUtil.getButtonChooseString());
		btnSelect.addActionListener(new ActionListener() {
			
			/**
			 * Runs through the table and looks for all individuals where the checkbox is checked. These
			 * individuals are added to the informationPackageManager as values for this object
			 * property. 
			 */
			@Override
			public void processAction(ActionEvent event) throws AbortProcessingException {
				for (int i = 0; i < dataModel.getRowCount(); i++) {
					DataRow rowData = dataModel.getRowData(i);
					Uuid uuid = rowData.getUuidOfIndividual();
					Optional<OntologyIndividual> optionalWithIndividual = individualList.getIndividualByUuid(uuid);
					if (rowData.isSelected()) {
						if (informationPackageManager.containsNotObjectPropertyValue(propertyIri, optionalWithIndividual.get())) {
							optionalWithIndividual.ifPresent(ontolgyIndividual -> 
							informationPackageManager.addObjectPropertyValue(propertyIri, ontolgyIndividual));
						}
					} else if (rowData.isNotSelected()){
						optionalWithIndividual.ifPresent(ontologyIndividual -> 
							informationPackageManager.removeObjectPropertyValue(propertyIri, ontologyIndividual));
					}
				}
			}
		});
		dataTable.getFacets().put(FACET_FOOTER, btnSelect);
	}
	
	/**
	 * Initializes the data table. 
	 */
	private void configureDataTable() {
		dataTable = PrimeFacesComponentFactory.createDataTable();
		dataTable.setVar(VAR_INDIVIDUAL);
		configureToggler();
		configureSelectButton();
	}
	
	/**
	 * Creates the columns. Uses the properties found in ontologyClass. For every property a column
	 * is created.
	 */
	private void createDynamicColumns(final OntologyClass ontologyClass) {
        columns.clear();  
        createColumnSelected();
        createColumnUuid();
        createColumnsForProperties();
	}

	/**
	 * Creates a column with a checkbox for selecting an individual.  
	 */
	private void createColumnSelected() {
		Column columnCheckboxes = PrimeFacesComponentFactory.createColumn();
        dataTable.getChildren().add(columnCheckboxes);
        SelectBooleanCheckbox booleanCheckbox = PrimeFacesComponentFactory.createSelectBooleanCheckbox();
        ValueExpression selectedValueExpression = JsfUtil.createValueExpression(VALUE_EXPRESSION_SELECTED, String.class);
        booleanCheckbox.setValueExpression(ATTRIBUTE_VALUE, selectedValueExpression);
        columnCheckboxes.getChildren().add(booleanCheckbox);
	}

	/**
	 * Creates a column that shows the uuid of an individual. 
	 */
	private void createColumnUuid() {
		final Column columnUuid = PrimeFacesComponentFactory.createColumn();
		final UIOutput columnUuidTitle = PrimeFacesComponentFactory.createUIOutput();
		columnUuidTitle.setValue(TITLE_COLUMN_UUID);
		columnUuid.getFacets().put(FACET_HEADER, columnUuidTitle);
		dataTable.getChildren().add(columnUuid);
		
		final HtmlOutputText lblColumnUuid = new HtmlOutputText();
		final ValueExpression uuidValueExpression = JsfUtil.createValueExpression(VALUE_EXPRESSION_UUID, String.class);
		lblColumnUuid.setValueExpression(ATTRIBUTE_VALUE, uuidValueExpression);
		columnUuid.getChildren().add(lblColumnUuid);
		
	}
	
	/**
	 * Creates columns for the properties in a class of an ontology. 
	 */
	private void createColumnsForProperties() {
        for (int i = 0; i < ontologyClass.getDatatypePropertiesCount(); i++) {
        	final OntologyDatatypeProperty ontologyProperty = ontologyClass.getDatatypeProperty(i);
        	final String columnTitleString = ontologyProperty.getLabel();
        	final Column column = PrimeFacesComponentFactory.createColumn();
        	final UIOutput columnTitle = PrimeFacesComponentFactory.createUIOutput();
			columnTitle.setValue(columnTitleString);
			column.getFacets().put(FACET_HEADER, columnTitle);
			dataTable.getChildren().add(column);
			
			final HtmlOutputText lblColumn = PrimeFacesComponentFactory.createHtmlOutputText();
			final ValueExpression valueExpression = JsfUtil.createValueExpression("#{individual.values[" + i + "]}", String.class);
			lblColumn.setValueExpression(ATTRIBUTE_VALUE, valueExpression);
			column.getChildren().add(lblColumn);
			dataTable.getChildren().add(column);
        }
	}
	
	/**
	 * Creates the data that are shown in the rows of the table.
	 * @param individualList These individuals contain the data that should be shown. 
	 */
	private void createRowData(OntologyIndividualCollection individualList) {
		OntologyIndividual firstIndividual = individualList.getIndividual(0);
		String iriOfOntologyClass = firstIndividual.getOntologyClass().getFullName();
		
		dataRows = new ArrayList<>();
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			createOneDataRow(i);
		}
		
		dataModel = new DataRowTableModel(dataRows);
		dataTable.setValue(dataModel);
		dynamicGuiManager.addDataModel(iriOfOntologyClass, dataModel);
	}
	
	/**
	 * Creates a row for one individual. 
	 * @param index The index of the individual in the collection with individuals. 
	 */
	private void createOneDataRow(int index) {
		final OntologyIndividual individual = individualList.getIndividual(index); 
		final DataRow dataRow = new DataRow(individual.getDatatypePropertiesCount());
		dataRow.setUuidOfIndividual(individual.getUuid());
		
		final String[] values = new String[individual.getDatatypePropertiesCount()];
		
		final UIComponent checkboxInTable = dataTable.getChildren().get(0);
		final Column checkboxColumn = (Column)checkboxInTable;
		final SelectBooleanCheckbox checkbox = (SelectBooleanCheckbox)checkboxColumn.getChildren().get(0);
		final String labelText = label.getValue().toString();
		final Optional<OntologyObjectProperty> optionalWithObjectProperty = individual.findObjectPropertyByLabel(labelText);
		optionalWithObjectProperty.ifPresent(ontologyObjectProperty ->  {
			checkbox.setSelected(true);
			//informationPackageManager.addObjectPropertyValue(ontologyObjectProperty.getPropertyId(), individual);
		});
		
		/* Because the first column with the index 0 is the checkbox that has no
		 * representation in the individual. 
		 */
		for (int columnCounter = 1; columnCounter < dataTable.getColumnsCount(); columnCounter++) {
			final UIComponent componentInTable = dataTable.getChildren().get(columnCounter);
			final Column column = (Column)componentInTable;
			final UIComponent headerFacet = column.getFacet(FACET_HEADER);
			if (headerFacet != null) {
				final UIOutput columnOutput = (UIOutput)headerFacet;
				final String columnTitle = columnOutput.getValue().toString();
				final Optional<OntologyDatatypeProperty> optionalWithDatatypeProperty = individual.
						findDatatypePropertyByLabel(columnTitle);
				final int i = columnCounter;
				optionalWithDatatypeProperty.ifPresent(datatypeProperty -> {
					Object value = datatypeProperty.getPropertyValue();
					
					if (value != null) {
						values[i-2] = datatypeProperty.getPropertyValue().toString();
					} else {
						values[i-2] = StringUtils.EMPTY_STRING;
					}
				});
			}
		}
		
		dataRow.setValues(values);
		dataRows.add(dataRow);
	}
	
	/**
	 * Create the data of the table dynamically with the values contained in a collection
	 * with individuals. 
	 */
	private void createDynamicData() {
		if (individualList.isNotEmpty()) {
			createRowData(individualList);
			panel.getChildren().add(dataTable);
		}
	}
	
	/**
	 * Creates a panel that contain the data table with the individuals. 
	 * @return The panel. 
	 */
	private HtmlPanelGroup createPanel() {
		if (panel == null) {
			panel = PrimeFacesComponentFactory.createPanelGroup();
		}
		
		panel.getChildren().clear();
		if (dataTable != null) {
			panel.getChildren().add(dataTable); 
			panel.getChildren().add(txtPropertyId);
		}
		
		return panel;
	}
	
	private void initializeInputHidden(String propertyId) {
		txtPropertyId = PrimeFacesComponentFactory.createInputHidden();
		txtPropertyId.setValue(propertyId);
	}
	
	/**
	 * Represents the columns in the table. It is needed because the columns are dynamically added
	 * to the table. 
	 * @author Christopher Olbertz
	 *
	 */
	static public class ColumnModel implements Serializable {
		private static final long serialVersionUID = -8155400270246467287L;
		private String header;
        private String property;
 
        public ColumnModel(String header, String property) {
            this.header = header;
            this.property = property;
        }
 
        public String getHeader() {
            return header;
        }
 
        public String getProperty() {
            return property;
        }
	}

	@Override
	public Object getGuiElement() {
		return createPanel();
	}

	@Override
	public Object getLabel() {
		return label;
	}

	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return null;
	}

	public OntologyIndividualCollection getIndividualList() {
		return individualList;
	}

	public void setIndividualList(OntologyIndividualCollection individualList) {
		this.individualList = individualList;
	}

	public List<ColumnModel> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnModel> columns) {
		this.columns = columns;
	}
	
	public void setDataRows(List<DataRow> dataRows) {
		this.dataRows = dataRows;
	}
	
	public List<DataRow> getDataRows() {
		return dataRows;
	}
	
	public void setSelectedDataRows(List<DataRow> selectedDataRows) {
		this.selectedDataRows = selectedDataRows;
	}
	
	public List<DataRow> getSelectedDataRows() {
		return selectedDataRows;
	}
}