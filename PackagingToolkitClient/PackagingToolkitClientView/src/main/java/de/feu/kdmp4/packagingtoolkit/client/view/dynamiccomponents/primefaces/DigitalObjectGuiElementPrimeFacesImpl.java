package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import java.util.Optional;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.outputlabel.OutputLabel;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nComponentLabelUtil;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DigitalObjectGuiElement;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;

/**
 * Creates a component that contains the digital objects of an information package.
 * @author Christopher Olbertz
 *
 */
public class DigitalObjectGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements DigitalObjectGuiElement {
	/**
	 * This component contains the labels with the file names. 
	 */
	private HtmlPanelGrid panelGridWithDigitalObjects;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	
	/**
	 * Creates a new component object.
	 * @param informationPackageManager Contains the information about the information package and
	 * its digital objects.
	 */
	public DigitalObjectGuiElementPrimeFacesImpl(final InformationPackageManager informationPackageManager) {
		super(PackagingToolkitConstants.DIGITAL_OBJECT_CLASS_NAME, PackagingToolkitConstants.DIGITAL_OBJECT_CLASS_NAME);
		initializeHtmlPanelGrid(informationPackageManager);
		initializePanelGroup();
	}
	
	/**
	 * Initializes the panel grid with the digital objects. Each digital object is rendered as 
	 * a output text that is contained in the panel grid. 
	 * @param informationPackageManager Contains the information about the information package and
	 * its digital objects.
	 */
	private void initializeHtmlPanelGrid(final InformationPackageManager informationPackageManager) {
		panelGridWithDigitalObjects = PrimeFacesComponentFactory.createHtmlPanelGrid();
		panelGridWithDigitalObjects.setColumns(1);
		
		if (informationPackageManager.noDigitalObjectContained()) {
			final HtmlOutputText txtDigitalObject = PrimeFacesComponentFactory.createHtmlOutputText();
			txtDigitalObject.setValue(I18nComponentLabelUtil.getLabelNoDigitalObjects());
			panelGridWithDigitalObjects.getChildren().add(txtDigitalObject);
		} else {
			for (int i = 0; i < informationPackageManager.getDigitalObjectCount(); i++) {
				final Optional<DigitalObject> optionalWithDigitalObject = informationPackageManager.getDigitalObjectAt(i);
				optionalWithDigitalObject.ifPresent(digitalObject -> {
					final HtmlOutputText txtDigitalObject = PrimeFacesComponentFactory.createHtmlOutputText();
					txtDigitalObject.setValue(digitalObject.getFilename());
					panelGridWithDigitalObjects.getChildren().add(txtDigitalObject);
				});
			}
		}
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(panelGridWithDigitalObjects);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
