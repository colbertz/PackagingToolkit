package de.feu.kdmp4.packagingtoolkit.client.view.prime.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Converts objects of 
 * {@link de.feu.kdmp4.packagingtoolkit.models.classes.Uuid} in String and
 * vice versa.
 * @author Christopher Olbertz
 *
 */
@FacesConverter(forClass = Uuid.class)
public class UuidConverter implements Converter {

	/**
	 * Converts a string entered by the user into an object
	 * @param context The faces context.
	 * @param component The component the user entered the value in.
	 * @param value The value itself. This is converted into an uuid.
	 * @return The uuid.  
	 */
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return new Uuid(value);
	}

	/**
	 * Converts an uuid from the application into a string for showing in the user interface.
	 * @param context The faces context.
	 * @param component The component that should show the uuid.
	 * @param value The uuid that has be converted into a String.
	 * @return A String representation of the uuid. 
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Uuid uuid = (Uuid)value;
		return uuid.toString();
	}

}
