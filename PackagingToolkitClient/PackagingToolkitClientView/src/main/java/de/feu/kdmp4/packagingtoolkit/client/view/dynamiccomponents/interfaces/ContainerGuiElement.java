package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

/**
 * Represents a component for the user interface that can contain other components. 
 */
public interface ContainerGuiElement extends GuiElement {
	/**
	 * Creates a component in a certain framework.
	 * @return The root component that contain all other components.
	 */
	public abstract Object toGuiElement();
	/**
	 * Adds a new child component to this container component.
	 * @param childElement The child element that should be added.
	 */
	public abstract void addChildElement(final GuiElement childElement);
}
