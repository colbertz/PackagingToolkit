package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;

/**
 * Contains some configuration data for the whole application, e.g. the status of the
 * application (productive or development). At the moment all configuration data are
 * read from the file application.properties.
 * @author Christopher Olbertz
 *
 */
public class ApplicationConfigurationBean {
	/**
	 * The path of the properties file for configuring the application. 
	 */
	private static final String APPLICATION_PROPERTIES = "src/main/resources/application.properties";
	/**
	 * The property in the configuration file that says if the application is in development status.
	 */
	private static final String PROPERTY_IN_DEVELOPMENT = "packagingtoolkit.inDevelopment";
	/**
	 * Contains the logo that is showed in the header of the application.
	 */
	//private StreamedContent logo;
	private String logo;
	
	private ServiceFacade serviceFacade;
	
	/**
	 * True if the application is running in development mode. If the application is 
	 * running in development mode, the debug windows are rendered.
	 */
	private boolean inDevelopment;
	
	/**
	 * Is called while starting the application and reads if the application runs in development mode. 
	 */
	@PostConstruct
	public void initialize() {
		readInDevelopment();
		readLogo();
	}
	
	/**
	 * Determines if the application is running in development mode.
	 * @return True if the application is running in development mode.
	 */
	private boolean readInDevelopment() {
		File propertiesFile = new File(APPLICATION_PROPERTIES);
		Properties properties = new Properties();
		
		try (InputStream inputStream = new FileInputStream(propertiesFile)) {
			// Load the properties.
			properties.load(inputStream);
			// Get the properties and write them in an object.
			String productive = properties.getProperty(PROPERTY_IN_DEVELOPMENT);
			return Boolean.valueOf(productive);
		} catch (IOException ioex) {
			return false;
		}
	}
	
	private void readLogo() {
		final File logoFile = serviceFacade.getLogo();
		/*try {
			logo = new DefaultStreamedContent(new FileInputStream(logoFile), "image/png");
		} catch (FileNotFoundException e) {
			JsfUtil.addErrorMessageToContext(I18nExceptionUtil.getErrorString(), I18nExceptionUtil.getLogoFIleNotFoundString());
		}*/
		logo = logoFile.getAbsolutePath();
	}
	
	public boolean isInDevelopment() {
		return inDevelopment;
	}
	
	public void setInDevelopment(boolean inDevelopment) {
		this.inDevelopment = inDevelopment;
	}
	
	public String getLogo() {
		return logo;
	}
	
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	public void setServiceFacade(ServiceFacade serviceFacade) {
		this.serviceFacade = serviceFacade;
	}
}
