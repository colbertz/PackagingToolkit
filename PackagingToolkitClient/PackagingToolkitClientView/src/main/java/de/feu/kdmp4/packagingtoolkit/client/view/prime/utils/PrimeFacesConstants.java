package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;

/**
 * Contains some useful constants for the work with PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class PrimeFacesConstants {
	/**
	 * The name of the header facet.
	 */
	public static final String FACET_HEADER = "header";
	/**
	 * The name of the header facet.
	 */
	public static final String FACET_FOOTER = "footer";
	/**
	 * The icon that shows a calculator. 
	 */
	public static final String ICON_CALCULATOR = "ui-icon-calculator";
	/**
	 * The name of the attribute value in PrimeFaces.
	 */
	public static final String ATTRIBUTE_VALUE = "value";
	/**
	 * The type of a button component in PrimeFaces.
	 */
	public static final String TYPE_BUTTON = "button";
}
