package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;

import java.io.File;

import javax.faces.context.FacesContext;

import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * Contains some helper methods related to the webapplication.
 * @author Christopher Olbertz
 *
 */
public abstract class WebAppUtils {
	private static final String TEMPORARY_DIRECTORY_NAME = "temp"; 
	/**
	 * Determines the temporary directory of the client. If it does not exist, it
	 * is created. The temporary directory contain all uploaded files that are
	 * processed later, for example the digital objects that are sended to the
	 * server.
	 * @return The path of the temporary directory.
	 */
	public static final String getClientTempDir() {
		final FacesContext facesContext = JsfUtil.getFacesContext();
		final String webappPath = facesContext.getExternalContext().getRealPath("/");
		final String tempDirName = webappPath + File.separator + TEMPORARY_DIRECTORY_NAME;
		
		final File tempDir = new File(tempDirName);
		
		if (!tempDir.exists()) {
			PackagingToolkitFileUtils.createDirectory(tempDirName);
		}
		
		return tempDir.getAbsolutePath();
	}
}
