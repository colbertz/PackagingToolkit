package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.outputlabel.OutputLabel;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DateGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

/**
 * Creates a component for a date value as a Calendar from PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class DateGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements DateGuiElement {
	/**
	 * This components represents the value. 
	 */
	private Calendar calendar;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;

	/**
	 * Creates a new component object.
	 * @param ontologyDateProperty The property that should be processed with this component.
	 */
	public DateGuiElementPrimeFacesImpl(final OntologyDateProperty ontologyDateProperty) {
		super(ontologyDateProperty);
		final String labelText = ontologyDateProperty.getLabel();
		final LocalDate value = ontologyDateProperty.getPropertyValue();
		initializeCalendar(labelText, value);
		initializePanelGroup();
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(calendar);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	/**
	 * Initializes the calendar. 
	 * @param labelText Is used for creating a identifier for this component.
	 * @param value The value that should be displayed in this component.
	 */
	private void initializeCalendar(final String labelText, final LocalDate value) {
		calendar = PrimeFacesComponentFactory.createCalendar();
		final String idCalendar = UniqueIdentifierUtil.createCalendarIdentifier(labelText);
		calendar.setId(idCalendar);
		final Locale currentLocale = Locale.getDefault();
		calendar.setLocale(currentLocale);
		if (value != null) {
			Date date = DateTimeUtils.convertLocalDateToDate(value);
			calendar.setValue(date);
		}
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
