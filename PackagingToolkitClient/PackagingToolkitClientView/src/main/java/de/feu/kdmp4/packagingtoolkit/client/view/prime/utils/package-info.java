/**
 * Contains some classes with helper classes and constants for the view.
 * @author Christopher Olbertz
 */

package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;