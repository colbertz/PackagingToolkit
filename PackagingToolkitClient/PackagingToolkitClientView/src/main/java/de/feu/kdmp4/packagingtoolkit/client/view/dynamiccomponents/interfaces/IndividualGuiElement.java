package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

/**
 * Represents a component for the user interface that can contain multiple individuals,
 * for example a table. 
 * @author Christopher Olbertz
 *
 */
public interface IndividualGuiElement extends GuiElement {

}
