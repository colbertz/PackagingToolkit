package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.client.enums.Views;
import de.feu.kdmp4.packagingtoolkit.client.exceptions.ConnectionException;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.utils.ClientLogUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

/**
 * This managed bean administrates the workspace of the application. The workspace
 * contains the main user interface. The managed beans contains the url of the page 
 * that is shown in the workspace. Every page of the application ist represented
 * by a constant in this class.
 * @author Christopher Olbertz
 *
 */
public class WorkspaceBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5382922662309703089L;
	/**
	 * The name of this managed bean. 
	 */
	public static final String MANAGED_BEAN_NAME = "workspaceBean";
	/**
	 * The attribute for the next page. 
	 */
	public static final String PAGE_ATTRIBUTE = "page";
	/**
	 * The page the user is currently visiting. 
	 */
	//private String page;
	/**
	 * A reference to the business logic of the client. 
	 */
	private ServiceFacade serviceFacade;

	private InformationPackageManager informationPackageManager;
	
	/**
	 * Shows the start page. 
	 */
	@PostConstruct
	// DELETE_ME
	public void init() {
		Views currentView = Views.STARTPAGE;
		//page = currentView.getUrl();
		ClientLogUtil.logNavigation(currentView);
	}
	
	/**
	 * Is called if the user wants to cancel the creation of an information package. In this case, the server has to be informed 
	 * because he eventually needs to delete the digital object of the cancelled information package if no other information
	 * package is referencing to this file.
	 */
	public String cancelInformationPackageCreation() {
		serviceFacade.cancelInformationPackageCreation();
		//informationPackageManager.cancelInformationPackageCreation();
		informationPackageManager.clearData();
		return onShowStartpage();
	}
	
	/**
	 * Shows the page selectOntologyNodes.xhtml.
	 */
	public String onShowSelectOntologyNodes() {
		Views currentView = Views.SELECT_ONTOLOGY_NODES;
		ClientLogUtil.logNavigation(currentView);
		return currentView.getUrl();
	}
	
	/**
	 * Throws an exception if the information package is expired.
	 */
    public void throwViewExpiredException() {
		OntologyReasonerException exception = OntologyReasonerException.createIndividualInvalidException(new StringList());
		throw exception;
    }
	
    /**
     * Shows the page with the list with existing archives. 
     */
	public String onShowArchiveList() {
		try {
			serviceFacade.pingServer();
			final Views currentView = Views.SHOW_ARCHIVE_LIST;
			ClientLogUtil.logNavigation(currentView);
			return currentView.getUrl();
		} catch (ConnectionException ex) {
			final Views currentView = Views.CONFIGURATION;
			ClientLogUtil.logNavigation(Views.CONFIGURATION);
			return currentView.getUrl();
		}
	}
	
	/**
	 * Shows the configuration page. 
	 */
	public String onShowConfiguration() {
		Views currentView = Views.CONFIGURATION;
		ClientLogUtil.logNavigation(currentView);
		return currentView.getUrl();
	}
	
	/**
	 * Shows the page for creating an information package.
	 */
	public String onShowCreateInformationPackage() {
		try {
			serviceFacade.pingServer();
			serviceFacade.pingMediator();
			final Views currentView = Views.CREATE_INFORMATION_PACKAGE;
			ClientLogUtil.logNavigation(currentView);
			return currentView.getUrl();
		} catch (ConnectionException ex) {
			final Views currentView = Views.CONFIGURATION;
			ClientLogUtil.logNavigation(Views.CONFIGURATION);
			return currentView.getUrl();
		}
	}
	
	/**
	 * Shows the page where the user can enter the property values.
	 */
	public String onShowOntologyTreeView() {
		Views currentView = Views.ONTOLOGY_TREE_VIEW;
		ClientLogUtil.logNavigation(currentView);
		return currentView.getUrl();
	}
	
	/**
	 * Shows the page with the existing views. 
	 */
	public String onShowSavedViews() {
		Views currentView = Views.SAVED_VIEWS;
		ClientLogUtil.logNavigation(currentView);
		return currentView.getUrl();
	}
	
	/**
	 * Shows the startpage. 
	 */
	public String onShowStartpage() {
		Views currentView = Views.STARTPAGE;
		ClientLogUtil.logNavigation(currentView);
		return currentView.getUrl();
	}
	
	public void setServiceFacade(ServiceFacade serviceFacade) {
		this.serviceFacade = serviceFacade;
	}
	
	public void setInformationPackageManager(InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}
}
