package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;

/**
 * Contains some constants that are only used in the client of the PackagingToolkit.
 * @author Christopher Olbertz
 *
 */
public class PackagingToolkitClientConstants {
	/**
	 * A default pattern for date time components for formatting the date and the time. 
	 */
	public static final String DEFAULT_PATTERN_DATE_TIME = "dd.MM.yyyy HH:mm:ss";
	/**
	 * A default pattern for time components for formatting the date and the time. 
	 */
	public static final String DEFAULT_PATTERN_TIME = "HH:mm:ss";
	/**
	 * A constant for application/octet-stream.
	 */
	public static final String OCTET_STREAM = "application/octet-stream";
}
