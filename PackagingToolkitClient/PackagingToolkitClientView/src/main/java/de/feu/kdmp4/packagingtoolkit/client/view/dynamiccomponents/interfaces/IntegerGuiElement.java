package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

/**
 * Represents a component for the user interface that can contain an integer value. At the
 * moment, this interface is empty, but it can be used for further development.
 * @author Christopher Olbertz
 *
 */
public interface IntegerGuiElement extends GuiElement {

}
