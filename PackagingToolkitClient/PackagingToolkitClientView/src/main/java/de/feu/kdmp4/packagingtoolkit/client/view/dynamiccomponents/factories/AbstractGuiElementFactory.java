package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories;

import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.BooleanGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ByteGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ContainerGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DateGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DateTimeGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DoubleGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DurationGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.FloatGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.GuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.IndividualGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.IntegerGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.LongGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ShortGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.StringGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.TimeGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ValueInputComponent;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * An abstract super class that contains methods for creating components for the graphical user interface.
 * It contains methods that determine the datatype of a property that should be processed in the user interface
 * and calls the correct method for displaying a component for the property. The methods that create the
 * concrete components are abstract and have to be implemented in concrete subclasses. The subclasses 
 * implement the methods for a certain gui framework like JSF. This abstract class has to be independant from
 * frameworks. 
 * @author Christopher Olbertz
 *
 */
public abstract class AbstractGuiElementFactory {
	/**
	 * Contains all information about the information package that is being created. 
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * Caches the values that were read from the ontologies from the server. 
	 */
	private ServerCache serverCache;
	/**
	 * Reference to an objct that contains some important values for creating the gui. 
	 */
	private DynamicGuiManager dynamicGuiManager;
	
	/**
	 * Creates a gui element factory. 
	 * @param informationPackageManager The information about the information package that is 
	 * created.
	 * @param savedIndividualList The individuals saved in the triple store 
	 * of the class for that gui components are created.
	 * If there are any individuals saved in the triple store, they are shown in the
	 * list with the individuals. 
	 */
	public AbstractGuiElementFactory(final InformationPackageManager informationPackageManager, 
			final ServerCache serverCache, final DynamicGuiManager dynamicGuiManager) {
		this.informationPackageManager = informationPackageManager;
		this.serverCache = serverCache;
		this.dynamicGuiManager = dynamicGuiManager;
	}
	
	/**
	 * Creates a GUI component. It checks the type of the default value of
	 * the desired component. Then the method, that creates the real 
	 * component is called. This component is created by the concrete
	 * subclasses, that implement the methods for a given technology.
	 * @param defaultValue A default value for the component.
	 * @param label The label for the component.
	 * @return The created component.
	 */
	public GuiElement createGuiComponent(final OntologyProperty ontologyProperty) {
		if (ontologyProperty.isBooleanProperty()) {
			return createBooleanGuiElement((OntologyBooleanProperty)ontologyProperty);
		} else if (ontologyProperty.isByteProperty()) {
			return createByteGuiElement((OntologyByteProperty)ontologyProperty);
		} else if (ontologyProperty.isDateProperty()) {
			return createDateGuiElement((OntologyDateProperty)ontologyProperty);
		} else if (ontologyProperty.isDateTimeProperty()) {
			return createDateTimeGuiElement((OntologyDateTimeProperty)ontologyProperty);
		} else if (ontologyProperty.isDoubleProperty()) {
			return createDoubleGuiElement((OntologyDoubleProperty)ontologyProperty);
		} else if (ontologyProperty.isDurationProperty()) {
			return createDurationGuiElement((OntologyDurationProperty)ontologyProperty);
		} else if (ontologyProperty.isFloatProperty()) {
			return createFloatGuiElement((OntologyFloatProperty)ontologyProperty);
		} else if (ontologyProperty.isIntegerProperty()) {
			return createIntegerGuiElement((OntologyIntegerProperty)ontologyProperty);
		} else if (ontologyProperty.isLongProperty()) {
			return createLongGuiElement((OntologyLongProperty)ontologyProperty);
		} else if (ontologyProperty.isShortProperty()) {
			return createShortGuiElement((OntologyShortProperty)ontologyProperty);
		} else if (ontologyProperty.isStringProperty()) {
			return createStringGuiElement((OntologyStringProperty)ontologyProperty);
		} else if (ontologyProperty.isTimeProperty()) {
			return createTimeGuiElement((OntologyTimeProperty)ontologyProperty);
		} else if (ontologyProperty.isObjectProperty()) {
			final OntologyObjectProperty objectProperty = (OntologyObjectProperty)ontologyProperty;
			final String rangeAsString = objectProperty.getPropertyRange();
			final String propertyIri = objectProperty.getPropertyId();
			
			if (StringValidator.isNotNullOrEmpty(rangeAsString)) {
				final OntologyClass range = serverCache.getClassByIri(rangeAsString);
				final OntologyIndividualCollection savedIndividualList = serverCache.getIndividualsByClass(range);
				final DatatypePropertyCollection datatypeProperties = serverCache.getDatatypePropertiesListByClassname(range.getFullName());
				if (!range.hasDatatypeProperties()) {
					range.addDatatypeProperties(datatypeProperties);
				}
				
				/*final ObjectPropertyList objectProperties = serverCache.getObjectPropertiesByClassname(range.getFullName());
				if (!range.hasObjectProperties()) {
					range.addObjectProperties(objectProperties);
				}*/
				
				return createIndividualGuiElement(savedIndividualList, range, propertyIri);
			}
		}
		throw new Error("Gui-Komponente muss noch implementiert werden!");
	}
	
	/**
	 * Creates the elements that should contain the information about the digital
	 * objects. This elements are shown if the user selects the class Digital_Object.
	 * @param informationPackageManager Contains the information about the information package and
	 * its digital objects.
	 * @return The container component that contains the elements for the digital
	 * objects.
	 */
	public abstract GuiElement createGuiElementsForDigitalObject(InformationPackageManager informationPackageManager);
	
	/**
	 * Creates an element for the gui that contains a taxonomy.
	 * @param ontologyIndividual The root individual of the taxonomy we want to display in the gui.
	 * @param serverCache Contains the data cached from the server.
	 * @return An element for the gui that contains the taxonomy determined by its root individual.
	 */
	public abstract GuiElement createTaxonomyGuiElement(final ServerCache serverCache);
	
	/**
	 * Creates a gui element for a boolean property.
	 * @param ontologyBooleanProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract BooleanGuiElement createBooleanGuiElement(final OntologyBooleanProperty ontologyBooleanProperty);
	/**
	 * Creates a gui element for a byte property.
	 * @param ontologyByteProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract ByteGuiElement createByteGuiElement(final OntologyByteProperty ontologyByteProperty);
	/**
	 * Creates a gui element for an object property. The element must be able to contain the values of individuals. Therefore
	 * it has be some kind of container element.
	 * @param ontologyClass The class the individuals are assigned to.
	 * @param serverCache Contains the data cached from the server.
	 * @return The created gui element.
	 */
	public abstract ContainerGuiElement createContainerElement(final OntologyClass ontologyClass, final ServerCache serverCache);
	/**
	 * Creates a gui element for a date property.
	 * @param ontologyDateProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract DateGuiElement createDateGuiElement(final OntologyDateProperty ontologyDateProperty);
	/**
	 * Creates a gui element for a datetime property.
	 * @param ontologyDateTimeProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract DateTimeGuiElement createDateTimeGuiElement(final OntologyDateTimeProperty ontologyDateTimeProperty);
	/**
	 * Creates a gui element for a double property.
	 * @param ontologyDoubleProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract DoubleGuiElement createDoubleGuiElement(final OntologyDoubleProperty ontologyDoubleProperty);
	/**
	 * Creates a gui element for a duration property. Because a duration value consists of multiple values, the created element has
	 * to contain several gui components. 
	 * @param ontologyDurationProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract DurationGuiElement createDurationGuiElement(final OntologyDurationProperty ontologyDurationProperty);
	/**
	 * Creates a gui element for a float property.
	 * @param ontologyFloatProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract FloatGuiElement createFloatGuiElement(final OntologyFloatProperty ontologyFloatProperty);
	/**
	 * Creates a gui element for a integer property.
	 * @param ontologyIntegerProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract IntegerGuiElement createIntegerGuiElement(final OntologyIntegerProperty ontologyIntegerProperty);
	/**
	 * Creates a gui element for a long property.
	 * @param ontologyLongProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract LongGuiElement createLongGuiElement(final OntologyLongProperty ontologyLongProperty);
	/**
	 * Creates a gui element for a short property.
	 * @param ontologyShortProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract ShortGuiElement createShortGuiElement(final OntologyShortProperty ontologyShortProperty);
	/**
	 * Creates a gui element for a string property.
	 * @param ontologyStringProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract StringGuiElement createStringGuiElement(final OntologyStringProperty ontologyStringProperty);
	/**
	 * Creates a gui element for a time property.
	 * @param ontologyTimeProperty The property for that a gui element should be created.
	 * @return The created gui element.
	 */
	protected abstract TimeGuiElement createTimeGuiElement(final OntologyTimeProperty ontologyTimeProperty);
	/**
	 * Creates a container that contains multiple components for entering data. The ontologyClass contains the properties and 
	 * the input component are build after these properties. For every property the created container element contains one
	 * label component and one or more input components.
	 * @param ontologyClass Contains the properties that are used for describing the structure of the container element.
	 * @return The created container element. It contains components for every property contained in the ontology class.
	 */
	public abstract ValueInputComponent createValueInputComponent(final OntologyClass ontologyClass);
	
	protected InformationPackageManager getInformationPackageManager() {
		return informationPackageManager;
	}
	
	protected ServerCache getServerCache() {
		return serverCache;	
	}
	
	protected DynamicGuiManager getDynamicGuiManager() {
		return dynamicGuiManager;
	}

	/**
	 * Creates a gui element for an object property. The element must be able to contain the values of individuals. Therefore
	 * it has be some kind of container element.
	 * @param ontologyClass The class the individuals are assigned to.
	 * @param referencedIndividuals The individuals that should be shown in the component.
	 * @param propertyIri The iri of the object property. 
	 * @return The created gui element.
	 */
	protected abstract IndividualGuiElement createIndividualGuiElement(final OntologyIndividualCollection individualList,
			final OntologyClass ontologyClass, final String propertyIri);
}
