package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.mindmap.DefaultMindmapNode;
import org.primefaces.model.mindmap.MindmapNode;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nMessagesUtil;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.utils.JsfConstants;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage.ViewException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Language;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * The classes and their subclasses are visualized in a mindmap like 
 * structure. The classes contained in the view the user has chosen in
 * the last step are preselected. If the user chose to create
 * a new view, no classes are selected. Now the user can
 * select some classes for the next step. He can also save the selected
 * classes as a new view.
 * @author Christopher Olbertz
 *
 */
public class OntologyNodesSelectionBean {
	// *************** Constants ************
	/**
	 * The color for the current main node. It is displayed in the 
	 * middle of the mind map.
	 */
	private static final String COLOR_MAIN_NODE = "ffcc00";
	/**
	 * The color for the parent nodes of the current main node.
	 */
	private static final String COLOR_PARENT_NODE = "6e9ebf";
	/**
	 * The color for the child nodes of the current main node.
	 */
	private static final String COLOR_SUB_NODE = "aa34e2";

	// ************* Attributes ************
	/**
	 * True if the button for the next step is disabled. This
	 * button is only enabled if the list with the selected nodes is 
	 * not empty.
	 */
	private boolean btnNextStepDisabled;
	
	/**
	 * True if the button for selecting all nodes is disabled. This
	 * button is only enabled if the list with the selected nodes is 
	 * empty.
	 */
	private boolean btnSelectAllNodesDisabled;
	/**
	 * True if the button for removing one nodes from the list is disabled. This
	 * button is only enabled if the list with the selected nodes is not 
	 * empty.
	 */
	private boolean btnRemoveNodeDisabled;
	/**
	 * True if the button for removing all nodes from the list is disabled. This
	 * button is only enabled if the list with the selected nodes is not 
	 * empty.
	 */
	private boolean btnRemoveAllNodesDisabled;
	/**
	 * Contains information about the information package the user is
	 * building.
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * The name of the node the user wants to delete from the list
	 * of the selected nodes.
	 */
	private String labelOfNodeToDelete;
	/**
	 * The root node of the mindmap.
	 */
	private MindmapNode rootNode;
	/**
	 * The facade class for accessing the service methods.
	 */
	private ServiceFacade serviceFacade;
	/**
	 * The name of the new view if the user wants to create one. 
	 */
	private String newViewName;
	/**
	 * Contains the nodes the user has selected for working with.
	 */
	private List<MindmapNode> selectedNodes;
	/**
	 * The cache that contains the data from the server.
	 */
	private ServerCache serverCache;
	/**
	 * True if the button for saving a new view is disabled. It is disabled if
	 * no classes are selected or if the name of the view is empty. 
	 */
	private boolean btnSaveNewViewDisabled;
	
	// ************** Initializing **************
	/**
	 * Initializes the view. The buttons are activated and respectively 
	 * disabled. The ontology is loaded and processed and according to the 
	 * ontology, the graphical representation is created.
	 */
	@PostConstruct
	public void initialize() {
		selectedNodes = new ArrayList<>();
		OntologyClass mainClass = null;
		enableAndDisableButtons();

		View selectedView = informationPackageManager.getSelectedView();
		if (selectedView.isNewView()) {
			// User wants to start with a new view.
			mainClass = serviceFacade.getHierarchyOfBaseOntology();
		} else {
			mainClass = extractClassHierarchyOfView(selectedView);
			enableAndDisableButtons();
		}
		
		Language selectedLanguage = (Language)JsfUtil.
				getBeanPropertyValue(JsfConstants.BEAN_I18N , JsfConstants.PROPERTY_SELECTED_LANGUAGE);
		createMindMap(mainClass);
	}
	
	public void initializePage(ComponentSystemEvent event) {
		boolean newVisit = informationPackageManager.isCancelled();
		if (newVisit) {
			initialize();
			informationPackageManager.setNewInformationPackage(false);
		}
	}

	/**
	 * Creates the mind map. The root node is the configured base class. Then the determined subclasses
	 * are processed and for each subclass on the first level of the hierarchy there is a mind map node 
	 * created. 
	 * @param mainClass Contains the subclasses that are visualized by the mind map.
	 */
	private void createMindMap(OntologyClass mainClass) {
		rootNode = new DefaultMindmapNode(mainClass.getLocalName(), 
				  mainClass, 
				  COLOR_MAIN_NODE, true);
		for (int i = 0; i < mainClass.getSubclassCount(); i++) {
			final OntologyClass ontologyClass = mainClass.getSubClassAt(i);
			addMindMapNodeForOntologyClass(ontologyClass);
		}		
	}
	
	/**
	 * Adds a node to the mind map for an ontology class.
	 * @param ontologyClass The ontology class that should be represented by a new mind map node.
	 */
	private void addMindMapNodeForOntologyClass(final OntologyClass ontologyClass) {
		final String label = determineLabel(ontologyClass.getFullName());
		final MindmapNode mindmapNode = new DefaultMindmapNode(label, 
														 ontologyClass, 
														 COLOR_PARENT_NODE, true);
		rootNode.addNode(mindmapNode);
	}
	
	/**
	 * Determines the label for the component for a property for the current language. If there is no
	 * label for the current language, the default label is used. If there is no default label, the iri of 
	 * the property is used as label.  
	 * @param ontologyProperty The property whose label we want to determine. 
	 */
	public String determineLabel(final String ontologyClassIri) {
		final OntologyClass ontologyClass = serverCache.getClassByIri(ontologyClassIri);
		final Locale locale = PrimeFacesUtils.getCurrentLocale();
		String labelText = ontologyClass.findLabelByLanguage(locale);
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyClass.getLabel();
		}
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyClass.getLocalName();
		}
		
		return labelText;
	}
	
	/**
	 * Determines the class hierarchy of the classes contained in a view. 
	 * @param selectedView The view which contains the classes the hierarchy is determined of.
	 * @return The class that is the root class of the determined hierarchy.
	 */
	private OntologyClass extractClassHierarchyOfView(final View selectedView) {
		final StringList ontologyClassNames = PackagingToolkitModelFactory.getStringList();
		OntologyClass mainClass = null;
		
		if (selectedView.isNewView()) {
			mainClass = serverCache.getRootClassWithHierarchy();
		} else {
			for (int i = 0; i < selectedView.getClassesCount(); i++) {
				final OntologyClass ontologyClass = selectedView.getOntologyClassAt(i);
				ontologyClassNames.addStringToList(ontologyClass.toString());
			}
			mainClass = serverCache.getHierarchy(ontologyClassNames);
		}
		
		return mainClass;
	}
	
	/**
	 * Is called if the user selects a node in the mind map. The node is added to the list with the selected
	 * nodes.
	 * @param selectEvent
	 */
	public void nodeToList(SelectEvent selectEvent) {
		final MindmapNode node = (MindmapNode)selectEvent.getObject();
		if (!isNodeSelected(node)) {
			final String nodeLabel = node.getLabel();
			final MindmapNode foundNode = searchForNodeByLabel(nodeLabel, rootNode);
			addNodeToList(foundNode);
		}
	}
	
	/**
	 * Checks if a node is null. If it is not null, then the node is added to the list
	 * with the selected nodes.
	 * @param node The node that should be added to the list.
	 */
	private void addNodeToList(final MindmapNode node) {
		if (node != null) {
			selectedNodes.add(node);
			enableAndDisableButtons();
		}
	}
	
	/**
	 * Looks recursivly for a node with a given label. 
	 * @param searchNodeLabel The label to look for.
	 * @param startNode The node the search in the next step has to start at.
	 * @return The found node or null if there was no node with the given label.
	 */
	private MindmapNode searchForNodeByLabel(final String searchNodeLabel, final MindmapNode startNode) {
		int i = 0;
		
		while(i < startNode.getChildren().size()) {
			final MindmapNode theOtherNode = startNode.getChildren().get(i);
			final String theOtherLabel = theOtherNode.getLabel();
			
			if (searchNodeLabel.equals(theOtherLabel)) {
				return theOtherNode;
			} else if(hasNodeChildren(theOtherNode)) {
				MindmapNode foundNode = searchForNodeByLabel(searchNodeLabel, theOtherNode);
				if (foundNode != null) {
					return foundNode;
				}
			}
			i++;
		}
		return null;
	}
	
	private boolean hasNodeChildren(final MindmapNode mindmapNode) {
		if (mindmapNode.getChildren().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Removes a node from the list with the selected nodes.
	 */
	public void removeNodeFromList() {
		final String searchLabel = StringUtils.extractLocalNameFromIri(labelOfNodeToDelete);
		final MindmapNode selectedNode = searchForNodeByLabel(searchLabel, rootNode);
		selectedNodes.remove(selectedNode);
		enableAndDisableButtons();
	}
	
	/**
	 * Removes all nodes from the list with the selected nodes. 
	 */
	public void removeAllNodesFromList() {
		selectedNodes.clear();
		enableAndDisableButtons();
	}
	
	/**
	 * The action event for saving the selected nodes as new view.
	 * @param event
	 */
	public void saveAsNewView(ActionEvent event) {
		if (StringValidator.isNullOrEmpty(newViewName)) {
			JsfUtil.addErrorMessageToContext(I18nMessagesUtil.getErrorString(), I18nMessagesUtil.getViewNameMayNotBeEmptyString());
		} else {
			if (selectedNodes.size() == 0) {
				JsfUtil.addErrorMessageToContext(I18nMessagesUtil.getErrorString(), I18nMessagesUtil.getViewMustContainClassesString());
			} else {
				try {
					final View newView = ClientModelFactory.createView(newViewName);
					addSelectedNodesToView(newView);
					serviceFacade.saveNewView(newView);
					newViewName = StringUtils.EMPTY_STRING;
					JsfUtil.addFacesMessageToContext(I18nMessagesUtil.getSuccessString(), I18nMessagesUtil.getViewSavedString());
				} catch (ViewException viewException) {
					JsfUtil.addErrorMessageToContext(viewException.getSummary(), viewException.getMessage());
				}
			}
		}
	}
	
	/**
	 * Adds the selected nodes to a view. 
	 * @param view The view that should contain the selected nodes.
	 */
	private void addSelectedNodesToView(final View view) {
		for (int i = 0; i < selectedNodes.size(); i++) {
			final MindmapNode mindmapNode = selectedNodes.get(i);
			final OntologyClass ontologyClass = (OntologyClass)mindmapNode.getData();
			view.addOntologyClass(ontologyClass);
		}
	}
	
	/**
	 * Is called if the user selects a node in the mind map. The selected node is the new
	 * root node and its children are displayed as sub nodes. This means the application zooms into
	 * the mind map. 
	 * @param selectEvent Contains the selected node.
	 */
	public void onSelectNode(SelectEvent selectEvent) {
		final MindmapNode node = (MindmapNode)selectEvent.getObject();
		
		if (!isNodeSelected(node) && hasSubNodes(node)) {
			createSubNodesForSubClasses(node);
		}
		
		if (isNodeSelected(node)) {
			JsfUtil.addFacesMessageToContext("Hinweis", "Unterklassen werden nicht angezeigt, weil der Knoten bereits ausgewaehlt wurde!");
		}
		enableAndDisableButtons();
	}
	
	/**
	 * Checks if a node is in the list with the selected nodes.
	 * @param mindmapNode The node we want to check.
	 * @return True, if mindmapNode is in the list with the selected nodes, false otherwise.
	 */
	private boolean isNodeSelected(final MindmapNode mindmapNode) {
		for (final MindmapNode nodeInList: selectedNodes) {
			if (nodeInList.equals(mindmapNode)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Extracts the ontology class that is saved in a mind map node. Then extracts its sub classes
	 * and creates a new mind map node for every sub class.
	 * @param node The node that contains an ontology class.
	 */
	private void createSubNodesForSubClasses(final MindmapNode node) {
		final OntologyClass ontologyClass = (OntologyClass)node.getData();
		
		for (int i = 0; i < ontologyClass.getSubclassCount(); i++) {
			final OntologyClass ontologySubclass = ontologyClass.getSubClassAt(i);
			createSubNodeForOntologyClass(ontologySubclass, node);
		}
	}
	
	/**
	 * Determines if the node has some sub nodes.
	 * @param node The node to check.
	 * @return True if node has sub nodes false otherwise.
	 */
	private boolean hasSubNodes(final MindmapNode node) {
		return node.getChildren().isEmpty();
	}
	
	/**
	 * Creates a sub node in the mind map for a given ontology class.
	 * @param ontologyClass The ontology class that should be represented in a mind map node.
	 * @param parentNode The node that contains the ontology class as sub node.
	 */
	private void createSubNodeForOntologyClass(final OntologyClass ontologyClass, final MindmapNode parentNode) {
		if (!ontologyClass.isNothing()) {
			final String label = determineLabel(ontologyClass.getFullName());
			final MindmapNode mindmapNode = new DefaultMindmapNode(label, 
					ontologyClass, COLOR_SUB_NODE, true);
			parentNode.addNode(mindmapNode);
		}
	}
	
	/**
	 * Is called if the user clicks the button for the next step. The classes that were selected by the user are
	 * saved as filter classes for the next step. Only these classes are shown in the next page. 
	 */
	public void nextStep() {
		final OntologyClassList ontologyClassList = determineFilterClassesForNextStep();		
		informationPackageManager.setFilterClasses(ontologyClassList);
	}
	
	/**
	 * Determines the classes the user has selected and saves them as filter classes for the
	 * next step. 
	 * @return The list with the classes selected for the next step. 
	 */
	private OntologyClassList determineFilterClassesForNextStep() {
		final OntologyClassList ontologyClassList = ClientModelFactory.createEmptyOntologyClassList();
		
		for (final MindmapNode mindmapNode: selectedNodes) {
			final OntologyClass ontologyClass = (OntologyClass)mindmapNode.getData();
			ontologyClassList.addOntologyClassToList(ontologyClass);
		}
		
		return ontologyClassList;
	}
	
	/**
	 * Enables and disables the button depending on the selected nodes. There are these two cases
	 * for enabling and disabling several buttons:
	 * <ul>
	 * 	<li>No node is selected.</li>
	 * 	<li>At least one node is selected.</li>
	 * </ul>
	 */
	private void enableAndDisableButtons() {
		if (selectedNodes.isEmpty()) {
			btnRemoveNodeDisabled = true;
			btnRemoveAllNodesDisabled = true;
			btnSelectAllNodesDisabled = false;
			btnNextStepDisabled = true;
		} else {
			btnRemoveNodeDisabled = false;
			btnRemoveAllNodesDisabled = false;
			btnSelectAllNodesDisabled = true;
			btnNextStepDisabled = false;
		}
		enableAndDisableButtonSaveNewView();
	}
	
	/**
	 * Is called if the user has changed the new view. The application has
	 * to decide if the button for saving the view has to be enabled or
	 * disabled.
	 */
	public void onNewViewNameChanged() {
		enableAndDisableButtonSaveNewView();
	}
	
	/**
	 * Enables or disables the button for saving a new view. The button
	 * is enabled if a name has been entered in the text field and if 
	 * at least one class has been selected.
	 */
	private void enableAndDisableButtonSaveNewView() {
		if (StringValidator.isEmpty(newViewName)) {
			btnSaveNewViewDisabled = true;
		} else if (selectedNodes.isEmpty()) {
			btnSaveNewViewDisabled = true;
		} else {
			btnSaveNewViewDisabled = false;
		}
	}
	
	/**
	 * The listener of the button for selecting all nodes. All nodes are inserted into the
	 * list with the selected nodes.
	 */
	public void selectAllNodes() {
		for (final MindmapNode node: rootNode.getChildren()) {
			selectedNodes.add(node);
		}
		enableAndDisableButtons();
	}
	
	/**
	 * Is called if the user selects a node in the list of selected nodes
	 * for deletion. The selection does not work without this listener
	 * although it does not do anything.
	 */
	public void selectNodeToDelete() {
		// Is necessary for the application to work. I dont now why.
	}
	
	public MindmapNode getRootNode() {
		return rootNode;
	}

	public boolean isBtnSelectAllNodesDisabled() {
		return btnSelectAllNodesDisabled;
	}

	public void setBtnSelectAllNodesDisabled(boolean btnSelectAllNodesDisabled) {
		this.btnSelectAllNodesDisabled = btnSelectAllNodesDisabled;
	}

	public boolean isBtnRemoveAllNodesDisabled() {
		return btnRemoveAllNodesDisabled;
	}

	public void setBtnRemoveAllNodesDisabled(boolean btnRemoveAllNodesDisabled) {
		this.btnRemoveAllNodesDisabled = btnRemoveAllNodesDisabled;
	}

	public List<MindmapNode> getSelectedNodes() {
		return selectedNodes;
	}

	public void setSelectedNodes(List<MindmapNode> selectedNodes) {
		this.selectedNodes = selectedNodes;
	}

	public boolean isBtnRemoveNodeDisabled() {
		return btnRemoveNodeDisabled;
	}

	public void setBtnRemoveNodeDisabled(boolean btnRemoveNodeDisabled) {
		this.btnRemoveNodeDisabled = btnRemoveNodeDisabled;
	}

	public boolean isBtnNextStepDisabled() {
		return btnNextStepDisabled;
	}

	public void setBtnNextStepDisabled(boolean btnNextStepDisabled) {
		this.btnNextStepDisabled = btnNextStepDisabled;
	}

	public String getNewViewName() {
		return newViewName;
	}

	public void setNewViewName(String newViewName) {
		this.newViewName = newViewName;
	}

	public String getNodeToDelete() {
		return labelOfNodeToDelete;
	}

	public void setNodeToDelete(String nodeToDelete) {
		this.labelOfNodeToDelete = nodeToDelete;
	}

	public void setInformationPackageManager(InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}

	public void setServiceFacade(ServiceFacade serviceFacade) {
		this.serviceFacade = serviceFacade;
	}
	
	public void setServerCache(ServerCache serverCache) {
		this.serverCache = serverCache;
	}
	
	public boolean isBtnSaveNewViewDisabled() {
		return btnSaveNewViewDisabled;
	}
	
	public void setBtnSaveNewViewDisabled(boolean btnSaveNewViewDisabled) {
		this.btnSaveNewViewDisabled = btnSaveNewViewDisabled;
	}
}