/**
 * Contains classes that are used for managing and saving important values through
 * the whole process of creating an information package. 
 */

package de.feu.kdmp4.packagingtoolkit.client.view.managers;