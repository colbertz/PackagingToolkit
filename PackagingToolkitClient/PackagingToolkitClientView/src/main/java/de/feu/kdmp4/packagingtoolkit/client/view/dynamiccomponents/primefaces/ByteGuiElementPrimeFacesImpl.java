package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.slider.Slider;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ByteGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;

/**
 * Creates a component for a byte value as a InputText and a Slider from PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class ByteGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements ByteGuiElement {
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	/**
	 * This components represents the value. 
	 */
	private InputText inputNumber;
	/**
	 * Can be used to manipulate the value.
	 */
	private Slider slider;
	
	/**
	 * Creates a new component object.
	 * @param ontologyByteProperty The property that should be processed with this component.
	 */
	public ByteGuiElementPrimeFacesImpl(final OntologyByteProperty ontologyByteProperty) {
		super(ontologyByteProperty);
		final String labelText = ontologyByteProperty.getLabel();
		short value = ontologyByteProperty.getPropertyValue();
		
		initializeInputElements(labelText, value);
		initializePanelGroup();
	}
	
	/**
	 * Initializes the input components.
	 * @param labelText The text of the description for the user. 
	 * @param value The value of the property that should be shown in the component.
	 */
	private void initializeInputElements(final String labelText, short value) {
		final String idInputNumber = UniqueIdentifierUtil.createInputNumberIdentifier(labelText);
		inputNumber = PrimeFacesComponentFactory.createInputText();
		inputNumber.setId(idInputNumber);
		inputNumber.setValue(value);
		
		slider = PrimeFacesComponentFactory.createSlider();
		final String idSlider = UniqueIdentifierUtil.createSliderIdentifier(labelText);
		slider.setId(idSlider);
		slider.setFor(idInputNumber);
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(inputNumber);
		panelGroup.getChildren().add(slider);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
