package de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels;

import java.util.ArrayList;
import java.util.List;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.*;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

/**
 * Contains a data model for the table with the individuals. This is needed because the table
 * is generated dynamically in the Java code.  
 * @author Christopher Olbertz
 *
 */
public class DataRowTableModel extends ListDataModel<DataRow> implements SelectableDataModel<DataRow> {
	/**
	 * The rows the user has selected by clicking the checkbox in the first column.
	 */
	private List<DataRow> selectedDataRows;
	
	/**
	 * Creates a new table model. 
	 */
	public DataRowTableModel() {
		selectedDataRows = new ArrayList<>();
	}
	
	/**
	 * Creates a new table model.
	 * @param data The data that should be shown in the table. 
	 */
	public DataRowTableModel(List<DataRow> data) {
		super(data);
		selectedDataRows = new ArrayList<>();
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public DataRow getRowData(String rowKey) {
		List<DataRow> dataRows = (List<DataRow>)getWrappedData();
		
		for (DataRow dataRow: dataRows) {
			String uuid = dataRow.getUuidOfIndividual().toString(); 
			if(areStringsEqual(uuid, rowKey)) {
				return dataRow;
			}
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public DataRow getRowData(int index) {
		List<DataRow> dataRows = (List<DataRow>)getWrappedData();
		return dataRows.get(index);
	}

	@Override
	public Object getRowKey(DataRow dataRow) {
		return dataRow.getUuidOfIndividual();
	}
	
	public List<DataRow> getSelectedDataRows() {
		return selectedDataRows;
	}
	
	public void setSelectedDataRows(List<DataRow> selectedDataRows) {
		this.selectedDataRows = selectedDataRows;
	}
}