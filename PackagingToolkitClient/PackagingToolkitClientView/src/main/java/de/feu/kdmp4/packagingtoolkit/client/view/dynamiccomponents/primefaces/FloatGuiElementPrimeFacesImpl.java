package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.FloatGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;

/**
 * Creates a component for a float value as a InputText from PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class FloatGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements FloatGuiElement {
	/**
	 * This components represents the value. 
	 */
	private InputText inputNumber;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	
	/**
	 * Creates a new component object.
	 * @param ontologyDateProperty The property that should be processed with this component.
	 */
	public FloatGuiElementPrimeFacesImpl(final OntologyFloatProperty ontologyFloatProperty) {
		super(ontologyFloatProperty);
		final String labelText = ontologyFloatProperty.getLabel();
		final float value = ontologyFloatProperty.getPropertyValue();
		
		initializeInputText(labelText, value);
		initializePanelGroup();
	}
	
	/**
	 * Initializes the input field for the number. 
	 * @param labelText Is used for creating a identifier for this component.
	 * @param value The value that should be displayed in this component.
	 */
	private void initializeInputText(final String labelText, final float value) {
		final String idInputNumber = UniqueIdentifierUtil.createInputNumberIdentifier(labelText);
		inputNumber = PrimeFacesComponentFactory.createInputText();
		inputNumber.setId(idInputNumber);
		inputNumber.setValue(value);
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(inputNumber);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
