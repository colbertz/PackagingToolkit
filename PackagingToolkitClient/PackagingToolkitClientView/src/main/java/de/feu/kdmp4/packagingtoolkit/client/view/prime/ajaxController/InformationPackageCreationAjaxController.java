package de.feu.kdmp4.packagingtoolkit.client.view.prime.ajaxController;

import javax.annotation.PostConstruct;

/**
 * Contains some ajax functions for 
 * {@link de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans.InformationPackageCreationBean}.
 * @author Christopher Olbertz
 *
 */
public class InformationPackageCreationAjaxController {
	/**
	 * True if the button for adding a new reference is disabled. This is the case if the user has already added a reference or a file
	 * to a SIP.
	 */
	private boolean addReferenceDisabled;

	/**
	 * True if the upload element for digital objects is disabled. It is 
	 * enabled if the information package that is created is not virtual. 
	 */
	private boolean digitalObjectUploadDisabled;
	/**
	 * True if the table with the references is visible. This is the case if the user has created any references. 
	 */
	private boolean referencesTableVisible;
	/**
	 * If true, the button for saving the ontology is disabled. In this case,
	 * the required data for the next step were not entered correctly.
	 */
	private boolean saveButtonDisabled;
	/**
	 * Is true, if the user entered a title for the package. If it is
	 * false, the next input components are disabled.
	 */
	private boolean titleValid;
	
	/**
	 * Resets the states of the components:
	 * <ul>
	 * 	<li>File upload is enabled.</li>
	 * 	<li>Button for adding a reference is enabled.</li>
	 * 	<li>Table with the references is hidden.</li>
	 * 	<li>Button for the next step is disabled.</li>
	 * </ul>
	 */
	public void reset() {
		enableFileUpload();
		enableAddReferenceButton();
		hideReferencesTable();
		disableSaveButton();
	}
	
	public void disableAddReferenceButton() {
		addReferenceDisabled = true;
	}
	
	public void disableSaveButton() {
		saveButtonDisabled = true;
		titleValid = false;
	}
	
	public void enableAddReferenceButton() {
		addReferenceDisabled = false;
	}
	
	public void enableSaveButton() {
		saveButtonDisabled = false;
	}
	
	public void enableFileUpload() {
		digitalObjectUploadDisabled = false;
	}
	
	public void disableFileUpload() {
		digitalObjectUploadDisabled = true;
	}
	
	public void hideReferencesTable() {
		referencesTableVisible = false;
	}
	
	public void showReferencesTable() {
		referencesTableVisible = true;
	}
	
	public boolean isAddReferenceDisabled() {
		return addReferenceDisabled;
	}

	public void setAddReferenceDisabled(final boolean addReferenceDisabled) {
		this.addReferenceDisabled = addReferenceDisabled;
	}

//	public boolean isDigitalObjectsTableVisible() {
//		return digitalObjectsTableVisible;
//	}
	
	public boolean isReferencesTableVisible() {
		return referencesTableVisible;
	}

	public void setReferencesTableVisible(final boolean referencesTableVisible) {
		this.referencesTableVisible = referencesTableVisible;
	}
	
	public boolean isSaveButtonDisabled() {
		return saveButtonDisabled;
	}

	public void setSaveButtonDisabled(final boolean saveButtonDisabled) {
		this.saveButtonDisabled = saveButtonDisabled;
	}
	
	public boolean isTitleValid() {
		return titleValid;
	}

	public void setTitleValid(final boolean titleValid) {
		this.titleValid = titleValid;
	}
	
	public void setTitleValid() {
		this.titleValid = true;
	}
	
	public void setTitleInvalid() {
		this.titleValid = false;
	}
	
	public boolean isDigitalObjectUploadDisabled() {
		return digitalObjectUploadDisabled;
	}

	public void setDigitalObjectUploadDisabled(final boolean digitalObjectUploadDisabled) {
		this.digitalObjectUploadDisabled = digitalObjectUploadDisabled;
	}
}
