package de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

/**
 * Describes a row in the table with the individuals. One row corresponds 
 * to one individual. 
 * @author Christopher Olbertz
 *
 */
public class DataRow {
	/**
	 * The uuid of the individual. 
	 */
	private Uuid uuidOfIndividual;
	/**
	 * True if the user has selected the individual.
	 */
	private boolean selected;
	/**
	 * The values of the individual. These are the values of the datatype properties of 
	 * the individual. 
	 */
	private String[] values;
	/**
	 * Maps a property on its iri. 
	 */
	private Map<String, Object> propertyMap;
	
	/**
	 * Creates a row.
	 * @param valueCount The number of columns in this row. 
	 */
	public DataRow(int valueCount) {
		values = new String[valueCount];
		propertyMap = new HashMap<>();
	}
	
	/**
	 * Adds a property to the row. 
	 * @param key The iri of the property. 
	 * @param value The property itself. 
	 */
	public void addProperty(String key, Object value) {
		propertyMap.put(key, value);
	}
	
	/**
	 * Marks this row as selected.
	 */
	public void select() {
		selected = true;
	}
	
	/**
	 * Removes the selection of this row.
	 */
	public void removeSelection() {
		selected = false;
	}
	
	public String[] getValues() {
		return values;
	}
	
	public void setValues(String[] values) {
		this.values = values;
	}
	
	public void setUuidOfIndividual(Uuid uuidOfIndividual) {
		this.uuidOfIndividual = uuidOfIndividual;
	}
	
	public Uuid getUuidOfIndividual() {
		return uuidOfIndividual;
	}
	
	public Map<String, Object> getPropertyMap() {
		return propertyMap;
	}
	
	public void setPropertyMap(Map<String, Object> propertyMap) {
		this.propertyMap = propertyMap;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public boolean isNotSelected() {
		return !selected;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}