package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import de.feu.kdmp4.packagingtoolkit.client.exceptions.ConnectionException;
import de.feu.kdmp4.packagingtoolkit.client.exceptions.informationpackages.InformationPackageCreationException;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.ajaxController.InformationPackageCreationAjaxController;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * The managed bean of the view for entering the data of a new information
 * package. The user has to enter the following data:
 * <ul>
 * 	<li>A title for the information package.</li>
 * 	<li>A package type: SIP or SIU.</li>
 * 	<li>The digital objects or the references of this information package.</li>
 * </ul>
 * The digital objects are uploaded into the directory for the 
 * temporary files of the client. The ontology is analyzed and a
 * tree of java object is build that represents the ontology tree.
 * The tree is displayed in the next step.  
 * @author Christopher Olbertz
 *
 */
public class InformationPackageCreationBean implements Serializable {
	// ************ Constants ********************
	private static final long serialVersionUID = -7258610833904479284L;
	private static final String WIDGET_VAR_CONFIRM_DELETE_ALL = "deleteDigitalObjectConfirmation";
	private static final String WIDGET_VAR_CONFIRM_CHANGE_TO_VIRTUAL = "changeToNotVirtualConfirmation";
	
	// ************ Attributes *******************
	/**
	 * The name of the used base ontology. It is displayed in the gui. 
	 */
	private String configuredBaseOntology;
	/**
	 * Contains the digital objects that were uploaded for this 
	 * information package.
	 */
	private DigitalObjectCollection digitalObjectList;
	/**
	 * Administrates the information package for further use in the
	 * client.
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * Contains a reference for the ajax controller of this bean. The ajax controller contains all information
	 * for enabling and disabling components or to make them visible or not.
	 */
	private InformationPackageCreationAjaxController ajaxController;
	/**
	 * The digital object the user selected for deleting in the table of digital 
	 * objects.
	 */
	private DigitalObject selectedDigitalObject;
	/**
	 * A reference to the object that encapsulates the service layer of the application.
	 */
	private ServiceFacade serviceFacade;
	/**
	 * the url of a new reference the user has added.
	 */
	private String newReferenceUrl;
	/**
	 * The ontology file uploaded by the user.
	 */
	private UploadedFile uploadedOntologyFile;
	/**
	 * The package type selected by the user as string. Is not used in the current version of
	 * PackagingToolkit. 
	 */
	// NOT_USED_IN_CURRENT_VERSION
	private String selectedPackageType;
	/**
	 * The references that have been saved in the information package.
	 */
	private ReferenceCollection referencesInInformationPackage;
	/**
	 * The reference the user has selected in the table.
	 */
	private Reference selectedReference;
	/**
	 * A reference to the object that creates model objects.
	 */
	private ClientModelFactory clientModelFactory;

	/**
	 * The constructor gets all necessary objects. They are injected by Spring or another external instance.
	 * @param informationPackageManager The information package manager saves all values of the information package that
	 * is in creation process.
	 * @param serviceFacade The facade that hides the service layer. All calls of the service layer have to be made with the help of
	 * this object.
	 */
	public InformationPackageCreationBean(final InformationPackageManager informationPackageManager, final ServiceFacade serviceFacade, 
			final ReferenceCollection referencesInInformationPackage, final DigitalObjectCollection digitalObjectList) {
		this.informationPackageManager = informationPackageManager;
		this.serviceFacade = serviceFacade;
		this.referencesInInformationPackage = referencesInInformationPackage;
		this.digitalObjectList = digitalObjectList;
		this.ajaxController = new InformationPackageCreationAjaxController();
	}
	
	@PostConstruct
	public void initalize() {
		informationPackageManager.clearData();
	}
	
	/**
	 * The listener, that creates an information package according to the user input.
	 * The information package is contained in the information package manager. 
	 */
	public void createInformationPackage() {
		readInformationPackageDataFromGui();
		createInformationPackageOnServer();
		addRemoteFileReferencesToInformationPackage();
		addDigitalObjectToInformationPackageManager();
		digitalObjectList = clientModelFactory.getEmptyDigitalObjectList();
		referencesInInformationPackage = ClientModelFactory.createEmptyReferenceCollection();
		ajaxController.reset();
		//informationPackageManager.clearDigitalObjects();
	}
	
	/**
	 * Adds the references to remote files to the information package. They are send to the server
	 * and saved in the information package in the session.
	 */
	private void addRemoteFileReferencesToInformationPackage() {
		if (referencesInInformationPackage.getReferencesCount() > 0) {
			for(int i = 0; i < referencesInInformationPackage.getReferencesCount(); i++) {
				final Reference reference = referencesInInformationPackage.getReference(i);
				serviceFacade.addRemoteFileReference(reference);
			}
		}
	}
	
	/**
	 * Adds the digital objects the user has chosen to the information package manager. The meta data of the 
	 * files are requested from the mediator.
	 */
	private void addDigitalObjectToInformationPackageManager() {
		final Uuid uuidOfInformationPackage = informationPackageManager.getUuid();
		for (int i = 0; i < digitalObjectList.getDigitalObjectCount(); i++) {
			final DigitalObject digitalObject = digitalObjectList.getDigitalObjectAt(i);
			digitalObject.setUuidOfInformationPackage(uuidOfInformationPackage);
			serviceFacade.addLocalFileReference(digitalObject);
			readMetadata(digitalObject);
		}
	}
	
	/**
	 * Reads the metadata of a digital object and puts the digital object in the information package manager.
	 * @param digitalObject The digital object whose metadata we want to receive from the mediator.
	 */
	private void readMetadata(final DigitalObject digitalObject) {
		final String filename = digitalObject.getAbsolutePath();
		final File file = new File(filename);
		final MediatorDataCollection mediatorDataCollection = serviceFacade.collectMetadata(file);
		
		final List<MediatorData> mediatorDataList = mediatorDataCollection.toList();
		for(final MediatorData mediatorData: mediatorDataList) {
			final String key = mediatorData.getName();
			final String value = mediatorData.getValue();
			digitalObject.addMetadata(key, value);
		}
		
		informationPackageManager.addDigitalObject(digitalObject);
	}
	 
	/**
	 * Reads the data of the information package entered by the user. The data are saved in the information
	 * package manager for use in the rest of the program.
	 */
	private void readInformationPackageDataFromGui() {
		final PackageType informationPackageType = PackageType.valueOf(selectedPackageType);
		informationPackageManager.setInformationPackageType(informationPackageType);
		//informationPackageManager.setTitle(packageTitle);
	}

	/**
	 * Creates an information package on the server with the help of the data entered by the user and
	 * saved in the information package manager.
	 */
	private void createInformationPackageOnServer() {
		final String packageTitle = getPackageTitle();
		if (informationPackageManager.isSip()) {
			serviceFacade.createSubmissionInformationPackage(packageTitle);
		} else {
			serviceFacade.createSubmissionInformationUnit(packageTitle);
		}
	}
	
	/**
	 * Is fired if the user wants to delete a digital object from the 
	 * information package. A action method is used because it seem to
	 * be the only one that is called before the f:setPropertyActionListener.
	 * With every other tried possibility like action listener the
	 * selectedDigitalObject was null. Because the action method is only
	 * a workaround and we want to stay on the current page we return null.  
	 */
	public String deleteDigitalObject() {
		digitalObjectList.deleteDigitalObject(selectedDigitalObject);
		final String filepath = PrimeFacesUtils.deleteFileFromTempDir(selectedDigitalObject.getAbsolutePath());
		informationPackageManager.removeReference(filepath);
		ajaxController.enableFileUpload();
		ajaxController.enableAddReferenceButton();
		return null;
	}
	
	/**
	 * Initializes the form. As default, the package type SIP is
	 * selected and the radio button for a virtual information
	 * package.
	 */
	@PostConstruct
	public void initialize() {
		try {
			prepareGuiForEmptyInformationPackage();
			configuredBaseOntology = serviceFacade.getBaseOntologyName();
		} catch (ConnectionException connectionException) {
			JsfUtil.showErrorMessage(connectionException);
		}
	}
	
	/**
	 * Is called if the user clicks the button for adding a new reference. The components that have to be disabled are disabled. 
	 * At the moment the user can only create SIPs. SIPs can contain only one reference. This means that the component for 
	 * creating a remote reference and digital objects are disabled. If you want to allow SIUs, you have to modify this method.
	 */
	public void onAddReference(final AjaxBehaviorEvent ajaxBehaviorEvent) {
		try {
			if (StringValidator.isNotNullOrEmpty(newReferenceUrl)) {
				addUrlToInformationPackage();
				clearUrlInputField(ajaxBehaviorEvent);
				ajaxController.disableFileUpload();
				disableAddReference();
				ajaxController.showReferencesTable();
			} else {
				JsfUtil.addErrorMessageToContext(I18nExceptionUtil.getErrorString(), 
												 I18nExceptionUtil.getReferenceMayNotBeNullString());
			}
		} catch (UserException userException) {
			JsfUtil.showErrorMessage(userException);
		}
	}
	
	/**
	 * Adds the url the user has entered to the information package. Then the url is cleared.
	 */
	private void addUrlToInformationPackage() {
		newReferenceUrl = newReferenceUrl.trim();
		final Reference reference = informationPackageManager.addRemoteFileReference(newReferenceUrl);
		referencesInInformationPackage.addReference(reference);
		newReferenceUrl = StringUtils.EMPTY_STRING;
	}
	
	/**
	 * Deletes the text from the text field the user has entered the url in. 
	 * @param ajaxBehaviorEvent The event that has been fired by the command button. It contains the source
	 * of the event, the button that is used as start for the search for the input text field.
	 */
	private void clearUrlInputField(final AjaxBehaviorEvent ajaxBehaviorEvent) {
		final UIComponent sourceButton = (UIComponent)ajaxBehaviorEvent.getSource();
		final UIComponent parentOfButton = sourceButton.getParent();
		
		for (final UIComponent component: parentOfButton.getChildren()) {
			if (component instanceof InputText) {
				InputText inputText = (InputText) component;
				inputText.setValue(StringUtils.EMPTY_STRING);
			}
		}
	}
	
	/**
	 * Is called if the user clicks the button for deleting a reference from the information package.
	 */
	public void deleteReference() {
		deleteReferenceFromInformationPackage();
		ajaxController.enableFileUpload();
		enableAddReference();
		ajaxController.hideReferencesTable();
	}
	
	/**
	 * Removes a reference to a remote file from the information package.
	 */
	private void deleteReferenceFromInformationPackage() {
		final String url = selectedReference.getUrl();
		referencesInInformationPackage.removeReference(url);
		informationPackageManager.removeReference(url);
	}
	
	/**
	 * Prepares the gui if the user wants to create a new information package. The default
	 * values are entered in the gui.
	 */
	private void prepareGuiForEmptyInformationPackage() {
		ajaxController.disableFileUpload();
		disableAddReference();
		selectedPackageType = PackageType.SIP.toString();
		ajaxController.disableSaveButton();
		ajaxController.disableFileUpload();
	}
	
	/**
	 * Checks if a title has been entered, if the value change event of
	 * the input box for the title has been fired. If the title is empty, 
	 * the input components below the title are deactivated.
	 * @param ajaxBehaviorEvent The event fired by a value change of the 
	 * text input box for the title.
	 */
	public void onPackageTitleChanged(final AjaxBehaviorEvent ajaxBehaviorEvent) {
		final String packageTitle = getPackageTitle();
		if (StringValidator.isEmpty(packageTitle)) {
			ajaxController.disableFileUpload();
			disableAddReference();
			ajaxController.setTitleInvalid();
		} else {
			ajaxController.enableFileUpload();
			enableAddReference();
			ajaxController.setTitleValid();
		}
	}

	/**
	 * Checks if a url for a reference has been entered, if the value change event of
	 * the input box for the reference url has been fired. If the url is empty, 
	 * the button for saving the reference is disabled.
	 * @param ajaxBehaviorEvent The event fired by a value change of the 
	 * url input box for the title.
	 */
	public void onReferenceUrlChanged(final AjaxBehaviorEvent ajaxBehaviorEvent) {
		if (StringValidator.isEmpty(newReferenceUrl)) {
			ajaxController.disableAddReferenceButton();
		} else {
			ajaxController.enableAddReferenceButton();
		}
	}
	
	/**
	 * Checks if the user changes the selected package type from SIC to SIP. If yes, 
	 * the listener checks if there is more than one digital object in the information
	 * package. If yes, a message has to be showed. 
	 * @param valueChangeEvent The value change event.
	 */
	// NOT_USED_IN_CURRENT_VERSION
	public void onPackageTypeChanged(final AjaxBehaviorEvent ajaxBehaviorEvent) {
		if (isSipSelected()) {
			if (digitalObjectList.getDigitalObjectCount() > 
						PackagingToolkitConstants.SIP_MAX_DIGITAL_OBJECTS_NUMBER) {
				PrimeFacesUtils.showDialog(WIDGET_VAR_CONFIRM_DELETE_ALL);
			} else if (digitalObjectList.getDigitalObjectCount() ==
						PackagingToolkitConstants.SIP_MAX_DIGITAL_OBJECTS_NUMBER) {
				ajaxController.disableFileUpload();
			} else {
				ajaxController.enableFileUpload();
			}
		} else {
			ajaxController.enableFileUpload();
		}
	}
	
	/**
	 * Is fired if the user decides in the confirmation dialog that he does not want to
	 * change from not virtual to virtual because all digital objects are deleted then.
	 * @param ajaxBehaviorEvent
	 */
	public void doNotChangePackageTypeToVirtual(final AjaxBehaviorEvent ajaxBehaviorEvent) {
		ajaxController.enableFileUpload(); 
		PrimeFacesUtils.hideDialog(WIDGET_VAR_CONFIRM_CHANGE_TO_VIRTUAL);
	}
	
	/**
	 * Is fired if the user clicks on the no button in the confirmation dialog. The
	 * dialog wants a confirmation that the package type should be switched from
	 * SIU to SIP although there have been too many digital objects uploaded. If
	 * the user clicks no, the package type stays SIU and no digital objects are
	 * deleted.
	 * @param ajaxBehaviorEvent
	 */
	public void doNotChangePackageTypeToSip(final AjaxBehaviorEvent ajaxBehaviorEvent) {
		selectedPackageType = PackageType.SIU.toString();
		PrimeFacesUtils.hideDialog(WIDGET_VAR_CONFIRM_DELETE_ALL);
	}
	
	/**
	 * Is fired by the upload component for the digital objects. If a
	 * digital object is uploaded, it is copied to the temp directory
	 * of the client and added to the list with the digital objects
	 * of this information package.
	 * <br />
	 * Adds a error message to the context if the file already exists
	 * in the temp directory and if the user tries to upload a
	 * second digital object to a SIP.g
	 * @param fileUploadEvent The event that contains the uploaded file.
	 */
	public void uploadDigitalObject(final FileUploadEvent fileUploadEvent) {
		try {
			addDigitalObjectToInformationPackage(fileUploadEvent);
			
			if (isSipSelected()) {
				ajaxController.disableFileUpload();
				disableAddReference();
			}
		} catch (Exception ex) {
			FacesContext facesContext = JsfUtil.getFacesContext();
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					ex.getMessage(), ex.getMessage()));
		}
	}
	
	/**
	 * Reads the digital object the user has entered and copies the file into the temporary directory of the client.
	 * It is not uploaded to the server yet.
	 * @param fileUploadEvent The event that contains the uploaded file.
	 */
	private void addDigitalObjectToInformationPackage(final FileUploadEvent fileUploadEvent) {
		checkDigitalObjectCount();
		final UploadedFile uploadedFile = fileUploadEvent.getFile();
		final String filename = PrimeFacesUtils.copyUploadedFileToTempDir(uploadedFile);
		final DigitalObject digitalObject = ClientModelFactory.getDigitalObject(filename);
		digitalObjectList.addDigitalObject(digitalObject);
		informationPackageManager.addLocalFileReference(filename);
	}
	
	/**
	 * Checks if the upload of a digital object is allowed. It is not allowed if
	 * the selected package type is SIP and the information package already
	 * contains one digital object.
	 * @throws InformationPackageCreationException Is thrown if SIP is selected
	 * as package type and it contains already one digital object.
	 */
	private void checkDigitalObjectCount() {
		if (selectedPackageType != null) {
			final PackageType packageType = PackageType.valueOf(selectedPackageType);
			// A SIP may contain only one digital object.
			if (packageType == PackageType.SIP) {
				if (digitalObjectList.getDigitalObjectCount() == 1) {
					final InformationPackageCreationException exception = 
							InformationPackageCreationException.createDigitalObjectCountSipException();
					throw exception;
				}
			}
		}
	}
	
	// ************* getter and setter **************
	public String getPackageTitle() {
		return informationPackageManager.getTitle();
	}

	public void setPackageTitle(String packageTitle) {
		informationPackageManager.setTitle( packageTitle);
	}
	
	public String getSelectedPackageType() {
		return selectedPackageType;
	}

	public void setSelectedPackageType(String selectedPackageType) {
		this.selectedPackageType = selectedPackageType;
	}

	/**
	 * Runs through the list with the uploaded digital objects
	 * and return them as list. The method is necessary because
	 * the list is encapsulated and the data table of PrimeFaces
	 * needs access to the list. In this method, copies of the digital objects
	 * are created because we want only to show their name and not their
	 * path in the table. But we need their path for the further processing.
	 * @return The list with the digital objects.
	 */
	public List<DigitalObject> getDigitalObjectList() {
		final List<DigitalObject> digitalObjects = new ArrayList<>();
		
		for (int i = 0; i < digitalObjectList.getDigitalObjectCount(); i++) {
			final DigitalObject digitalObject = digitalObjectList.getDigitalObjectAt(i);
			String filename = digitalObject.getAbsolutePath();
			filename = PackagingToolkitFileUtils.extractFileName(filename);
			final DigitalObject copyOfDigitalObject = ClientModelFactory.getDigitalObject(filename, digitalObject.
					getUuidOfInformationPackage(), digitalObject.getUuidOfDigitalObject());
			digitalObjects.add(copyOfDigitalObject);
		}
		
		return digitalObjects;
	}
	
	/**
	 * Determines if there are any digital objects uploaded yet. It is called by
	 * the GUI for deciding if the table with the digital objects should be 
	 * rendered or not.
	 * @return True if there are any digital objects uploaded, false otherwise.
	 */
	public boolean isDigitalObjectUploaded() {
		if (digitalObjectList.getDigitalObjectCount() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public DigitalObject getSelectedDigitalObject() {
		return selectedDigitalObject;
	}

	public void setSelectedDigitalObject(DigitalObject selectedDigitalObject) {
		this.selectedDigitalObject = selectedDigitalObject;
	}
	
	public boolean getOntologyUploaded() {
		return uploadedOntologyFile != null;
	}

	public String getConfiguredBaseOntology() {
		return configuredBaseOntology;
	}

	private boolean isSipSelected() {
		return selectedPackageType.equals(PackageType.SIP.toString());
	}
	
	private void enableAddReference() {
		ajaxController.enableAddReferenceButton();
	}
	
	private void disableAddReference() {
		ajaxController.disableAddReferenceButton();
	}
	
	public String getNewReferenceUrl() {
		return newReferenceUrl;
	}

	public void setNewReferenceUrl(String newReferenceUrl) {
		this.newReferenceUrl = newReferenceUrl;
	}
	
	/**
	 * Runs through the list with the references
	 * and return them as list. The method is necessary because
	 * the list is encapsulated and the data table of PrimeFaces
	 * needs access to the list.
	 * @return The list with the references.
	 */
	public List<Reference> getReferencesInInformationPackage() {
		final List<Reference> references = new ArrayList<>();
		
		for (int i = 0; i < referencesInInformationPackage.getReferencesCount(); i++) {
			final Reference reference = referencesInInformationPackage.getReference(i);
			references.add(reference);
		}
		
		return references;
	}

	public Reference getSelectedReference() {
		return selectedReference;
	}

	public void setSelectedReference(Reference selectedReference) {
		this.selectedReference = selectedReference;
	}
	
	public InformationPackageCreationAjaxController getAjaxController() {
		return ajaxController;
	}
	
	public void setAjaxController(InformationPackageCreationAjaxController ajaxController) {
		this.ajaxController = ajaxController;
	}
	
	public void setClientModelFactory(ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}
}