package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import java.io.File;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.behavior.ajax.AjaxBehavior;
import org.primefaces.component.fieldset.Fieldset;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.outputpanel.OutputPanel;
import org.primefaces.component.selectbooleanbutton.SelectBooleanButton;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import de.feu.kdmp4.packagingtoolkit.client.exceptions.ConnectionException;
import de.feu.kdmp4.packagingtoolkit.client.exceptions.gui.ServerUrlMayNotBeEmptyException;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nComponentLabelUtil;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nMessagesUtil;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ServerConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.utils.JsfConstants;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.WebAppUtils;
import de.feu.kdmp4.packagingtoolkit.exceptions.ConfigurationException;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * This managed bean is used for the configuration of the client. The 
 * user can input the data for the configuration and they are saved
 * in a properties file. The user is able to modify the configuration
 * data. It is not possible to work with the program if the configuration
 * data are not set. 
 * @author Christopher Olbertz
 *
 */
public class ConfigurationBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4305115935644176636L;
	/**
	 * This object allows the access to the methods of the service layer.
	 */
	private ServiceFacade serviceFacade;
	/**
	 * The name of the configured base ontology. 
	 */
	private String configuredBaseOntology;
	/**
	 * The name of the configured rule ontology. 
	 */
	private String configuredRuleOntology;
	/**
	 * The panel in the gui that contains the extractor tools.
	 */
	private OutputPanel dataSourcesPanel;
	/**
	 * The url of the mediator.
	 */
	private String mediatorUrl;
	/**
	 * The url of the server.
	 */
	private String serverUrl;
	/**
	 * The file with the logo image the user has uploaded.
	 */
	private File logoFile;
	/**
	 * True if the logo has changed and the logo file must be processed.
	 */
	private boolean logoChanged;
	/**
	 * The base ontology the user uploaded.
	 */
	private File uploadedBaseOntology;
	/**
	 * The rule ontology the user uploaded.
	 */
	private File uploadedRuleOntology;
	/**
	 * The path to the working directory on the server.
	 */
	private String workingDirectoryPath;
	/**
	 * Allows the creation of model objects of the client.
	 */
	private ClientModelFactory clientModelFactory;
	/**
	 * True if the panel for the configuration of the mediator is disabled.
	 */
	private boolean mediatorPanelDisabled;
	/**
	 * True if the panel for the configuration of the server is disabled.
	 */
	private boolean serverPanelDisabled;
	/**
	 * Caches the data that have been received from the server. 
	 */
	private ServerCache serverCache;
	
	/**
	 * Initializes the bean with the data from the configuration on client, server
	 * and mediator.
	 */
	@PostConstruct
	public void init() {
		initClientConfiguration();
		connectToMediator();
		connectToServer();
	}
	
	/**
	 * Initializes the configuration data of the client read from the properties file in the user interface.  
	 */
	private void initClientConfiguration() {
		final ClientConfigurationData configurationData = serviceFacade.
				getClientConfigurationData();
		serverUrl = configurationData.getServerUrl();
		mediatorUrl = configurationData.getMediatorUrl();
	}
	
	/**
	 * Connects to the server. If the server is not accessible the tab for the configuration of the mediator is 
	 * disabled.
	 */
	private void connectToMediator() {
		try {
			initMediatorConfiguration();
			mediatorPanelDisabled = false;
		} catch (UserException ex) {
			JsfUtil.showErrorMessage(ex);
	        mediatorPanelDisabled = true;
		}
	}
	
	/**
	 * Initializes the configuration data of the mediator read from the properties file on the mediator in the user interface.  
	 */
	private void initMediatorConfiguration() {
		dataSourcesPanel = new OutputPanel();
		initializeDataSourcesPanel();
	}
	
	/**
	 * Initializes the panel with the data sources that are registered on the mediator. Then the 
	 * data sources are shown in the user interface. 
	 */
	private void initializeDataSourcesPanel() {
		final DataSourceCollection dataSources = serviceFacade.findAllDataSources();

		for (int i = 0; i < dataSources.getDataSourceCount(); i++) {
			final DataSource dataSource = dataSources.getDataSourceAt(i);
			String dataSourceName = dataSource.getDataSourceName();
			dataSourceName = StringUtils.deleteSpaces(dataSourceName);
			final Fieldset dataSourceFieldset = new Fieldset();
			dataSourceFieldset.setLegend(dataSource.getDataSourceName());
			final HtmlPanelGrid dataSourcePanelGrid = new HtmlPanelGrid();
			dataSourcePanelGrid.setColumns(2);
			final String panelDataSourceId = UniqueIdentifierUtil.createPanelIdentifier(dataSourceName);
			dataSourcePanelGrid.setId(panelDataSourceId);
			
			for (int j = 0; j < dataSource.getDataSourceToolCount(); j++) {
				final DataSourceTool dataSourceTool = dataSource.getDataSourceToolAt(j);
				createButtonForDataSourceTool(dataSourceTool, dataSourcePanelGrid, i);
			}
			dataSourceFieldset.getChildren().add(dataSourcePanelGrid);
			dataSourcesPanel.getChildren().add(dataSourceFieldset);
		}
	}
	
	/**
	 * Initializes the ajax behavior of the boolean buttons for the data source tools. If a button is clicked, it changes its state
	 * from off to on. The values of all other buttons are changed to false.
	 * @param event The event that fires this behavior.
	 * @param dataSourcePanelGrid The panel that contains all boolean buttons.
	 */
	private void initializeAjaxEventOfBooleanSelectButton(final AjaxBehaviorEvent event, final HtmlPanelGrid dataSourcePanelGrid) {
		// Determine the button that has been clicked.
		final UIComponent sourceComponent = event.getComponent();
		final String sourceId = sourceComponent.getClientId();
		
		// The buttons are the children of fitsPanelGrid.
		for (final UIComponent component: dataSourcePanelGrid.getChildren()) {
			// The value of the source button may not be changed.
			if (!sourceId.equals(component.getClientId())) {
				// For security reasons: Check before cast.
				if (component instanceof SelectBooleanButton) {
					// Change the value of the button to false.
					final SelectBooleanButton theButton = (SelectBooleanButton)component;
					theButton.setValue(false);
				}
			}
		}
	}
	
	/**
	 * Creates a button for one data source tool. It is a boolean button, this means a button that can have to states: on 
	 * and off. 
	 * @param dataSourceTool The data source tool the button is for.
	 * @param dataSourcePanelGrid The panel grid that contains the buttons.
	 * @param dataSourceToolCounter A counter value for the data source tool. Is used for creating an id for the button.
	 */
	private void createButtonForDataSourceTool(final DataSourceTool dataSourceTool, final HtmlPanelGrid 
			dataSourcePanelGrid, int dataSourceToolCounter) {
		String toolname = dataSourceTool.getToolName();
		toolname = StringUtils.deleteSpaces(toolname);
		final boolean activated = dataSourceTool.isActivated();
		final OutputLabel outputLabel = new OutputLabel();
		outputLabel.setValue(toolname);
		dataSourcePanelGrid.getChildren().add(outputLabel);
		final SelectBooleanButton selectBooleanButton = new SelectBooleanButton();
		selectBooleanButton.setValue(activated);
		selectBooleanButton.setOnLabel(I18nComponentLabelUtil.getLabelYesString());
		selectBooleanButton.setOffLabel(I18nComponentLabelUtil.getLabelNoString());
		selectBooleanButton.setOffIcon(JsfConstants.ICON_CLOSE);
		selectBooleanButton.setOnIcon(JsfConstants.ICON_CHECK);
		final String selectBooleanId = UniqueIdentifierUtil.createButtonIdentifier(String.valueOf(dataSourceToolCounter));
		selectBooleanButton.setId(selectBooleanId);
		
		final AjaxBehavior clickBehavior = new AjaxBehavior();
		clickBehavior.addAjaxBehaviorListener(event -> {
				initializeAjaxEventOfBooleanSelectButton(event, dataSourcePanelGrid);
		});
		
		clickBehavior.setUpdate(dataSourcePanelGrid.getClientId());
		selectBooleanButton.addClientBehavior(JsfConstants.AJAX_EVENT_CHANGE, clickBehavior);
		
		dataSourcePanelGrid.getChildren().add(selectBooleanButton);
	}
	
	/**
	 * Connects to the server. If the server is not accessible the tab for the server configuration is 
	 * disabled.
	 */
	private void connectToServer() {
		try {
			initServerConfiguration();
			serverPanelDisabled = false;
		} catch (ServerUrlMayNotBeEmptyException ex) {
			String message = ex.getMessage();
			JsfUtil.addErrorMessageToContext("Fehler", message);
		} catch (ConnectionException ex) {
			JsfUtil.showErrorMessage(ex);
	        serverPanelDisabled = true;
		}
	}

	/**
	 * Initializes the configuration data of the server read from the properties file on the server in the user interface.  
	 */
	private void initServerConfiguration() {
		final ServerConfigurationData serverConfiguration = serviceFacade.readServerConfiguration();
		workingDirectoryPath = serverConfiguration.getWorkingDirectoryPath();
		configuredRuleOntology = serverConfiguration.getConfiguredRuleOntologyPath();
		configuredBaseOntology = serverConfiguration.getConfiguredBaseOntologyPath();
	}
	
	/**
	 * The action listener for saving the configuration of the client. It is called when
	 * the user clicks the save button. A message is shown if the configuration has been 
	 * saved or an error message in the case of an error.
	 */
	public void onSaveClientConfiguration(ActionEvent actionEvent) {
		try {
			final ClientConfigurationData clientConfigurationData = clientModelFactory.
					getClientConfigurationData(mediatorUrl, serverUrl);
			serviceFacade.writeClientConfigurationData(clientConfigurationData);
			
			if(logoChanged) {
				serviceFacade.saveLogo(logoFile);
				logoChanged = false;
			}

			JsfUtil.addFacesMessageToContext(I18nMessagesUtil.getSuccessString(), 
											 I18nMessagesUtil.getConfigurationSavedString());
			connectToMediator();
			connectToServer();
		} catch (ConfigurationException ex) {
			JsfUtil.addErrorMessageToContext(I18nMessagesUtil.getErrorString(), 
											 I18nMessagesUtil.getConfigurationSavingErrorString());
		}
	}
	
	/**
	 * Is called if the user cancels the configuration. The values are not saved and the
	 * uploaded files in the temporary directory are deleted.
	 * @param event The action event fired by the cancel button.
	 */
	public void removeUploadedFiles(ActionEvent event) {
		final File temporaryDirectory = new File(WebAppUtils.getClientTempDir());
		final File[] tempFiles = temporaryDirectory.listFiles();
		
		if (tempFiles != null) {
			for (final File file: tempFiles) {
				file.delete();
			}
		}
	}
	
	/**
	 * Saves the configuration of the server.
	 */
	public void onSaveServerConfiguration() {
		try {
			final ServerConfigurationData serverConfigurationData = ClientModelFactory.
					createServerConfigurationData(workingDirectoryPath);
			
			if (uploadedBaseOntology != null) {
				final String baseOntologyPath = uploadedBaseOntology.getAbsolutePath();
				serverConfigurationData.setConfiguredBaseOntologyPath(baseOntologyPath);
			}
			
			if (uploadedRuleOntology != null) {
				final String ruleOntologyPath = uploadedRuleOntology.getAbsolutePath();
				serverConfigurationData.setConfiguredRuleOntologyPath(ruleOntologyPath);
			}
			
			saveRuleOntology(serverConfigurationData);
			saveBaseOntology(serverConfigurationData);
			
			serviceFacade.writeServerConfigurationData(serverConfigurationData);
			serverCache.initialize();
			
			JsfUtil.addFacesMessageToContext(I18nMessagesUtil.getSuccessString(), 
											 I18nMessagesUtil.getConfigurationSavedString());
		} catch (ConfigurationException ex) {
			JsfUtil.addErrorMessageToContext(I18nMessagesUtil.getErrorString(), 
											 ex.getMessage());
		}
	}
	
	/**
	 * Saves the rule ontology on the server.
	 * @param serverConfigurationData Contains the configuration data for the server. The rule ontology
	 * is entered in this object.
	 */
	private void saveRuleOntology(final ServerConfigurationData serverConfigurationData) {
		if (uploadedRuleOntology != null) {
			final String ontologyName = uploadedRuleOntology.getName();
			serverConfigurationData.setConfiguredRuleOntologyPath(ontologyName);
			serverConfigurationData.setRuleOntology(uploadedRuleOntology);
			configuredRuleOntology = ontologyName;
			// Reset the file to avoid errors.
			uploadedRuleOntology = null;
		}
	}
	
	/**
	 * Saves the base ontology on the server.
	 * @param serverConfigurationData Contains the configuration data for the server. The base ontology
	 * is entered in this object.
	 */
	private void saveBaseOntology(final ServerConfigurationData serverConfigurationData) {
		if (uploadedBaseOntology != null) {
			final String ontologyName = uploadedBaseOntology.getName();
			serverConfigurationData.setConfiguredBaseOntologyPath(ontologyName);
			serverConfigurationData.setBaseOntology(uploadedBaseOntology);
			configuredBaseOntology = ontologyName;
			// Reset the file to avoid errors.
			uploadedBaseOntology = null;
		}
	}
	
	/**
	 * Is called, when a user uploads a base ontology. 
	 * Displays a success message for the user. 
	 */
	public void uploadBaseOntology(FileUploadEvent fileUploadEvent) {
		final UploadedFile uploadedOntologyFile = fileUploadEvent.getFile();

		if (uploadedOntologyFile != null) {
			final String filename = PrimeFacesUtils.copyUploadedFileToTempDir(uploadedOntologyFile);
			uploadedBaseOntology = new File(filename);
		
			// The ontology file was successfully uploaded and processed.
			JsfUtil.addFacesMessageToContext(I18nMessagesUtil.getInformationString(), 
					 I18nMessagesUtil.getOntologyUploadedString());
		}
	}
	
	/**
	 * Is called, when a user uploads a rule ontology. 
	 * Displays a success message for the user. 
	 */
	public void uploadRuleOntology(FileUploadEvent fileUploadEvent) {
		final UploadedFile uploadedOntologyFile = fileUploadEvent.getFile();

		if (uploadedOntologyFile != null) {
			final String filename = PrimeFacesUtils.copyUploadedFileToTempDir(uploadedOntologyFile);
			uploadedRuleOntology = new File(filename);
		
			// The ontology file was successfully uploaded.
			JsfUtil.addFacesMessageToContext(I18nMessagesUtil.getInformationString(), 
					 I18nMessagesUtil.getOntologyUploadedString());
		}
	}
	
	
	/**
	 * Is called, when a user uploads a logo. 
	 * Displays a success message for the user. 
	 */
	public void uploadLogo(FileUploadEvent fileUploadEvent) {
		final UploadedFile uploadedOntologyFile = fileUploadEvent.getFile();

		if (uploadedOntologyFile != null) {
			final String filename = PrimeFacesUtils.copyUploadedFileToTempDir(uploadedOntologyFile);
			logoFile = new File(filename);
		
			// The logo  file was successfully uploaded.
			JsfUtil.addFacesMessageToContext(I18nMessagesUtil.getInformationString(), 
					 I18nMessagesUtil.getLogoUploadedString());
			logoChanged = true;
		}
	}
	
	/**
	 * Saves the configuration of the mediator.
	 * @param actionEvent The action event fired by the command button.
	 */
	public void onSaveMediatorConfiguration(ActionEvent actionEvent) {
		// The list with the configuration done by the user.
		final DataSourceCollection dataSources = clientModelFactory.createDataSourceCollection();
		
		// Loop through the children of the root panel. The children are the fieldset objects.
		for (Object object: dataSourcesPanel.getChildren()) {
			final DataSourceToolCollection dataSourceTools = clientModelFactory.createEmptyDataSourceToolCollection();
			final Fieldset fieldset = (Fieldset)object;
			final HtmlPanelGrid panelGrid = (HtmlPanelGrid)fieldset.getChildren().get(0);
			// Loop through the children of the panel grid. That are the labels and buttons.
			for (int i = 0; i < panelGrid.getChildCount(); i = i + 2) {
				/* The loop counter is incremented in steps of two, because one line 
				 * consisting of two elements (one label and one boolean button) is 
				 * processed in one loop step.
				 */
				final DataSourceTool dataSourceTool = determineDataSourceToolByButton(i, panelGrid);
				dataSourceTools.addDataSourceToolToList(dataSourceTool);
			}
			final String dataSourceName = fieldset.getLegend();
			final DataSource dataSource = clientModelFactory.createDataSource(dataSourceName, dataSourceTools);
			dataSources.addDataSource(dataSource);
		}
		serviceFacade.saveDataSourceConfiguration(dataSources);
	}
	
	/**
	 * Determines which data source is tool is describes by a button. 
	 * @param buttonIndex The index of the button in the panel grid.
	 * @param panelGrid The panel grid that contains the buttons.
	 * @return The data source tool described by the button.
	 */
	private DataSourceTool determineDataSourceToolByButton(final int buttonIndex, final HtmlPanelGrid panelGrid) {
		final OutputLabel outputLabel = (OutputLabel)panelGrid.getChildren().get(buttonIndex);
		final SelectBooleanButton booleanButton = (SelectBooleanButton)panelGrid.
				getChildren().get(buttonIndex + 1);
		final String toolName = (String)outputLabel.getValue();
		final boolean activated = (Boolean)booleanButton.getValue();
		final DataSourceTool dataSourceTool = clientModelFactory.createDataSourceTool(toolName, activated);
		return dataSourceTool;
	}
	
	public String getServerUrl() {
		return serverUrl;
	}
	
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}
	
	public String getMediatorUrl() {
		return mediatorUrl;
	}

	public void setMediatorUrl(String mediatorUrl) {
		this.mediatorUrl = mediatorUrl;
	}

	public OutputPanel getDataSourcesPanel() {
		return dataSourcesPanel;
	}

	public void setDataSourcesPanel(OutputPanel dataSourcesPanel) {
		this.dataSourcesPanel = dataSourcesPanel;
	}

	public String getWorkingDirectoryPath() {
		return workingDirectoryPath;
	}

	public void setWorkingDirectoryPath(String workingDirectoryPath) {
		this.workingDirectoryPath = workingDirectoryPath;
	}

	public String getConfiguredRuleOntology() {
		if (StringValidator.isNotNullOrEmpty(configuredRuleOntology)) {
			return configuredRuleOntology;
		} else {
			return I18nComponentLabelUtil.getLabelNothingConfiguredString();
		}
	}

	public void setConfiguredRuleOntology(String configuredRuleOntology) {
		this.configuredRuleOntology = configuredRuleOntology;
	}

	public String getConfiguredBaseOntology() {
		if (StringValidator.isNotNullOrEmpty(configuredBaseOntology)) {
			return configuredBaseOntology;
		} else {
			return I18nComponentLabelUtil.getLabelNothingConfiguredString();
		}
	}

	public void setConfiguredBaseOntology(String configuredBaseOntology) {
		this.configuredBaseOntology = configuredBaseOntology;
	}

	public void setServiceFacade(ServiceFacade serviceFacade) {
		this.serviceFacade = serviceFacade;
	}
	
	public void setClientModelFactory(ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}
	
	public boolean isMediatorPanelDisabled() {
		return mediatorPanelDisabled;
	}
	
	public void setMediatorPanelDisabled(boolean mediatorPanelDisabled) {
		this.mediatorPanelDisabled = mediatorPanelDisabled;
	}
	
	public boolean isServerPanelDisabled() {
		return serverPanelDisabled;
	}
	
	public void setServerPanelDisabled(boolean serverPanelDisabled) {
		this.serverPanelDisabled = serverPanelDisabled;
	}
	
	public void setServerCache(ServerCache serverCache) {
		this.serverCache = serverCache;
	}
	
	public ServerCache getServerCache() {
		return serverCache;
	}
}
