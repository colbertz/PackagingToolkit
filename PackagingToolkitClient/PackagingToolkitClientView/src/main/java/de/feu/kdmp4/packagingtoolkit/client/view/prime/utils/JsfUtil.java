package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nMessagesUtil;
import de.feu.kdmp4.packagingtoolkit.client.utils.StringUtil;
import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Contains some helper methods for the work with JSF. They are independant from any other
 * JSF based framework like PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class JsfUtil {
	/**
	 * Adds a message to the faces context. 
	 * @param title The title of the message. 
	 * @param text The text of the message. 
	 */
	public static void addFacesMessageToContext(final String title, final String text) {
		final FacesContext facesContext = getFacesContext();
		facesContext.addMessage(null, new FacesMessage(title, text));
	}
	
	/**
	 * Adds an error message to the context. Title and text of the message are contained in an exception.
	 * @param userException The exception that contains the title and the text of the message.
	 */
	public static void showErrorMessage(final UserException userException) {
		final String title = userException.getSummary();
		final String text = userException.getMessage();
		JsfUtil.addErrorMessageToContext(title, text);
	}
	
	/**
	 * Adds an error message to the context.
	 * @param title The title of the message. If null or empty, the string "error" is used.
	 * @param text The text of the error message.
	 */
	public static void addErrorMessageToContext(String title, final String text) {
		final FacesContext facesContext = getFacesContext();
		if (StringValidator.isNullOrEmpty(title)) {
			title = I18nMessagesUtil.getErrorString();
		}
		
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
				   title, text));
	}
	
	/**
	 * Generates a string that contains a AJAX update. This is a string with the ids of the components
	 * that are updated by AJAX. the ids are separated with a blank. The string is assigned to the
	 * attribute update of an AjaxBehavior.
	 * @param updateComponents An array with the components to update.
	 * @return The update string for AJAX.
	 */
	public static String generateAjaxUpdateString(final UIComponent[] updateComponents) {
		final StringBuffer updateString = new StringBuffer();
		
		for (final UIComponent component: updateComponents) {
			updateString.append(component.getClientId());
			updateString.append(StringUtil.EMPTY_STRING);
		}
		
		return updateString.toString().trim();
	}
	
	/**
	 * Determines the faces context.
	 * @return The faces context. 
	 */
	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	/**
	 * 
	 * Determines the EL context.
	 * @return The EL context.
	 */
	  public static ELContext getELContext() {
		   return FacesContext.getCurrentInstance().getELContext();
	  }
	
	  /**
	   * Determines the expression factory.
	   * @param facesContext The current faces context.
	   * @return The expression factory.
	   */
	  public static ExpressionFactory getExpressionFactory(final FacesContext facesContext) {
		  return getApplication(facesContext).getExpressionFactory();
	  }
	  
	  /**
	   * Determines the application object.
	   * @param facesContext The current faces context.
	   * @return The application object. 
	   */
	  public static Application getApplication(final FacesContext facesContext) {
		  final Application application = facesContext.getApplication(); 
		  return application;
	  }
	  
	  /**
	   * Creates a value expression for using in the Java code. An value expression has the form: 
	   * #{bean.property}. Does not check if the parameters are correct, for example does not check
	   * if a bean actually exists. 
	   * @param beanName The name of the bean the value expression is related to.  
	   * @param propertyName The name of the property the value expression is related to.
	   * @param theClass The class the value expression returns.
	   * @param facesContext The current faces context. 
	   * @return The created value expression. 
	   */
	  public static ValueExpression createValueExpression(final String beanName, 
			  final String propertyName,
			  final Class<?> theClass,
			  final FacesContext facesContext) {
		  final String expression = "#{" + beanName + "." + propertyName + "}";
		  final ExpressionFactory expressionFactory = getExpressionFactory(facesContext);
		  final ValueExpression valueExpression = expressionFactory.
				  createValueExpression(getELContext(), expression, theClass);
		  return valueExpression;
	  }  
	  
	  /**
	   * Creates a value expression for using in the Java code. An value expression has the form: 
	   * #{bean.property}. Does not check if the parameters are correct, for example does not check
	   * if a bean actually exists. 
	   * @param valueExpression The value expression as String.   
	   * @param valueType The class the value expression returns.
	   * @return The created value expression. 
	   */
		public static ValueExpression createValueExpression(final String valueExpression, final Class<?> valueType) {
	        final FacesContext facesContext = FacesContext.getCurrentInstance();
	        return facesContext.getApplication().getExpressionFactory().createValueExpression(
	            facesContext.getELContext(), valueExpression, valueType);
	    }
	  
	  /**
	   * Creates a method expression for using in the Java code. An value expression has the form: 
	   * #{bean.method}. Does not check if the parameters are correct, for example does not check
	   * if a bean actually exists. 
	   * @param beanName The name of the bean the value expression is related to.  
	   * @param methodName The name of the method the method expression is related to.
	   * @param returnedClass The class that is returned by the method. 
	   * @param parameterClasses The classes of the parameters of the method. 
	   * @return The created method expression. 
	   */
		public static MethodExpression createMethodExpression(String expression, Class<?> returnType, Class<?>... parameterTypes) {
		    FacesContext facesContext = FacesContext.getCurrentInstance();
		    return facesContext.getApplication().getExpressionFactory().createMethodExpression(
		        facesContext.getELContext(), expression, returnType, parameterTypes);
		}
	  /*public static MethodExpression createMethodExpression(final String beanName,
			  final String methodName,
			  final Class<?> returnedClass,
			  final Class<?>[] ... parameterClasses) {
		  final FacesContext facesContext = getFacesContext();
		  final String expression = "#{" + beanName + "." + methodName + "}";
		  final ExpressionFactory expressionFactory = getExpressionFactory(facesContext); 
		  return expressionFactory.createMethodExpression(getELContext(), expression, 
				  								   returnedClass, parameterClasses);
	  }*/
	  
	  /**
	   * Creates a value expression for using in the Java code. This expression changes the value of a property.
	   * An value expression has the form: 
	   * #{bean.property}. Does not check if the parameters are correct, for example does not check
	   * if a bean actually exists. 
	   * @param beanName The name of the bean the value expression is related to.  
	   * @param propertyName The name of the property the value expression is related to.
	   * @param value The value the property should get. 
	   * @return The created value expression. 
	   */
	  public static void setValueExpression(final String beanName, 
			  final String propertyName,
			  final Class<?> theClass,
			  final String value) {
		final String expression = "#{" + beanName + "." + propertyName + "}";
		final ExpressionFactory e = getExpressionFactory(getFacesContext());
		final ValueExpression valueExpression = e.
				createValueExpression(getELContext(), expression, theClass);
		valueExpression.setValue(getELContext(), value);
	  }  
	  
	  /**
	   * Determines the value of the property of a bean. 
	   * @param beanName The name of the bean.
	   * @param propertyName The name of the property. 
	   * @return The value the property has. 
	   */
	  public static Object getBeanPropertyValue(final String beanName, final String propertyName) {
		  final FacesContext facesContext = getFacesContext();
		  final Application application = facesContext.getApplication();
		  final String expression = "#{" + beanName + "." + propertyName + "}";
		  return application.evaluateExpressionGet(facesContext, expression, Object.class);
	  }
	  
	  /**
	   * Determines the id of the current view. Is useful for debbuging issues.
	   * @return The id of the current view.
	   */
	  public static String getViewId() {
		  return getFacesContext().getViewRoot().getViewId();
	  }
}
