package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import static de.feu.kdmp4.packagingtoolkit.validators.StringValidator.isNotEmpty;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIPanel;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.AjaxBehaviorListener;

import org.primefaces.behavior.ajax.AjaxBehavior;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.spinner.Spinner;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nComponentLabelUtil;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.decorators.interfaces.IndividualsFinishedDecoratedList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.utils.JsfConstants;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.css.CssUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesGuiElementFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.UserInputComponent;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ValueInputComponent;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRow;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;

public class UserInputComponentImpl implements UserInputComponent {
	/**
	 * Is used for creating a unique identifier for the reset button.
	 */
	private static final String IDENTIFIER_BUTTON_RESET = "Reset";
	/**
	 * Is used for creating a unique identifier for the save button.
	 */
	private static final String IDENTIFIER_BUTTON_SAVE = "Save";
	/**
	 * Is used for creating a unique identifier for the button for creating a new individual.
	 */
	private static final String IDENTIFIER_BUTTON_NEW_INDIVIDUAL = "NewIndividual";
	/**
	 * The index of the input field for the days in a duration element. Is needed for evaluating
	 * a duration component.
	 */
	private static final int INDEX_INPUT_DAYS = 5;
	/**
	 * The index of the input field for the hours in a duration element. Is needed for evaluating
	 * a duration component.
	 */
	private static final int INDEX_INPUT_HOURS = 7;
	/**
	 * The index of the input field for the minutes in a duration element. Is needed for evaluating
	 * a duration component.
	 */
	private static final int INDEX_INPUT_MINUTES = 9;
	/**
	 * The index of the input field for the months in a duration element. Is needed for evaluating
	 * a duration component.
	 */
	private static final int INDEX_INPUT_MONTHS = 3;
	/**
	 * The index of the input field for the negative checkbox in a duration element. Is needed for evaluating
	 * a duration component.
	 */
	private static final int INDEX_INPUT_NEGATIVE = 13;
	/**
	 * The index of the input field for the seconds in a duration element. Is needed for evaluating
	 * a duration component.
	 */
	private static final int INDEX_INPUT_SECONDS = 11;
	/**
	 * The index of the input field for the years in a duration element. Is needed for evaluating
	 * a duration component.
	 */
	private static final int INDEX_INPUT_YEARS = 1;
	
	//private static final int INDEX_TARGET_LIST = 5;
	
	/**
	 * The button the user can user to empty all fields. 
	 */
	private CommandButton btnReset;
	/**
	 * The button the user can user to save the individual. 
	 */
	private CommandButton btnSave;
	/**
	 * The button the user can user to create a new individual with empty fields. 
	 */
	private CommandButton btnNewIndividual;
	/**
	 * Contains the input component that were dynamically created.
	 */
	private HtmlPanelGroup pnlLiterals;
	/**
	 * Contains a label that says if the user is creating a new individual or editing an
	 * existing one. 
	 */
	private HtmlPanelGroup pnlNorth;
	/**
	 * Contains important information about the information package that is currently
	 * being created. 
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * Caches the elements read from the ontologies on the server.
	 */
	private ServerCache serverCache;
	/**
	 * The class from the ontologies we are working with. The individual created is assigned to
	 * this class. 
	 */
	private OntologyClass ontologyClass;
	/**
	 * Contains the input components.
	 */
	private ValueInputComponent valueInputComponent;
	/**
	 * Creates objects for the properties in an ontology. 
	 */
	private PropertyFactory propertyFactory;
	/**
	 * Contains the message if the user is editing an existing individual or creating a new one.
	 */
	private OutputLabel lblIndividualStatus;
	/**
	 * The individual the user is currently working with. If the user wants to edit an existing individual, 
	 * this object contains the values of this individual.
	 */
	private OntologyIndividual ontologyIndividualInProcess;
	/**
	 * True if the user is editing an individual, false otherwise. This information is important for calling the 
	 * correct method on the server. The attribute is set if the user clicks on a row in the table. 
	 */
	private boolean editingAnIndividual;
	/**
	 * Contains objects if the user can enter data for classes of the Premis ontology. These objects represent the individuals 
	 * of the Premis ontology that the user has created and that have to be send to the server. 
	 */
	private IndividualsFinishedDecoratedList premisIndividualsInProcess;
	
	/**
	 * Constructs a new object. 
	 * @param informationPackageManager Contains important information about the information package that is currently
	 * being created. 
	 * @param ontologyClass The class from the ontologies we are working with. The individual created is assigned to
	 * this class. 
	 * @param informationPackagePath 
	 */
	public UserInputComponentImpl(final InformationPackageManager informationPackageManager, final ServerCache serverCache, 
			final OntologyClass ontologyClass, final PropertyFactory propertyFactory, final DynamicGuiManager dynamicGuiManager) {
		this.informationPackageManager = informationPackageManager;
		this.serverCache = serverCache;
		this.propertyFactory = propertyFactory;
		
		valueInputComponent = new PrimeFacesGuiElementFactory(informationPackageManager, serverCache, dynamicGuiManager).
				createValueInputComponent(ontologyClass);
		this.ontologyClass = ontologyClass;
		initializeStatusLabel();
		initializeSaveButton();
		intializeNewIndividualButton();
		initializeResetButton();
		initializeNorthPanel();
		initializePropertiesPanel();
	}	
	
	/**
	 * Initializes the status label that says if the user is creating a new individual or editing an
	 * existing on.e
	 */
	private void initializeStatusLabel() {
		lblIndividualStatus = PrimeFacesComponentFactory.createOutputLabel();
		lblIndividualStatus.setValue(I18nComponentLabelUtil.getLabelYouAreCreatingNewIndividualString());
		lblIndividualStatus.setStyle(CssUtil.createStyleForStatusLabel());
	}
	
	/**
	 * If the user is editing an existing individual, this individual is set. 
	 * @param ontologyIndividual The individual the user is working with. 
	 */
	public void setIndividual(final OntologyIndividual ontologyIndividual) {
		valueInputComponent.setValuesOfIndividual(ontologyIndividual);
		ontologyIndividualInProcess = ontologyIndividual;
		final String uuidOfIndividual = ontologyIndividual.getUuid().toString();
		lblIndividualStatus.setValue(I18nComponentLabelUtil.getLabelYouAreEditingIndividualString(uuidOfIndividual));
	}
	
	/**
	 * Gets the panel that contains the status label. 
	 * @return The panel that contains the status label.
	 */
	public HtmlPanelGroup getStatusPanel() {
		return pnlNorth;
	}

	/**
	 * Initializes the reset button.
	 */
	private void initializeResetButton() {
		btnReset = PrimeFacesComponentFactory.createCommandButton();
		final String idBtnReset = UniqueIdentifierUtil.createButtonIdentifier(IDENTIFIER_BUTTON_RESET);
		btnReset.setValue(I18nComponentLabelUtil.getButtonResetString());
		btnReset.setId(idBtnReset);
		btnReset.setAjax(true);
		
		final AjaxBehavior clearBehavior = new AjaxBehavior();
		clearBehavior.addAjaxBehaviorListener(new AjaxBehaviorListener() {
			
			@Override
			public void processAjaxBehavior(AjaxBehaviorEvent event) throws AbortProcessingException {
				clearComponents();
			}
		});
		btnReset.addClientBehavior(JsfConstants.AJAX_EVENT_CLICK, clearBehavior);
		btnReset.setUpdate(JsfConstants.AJAX_ALL);
	}
	
	/**
	 * Initializes the button for creating a new individual. 
	 */
	private void intializeNewIndividualButton() {
		btnNewIndividual = PrimeFacesComponentFactory.createCommandButton();
		final String idBtnNewIndividual = UniqueIdentifierUtil.createButtonIdentifier(IDENTIFIER_BUTTON_NEW_INDIVIDUAL);
		btnNewIndividual.setId(idBtnNewIndividual);
		btnNewIndividual.setValue(I18nComponentLabelUtil.getButtonNewIndividualString());
		btnNewIndividual.setAjax(false);
		
		btnNewIndividual.addActionListener(new ActionListener() {
			
			@Override
			public void processAction(ActionEvent event) throws AbortProcessingException {
				clearComponents();
				ontologyIndividualInProcess = null;
				lblIndividualStatus.setValue(I18nComponentLabelUtil.getLabelYouAreCreatingNewIndividualString());
			}
		});
	}

	/**
	 * Initializes the panel that contains the buttons and the status label. 
	 */
	private void initializeNorthPanel() {	
		pnlNorth = new HtmlPanelGroup();
		final PanelGrid inputPanelGrid = (PanelGrid)valueInputComponent.toGuiElement();
		pnlNorth.getChildren().add(lblIndividualStatus);
		pnlNorth.getChildren().add(inputPanelGrid);
		pnlNorth.getChildren().add(btnSave); 
		pnlNorth.getChildren().add(btnReset);
		pnlNorth.getChildren().add(btnNewIndividual);
	}
	
	/**
	 * Initializes the panel that contains the input components for the property values. 
	 */
	private void initializePropertiesPanel() {
		final List<HtmlPanelGroup> valueInputPanelGroupList = getGuiElement(); 
		pnlLiterals = PrimeFacesComponentFactory.createPanelGroup();
		
		for (final HtmlPanelGroup htmlPanelGroup: valueInputPanelGroupList) {
			pnlLiterals.getChildren().add(htmlPanelGroup);
		}
	}

	/**
	 * Initializes the save buttons and its behavior. 
	 */
	private void initializeSaveButton() {
		btnSave = new CommandButton();
		final String idBtnSave = UniqueIdentifierUtil.createButtonIdentifier(IDENTIFIER_BUTTON_SAVE);
		btnSave.setId(idBtnSave);
		btnSave.setValue(I18nComponentLabelUtil.getButtonSaveIndividualString());
		btnSave.addActionListener(new ActionListener() {
					
			@Override
			public void processAction(ActionEvent event) throws AbortProcessingException {
				final PanelGrid literalPanelGrid = (PanelGrid)valueInputComponent.toGuiElement();
				
				if (ontologyIndividualInProcess == null) {
					 ontologyIndividualInProcess = ClientModelFactory.createOntologyIndividual(ontologyClass);
				}
				
				if (premisIndividualsInProcess == null) {
					premisIndividualsInProcess = ClientModelFactory.createEmptyIndividualsFinishedDecoratedList();
				}
				
				for (UIComponent uiComponent: literalPanelGrid.getChildren()) { 
					// This panel grid contains the input components for one property.
					if (uiComponent instanceof UIPanel) {
						final UIPanel uiPanel = (UIPanel)uiComponent;
						final OntologyProperty ontologyProperty = readOntologyProperty(uiPanel); 
						if (ontologyProperty instanceof OntologyDatatypeProperty) {
							saveDatatypeProperty(ontologyProperty);
						} else if (ontologyProperty instanceof OntologyObjectProperty) {
							saveObjectProperty(ontologyProperty);
						}
					}
				}
				
				if (editingAnIndividual) {
					serverCache.updateIndividualInServerAndCache(ontologyIndividualInProcess);
				} else {
					serverCache.saveIndividualInServerAndCache(ontologyIndividualInProcess);
				}
				
				clearComponents();
			}
		});
		btnSave.setAjax(true);
		btnSave.setUpdate(JsfConstants.AJAX_ALL);
	}
	
	/**
	 * Saves the values of a datatype property in an invidiual.
	 * @param ontologyProperty The datatype property that should be saved.
	 */
	private void saveDatatypeProperty(final OntologyProperty ontologyProperty) {
		//final String ontologyPropertyIri = ontologyProperty.getPropertyId();
		// DELETE_ME
		/*if (OntologyUtils.isPremisIri(ontologyPropertyIri)) {
			final Optional<OntologyIndividualFinishedDecorator> optionalWithIndividual = premisIndividualsInProcess.getUnfinishedIndividual();
			OntologyIndividualFinishedDecorator premisIndividual =  null;
			if (!optionalWithIndividual.isPresent()) {
				final OntologyClass ontologyPremisClass = serverCache.getClassByIri(ontologyPropertyIri);
				final OntologyIndividual premisOntologyIndividual = ClientModelFactory.createOntologyIndividual(ontologyPremisClass);
				premisIndividual = ClientModelFactory.createOntologyIndividualFinishedDecorator(premisOntologyIndividual);
			} else {
				premisIndividual = optionalWithIndividual.get();
			}
			premisIndividual.addDatatypeProperty((OntologyDatatypeProperty)ontologyProperty);
		} else {*/
			ontologyIndividualInProcess.addDatatypeProperty((OntologyDatatypeProperty)ontologyProperty);
		//}
	}
	
	/**
	 * Saves the values of an object property in an invidiual.
	 * @param ontologyProperty The object property that should be saved.
	 */
	private void saveObjectProperty(final OntologyProperty ontologyProperty) {
		final String iriOfObjectProperty = ontologyProperty.getPropertyId();
		final OntologyObjectProperty copyOfOntologyProperty = propertyFactory.copyOntologyObjectProperty(
				(OntologyObjectProperty)ontologyProperty);
		final Optional<OntologyIndividualCollection> optionalWithObjectPropertyValues = informationPackageManager.
				getIndividualsOfObjectProperty(iriOfObjectProperty);
		
		optionalWithObjectPropertyValues.ifPresent(objectPropertyValues ->  {
			copyOfOntologyProperty.clearValues();
			for (int i = 0; i < objectPropertyValues.getIndiviualsCount(); i++) {
				final OntologyIndividual ontologyIndividual = objectPropertyValues.getIndividual(i);
				copyOfOntologyProperty.addOntologyIndividual(ontologyIndividual);
			}
		});
		ontologyIndividualInProcess.addObjectProperty(copyOfOntologyProperty);
	}
	
	/**
	 * Deletes all values from the input components.
	 */
	private void clearComponents() {
		final PanelGrid literalPanelGrid = (PanelGrid)valueInputComponent.toGuiElement();
		
		for (final UIComponent uiComponent: literalPanelGrid.getChildren()) { 
			// This panel grid contains the input components for one property.
			if (uiComponent instanceof UIPanel) {
				final UIPanel uiPanel = (UIPanel)uiComponent;
				for (UIComponent component: uiPanel.getChildren()) {
					if (component instanceof UIInput && !(component instanceof HtmlInputHidden)) {
						UIInput input = (UIInput)component;
						input.resetValue();
					}
					
					if (component instanceof DataTable) {
						final DataTable dataTable = (DataTable)component;
						clearIndividualSelection(dataTable);
					}
				}
			}
		}
		ontologyIndividualInProcess = null;
	}
	
	/**
	 * Clears all checkboxes that are used to select individuals. After the method has done its work, no individual
	 * in the table is selected any more.
	 * @param dataTable The data table that contains the individuals. The data in the table must be DataRow objects.
	 */
	private void clearIndividualSelection(final DataTable dataTable) {
		for (int i = 0; i < dataTable.getRowCount(); i++) {
			dataTable.setRowIndex(i);
			final Object dataOfRow = dataTable.getRowData();
			final DataRow dataRow = (DataRow)dataOfRow;
			dataRow.setSelected(false);
		}
	}
	
	/**
	 * Reads the value an user has entered in the input components from the panel with the components and creates
	 * an  ontology property with this value.
	 * @param uiPanel The panel that contains the input components.
	 * @return The ontology property with the read value.
	 */
	private OntologyProperty readOntologyProperty(final UIPanel uiPanel) {
		// The last child is the hidden field with the id of the property.
		int indexLastComponent = uiPanel.getChildCount() - 1;
		UIComponent uiComponent = uiPanel.getChildren().get(indexLastComponent);
		/* 
		 * The component with the references consists of an additional panel grid that contains the table, the
		 * list box with the object properties and the input hidden. 
		 */
		if (uiComponent instanceof PanelGrid) {
			PanelGrid panelGrid = (PanelGrid)uiComponent;
			indexLastComponent = panelGrid.getChildCount() - 1;
			uiComponent = panelGrid.getChildren().get(indexLastComponent);
		}
		final HtmlInputHidden inputHidden = (HtmlInputHidden)uiComponent;
		final String propertyId = (String)inputHidden.getValue();
		final OntologyDatatypeProperty ontologyDatatypeProperty = ontologyClass.getDatatypeProperty(propertyId); 
		
		if (ontologyDatatypeProperty != null) {
			final OntologyDatatypeProperty copyOfOntologyProperty = propertyFactory.copyOntologyDatatypeProperty(ontologyDatatypeProperty);
			if (ontologyDatatypeProperty.isDurationProperty()) {
				final OntologyDuration duration = readDurationFromGui(uiPanel);
				copyOfOntologyProperty.setPropertyValue(duration);
				//}
				// DELETE_ME Dieser Zweig muesste weiter unten im else abgehandelt werden.
			//} 
//			else if (copyOfOntologyProperty.isObjectProperty()){
//				UIComponent uiTargetList = uiPanel.getChildren().get(INDEX_TARGET_LIST);
//				SelectOneListbox targetListbox = (SelectOneListbox)uiTargetList;
//				UISelectItems selectItems = (UISelectItems)targetListbox.getChildren().get(0);
//				List<SelectItem> selectItemList = (List<SelectItem>)selectItems.getValue();
//				OntologyObjectProperty objectProperty = (OntologyObjectProperty)copyOfOntologyProperty;
//				OntologyClass range = objectProperty.getRange();
//				
//				for (int i = 0; i < selectItemList.size(); i++) {
//					SelectItem selectItem = selectItemList.get(i);
//					//OntologyIndividual individual = (OntologyIndividual)selectItem.getValue();
//					Uuid individualsUuid = (Uuid)selectItem.getValue();
//					//System.out.println("blabla");
//					OntologyIndividual ontologyIndividual = serverCache.getIndividual(individualsUuid, 
//							range);
//					objectProperty.addOntologyIndividual(ontologyIndividual);
//				}
			} else {
				/* Any other property than duration property, because they consist of only one
				 * input component. We can simply take their value and put it into the property.
				 */
				final UIInput uiInput = (UIInput)uiPanel.getChildren().get(0);
				final Object inputValue = uiInput.getValue();
				copyOfOntologyProperty.setPropertyValue(inputValue);
			}
			return (OntologyProperty)copyOfOntologyProperty;
		} else {
			return readObjectPropertyFromGui(propertyId);
		}
	}
	
	/**
	 * Reads an object property from the gui. The selected individuals are entered into the object property.
	 * @param propertyId The iri of the object property.
	 * @return An object property that contains the selected individuals. 
	 */
	private OntologyObjectProperty readObjectPropertyFromGui(final String propertyId) {
		final Optional<OntologyIndividualCollection> optionalWithIndividuals = informationPackageManager.
				getIndividualsOfObjectProperty(propertyId);
		final OntologyObjectProperty objectProperty = ontologyClass.getObjectProperty(propertyId);
		
		if (objectProperty != null) {
			optionalWithIndividuals.ifPresent(ontologyIndividuals -> {
				objectProperty.setPropertyValue(ontologyIndividuals);
			});
			
			objectProperty.setPropertyId(propertyId);
		}
		
		return objectProperty;
	}
	
	/**
	 * Reads the values from the components for a duration property.
	 * @param uiPanel Contains the components with the values we want to read.
	 * @return The duration with the values in uiPanel.
	 */
	private OntologyDuration readDurationFromGui(final UIPanel uiPanel) {
		final UIComponent uiYears = uiPanel.getChildren().get(INDEX_INPUT_YEARS); 
		final Spinner spnYears = (Spinner)uiYears;
		int years = 0;
		
		if (isNotEmpty(spnYears.getValue().toString())) {
			years = Integer.parseInt(spnYears.getValue().toString());
		}
		
		final UIComponent uiMonths = uiPanel.getChildren().get(INDEX_INPUT_MONTHS); 
		final Spinner spnMonths = (Spinner)uiMonths;
		
		int months = 0;
		
		if (isNotEmpty(spnMonths.getValue().toString())) {
			months = Integer.parseInt(spnMonths.getValue().toString());
		}
		
		final UIComponent uiDays = uiPanel.getChildren().get(INDEX_INPUT_DAYS); 
		final Spinner spnDays = (Spinner)uiDays;
		int days = 0;
		
		if (isNotEmpty(spnDays.getValue().toString())) {
			days = Integer.parseInt(spnDays.getValue().toString());
		}
		
		final UIComponent uiHours = uiPanel.getChildren().get(INDEX_INPUT_HOURS); 
		final Spinner spnHours = (Spinner)uiHours;
		int hours = 0;
		
		if (isNotEmpty(spnHours.getValue().toString())) {
			hours = Integer.parseInt(spnHours.getValue().toString());
		}
		
		final UIComponent uiMinutes = uiPanel.getChildren().get(INDEX_INPUT_MINUTES); 
		final Spinner spnMinutes = (Spinner)uiMinutes;
		int minutes = 0;
		
		if (isNotEmpty(spnMinutes.getValue().toString())) {
			minutes = Integer.parseInt(spnMinutes.getValue().toString());
		}
		
		final UIComponent uiSeconds = uiPanel.getChildren().get(INDEX_INPUT_SECONDS); 
		final Spinner spnSeconds = (Spinner)uiSeconds;
		int seconds = 0;
		
		if (isNotEmpty(spnSeconds.getValue().toString())) {
			seconds = Integer.parseInt(spnSeconds.getValue().toString());
		}
		
		final UIComponent uiNegative = uiPanel.getChildren().get(INDEX_INPUT_NEGATIVE);
		final SelectBooleanCheckbox chkNegative = (SelectBooleanCheckbox)uiNegative;
		
		boolean negative = false;
		if (chkNegative.getValue() != null) {
			String valueAsString = chkNegative.getValue().toString(); 
			negative = Boolean.parseBoolean(valueAsString);
		}
		
		final OntologyDuration duration = OntologyModelFactory.createOntologyDuration(years, 
				months, days, hours, minutes, seconds, negative);
		return duration;
	}
	
	/**
	 * Creates a gui element.
	 * @return Contains a panel for the status area. 
	 */
	private List<HtmlPanelGroup> getGuiElement() {
		final List<HtmlPanelGroup> panelGroupList = new ArrayList<>();
		panelGroupList.add(pnlNorth);
		return panelGroupList;
	}
	
	/**
	 * Is called if the user wants to edit an individual. 
	 */
	public void setEditingIndividual() {
		editingAnIndividual = true;
	}
}