package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nComponentLabelUtil;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nMessagesUtil;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Reference;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ReferenceCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.InformationPackagePath;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.OntologyModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.factories.PropertyFactory;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.ObjectPropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.utils.StringUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.UserInputComponentImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRow;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.models.classes.UuidList;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * <p>Contains the dynamically created gui for entering the values of the
 * properties of the classes in the base ontology. In the preceeding step 
 * the user has selected some classes. These classes and their subclasses
 * are displayed as a tree in this view. The user can move through the tree.
 * Every class is represented by some input components that correspondent 
 * to the properties of this class.</p>
 * <p>Example: The class A contains a data property xxx that is a string. Then
 * there is a text input field displayed for this property.</p> 
 * <p>The values the user entered are send to the server if the user
 * clicks the save button. The entered values are evaluated by the 
 * reasoner.</p>
 * <p>If the user chooses another class, the class and its properties
 * are requested from the server. Then the input elements are rendered. 
 * As labels for the input elements the labels of the properties are
 * used. If there are not any labels, the name of the property is used
 * as label for the input component.</p> 
 * @author Christopher Olbertz
 *
 */
public class OntologyTreeView implements Serializable {
	// ************* Constants *************
	private static final long serialVersionUID = 6423099914070213743L;
	private static final String IDENTIFIER_PANELGROUP_MAIN = "Main";
	private static final String IDENTIFIER_PANEL_SUPERCLASSES = "Superclasses";
	private static final String WIDGET_VAR_TAXONOMY_DIALOG = "dlgTaxonomy";
	
	// ************* Attributes ************
	/**
	 * The object with the input components for the property values that is shown currently. 
	 */
	private UserInputComponentImpl currentUserComponentImpl;
	/**
	 * Contains the information about the information package that
	 * the user is building. 
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * The root node of the tree that contains the hierarchy of the
	 * classes in the ontology.
	 */
	private TreeNode rootNode;
	/**
	 * A reference to the business logic of the client. 
	 */
	private ServiceFacade serviceFacade;
	/**
	 * The node in the tree that is currently selected.
	 */
	private TreeNode selectedTreeNode;
	/**
	 * Contains the columns the user has selected in a table with individuals.
	 */
	private List<String> selectedColumns;
	/**
	 * Contains the data in a table with individuals.  
	 */
	private List<DataRow> dataRows;
	/**
	 * Contains the columns for the dynamically created table with the individuals. 
	 */
	private Map<String, String> columnMap;
	/**
	 * The row the user has selected in the table with the individuals for editing an individual. 
	 */
	private DataRow selectedDataRow;
	/**
	 * Contains the rows that have been filtered in the table with the individuals. 
	 */
	private List<DataRow> filteredDataRows;
	/**
	 * Contains some important information about the dynamically generated gui. 
	 */
	private DynamicGuiManager dynamicGuiManager;
	/**
	 * Contains the dynamically created gui. 
	 */
	private PanelGrid superclassPanel;
	/**
	 * Contains the cached ontology objects received from the server.
	 */
	private ServerCache serverCache;
	/**
	 * The root component for the dynamically created user interface.
	 */
	private HtmlPanelGroup pnlMain;
	/**
	 * Contains the individuals that are assigned to the class the user is currently
	 * working with. 
	 */
	private List<OntologyIndividual> individualsOfClass;
	/**
	 * The individual the user has selected in the table with the individuals. 
	 */
	private OntologyIndividual selectedIndividual;
	/**
	 * Creates property objects for the properties in an ontology. 
	 */
	private PropertyFactory propertyFactory;
	/**
	 * The uuid of the information package the user is working with.
	 */
	private Uuid uuidOfInformationPackage;
	/**
	 * The package type of the information package the user is working with. It is shown
	 * in the user interface.
	 */
	private PackageType packageType;
	/**
	 * The title of the information package the user is working with. It is shown
	 * in the user interface.
	 */
	private String informationPackageTitle;
	/**
	 * The references of the information package the user is working with. They are shown
	 * in the user interface.
	 */
	private List<String> references;
	/**
	 * The nodes in the taxonomy tree the user has selected. They are related to a certain taxonomy.
	 */
	private TreeNode[] selectedTaxonomyNodes;
	/**
	 * Contains the tree nodes for each taxonomy individual. The individual is the key
	 * and the node is the value. 
	 */
	private Map<TaxonomyIndividual, TreeNode> taxonomyIndividualTreeNodeMap;
	
	// ************ Initialization *********
	/**
	 * Initializes the gui. The ontology is processed and the tree 
	 * with the input elements is constructed.
	 */
	@PostConstruct
	public void initialize() {
		taxonomyIndividualTreeNodeMap = new HashMap<>();
		initializeBasicDataOfInformationPackage();
		initializeComponents();
		createClassesTree();
		individualsOfClass = new ArrayList<>();
		selectedColumns = new ArrayList<>();
	}
	
	/**
	 * Initializes the column of the table with the individuals. 
	 * @param ontologyClass Contains the properties that describe the columns. 
	 */
	private void initializeColumns(OntologyClass ontologyClass) {
		ontologyClass = serviceFacade.getPropertiesOfClass(ontologyClass.getFullName());
		columnMap = new LinkedHashMap<>();
		selectedColumns = new ArrayList<>();
		
		for (int i = 0; i < ontologyClass.getDatatypePropertiesCount(); i++) {
			final OntologyDatatypeProperty ontologyProperty = ontologyClass.getDatatypeProperty(i);
			final String columnTitle = ontologyProperty.getLabel();
			columnMap.put(columnTitle, columnTitle);
		}
		
		selectedColumns.addAll(columnMap.keySet());
	}
	
	/**
	 * Initializes the basic data of an information package. They are displayed above the input
	 * components for the properties.
	 */
	private void initializeBasicDataOfInformationPackage() {
		uuidOfInformationPackage = informationPackageManager.getUuid();
		informationPackageTitle = informationPackageManager.getTitle();
		packageType = informationPackageManager.getInformationPackageType();
		references = new ArrayList<>();
		
		if (informationPackageManager.isEditMode()) {
			ReferenceCollection references = serviceFacade.findReferencesOfInformationPackage();
			informationPackageManager.setReferencesOfInformationPackage(references);
		}
		
		initializeReferences();
	}
	
	/**
	 * Initializes the information about the refences of the information package the user is working with.
	 * They are shown in the upper area of the window. 
	 */
	private void initializeReferences() {
		for (int i = 0; i < informationPackageManager.getReferencesCount(); i++) {
			final Reference reference = informationPackageManager.getReferenceAt(i);
			if (reference.isLocalFileReference()) {
				final String pathOfDigitalObject = reference.getUrl();
				final String fileName = PackagingToolkitFileUtils.extractFileName(pathOfDigitalObject);
				references.add(fileName);
			} else {
				String url = reference.getUrl();
				references.add(url);
			}
		}
	}
	
	// DELETE_ME
	public void onChangeIndividualSelection(AjaxBehaviorEvent event) {
		/*event.
		final HtmlSelectBooleanCheckbox checkbox = (HtmlSelectBooleanCheckbox)event.getComponent();
		final boolean selection = Boolean.parseBoolean(checkbox.getValue().toString());
		if (selection) {o
			System.out.println("************");
		} else {
			System.out.println("############");
		}*/
	}
	
	/**
	 * This listener is called if the has selected or deselected the individuals
	 * for the current information package and wants to save the associations.
	 */
	public void onIndividualsSelectForInformationPackage() {
		final UuidList uuidsOfAssignedIndividuals = PackagingToolkitModelFactory.createEmptyUuidList();
		uuidsOfAssignedIndividuals.addUuidToList(informationPackageManager.getUuid());
		final UuidList uuidsOfUnassignedIndividuals = PackagingToolkitModelFactory.createEmptyUuidList();
		uuidsOfUnassignedIndividuals.addUuidToList(informationPackageManager.getUuid());
		
		for (final DataRow dataRow: dataRows) {
			if (dataRow.isSelected()) {
				final Uuid uuidOfIndividual = dataRow.getUuidOfIndividual();
				uuidsOfAssignedIndividuals.addUuidToList(uuidOfIndividual);
			} else {
				final Uuid uuidOfIndividual = dataRow.getUuidOfIndividual();
				uuidsOfUnassignedIndividuals.addUuidToList(uuidOfIndividual);
			}
		}
		
		serviceFacade.assignIndividualsToInformationPackage(uuidsOfAssignedIndividuals);
		serviceFacade.unassignIndividualsFromInformationPackage(uuidsOfUnassignedIndividuals);
	}
	
	/**
	 * Creates the data of the rows of the table with the individuals. 
	 * @param individualList The individuals we want to see in the table. 
	 */
	private void createRowData(final OntologyIndividualCollection individualList) {
		dataRows = new ArrayList<>();
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual individual = individualList.getIndividual(i); 
			final DataRow dataRow = new DataRow(individual.getDatatypePropertiesCount());
			final Uuid uuidOfCurrentIndividual = individual.getUuid();
			
			final boolean containedInInformationPackage = informationPackageManager.containsIndividual(
					uuidOfCurrentIndividual);
			
			if(containedInInformationPackage) {
				dataRow.select();
			}
			
			dataRow.setUuidOfIndividual(uuidOfCurrentIndividual);
			
			final String[] values = new String[individual.getDatatypePropertiesCount()];
			for (int j = 0; j < individual.getDatatypePropertiesCount(); j++) {
				final OntologyDatatypeProperty datatypeProperty = individual.getDatatypePropertyAt(j);
				final Object value = datatypeProperty.getPropertyValue();
				if (value != null) {
					values[j] = datatypeProperty.getPropertyValue().toString();
					dataRow.addProperty(datatypeProperty.getLabel(), datatypeProperty.getPropertyValue());
				} else {
					values[j] = StringUtils.EMPTY_STRING;
					dataRow.addProperty(datatypeProperty.getLabel(), StringUtil.EMPTY_STRING);
				}
			}
			
			dataRow.setValues(values);
			dataRows.add(dataRow);
		}
	}
	
	/**
	 * Initializes the components. They are instanciated and they get unique 
	 * identifiers.
	 */
	private void initializeComponents() {
		pnlMain = PrimeFacesComponentFactory.createPanelGroup();
		pnlMain.setId(UniqueIdentifierUtil.createPanelIdentifier(IDENTIFIER_PANELGROUP_MAIN));
		superclassPanel = PrimeFacesComponentFactory.createPanelGrid();
		superclassPanel.setId(UniqueIdentifierUtil.createPanelIdentifier(IDENTIFIER_PANEL_SUPERCLASSES));
	}
	
	/**
	 * Runs through the ontology and creates the tree with the ontology classes
	 * and the input components. This method is the starting point for the recursion
	 * that builds the tree.
	 */
	private void createClassesTree() {
		rootNode = new DefaultTreeNode();
		
		for (int i = 0; i < informationPackageManager.getFilterClassesCount(); i++) {
			final OntologyClass ontologyClass = informationPackageManager.getFilterClassAt(i);
			final TreeNode treeNode = new DefaultTreeNode(ontologyClass);
			rootNode.getChildren().add(treeNode);
			createVisualTree(ontologyClass, treeNode);
		}
	}
	
	/**
	 * Fires when a node is selected. Then there are the input components showed 
	 * on the right side of the window if there are any properties in the class 
	 * that is contained in the selected node. Only leaf nodes can contain
	 * properties. 
	 * <br >
	 * In the background the object of {@link de.feu.kdmp4.packagingtoolkit.client.managers.
	 * InformationPackageManager} is extended by objects of   
	 * {@link de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.
	 * InformationPackagePath}. These objects represent the paths to the classes
	 * through the ontology tree. They are stored in a map in informationPackageManager
	 * and they are assigned to the information the user enters for the properties
	 * of a given class.
	 * @param nodeSelectEvent
	 */
	public void onNodeSelect(NodeSelectEvent nodeSelectEvent) {
		selectedTreeNode = nodeSelectEvent.getTreeNode();
		final OntologyClass ontologyClass = (OntologyClass)selectedTreeNode.getData();
		createPropertiesPanel(ontologyClass);
	}
	
	/**
	 * Creates the panel that contains the input components for the properties.  
	 * @param ontologyClass Contains the properties. 
	 */
	private void createPropertiesPanel(OntologyClass ontologyClass) {
		if (ontologyClass != null) {
			
			final boolean isTaxonomyStartClass = serverCache.isTaxonomyStartClass(ontologyClass);
			
			final DatatypePropertyCollection dataypeProperties  = serverCache.getDatatypePropertiesListByClassname(ontologyClass.getFullName()); 
			if (!ontologyClass.hasDatatypeProperties()) {
				ontologyClass.addDatatypeProperties(dataypeProperties);
			}
			
			final ObjectPropertyCollection objectProperties = serverCache.getObjectPropertiesByClassname(ontologyClass.getFullName());
			if (!ontologyClass.hasObjectProperties()) {
				ontologyClass.addObjectProperties(objectProperties);
			}
			
			if (isTaxonomyStartClass) {
				OntologyObjectProperty hasTaxonomyProperty = propertyFactory.createHasTaxonomyProperty();
				ontologyClass.addObjectProperty(hasTaxonomyProperty);
				/*final OntologyIndividualList individualsOfStartClass = serverCache.getIndividualsByClass(ontologyClass);
				final OntologyIndividual individualOfStartClass = individualsOfStartClass.getIndividual(0);
				final GuiElement taxonomyGuiElement = createTaxonomyGuiElement(individualOfStartClass);
				containerGuiElement.addChildElement(taxonomyGuiElement);*/
			} 
			
			// Reaction on the event is necessary only if there are properties in this class.
			if (ontologyClass.hasDatatypeProperties() || ontologyClass.hasObjectProperties() || ontologyClass.isDigitalObjectClass()) {
				// Determine the path of the component in the tree.
				TreeNode node = selectedTreeNode.getParent();
				/* We build a path object that represents the path to our information
				 * in the ontology tree.
				 */
				final InformationPackagePath informationPackagePath = OntologyModelFactory.createInformationPackagePath();
				// The first component of the path is the selected node itself.
				informationPackagePath.addPathComponent(selectedTreeNode.getData().toString());
				
				// Now the parents of the selected node are added to the path.
				while (node != null && node.getData() != null) {
					informationPackagePath.addPathComponent(node.getData().toString());
					node = node.getParent();
				}
	
				informationPackageManager.addInformationPackageComponent(informationPackagePath);
				// Only if an existing information package should be modified, the individuals have to requested from the server.
				if (informationPackageManager.isEditMode()) {
					 if (informationPackageManager.haveIndividualsToBeRequested(informationPackagePath)) {
						 final OntologyIndividualCollection ontologyIndividuals = serviceFacade.getSavedIndividualsByClass(ontologyClass);
						informationPackageManager.addIndividualsToInformationPackageComponent(ontologyIndividuals, informationPackagePath);
					}
				}
				
				currentUserComponentImpl = new UserInputComponentImpl(
						informationPackageManager, serverCache, ontologyClass, propertyFactory, dynamicGuiManager);

				pnlMain.getChildren().clear();
				final HtmlPanelGroup panelGroup = currentUserComponentImpl.getStatusPanel(); 
				pnlMain.getChildren().add(panelGroup);
				createClassesTree();
				createIndividualTable(ontologyClass);
			} else {
				final OutputLabel outputLabel = new OutputLabel();
				outputLabel.setValue(I18nComponentLabelUtil.getLabelNoValues());
				if (dataRows != null) {
					dataRows.clear();
				}
				pnlMain.getChildren().clear();
				pnlMain.getChildren().add(outputLabel);
			}
		}
	}
	
	/**
	 * Creates the table with the individuals where the user can select an individual for editing. 
	 * @param ontologyClass The class the individuals are assigned to. 
	 */
	private void createIndividualTable(final OntologyClass ontologyClass)  {
		initializeColumns(ontologyClass);
		final OntologyIndividualCollection ontologyIndividuals = serverCache.getIndividualsByClass(ontologyClass);
		individualsOfClass = ontologyIndividuals.toList();
		createRowData(ontologyIndividuals);
		
		for (int i = 0; i < ontologyIndividuals.getIndiviualsCount(); i++) {
			final OntologyIndividual individual = ontologyIndividuals.getIndividual(i);
			individualsOfClass.add(individual);
		}
	}
	
	/**
	 * Saves an information package. 
	 */
	public void saveInformationPackage() {
		serviceFacade.saveCurrentInformationPackage();
		informationPackageManager.clearData();
		informationPackageManager.setNewInformationPackage(true);
		final String title = I18nMessagesUtil.getSuccessString();
		final String text = I18nMessagesUtil.getInformationPackageSavedString();
		JsfUtil.addFacesMessageToContext(title, text);
	}
	
	/**
	 * Traverses recursively through the tree of ontology classes and creates the tree
	 * structure that is displayed in the gui.
	 * @param parentClass The parent class for the recursion step.
	 * @param parentNode The node that is the parent for the nodes that are
	 * created in this step.
	 */
	private void createVisualTree(final OntologyClass parentClass, final TreeNode parentNode) {
		for (int i = 0; i < parentClass.getSubclassCount(); i++) {
			final OntologyClass ontologyClass = parentClass.getSubClassAt(i);
			final TreeNode nextTreeNode = new DefaultTreeNode(ontologyClass);
			createVisualTree(ontologyClass, nextTreeNode);
			parentNode.getChildren().add(nextTreeNode);
		}
	}
	
	public HtmlPanelGroup getPnlMain() {
		return pnlMain;
	}

	public void setPnlMain(HtmlPanelGroup pnlMain) {
		this.pnlMain = pnlMain;
	}

	public TreeNode getRootNode() {
		return rootNode;
	}

	public void setRootNode(TreeNode rootNode) {
		this.rootNode = rootNode;
	}

	/**
	 * Is called if the user select a row in the table with the individuals.  The input components are filled
	 * with the property values of this individual because the user wants to edit it. 
	 * @param selectEvent
	 */
	public void onRowSelect(SelectEvent selectEvent) {
		final DataRow selectedRow = (DataRow)selectEvent.getObject();
		final Uuid uuidOfSelectedIndividual = selectedRow.getUuidOfIndividual();
		final OntologyIndividual selectedIndividual = findIndividual(uuidOfSelectedIndividual);
		currentUserComponentImpl.setIndividual(selectedIndividual);
		currentUserComponentImpl.setEditingIndividual();
	}
	
	/**
	 * Is called if the user has selected some individuals of a taxonomy in the dialog with the taxonomy tree and wants 
	 * to save them.
	 */
	public void onApplySelectedTaxonomyNodes(TreeNode[] selectedTaxonomyNodes)  {
		final TaxonomyIndividualCollection selectedTaxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		for (final TreeNode treeNode: selectedTaxonomyNodes) {
			final TaxonomyIndividual selectedIndividual = (TaxonomyIndividual) treeNode.getData();
			selectedTaxonomyIndividuals.addTaxonomyIndividual(selectedIndividual); 
		}
		
		final Iri iriOfInformationPackage = informationPackageManager.getIriOfInformationPackage();
		serviceFacade.assignTaxonomyIndividuals(selectedTaxonomyIndividuals, iriOfInformationPackage);
		//informationPackageManager.addSelectIndividualsOfCurrentTaxonomy(selectedTaxonomyIndividuals);
		PrimeFacesUtils.hideDialog(WIDGET_VAR_TAXONOMY_DIALOG);
		informationPackageManager.deselectedCurrentTaxonomy();
	}
	
	/**
	 * Finds an individual in the table with the individuals with the help from its uuid.
	 * @param uuid The uuid of the individual we are looking for. 
	 * @return The found individual.
	 */
	private OntologyIndividual findIndividual(final Uuid uuid) {
		for (final OntologyIndividual ontologyIndividual: individualsOfClass) {
			if (ontologyIndividual.getUuid().equals(uuid)) {
				return ontologyIndividual;
			}
		}
		
		return null;
	}

	public List<OntologyIndividual> getIndividualsOfClass() {
		return individualsOfClass;
	}

	public void setIndividualsOfClass(List<OntologyIndividual> individualsOfClass) {
		this.individualsOfClass = individualsOfClass;
	}

	public OntologyIndividual getSelectedIndividual() {
		return selectedIndividual;
	}

	public void setSelectedIndividual(OntologyIndividual selectedIndividual) {
		this.selectedIndividual = selectedIndividual;
	}

	public void setInformationPackageManager(InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}

	public void setServiceFacade(ServiceFacade serviceFacade) {
		this.serviceFacade = serviceFacade;
	}

	public void setServerCache(ServerCache serverCache) {
		this.serverCache = serverCache;
	}
	
	public Uuid getUuidOfInformationPackage() {
		return uuidOfInformationPackage;
	}

	public void setUuidOfInformationPackage(Uuid uuidOfInformationPackage) {
		this.uuidOfInformationPackage = uuidOfInformationPackage;
	}

	public PackageType getPackageType() {
		return packageType;
	}

	public void setPackageType(PackageType packageType) {
		this.packageType = packageType;
	}

	public String getInformationPackageTitle() {
		return informationPackageTitle;
	}

	public void setInformationPackageTitle(String informationPackageTitle) {
		this.informationPackageTitle = informationPackageTitle;
	}
	
	public List<String> getReferences() {
		return references;
	}
	
	public List<String> getSelectedColumns() {
		return selectedColumns;
	}

	public void setSelectedColumns(List<String> selectedColumns) {
		this.selectedColumns = selectedColumns;
	}

	public List<DataRow> getDataRows() {
		if (selectedTreeNode != null) {
			OntologyClass ontologClass = (OntologyClass)selectedTreeNode.getData();
			createIndividualTable(ontologClass);
		}
		return dataRows;
	}

	public void setDataRows(List<DataRow> dataRows) {
		this.dataRows = dataRows;
	}

	public Map<String, String> getColumnMap() {
		return columnMap;
	}

	public void setColumnMap(Map<String, String> columnMap) {
		this.columnMap = columnMap;
	}

	public void setSelectedDataRow(DataRow selectedDataRow) {
		this.selectedDataRow = selectedDataRow;
	}
	
	public DataRow getSelectedDataRow() {
		return selectedDataRow;
	}
	
	public List<DataRow> getFilteredDataRows() {
		return filteredDataRows;
	}
	
	public void setFilteredDataRows(List<DataRow> filteredDataRows) {
		this.filteredDataRows = filteredDataRows;
	}

	public TreeNode getSelectedTreeNode() {
		return selectedTreeNode;
	}
	
	public void setPropertyFactory(PropertyFactory propertyFactory) {
		this.propertyFactory = propertyFactory;
	}
	
	public void setDynamicGuiManager(DynamicGuiManager dynamicGuiManager) {
		this.dynamicGuiManager = dynamicGuiManager;
	}
	
	public TreeNode[] getSelectedTaxonomyNodes() {
		return selectedTaxonomyNodes;
	}
	
	public void setSelectedTaxonomyNodes(TreeNode[] selectedTaxonomyNodes) {
		this.selectedTaxonomyNodes = selectedTaxonomyNodes;
	}

	/**
	 * Reads the taxonomy the user has selected ad creates the tree component for displaying the
	 * taxonomy. 
	 * @return The root node of the taxonomy that contains the top concepts.
	 */
	public TreeNode getTaxonomyRootNode() {
		final Taxonomy taxonomy = informationPackageManager.getCurrentTaxonomy();
		if (taxonomy != null) {
			final TreeNode rootNode = initializeTaxonomyTree(taxonomy);
			
			final Optional<TaxonomyIndividualCollection> optionalWithTaxonomyIndividuals = informationPackageManager.
					findSelectedIndividualsByTaxonomy(taxonomy);
			
			optionalWithTaxonomyIndividuals.ifPresent(taxonomyIndividuals -> {
				final int taxonomyIndividualCount = taxonomyIndividuals.getSize();
				selectedTaxonomyNodes = new TreeNode[taxonomyIndividualCount];
				for (int i = 0; i < taxonomyIndividuals.getSize(); i++) {
					final int counterForArray = i;
					final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = taxonomyIndividuals.getTaxonomyIndividual(i);
					optionalWithTaxonomyIndividual.ifPresent(taxonomyIndividual -> {
						selectedTaxonomyNodes[counterForArray] = taxonomyIndividualTreeNodeMap.get(taxonomyIndividual);
						selectedTaxonomyNodes[counterForArray].setSelected(true);
					});
				}
				
			});
			
			return rootNode;
		}
		
		return new DefaultTreeNode();
	}
	
	/**
	 * Initializes the tree for  the graphical user interface. 
	 */
	private TreeNode initializeTaxonomyTree(final Taxonomy taxonomy) {
		final TaxonomyIndividual rootIndividual = taxonomy.getRootIndividual();
		final TreeNode rootNode = new DefaultTreeNode(rootIndividual, null); 
		taxonomyIndividualTreeNodeMap.put(rootIndividual, rootNode);

		while (rootIndividual.hasNextNarrower()) {
			final Optional<TaxonomyIndividual> optionalWithNarrower = rootIndividual.getNextNarrower();
			if (optionalWithNarrower.isPresent()) {
				final TaxonomyIndividual narrower = optionalWithNarrower.get();
				addNarrowerToNode(narrower, rootNode);
			}
		}
		
		return rootNode;
	}
	
	/**
	 * Adds a narrower of an individual to the taxonomy tree.
	 * @param narrowerIndividual The individual that should be inserted as child of treeNode.
	 * @param broaderNode The node that contains the individual narrowerIndividual is a narrower of. 
	 */
	private void addNarrowerToNode(final TaxonomyIndividual narrowerIndividual, final TreeNode broaderNode) {
		final TreeNode newTreeNode = new DefaultTreeNode(narrowerIndividual, broaderNode);
		taxonomyIndividualTreeNodeMap.put(narrowerIndividual, newTreeNode);
		
		while (narrowerIndividual.hasNextNarrower()) {
			final Optional<TaxonomyIndividual> optionalWithNarrower = narrowerIndividual.getNextNarrower();
			if (optionalWithNarrower.isPresent()) {
				final TaxonomyIndividual narrower = optionalWithNarrower.get();
				addNarrowerToNode(narrower, newTreeNode);
			}
		}
	}
}
