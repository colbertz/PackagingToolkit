package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import java.time.LocalTime;
import java.util.Date;
import java.util.Locale;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.outputlabel.OutputLabel;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.TimeGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PackagingToolkitClientConstants;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

/**
 * Creates a component for a time value as a Calendar from PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class TimeGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements TimeGuiElement {
	/**
	 * This components represents the value. 
	 */
	private Calendar calendar;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	
	/**
	 * Creates a new component object.
	 * @param ontologyTimeProperty The property that should be processed with this component.
	 */
	public TimeGuiElementPrimeFacesImpl(final OntologyTimeProperty ontologyTimeProperty) {
		super(ontologyTimeProperty);
		final String labelText = ontologyTimeProperty.getLabel();
		final LocalTime value = ontologyTimeProperty.getPropertyValue();
		
		initializeCalendar(labelText, value);
		initializePanelGroup();
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(calendar);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	/**
	 * Initializes the calendar. 
	 * @param labelText Is used for creating a identifier for this component.
	 * @param localTime The value that should be displayed in this component.
	 */
	private void initializeCalendar(final String labelText, final LocalTime localTime) {
		calendar = PrimeFacesComponentFactory.createCalendar();
		final String idCalendar = UniqueIdentifierUtil.createCalendarIdentifier(labelText);
		calendar.setId(idCalendar);
		calendar.setTimeOnly(true);
		calendar.setPattern(PackagingToolkitClientConstants.DEFAULT_PATTERN_TIME);
		final Locale currentLocale = Locale.getDefault();
		calendar.setLocale(currentLocale);
		if (localTime != null) {
			Date value = DateTimeUtils.convertLocalTimeToDate(localTime);
			calendar.setValue(value);
		}
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
