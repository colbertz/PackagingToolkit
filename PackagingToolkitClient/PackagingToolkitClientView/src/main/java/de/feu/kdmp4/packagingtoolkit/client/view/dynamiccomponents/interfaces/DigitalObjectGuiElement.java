package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.outputlabel.OutputLabel;

public interface DigitalObjectGuiElement  extends GuiElement {

	OutputLabel getLabel();

	HtmlPanelGroup getGuiElement();

}
