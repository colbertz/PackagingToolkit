package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils.asInputComponent;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils.isInputComponent;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils.isPanelComponent;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils.isTableComponent;
import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.panelgrid.PanelGrid;
import org.springframework.beans.factory.annotation.Autowired;

import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.AbstractGuiElementFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesGuiElementFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ValueInputComponent;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRow;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRowTableModel;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

/**
 * Contains the dynamically created components for entering data. 
 * @author Christopher Olbertz
 *
 */
public class ValueInputComponentPrimeFacesImpl implements ValueInputComponent {
	/**
	 * The number of columns of the main panel grid. In left column there is the label and in the
	 * right column are the input components.
	 */
	private static final int COLUMN_COUNT = 2;
	/**
	 * The panel grid that contains the control elements for entering data
	 * and the labels that describe the input elements.
	 */
	private PanelGrid mainPanelGrid;
	/**
	 * Creates the components for a certain framework .
	 */
	@Autowired
	private AbstractGuiElementFactory guiElementFactory;
	/**
	 * Contains some important information about the user interface.
	 */
	private DynamicGuiManager dynamicGuiManager;
	private InformationPackageManager informationPackageManager;
	
	/**
	 * The constructor generates the control elements.
	 * @param ontologyClass The ontology class for whose properties control
	 * elements should be rendered.
	 */
	public ValueInputComponentPrimeFacesImpl(final OntologyClass ontologyClass, 
			final InformationPackageManager informationPackageManager, final ServerCache serverCache, 
			final DynamicGuiManager dynamicGuiManager) {
		this.dynamicGuiManager = dynamicGuiManager;
		this.informationPackageManager = informationPackageManager;
		guiElementFactory = new PrimeFacesGuiElementFactory(informationPackageManager, serverCache, dynamicGuiManager);
		ContainerGuiElementPrimefaces containerGuiElement = null;
		mainPanelGrid = PrimeFacesComponentFactory.createPanelGrid();
		
		if (ontologyClass.isDigitalObjectClass()) {
			containerGuiElement = (ContainerGuiElementPrimefaces)
					guiElementFactory.createGuiElementsForDigitalObject(informationPackageManager);
		} else {
			containerGuiElement = (ContainerGuiElementPrimefaces)
					guiElementFactory.createContainerElement(ontologyClass, serverCache);	
		}
		mainPanelGrid = containerGuiElement.toGuiElement();
		mainPanelGrid.setColumns(COLUMN_COUNT);
	}
	
	/**
	 * If the user wants to edit an existing individual, its values are entered into the components. 
	 */
	@Override
	public void setValuesOfIndividual(final OntologyIndividual ontologyIndividual) {
		for(final UIComponent component: mainPanelGrid.getChildren()) {
			if (isPanelComponent(component)) {
				for (final UIComponent componentInPanel: component.getChildren()) {
					determineInputComponentAndWritePropertyValue(componentInPanel, ontologyIndividual);
				}
			}
		}
	}
	
	/**
	 * Determines if a component is an input component. If so, the property of the individual is searched that 
	 * corresponds to this input component and then its value is written into the component.
	 * @param componentInPanel The component that is checked.
	 * @param ontologyIndividual The individual that contains the properties whose values should be entered
	 * into the input components.
	 */
	private void determineInputComponentAndWritePropertyValue(final UIComponent componentInPanel, 
			final OntologyIndividual ontologyIndividual) {
		if (isInputComponent(componentInPanel)) {
			final UIInput input = asInputComponent(componentInPanel);
			for (int i = 0; i < ontologyIndividual.getDatatypePropertiesCount(); i++) {
				final OntologyDatatypeProperty datatypeProperty = ontologyIndividual.getDatatypePropertyAt(i);
				writeDatatypePropertyValueInInputComponent(input, datatypeProperty);
			}
		} else if (isTableComponent(componentInPanel)) {
			for (int i = 0; i < ontologyIndividual.getObjectPropertiesCount(); i++) {
				final OntologyObjectProperty ontologyObjectProperty = ontologyIndividual.getObjectPropertyAt(i);
				final DataTable dataTable = (DataTable)componentInPanel;
				writeObjectPropertyValueInInputComponent(dataTable, ontologyObjectProperty);
			}
		}
	}
	
	/**
	 * Writes the values of an object property into a table. The values of the property are individuals. 
	 * @param dataTable The table that should contain the individuals.
	 * @param ontologyObjectProperty Contains the individuals as value. 
	 */
	private void writeObjectPropertyValueInInputComponent(final DataTable dataTable, 
			final OntologyObjectProperty ontologyObjectProperty) {
		final String iriOfOntologyClass = ontologyObjectProperty.getPropertyRange();
		final DataRowTableModel dataModel = dynamicGuiManager.getDataModel(iriOfOntologyClass);
		clearObjectPropertySelection(dataModel);
		
		final int individualsCount = ontologyObjectProperty.getPropertyValue().getIndiviualsCount(); 
		for (int i = 0; i < individualsCount; i++) {
			final OntologyIndividual individual = ontologyObjectProperty.getPropertyValue().getIndividual(i);
			final Iri iriOfIndividual = individual.getIriOfIndividual();
			final String uuidOfIndividual = iriOfIndividual.getLocalName().getLocalName();
			
			for (int j = 0; j < dataModel.getRowCount(); j++) {
				final DataRow dataRow = dataModel.getRowData(j);
				final String uuidOfIndividualInDataModel = dataRow.getUuidOfIndividual().toString();
				if (areStringsEqual(uuidOfIndividual, uuidOfIndividualInDataModel)) {
					dataRow.setSelected(true);
					informationPackageManager.addObjectPropertyValue(ontologyObjectProperty.getPropertyId(), individual);
				}
			}
		}
	}
	
	private void clearObjectPropertySelection(DataRowTableModel dataRowTableModel) {
		for (int i = 0; i < dataRowTableModel.getRowCount(); i++) {
			final DataRow dataRow = dataRowTableModel.getRowData(i);
			dataRow.setSelected(false);
		}
	}
	
	/**
	 * Writes the value of a property in the corresponding input component. The input component is determined by
	 * its id. The label of the property must be contained in the id of the component. If so the correct input component
	 * is found and the value of the property can be entered.
	 * @param input The input component that can be the correct component for this property.
	 * @param ontologyDatatypeProperty The property that contains the value for the input component.
	 */
	private void writeDatatypePropertyValueInInputComponent(final UIInput input, final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final String labelOfProperty = ontologyDatatypeProperty.getLabel();
		final String idOfComponent = input.getClientId();
		if (idOfComponent.contains(labelOfProperty)) {
			if (ontologyDatatypeProperty.isDateProperty()) {
				writeDatePropertyInComponents(input, ontologyDatatypeProperty);
			} else if (ontologyDatatypeProperty.isDateTimeProperty()) {
				writeDateTimePropertyInComponents(input, ontologyDatatypeProperty);
			} else if (ontologyDatatypeProperty.isTimeProperty()) {
				writeTimePropertyInComponents(input, ontologyDatatypeProperty);
			} else {
				input.setValue(ontologyDatatypeProperty.getPropertyValue());
			}
		} else {
			// Now we have a duration property. It consists of several components. But let's check that for security reasons.
			if (ontologyDatatypeProperty.isDurationProperty()) { 
				writeDurationPropertyInComponents(input, ontologyDatatypeProperty);
			}
		}
	}
	
	/**
	 * Reads the values of a date property and puts them into the user interface.
	 * @param input The component we want to put a value in. 
	 * @param ontologyDatatypeProperty The date property. 
	 */
	private void writeDatePropertyInComponents(final UIInput input, final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final OntologyDateProperty ontologyDateProperty = (OntologyDateProperty)ontologyDatatypeProperty;
		final LocalDate localDateValue = ontologyDateProperty.getPropertyValue();
		if (localDateValue != null) {
			Date dateValue = DateTimeUtils.convertLocalDateToDate(localDateValue);
			input.setValue(dateValue);
		}
	}
	
	/**
	 * Reads the values of a time property and puts them into the user interface.
	 * @param input The component we want to put a value in. 
	 * @param ontologyDatatypeProperty The time property. 
	 */
	private void writeTimePropertyInComponents(final UIInput input, final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final OntologyTimeProperty ontologyTimeProperty = (OntologyTimeProperty)ontologyDatatypeProperty;
		final LocalTime localTimeValue = ontologyTimeProperty.getPropertyValue();
		if (localTimeValue != null) {
			final Date dateValue = DateTimeUtils.convertLocalTimeToDate(localTimeValue);
			input.setValue(dateValue);
		}
	}
	
	/**
	 * Reads the values of a datetime property and puts them into the user interface.
	 * @param input The component we want to put a value in. 
	 * @param ontologyDatatypeProperty The datetime property. 
	 */
	private void writeDateTimePropertyInComponents(final UIInput input, final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final OntologyDateTimeProperty ontologyDateTimeProperty = (OntologyDateTimeProperty)ontologyDatatypeProperty;
		final LocalDateTime localDateTimeValue = ontologyDateTimeProperty.getPropertyValue();
		if (localDateTimeValue != null) {
			final Date dateValue = DateTimeUtils.convertLocalDateTimeToDate(localDateTimeValue);
			input.setValue(dateValue);
		}
	}
	
	/**
	 * Reads the values of a duration property and puts them into the user interface.
	 * @param input The component we want to put a value in. 
	 * @param ontologyDatatypeProperty The duration property. 
	 */
	private void writeDurationPropertyInComponents(final UIInput input, final OntologyDatatypeProperty ontologyDatatypeProperty) {
		final OntologyDurationProperty ontologyDurationProperty = (OntologyDurationProperty)ontologyDatatypeProperty;
		final OntologyDuration ontologyDuration = ontologyDurationProperty.getPropertyValue();
		if (input.getClientId().contains(PackagingToolkitConstants.IDENTIFIER_DAY)) {
			input.setValue(ontologyDuration.getDays());
		} else if (input.getClientId().contains(PackagingToolkitConstants.IDENTIFIER_HOUR)) {
			input.setValue(ontologyDuration.getHours());
		} else if (input.getClientId().contains(PackagingToolkitConstants.IDENTIFIER_MINUTE)) {
			input.setValue(ontologyDuration.getMinutes());
		} else if (input.getClientId().contains(PackagingToolkitConstants.IDENTIFIER_MONTH)) {
			input.setValue(ontologyDuration.getMonths());
		} else if (input.getClientId().contains(PackagingToolkitConstants.IDENTIFIER_NEGATIVE)) {
			input.setValue(ontologyDuration.isNegativeDuration());
		} else if (input.getClientId().contains(PackagingToolkitConstants.IDENTIFIER_SECOND)) {
			input.setValue(ontologyDuration.getSeconds());
		} else if (input.getClientId().contains(PackagingToolkitConstants.IDENTIFIER_YEAR)) {
			input.setValue(ontologyDuration.getYears());
		}
	}
	
	@Override
	public PanelGrid toGuiElement() {
		return mainPanelGrid;
	}
}
