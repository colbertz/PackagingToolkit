/**
 * Contains the implementations of the components for the user interface with the 
 * framework PrimeFaces.
 */

package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;