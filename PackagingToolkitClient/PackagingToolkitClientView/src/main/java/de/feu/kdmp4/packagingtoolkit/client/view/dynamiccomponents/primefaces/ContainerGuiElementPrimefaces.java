package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputHidden;

import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ContainerGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.GuiElement;

public class ContainerGuiElementPrimefaces implements ContainerGuiElement {
	/**
	 * Contains the components.
	 */
	private PanelGrid panelGrid;
	/**
	 * Contains the description of the components in this container.
	 */
	private OutputLabel outputLabel;
	
	/**
	 * Creates a new component object.
	 * @param ontologyClass Contains the properties that are displayed in the children of this
	 * container component.
	 */
	public ContainerGuiElementPrimefaces(OntologyClass ontologyClass) {
		assert ontologyClass != null: "The container element may not be null!";
		panelGrid = PrimeFacesComponentFactory.createPanelGrid();
	}
	
	/**
	 * Creates a new empty component object.
	 */
	public ContainerGuiElementPrimefaces() {
		panelGrid = PrimeFacesComponentFactory.createPanelGrid();
	}
	
	@Override
	public PanelGrid toGuiElement() {
		return panelGrid;
	}

	@Override
	public void addChildElement(GuiElement childElement) {
		if (childElement != null)  {
			UIComponent primeFacesElement = (UIComponent)childElement.getGuiElement();
			outputLabel = (OutputLabel)childElement.getLabel();
			panelGrid.getChildren().add(outputLabel);
			panelGrid.getChildren().add(primeFacesElement);
		}
	}

	@Override
	public Object getLabel() {
		return outputLabel;
	}

	public boolean labelContains(String aString) {
		final String labelValue = outputLabel.getValue().toString(); 
		if (labelValue.contains(aString)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object getGuiElement() {
		return panelGrid;
	}

	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return PrimeFacesComponentFactory.createInputHidden();
	}
}
