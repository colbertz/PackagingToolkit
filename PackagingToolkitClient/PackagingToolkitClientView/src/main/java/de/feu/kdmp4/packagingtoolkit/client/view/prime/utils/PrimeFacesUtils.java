package de.feu.kdmp4.packagingtoolkit.client.view.prime.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIPanel;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Contains methods, that simplify the work with PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class PrimeFacesUtils {
	/**
	 * Copies an uploaded file to the temp directory of the client.
	 * @param uploadedFile The file that should be uploaded. 
	 * @throws FileAlreadyExistsException Is thrown if the file that should be uploaded
	 * already exists in the temporary directory.
	 * @returns The name of the file in the temp directory.
	 */
	public static final String copyUploadedFileToTempDir(final UploadedFile uploadedFile) {
		if (uploadedFile != null) {
			String filename = "";
			InputStream input = null;
			OutputStream output = null;

			filename = WebAppUtils.getClientTempDir();
			filename = filename + File.separator + FilenameUtils.getName(uploadedFile.getFileName());
			File file = new File(filename);
			
			if (file.exists()) {
				/*throw new FileAlreadyUploadedException(filename, CLASSNAME, 
						METHOD_COPY_UPLOADED_FILE_TO_TEMP_DIR, 
						PackagingToolkitComponent.CLIENT);*/
			}
			
			try {
				input = uploadedFile.getInputstream();
			    output = new FileOutputStream(new File(filename));
			    IOUtils.copy(input, output);
			    return filename;
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			} finally {
		        IOUtils.closeQuietly(input);
		        IOUtils.closeQuietly(output);
		    }
		}
		
		return null;
	}
	
	/**
	 * Deletes a file from the temporary directory on the client.
	 * @param filename The name of the file that should be deleted.
	 * @return The absolute path of the deleted file.
	 */
	public static String deleteFileFromTempDir(final String filename) {
		final File file = new File(WebAppUtils.getClientTempDir() + File.separator + filename);
		final String filepath = file.getAbsolutePath(); 
		file.delete();
		return filepath;
	}
	
	/**
	 * Shows a PrimeFaces dialog. 
	 * @param widgetVar The name of the dialog used in the xhtml file.
	 */
	public static final void showDialog(final String widgetVar) {
		final RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('" + widgetVar + "').show();");
	}
	
	/**
	 * Hides a PrimeFaces dialog. 
	 * @param widgetVar The name of the dialog used in the xhtml file.
	 */
	public static final void hideDialog(final String widgetVar) {
		final RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('" + widgetVar + "').hide();");
	}
	
	/**
	 * Changes the locale. 
	 * @param locale The new Locale.
	 */
	public static void setLocale(Locale locale) {
		JsfUtil.getFacesContext().getViewRoot().setLocale(locale);
	}
	
	/**
	 * Determines the locale that is currently set. 
	 * @return The current locale.
	 */
	public static Locale getCurrentLocale() {
		return JsfUtil.getFacesContext().getViewRoot().getLocale();
	}
	
	/**
	 * Gets the tag of a language. 
	 * @return The tag of a language. 
	 */
	public static String getLanguageTag() {
		return JsfUtil.getFacesContext().getViewRoot().getLocale().toLanguageTag();
	}
	
	/**
	 * Checks if a given PrimeFaces component is an input component.
	 * @param component The component to check.
	 * @return True if component is an input component, else otherwise.
	 */
	public static boolean isInputComponent(UIComponent component) {
		return component instanceof UIInput;
	}
	
	/**
	 * Checks if a given PrimeFaces component is a data table component.
	 * @param component The component to check.
	 * @return True if component is a data table component, else otherwise.
	 */
	public static boolean isTableComponent(UIComponent component) {
		return component instanceof DataTable;
	}
	
	/**
	 * Checks if a given PrimeFaces component is a UIPanel.
	 * @param component The component to check.
	 * @return True if component is a panel component, else otherwise.
	 */
	public static boolean isPanelComponent(UIComponent component) {
		return component instanceof UIPanel;
	}
	
	/**
	 * Casts an UIComponent to an UIInput.
	 * @param component The component to cast.
	 * @return The component as UIInput. Returns null if the cast is not possible.
	 */
	public static UIInput asInputComponent(UIComponent component) {
		if (isInputComponent(component)) {
			return (UIInput)component;
		}
		return null;
	}
	
	/**
	 * Casts an UIComponent to an UIPanel.
	 * @param component The component to cast.
	 * @return The component as UIPanel. Returns null if the cast is not possible.
	 */
	public static UIPanel asPanelComponent(UIComponent component) {
		if (isPanelComponent(component)) {
			return (UIPanel)component;
		}
		return null;
	}
}
