package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesConstants.ATTRIBUTE_VALUE;
import static de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesConstants.FACET_HEADER;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.api.UIColumn;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Iri;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.IriCollection;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.OntologyClassList;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.TaxonomyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObject;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.TaxonomyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDatatypeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyObjectProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRow;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRowTableModel;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PackagingToolkitClientConstants;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.HtmlUtils;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Shows a list with all archives found on the server. The user can modify or delete an information package and he
 * can download an information package als zip archive. The zip archive can be virtual or not virtual. An archive that is
 * not virtual contains all digital objects as files additional to the manifest file. A virtual archive contains only the manifest file.
 * And there is a button for showing the details of an information package as popup dialog.
 * <br />
 * The main element of this window is a table that contain all information packages. There are several buttons the user can
 * start the actions described above with. If the user wants to download an archive, there is a popup dialog shown. This dialog
 * allows to give some information about the archive to the application like the file name and wheather the archive should be
 * virtual or not virtual.
 * 
 * @author Christopher Olbertz
 *
 */
public class ShowArchiveListBean implements Serializable {
	/**
	 * The expression value used for access the selected attribute of an individual.
	 */
	private static final String VALUE_EXPRESSION_SELECTED = "#{individual.selected}";
	/**
	 * The name of the variable that represents the current individual in the table with the individuals.
	 */
	private static final String VAR_INDIVIDUAL = "individual";
	/**
	 * The expression value used for access the uuid attribute of an individual.
	 */
	private static final String VALUE_EXPRESSION_UUID = "#{individual.uuidOfIndividual}";
	/**
	 * The title of the column with the uuid.
	 */
	private static final String TITLE_COLUMN_UUID = "Uuid";
	/**
	 * This is the value the checkbox for the virtual archive is initialized with. If this value is true, the checkbox is checked
	 * and the archive is virtual. If you change this constant, you change the default value of this checkbox.
	 */
	private static final boolean INIT_VALUE_VIRTUAL_DOWNLOAD = true;
	/**
	 * The name of the dialog that allows the user to configure the download with the necessary information like the name
	 * of the zip file.
	 */
	private static final String DOWNLOAD_CONFIGURATION_DIALOG = "archiveDownloadDialog";
	/**
	 * The serial version uid for the serialization.
	 */
	private static final long serialVersionUID = -8632500433418251593L;
	/**
	 * This list contains the archives that should be shown in the table. It is gathered from the server. 
	 */
	private List<Archive> archiveList;
	/**
	 * The archive the user has selected for working with. 
	 */
	private Archive selectedArchive;
	/**
	 * Allows the access to the methods of the service layer.
	 */
	private ServiceFacade serviceFacade;
	/**
	 * The panel that contains the tables with the individuals. 
	 */
	private PanelGrid panelWithIndividualTables;
	/**
	 * The serialization format the user has selected in the dialog for configuring the download. 
	 */
	private String selectedSerializationFormat;
	/**
	 * True if the user has decided to download a virtual archive false otherwise.
	 */
	private boolean downloadVirtual;
	/**
	 * The name the user has chosen for the archive file he wants to download. The file name is initialized with the
	 * uuid of the selected archive.
	 */
	private String filename;
	/**
	 * The uuid of the archive the user has selected in the table.
	 */
	private String selectedArchiveUuid;
	/**
	 * Contains the information of the information package the user is currently working with. 
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * A reference to the object that contains the data cached from the server. These are
	 * for example the data read from the base ontology. 
	 */
	private ServerCache serverCache;
	/**
	 * The names of the columns of the table that contains the information about the
	 * digital objects of the information package. Every metadata element of the
	 * digital objects is represented by one column in the table. 
	 */
	private List<String> columnsOfDigitalObjectsOfSelectedInformationPackage;
	
	/**
	 * Contains the columns for the dynamically created table with the digital objects. 
	 */
	private Map<String, String> columnMap;
	/**
	 * A list with tables that contain the individuals of the selected information
	 * package. For every class of the base ontology there is one table in the list if
	 * there are individuals of this class. If a class does not have any individuals
	 * that are contained in the information package there is no table shown.
	 */
	private List<DataTable> dataTableWithIndividuals;
	/**
	 * The the taxonomy individuals contained in the information package that was selected
	 * for detail view.
	 */
	private TaxonomyIndividualCollection containedTaxonomyIndividuals;
	/**
	 * Constructs an object with a reference on the interface to the service layer.
	 * @param serviceFacade The interface class to the service layer.
	 */
	public ShowArchiveListBean(final ServiceFacade serviceFacade) {
		this.serviceFacade = serviceFacade;
	}
	
	/**
	 * Initializes the window. The archives are requested from the server and put into the table. The first archive
	 * is selected by default. 
	 */
	@PostConstruct
	public void initialize() {
		archiveList = new ArrayList<>();
		final ArchiveList archives = serviceFacade.getArchiveList();
		downloadVirtual = INIT_VALUE_VIRTUAL_DOWNLOAD;
		
		for (int i = 0; i < archives.getArchiveCount(); i++) {
			archiveList.add(archives.getArchiveAt(i));
		}
		
		if (!archiveList.isEmpty()) {
			selectedArchive = archiveList.get(0);
		}
	}
	
	/**
	 * Allows the user to delete the selected archive. After the archive is deleted, the list that contains the archives for the
	 * table is cleared and the remaining archives are written into the list.
	 */
	public void deleteArchive() {
		final ArchiveList archives = serviceFacade.deleteArchive(selectedArchive);
		archiveList.clear();
		
		for (int i = 0; i < archives.getArchiveCount(); i++) {
			final Archive archive = archives.getArchiveAt(i);  
			archiveList.add(archive);
		}
	}
	
	/**
	 * Is fired when the user wants to edit an archive. The archive is loaded and the user is
	 * redirected to the first step of creating an information package.
	 */
	public void editArchive() {
		final String title = selectedArchive.getTitle();
		final Uuid uuid = selectedArchive.getUuid();
		final PackageType packageType = selectedArchive.getPackageType();
		final SerializationFormat serializationFormat = selectedArchive.getSerializationFormat();
		
		informationPackageManager.setTitle(title);
		informationPackageManager.setUuid(uuid);
		informationPackageManager.setInformationPackageType(packageType);
		informationPackageManager.setSerializationFormat(serializationFormat);
		
		informationPackageManager.activateEditMode();
		serviceFacade.loadInformationPackage(selectedArchive);
	}
	
	/**
	 * Hides the dialog that allows the user to configure the download. 
	 */
	public void closeDownloadConfigurationDialog() {
		PrimeFacesUtils.hideDialog(DOWNLOAD_CONFIGURATION_DIALOG);
	}
	
	/**
	 * Shows the dialog that allows the user to configure the download.
	 */
	public void onOpenDownloadDialog() {
		filename = selectedArchive.getUuid().toString();
	}

	/**
	 * Returns the list with the archives. May only be used by PrimeFaces.
	 * @return The list with the archives.
	 */
	public List<Archive> getArchiveList() {
		return archiveList;
	}

	/**
	 * Sets the list with the archives. May only be used by PrimeFaces.
	 * @param archiveList The list with the archives.
	 */
	public void setArchiveList(List<Archive> archiveList) {
		this.archiveList = archiveList;
	}

	/**
	 * Returns the archive selected by the user. May only be used by PrimeFaces.
	 * @return The selected archive.
	 */
	public Archive getSelectedArchive() {
		return selectedArchive;
	}

	/**
	 * Sets the selected archive and the uuid of the selected archive. May only be used by PrimeFaces
	 * @param selectedArchive The selected archive.
	 */
	public void setSelectedArchive(final Archive selectedArchive) {
		if (selectedArchive != null) {
			this.selectedArchive = selectedArchive;
			selectedArchiveUuid = selectedArchive.getUuid().toString();
		}
	}
	
	/**
	 * Returns a list with all available serialization formats as strings. The serialization formats are values of the enum
	 * {@link de.feu.kdmp4.packagingtoolkit.enums} and their string values are put into the list.
	 * @return A list with all available serialization formats.
	 */
	public List<String> getAvailableSerializationFormats() {
		final List<String> serializationFormats = new ArrayList<>();
		for(final SerializationFormat serializationFormat:  SerializationFormat.values()) {
			serializationFormats.add(serializationFormat.toString());
		}
		
		return serializationFormats;
	}

	/**
	 * Returns the serialization format selected by the user. May only be used by PrimeFaces.
	 * @return The selected serialization format.
	 */
	public String getSelectedSerializationFormat() {
		return selectedSerializationFormat;
	}

	/**
	 *  Sets the serialization format selected by the user. May only be used by PrimeFaces.
	 * @param selectedSerializationFormat The selected serialization format.
	 */
	public void setSelectedSerializationFormat(String selectedSerializationFormat) {
		this.selectedSerializationFormat = selectedSerializationFormat;
	}

	/**
	 * Returns wheather the desired archive should be virtual or not. May only be used by PrimeFaces.
	 * @return True, if the archive should be virtual, false otherwise. 
	 */
	public boolean isDownloadVirtual() {
		return downloadVirtual;
	}

	/**
	 * Set wheather the desired archive should be virtual or not. May only be used by PrimeFaces.
	 * @param  downloadVirtual True, if the archive should be virtual, false otherwise. 
	 */
	public void setDownloadVirtual(boolean downloadVirtual) {
		this.downloadVirtual = downloadVirtual;
	}

	/**
	 * Returns the file name of the archive to download. May only be used by PrimeFaces.
	 * @return The file name of the archive to download. 
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Sets the file name of the archive to download. May only be used by PrimeFaces.
	 * @param filename  The file name of the archive to download
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	/**
	 * Returns the uuid of the selected archive. May only be used by PrimeFaces.
	 * @return The uuid of the selected archive. 
	 */
	public String getSelectedArchiveUuid() {
		return selectedArchiveUuid;
	}
	
	/**
	 * Sets the uuid of the selected archive. May only be used by PrimeFaces.
	 * @param selectedArchiveUuid The uuid of the selected archive. 
	 */
	public void setSelectedArchiveUuid(String selectedArchiveUuid) {
		this.selectedArchiveUuid = selectedArchiveUuid;
	}
	
	/**
	 * Sets a reference to the information package manager that contains all data about the information package that
	 * is currently in process.
	 * @param informationPackageManager A reference to the information package manager. It is set by Spring.
	 */
	public void setInformationPackageManager(InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}
	
	/**
	 * Creates a zip file with the information package. The file is opened in the download manager
	 * of the browser.
	 * @return The zip file as streamed content.
	 */
	public StreamedContent getDownloadArchiveFile() {
		selectedArchive.setSerializationFormat(selectedSerializationFormat);
		selectedArchive.setVirtual(downloadVirtual);
		filename = filename + PackagingToolkitFileUtils.ZIP_FILE_EXTENSION;
		
		final InputStream inputStream = serviceFacade.downloadArchiveFile(selectedArchive);
		final StreamedContent streamedContent = new DefaultStreamedContent(inputStream, 
				PackagingToolkitClientConstants.OCTET_STREAM, filename);
		return streamedContent;
	}
	
	/**
	 * Creates the table that contains the digital objects of an information package.
	 * @param digitalObjects The digital objects that should be shown in the table. 
	 */
	private void createDigitalObjectTable(final DigitalObjectCollection digitalObjects) {
		if (digitalObjects.getDigitalObjectCount() > 0) {
			initializeColumnsOfDigitalObjects(digitalObjects.getDigitalObjectAt(0));
		}
	}
	
	/**
	 * Is called when the user clicks the button for opening the details of an information
	 * package. Loads all data of the information package in the information package
	 * manager and creates the tables that contain the details about the digital objects
	 * and the individuals.
	 */
	public void onOpenDetailsDialog() {
		createDigitalObjectTable(selectedArchive.getDigitalObjects());
		loadInformationPackageInManager();
		//createIndividualDataTables();
	}
	
	/**
	 * Loads all information about the selected information package in the information package
	 * manager.
	 */
	private void loadInformationPackageInManager() {
		final String title = selectedArchive.getTitle();
		final Uuid uuid = selectedArchive.getUuid();
		final PackageType packageType = selectedArchive.getPackageType();
		final SerializationFormat serializationFormat = selectedArchive.getSerializationFormat();
		
		informationPackageManager.setTitle(title);
		informationPackageManager.setUuid(uuid);
		informationPackageManager.setInformationPackageType(packageType);
		informationPackageManager.setSerializationFormat(serializationFormat);
		serviceFacade.loadInformationPackage(selectedArchive);
		
		final IriCollection irisOfTaxonomyIndividuals = serviceFacade.findAllAssignedTaxonomyIndividuals(uuid);
		containedTaxonomyIndividuals = ClientModelFactory.createEmptyTaxonomyIndividualCollection();
		for (int i = 0; i < irisOfTaxonomyIndividuals.getIriCount(); i++) {
			final Iri iri = irisOfTaxonomyIndividuals.getIriAt(i);
			final Optional<TaxonomyIndividual> optionalWithTaxonomyIndividual = serverCache.findTaxonomyIndividualByIri(iri);
			optionalWithTaxonomyIndividual.ifPresent(taxonomyIndividual -> 
				containedTaxonomyIndividuals.addTaxonomyIndividual(taxonomyIndividual));
		}
	}
	
	/**
	 * Creates tables for all classes of that individuals are contained in the information
	 * package. 
	 */
	private void createIndividualDataTables() {
		dataTableWithIndividuals = new ArrayList<>();
		panelWithIndividualTables = new PanelGrid();
		panelWithIndividualTables.setColumns(1);
		final OntologyClassList ontologyClasses = serverCache.findAllOntologyClasses();
		
		for (int i = 0; i < ontologyClasses.getOntologyClassCount(); i++) {
			final OntologyClass ontologyClass = ontologyClasses.getOntologyClassAt(i);
			createOneIndividualDataTable(ontologyClass);
		}
	}
	
	/**
	 * Creates one data table for one ontology class. The table contains the individuals
	 * of the class. If there are no individuals in the class, there is no data table
	 * created.
	 * @param ontologyClass The class whose individuals should be shown in the table. 
	 * Only individuals that are contained in the selected information package are
	 * considered.
	 */
	private void createOneIndividualDataTable(final OntologyClass ontologyClass) {
		final OntologyIndividualCollection ontologyIndividuals = serverCache.getIndividualsByClass(ontologyClass);
		final OntologyIndividualCollection individualsInInformationPackage = ClientModelFactory.createEmptyOntologyIndividualCollection();
		
		for (int i = 0; i < ontologyIndividuals.getIndiviualsCount(); i++) {
			final OntologyIndividual individual = ontologyIndividuals.getIndividual(i);
			final Uuid uuidOfIndividual = individual.getUuid();
			if (informationPackageManager.containsIndividual(uuidOfIndividual)) {
				individualsInInformationPackage.addIndividual(individual);
			}
		}
		
		if (individualsInInformationPackage.isNotEmpty()) {
			final DataTable dataTable = configureDataTable();
			//createColumnSelected(dataTable);
			createDynamicColumns(ontologyClass, dataTable);
			createDynamicData(individualsInInformationPackage, dataTable, ontologyClass);
			dataTableWithIndividuals.add(dataTable);
			panelWithIndividualTables.getChildren().add(dataTable);
		}
	}
	
	/**
	 * Create the data of the table dynamically with the values contained in a collection
	 * with individuals. 
	 */
	private void createDynamicData(final OntologyIndividualCollection individualsInInformationPackage, 
								   final DataTable dataTable, final OntologyClass ontologyClass) {
		if (individualsInInformationPackage.isNotEmpty()) {
			createRowData(individualsInInformationPackage, dataTable, ontologyClass);
		}
	}
	
	/**
	 * Creates the data that are shown in the rows of the table.
	 * @param individualList These individuals contain the data that should be shown. 
	 * @param dataTable The data table we want to create the data for.
	 */
	private void createRowData(final OntologyIndividualCollection individualList, final DataTable dataTable, 
			final OntologyClass ontologyClass) {
		//final OntologyIndividual firstIndividual = individualList.getIndividual(0);
		//final String iriOfOntologyClass = firstIndividual.getOntologyClass().getFullName();
		
		final List<DataRow> dataRows = new ArrayList<>();
		for (int i = 0; i < individualList.getIndiviualsCount(); i++) {
			final OntologyIndividual ontologyIndividual = individualList.getIndividual(i);
			createOneDataRow(ontologyIndividual, dataTable, dataRows, ontologyClass);
		}
		
		DataRowTableModel dataModel = new DataRowTableModel(dataRows);
		dataTable.setValue(dataModel);
		//dynamicGuiManager.addDataModel(iriOfOntologyClass, dataModel);
	}
	
	/**
	 * Creates a column with a checkbox for selecting an individual.  
	 */
	private void createColumnSelected(final DataTable dataTable) {
		Column columnCheckboxes = PrimeFacesComponentFactory.createColumn();
        dataTable.getChildren().add(columnCheckboxes);
        SelectBooleanCheckbox booleanCheckbox = PrimeFacesComponentFactory.createSelectBooleanCheckbox();
        ValueExpression selectedValueExpression = JsfUtil.createValueExpression(VALUE_EXPRESSION_SELECTED, String.class);
        booleanCheckbox.setValueExpression(ATTRIBUTE_VALUE, selectedValueExpression);
        columnCheckboxes.getChildren().add(booleanCheckbox);
	}
	
	/**
	 * Creates a row for one individual. 
	 * @param index The index of the individual in the collection with individuals. 
	 * @param dataTable The data table we want to create the data for.
	 */
	private void createOneDataRow(final OntologyIndividual ontologyIndividual, final DataTable dataTable,
			final List<DataRow> dataRows, final OntologyClass ontologyClass) {
		final DataRow dataRow = new DataRow(ontologyClass.getDatatypePropertiesCount());
		dataRow.setUuidOfIndividual(ontologyIndividual.getUuid());
		
		final String[] values = new String[ontologyClass.getDatatypePropertiesCount()];
		
		final UIComponent checkboxInTable = dataTable.getChildren().get(0);
		final Column checkboxColumn = (Column)checkboxInTable;
		final SelectBooleanCheckbox checkbox = (SelectBooleanCheckbox)checkboxColumn.getChildren().get(0);
		final String labelText = ontologyClass.getLocalName();
		final Optional<OntologyObjectProperty> optionalWithObjectProperty = ontologyIndividual.findObjectPropertyByLabel(labelText);
		optionalWithObjectProperty.ifPresent(ontologyObjectProperty ->  {
			checkbox.setSelected(true);
			//informationPackageManager.addObjectPropertyValue(ontologyObjectProperty.getPropertyId(), individual);
		});
		
		/* Because the first column with the index 0 is the checkbox that has no
		 * representation in the individual. 
		 */
		for (int columnCounter = 1; columnCounter < dataTable.getColumnsCount(); columnCounter++) {
			final UIComponent componentInTable = dataTable.getChildren().get(columnCounter);
			final Column column = (Column)componentInTable;
			final UIComponent headerFacet = column.getFacet(FACET_HEADER);
			if (headerFacet != null) {
				final UIOutput columnOutput = (UIOutput)headerFacet;
				final String columnTitle = columnOutput.getValue().toString();
				final Optional<OntologyDatatypeProperty> optionalWithDatatypeProperty = ontologyIndividual.
						findDatatypePropertyByLabel(columnTitle);
				final int i = columnCounter;
				optionalWithDatatypeProperty.ifPresent(datatypeProperty -> {
					Object value = datatypeProperty.getPropertyValue();
					
					if (value != null) {
						values[i-2] = datatypeProperty.getPropertyValue().toString();
					} else {
						values[i-2] = StringUtils.EMPTY_STRING;
					}
				});
			}
		}
		
		dataRow.setValues(values);
		dataRows.add(dataRow);
	}
	
	/**
	 * Creates and configures one data table for individuals.
	 * @return The created data table.
	 */
	private DataTable configureDataTable() {
		final DataTable dataTable = PrimeFacesComponentFactory.createDataTable();
		dataTable.setVar(VAR_INDIVIDUAL);
		return dataTable;
	}
	
	/**
	 * Creates the columns for a table with individuals. Uses the properties found in ontologyClass. 
	 * For every property a column is created.
	 */
	private void createDynamicColumns(final OntologyClass ontologyClass, final DataTable dataTable) {
        //columns.clear();  
        //createColumnUuid(dataTable);
        createColumnsForProperties(ontologyClass, dataTable);
	}
	
	/**
	 * Creates a column that shows the uuid of an individual.
	 * @param dataTable The data table that should contain the created uuid column. 
	 */
	private void createColumnUuid(final DataTable dataTable) {
		final Column columnUuid = PrimeFacesComponentFactory.createColumn();
		final UIOutput columnUuidTitle = PrimeFacesComponentFactory.createUIOutput();
		columnUuidTitle.setValue(TITLE_COLUMN_UUID);
		columnUuid.getFacets().put(FACET_HEADER, columnUuidTitle);
		dataTable.getChildren().add(columnUuid);
		
		final HtmlOutputText lblColumnUuid = new HtmlOutputText();
		final ValueExpression uuidValueExpression = JsfUtil.createValueExpression(VALUE_EXPRESSION_UUID, String.class);
		lblColumnUuid.setValueExpression(ATTRIBUTE_VALUE, uuidValueExpression);
		columnUuid.getChildren().add(lblColumnUuid);
	}
	
	/**
	 * Creates columns for the properties in a class of an ontology. 
	 * @param ontologyClass The ontology class with the properties that should be used as columns for 
	 * the data table.
	 * @param dataTable The data table we are creating columns for. 
	 */
	private void createColumnsForProperties(final OntologyClass ontologyClass, final DataTable dataTable) {
        for (int i = 0; i < ontologyClass.getDatatypePropertiesCount(); i++) {
        	final OntologyDatatypeProperty ontologyProperty = ontologyClass.getDatatypeProperty(i);
        	final String columnTitleString = ontologyProperty.getLabel();
        	final Column column = PrimeFacesComponentFactory.createColumn();
        	final UIOutput columnTitle = PrimeFacesComponentFactory.createUIOutput();
			columnTitle.setValue(columnTitleString);
			column.getFacets().put(FACET_HEADER, columnTitle);
			//dataTable.getChildren().add(column);
			
			final HtmlOutputText lblColumn = PrimeFacesComponentFactory.createHtmlOutputText();
			final ValueExpression valueExpression = JsfUtil.createValueExpression("#{individual.values[" + i + "]}", String.class);
			lblColumn.setValueExpression(ATTRIBUTE_VALUE, valueExpression);
			column.getChildren().add(lblColumn);
			dataTable.getChildren().add(column);
        }
        
        /*for (final UIColumn column: dataTable.getColumns()) {
        	column.getFacet(FACET_HEADER)
        	System.out.println(((UIOutput)column.getFacet(FACET_HEADER).getChildren().get(0)).getValue().toString());
        }*/
	}
	
	/**
	 * Reads the keys from the map with the metadata of one digital object. Then 
	 * the columns for the table with the digital objects are created. For one key
	 * in the map there is one column in the table. Because all digital objects have
	 * the same meta data at the moment it is enough to analyse the meta data of one
	 * digital object. 
	 * @param digitalObject
	 */
	private void initializeColumnsOfDigitalObjects(final DigitalObject digitalObject) {
		columnMap = new LinkedHashMap<>();
		columnsOfDigitalObjectsOfSelectedInformationPackage = new ArrayList<>();
		final StringList keysOfMetadata = digitalObject.getKeysOfMetadata();
		
		for (int i = 0; i < keysOfMetadata.getSize(); i++) {
			final String columnTitle  = keysOfMetadata.getStringAt(i);
			columnMap.put(columnTitle, columnTitle);
		}
		
		columnsOfDigitalObjectsOfSelectedInformationPackage.addAll(columnMap.keySet());
	}
	
	public List<String> getColumnsOfDigitalObjectsOfSelectedInformationPackage() {
		return columnsOfDigitalObjectsOfSelectedInformationPackage;
	}
	
	public void setColumnsOfDigitalObjectsOfSelectedInformationPackage(
			List<String> columnsOfDigitalObjectsOfSelectedInformationPackage) {
		this.columnsOfDigitalObjectsOfSelectedInformationPackage = columnsOfDigitalObjectsOfSelectedInformationPackage;
	}
	
	public Map<String, String> getColumnMap() {
		return columnMap;
	}
	
	public void setColumnMap(Map<String, String> columnMap) {
		this.columnMap = columnMap;
	}
	
	public List<DigitalObject> getDigitalObjectsOfInformationPackage() {
		return selectedArchive.getDigitalObjects().asList();
	}
	
	public void setServerCache(ServerCache serverCache) {
		this.serverCache = serverCache;
	}
	
	public List<DataTable> getDataTableWithIndividuals() {
		return dataTableWithIndividuals;
	}
	
	public void setDataTableWithIndividuals(List<DataTable> dataTableWithIndividuals) {
		this.dataTableWithIndividuals = dataTableWithIndividuals;
	}
	
	public PanelGrid getPanelWithIndividualTables() {
		return panelWithIndividualTables;
	}
	
	public void setPanelWithIndividualTables(PanelGrid panelWithIndividualTables) {
		this.panelWithIndividualTables = panelWithIndividualTables;
	}
	
	public String getIndividualTablesString() {
		final StringBuffer tablesString = new StringBuffer();
		final OntologyClassList ontologyClasses = serverCache.findAllOntologyClasses();
		if (ontologyClasses != null) {
		
			for (int i = 0; i < ontologyClasses.getOntologyClassCount(); i++) {
				final OntologyClass ontologyClass = ontologyClasses.getOntologyClassAt(i);
				final OntologyIndividualCollection ontologyIndividuals = serverCache.getIndividualsByClass(ontologyClass);
				informationPackageManager.findOntologyClassByIri(classname)
				tablesString.append("<table border=\"1\">");
				tablesString.append("<tr>");
				
		        for (int j = 0; j < ontologyClass.getDatatypePropertiesCount(); j++) {
		        	final OntologyDatatypeProperty ontologyProperty = ontologyClass.getDatatypeProperty(j);
		        	final String columnTitleString = ontologyProperty.getLabel();
		        	tablesString.append("<th>").append(columnTitleString).append("</th>");
		        }
		        tablesString.append("</tr>");
				
				for (int j = 0; j < ontologyIndividuals.getIndiviualsCount(); j++) {
					final OntologyIndividual individual = ontologyIndividuals.getIndividual(j);
					final Uuid uuidOfIndividual = individual.getUuid();
					if (informationPackageManager.containsIndividual(uuidOfIndividual)) {
						tablesString.append("<tr>");
						for (int k = 0; k < ontologyClass.getDatatypePropertiesCount(); k++) {
				        	final OntologyDatatypeProperty ontologyProperty = ontologyClass.getDatatypeProperty(k);
				        	final String columnTitleString = ontologyProperty.getLabel();
				        	tablesString.append("<td>");
				        	final Optional<OntologyDatatypeProperty> optionalWithProperty = individual.findDatatypePropertyByLabel(columnTitleString);
				        	optionalWithProperty.ifPresent(datatypeProperty -> tablesString.append(datatypeProperty.getPropertyValue()));
				        	tablesString.append("</td>");
				        }
						tablesString.append("</tr>");
					}
				}
				
				tablesString.append("</tr>");
				tablesString.append("</table>");
				
				/*if (individualsInInformationPackage.isNotEmpty()) {
					for (int j = 0; j < individualsInInformationPackage.getIndiviualsCount(); j++) {
						tablesString.append("<table>");
						tablesString.append("<tr>");
						for (final UIColumn column: dataTable.getColumns()) {
							tablesString.append("<th>").append(column.getHeaderText()).append("</th>");
						}
						tablesString.append("</tr>");
						tablesString.append("</table>");
					}
				}*/
			}
		}
		
		
		/*if (dataTableWithIndividuals != null) {
			for (final DataTable dataTable: dataTableWithIndividuals) {
				tablesString.append("<table>");
				tablesString.append("<tr>");
				for (final UIColumn column: dataTable.getColumns()) {
					tablesString.append("<th>").append(column.getHeaderText()).append("</th>");
				}
				tablesString.append("</tr>");
				tablesString.append("</table>");
			}
		}*/
		return tablesString.toString();
	}
	
	public String getTaxonomyIndividualsString() {
		if (containedTaxonomyIndividuals != null) {
			return containedTaxonomyIndividuals.getAsHtmlUnorderedList();
		} else {
			return StringUtils.EMPTY_STRING;
		}
	}
}