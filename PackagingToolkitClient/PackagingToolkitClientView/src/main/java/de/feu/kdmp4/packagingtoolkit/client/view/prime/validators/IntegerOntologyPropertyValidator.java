package de.feu.kdmp4.packagingtoolkit.client.view.prime.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.RestrictionNotSatisfiedException;

public class IntegerOntologyPropertyValidator implements Validator {
	private OntologyIntegerProperty ontologyProperty;
	
	public IntegerOntologyPropertyValidator(OntologyIntegerProperty ontologyProperty) {
		this.ontologyProperty = ontologyProperty;
	}
	
	@Override
	public void validate(FacesContext facesContext, UIComponent component, 
			Object object) throws ValidatorException {
		Integer value = (Integer)object;
		JsfUtil.addErrorMessageToContext("Blabla", "Validator");
		try {
			//RestrictionValue<Integer> restrictionValue = new RestrictionValue<Integer>(value);
			//ontologyProperty.checkRestrictions(restrictionValue);
		} catch (RestrictionNotSatisfiedException ex) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					ex.getSummary(), ex.getMessage()));
		}
	}
}
