/**
 * Contains the managed beans for PrimeFaces.
 */
package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;