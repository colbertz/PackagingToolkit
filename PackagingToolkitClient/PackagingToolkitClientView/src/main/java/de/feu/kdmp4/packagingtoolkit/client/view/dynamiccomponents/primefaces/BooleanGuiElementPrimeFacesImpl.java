package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import java.util.Locale;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.BooleanGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;

/**
 * Creates a component for a boolean value as a SelectBooleanCheckbox from PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class BooleanGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements BooleanGuiElement {
	/**
	 * This components represents the value. 
	 */
	private SelectBooleanCheckbox checkbox;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	
	/**
	 * Creates a new component object.
	 * @param ontologyBooleanProperty The property that should be processed with this component.
	 */
	public BooleanGuiElementPrimeFacesImpl(OntologyBooleanProperty ontologyBooleanProperty) {
		super(ontologyBooleanProperty);

		final String labelText = ontologyBooleanProperty.getLabel();
		final boolean value = ontologyBooleanProperty.getPropertyValue();
		initializeBooleanCheckbox(labelText, value);
		initializePanelGroup(); 
	}

	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(checkbox);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	/**
	 * Initializes the checkbox. 
	 * @param labelText Is used for creating a identifier for this component.
	 * @param value The value that should be displayed in this component.
	 */
	private void initializeBooleanCheckbox(final String labelText, final boolean value) {
		checkbox = PrimeFacesComponentFactory.createSelectBooleanCheckbox();
		final String idCheckbox = UniqueIdentifierUtil.createCheckboxIdentifier(labelText);
	
		checkbox.setValue(value);
		checkbox.setId(idCheckbox);
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}

	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
