/**
 * Contains interfaces for the dynamically created components. They are needed for hold the
 * application independant from any concrete framework. Most of the interfaces are empty, but
 * they can be used for further development.
 */

package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;