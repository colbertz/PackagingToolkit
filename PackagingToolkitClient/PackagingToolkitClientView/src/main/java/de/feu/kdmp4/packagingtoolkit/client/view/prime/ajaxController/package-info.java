/**
 * Contains classes that are related to ajax with PrimeFaces. They are used to outsource some functionality
 * related to ajax for avoiding that the managed beans classes are getting too long. 
 */

package de.feu.kdmp4.packagingtoolkit.client.view.prime.ajaxController;