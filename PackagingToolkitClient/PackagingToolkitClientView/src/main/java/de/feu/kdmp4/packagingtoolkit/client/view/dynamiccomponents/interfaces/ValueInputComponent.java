package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;

/**
 * Contains an area with dynamically generated input components.
 * @author Christopher Olbertz
 *
 */
public interface ValueInputComponent {
	/**
	 * Renders the whole area and all contained components with the help of a certain
	 * framework.
	 * @return The root component that contains all components of this area.
	 */
	public Object toGuiElement();
	/**
	 * Sets the values of an individual in the components of the gui so that the user can edit them.
	 * @param ontologyIndividual The individual that contains the values to set.
	 */
	void setValuesOfIndividual(final OntologyIndividual ontologyIndividual);
}
