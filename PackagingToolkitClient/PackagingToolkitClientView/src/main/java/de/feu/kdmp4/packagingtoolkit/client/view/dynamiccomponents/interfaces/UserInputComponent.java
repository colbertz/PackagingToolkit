package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

/**
 * Represents the gui with the input elements for the properties of an ontology
 * class.
 * @author Christopher Olbertz
 *
 */
public interface UserInputComponent {

}
