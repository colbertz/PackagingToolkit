package de.feu.kdmp4.packagingtoolkit.client.view.managers;

import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.client.view.prime.tablemodels.DataRowTableModel;


/**
 * Contains some important values that have to be saved during the user is working with the 
 * dynamically generated gui.
 * @author Christopher Olbertz
 *
 */
public class DynamicGuiManager {
	/**
	 * Contains the data models for the tables that contain the object property values. This is
	 * necessary for writing back the values when an existing individual is loaded. The individuals
	 * that are assigned to the object properties of this individual have to be checked in the data
	 * tables. Therefore the data models are needed.
	 * <br />
	 * The value is the data model itself and the key is the iri of the class the object property
	 * belongs to.
	 */
	private Map<String, DataRowTableModel> dataModels;
	
	/**
	 * Initializes the manager with a hash map. 
	 */
	public DynamicGuiManager() {
		this.dataModels = new HashMap<>();
	}
	
	/**
	 * Adds a table model to the manager.
	 * @param iriOfOntologyClass The class the model refers.
	 * @param dataModel The model that should be added.
	 */
	public void addDataModel(final String iriOfOntologyClass, final DataRowTableModel dataModel) {
		dataModels.put(iriOfOntologyClass, dataModel);
	}
	
	/**
	 * Determines the model that is assigned to a certain class. 
	 * @param iriOfOntologyClass The iri of the class. 
	 * @return The model that has been found. 
	 */
	public DataRowTableModel getDataModel(final String iriOfOntologyClass) {
		return dataModels.get(iriOfOntologyClass);
	}
}
