package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.component.UISelectItems;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.AjaxBehaviorListener;
import javax.faces.model.SelectItem;

import org.primefaces.behavior.ajax.AjaxBehavior;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.selectoneradio.SelectOneRadio;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nComponentLabelUtil;
import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nMessagesUtil;
import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.View;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ViewList;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.service.facades.ServiceFacade;
import de.feu.kdmp4.packagingtoolkit.client.utils.JsfConstants;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.JsfUtil;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * This managed bean belongs to the page that shows the saves views for the
 * configured base ontology. A view consists of several classes from the base
 * ontology. This classes are used in the next step. The user can create a
 * new view or use an existing view. If he selects an existing one, the 
 * classes contained in the selected view are shown in the next step. If he
 * chooses to create a new view, the next step does not contain any ontology
 * classes.
 * @author Christopher Olbertz
 *
 */
@Component
@Scope("view")
public class SavedViewsBean implements Serializable {
	private static final long serialVersionUID = -7968692717001516106L;
	/**
	 * This value indicated that the user wants to create a new view.
	 */
	private static final int VALUE_NEW_VIEW = -1;
	
	/**
	 * A list with the names of the classes contained in the selected
	 * view.
	 */
	private List<String> containedClasses;
	/**
	 * The information package manager contains all information about
	 * the information package that are important in the steps on the
	 * client. 
	 */
	private InformationPackageManager informationPackageManager;
	/**
	 * The panel with the buttons for choosing one view.
	 */
	private Panel pnlButtons;
	/**
	 * The radio buttons that represent the views. The first entry
	 * indicates that the user wants to create a new view.
	 */
	private SelectOneRadio radioViews;
	/**
	 * The facade object for using the service methods.
	 */
	private ServiceFacade serviceFacade;
	/**
	 * The view the user has selected for the next step.
	 */
	private View selectedView;
	/**
	 * Contains the views shown on the page.
	 */
	private ViewList viewList;
	/**
	 * True if the button for going to the next step is disabled. 
	 */
	private boolean buttonNextDisabled;
	/**
	 * Caches the data received from the server.
	 */
	private ServerCache serverCache;
	
	@PostConstruct
	public void initialize() {
		containedClasses = new ArrayList<>();
		initializeRadioButtonPanel();
		buttonNextDisabled = true;
	}
	
	/**
	 * Initializes the panel with the radio buttons.
	 */
	private void initializeRadioButtonPanel() {
		pnlButtons = PrimeFacesComponentFactory.createPanel();
		radioViews = PrimeFacesComponentFactory.createSelectOneRadio(); 
		final AjaxBehavior clickBehavior = createClickBehaviorForRadioButtons();
		radioViews.addClientBehavior(JsfConstants.AJAX_EVENT_CHANGE, clickBehavior);
		radioViews.setLayout(JsfConstants.PRIME_RADIO_GRID);
		radioViews.setColumns(1);
		addRadioButtonsForViewsToPage();
		pnlButtons.getChildren().add(radioViews);
	}

	/**
	 * Is called if the user clicks the button for deleting a view.
	 */
	public void onDeleteSelectedView(AjaxBehaviorEvent ajaxBehaviorEvent) {
		final int viewId = selectedView.getViewId();
		if (viewId > VALUE_NEW_VIEW) {
			serviceFacade.deleteView(viewId);
			JsfUtil.addFacesMessageToContext(I18nMessagesUtil.getSuccessString(), I18nMessagesUtil.getViewDeletedString());
			addRadioButtonsForViewsToPage();
		} else {
			JsfUtil.addErrorMessageToContext(I18nMessagesUtil.getErrorString(), I18nMessagesUtil.getSelectViewForDeletingString());
		}
	}
	
	/**
	 * Creates a click behavior for the radio buttons. If a radio button is clicked by the user,
	 * an other view is selected. Then on the right side of the window, the ontology classes
	 * contained in the selected view are shown. So, the click behavior updates the right
	 * side of the window.  
	 * @return The click behavior described above.
	 */
	private AjaxBehavior createClickBehaviorForRadioButtons() {
		final AjaxBehavior clickBehavior = new AjaxBehavior();
		clickBehavior.addAjaxBehaviorListener(new AjaxBehaviorListener() {
			
			@Override
			public void processAjaxBehavior(AjaxBehaviorEvent event) throws AbortProcessingException {
				Object sourceComponent = event.getSource();
				// This cast is not dangerous because the source can only be a radio button.
				final SelectOneRadio selectOneRadio = (SelectOneRadio) sourceComponent;
				int selectedViewId = Integer.parseInt(selectOneRadio.getValue().toString());
				
				String selectedViewName = StringUtils.EMPTY_STRING;
				for (int i = 0; i < viewList.getViewCount(); i++) {
					final View view = viewList.getViewAt(i);
					if (view.getViewId() == selectedViewId) {
						selectedViewName = view.getViewname();
					}
				}
				
				buttonNextDisabled = false;				
				
				if (selectedViewId != VALUE_NEW_VIEW) {
					selectedView = viewList.getViewByName(selectedViewName);
					addOntologyClassesOfSelectedViewToPage();
				} else {
					clearOntologyClassesOnPage();
					selectedView = ClientModelFactory.createNewView();
				}
			}
		});
		clickBehavior.setUpdate(JsfConstants.AJAX_FORM);
		return clickBehavior;
	}

	/**
	 * Removes the ontology classes shown on the right side of the page. The right side
	 * of the page is empty now.   
	 */
	private void clearOntologyClassesOnPage() {
		containedClasses.clear();
		initializeRadioButtonPanel();
	}
	
	/**
	 * Adds the ontology classes contained in the view selected by the user in the right
	 * side of the page. 
	 */
	private void addOntologyClassesOfSelectedViewToPage() {
		containedClasses = new ArrayList<>();
		
		if (selectedView != null) {
			for (int i = 0; i < selectedView.getClassesCount(); i++) {
				final OntologyClass ontologyClass = selectedView.getOntologyClassAt(i);
				final String label = determineLabel(ontologyClass.getFullName()); 
				containedClasses.add(label);
			}
		}
	}

	/**
	 * Determines the label for the component for a property for the current language. If there is no
	 * label for the current language, the default label is used. If there is no default label, the iri of 
	 * the property is used as label.  
	 * @param ontologyProperty The property whose label we want to determine. 
	 */
	public String determineLabel(final String ontologyClassIri) {
		final OntologyClass ontologyClass = serverCache.getClassByIri(ontologyClassIri);
		final Locale locale = PrimeFacesUtils.getCurrentLocale();
		String labelText = ontologyClass.findLabelByLanguage(locale);
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyClass.getLabel();
		}
		
		if (StringValidator.isNullOrEmpty(labelText)) {
			labelText = ontologyClass.getFullName();
		}
		
		return labelText;
	}
	
	/**
	 * Adds for every view in a given list a radio button to the page. The views are determined by
	 * a query against the database on the server. 
	 */
	private void addRadioButtonsForViewsToPage() {
		radioViews.getChildren().clear();
		viewList = serviceFacade.getAllViews();
		final List<SelectItem> selectItemList = new ArrayList<>();
		final UISelectItems selectItems = new UISelectItems();
		selectItems.setValue(selectItemList);

		selectItemList.add(createSelectItemNewView());
		
		for (int i = 0; i < viewList.getViewCount(); i++) {
			final View view = viewList.getViewAt(i);
			final SelectItem selectItem = createSelectItemForView(view);
			selectItemList.add(selectItem);
		}
		radioViews.getChildren().add(selectItems);
	}
	
	/**
	 * Creates a select item for the radio buttons. The select item represents one view.
	 * @param view The view that should be represented by the select item.
	 * @return The select item representing the view.
	 */
	private SelectItem createSelectItemForView(final View view) {
		final SelectItem selectItem = new SelectItem();
		selectItem.setLabel(view.getViewname());
		selectItem.setValue(view.getViewId());
		return selectItem;
	}
	
	/**
	 * Is called if the user has chosen a view and wants to go to the next step.
	 * @param event
	 */
	public void continueWithSelectedView(ActionEvent event) {
		informationPackageManager.setSelectedView(selectedView);
		initialize();
	}
	
	/**
	 * Creates a select item for the option to continue was a new view.
	 * @return The select item.
	 */
	private SelectItem createSelectItemNewView() {
		final SelectItem selectItemNewView = new SelectItem();
		selectItemNewView.setLabel(I18nComponentLabelUtil.getRadioButtonNewViewString());
		selectItemNewView.setValue(VALUE_NEW_VIEW);
		return selectItemNewView;
	}

	// ********* Getters and Setters **********
	public List<String> getContainedClasses() {
		return containedClasses;
	}

	public void setContainedClasses(List<String> containedClasses) {
		this.containedClasses = containedClasses;
	}

	public Panel getPnlButtons() {
		return pnlButtons;
	}

	public void setPnlButtons(Panel pnlButtons) {
		this.pnlButtons = pnlButtons;
	}

	public void setInformationPackageManager(InformationPackageManager informationPackageManager) {
		this.informationPackageManager = informationPackageManager;
	}

	public void setServiceFacade(ServiceFacade serviceFacade) {
		this.serviceFacade = serviceFacade;
	}
	
	public boolean isButtonNextDisabled() {
		return buttonNextDisabled;
	}

	public void setButtonNextDisabled(boolean buttonNextDisabled) {
		this.buttonNextDisabled = buttonNextDisabled;
	}
	
	public ServerCache getServerCache() {
		return serverCache;
	}
	
	public void setServerCache(ServerCache serverCache) {
		this.serverCache = serverCache;
	}
}