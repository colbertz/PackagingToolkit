package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

import javax.faces.component.html.HtmlInputHidden;

/**
 * A super interface for an element in the user interface. Every other interface for 
 * gui elements extends this interface.
 * @author Christopher Olbertz
 *
 */
public interface GuiElement  {
	/**
	 * Creates a representation of the current element as component in a certain
	 * framework. The implementing classes decide which framework is used for
	 * rendering the component.
	 * @return The element as gui component.
	 */
	Object getGuiElement();
	/**
	 * Gets the label of this component. The label contains a text that describes the
	 * data of this component. The class of the returned object is dependant from
	 * the framework used for the user interface.
	 * @return The label of this component.
	 */
	Object getLabel();
	/**
	 * Returns the hidden field of this component. The hidden field contains the iri of the 
	 * property that is assigned to this component. This is important because we need to 
	 * know this iri for evaluating the value entered by the user. With the help from this
	 * iri we are able to determine the java class for this property.  
	 * @return The hidden field that contains the iri of this property.
	 */
	HtmlInputHidden getTxtPropertyIri();
}
