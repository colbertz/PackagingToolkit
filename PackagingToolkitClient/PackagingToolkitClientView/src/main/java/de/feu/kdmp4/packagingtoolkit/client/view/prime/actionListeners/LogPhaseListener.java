package de.feu.kdmp4.packagingtoolkit.client.view.prime.actionListeners;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * A phase listener that creates an output on the terminal before and after every phase
 * in the lifecycle of PrimeFaces. It can be useful for the search for errors related to the
 * lifecycle.
 * @author Christopher Olbertz
 *
 */
public class LogPhaseListener implements PhaseListener {
	private static final long serialVersionUID = 3350753686757067056L;

	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

	public void afterPhase(PhaseEvent pe) {
		System.out.println("After Phase: " + pe.getPhaseId());
	}

	public void beforePhase(PhaseEvent pe) {
		System.out.println("Before Phase : " + pe.getPhaseId());
	}
}