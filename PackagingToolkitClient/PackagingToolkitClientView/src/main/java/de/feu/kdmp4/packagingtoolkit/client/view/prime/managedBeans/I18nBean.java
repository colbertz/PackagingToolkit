package de.feu.kdmp4.packagingtoolkit.client.view.prime.managedBeans;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PrimeFacesUtils;
import de.feu.kdmp4.packagingtoolkit.models.classes.Language;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * This bean managed the internationalization. It contains the available languages and the listeners that are fired
 * when the language is changed. 
 * @author Christopher Olbertz
 *
 */
@Component
@Scope("session")
public class I18nBean implements Serializable {

	/**
	 * Field serialVersionUID.
	 * (value is 1)
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The code of the current locale.
	 */
	private String localeCode;

	/**
	 * Contains the available languages.
	 */
	private List<Language> languages;

	/**
	 * Initializes the available languages.
	 */
	@PostConstruct
	public void processEvent() {
		languages = new ArrayList<Language>();
		languages.add(new Language(Locale.GERMAN, "Deutsch"));
		languages.add(new Language(Locale.ENGLISH, "English"));
	}

	/**
	 * Is fired if the user wants to change the language. Looks for the selected language in the list with the 
	 * available languages and sets the new locale.
	 * @param valueChangeEvent The event that was fired. 
	 */
	public void onLocaleCodeChange(ValueChangeEvent valueChangeEvent) {
		final String newLocale = valueChangeEvent.getNewValue().toString();

		for (final Language language: languages) {
			if (areStringsEqual(language.getLanguageDescription(), newLocale)) {
				PrimeFacesUtils.setLocale(language.getLocale());
			}
		}
	}

	public String getLocaleCode() {
		return localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	/**
	 * Returns the language selected  by the user. If no language is selected the first
	 * language is returned.
	 * @return The selected language or the first language. 
	 */
	public Language getSelectedLanguage() {
		if (StringValidator.isNullOrEmpty(localeCode)) {
			for (final Language language: languages) {
				if (areStringsEqual(language.getLanguageCode(), localeCode)) {
					return language;
				}
			}
		}
		
		return languages.get(0);
	}
}