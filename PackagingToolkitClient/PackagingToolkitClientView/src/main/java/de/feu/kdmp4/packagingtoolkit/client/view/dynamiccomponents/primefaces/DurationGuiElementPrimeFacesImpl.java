package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import javax.faces.component.html.HtmlInputHidden;

import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.spinner.Spinner;

import de.feu.kdmp4.packagingtoolkit.client.i18n.I18nComponentLabelUtil;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DurationGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.models.interfaces.OntologyDuration;

/**
 * Creates a component for a duration value. This component consists of several components.
 * There are six labels and six input fields and sliders for the six parts of a duration value
 * (years, months, days, hours, minutes, seconds). There is a seventh part: is this a negative
 * value? This part is represented by a BooleanCheckbox and a label. All components are
 * containd in a PanelGrid.  
 * @author Christopher Olbertz
 *
 */
public class DurationGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements DurationGuiElement {
	/**
	 * The number of columns of the PanelGrid. 
	 */
	private static final int COLUMN_COUNT = 4;
	/**
	 * This checkbox is checked if the duration is a negative value. 
	 */
	private SelectBooleanCheckbox chkNegative;
	/**
	 * Contains the description for the days.
	 */
	private OutputLabel outputLabelDays;
	/**
	 * Contains the description for the hours.
	 */
	private OutputLabel outputLabelHours;
	/**
	 * Contains the description for the minutes.
	 */
	private OutputLabel outputLabelMinutes;
	/**
	 * Contains the description for the months.
	 */
	private OutputLabel outputLabelMonths;
	/**
	 * Contains the description for the negative checkbox.
	 */
	private OutputLabel outputLabelNegative;
	/**
	 * Contains the description for the seconds.
	 */
	private OutputLabel outputLabelSeconds;
	/**
	 * Contains the description for the years.
	 */
	private OutputLabel outputLabelYears;
	/**
	 * Contains all components.
	 */
	private PanelGrid pnlDurationElement;
	/**
	 * A spinner for entering the days.
	 */
	private Spinner spinnerDays;
	/**
	 * A spinner for entering the hours.
	 */
	private Spinner spinnerHours;
	/**
	 * A spinner for entering the minutes.
	 */
	private Spinner spinnerMinutes;
	/**
	 * A spinner for entering the months.
	 */
	private Spinner spinnerMonths;
	/**
	 * A spinner for entering the seconds.
	 */
	private Spinner spinnerSeconds;
	/**
	 * A spinner for entering the years.
	 */
	private Spinner spinnerYears;
	
	/**
	 * Creates a new component object.
	 * @param ontologyDurationProperty The property that should be processed with this component.
	 */
	public DurationGuiElementPrimeFacesImpl(final OntologyDurationProperty ontologyDurationProperty) {
		super(ontologyDurationProperty);
		final String labelText = ontologyDurationProperty.getLabel();
		final OntologyDuration value = ontologyDurationProperty.getPropertyValue();
		
		initializeOutputLabels(labelText);
		initializeSpinners();
		chkNegative = PrimeFacesComponentFactory.createSelectBooleanCheckbox();
		final String idChkNegative = UniqueIdentifierUtil.createCheckboxIdentifier(PackagingToolkitConstants.IDENTIFIER_NEGATIVE);
		chkNegative.setId(idChkNegative);
		initializePanel(value);
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public PanelGrid getGuiElement() {
		return pnlDurationElement;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
	
	/**
	 * Initializes the spinner components.  They cannot accept negative values.
	 */
	private void initializeSpinners() {
		final String idSpinnerDays = UniqueIdentifierUtil.createSpinnerIdentifier(PackagingToolkitConstants.IDENTIFIER_DAY);
		final String idSpinnerHours = UniqueIdentifierUtil.createSpinnerIdentifier(PackagingToolkitConstants.IDENTIFIER_HOUR);
		final String idSpinnerMinutes = UniqueIdentifierUtil.createSpinnerIdentifier(PackagingToolkitConstants.IDENTIFIER_MINUTE);
		final String idSpinnerMonths = UniqueIdentifierUtil.createSpinnerIdentifier(PackagingToolkitConstants.IDENTIFIER_MONTH);
		final String idSpinnerSeconds = UniqueIdentifierUtil.createSpinnerIdentifier(PackagingToolkitConstants.IDENTIFIER_SECOND);
		final String idSpinnerYears = UniqueIdentifierUtil.createSpinnerIdentifier(PackagingToolkitConstants.IDENTIFIER_YEAR);
		
		spinnerDays = new Spinner();
		spinnerDays.setMin(0);
		spinnerDays.setId(idSpinnerDays);
		spinnerDays.setValue(0);
		spinnerHours = new Spinner();
		spinnerHours.setMin(0);
		spinnerHours.setId(idSpinnerHours);
		spinnerHours.setValue(0);
		spinnerMinutes = new Spinner();
		spinnerMinutes.setMin(0);
		spinnerMinutes.setId(idSpinnerMinutes);
		spinnerMinutes.setValue(0);
		spinnerMonths = new Spinner();
		spinnerMonths.setMin(0);
		spinnerMonths.setId(idSpinnerMonths);
		spinnerMonths.setValue(0);
		spinnerSeconds = new Spinner();
		spinnerSeconds.setMin(0);
		spinnerSeconds.setId(idSpinnerSeconds);
		spinnerSeconds.setValue(0);
		spinnerYears = new Spinner();
		spinnerYears.setMin(0);
		spinnerYears.setId(idSpinnerYears);
		spinnerYears.setValue(0);
	}
	
	/**
	 * Initializes the output labels with the descriptions of the input components.
	 * @param labelText Is used for creating the unique identifiers for the components. This should avoid
	 * that identifiers are twice on the page. 
	 */
	private void initializeOutputLabels(final String labelText) {
		outputLabelDays = PrimeFacesComponentFactory.createOutputLabel();
		outputLabelHours = PrimeFacesComponentFactory.createOutputLabel();
		outputLabelMinutes = PrimeFacesComponentFactory.createOutputLabel();
		outputLabelMonths = PrimeFacesComponentFactory.createOutputLabel();
		outputLabelNegative = PrimeFacesComponentFactory.createOutputLabel(); 
		outputLabelSeconds = PrimeFacesComponentFactory.createOutputLabel();
		outputLabelYears = PrimeFacesComponentFactory.createOutputLabel();

		final String idLabelDays = UniqueIdentifierUtil.createLabelIdentifier(labelText + PackagingToolkitConstants.IDENTIFIER_DAY);
		final String idLabelHours = UniqueIdentifierUtil.createLabelIdentifier(labelText + PackagingToolkitConstants.IDENTIFIER_HOUR);
		final String idLabelMinutes = UniqueIdentifierUtil.createLabelIdentifier(labelText + PackagingToolkitConstants.IDENTIFIER_MINUTE);
		final String idLabelMonths = UniqueIdentifierUtil.createLabelIdentifier(labelText + PackagingToolkitConstants.IDENTIFIER_MONTH);
		final String idLabelNegative = UniqueIdentifierUtil.createLabelIdentifier(labelText + PackagingToolkitConstants.IDENTIFIER_NEGATIVE);
		final String idLabelSeconds = UniqueIdentifierUtil.createLabelIdentifier(labelText + PackagingToolkitConstants.IDENTIFIER_SECOND);
		final String idLabelYears = UniqueIdentifierUtil.createLabelIdentifier(labelText + PackagingToolkitConstants.IDENTIFIER_YEAR);

		outputLabelDays.setId(idLabelDays);
		outputLabelHours.setId(idLabelHours);
		outputLabelMinutes.setId(idLabelMinutes);
		outputLabelMonths.setId(idLabelMonths);
		outputLabelNegative.setId(idLabelNegative);
		outputLabelSeconds.setId(idLabelSeconds);
		outputLabelYears.setId(idLabelYears);
		
		outputLabelDays.setValue(I18nComponentLabelUtil.getLabelDaysString());
		outputLabelHours.setValue(I18nComponentLabelUtil.getLabelHoursString());
		outputLabelMinutes.setValue(I18nComponentLabelUtil.getLabelMinutesString());
		outputLabelMonths.setValue(I18nComponentLabelUtil.getLabelMonthsString());
		outputLabelNegative.setValue(I18nComponentLabelUtil.getLabelNegativeString());
		outputLabelSeconds.setValue(I18nComponentLabelUtil.getLabelSecondsString());
		outputLabelYears.setValue(I18nComponentLabelUtil.getLabelYearsString());
	}
	
	/**
	 * Puts the components into the panel. The spinners get as default values the values in the
	 * value of the property if there is any.
	 */
	private void initializePanel(final OntologyDuration ontologyDuration) {
		pnlDurationElement = PrimeFacesComponentFactory.createPanelGrid();
		pnlDurationElement.setColumns(COLUMN_COUNT);

		if (ontologyDuration != null) {
			spinnerYears.setValue(ontologyDuration.getYears());
			spinnerMonths.setValue(ontologyDuration.getMonths());
			spinnerDays.setValue(ontologyDuration.getDays());
			spinnerHours.setValue(ontologyDuration.getHours());
			spinnerMinutes.setValue(ontologyDuration.getMinutes());
			spinnerSeconds.setValue(ontologyDuration.getSeconds());
			chkNegative.setValue(ontologyDuration.isNegativeDuration());
		}
		
		pnlDurationElement.getChildren().add(outputLabelYears);
		pnlDurationElement.getChildren().add(spinnerYears);
		pnlDurationElement.getChildren().add(outputLabelMonths);
		pnlDurationElement.getChildren().add(spinnerMonths);
		pnlDurationElement.getChildren().add(outputLabelDays);
		pnlDurationElement.getChildren().add(spinnerDays);
		pnlDurationElement.getChildren().add(outputLabelHours);
		pnlDurationElement.getChildren().add(spinnerHours);
		pnlDurationElement.getChildren().add(outputLabelMinutes);
		pnlDurationElement.getChildren().add(spinnerMinutes);
		pnlDurationElement.getChildren().add(outputLabelSeconds);
		pnlDurationElement.getChildren().add(spinnerSeconds);
		pnlDurationElement.getChildren().add(outputLabelNegative);
		pnlDurationElement.getChildren().add(chkNegative);
		pnlDurationElement.getChildren().add(getTxtPropertyIri());
	}
}
