package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.StringGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;

/**
 * Creates a component for a string value as a InputText from PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class StringGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements StringGuiElement {
	/**
	 * This components represents the value. 
	 */
	private InputText inputText;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	
	/**
	 * Creates a new component object.
	 * @param ontologyStringProperty The property that should be processed with this component.
	 */
	public StringGuiElementPrimeFacesImpl(final OntologyStringProperty ontologyStringProperty) {
		super(ontologyStringProperty);
		final String labelText = ontologyStringProperty.getLabel();
		final String value = ontologyStringProperty.getPropertyValue();
		
		initializeInputText(labelText, value);
		initializePanelGroup();
	}
	
	/**
	 * Initializes the input field. 
	 * @param labelText Is used for creating a identifier for this component.
	 * @param value The value that should be displayed in this component.
	 */
	private void initializeInputText(final String labelText, final String value) {
		final String idInputText = UniqueIdentifierUtil.createInputNumberIdentifier(labelText);
		inputText = PrimeFacesComponentFactory.createInputText();
		inputText.setValue(value);
		inputText.setId(idInputText);
		
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(inputText);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
