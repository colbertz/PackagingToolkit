package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

/**
 * Describes the representation of a taxonomy in the gui.
 * @author Christopher Olbertz
 *
 */
public interface TaxonomyElement extends GuiElement {

}
