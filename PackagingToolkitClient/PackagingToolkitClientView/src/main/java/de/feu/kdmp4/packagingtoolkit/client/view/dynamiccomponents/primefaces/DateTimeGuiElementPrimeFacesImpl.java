package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;

import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlPanelGroup;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.outputlabel.OutputLabel;

import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories.PrimeFacesComponentFactory;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DateTimeGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.PackagingToolkitClientConstants;
import de.feu.kdmp4.packagingtoolkit.client.view.prime.utils.UniqueIdentifierUtil;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;

public class DateTimeGuiElementPrimeFacesImpl extends AbstractPrimeFacesComponent implements DateTimeGuiElement {
	/**
	 * This components represents the value. 
	 */
	private Calendar calendar;
	/**
	 * Contains the components. 
	 */
	private HtmlPanelGroup panelGroup;
	/**
	 * Contains an error message for the component.
	 */
	// TODO Fehlermeldungen eventuell in die Oberklasse herausfaktorisieren.
//	private Message error;
	
	/**
	 * Creates a component for a datetime value as a Calendar from PrimeFaces.
	 * @author Christopher Olbertz
	 *
	 */
	public DateTimeGuiElementPrimeFacesImpl(final OntologyDateTimeProperty ontologyDateTimeProperty) {
		super(ontologyDateTimeProperty);
		final String labelText = ontologyDateTimeProperty.getLabel();
		final LocalDateTime value = ontologyDateTimeProperty.getPropertyValue();
		
		initializeCalendar(labelText, value);
		initializePanelGroup();
	}
	
	/**
	 * Puts the components into the panel. 
	 */
	private void initializePanelGroup() {
		panelGroup = PrimeFacesComponentFactory.createPanelGroup();
		panelGroup.getChildren().add(getOutputLabel());
		panelGroup.getChildren().add(calendar);
		panelGroup.getChildren().add(getTxtPropertyIri());
	}
	
	/**
	 * Initializes the calendar. 
	 * @param labelText Is used for creating a identifier for this component.
	 * @param localDateTime The value that should be displayed in this component.
	 */
	private void initializeCalendar(final String labelText, final LocalDateTime localDateTime) {
		calendar = PrimeFacesComponentFactory.createCalendar();
		final String idCalendar = UniqueIdentifierUtil.createCalendarIdentifier(labelText);
		final Locale currentLocale = Locale.getDefault();
		calendar.setLocale(currentLocale);
		calendar.setId(idCalendar);
		calendar.setPattern(PackagingToolkitClientConstants.DEFAULT_PATTERN_DATE_TIME);
		// error = new Message();
		// error.setFor("frmOntologyTree:" + calendar.getClientId());
		if (localDateTime != null) {
			Date value = DateTimeUtils.convertLocalDateTimeToDate(localDateTime);
			calendar.setValue(value);
		}
	}
	
	@Override
	public OutputLabel getLabel() {
		return super.getOutputLabel();
	}
	
	@Override
	public HtmlPanelGroup getGuiElement() {
		return panelGroup;
	}
	
	@Override
	public HtmlInputHidden getTxtPropertyIri() {
		return super.getTxtPropertyIri();
	}
}
