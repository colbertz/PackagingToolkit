package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces;

/**
 * Represents a component for the user interface that can contain a float value. At the
 * moment, this interface is empty, but it can be used for further development.
 * @author Christopher Olbertz
 *
 */
public interface FloatGuiElement extends GuiElement {

}
