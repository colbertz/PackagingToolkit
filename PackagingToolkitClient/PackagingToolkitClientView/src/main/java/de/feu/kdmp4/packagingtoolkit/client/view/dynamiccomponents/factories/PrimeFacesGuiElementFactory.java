package de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.factories;

import de.feu.kdmp4.packagingtoolkit.client.managers.InformationPackageManager;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Taxonomy;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.DigitalObjectCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.DatatypePropertyCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyClass;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividual;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.OntologyIndividualCollection;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyBooleanProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyByteProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDateTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDoubleProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyDurationProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyFloatProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyIntegerProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyLongProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyShortProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyStringProperty;
import de.feu.kdmp4.packagingtoolkit.client.ontology.model.interfaces.properties.OntologyTimeProperty;
import de.feu.kdmp4.packagingtoolkit.client.service.cache.ServerCache;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ByteGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DateGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DateTimeGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DoubleGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.DurationGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.FloatGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.GuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.IndividualGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.IntegerGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.LongGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ShortGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.TaxonomyElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.TimeGuiElement;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.interfaces.ValueInputComponent;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.BooleanGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.ByteGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.ContainerGuiElementPrimefaces;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.DateGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.DateTimeGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.DigitalObjectGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.DoubleGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.DurationGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.FloatGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.IndividualGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.IntegerGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.LongGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.ShortGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.StringGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.TaxonomyElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.TimeGuiElementPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.dynamiccomponents.primefaces.ValueInputComponentPrimeFacesImpl;
import de.feu.kdmp4.packagingtoolkit.client.view.managers.DynamicGuiManager;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * Creates components for the framework PrimeFaces.
 * @author Christopher Olbertz
 *
 */
public class PrimeFacesGuiElementFactory extends AbstractGuiElementFactory {
	/**
	 * Creates an object of this factory.
	 * @param informationPackageManager Contains information about the information package that is in process.
	 * @param serverCache Contains the object that have been read from the ontologies on the server.
	 * @param dynamicGuiManager Contains information about the gui.
	 */
	public PrimeFacesGuiElementFactory(final InformationPackageManager informationPackageManager, 
			final ServerCache serverCache, final DynamicGuiManager dynamicGuiManager) {
		super(informationPackageManager, serverCache, dynamicGuiManager);
	}

	@Override
	public BooleanGuiElementPrimeFacesImpl createBooleanGuiElement(final OntologyBooleanProperty ontologyBooleanProperty) {
		return new BooleanGuiElementPrimeFacesImpl(ontologyBooleanProperty);
	}
	
	@Override
	public StringGuiElementPrimeFacesImpl createStringGuiElement(final OntologyStringProperty ontologyTextProperty) {
		return new StringGuiElementPrimeFacesImpl(ontologyTextProperty);
	}

	@Override
	public ContainerGuiElementPrimefaces createContainerElement(final OntologyClass ontologyClass, final ServerCache serverCache) {
		final ContainerGuiElementPrimefaces containerGuiElement = new ContainerGuiElementPrimefaces();
		final boolean isTaxonomyStartClass = getServerCache().isTaxonomyStartClass(ontologyClass);
		
		if (isTaxonomyStartClass) {
			final OntologyIndividualCollection individualsOfStartClass = getServerCache().getIndividualsByClass(ontologyClass);
			final OntologyIndividual individualOfStartClass = individualsOfStartClass.getIndividual(0);
			final GuiElement taxonomyGuiElement = createTaxonomyGuiElement(serverCache);
			containerGuiElement.addChildElement(taxonomyGuiElement);
		} else {
			createComponentsForDatatypeProperties(ontologyClass, containerGuiElement);
			createComponentsForObjectProperties(ontologyClass, containerGuiElement);
		}
		
		return containerGuiElement;
	}
	
	/**
	 * Creates components for the datatype properties contained in an ontology class.
	 * @param ontologyClass Contains the datatype properties we want use for the creation of the
	 * components.
	 * @param containerGuiElement Contains the components. The newly created components are put into this container.
	 */
	private void createComponentsForDatatypeProperties(final OntologyClass ontologyClass, 
			final ContainerGuiElementPrimefaces containerGuiElement)  {
		final int datatypePropertyCount = ontologyClass.getDatatypePropertiesCount();
		for (int i = 0; i < datatypePropertyCount; i++) {
			final OntologyProperty ontologyProperty = (OntologyProperty)ontologyClass.getDatatypeProperty(i);
			final GuiElement guiElement = createGuiComponent(ontologyProperty);
			containerGuiElement.addChildElement(guiElement);
		}
	}
	
	/**
	 * Creates components for the object properties contained in an ontology class.
	 * @param ontologyClass Contains the object properties we want use for the creation of the
	 * components.
	 * @param containerGuiElement Contains the components. The newly created components are put into this container.
	 */
	private void createComponentsForObjectProperties(final OntologyClass ontologyClass, 
			final ContainerGuiElementPrimefaces containerGuiElement) {
		final int objectPropertyCount = ontologyClass.getObjectPropertiesCount();
		for (int i = 0; i < objectPropertyCount; i++) {
			final OntologyProperty objectProperty = (OntologyProperty)ontologyClass.getObjectProperty(i);
			// DELETE_ME
			//if(OntologyUtils.isPremisIri(objectProperty.getPropertyRange())) {
				//processPremisClass(objectProperty.getPropertyRange(), containerGuiElement);
			//} else {
			final String rangeAsString = objectProperty.getPropertyRange();
			if (StringValidator.isNotNullOrEmpty(rangeAsString)) {
				final GuiElement guiElement = createGuiComponent(objectProperty);
				containerGuiElement.addChildElement(guiElement);
			}
			//}
		}
	}

	/**
	 * Runs through the data properties of a Premis class and displays them as input elements.
	 * @param iriOfPremisClass The iri of the class. The class has to be a Premis class.
	 * @param containerGuiElement The container element that contains the components of the gui. The elements that are created
	 * with the help of the dataproperties of the Premis class are written into this container.
	 */
	private void processPremisClass(final String iriOfPremisClass, final ContainerGuiElementPrimefaces containerGuiElement) {
		final DatatypePropertyCollection datatypeProperties = getServerCache().getDatatypePropertiesListByClassname(iriOfPremisClass);
		final int datatypePropertyCount = datatypeProperties.getPropertiesCount();
		
		for (int i = 0; i < datatypePropertyCount; i++) {
			final OntologyProperty ontologyProperty = datatypeProperties.getDatatypeProperty(i);
			final GuiElement guiElement = createGuiComponent(ontologyProperty);
			containerGuiElement.addChildElement(guiElement);
		}
	}

	@Override
	protected IntegerGuiElement createIntegerGuiElement(final OntologyIntegerProperty ontologyIntegerElement) {
		return new IntegerGuiElementPrimeFacesImpl(ontologyIntegerElement);
	}

	@Override
	protected DoubleGuiElement createDoubleGuiElement(final OntologyDoubleProperty ontologyDoubleProperty) {
		return new DoubleGuiElementPrimeFacesImpl(ontologyDoubleProperty);
	}

	@Override
	public ValueInputComponent createValueInputComponent(final OntologyClass ontologyClass) {
		return new ValueInputComponentPrimeFacesImpl(ontologyClass, getInformationPackageManager(), 
				getServerCache(), getDynamicGuiManager());
	}

	@Override
	protected DateTimeGuiElement createDateTimeGuiElement(final OntologyDateTimeProperty ontologyDateTimeProperty) {
		return new DateTimeGuiElementPrimeFacesImpl(ontologyDateTimeProperty);
	}

	@Override
	protected DurationGuiElement createDurationGuiElement(final OntologyDurationProperty ontologyDurationProperty) {
		return new DurationGuiElementPrimeFacesImpl(ontologyDurationProperty);
	}

	@Override
	protected DateGuiElement createDateGuiElement(final OntologyDateProperty ontologyDateProperty) {
		return new DateGuiElementPrimeFacesImpl(ontologyDateProperty);
	}

	@Override
	protected TimeGuiElement createTimeGuiElement(final OntologyTimeProperty ontologyTimeProperty) {
		return new TimeGuiElementPrimeFacesImpl(ontologyTimeProperty);
	}

	@Override
	protected ByteGuiElement createByteGuiElement(final OntologyByteProperty ontologyByteProperty) {
		return new ByteGuiElementPrimeFacesImpl(ontologyByteProperty);
	}

	@Override
	protected FloatGuiElement createFloatGuiElement(final OntologyFloatProperty ontologyFloatProperty) {
		return new FloatGuiElementPrimeFacesImpl(ontologyFloatProperty);
	}

	@Override
	protected LongGuiElement createLongGuiElement(final OntologyLongProperty ontologyLongProperty) {
		return new LongGuiElementPrimeFacesImpl(ontologyLongProperty);
	}

	@Override
	protected ShortGuiElement createShortGuiElement(final OntologyShortProperty ontologyShortProperty) {
		return new ShortGuiElementPrimeFacesImpl(ontologyShortProperty);
	}

	@Override
	protected IndividualGuiElement createIndividualGuiElement(final OntologyIndividualCollection individualList, 
			final OntologyClass ontologyClass, final String propertyIri) {
		return new IndividualGuiElementPrimeFacesImpl(individualList, ontologyClass, 
				getInformationPackageManager(), getDynamicGuiManager(), propertyIri);
	}

	@Override
	public GuiElement createTaxonomyGuiElement(final ServerCache serverCache) {
		/*final OntologyObjectProperty hasTaxonomyProperty = ontologyIndividual.getObjectPropertyAt(0);
		OntologyIndividualList taxonomyRootIndividuals = hasTaxonomyProperty.getPropertyValue();
		String iriOfOntologyClassOfRange = hasTaxonomyProperty.getPropertyRange(); 
		iriOfOntologyClassOfRange = StringUtils.deleteDoubleSharps(iriOfOntologyClassOfRange); 
		
		if (taxonomyRootIndividuals == null || taxonomyRootIndividuals.isEmpty()) {
			taxonomyRootIndividuals = getServerCache().getIndividualsByClass(iriOfOntologyClassOfRange);
		}*/
		
	//	if (taxonomyRootIndividuals.isNotEmpty()) {
			final Taxonomy taxonomy = getServerCache().findTaxonomyWithTitle();
			if (taxonomy != null) { 
				final TaxonomyElement taxonomyElement = new TaxonomyElementPrimeFacesImpl(taxonomy, getInformationPackageManager(), serverCache);
				return taxonomyElement;
		//	}
			/*final OntologyIndividual taxonomyRootIndividual = taxonomyRootIndividuals.getIndividual(0); 
			
			final Iri iriOfIndividual = taxonomyRootIndividual.getIriOfIndividual();
			final TaxonomyIndividual rootIndividual = ClientModelFactory.createTaxonomyIndividual(iriOfIndividual);
			final Optional<Taxonomy> optionalWithTaxonomyForGuiElement = getServerCache().getTaxonomyByRootIndividual(rootIndividual);
			if (optionalWithTaxonomyForGuiElement.isPresent()) {
				final Taxonomy taxonomy = optionalWithTaxonomyForGuiElement.get();
				final TaxonomyElement taxonomyElement = new TaxonomyElementPrimeFacesImpl(taxonomy, getInformationPackageManager());
				return taxonomyElement;
			}*/
		}
		
		return null;
	}

	@Override
	public GuiElement createGuiElementsForDigitalObject(final InformationPackageManager informationPackageManager) {
		final GuiElement guiElement = new DigitalObjectGuiElementPrimeFacesImpl(informationPackageManager);
		ContainerGuiElementPrimefaces container = new ContainerGuiElementPrimefaces();
		container.addChildElement(guiElement);
		return container;
	}
}	
