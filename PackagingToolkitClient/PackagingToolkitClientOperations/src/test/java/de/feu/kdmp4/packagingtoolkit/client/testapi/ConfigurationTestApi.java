package de.feu.kdmp4.packagingtoolkit.client.testapi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Contains methods for preparing the tests for the configuration classes.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationTestApi {
	private static final String CONFIGURATION_FILE_NAME = "config.properties";
	/**
	 * The name of the file with the logo for the tests.
	 */
	private static final String LOGO_TEST_NAME = "logo.png";
	/**
	 * The url of the mediator for the tests.
	 */
	private static final String MEDIATOR_TEST_URL = "http://localhost:8181";
	/**
	 * The url of the server for the tests.
	 */
	private static final String SERVER_TEST_URL = "http://localhost:8282";
	/**
	 * The name of the directory that contains the images for the client. 
	 */
	private static final String IMAGE_DIRECTORY = "images";
	/**
	 * The property in the configuration file that saves the url of the mediator.
	 */
	private static final String PROPERTY_MEDIATOR_URL = "mediatorUrl";
	/**
	 * The property in the configuration file that contains the name of the file with the logo. The
	 * file is saved in the images directory of the client.
	 */
	private static final String PROPERTY_LOGO_File = "logoFile";
	/**
	 * The property in the configuration file that saves the url of the server.
	 */
	private static final String PROPERTY_SERVER_URL = "serverUrl";
	
	/**
	 * Creates a configuration file for the tests.
	 * @param path The path of the directory where the test file should be created in.
	 * @return The test file.
	 * @throws IOException 
	 */
	public static final File createTestConfgurationFile(final String path) throws IOException {
		final String filePath = path + File.separator + CONFIGURATION_FILE_NAME;
		final File configurationFile = new File(filePath);
		configurationFile.createNewFile();
		
		final Properties properties = new Properties();
		properties.setProperty(PROPERTY_SERVER_URL, SERVER_TEST_URL);
		properties.setProperty(PROPERTY_MEDIATOR_URL, MEDIATOR_TEST_URL);
		properties.setProperty(PROPERTY_LOGO_File, LOGO_TEST_NAME); 
		final File propertiesFile = new File(filePath);
		
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(propertiesFile);
		} catch (FileNotFoundException e) {
			propertiesFile.createNewFile();
		}
		properties.store(outputStream, "");
		
		return configurationFile;
	}
	
	/**
	 * Returns the expected logo path.
	 * @return The logo path we are expecting the the tests.
	 */
	public static final File getTestLogo() {
		final File logoFile = new File(IMAGE_DIRECTORY, LOGO_TEST_NAME);
		return logoFile;
	}
}
