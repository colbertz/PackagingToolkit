package de.feu.kdmp4.packagingtoolkit.client.operations.classes;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ClientConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.client.testapi.ConfigurationTestApi;

public class ConfigurationOperationsTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	/**
	 * The name of the directory that contains the configuration files.
	 */
	private static final String CONFIGURATION_DIRECTORY = "configuration";
	
	@Test
	public void testGetLogoPath() throws IOException {
		final String configurationDirectory = temporaryFolder.newFolder(CONFIGURATION_DIRECTORY).getAbsolutePath();
		ConfigurationTestApi.createTestConfgurationFile(configurationDirectory);
		final ClientConfigurationOperations clientConfigurationOperations = new ConfigurationOperationsPropertyImpl();
		final File expectedLogo = ConfigurationTestApi.getTestLogo();
		
		final File actualLogo = clientConfigurationOperations.getLogoPath(configurationDirectory);
		
		assertEquals(expectedLogo, actualLogo);
	}
}
