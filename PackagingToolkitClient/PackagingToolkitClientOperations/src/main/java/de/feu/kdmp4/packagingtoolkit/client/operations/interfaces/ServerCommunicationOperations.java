package de.feu.kdmp4.packagingtoolkit.client.operations.interfaces;

import java.io.File;
import java.io.InputStream;

import de.feu.kdmp4.packagingtoolkit.client.exceptions.ConnectionException;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

/**
 * Contains the operations that are used for communicating with the server. 
 * @author Christopher Olbertz
 *
 */
public interface ServerCommunicationOperations {
	/**
	 * Sends the configuration data for the server to the server.
	 * @param configurationData The configuration data to send.
	 * @param serverUrl The url of the server.
	 */
	void writeServerConfiguration(final ServerConfigurationDataResponse configurationData, final String serverUrl);
	/**
	 * Reads the configuration of the server via a rest request.
	 * @param serverUrl The url the request is send to.
	 * @return The configuration data of the server.
	 */
	ServerConfigurationDataResponse readServerConfiguration(final String serverUrl);
	/**
	 * Uploads the rule ontology on the server.
	 * @param ontologyFile The file that points to the rule ontology in the temporary directory of the server.
	 * @param serverUrl The url of the server.
	 */
	void configureRuleOntology(final File ontologyFile, final String serverUrl);
	/**
	 * Uploads a base ontology on the server.
	 * @param ontologyFile The file that points to the base ontology in the temporary directory of the server.
	 * @param serverUrl The url of the server.
	 */
	void configureBaseOntology(final File ontologyFile, final String serverUrl);
	/**
	 * Determines a list with all archived information packages on the server.
	 * @param serverUrl The url of the server.
	 * @return Contains all information packages on the server.
	 */
	ArchiveListResponse getArchiveList(final String serverUrl);
	/**
	 * Deletes an archived information package on the server. 
	 * @param uuid The uuid of the information package that should be deleted.
	 * @param serverUrl The url of the server.
	 */
	void deleteArchive(final Uuid uuid, final String serverUrl);
	/**
	 * Gets a list with all saved views on the server. The views are related to the
	 * base ontology configured on the server. 
	 * @return A list wiht all view related to the configured base ontology.
	 */
	ViewListResponse getAllViews(final String serverUrl);
	/**
	 * Downloads the base ontology as owl file from the server.
	 * @param serverUrl The url of the server.
	 * @param downloadDirectory The directory the owl file is downloaded to.
	 * @return A file object that points to the downloaded file.
	 */
	File downloadBaseOntology(final String serverUrl, final File downloadDirectory);
	/**
	 * Saves a new view on the server. 
	 * @param serverUrl The url of the server.
	 * @param viewResponse Contains the data of the view that should be saved.
	 */
	void saveNewView(final String serverUrl, final ViewResponse viewResponse);
	/**
	 * Determines the class hierarchy in the base ontology for some classes.
	 * @param serverUrl The url of the server. 
	 * @param classnames The names of the classes whose subclasses have to be 
	 * determined.
	 * @return The configured root class. Its subclasses are the classes whose names
	 * were given to this method. They contain their hierarchy.
	 */
	OntologyClassResponse getHierarchy(final String serverUrl, final StringListResponse classnames);
	/**
	 * Determins the class hierarchy in the base ontology for all classes.
	 * @param serverUrl The url of the server.
	 * @return
	 */
	OntologyClassResponse getHierarchy(final String serverUrl);
	/**
	 * Requests the properties of a given class from the server.
	 * @param classname The name of the class whose properties are requested.
	 * @return The class with its properties.
	 */
	OntologyClassResponse getPropertiesOfClass(final String serverUrl, final String classname);
	/**
	 * Determines the individuals of the given class that are saved in the triple store
	 * for the information package with the uuid uuidOfInformationPackage.
	 * @param ontologyClassResponse The class whose individuals are searched.
	 * @param uuidOfInformationPackage The uuid of the information package, whose data are
	 * searched.
	 * @return The list with the found individuals. Can be empty if there were not 
	 * individuals found. 
	 */
	IndividualListResponse getSavedIndividuals(final String serverUrl, final OntologyClassResponse ontologyClassResponse,
			final UuidResponse uuidOfInformationPackage);
	/**
	 * Demands the digital objects of an information package.
	 * @param uuid The uuid of the information package.
	 * @return A list with the digital objects of the information package or an empty list.
	 */
	DigitalObjectListResponse getDigitalObjectsOfInformationPackage(final UuidResponse uuid);
	void setClientModelFactory(final ClientModelFactory clientModelFactory);
	/**
	 * Creates a new submission information unit (siu) on the server.
	 * @param serverUrl The url of the server.
	 * @param title The title of the siu.
	 * @return A package response that contains the data of the siu send by the server.
	 */
	PackageResponse createSubmissionInformationUnit(final String serverUrl, final String title);
	/**
	 * Creates a new submission information package (sip) on the server.
	 * @param serverUrl The url of the server.
	 * @param title The title of the sip.
	 * @return A package response that contains the data of the sip send by the server.
	 */
	PackageResponse createSubmissionInformationPackage(final String serverUrl, final String title);
	/**
	 * Request the zip file of a virtual archive for download.
	 * @param archiveResponse Describes the archive and the required information for building the zip
	 * file.
	 * @param serverUrl The url of the server the archive is requested from.
	 * @return An input stream that contains the data of the files for download.
	 */
	InputStream downloadVirtualArchive(final ArchiveResponse archiveResponse, final String serverUrl);
	/**
	 * Request the zip file of a  not virtual archive for download.
	 * @param archiveResponse Describes the archive and the required information for building the zip
	 * file.
	 * @param serverUrl The url of the server the archive is requested from.
	 * @return An input stream that contains the data of the files for download.
	 */
	InputStream downloadNotVirtualArchive(final ArchiveResponse archiveResponse, final String serverUrl);
	/**
	 * Adds a http reference to a certain information package.
	 * @param referenceUrl The url the reference is pointing to. 
	 * @param uuidOfInformationPackage The uuid of the information package the reference is added to.
	 * @param serverUrl The url of the server.
	 */
	void addHttpReference(final ReferenceResponse referenceResponse, final String serverUrl);
	/**
	 * Sends a digital object to the server to create a file reference. If the file does not exist in the storage of
	 * the server, it is uploaded to the server. If it exists in the the storage only the reference to the file is
	 * created in the information package.
	 * @param digitalObjectResponse
	 * @param serverUrl
	 * @return True If the reference has been created, false if the file is not present on the server and the
	 * reference has not been created.
	 */
	boolean addFileReference(final DigitalObjectResponse digitalObjectResponse, final String serverUrl);
	/**
	 * Uploads a file to the storage directory of the server.
	 * @param file The file that should be uploaded.
	 * @param serverUrl The url of the server.
	 */
	void uploadFileToStorage(final File file, final String serverUrl);
	/**
	 * Gets the individuals that are assigned to a given class only in the base ontology. The 
	 * triple store is not queried. 
	 * @param serverUrl The url of the server.
	 * @param classname The name of the class whose individuals are requested.
	 * @return A list with the individuals of the class.
	 */
	IndividualListResponse findPredefinedIndividualsByClass(final String serverUrl, final String iriOfClass);
	/**
	 * Sends an individual to the server for being checked by the reasoner and for saving.
	 * @param serverUrl The url of the server the individual is send to.
	 * @param ontologyIndividual The individual.
	 */
	void saveIndividual(final String serverUrl, final IndividualResponse ontologyIndividual);
	/**
	 * Sends the signal to the server that the created information package should be saved.
	 * @param uuidOfInformationPackage The uuid of the information package that should be saved.
	 * @param serverUrl The url of the server the individual is send to.
	 */
	void saveInformationPackage(final UuidResponse uuidOfInformationPackage, final String serverUrl);
	/**
	 * Sends a signal to the server to test if the server is accessible. 
	 * @param serverUrl The url of the server.
	 * @throws ConnectionException if the server does not respond.
	 */
	void pingServer(final String serverUrl);
	/**
	 * Updates an individual with new values on the server.
	 * @param serverUrl The url of the server.
	 * @param ontologyIndividual The individual that should be updated.
	 */
	void updateIndividual(final String serverUrl, final IndividualResponse ontologyIndividual);
	/**
	 * Requests the references of an information package from the server.
	 * @param serverUrl The url of the server.
	 * @param uuidOfInformationPackage The uuid of the information package whose references are requested.
	 * @return A list that contains the found references. If there are not any references assigned to this information
	 * package the list is empty.
	 */
	ReferenceListResponse findReferencesOfInformationPackage(final String serverUrl, final String uuidOfInformationPackage);
	/**
	 * Returns all classes contained in the Premis file that is saved on the server as subclasses of a class named Premis.
	 * @param serverUrl The url of the server.
	 * @return All classes contained in Premis.
	 */
	OntologyClassResponse getAllPremisClasses(final String serverUrl);
	/**
	 * Returns all classes contained in the SKOS file that saved on the server as subclasses of a class named Skos.
	 * @param serverUrl The url of the server.
	 * @return All classes contained in SKOS.
	 */
	OntologyClassResponse getAllSkosClasses(final String serverUrl);
	/**
	 * If the creation of an information package has been cancelled, the server is being informed. The server has to delete all
	 * artifacts that have been creation while the creation process. 
	 * @param uuid The uuid of the information package that should be deleted.
	 * @param serverUrl The url of the server.
	 */
	void cancelInformationPackageCreation(final Uuid uuid, final String serverUrl);
	/**
	 * Loads an information package so that the user can edit its data.
	 * @param uuid The uuid of the information package that should be load.
	 * @param serverUrl The url of the server.
	 */
	void loadInformationPackage(final String uuid, final String serverUrl);
	/**
	 * Sends a request for deleting a view to the server.
	 * @param viewId The id of the view to delete.
	 * @param serverUrl The url of the server.
	 */
	void deleteView(final int viewId, final String serverUrl);
	/**
	 * Determines the individuals of the given class that are saved in the triple store.
	 * @param ontologyClassResponse The class whose individuals are searched.
	 * @return The list with the found individuals. Can be empty if there were not 
	 * individuals found. 
	 */
	IndividualListResponse findSavedIndividualsByClass(final String serverUrl, final OntologyClassResponse ontologyClassResponse);
	/**
	 * Adds meta data to an information package.
	 * @param serverUrl The url of the server.
	 * @param mediatorData Contains the data that should be send to the server and the information about the
	 * information package.
	 */
	void addMetadataToInformationPackage(String serverUrl, MediatorDataListResponse mediatorData);
	/**
	 * Determines all taxonomies defined in the base ontology on the server.
	 * @param serverUrl The url of the server.
	 * @return All found taxonomies.
	 */
	TaxonomyListResponse getAllTaxonomies(String serverUrl);
	/**
	 * Determines the class the taxonomies start with.
	 * @param serverUrl The url of the server.
	 * @return The class the taxonomies start with.
	 */
	OntologyClassResponse getTaxonomyStartClass(String serverUrl);
	/**
	 * Saves individuals of a taxonomy as selected for an information package.
	 * @param taxonomyIndividuals The individuals that have been selected for the information package.
	 * @param serverUrl The url of the server.
	 */
	void saveSelectedTaxonomyIndividuals(TaxonomyIndividualListResponse taxonomyIndividuals, String serverUrl);
	/**
	 * Forces the initialization of the ontology cache. Can be used if a new base ontology has been uploaded.
	 */
	void updateOntologyCache(String serverUrl);
	/**
	 * Determines all individuals that are assigned to a given information package.
	 * @param serverUrl The url of the server.
	 * @param uuidOfInformationPackage The uuid of the information package whose individuals 
	 * we want to determine.
	 * @return All individuals that are assigned to this information package.
	 */
	IndividualListResponse findAllIndividualsOfInformationPackage(String serverUrl,
			UuidResponse uuidOfInformationPackage);
	/**
	 * Sends the uuids of some indidivuals to the server. These individuals should be assigned to
	 * a certain information package.
	 * @param uuidsResponse Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals.
	 * @param serverUrl The url of the server.
	 */
	void assignIndividualsToInformationPackage(UuidListResponse uuidsResponse, String serverUrl);
	/**
	 * Sends the uuids of some indidivuals to the server. These individuals should be unassigned from
	 * a certain information package.
	 * @param uuidsResponse Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals.
	 * @param serverUrl The url of the server.
	 */
	void unassignIndividualsFromInformationPackage(UuidListResponse uuidsResponse, String serverUrl);
	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param irisOfIndividuals The iris of the individuals from the taxonomy that should be 
	 * assigned to the information package. The first entry is the iri of the information package.
	 */
	void assignTaxonomyIndividuals(IriListResponse irisOfTaxonomyIndividuals, String serverUrl);
	/**
	 * Requests all taxonomy individuals that are assigned to a certain information package from 
	 * the server.
	 * @param uuidOfInformationPackage The uuid of the information package whose taxonomy 
	 * individuals we want to request.
	 * @param serverUrl The url of the server.
	 * @return The iris of the taxonomy individuals that are assigned to the information package.
	 */
	IriListResponse findAllAssignedTaxonomyIndividuals(String serverUrl,
			UuidResponse uuidOfInformationPackageResponse);
}
