package de.feu.kdmp4.packagingtoolkit.client.operations.classes;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import de.feu.kdmp4.packagingtoolkit.client.exceptions.ConnectionException;
import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ServerCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.informationpackage.ViewException;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidAndIriResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;
import de.feu.kdmp4.packagingtoolkit.utils.UrlUtils;

public class ServerCommunicationOperationsImpl implements ServerCommunicationOperations {
	private RestTemplate restTemplate;
	private ClientModelFactory clientModelFactory;

	@Override
	public PackageResponse createSubmissionInformationUnit(final String serverUrl, final String title)  {
		return null;
	}
	
	@Override
	public void updateOntologyCache(final String serverUrl) {
		try {
			final String targetAddress = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + 
				PathConstants.SERVER_UPDATE_ONTOLOGY_CACHE;
			restTemplate.getForObject(targetAddress, String.class);
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public IndividualListResponse findAllIndividualsOfInformationPackage(final String serverUrl,
			final UuidResponse uuidOfInformationPackage) {
		try {
			final String targetAddress = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE +  
				PathConstants.SERVER_FIND_ALL_INDIVIDUALS_OF_INFORMATION_PACKAGE;
			restTemplate = getRestTemplate();
			final IndividualListResponse individuals = restTemplate.postForObject(targetAddress, uuidOfInformationPackage, IndividualListResponse.class);
			return individuals;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public ViewListResponse getAllViews(final String serverUrl) {
		try {
			final String targetAddress = serverUrl + PathConstants.SERVER_VIEWS_INTERFACE + 
				PathConstants.SERVER_GET_ALL_VIEWS;
			final ViewListResponse views = restTemplate.getForObject(targetAddress, ViewListResponse.class);
			return views;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public TaxonomyListResponse getAllTaxonomies(final String serverUrl) {
		try {
			final String targetAddress = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + 
				PathConstants.SERVER_FIND_ALL_TAXONOMIES;
			restTemplate = getRestTemplate();
			final TaxonomyListResponse taxonomies = restTemplate.getForObject(targetAddress, TaxonomyListResponse.class);
			return taxonomies;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public OntologyClassResponse getTaxonomyStartClass(final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.SERVER_GET_TAXONOMY_START_CLASS;
			restTemplate = getRestTemplate();
			final OntologyClassResponse ontologyClass = restTemplate.getForObject(url, OntologyClassResponse.class);
			return ontologyClass;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void pingServer(final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_CONFIGURATION_INTERFACE + PathConstants.SERVER_PING;
			restTemplate = getRestTemplate();
			final String responseOfServer = restTemplate.getForObject(url, String.class);
			
			if (StringUtils.areStringsUnequal(responseOfServer, PackagingToolkitConstants.I_AM_HERE)) {
				final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
				throw connectionException;
			}
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public ServerConfigurationDataResponse readServerConfiguration(final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_CONFIGURATION_INTERFACE + PathConstants.SERVER_GET_CONFIGURATION_DATA;
			restTemplate = getRestTemplate();
			final ServerConfigurationDataResponse serverConfigurationData = restTemplate.getForObject(url, ServerConfigurationDataResponse.class);
			return serverConfigurationData;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void configureBaseOntology(final File ontologyFile, final String serverUrl) {
		try{
			final MultiValueMap<String, Object> parameterMap = createMultipartFileParam(ontologyFile.getAbsolutePath());

			final HttpHeaders fileHeader = new HttpHeaders();
			fileHeader.setContentType(MediaType.MULTIPART_FORM_DATA);
			
			final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameterMap, fileHeader);
			
			final String url = serverUrl + PathConstants.SERVER_CONFIGURATION_INTERFACE + PathConstants.SERVER_UPLOAD_BASE_ONTOLOGY;
			restTemplate = getRestTemplate();
			final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				
			}
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		} finally {
			// Delete the file from the temporary directory of the client.
			ontologyFile.delete();
		}	
	}

	@Override
	public void uploadFileToStorage(final File file, final String serverUrl) {
		try{
			final MultiValueMap<String, Object> parameterMap = createMultipartFileParam(file.getAbsolutePath());
			parameterMap.add("file", file);

			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			
			final String url = serverUrl + PathConstants.SERVER_STORAGE_INTERFACE + PathConstants.SERVER_UPLOAD_FILE;
			restTemplate = getRestTemplate();
			final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<MultiValueMap<String, Object>>(parameterMap, headers), 
				    String.class);
			System.out.println(response.getHeaders().getAccept());
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		} finally {
			// Delete the file from the temporary directory of the client.
			file.delete();
		}
	}
	
	private MultiValueMap<String, Object> createMultipartFileParam(final String filePath) {
		final MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();    
		final File file = new File(filePath);
        parts.add("file", new FileSystemResource(file));
        return parts;
	}
	
	@Override
	public void configureRuleOntology(final File ontologyFile, final String serverUrl) {
		try{
			final MultiValueMap<String, Object> parameterMap = createMultipartFileParam(ontologyFile.getAbsolutePath());

			final HttpHeaders fileHeader = new HttpHeaders();
			fileHeader.setContentType(MediaType.MULTIPART_FORM_DATA);
			
			final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameterMap, fileHeader);
			
			final String url = serverUrl + PathConstants.SERVER_CONFIGURATION_INTERFACE + PathConstants.SERVER_UPLOAD_RULE_ONTOLOGY;
			restTemplate = getRestTemplate();
			final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				
			}
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		} finally {
			// Delete the file from the temporary directory of the client.
			ontologyFile.delete();
		}	
	}
	
	@Override
	public void deleteArchive(final Uuid uuid, final String serverUrl) {
		String url = serverUrl + PathConstants.SERVER_STORAGE_INTERFACE + PathConstants.SERVER_DELETE_ARCHIVE;
		url = UrlUtils.replaceUrlParameters(url, ParamConstants.PARAM_UUID, String.valueOf(uuid.toString()));
		restTemplate = getRestTemplate();
		restTemplate.delete(url);
	}
	
	@Override
	public void deleteView(final int viewId, final String serverUrl) {
		String url = serverUrl + PathConstants.SERVER_VIEWS_INTERFACE + PathConstants.SERVER_DELETE_VIEW;
		url = UrlUtils.replaceUrlParameters(url, ParamConstants.PARAM_VIEW_ID, String.valueOf(viewId));
		restTemplate = getRestTemplate();
		restTemplate.delete(url);
	}
	
	@Override
	public void cancelInformationPackageCreation(final Uuid uuid, final String serverUrl) {
		String url = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_CANCEL_INFORMATION_PACKAGE_CREATION;
		url = UrlUtils.replaceUrlParameters(url, ParamConstants.PARAM_UUID, String.valueOf(uuid.toString()));
		restTemplate = getRestTemplate();
		restTemplate.delete(url);
	}
	
	@Override
	public File downloadBaseOntology(final String serverUrl, final File downloadDirectory) {
		// TODO Braucht noch eine Implementierung.
		return null;
	}
	
	@Override
	public void saveNewView(final String serverUrl, final ViewResponse viewResponse) {
		try {
			final String targetUrl = serverUrl + PathConstants.SERVER_VIEWS_INTERFACE + PathConstants.SERVER_SAVE_NEW_VIEW;
			restTemplate = getRestTemplate();
			
			final HttpEntity<ViewResponse> request = new HttpEntity<>(viewResponse);
			
			restTemplate.exchange(targetUrl, HttpMethod.POST, request, String.class);
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		} catch (HttpClientErrorException exception) {
			final String message = exception.getResponseBodyAsString();
			throw new ViewException(message);
		}
	}
	
	@Override
	public void addHttpReference(final ReferenceResponse referenceResponse, final String serverUrl) {
		try {
			final String targetUrl = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_ADD_HTTP_REFERENCE;
			restTemplate = getRestTemplate();
			final HttpEntity<ReferenceResponse> request = new HttpEntity<>(referenceResponse);
			
			final ResponseEntity<String> response = restTemplate.exchange(targetUrl, HttpMethod.POST, request, String.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			} else {
			}
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}

	}

	@Override
	public boolean addFileReference(final DigitalObjectResponse digitalObjectResponse, final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_ADD_FILE_REFERENCE;
			restTemplate = getRestTemplate();
			final HttpEntity<DigitalObjectResponse> request = new HttpEntity<>(digitalObjectResponse);
			final ResponseEntity<Boolean> response = restTemplate.exchange(url, HttpMethod.POST, request, Boolean.class);
			return response.getBody();
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void addMetadataToInformationPackage(final String serverUrl, final MediatorDataListResponse mediatorData) {
		try {
			final String url = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_RECEIVE_METADATA;
			restTemplate = getRestTemplate();
			final HttpEntity<MediatorDataListResponse> request = new HttpEntity<>(mediatorData);
			final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public OntologyClassResponse getHierarchy(final String serverUrl, final StringListResponse classnames) {
		try {
			final String targetUrl = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.SERVER_READ_HIERARCHY_WITH_CLASS_NAMES;
			restTemplate = getRestTemplate();
			final HttpEntity<StringListResponse> request = new HttpEntity<>(classnames);
			
			final ResponseEntity<OntologyClassResponse> response = restTemplate.exchange(targetUrl, HttpMethod.POST, request, OntologyClassResponse.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
				return response.getBody();
			} else {
				
			}
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}

		return null;
	}

	@Override
	public OntologyClassResponse getHierarchy(final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.SERVER_READ_HIERARCHY;
			restTemplate = getRestTemplate();
			final OntologyClassResponse ontologyClass = restTemplate.getForObject(url, OntologyClassResponse.class);
			return ontologyClass;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public OntologyClassResponse getPropertiesOfClass(final String serverUrl, final String classname) {
		try { 
			final String targetAddress = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + 
					PathConstants.SERVER_GET_PROPERTIES_OF_CLASS;
			final MultiValueMap<String, String> parameterMap =  new LinkedMultiValueMap<>();
			parameterMap.add(ParamConstants.PARAM_CLASSNAME, classname);
			restTemplate = getRestTemplate();
			final HttpEntity<String> request = new HttpEntity<>(classname);
			final URI targetUri = new URI(targetAddress);
			UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(targetUri).queryParam
					(ParamConstants.PARAM_CLASSNAME, classname);
			System.out.println(("DEBUG: " + classname));
			final ResponseEntity<OntologyClassResponse> response = restTemplate.exchange(uriComponentsBuilder.build().encode().toUri(), HttpMethod.GET, request,
					OntologyClassResponse.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			}
			final OntologyClassResponse ontologyClass = response.getBody();
			return ontologyClass;
		} catch (HttpClientErrorException | URISyntaxException ex) {
			ex.printStackTrace();
			return null;
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public PackageResponse createSubmissionInformationPackage(final String serverUrl, final String title)  {
		try { 
			final String targetAddress = serverUrl + 	PathConstants.SERVER_PACKAGING_INTERFACE + 
					PathConstants.SERVER_CREATE_SUBMISSION_INFORMATION_PACKAGE;
			
			final MultiValueMap<String, String> parameterMap =  new LinkedMultiValueMap<>();
			parameterMap.add("title", title);
			restTemplate = getRestTemplate();
			final HttpEntity<String> request = new HttpEntity<>(title);
			final URI targetUri = new URI(targetAddress);
			final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(targetUri).queryParam("title", title);
			System.out.println(("DEBUG: " + uriComponentsBuilder.build().encode().toUri()));
			final ResponseEntity<PackageResponse> response = restTemplate.exchange(uriComponentsBuilder.build().encode().toUri(), HttpMethod.GET, request,
					PackageResponse.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			}
			final PackageResponse packageResponse = response.getBody();
			return packageResponse;
		} catch (HttpClientErrorException | URISyntaxException ex) {
			ex.printStackTrace();
			return null;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}

	@Override
	public void saveIndividual(final String serverUrl, final IndividualResponse ontologyIndividual) {
		ResponseEntity<String> response = null;
		
		try {
			final String url = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_ADD_INDIVIDUAL_TO_INFORMATION_PACKAGE;
			restTemplate = getRestTemplate();
			final HttpEntity<IndividualResponse> request = new HttpEntity<>(ontologyIndividual);
			response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
			
			if (response.getStatusCode() != HttpStatus.OK) {
				final String reasonerErrors = response.getBody();
				final OntologyReasonerException exception = OntologyReasonerException.createIndividualInvalidException(reasonerErrors);
				throw exception;
			}
		} catch (final ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		} catch (final HttpStatusCodeException ex) {
			final String reasonerErrors = ex.getResponseBodyAsString();
			final OntologyReasonerException exception = OntologyReasonerException.createIndividualInvalidException(reasonerErrors);
			throw exception;
		}
	}
	
	@Override
	public void updateIndividual(final String serverUrl, final IndividualResponse ontologyIndividual) {
		try {
			final String url = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_UPDATE_INDIVIDUAL_IN_INFORMATION_PACKAGE;
			restTemplate = getRestTemplate();
			final HttpEntity<IndividualResponse> request = new HttpEntity<>(ontologyIndividual);
			final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			} else {
			}
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override 
	public void saveInformationPackage(final UuidResponse uuidOfInformationPackage, final String serverUrl) {
		try {
			final String targetUrl = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_SAVE_INFORMATION_PACKAGE;
			restTemplate = getRestTemplate();
			final HttpEntity<UuidResponse> request = new HttpEntity<>(uuidOfInformationPackage);
			
			final ResponseEntity<String> response = restTemplate.exchange(targetUrl, HttpMethod.POST, request, String.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			} else {
			}
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void saveSelectedTaxonomyIndividuals(final TaxonomyIndividualListResponse taxonomyIndividuals, final String serverUrl) {
		try {
			final String targetUrl = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.SERVER_SAVE_SELECTED_TAXONOMY_INDIVIDUALS;
			restTemplate = getRestTemplate();
			final HttpEntity<TaxonomyIndividualListResponse> request = new HttpEntity<>(taxonomyIndividuals);
			
			final ResponseEntity<String> response = restTemplate.exchange(targetUrl, HttpMethod.POST, request, String.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			} else {
			}
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void writeServerConfiguration(final ServerConfigurationDataResponse configurationData,
			final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_CONFIGURATION_INTERFACE + PathConstants.SERVER_CONFIGURE_SERVER;
			restTemplate = getRestTemplate();
			final HttpEntity<ServerConfigurationDataResponse> request = new HttpEntity<>(configurationData);
			final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			} else {
			}
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}

	}

	@Override
	public IndividualListResponse findSavedIndividualsByClass(final String serverUrl, final OntologyClassResponse ontologyClassResponse) {
		try {
			final String url = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.SERVER_FIND_INDIVIDUALS_OF_CLASS;
			final String ontologyClassIri = ontologyClassResponse.getIri();
			
			final HttpEntity<String> request = new HttpEntity<>(ontologyClassIri);
			final URI targetUri = new URI(url);
			final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(targetUri).queryParam
					(ParamConstants.PARAM_CLASSNAME, ontologyClassIri);
			
			restTemplate = getRestTemplate();
			final ResponseEntity<IndividualListResponse> response = restTemplate.exchange(uriComponentsBuilder.build().encode().toUri(), HttpMethod.GET, request,
					IndividualListResponse.class);

			return response.getBody();
		} catch (ResourceAccessException | URISyntaxException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public IndividualListResponse findPredefinedIndividualsByClass(String serverUrl, String iriOfClass) {
		try {
			final String targetAddress = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + 
					PathConstants.SERVER_FIND_PREDEFINED_INDIVIDUALS_OF_CLASS;
			restTemplate = getRestTemplate();
			final HttpEntity<String> request = new HttpEntity<>(iriOfClass);
			final URI targetUri = new URI(targetAddress);
			final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(targetUri).queryParam
					(ParamConstants.PARAM_CLASSNAME, iriOfClass);
			System.out.println(("DEBUG: " + uriComponentsBuilder.build().encode().toUri()));
			final ResponseEntity<IndividualListResponse> response = restTemplate.exchange(uriComponentsBuilder.build().encode().toUri(), HttpMethod.GET, request,
					IndividualListResponse.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			}
			
			final IndividualListResponse individuals = response.getBody();
			return individuals;
		} catch (HttpClientErrorException | URISyntaxException ex) {
			ex.printStackTrace();
			return null;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public IndividualListResponse getSavedIndividuals(final String serverUrl, final OntologyClassResponse ontologyClassResponse,
			final UuidResponse uuidOfInformationPackage) {
		try {
			final String url = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.
					SERVER_FIND_INDIVIDUALS_OF_CLASS_AND_INFORMATION_PACKAGE;
			final UuidAndIriResponse uuidAndIriResponse = ResponseModelFactory.createUuidAndResponse(ontologyClassResponse.getIri(), 
					uuidOfInformationPackage.toString());
			
			restTemplate = getRestTemplate();
			final IndividualListResponse individualListResponse = restTemplate.postForObject(url, uuidAndIriResponse, IndividualListResponse.class);

			return individualListResponse;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}

	// DELETE_ME
	public OntologyClassResponse xxx(final String serverUrl, final String classname) {
		try { 
			final String targetAddress = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + 
					PathConstants.SERVER_GET_PROPERTIES_OF_CLASS;
			restTemplate = getRestTemplate();
			final HttpEntity<String> request = new HttpEntity<>(classname);
			final URI targetUri = new URI(targetAddress);
			final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(targetUri).queryParam
					(ParamConstants.PARAM_CLASSNAME, classname);
			System.out.println(("DEBUG: " + uriComponentsBuilder.build().encode().toUri()));
			final ResponseEntity<OntologyClassResponse> response = restTemplate.exchange(uriComponentsBuilder.build().encode().toUri(), HttpMethod.GET, request,
					OntologyClassResponse.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				System.out.println("***********************");
			}
			final OntologyClassResponse ontologyClass = response.getBody();
			return ontologyClass;
		} catch (HttpClientErrorException | URISyntaxException ex) {
			ex.printStackTrace();
			return null;
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public DigitalObjectListResponse getDigitalObjectsOfInformationPackage(final UuidResponse uuid) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void setClientModelFactory(final ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}

	@Override
	public InputStream downloadVirtualArchive(ArchiveResponse archiveResponse, String serverUrl) {
		try {
			final String uuid = archiveResponse.getUuid();
			String targetUrl = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_DOWNLOAD_VIRTUAL_ARCHIVE;
			targetUrl = targetUrl.replace("{uuid}", uuid);
			final RestTemplate restTemplate = getRestTemplate();
			restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
			final HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
		
		    final MultiValueMap<String, String> parameterMap = new LinkedMultiValueMap<>();
			parameterMap.add(ParamConstants.PARAM_UUID, uuid);
			final HttpEntity<String> entity = new HttpEntity<String>(headers);
		    
			final ResponseEntity<byte[]> response = restTemplate.exchange(targetUrl, HttpMethod.GET, entity, byte[].class);
		    if (response.getStatusCode() == HttpStatus.OK) {
		    	byte[] fileContent = response.getBody();
		    	return new ByteArrayInputStream(fileContent);
		    }
		    
		    return null;
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
    public RestTemplate getRestTemplate() {
        if(restTemplate==null) {
            restTemplate = createRestTemplate(); 
        }
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private RestTemplate createRestTemplate() {
        restTemplate = new RestTemplate();
        return restTemplate;
    }

	@Override
	public OntologyClassResponse getAllPremisClasses(final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.SERVER_READ_PREMIS;
			restTemplate = getRestTemplate();
			final OntologyClassResponse ontologyClass = restTemplate.getForObject(url, OntologyClassResponse.class);
			return ontologyClass;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}

	@Override
	public OntologyClassResponse getAllSkosClasses(final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_ONTOLOGY_INTERFACE + PathConstants.SERVER_READ_SKOS;
			restTemplate = getRestTemplate();
			final OntologyClassResponse ontologyClass = restTemplate.getForObject(url, OntologyClassResponse.class);
			return ontologyClass;
		} catch (ResourceAccessException exception) {
			final ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}

	@Override
	public void loadInformationPackage(final String uuid, final String serverUrl) {
		String targetUrl = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_LOAD_INFORMATION_PACKAGE;
		targetUrl = UrlUtils.replaceUrlParameters(targetUrl, "uuid", uuid);
		final RestTemplate restTemplate = getRestTemplate();
		final ResponseEntity<String> response = restTemplate.getForEntity(targetUrl, String.class);
		if (!response.getStatusCode().equals(HttpStatus.OK)) {
			
		}
	}
	
	@Override
	public InputStream downloadNotVirtualArchive(final ArchiveResponse archiveResponse, final String serverUrl) {
		try {
			final String uuid = archiveResponse.getUuid();
			String targetUrl = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_DOWNLOAD_NOT_VIRTUAL_ARCHIVE;
			targetUrl = UrlUtils.replaceUrlParameters(targetUrl, "uuid", uuid);
			final RestTemplate restTemplate = getRestTemplate();
			restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
			final HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
		
		    final MultiValueMap<String, String> parameterMap = new LinkedMultiValueMap<>();
			parameterMap.add(ParamConstants.PARAM_UUID, uuid);
			final HttpEntity<String> entity = new HttpEntity<String>(headers);
		    
			final ResponseEntity<byte[]> response = restTemplate.exchange(targetUrl, HttpMethod.GET, entity, byte[].class);
		    if (response.getStatusCode() == HttpStatus.OK) {
		    	byte[] fileContent = response.getBody();
		    	return new ByteArrayInputStream(fileContent);
		    }
		    
		    return null;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}

	@Override
	public ReferenceListResponse findReferencesOfInformationPackage(String serverUrl, String uuidOfInformationPackage) {
		try {
			String url = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + 
					PathConstants.SERVER_FIND_ALL_REFERENCES_OF_INFORMATION_PACKAGE;
			url = UrlUtils.replaceUrlParameters(url, "uuid", uuidOfInformationPackage);
			restTemplate = getRestTemplate();
			final ReferenceListResponse referenceListResponse = restTemplate.getForObject(url, ReferenceListResponse.class);
			return referenceListResponse;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public ArchiveListResponse getArchiveList(final String serverUrl) {
		try {
			final String url = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + PathConstants.SERVER_FIND_ALL_ARCHIVES;
			restTemplate = getRestTemplate();
			final ArchiveListResponse archiveListResponse = restTemplate.getForObject(url, ArchiveListResponse.class);
			return archiveListResponse;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void assignIndividualsToInformationPackage(final UuidListResponse uuidsResponse, final String serverUrl) {
		try {
			final HttpEntity<UuidListResponse> request = new HttpEntity<>(uuidsResponse);
			final String targetAddress = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE +  
				PathConstants.SERVER_ASSIGN_INDIVIDUALS_TO_INFORMATION_PACKAGE;
			restTemplate = getRestTemplate();
			restTemplate.exchange(targetAddress, HttpMethod.POST, request, String.class);
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void unassignIndividualsFromInformationPackage(final UuidListResponse uuidsResponse, final String serverUrl) {
		try {
			final HttpEntity<UuidListResponse> request = new HttpEntity<>(uuidsResponse);
			final String targetAddress = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE +  
				PathConstants.SERVER_UNASSIGN_INDIVIDUALS_FROM_INFORMATION_PACKAGE;
			restTemplate = getRestTemplate();
			restTemplate.exchange(targetAddress, HttpMethod.POST, request, String.class);
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void assignTaxonomyIndividuals(final IriListResponse irisOfTaxonomyIndividuals, final String serverUrl) {
		try {
			final HttpEntity<IriListResponse> request = new HttpEntity<>(irisOfTaxonomyIndividuals);
			final String targetAddress = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE +  
				PathConstants.SERVER_ASSIGN_TAXONOMY_INDIVIDUALS;
			restTemplate = getRestTemplate();
			restTemplate.exchange(targetAddress, HttpMethod.POST, request, String.class);
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}

	@Override
	public IriListResponse findAllAssignedTaxonomyIndividuals(final String serverUrl,
			final UuidResponse uuidOfInformationPackageResponse) {
		try {
			final String targetAddress = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE +  
				PathConstants.SERVER_FIND_ALL_ASSIGNED_TAXONOMY_INDIVIDUALS;
			restTemplate = getRestTemplate();
			final IriListResponse irisOfIndividuals = restTemplate.postForObject(targetAddress, uuidOfInformationPackageResponse, 
					IriListResponse.class);
			return irisOfIndividuals;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
			throw connectionException;
		}
	}
}
