package de.feu.kdmp4.packagingtoolkit.client.operations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class OperationsMain 
{
	/**
	 * Is used for deciding in the program if the communication via webservices is
	 * working or not. If set to true, the test data for the communication with
	 * the server are taken from static test lists. If set to false, the data 
	 * are accepted via web services. This constants
	 * and the respective get method are necessary for testing purposes.
	 */
	private static boolean mockServer;
	/**
	 * Is used for deciding in the program if the communication via webservices is
	 * working or not. If set to true, the test data for the communication with
	 * the mediator are taken from static test lists. If set to false, the data 
	 * are accepted via web services. This constants
	 * and the respective get method are necessary for testing purposes.
	 */
	private static boolean mockMediator;
	
	public static void main(String[] args) {
		SpringApplication.run(OperationsMain.class, args);
	}
	
	public static boolean isMockServer() {
		return mockServer;
	}

	public static boolean isMockMediator() {
		return mockMediator;
	}

	public static void setMockServer(boolean mockServer) {
		OperationsMain.mockServer = mockServer;
	}

	public static void setMockMediator(boolean mockMediator) {
		OperationsMain.mockMediator = mockMediator;
	}
}
