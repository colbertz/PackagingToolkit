package de.feu.kdmp4.packagingtoolkit.client.operations.classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.model.classes.ClientConfigurationDataImpl;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ClientConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.exceptions.ConfigurationException;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

public class ConfigurationOperationsPropertyImpl implements ClientConfigurationOperations {
	/**
	 * The name of the configuration file of the client. It lies in the root directory of the client application.
	 */
	private static final String CONFIG_FILE_PATH = "config.properties";
	/**
	 * The default name of the file with the logo.
	 */
	private static final String LOGO_DEFAULT_NAME = "logo.png";
	/**
	 * The name of the directory that contains the images for the client. 
	 */
	private static final String IMAGE_DIRECTORY = "images";
	/**
	 * The property that contains the time when the individuals have to be updated from the server. 
	 */
	private static final String PROPERTY_INDIVIDUALS_UPDATE_TIME = "individualsUpdateTimeInMilliseconds";
	/**
	 * The property in the configuration file that saves the url of the mediator.
	 */
	private static final String PROPERTY_MEDIATOR_URL = "mediatorUrl";
	/**
	 * The property in the configuration file that contains the name of the file with the logo. The
	 * file is saved in the images directory of the client.
	 */
	private static final String PROPERTY_LOGO_FILE = "logoFile";
	/**
	 * The property in the configuration file that saves the url of the server.
	 */
	private static final String PROPERTY_SERVER_URL = "serverUrl";
	/**
	 * The name of the directory where the client can save temporary data.
	 */
	private static final String TEMP_DIRECTORY_NAME = "temp";
	
	// ********** Public methods *******
	@Override
	public ClientConfigurationData getClientConfigurationData() {
		// Read the properties file.
		final Properties properties = new Properties();
		final File file = new File(CONFIG_FILE_PATH);

		try (InputStream inputStream = new FileInputStream(file)) {
			// Load the properties.
			properties.load(inputStream);
			// Get the properties and write them in an object.
			final String serverUrl = properties.getProperty(PROPERTY_SERVER_URL);
			final String mediatorUrl = properties.getProperty(PROPERTY_MEDIATOR_URL);
			return new ClientConfigurationDataImpl(serverUrl, mediatorUrl);
		} catch (FileNotFoundException ex) {
			try {
				// If the file was not found, create it and return an empty configuration. 
				file.createNewFile();
				return ClientModelFactory.getEmptyClientConfigurationData();
			} catch (IOException e) {
				ConfigurationException exception = ConfigurationException.
						generalIOException(e, CONFIG_FILE_PATH);
				throw exception;
			}
		} catch (IOException ioex) {
			ConfigurationException exception = ConfigurationException.
					generalIOException(ioex, CONFIG_FILE_PATH);
			throw exception;
		}
	}
	
	@Override
	public void saveLogo(final File temporaryLogoFile, final String configurationDirectory) {
		final File logoFile = getLogoPath(configurationDirectory);
		logoFile.delete();
		PackagingToolkitFileUtils.copyFile(temporaryLogoFile, logoFile);
		temporaryLogoFile.delete();
	}
	
	@Override
	public File getLogoPath(final String configurationDirectory) {
		// Read the properties file.
		final Properties properties = new Properties();
		final File file = new File(configurationDirectory, CONFIG_FILE_PATH);

		try (InputStream inputStream = new FileInputStream(file)) {
			// Load the properties.
			properties.load(inputStream);
			final String logoName = properties.getProperty(PROPERTY_LOGO_FILE);
			final String logoPath = IMAGE_DIRECTORY + File.separator + logoName;
			return new File(logoPath);
		} catch (FileNotFoundException ex) {
			try {
				// If the file was not found, create it and return an empty configuration. 
				file.createNewFile();
				final String logoPath = IMAGE_DIRECTORY + File.separator + LOGO_DEFAULT_NAME;
				return new File(logoPath);
			} catch (IOException e) {
				ConfigurationException exception = ConfigurationException.
						generalIOException(e, CONFIG_FILE_PATH);
				throw exception;
			}
		} catch (IOException ioex) {
			ConfigurationException exception = ConfigurationException.
					generalIOException(ioex, CONFIG_FILE_PATH);
			throw exception;
		}
	}
	
	@Override
	public long getIndividualsUpdateTimePath(final String configurationDirectory) {
		// Read the properties file.
		final Properties properties = new Properties();
		final File file = new File(configurationDirectory, CONFIG_FILE_PATH);

		try (InputStream inputStream = new FileInputStream(file)) {
			// Load the properties.
			properties.load(inputStream);
			final String updateTimeAsString = properties.getProperty(PROPERTY_INDIVIDUALS_UPDATE_TIME);
			final long updateTime = Long.parseLong(updateTimeAsString);
			return updateTime;
		} catch (FileNotFoundException ex) {
			try {
				// If the file was not found, create it and return an empty configuration. 
				file.createNewFile();
				return 0;
			} catch (IOException e) {
				ConfigurationException exception = ConfigurationException.
						generalIOException(e, CONFIG_FILE_PATH);
				throw exception;
			}
		} catch (IOException ioex) {
			ConfigurationException exception = ConfigurationException.
					generalIOException(ioex, CONFIG_FILE_PATH);
			throw exception;
		} catch (NumberFormatException exception) {
			return 0;
		}
	}
	
	@Override
	public File getTemporaryDirectoryOnClient() {
		return new File(TEMP_DIRECTORY_NAME);
	}
	
	@Override
	public void writeClientConfigurationData(final ClientConfigurationData 
			clientConfigurationData) throws IOException {
		final Properties properties = new Properties();
		properties.setProperty(PROPERTY_SERVER_URL, clientConfigurationData.getServerUrl());
		properties.setProperty(PROPERTY_MEDIATOR_URL, clientConfigurationData.getMediatorUrl());
		final File logoFile = getLogoPath(".");
		final String logoName = logoFile.getName();
		properties.setProperty(PROPERTY_LOGO_FILE, logoName);
		final File propertiesFile = new File(CONFIG_FILE_PATH);
		
		OutputStream outputStream = null;
		/* Repeat while the file was created. The loop is a workaround because
		 * of the checked exception while creating a file. 
		 */
		do {
			try {
				outputStream = new FileOutputStream(propertiesFile);
			} catch (FileNotFoundException e) {
				propertiesFile.createNewFile();
			}
		} while (outputStream == null);
		properties.store(outputStream, "");
	}
}
