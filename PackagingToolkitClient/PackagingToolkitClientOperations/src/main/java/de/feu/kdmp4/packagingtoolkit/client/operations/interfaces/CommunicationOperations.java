package de.feu.kdmp4.packagingtoolkit.client.operations.interfaces;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.Archive;
import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ArchiveList;

// DELETE_ME
public interface CommunicationOperations {
	//public InformationPackage sendFile(String url, File file);
	//public InformationPackage sendFile(String url, File file, String uuid);
	//XmlEntity sendRequest(String uri, String path, ParamList paramList);
	ArchiveList getArchiveList();
	void deleteArchive(Archive archive);
}
