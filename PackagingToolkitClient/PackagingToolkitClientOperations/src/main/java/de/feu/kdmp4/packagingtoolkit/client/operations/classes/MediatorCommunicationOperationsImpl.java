package de.feu.kdmp4.packagingtoolkit.client.operations.classes;

import java.io.File;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import de.feu.kdmp4.packagingtoolkit.client.exceptions.ConnectionException;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.MediatorCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorApiError;
import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementListResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringResponse;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

public class MediatorCommunicationOperationsImpl implements MediatorCommunicationOperations {
	private RestTemplate restTemplate;
	
	@Override
	public MetadataElementListResponse getMetadataFromMediator() {
		return new MetadataElementListResponse();
	}
	
	
	@Override
	public MediatorDataListResponse collectData(File file, String mediatorUrl) {
		try{
			final MultiValueMap<String, Object> parameterMap = createMultipartFileParam(file.getAbsolutePath());

			HttpHeaders fileHeader = new HttpHeaders();
			fileHeader.setContentType(MediaType.MULTIPART_FORM_DATA);
			
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameterMap, fileHeader);
			String url = mediatorUrl + PathConstants.MEDIATOR_CLIENT_INTERFACE + PathConstants.MEDIATOR_COLLECT_DATA_FROM_FILE;
			restTemplate = getRestTemplate();
			ResponseEntity<MediatorDataListResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, MediatorDataListResponse.class);
			 return response.getBody();
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createMediatorConnectionError(mediatorUrl);
			throw connectionException;
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			return null;
		} finally {
			// Delete the file from the temporary directory of the client.
			file.delete();
		}
	}
	
	@Override
	public void pingMediator(String mediatorUrl) {
		try {
			String url = mediatorUrl + PathConstants.MEDIATOR_CLIENT_INTERFACE + PathConstants.MEDIATOR_PING;
			restTemplate = getRestTemplate();
			String responseOfServer = restTemplate.getForObject(url, String.class);
			
			if (StringUtils.areStringsUnequal(responseOfServer, PackagingToolkitConstants.I_AM_HERE)) {
				ConnectionException connectionException = ConnectionException.createMediatorConnectionError(mediatorUrl);
				throw connectionException;
			}
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createMediatorConnectionError(mediatorUrl);
			throw connectionException;
		}
	}

	/**
	 * Creates an object for sending a file via http.
	 * @param filePath The path of the file we want to send on the local disk.
	 * @return Contains the information about the file for sending.
	 */
	private MultiValueMap<String, Object> createMultipartFileParam(String filePath) {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();   
        File file = new File(filePath);
        parts.add("file", new FileSystemResource(file));
        return parts;
	}
	
	@Override
	public DataSourceListResponse readDataSources(String mediatorUrl) {
		/*String url = mediatorUrl + PathConstants.MEDIATOR_CLIENT_INTERFACE + "/testException";
		restTemplate = getRestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_XML);
		try {
			StringResponse test = restTemplate.getForObject(url, StringResponse.class);
			if (test.getMediatorApiError() != null) {
				System.out.println(test.getMediatorApiError());
			} else {
				System.out.println(test.getValue());
			}
			
			//@SuppressWarnings("unchecked")
			//LinkedHashMap<String, String> m = (LinkedHashMap<String, String>)test;
			//System.out.println(m.get("status"));
		} catch (IllegalArgumentException e) {
			System.out.println("Exceptiuon");
		}
		
		
		return null;*/
		try {
			String url = mediatorUrl + PathConstants.MEDIATOR_CLIENT_INTERFACE + PathConstants.MEDIATOR_READ_DATASOURCES;
			restTemplate = getRestTemplate();
			DataSourceListResponse dataSources = restTemplate.getForObject(url, DataSourceListResponse.class);
			return dataSources;
		} catch (ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createMediatorConnectionError(mediatorUrl);
			throw connectionException;
		} 
	}
	
	@Override
	public void saveDataSourceConfiguration(String mediatorUrl, DataSourceListResponse dataSourceListResponse) {
		try { 
			String url = mediatorUrl + PathConstants.MEDIATOR_CLIENT_INTERFACE + PathConstants.MEDIATOR_CONFIGURE_DATA_SOURCES;
			restTemplate = getRestTemplate();
			HttpEntity<DataSourceListResponse> request = new HttpEntity<>(dataSourceListResponse);
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		} catch(ResourceAccessException exception) {
			ConnectionException connectionException = ConnectionException.createMediatorConnectionError(mediatorUrl);
			throw connectionException;
		}
	}
	
	@Override
	public void checkDataSourceInstallations(String mediatorUrl) {
		String url = mediatorUrl + PathConstants.MEDIATOR_CLIENT_INTERFACE + PathConstants.MEDIATOR_CHECK_INSTALLATION_DIRECTORIES;
		restTemplate = getRestTemplate();

		StringResponse stringResponse = restTemplate.getForObject(url, StringResponse.class);
		MediatorApiError mediatorApiError = stringResponse.getMediatorApiError(); 
		if (mediatorApiError != null) {
			String exceptionClass = mediatorApiError.getException();
			if (exceptionClass.equals(MediatorConfigurationException.class.getName())) {
				String message = mediatorApiError.getMessage();
				//String summary = mediatorApiError.getSummary();
				//String originalException = mediatorApiError.getInformationOfError().get("originalException");
				//String dataSourceName = mediatorApiError.getInformationOfError().get("dataSourceName");
				MediatorConfigurationException exception = MediatorConfigurationException.createDataSourceConfigurationException(message);
				throw exception;
			}
		}
	}
	
	/**
	 * Creates an object of RestTemplate. If there is already an object existing, there
	 * is no new object created.
	 * @return The object of RestTemplate.
	 */
    public RestTemplate getRestTemplate() {
    	if(restTemplate == null) {
             restTemplate = createRestTemplate(); 
         }
         return restTemplate;
     }

     public void setRestTemplate(RestTemplate restTemplate) {
         this.restTemplate = restTemplate;
     }

     private RestTemplate createRestTemplate() {
         restTemplate = new RestTemplate();
         return restTemplate;
     }
	
}
