package de.feu.kdmp4.packagingtoolkit.client.operations.classes;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import de.feu.kdmp4.packagingtoolkit.client.factories.ClientModelFactory;
import de.feu.kdmp4.packagingtoolkit.client.oais.model.interfaces.BasicInformationPackage;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ServerCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.client.utils.StringUtil;
import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.enums.OntologyDatatypeResponse;
import de.feu.kdmp4.packagingtoolkit.enums.PackageType;
import de.feu.kdmp4.packagingtoolkit.enums.SerializationFormat;
import de.feu.kdmp4.packagingtoolkit.enums.Sign;
import de.feu.kdmp4.packagingtoolkit.exceptions.ontology.OntologyReasonerException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DatatypePropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ObjectPropertyResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;
import de.feu.kdmp4.packagingtoolkit.utils.DateTimeUtils;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * Mocks the communication methods to the server. For testing the gui, 
 * the spring configuration has to be changed, that the test operation
 * classes are used. The methods of this class generate data of that kind
 * that the server will send. Therefore a testing is possible without any
 * communication between client and server.
 * @author Christopher Olbertz
 *
 */
public class MockedServerOperationsImpl implements ServerCommunicationOperations {
	// *********** Constants ***************
	private static final String CLASSNAME_BASE_ONTOLOGY_CLASS = "Information_Package";
	
	private static final String CLASSNAME_ACCESS_RIGHTS_INFORMATION = "Access_Rights_Information";
	private static final String CLASSNAME_CONTENT_INFORMATION = "Content_Information";
	private static final String CLASSNAME_CONTEXT_INFORMATION = "Context_Information";
	private static final String CLASSNAME_DATA_OBJECT = "Data_Object";
	private static final String CLASSNAME_DIGITAL_OBJECT = "Digital_Object";
	private static final String CLASSNAME_FIXITY_INFORMATION = "Fixity_Information";
	private static final String CLASSNAME_PHYSICAL_OBJECT = "Physical_Object";
	private static final String CLASSNAME_PRESERVATION_DESCRIPTION_INFORMATION = "Preservation_Description_Information";
	private static final String CLASSNAME_PROVENANCE_INFORMATION = "Provenance_Information";
	private static final String CLASSNAME_REFERENCE_INFORMATION = "Reference_Information";
	private static final String CLASSNAME_OTHER_REPRESENTATION_INFORMATION = "Other_Representation_Information";
	private static final String CLASSNAME_SEMANTIC_INFORMATION = "Semantic_Information";
	private static final String CLASSNAME_REPRESENTATION_INFORMATION = "Representation_Information";
	private static final String CLASSNAME_STRUCTURE_INFORMATION = "Structure_Information";
	private static final String CLASSNAME_DOMAIN_INFORMATION = "Domain_Information";
	private static final String CLASSNAME_RESEARCH_DOMAIN_INFORMATION = "Research_Domain_Information";
	
	private static final String DIGITAL_OBJECT_FILENAME = "filename";
	private static final int DIGITAL_OBJECTS_PER_SIU = 3;
	
	private static final int FIXITY_INDIVIDUALS_COUNT = 3;
	
	private static final String NAMESPACE_SEPARATOR = "#";
	
	private static final String PROPERTY_BOOLEAN = "booleanProperty";
	private static final String PROPERTY_BYTE = "byteProperty";
	private static final String PROPERTY_DATE = "dateProperty";
	private static final String PROPERTY_DATE_TIME = "dateTimeProperty";
	private static final String PROPERTY_DOUBLE = "doubleProperty";
	private static final String PROPERTY_DURATION = "durationProperty";
	private static final String PROPERTY_FLOAT = "floatProperty";
	private static final String PROPERTY_INTEGER = "integerProperty";
	private static final String PROPERTY_LONG = "longProperty";
	private static final String PROPERTY_SHORT = "shortProperty";
	private static final String PROPERTY_STRING = "stringProperty";
	private static final String PROPERTY_TIME = "timeProperty";
	private static final String PROPERY_POSITIVE_INT = "positiveInt";
	private static final String PROPERY_UNSIGNED_INT = "unsignedInt";
	private static final String PROPERY_NEGATIVE_INT = "negativeInt";
	/**
	 * The path to the property file with the configuration data of the mocked server.
	 */
	private static final String MOCKED_SERVER_CONFIGURATION_FILE_PATH = "src" + File.separator + "test"
			+ File.separator + "resources" + File.separator + "serverConfig.properties";
	/**
	 * The name of the directory that contains the file necessary for the configuration 
	 * of the mocked server. It is contained in the working directory.
	 */
	private static final String MOCKED_SERVER_CONFIGURATION_DIRECTORY = "config";
	/**
	 * The length of the testnames.
	 */
	private static final int NAME_LENGTH = 5;
	/**
	 * The number of the ontology classes contained in the views.
	 */
	private static final int ONTOLOGY_CLASS_COUNT = 5;
	/**
	 * A namespace for the test ontology classes
	 */
	private static final String ONTOLOGY_CLASSES_NAMESPACE = "http://kdmp4.eu.de/ontologies/KDMP4-OAIS-IP";
	
	private static final String SKOS_CLASS_CONCEPT_SCHEME = "ConceptScheme";
	private static final String SKOS_CLASS_CONCEPT = "Concept";
	private static final String SERVER_CONFIG_DIRECTORY = "config";
	/**
	 * The property that contains the value of the base ontology in the mocked
	 * server configuration file. 
	 */
	private static final String PROPERTY_BASE_ONTOLOGY = "baseOntology";
	/**
	 * The property that contains the value of the rule ontology in the mocked
	 * server configuration file. 
	 */
	private static final String PROPERTY_RULE_ONTOLOGY = "ruleOntology";
	/**
	 * The property that contains the value of the Premis ontology in the mocked
	 * server configuration file. 
	 */
	private static final String PROPERTY_PREMIS_ONTOLOGY = "premisOntology";
	/**
	 * The property that contains the value of the SKOS ontology in the mocked
	 * server configuration file. 
	 */
	private static final String PROPERTY_SKOS_ONTOLOGY = "skosOntology";
	/**
	 * The property that contains the value of the working directory in the mocked
	 * server configuration file. 
	 */
	private static final String PROPERTY_WORKING_DIRECTORY = "workingDirectory";
	/**
	 * The number of the views contained in the views.
	 */
	private static final int VIEWS_COUNT = 8;
	private static final int CONTENT_INFORMATION_INDIVIDUALS_COUNT = 3;	
	private static final int DATA_OBJECT_INDIVIDUALS_COUNT = 3;	
	
	private static final String NAMESPACE_PREMIS = "http://www.loc.gov/premis/rdf/v1/";
	private static final String NAMESPACE_SKOS = "http://www.w3.org/2004/02/skos/core";
	// *********** Attributes **************
	private ArchiveListResponse archiveList;
	private OntologyClassListResponse testOntologyClassList;
	private OntologyClassResponse testRootClass;
	private ViewListResponse viewList;
	private List<BasicInformationPackage> packagesInSession;
	private ClientModelFactory clientModelFactory;
	/**
	 * The taxonomies for testing purposes.
	 */
	private TaxonomyListResponse taxonomies;
	
	public MockedServerOperationsImpl() {
		archiveList = ResponseModelFactory.getArchiveListResponse();
		packagesInSession = new ArrayList<>();
		createTestOntology();		
	}
	
	@Override
	public ArchiveListResponse getArchiveList(String serverUrl) {
		if (archiveList.isEmpty()) {
			LocalDateTime now = DateTimeUtils.getNow();
			
			ArchiveResponse archive1 = ResponseModelFactory.getArchiveResponse(1, PackageType.SIP, 
					SerializationFormat.OAIORE, "Erstes Archiv", 
					new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357571"), now, now);
	
			ArchiveResponse archive2 = ResponseModelFactory.getArchiveResponse(2, PackageType.SIP, 
					SerializationFormat.OAIORE, "Zweites Archiv", 
					new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357572"), now, now);
			
			ArchiveResponse archive3 = ResponseModelFactory.getArchiveResponse(3, PackageType.SIP, 
					SerializationFormat.OAIORE, "Drittes Archiv", 
					new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357573"), now, now);
			
			ArchiveResponse archive4 = ResponseModelFactory.getArchiveResponse(4, PackageType.SIP, 
					SerializationFormat.OAIORE, "Viertes Archiv", 
					new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357574"), now, now);
			
			ArchiveResponse archive5 = ResponseModelFactory.getArchiveResponse(5, PackageType.SIP, 
					SerializationFormat.OAIORE, "Fuenftes Archiv", 
					new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357575"), now, now);
			
			ArchiveResponse archive6 = ResponseModelFactory.getArchiveResponse(6, PackageType.SIP, 
					SerializationFormat.OAIORE, "Sechstes Archiv", 
					new UuidResponse("9dfaf7bc-b9bd-4201-a8dc-d3b84c357576"), now, now);
	
			final DigitalObjectResponse digitalObjectResponse = createDigitalObjectForSIP();
			archive6.addDigitalObject(digitalObjectResponse);
			
			archiveList.addArchiveToList(archive1);
			archiveList.addArchiveToList(archive2);
			archiveList.addArchiveToList(archive3);
			archiveList.addArchiveToList(archive4);
			archiveList.addArchiveToList(archive5);
			archiveList.addArchiveToList(archive6);
			
			// A loop for setting the dates.
			//Calendar now = new GregorianCalendar();
			for (int i = 0; i < archiveList.getArchiveCount(); i++) {
				archiveList.getArchiveAt(i).setCreationDate(now);
				archiveList.getArchiveAt(i).setLastModificationDate(now);
			}
		}
		return archiveList;
	}
	
	/**
	 * Creates a list with one digital object for a SIP.
	 * @return A list that contains only one digital object.
	 */
	private DigitalObjectResponse createDigitalObjectForSIP() {
		final DigitalObjectResponse digitalObjectResponse = ResponseModelFactory.getDigitalObjectResponse("test1", new UuidResponse());
		digitalObjectResponse.addMetadata("AAAAA", "a value");
		digitalObjectResponse.addMetadata("BBBBB", "b value");
		digitalObjectResponse.addMetadata("CCCCC", "c value");
		digitalObjectResponse.addMetadata("DDDDD", "d value");
		digitalObjectResponse.addMetadata("EEEEE", "e value");
		
		return digitalObjectResponse;
	}
	
	/*@Override
	public InformationPackage sendFile(String url, File file) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InformationPackage sendFile(String url, File file, String uuid) {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public void writeServerConfiguration(ServerConfigurationDataResponse configurationData, String serverUrl) {
		final Properties properties = new Properties();
		properties.setProperty(PROPERTY_BASE_ONTOLOGY, configurationData.getConfiguredBaseOntologyPath());
		properties.setProperty(PROPERTY_RULE_ONTOLOGY, configurationData.getConfiguredRuleOntologyPath());
		properties.setProperty(PROPERTY_WORKING_DIRECTORY, configurationData.getWorkingDirectoryPath()); 	
		final File propertiesFile = new File(MOCKED_SERVER_CONFIGURATION_FILE_PATH);
		
		OutputStream outputStream = null;
		
		try {
			outputStream = new FileOutputStream(propertiesFile);
			properties.store(outputStream, "");
		} catch (IOException e) {
			
		}
	}
	
	@Override
	public OntologyClassResponse getHierarchy(final String serverUrl) {
		return testRootClass;
	}
	
	@Override
	public OntologyClassResponse getHierarchy(String serverUrl, StringListResponse classnames) {
		OntologyClassResponse ontologyClassResponse = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_BASE_ONTOLOGY_CLASS);
		
			for (int i = 0; i < testOntologyClassList.getOntologyClassCount(); i++) {
				OntologyClassResponse ontologyClass = testOntologyClassList.getOntologyClassAt(i);
				
				for (int j = 0; j < classnames.getStringCount(); j++) {
					String aClassname = classnames.getStringAtIndex(j);
					String ontologyClassFullName = ontologyClass.getNamespace() + PackagingToolkitConstants.NAMESPACE_SEPARATOR  + ontologyClass.getLocalName();
					if (ontologyClassFullName.contains(aClassname)) {
						if (!ontologyClassFullName.equals(CLASSNAME_BASE_ONTOLOGY_CLASS)) {	
							ontologyClassResponse.addSubclass(ontologyClass);
						}
					}
				}
			}
		
		return ontologyClassResponse;
	}
	
	@Override
	public OntologyClassResponse getPropertiesOfClass(String serverUrl, String classname) {
		DatatypePropertyListResponse propertyList = ResponseModelFactory.getOntologyPropertyListResponse();
		ObjectPropertyListResponse objectProperties = ResponseModelFactory.getObjectPropertyListResponse();
		
		int posSharp = classname.indexOf(NAMESPACE_SEPARATOR);
		OntologyClassResponse ontologyClass = null;
		
		if (posSharp >= 0) {
			String namespace = classname.substring(0, posSharp);
			String localName = classname.substring(posSharp + 1);
			ontologyClass = ResponseModelFactory.getOntologyClassResponse(namespace, localName);
		} else {
			ontologyClass = ResponseModelFactory.getOntologyClassResponse(StringUtil.EMPTY_STRING, classname);
		}

		if (classname.contains(CLASSNAME_PRESERVATION_DESCRIPTION_INFORMATION)) {
			DatatypePropertyResponse ontologyUnsignedIntegerProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyUnsignedIntegerProperty.setUnsigned(true);
			ontologyUnsignedIntegerProperty.setInclusiveZero(true);
			ontologyUnsignedIntegerProperty.setValue("44");
			ontologyUnsignedIntegerProperty.setPropertyName(PROPERY_UNSIGNED_INT);
			ontologyUnsignedIntegerProperty.setDatatype(OntologyDatatypeResponse.INTEGER);
			
			DatatypePropertyResponse ontologyNegativeIntegerProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyNegativeIntegerProperty.setUnsigned(false);
			ontologyNegativeIntegerProperty.setInclusiveZero(true);
			ontologyNegativeIntegerProperty.setSign(Sign.MINUS_SIGN);
			ontologyNegativeIntegerProperty.setValue("-44");
			ontologyNegativeIntegerProperty.setPropertyName(PROPERY_NEGATIVE_INT);
			ontologyNegativeIntegerProperty.setDatatype(OntologyDatatypeResponse.INTEGER);
			
			DatatypePropertyResponse ontologyPositiveIntegerProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyPositiveIntegerProperty.setUnsigned(true);
			ontologyPositiveIntegerProperty.setInclusiveZero(false);
			ontologyPositiveIntegerProperty.setSign(Sign.PLUS_SIGN);
			ontologyPositiveIntegerProperty.setPropertyName(PROPERY_POSITIVE_INT);
			ontologyPositiveIntegerProperty.setDatatype(OntologyDatatypeResponse.INTEGER);
			
			propertyList.addDatatypePropertyToList(ontologyUnsignedIntegerProperty);
			propertyList.addDatatypePropertyToList(ontologyNegativeIntegerProperty);
			propertyList.addDatatypePropertyToList(ontologyPositiveIntegerProperty);
		} else if (classname.contains(CLASSNAME_DIGITAL_OBJECT)) {
			DatatypePropertyResponse ontologyBooleanProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyBooleanProperty.setPropertyName(PROPERTY_BOOLEAN);
			ontologyBooleanProperty.setDatatype(OntologyDatatypeResponse.BOOLEAN);
			ontologyBooleanProperty.setValue("true");
			
			DatatypePropertyResponse ontologyByteProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyByteProperty.setPropertyName(PROPERTY_BYTE);
			ontologyByteProperty.setDatatype(OntologyDatatypeResponse.BYTE);
			
			DatatypePropertyResponse ontologyDateProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyDateProperty.setPropertyName(PROPERTY_DATE);
			ontologyDateProperty.setDatatype(OntologyDatatypeResponse.DATE);
			ontologyDateProperty.setValue("17405");
			
			DatatypePropertyResponse ontologyDateTimeProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyDateTimeProperty.setPropertyName(PROPERTY_DATE_TIME);
			ontologyDateTimeProperty.setDatatype(OntologyDatatypeResponse.DATETIME);
			ontologyDateTimeProperty.setValue("1441324800");
			
			DatatypePropertyResponse ontologyDurationProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyDurationProperty.setPropertyName(PROPERTY_DURATION);
			ontologyDurationProperty.setDatatype(OntologyDatatypeResponse.DURATION);
			ontologyDurationProperty.setValue("P5Y2M10D");
			
			propertyList.addDatatypePropertyToList(ontologyDurationProperty);
			propertyList.addDatatypePropertyToList(ontologyDateTimeProperty);
			propertyList.addDatatypePropertyToList(ontologyDateProperty);
			propertyList.addDatatypePropertyToList(ontologyByteProperty);
			propertyList.addDatatypePropertyToList(ontologyBooleanProperty);
		} else if (classname.contains(CLASSNAME_FIXITY_INFORMATION)) {
			DatatypePropertyResponse ontologyDoubleProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyDoubleProperty.setPropertyName(PROPERTY_DOUBLE);
			ontologyDoubleProperty.setDatatype(OntologyDatatypeResponse.DOUBLE);
			ontologyDoubleProperty.setValue("0.123456789");
			
			DatatypePropertyResponse ontologyFloatProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyFloatProperty.setPropertyName(PROPERTY_FLOAT);
			ontologyFloatProperty.setDatatype(OntologyDatatypeResponse.FLOAT);
			
			DatatypePropertyResponse ontologyIntegerProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyIntegerProperty.setPropertyName(PROPERTY_INTEGER);
			ontologyIntegerProperty.setDatatype(OntologyDatatypeResponse.INTEGER);
			ontologyIntegerProperty.setSign(Sign.PLUS_SIGN);
			
			DatatypePropertyResponse ontologyLongProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyLongProperty.setPropertyName(PROPERTY_LONG);
			ontologyLongProperty.setDatatype(OntologyDatatypeResponse.LONG);
			
			DatatypePropertyResponse ontologyShortProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyShortProperty.setPropertyName(PROPERTY_SHORT);
			ontologyShortProperty.setDatatype(OntologyDatatypeResponse.SHORT);
			
			propertyList.addDatatypePropertyToList(ontologyShortProperty);
			propertyList.addDatatypePropertyToList(ontologyLongProperty);
			propertyList.addDatatypePropertyToList(ontologyIntegerProperty);
			propertyList.addDatatypePropertyToList(ontologyFloatProperty);
			propertyList.addDatatypePropertyToList(ontologyDoubleProperty);
		} else if (classname.contains(CLASSNAME_CONTENT_INFORMATION)) {
			DatatypePropertyResponse ontologyStringProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyStringProperty.setPropertyName(PROPERTY_STRING);
			ontologyStringProperty.setDatatype(OntologyDatatypeResponse.STRING);
			
			DatatypePropertyResponse ontologyTimeProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyTimeProperty.setPropertyName(PROPERTY_TIME);
			ontologyTimeProperty.setDatatype(OntologyDatatypeResponse.TIME);
			ontologyTimeProperty.setValue("09:00:00");
			propertyList.addDatatypePropertyToList(ontologyTimeProperty);
			propertyList.addDatatypePropertyToList(ontologyStringProperty);
			
			ObjectPropertyResponse objectProperty = ResponseModelFactory.getObjectPropertyResponse();
			objectProperty.setLabel(CLASSNAME_DATA_OBJECT);
			objectProperty.setPropertyName(CLASSNAME_DATA_OBJECT);
			String dataObjectClassname = ONTOLOGY_CLASSES_NAMESPACE + NAMESPACE_SEPARATOR + CLASSNAME_DATA_OBJECT;
			objectProperty.setRange(dataObjectClassname);
			objectProperties.addObjectPropertyToList(objectProperty);
		} else if (classname.contains(CLASSNAME_DATA_OBJECT)) {
			DatatypePropertyResponse ontologyDoubleProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyDoubleProperty.setPropertyName(PROPERTY_DOUBLE);
			ontologyDoubleProperty.setDatatype(OntologyDatatypeResponse.DOUBLE);
			
			DatatypePropertyResponse ontologyBooleanProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyBooleanProperty.setPropertyName(PROPERTY_BOOLEAN);
			ontologyBooleanProperty.setDatatype(OntologyDatatypeResponse.BOOLEAN);
			ontologyBooleanProperty.setValue(PackagingToolkitConstants.TRUE);
			
			DatatypePropertyResponse ontologyStringProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyStringProperty.setPropertyName(PROPERTY_STRING);
			ontologyStringProperty.setDatatype(OntologyDatatypeResponse.STRING);
			ontologyStringProperty.setValue("Hello World");
			
			DatatypePropertyResponse ontologyIntegerProperty = ResponseModelFactory.getOntologyPropertyResponse();
			ontologyIntegerProperty.setPropertyName(PROPERTY_INTEGER);
			ontologyIntegerProperty.setDatatype(OntologyDatatypeResponse.INTEGER);
			ontologyIntegerProperty.setValue("55");
			
			propertyList.addDatatypePropertyToList(ontologyBooleanProperty);
			propertyList.addDatatypePropertyToList(ontologyDoubleProperty);
			propertyList.addDatatypePropertyToList(ontologyStringProperty);
			propertyList.addDatatypePropertyToList(ontologyIntegerProperty);
			
			/*ObjectPropertyResponse objectProperty = ResponseModelFactory.getObjectPropertyResponse();
			objectProperty.setLabel(CLASSNAME_DATA_OBJECT);
			objectProperty.setPropertyName(CLASSNAME_DATA_OBJECT);
			String dataObjectClassname = ONTOLOGY_CLASSES_NAMESPACE + NAMESPACE_SEPARATOR + CLASSNAME_DATA_OBJECT;
			objectProperty.setRange(dataObjectClassname);
			objectProperties.addObjectPropertyToList(objectProperty);*/
		}
		
		ontologyClass.setProperties(propertyList);
		ontologyClass.setObjectProperties(objectProperties);
		
		return ontologyClass;
	}

	@Override
	public ServerConfigurationDataResponse readServerConfiguration(String serverUrl) {
		// Read the properties file.
		final Properties properties = new Properties();
		final File file = new File(MOCKED_SERVER_CONFIGURATION_FILE_PATH);
		System.out.println("DEBUG: " + file.getAbsolutePath());
		try (InputStream inputStream = new FileInputStream(file)) {
			// Load the properties.
			properties.load(inputStream);
			// Get the properties and write them in an object.
			final String workingDirectory = properties.getProperty(PROPERTY_WORKING_DIRECTORY);
			final String baseOntology = properties.getProperty(PROPERTY_BASE_ONTOLOGY);
			final String ruleOntology = properties.getProperty(PROPERTY_RULE_ONTOLOGY);
			
			return new ServerConfigurationDataResponse(baseOntology, ruleOntology, workingDirectory,
					CLASSNAME_BASE_ONTOLOGY_CLASS);
		} catch (FileNotFoundException ex) {
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	public IndividualListResponse getSavedIndividuals(final String serverUrl, final OntologyClassResponse ontologyClassResponse, 
			final UuidResponse uuidOfInformationPackage) {
		IndividualListResponse individualListResponse = ResponseModelFactory.getIndividualListResponse();
		String iriOfClass = ontologyClassResponse.toString();
		
		if (iriOfClass.contains(CLASSNAME_DATA_OBJECT) && uuidOfInformationPackage.toString().equals("9dfaf7bc-b9bd-4201-a8dc-d3b84c357575")) {
			for (int i = 0; i < DATA_OBJECT_INDIVIDUALS_COUNT; i++) {
				IndividualResponse individual = ResponseModelFactory.getIndividualResponse();
				Uuid uuid = new Uuid();
				individual.setUuid(ResponseModelFactory.getUuidResponse(uuid));
				individual.setClassIri(iriOfClass);
				DatatypePropertyResponse datatypeProperty1 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty1.setDatatype(OntologyDatatypeResponse.DOUBLE);
				datatypeProperty1.setLabel(PROPERTY_DOUBLE);
				datatypeProperty1.setValue("1.1111" + i);
				datatypeProperty1.setPropertyName("http://kdmp4.feu.de");
				
				DatatypePropertyResponse datatypeProperty2 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty2.setDatatype(OntologyDatatypeResponse.BOOLEAN);
				datatypeProperty2.setLabel(PROPERTY_BOOLEAN);
				datatypeProperty2.setPropertyName("http://kdmp4.feu.de");
				
				if (i % 2 == 0) {
					datatypeProperty2.setValue("true");
				} else {
					datatypeProperty2.setValue("false");
				}
				
				DatatypePropertyResponse datatypeProperty3 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty3.setDatatype(OntologyDatatypeResponse.STRING);
				datatypeProperty3.setLabel(PROPERTY_STRING);
				datatypeProperty3.setValue("string " + i  + " in information package " + uuidOfInformationPackage);
				datatypeProperty3.setPropertyName("http://kdmp4.feu.de");
				
				DatatypePropertyResponse datatypeProperty4 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty4.setDatatype(OntologyDatatypeResponse.INTEGER);
				datatypeProperty4.setLabel(PROPERTY_INTEGER);
				String value = String.valueOf(i + 100);
				datatypeProperty4.setValue(value);
				datatypeProperty4.setPropertyName("http://kdmp4.feu.de");
				
				individual.addOntologyProperty(datatypeProperty1);
				individual.addOntologyProperty(datatypeProperty2);
				individual.addOntologyProperty(datatypeProperty3);
				individual.addOntologyProperty(datatypeProperty4);
				
				individualListResponse.addIndividualToList(individual);
			}
		} else if (iriOfClass.contains(CLASSNAME_DATA_OBJECT)) {
			for (int i = 0; i < DATA_OBJECT_INDIVIDUALS_COUNT; i++) {
				IndividualResponse individual = ResponseModelFactory.getIndividualResponse();
				Uuid uuid = new Uuid();
				individual.setUuid(ResponseModelFactory.getUuidResponse(uuid));
				individual.setClassIri(iriOfClass);
				DatatypePropertyResponse datatypeProperty1 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty1.setDatatype(OntologyDatatypeResponse.DOUBLE);
				datatypeProperty1.setLabel(PROPERTY_DOUBLE);
				datatypeProperty1.setValue("2.22222" + i);
				datatypeProperty1.setPropertyName("http://kdmp4.feu.de");
				
				DatatypePropertyResponse datatypeProperty2 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty2.setDatatype(OntologyDatatypeResponse.BOOLEAN);
				datatypeProperty2.setLabel(PROPERTY_BOOLEAN);
				datatypeProperty2.setPropertyName("http://kdmp4.feu.de");
				
				if (i % 2 == 0) {
					datatypeProperty2.setValue("true");
				} else {
					datatypeProperty2.setValue("false");
				}
				
				DatatypePropertyResponse datatypeProperty3 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty3.setDatatype(OntologyDatatypeResponse.STRING);
				datatypeProperty3.setLabel(PROPERTY_STRING);
				datatypeProperty3.setValue("string " + i  + " in information package " + uuidOfInformationPackage);
				datatypeProperty3.setPropertyName("http://kdmp4.feu.de");
				
				DatatypePropertyResponse datatypeProperty4 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty4.setDatatype(OntologyDatatypeResponse.INTEGER);
				datatypeProperty4.setLabel(PROPERTY_INTEGER);
				String value = String.valueOf(i + 100);
				datatypeProperty4.setValue(value);
				datatypeProperty4.setPropertyName("http://kdmp4.feu.de");
				
				individual.addOntologyProperty(datatypeProperty1);
				individual.addOntologyProperty(datatypeProperty2);
				individual.addOntologyProperty(datatypeProperty3);
				individual.addOntologyProperty(datatypeProperty4);
				
				individualListResponse.addIndividualToList(individual);
			}
		}else if (iriOfClass.contains(CLASSNAME_CONTENT_INFORMATION)) {
			for (int i = 0; i < CONTENT_INFORMATION_INDIVIDUALS_COUNT; i++) {
				IndividualResponse individual = ResponseModelFactory.getIndividualResponse();
				Uuid uuid = new Uuid();
				individual.setUuid(ResponseModelFactory.getUuidResponse(uuid));
				individual.setClassIri(iriOfClass);
				DatatypePropertyResponse datatypeProperty1 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty1.setDatatype(OntologyDatatypeResponse.STRING);
				datatypeProperty1.setLabel(PROPERTY_STRING);
				datatypeProperty1.setValue("String" + i);
				datatypeProperty1.setPropertyName("http://kdmp4.feu.de");
				
				DatatypePropertyResponse datatypeProperty2 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty2.setDatatype(OntologyDatatypeResponse.TIME);
				datatypeProperty2.setLabel(PROPERTY_TIME);
				datatypeProperty2.setPropertyName("http://kdmp4.feu.de");
				datatypeProperty2.setValue("1" + i + ":1" + i  + ":1" + i);
				
				individual.addOntologyProperty(datatypeProperty1);
				individual.addOntologyProperty(datatypeProperty2);
				
				individualListResponse.addIndividualToList(individual);
			}
		}
		
		return individualListResponse;
	}
	
	@Override 
	public void saveNewView(String serverUrl, ViewResponse viewResponse) {
		viewList.addViewToList(viewResponse);
	}
	
	@Override
	public File downloadBaseOntology(String serverUrl, File downloadDirectory) {
		ServerConfigurationDataResponse serverConfiguration = readServerConfiguration(serverUrl);
		String workingDirectoryPath = serverConfiguration.getWorkingDirectoryPath();
		File configurationDirectory = new File(workingDirectoryPath + File.separator + 
				SERVER_CONFIG_DIRECTORY);
		File baseOntologyOnServer = new File(configurationDirectory, serverConfiguration.getConfiguredBaseOntologyPath());
		File baseOntologyOnClient = new File(downloadDirectory, baseOntologyOnServer.getName());
		PackagingToolkitFileUtils.copyFile(baseOntologyOnServer, baseOntologyOnClient);
		return baseOntologyOnClient;
	}
	
	@Override
	public void configureRuleOntology(File ontologyFile, String serverUrl) {
		ServerConfigurationDataResponse configurationData = readServerConfiguration(serverUrl);
		String workingDirectory = configurationData.getWorkingDirectoryPath();
		File configDirectory = new File(workingDirectory + File.separator + 
				MOCKED_SERVER_CONFIGURATION_DIRECTORY);
		File destFile = new File(configDirectory, ontologyFile.getName());
		PackagingToolkitFileUtils.copyFile(ontologyFile, destFile);
		final Properties properties = new Properties();
		properties.setProperty(PROPERTY_BASE_ONTOLOGY, configurationData.getConfiguredBaseOntologyPath());
		properties.setProperty(PROPERTY_RULE_ONTOLOGY, ontologyFile.getName());
		properties.setProperty(PROPERTY_WORKING_DIRECTORY, configurationData.getWorkingDirectoryPath()); 	
		final File propertiesFile = new File(MOCKED_SERVER_CONFIGURATION_FILE_PATH);
		
		OutputStream outputStream = null;
		
		try {
			outputStream = new FileOutputStream(propertiesFile);
			properties.store(outputStream, "");
		} catch (IOException e) {
			
		}
	}

	@Override
	public void configureBaseOntology(File ontologyFile, String serverUrl) {
		ServerConfigurationDataResponse configurationData = readServerConfiguration(serverUrl);
		String workingDirectory = configurationData.getWorkingDirectoryPath();
		File configDirectory = new File(workingDirectory + File.separator + 
				MOCKED_SERVER_CONFIGURATION_DIRECTORY);
		File destFile = new File(configDirectory, ontologyFile.getName());
		PackagingToolkitFileUtils.copyFile(ontologyFile, destFile);
		final Properties properties = new Properties();
		properties.setProperty(PROPERTY_BASE_ONTOLOGY, ontologyFile.getName());
		properties.setProperty(PROPERTY_RULE_ONTOLOGY, configurationData.getConfiguredRuleOntologyPath());
		properties.setProperty(PROPERTY_WORKING_DIRECTORY, configurationData.getWorkingDirectoryPath()); 	
		final File propertiesFile = new File(MOCKED_SERVER_CONFIGURATION_FILE_PATH);
		
		OutputStream outputStream = null;
		
		try {
			outputStream = new FileOutputStream(propertiesFile);
			properties.store(outputStream, "");
		} catch (IOException e) {
			
		}
	}

	@Override
	public void deleteArchive(Uuid uuid, String serverUrl) {
		ArchiveListResponse newArchiveList = ResponseModelFactory.getArchiveListResponse();
		
		for (int i = 0; i < archiveList.getArchiveCount(); i++) {
			ArchiveResponse archive = archiveList.getArchiveAt(i);
			String theOtherUuid = archive.getUuid();
			
			// This is not our archive and so it is copied to the new list.
			if (!uuid.toString().equals(theOtherUuid)) {
				newArchiveList.addArchiveToList(archive);
			}
		}
		
		archiveList = ResponseModelFactory.getArchiveListResponse();
		
		for (int i = 0; i < newArchiveList.getArchiveCount(); i++) {
			ArchiveResponse archive = newArchiveList.getArchiveAt(i);
			archiveList.addArchiveToList(archive);
		}
	}

	@Override
	public ViewListResponse getAllViews(final String serverUrl) {
		return getKdmp4TestViewList();
	}
	
	/**
	 * Creates some test views for the ontology KDMP4.owl.
	 * <br />
	 * First view:
	 * 	<ul>
	 * 		<li>Digital_Object</li>
	 * 		<li>Physical_Object</li>
	 * 	</ul>
	 * Second view:
	 * 	<ul>
	 * 		<li>Other_Representation_Information</li>
	 * 		<li>Semantic_Information</li>
	 * 	</ul>
	 * Third view:
	 * 	<ul>
	 * 		<li>Representation_Information</li>
	 * 		<li>Data_Object</li>
	 * 	</ul>
	 * Fourth view:
	 * 	<ul>
	 * 		<li>Digital_Object</li>
	 * 		<li>Representation_Information</li>
	 * 		<li>Structure_Information</li>
	 * 	</ul>
	 * @return
	 */
	private ViewListResponse getKdmp4TestViewList() {
		viewList = ResponseModelFactory.getViewListResponse();
		
		OntologyClassResponse digitalObject = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_DIGITAL_OBJECT);
		OntologyClassResponse physicalObject = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_PHYSICAL_OBJECT);
		OntologyClassResponse otherRepesentationInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_OTHER_REPRESENTATION_INFORMATION);
		OntologyClassResponse semanticInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_SEMANTIC_INFORMATION);
		OntologyClassResponse representationInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_REPRESENTATION_INFORMATION);
		OntologyClassResponse structureInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_STRUCTURE_INFORMATION);
		OntologyClassResponse dataObject = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_DATA_OBJECT);
		
		ViewResponse firstView = ResponseModelFactory.getViewResponse("First View", 1);
		ViewResponse secondView = ResponseModelFactory.getViewResponse("Second View", 2);
		ViewResponse thirdView = ResponseModelFactory.getViewResponse("Third View", 3);
		ViewResponse fourthView = ResponseModelFactory.getViewResponse("Fourth View", 4);
		
		// Create the first view.
		OntologyClassListResponse ontologyClassForFirstView = ResponseModelFactory.getOntologyClassListResponse();
		ontologyClassForFirstView.addOntologyClassToList(digitalObject);
		ontologyClassForFirstView.addOntologyClassToList(physicalObject);
		firstView.setOntologyClassList(ontologyClassForFirstView);
		
		// Create the second view.
		OntologyClassListResponse ontologyClassForSecondView = ResponseModelFactory.getOntologyClassListResponse();
		ontologyClassForSecondView.addOntologyClassToList(otherRepesentationInformation);
		ontologyClassForSecondView.addOntologyClassToList(semanticInformation);
		secondView.setOntologyClassList(ontologyClassForSecondView);
		
		// Create the third view.
		OntologyClassListResponse ontologyClassForThirdView = ResponseModelFactory.getOntologyClassListResponse();
		ontologyClassForThirdView.addOntologyClassToList(representationInformation);
		ontologyClassForThirdView.addOntologyClassToList(dataObject);
		thirdView.setOntologyClassList(ontologyClassForThirdView);
		
		// Create the third view.
		OntologyClassListResponse ontologyClassForFourthView = ResponseModelFactory.getOntologyClassListResponse();
		ontologyClassForFourthView.addOntologyClassToList(digitalObject);
		ontologyClassForFourthView.addOntologyClassToList(representationInformation);
		ontologyClassForFourthView.addOntologyClassToList(structureInformation);
		fourthView.setOntologyClassList(ontologyClassForFourthView);
		
		viewList.addViewToList(firstView);
		viewList.addViewToList(secondView);
		viewList.addViewToList(thirdView);
		viewList.addViewToList(fourthView);
		return viewList;
	}
	
	private ViewListResponse getTestViewList() {
		ViewListResponse viewList = ResponseModelFactory.getViewListResponse();
		int viewId = 0;
		
		for (char i = 'A'; i < 'A' + VIEWS_COUNT; i++) {
			viewId++;
			String viewName = "";
			OntologyClassListResponse ontologyClassList = ResponseModelFactory.
					getOntologyClassListResponse();
			
			// Create a name with more than one letter.
			
			for (int j = 0; j < NAME_LENGTH; j++) {
				viewName = viewName + i;
			}
			
			ViewResponse view = ResponseModelFactory.getViewResponse(viewName, viewId);
			
			// Now we create some ontology classes for the view.
			for (char j = 'Z'; j > 'Z' - ONTOLOGY_CLASS_COUNT; j--) {
				String className = "";
				
				// Create a name with more than one letter.
				for (int k = 0; k < NAME_LENGTH; k++) {
					className = className + j;
				}
				
				// Add the ontology classes to the view.
				ontologyClassList.addOntologyClassToList(ResponseModelFactory.
						getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, className));
				
			}
			view.setOntologyClassList(ontologyClassList);
			viewList.addViewToList(view);
		}
		
		return viewList;
	}

	@Override
	public PackageResponse createSubmissionInformationUnit(String serverUrl, String title) {
		BasicInformationPackage informationPackage = ClientModelFactory.
				createInformationPackage(title, PackageType.SIP);
		packagesInSession.add(informationPackage);
		return informationPackage.toPackageResponse();
	}

	@Override
	public PackageResponse createSubmissionInformationPackage(String serverUrl, String title) {
		BasicInformationPackage informationPackage = ClientModelFactory.
				createInformationPackage(title, PackageType.SIU);
		packagesInSession.add(informationPackage);
		return informationPackage.toPackageResponse();
	}
	
	/*@Override
	public PackageResponse createVirtualSIU(String serverUrl, String title) {
		BasicInformationPackage informationPackage = ClientModelFactory.
				createInformationPackage(title, PackageType.SIU, true);
		packagesInSession.add(informationPackage);
		//Uuid uuid = informationPackage.getUuid();
		//UuidResponse uuidResponse = ResponseModelFactory.getUuidResponse(uuid);
		return informationPackage.toPackageResponse();
		//return ResponseModelFactory.getPackageResponse(title, PackageType.SIU, 
			//	uuidResponse, true);
	}

	@Override
	public PackageResponse createVirtualSIP(String serverUrl, String title) {
		BasicInformationPackage informationPackage = ClientModelFactory.
				createInformationPackage(title, PackageType.SIP, true);
		packagesInSession.add(informationPackage);
		return informationPackage.toPackageResponse();
	}

	@Override
	public PackageResponse createNotVirtualSIU(String serverUrl, String title) {
		BasicInformationPackage informationPackage = ClientModelFactory.
				createInformationPackage(title, PackageType.SIU, false);
		packagesInSession.add(informationPackage);
		return informationPackage.toPackageResponse();
	}*/
	
	private ArchiveResponse getArchiveResponseByUuid(UuidResponse uuid) {
		for (int i = 0; i < archiveList.getArchiveCount(); i++) {
			ArchiveResponse archiveResponse = archiveList.getArchiveAt(i);
			boolean uuidsEqual = archiveResponse.areUuidsEqual(uuid);
			if (uuidsEqual) {
				return archiveResponse;
			}
		}
		
		// Null only for the compiler. Should not be reached.
		return null;
	}

	@Override
	public DigitalObjectListResponse getDigitalObjectsOfInformationPackage(UuidResponse uuid) {
		DigitalObjectListResponse digitalObjectList = new DigitalObjectListResponse();
		ArchiveResponse archiveResponse = getArchiveResponseByUuid(uuid);
			
			//if (archiveResponse.isNotVirtual()) {
				DigitalObjectResponse digitalObjectResponse = new DigitalObjectResponse();
				digitalObjectResponse.setFilename(DIGITAL_OBJECT_FILENAME + "_" + 1);
				digitalObjectList.addDigitalObjectToList(digitalObjectResponse);
				
				if (archiveResponse.isSIU()) {
					for (int i = 1; i < DIGITAL_OBJECTS_PER_SIU; i++) {
						digitalObjectResponse = new DigitalObjectResponse();
						digitalObjectResponse.setFilename(DIGITAL_OBJECT_FILENAME + "_" + 1);
						digitalObjectList.addDigitalObjectToList(digitalObjectResponse);
				}
			//}
		}
		return digitalObjectList;
	}

	/*@Override
	public PackageResponse createNotVirtualSIP(String serverUrl, String title) {
		BasicInformationPackage informationPackage = ClientModelFactory.
				createInformationPackage(title, PackageType.SIP, false);
		packagesInSession.add(informationPackage);
		return informationPackage.toPackageResponse();
	}*/
	
	// ********* Private methods **********
	/**
	 * Creates a class hierarchy for testing purposes.
	 */
	public void createTestOntology() {
		testRootClass = ResponseModelFactory.getOntologyClassResponse
				(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_BASE_ONTOLOGY_CLASS);
		
		OntologyClassResponse contentInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_CONTENT_INFORMATION);
		OntologyClassResponse digitalObject = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_DIGITAL_OBJECT);
		OntologyClassResponse physicalObject = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_PHYSICAL_OBJECT);
		OntologyClassResponse preservationDescriptionInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_PRESERVATION_DESCRIPTION_INFORMATION);
		OntologyClassResponse otherRepesentationInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_OTHER_REPRESENTATION_INFORMATION);
		OntologyClassResponse semanticInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_SEMANTIC_INFORMATION);
		OntologyClassResponse representationInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_REPRESENTATION_INFORMATION);
		OntologyClassResponse structureInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_STRUCTURE_INFORMATION);
		OntologyClassResponse dataObject = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_DATA_OBJECT);
		OntologyClassResponse accessRightsInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_ACCESS_RIGHTS_INFORMATION);
		OntologyClassResponse contextInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_CONTEXT_INFORMATION);
		OntologyClassResponse fixityInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_FIXITY_INFORMATION);
		OntologyClassResponse provenanceInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_PROVENANCE_INFORMATION);
		OntologyClassResponse referenceInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_REFERENCE_INFORMATION);
		OntologyClassResponse domainInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_DOMAIN_INFORMATION);
		OntologyClassResponse researchDomainInformation = ResponseModelFactory.
				getOntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, CLASSNAME_RESEARCH_DOMAIN_INFORMATION);
		
		testRootClass.addSubclass(contentInformation);
		testRootClass.addSubclass(preservationDescriptionInformation);
		contentInformation.addSubclass(dataObject);
		dataObject.addSubclass(digitalObject);
		dataObject.addSubclass(physicalObject);
		dataObject.addSubclass(domainInformation);
		contentInformation.addSubclass(representationInformation);
		representationInformation.addSubclass(otherRepesentationInformation);
		representationInformation.addSubclass(semanticInformation);
		representationInformation.addSubclass(structureInformation);
		preservationDescriptionInformation.addSubclass(accessRightsInformation);
		preservationDescriptionInformation.addSubclass(contextInformation);
		preservationDescriptionInformation.addSubclass(fixityInformation);
		preservationDescriptionInformation.addSubclass(provenanceInformation);
		preservationDescriptionInformation.addSubclass(referenceInformation);
		domainInformation.addSubclass(researchDomainInformation);

		testOntologyClassList = ResponseModelFactory.getOntologyClassListResponse();
		testOntologyClassList.addOntologyClassToList(testRootClass);
		testOntologyClassList.addOntologyClassToList(accessRightsInformation);
		testOntologyClassList.addOntologyClassToList(contentInformation);
		testOntologyClassList.addOntologyClassToList(contextInformation);
		testOntologyClassList.addOntologyClassToList(dataObject);
		testOntologyClassList.addOntologyClassToList(digitalObject);
		testOntologyClassList.addOntologyClassToList(fixityInformation);
		testOntologyClassList.addOntologyClassToList(otherRepesentationInformation);
		testOntologyClassList.addOntologyClassToList(physicalObject);
		testOntologyClassList.addOntologyClassToList(preservationDescriptionInformation);
		testOntologyClassList.addOntologyClassToList(provenanceInformation);
		testOntologyClassList.addOntologyClassToList(referenceInformation);
		testOntologyClassList.addOntologyClassToList(representationInformation);
		testOntologyClassList.addOntologyClassToList(semanticInformation);
		testOntologyClassList.addOntologyClassToList(structureInformation);
		testOntologyClassList.addOntologyClassToList(domainInformation);
		testOntologyClassList.addOntologyClassToList(researchDomainInformation);
		//testOntologyClassList.addOntologyClassToList(concept);
		//testOntologyClassList.addOntologyClassToList(conceptScheme);
	}

	/*@Override
	public IndividualListResponse getIndividualsByClass(String serverUrl, String classname) {
		IndividualListResponse individualListResponse = ResponseModelFactory.getIndividualListResponse();
		
		if (classname.contains(CLASSNAME_DIGITAL_OBJECT)) {
			IndividualResponse dataObject1 = createDataObjectIndividual1();
			IndividualResponse dataObject2 = createDataObjectIndividual2();
			dataObject1.setClassIri(classname);
			dataObject2.setClassIri(classname);
			individualListResponse.addIndividualToList(dataObject1);
			individualListResponse.addIndividualToList(dataObject2);
		} 
		
		return individualListResponse;
	}*/
	
	private IndividualResponse createDataObjectIndividual1() {
		IndividualResponse dataObject = ResponseModelFactory.getIndividualResponse();
		
		DatatypePropertyResponse ontologyBooleanProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyBooleanProperty.setPropertyName(PROPERTY_BOOLEAN);
		ontologyBooleanProperty.setDatatype(OntologyDatatypeResponse.BOOLEAN);
		ontologyBooleanProperty.setValue("true");
		dataObject.addOntologyProperty(ontologyBooleanProperty);
		
		DatatypePropertyResponse ontologyByteProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyByteProperty.setPropertyName(PROPERTY_BYTE);
		ontologyByteProperty.setDatatype(OntologyDatatypeResponse.BYTE);
		ontologyByteProperty.setValue("55");
		dataObject.addOntologyProperty(ontologyByteProperty);
		
		DatatypePropertyResponse ontologyDateProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyDateProperty.setPropertyName(PROPERTY_DATE);
		ontologyDateProperty.setDatatype(OntologyDatatypeResponse.DATE);
		ontologyDateProperty.setValue("17413");
		dataObject.addOntologyProperty(ontologyDateProperty);
		
		DatatypePropertyResponse ontologyDateTimeProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyDateTimeProperty.setPropertyName(PROPERTY_DATE_TIME);
		ontologyDateTimeProperty.setDatatype(OntologyDatatypeResponse.DATETIME);
		ontologyDateTimeProperty.setValue("1472947200");
		dataObject.addOntologyProperty(ontologyDateTimeProperty);
		
		DatatypePropertyResponse ontologyDurationProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyDurationProperty.setPropertyName(PROPERTY_DURATION);
		ontologyDurationProperty.setDatatype(OntologyDatatypeResponse.DURATION);
		ontologyDurationProperty.setValue("P5Y2M10D");
		dataObject.addOntologyProperty(ontologyDurationProperty);
		
		return dataObject;
	}
	
	private IndividualResponse createDataObjectIndividual2() {
		IndividualResponse dataObject = ResponseModelFactory.getIndividualResponse();
		
		DatatypePropertyResponse ontologyBooleanProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyBooleanProperty.setPropertyName(PROPERTY_BOOLEAN);
		ontologyBooleanProperty.setDatatype(OntologyDatatypeResponse.BOOLEAN);
		ontologyBooleanProperty.setValue("false");
		dataObject.addOntologyProperty(ontologyBooleanProperty);
		
		DatatypePropertyResponse ontologyByteProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyByteProperty.setPropertyName(PROPERTY_BYTE);
		ontologyByteProperty.setDatatype(OntologyDatatypeResponse.BYTE);
		ontologyByteProperty.setValue("66");
		dataObject.addOntologyProperty(ontologyByteProperty);
		
		DatatypePropertyResponse ontologyDateProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyDateProperty.setPropertyName(PROPERTY_DATE);
		ontologyDateProperty.setDatatype(OntologyDatatypeResponse.DATE);
		ontologyDateProperty.setValue("17410");
		dataObject.addOntologyProperty(ontologyDateProperty);
		
		DatatypePropertyResponse ontologyDateTimeProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyDateTimeProperty.setPropertyName(PROPERTY_DATE_TIME);
		ontologyDateTimeProperty.setDatatype(OntologyDatatypeResponse.DATETIME);
		ontologyDateTimeProperty.setValue("1462320000");
		dataObject.addOntologyProperty(ontologyDateTimeProperty);
		
		DatatypePropertyResponse ontologyDurationProperty = ResponseModelFactory.getOntologyPropertyResponse();
		ontologyDurationProperty.setPropertyName(PROPERTY_DURATION);
		ontologyDurationProperty.setDatatype(OntologyDatatypeResponse.DURATION);
		ontologyDurationProperty.setValue("P6Y3M11D");
		dataObject.addOntologyProperty(ontologyDurationProperty);
		
		return dataObject;
	}
	
	private OntologyClassResponse getOntologyClassByLocalName(String fullName) {
		for (int i = 0; i < testOntologyClassList.getOntologyClassCount(); i++) {
			OntologyClassResponse ontologyClass = testOntologyClassList.getOntologyClassAt(i);
				
			//String ontologyClassFullName = ontologyClass.getNamespace() + PackagingToolkitConstants.NAMESPACE_SEPARATOR  + ontologyClass.getLocalName();
			if (ontologyClass.getLocalName().equals(fullName)) {
				return ontologyClass;
			}
		}
		
		return null;
	}
	
	@Override
	public void setClientModelFactory(ClientModelFactory clientModelFactory) {
		this.clientModelFactory = clientModelFactory;
	}

	@Override
	public InputStream downloadNotVirtualArchive(ArchiveResponse archiveResponse, String serverUrl) {
		String fileContent = "Package is not virtual ";
		
		fileContent = fileContent + "and serialization format is " + archiveResponse.getSerializationFormat();
		
		InputStream inputStream = new ByteArrayInputStream(
				fileContent.getBytes(StandardCharsets.UTF_8));
		return inputStream;
	}
	
	@Override
	public InputStream downloadVirtualArchive(ArchiveResponse archiveResponse, String serverUrl) {
		String fileContent = "Package is virtual ";
		
		fileContent = fileContent + "and serialization format is " + archiveResponse.getSerializationFormat();
		
		InputStream inputStream = new ByteArrayInputStream(
				fileContent.getBytes(StandardCharsets.UTF_8));
		return inputStream;
	}

	@Override
	public void addHttpReference(ReferenceResponse referenceResponse, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean addFileReference(DigitalObjectResponse digitalObjectResponse, String serverUrl) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void uploadFileToStorage(File file, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IndividualListResponse findPredefinedIndividualsByClass(String serverUrl, String iriOfClass) {
		IndividualListResponse individualListResponse = ResponseModelFactory.getIndividualListResponse();
		
		if (iriOfClass.contains(CLASSNAME_DATA_OBJECT)) {
			for (int i = 0; i < DATA_OBJECT_INDIVIDUALS_COUNT; i++) {
				IndividualResponse individual = ResponseModelFactory.getIndividualResponse();
				Uuid uuid = new Uuid();
				individual.setUuid(ResponseModelFactory.getUuidResponse(uuid));
				individual.setClassIri(iriOfClass);
				DatatypePropertyResponse datatypeProperty1 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty1.setDatatype(OntologyDatatypeResponse.DOUBLE);
				datatypeProperty1.setLabel(PROPERTY_DOUBLE);
				datatypeProperty1.setValue("1.2345" + i);
				datatypeProperty1.setPropertyName("http://kdmp4.feu.de");
				
				DatatypePropertyResponse datatypeProperty2 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty2.setDatatype(OntologyDatatypeResponse.BOOLEAN);
				datatypeProperty2.setLabel(PROPERTY_BOOLEAN);
				datatypeProperty2.setPropertyName("http://kdmp4.feu.de");
				
				if (i % 2 == 0) {
					datatypeProperty2.setValue("true");
				} else {
					datatypeProperty2.setValue("false");
				}
				
				DatatypePropertyResponse datatypeProperty3 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty3.setDatatype(OntologyDatatypeResponse.STRING);
				datatypeProperty3.setLabel(PROPERTY_STRING);
				datatypeProperty3.setValue("A string " + i);
				datatypeProperty3.setPropertyName("http://kdmp4.feu.de");
				
				DatatypePropertyResponse datatypeProperty4 = ResponseModelFactory.getOntologyPropertyResponse();
				datatypeProperty4.setDatatype(OntologyDatatypeResponse.INTEGER);
				datatypeProperty4.setLabel(PROPERTY_INTEGER);
				String value = String.valueOf(i);
				datatypeProperty4.setValue(value);
				datatypeProperty4.setPropertyName("http://kdmp4.feu.de");
				
				individual.addOntologyProperty(datatypeProperty1);
				individual.addOntologyProperty(datatypeProperty2);
				individual.addOntologyProperty(datatypeProperty3);
				individual.addOntologyProperty(datatypeProperty4);
				
				individualListResponse.addIndividualToList(individual);
			}
		} else if (iriOfClass.contains(CLASSNAME_RESEARCH_DOMAIN_INFORMATION)) {
			final IndividualResponse individual = ResponseModelFactory.getIndividualResponse();
			final Uuid uuid = new Uuid();
			individual.setUuid(ResponseModelFactory.getUuidResponse(uuid));
			final IriListResponse iris = new IriListResponse();
			iris.addIriToList(new IriResponse(NAMESPACE_SKOS, SKOS_CLASS_CONCEPT_SCHEME));
			final ObjectPropertyResponse ontologyProperty = new ObjectPropertyResponse();
			ontologyProperty.setValue(iris);
			ontologyProperty.setPropertyName(ONTOLOGY_CLASSES_NAMESPACE + "#hasTaxonomie");
			ontologyProperty.setRange(NAMESPACE_SKOS + "#" + SKOS_CLASS_CONCEPT_SCHEME);
			individual.addObjectProperty(ontologyProperty);
			individualListResponse.addIndividualToList(individual);
		} else if (iriOfClass.contains(SKOS_CLASS_CONCEPT_SCHEME)) {
			final IndividualResponse individual = ResponseModelFactory.getIndividualResponse();
			individual.setIriOfIndividual(new IriResponse("http://totem.semedica.com/taxonomy/The%20ACM%20Computing%20Classification%20System%20(CCS)", ""));
			individual.setClassIri(NAMESPACE_SKOS + "#" + SKOS_CLASS_CONCEPT_SCHEME);
			individualListResponse.addIndividualToList(individual);
		}
		
		return individualListResponse;
	}

	@Override
	public void saveIndividual(String serverUrl, IndividualResponse ontologyIndividual) {
		String classIri = ontologyIndividual.getClassIri();
		int i = 0;
		boolean found = false;
		OntologyClassResponse ontologyClass = null;
		
		if (classIri.contains(CLASSNAME_DATA_OBJECT)) {
			DatatypePropertyListResponse datatypeProperties = ontologyIndividual.getOntologyDatatypeProperties();
			DatatypePropertyResponse booleanProperty = datatypeProperties.getOntologyPropertyAt(0);
			boolean valueOfBooleanProperty = booleanProperty.getValueAsBoolean(); 
			if(valueOfBooleanProperty == true) {
				StringList messages = PackagingToolkitModelFactory.getStringList();
				messages.addStringToList("This is a test");
				messages.addStringToList("Assume the reasoner has found an error");
				messages.addStringToList("To avoid this exception, please set booleanProperty to false");
				
				OntologyReasonerException exception = OntologyReasonerException.createIndividualInvalidException(messages);
				throw exception;
			}
		}
		
		// Look for the class of this individual.
		while(found == false && i < testOntologyClassList.getOntologyClassCount()) {
			ontologyClass = testOntologyClassList.getOntologyClassAt(i);
			
			String iri = ontologyClass.getIri(); 
			if (classIri.equals(iri)) {
				found = true;
			} else {
				i++;
			}
		}
	}
	
	@Override
	public OntologyClassResponse getAllPremisClasses(String serverUrl) {
		OntologyClassResponse premis = new OntologyClassResponse(NAMESPACE_PREMIS, "Premis");
		OntologyClassResponse action = new OntologyClassResponse(NAMESPACE_PREMIS, "Action");
		OntologyClassResponse agent = new OntologyClassResponse(NAMESPACE_PREMIS, "agent");
		OntologyClassResponse file = new OntologyClassResponse(NAMESPACE_PREMIS, "File");
		premis.addSubclass(agent);
		premis.addSubclass(action);
		premis.addSubclass(file);
		return premis;
	}

	@Override
	public void saveInformationPackage(UuidResponse uuidOfInformationPackage, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pingServer(String serverUrl) {
		
	}

	@Override
	public void updateIndividual(String serverUrl, IndividualResponse ontologyIndividual) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public ReferenceListResponse findReferencesOfInformationPackage(String serverUrl, String uuidOfInformationPackage) {
		UuidResponse uuid = ResponseModelFactory.getUuidResponse(uuidOfInformationPackage);
		
		ReferenceListResponse references = ResponseModelFactory.createEmptyReferenceListResponse();
		if (uuidOfInformationPackage.equals("9dfaf7bc-b9bd-4201-a8dc-d3b84c357572")) {
			ReferenceResponse referenceResponse = ResponseModelFactory.getReferenceResponse();
			referenceResponse.setUuidOfFile(new UuidResponse());
			referenceResponse.setUuidOfInformationPackage(uuid);
			referenceResponse.setUrl("/tmp/aTestFile.txt");
			references.addReferenceToList(referenceResponse);
		} else if (uuidOfInformationPackage.equals("9dfaf7bc-b9bd-4201-a8dc-d3b84c357575")) {
			ReferenceResponse referenceResponse = ResponseModelFactory.getReferenceResponse();
			referenceResponse.setUrl("http://stl.htwsaar.de/myDocument.txt");
			referenceResponse.setUuidOfInformationPackage(uuid);
			references.addReferenceToList(referenceResponse);
		}
		
		return references;
	}

	@Override
	public OntologyClassResponse getAllSkosClasses(String serverUrl) {
		OntologyClassResponse skos = new OntologyClassResponse(NAMESPACE_SKOS, "SKOS");
		OntologyClassResponse concept = new OntologyClassResponse(NAMESPACE_SKOS, "Concept");
		OntologyClassResponse collection = new OntologyClassResponse(NAMESPACE_SKOS, "Collection");
		OntologyClassResponse conceptScheme = new OntologyClassResponse(NAMESPACE_SKOS, SKOS_CLASS_CONCEPT_SCHEME);
		
		skos.addSubclass(conceptScheme);
		skos.addSubclass(collection);
		skos.addSubclass(concept);
		
		return skos;
	}

	@Override
	public void cancelInformationPackageCreation(Uuid uuid, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadInformationPackage(String uuid, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteView(final int viewId, final String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IndividualListResponse findSavedIndividualsByClass(final String serverUrl,
			final OntologyClassResponse ontologyClassResponse) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addMetadataToInformationPackage(final String serverUrl, final MediatorDataListResponse mediatorData) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TaxonomyListResponse getAllTaxonomies(final String serverUrl) {
		if (taxonomies == null) {
			initializeTaxonomyList();
		}
		
		return taxonomies;
	}
	
	/**
	 * Creates a taxonomy list with test data.
	 */
	private void initializeTaxonomyList() {
		final TaxonomyIndividualResponse rootIndividual = new TaxonomyIndividualResponse();
		rootIndividual.setIri("http://totem.semedica.com/taxonomy/The%20ACM%20Computing%20Classification%20System%20(CCS)");
		
		final TaxonomyIndividualResponse informationSystem = new TaxonomyIndividualResponse();
		informationSystem.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10002951");
		informationSystem.setPreferredLabel("Information systems");
		rootIndividual.addNarrower(informationSystem);
		
		final TaxonomyIndividualResponse mathematicsOfComputing = new TaxonomyIndividualResponse();
		mathematicsOfComputing.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10002950");
		mathematicsOfComputing.setPreferredLabel("Mathematics of computing");
		rootIndividual.addNarrower(mathematicsOfComputing);
		
		final TaxonomyIndividualResponse dataManagementSystems = new TaxonomyIndividualResponse();
		dataManagementSystems.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10002952");
		dataManagementSystems.setPreferredLabel("Data management systems");
		mathematicsOfComputing.addNarrower(dataManagementSystems);
		
		final TaxonomyIndividualResponse databaseDesignAndModel = new TaxonomyIndividualResponse();
		databaseDesignAndModel.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10002953");
		databaseDesignAndModel.setPreferredLabel("Database design and models");
		dataManagementSystems.addNarrower(databaseDesignAndModel);
		
		final TaxonomyIndividualResponse dataStructures = new TaxonomyIndividualResponse();
		dataStructures.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10002971");
		dataStructures.setPreferredLabel("Data structures");
		dataManagementSystems.addNarrower(dataStructures);
		
		final TaxonomyIndividualResponse informationStorageSystems = new TaxonomyIndividualResponse();
		informationStorageSystems.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10003152");
		informationStorageSystems.setPreferredLabel("Information storage systems");
		informationSystem.addNarrower(informationStorageSystems);
		
		final TaxonomyIndividualResponse recordStorageSystems = new TaxonomyIndividualResponse();
		recordStorageSystems.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10003161");
		recordStorageSystems.setPreferredLabel("Record storage systems");
		informationStorageSystems.addNarrower(recordStorageSystems);
		
		final TaxonomyIndividualResponse storageReplication = new TaxonomyIndividualResponse();
		storageReplication.setIri("file:/C:/Users/heike/Documents/Hagen/Promotion/Modeling/Ontologies/official/ACMComputingClassificationSystemSKOSTaxonomy.xml#10003166");
		storageReplication.setPreferredLabel("Storage replication");
		informationStorageSystems.addNarrower(storageReplication);
		
		final TaxonomyResponse taxonomy = new TaxonomyResponse();
		taxonomy.setNamespace("http://totem.semedica.com/taxonomy/The%20ACM%20Computing%20Classification%20System%20(CCS)");
		taxonomy.setTitle("The ACM Computing Classification System (CCS)");
		taxonomy.setRootIndividual(rootIndividual);
		
		taxonomies = new TaxonomyListResponse();
		taxonomies.addTaxonomy(taxonomy);
	}

	@Override
	public OntologyClassResponse getTaxonomyStartClass(String serverUrl) {
		final String localName = "Research_Domain_Information";
		final OntologyClassResponse taxonomyStartClass = new OntologyClassResponse(ONTOLOGY_CLASSES_NAMESPACE, localName);
		return taxonomyStartClass;
	}

	@Override
	public void saveSelectedTaxonomyIndividuals(TaxonomyIndividualListResponse taxonomyIndividuals, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateOntologyCache(String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IndividualListResponse findAllIndividualsOfInformationPackage(String serverUrl,
			UuidResponse uuidOfInformationPackage) {
		return null;
	}

	@Override
	public void assignIndividualsToInformationPackage(UuidListResponse uuidsResponse, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unassignIndividualsFromInformationPackage(UuidListResponse uuidsResponse, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void assignTaxonomyIndividuals(IriListResponse irisOfTaxonomyIndividuals, String serverUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IriListResponse findAllAssignedTaxonomyIndividuals(String serverUrl,
			UuidResponse uuidOfInformationPackageResponse) {
		// TODO Auto-generated method stub
		return null;
	}
}