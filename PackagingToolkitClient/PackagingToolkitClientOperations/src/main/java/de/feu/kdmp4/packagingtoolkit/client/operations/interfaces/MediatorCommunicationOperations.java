package de.feu.kdmp4.packagingtoolkit.client.operations.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementListResponse;

public interface MediatorCommunicationOperations {
	/**
	 * Sends a request with the session id to the mediator and gets the extracted
	 * metadata. 
	 * @return A list with the extracted metadata.
	 */
	MetadataElementListResponse getMetadataFromMediator();
	/**
	 * Reads the data sources that are registered on the mediator.
	 * @param mediatorUrl The url of the mediator.
	 * @return Contains all data sources of the mediator.
	 */
	DataSourceListResponse readDataSources(String mediatorUrl);
	/**
	 * Sends a file to the mediator in order to extract its meta data.
	 * @param file The file whose meta data we want to extract.
	 * @param mediatorUrl The url of the mediator.
	 * @return The meta data of file.
	 */
	MediatorDataListResponse collectData(File file, String mediatorUrl);
	/**
	 * Sends a message to the mediator for testing if the mediator is accessible.
	 * @param mediatorUrl The url of the mediator.
	 */
	void pingMediator(String mediatorUrl);
	/**
	 * Checks the installation directories of all data sources.
	 * @param mediatorUrl The url of the mediator. 
	 * @throws MediatorConfigurationException if the installation directory of a data source is not correct.
	 */
	void checkDataSourceInstallations(String mediatorUrl);
	/**
	 * Saves the data sources the user has configured in the gui on the mediator.
	 * @param mediatorUrl The url of the mediator.
	 * @param dataSourceListResponse The data sources configured by the user.
	 */
	void saveDataSourceConfiguration(String mediatorUrl, DataSourceListResponse dataSourceListResponse);
}
