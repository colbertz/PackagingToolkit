package de.feu.kdmp4.packagingtoolkit.client.operations.classes;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.MediatorCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MetadataElementResponse;

public class MockedMediatorCommunicationOperationsImpl implements MediatorCommunicationOperations {
	private static final int METADATA_COUNT = 10;
	
	@Override
	public MetadataElementListResponse getMetadataFromMediator() {
		MetadataElementListResponse elementList = ResponseModelFactory.getMetadataElementListResponse();
		
		for (int i = 0; i < METADATA_COUNT; i++) {
			String name = String.valueOf(i);
			String value = String.valueOf(i + 10);
			MetadataElementResponse metadataElement = ResponseModelFactory.
					getMetadataElementResponse(name, value); 
			elementList.addMetadata(metadataElement);
			// Create some conflicting metadata.
			if (i == 1 || i == 4) {
				String conflictingValue = String.valueOf(i + 11);
				MetadataElementResponse conflict = ResponseModelFactory.
						getMetadataElementResponse(name, conflictingValue);
				metadataElement.addConflictingMetadata(conflict);
			}
			
			// Create some metadata elements with lists. 
			if (i == 3 || i == 6) {
				String listValue = String.valueOf(i + 1);
				metadataElement.addValue(listValue);
			}
		}
		
		
		return elementList;
	}
	
	@Override
	public DataSourceListResponse readDataSources(String mediatorUrl) {
		int dataSourceCount = 2;
		int toolsPerDataSourceCount = 5;
		
		DataSourceListResponse dataSourceListResponse = ResponseModelFactory.getDataSourceListResponse();

		for (int i = 0; i < dataSourceCount; i++) {
			DataSourceResponse dataSourceResponse = ResponseModelFactory.getDataSourceResponse();
			DataSourceToolListResponse dataSourceTools = ResponseModelFactory.getDataSourceToolListResponse();
			
			for (int j = i * toolsPerDataSourceCount + 1; j <= i * toolsPerDataSourceCount + toolsPerDataSourceCount; j++) {
				DataSourceToolResponse dataSourceTool = ResponseModelFactory.getDataSourceToolResponse();
				dataSourceTool.setDataSourceToolName("Tool" + j);
				if (j % 2 == 0) {
					dataSourceTool.setActivated(true);
				} else {
					dataSourceTool.setActivated(false);
				}
				dataSourceTools.addDataSourceTool(dataSourceTool);
			}
			dataSourceResponse.setDataSourceName("DataSource" + i);
			dataSourceResponse.setDataSourceTools(dataSourceTools);
			dataSourceListResponse.addDataSource(dataSourceResponse);
		}
		
		
		
		/*DataSourceToolResponse dataSourceTool2 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceTool2.setDataSourceToolName("Tool2");
		dataSourceTool2.setActivated(false);
		dataSourceTools1.addDataSourceTool(dataSourceTool2);
		
		DataSourceToolResponse dataSourceTool3 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceTool3.setDataSourceToolName("Tool3");
		dataSourceTool3.setActivated(true);
		dataSourceTools1.addDataSourceTool(dataSourceTool3);
		
		DataSourceResponse dataSource1 = ResponseModelFactory.getDataSourceResponse();
		dataSource1.setDataSourceName("DataSource1");
		dataSource1.setDataSourceTools(dataSourceTools1);
		dataSourceListResponse.addDataSource(dataSource1);

		DataSourceToolListResponse dataSourceTools2 = ResponseModelFactory.getDataSourceToolListResponse();
		DataSourceToolResponse dataSourceTool4 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceTool4.setDataSourceToolName("Tool4");
		dataSourceTool4.setActivated(true);
		dataSourceTools2.addDataSourceTool(dataSourceTool4);
		
		DataSourceToolResponse dataSourceTool5 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceTool5.setDataSourceToolName("Tool5");
		dataSourceTool5.setActivated(false);
		dataSourceTools2.addDataSourceTool(dataSourceTool5);
		
		DataSourceToolResponse dataSourceTool6 = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceTool6.setDataSourceToolName("Tool6");
		dataSourceTool6.setActivated(true);
		dataSourceTools2.addDataSourceTool(dataSourceTool6);
		
		DataSourceResponse dataSource2 = ResponseModelFactory.getDataSourceResponse();
		dataSource2.setDataSourceName("DataSource2");
		dataSource2.setDataSourceTools(dataSourceTools2);
		dataSourceListResponse.addDataSource(dataSource2);*/

		return dataSourceListResponse;
	}

	@Override
	public MediatorDataListResponse collectData(File file, String mediatorUrl) {
		MediatorDataResponse fileSizeData = new MediatorDataResponse("fileSize", "640");
		MediatorDataResponse pageCountData = new MediatorDataResponse("pageCount", "12");
		MediatorDataResponse titleData = new MediatorDataResponse("title", "FAtiMa");
		MediatorDataListResponse mediatorDataListResponse = new MediatorDataListResponse();
		mediatorDataListResponse.addMediatorData(titleData);
		mediatorDataListResponse.addMediatorData(pageCountData);
		mediatorDataListResponse.addMediatorData(fileSizeData);
		
		return mediatorDataListResponse;
	}

	@Override
	public void pingMediator(String mediatorUrl) {
		// Empty implementation because the mocked mediator is always accessible.
	}

	@Override
	public void checkDataSourceInstallations(String mediatorUrl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveDataSourceConfiguration(String mediatorUrl, DataSourceListResponse dataSourceListResponse) {
		// TODO Auto-generated method stub
		
	}
}
