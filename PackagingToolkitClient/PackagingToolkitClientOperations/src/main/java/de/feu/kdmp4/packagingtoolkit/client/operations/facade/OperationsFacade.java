package de.feu.kdmp4.packagingtoolkit.client.operations.facade;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ClientConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.MediatorCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.client.operations.interfaces.ServerCommunicationOperations;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ArchiveResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DigitalObjectResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.IndividualResponse;
import de.feu.kdmp4.packagingtoolkit.response.IriListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.OntologyClassResponse;
import de.feu.kdmp4.packagingtoolkit.response.PackageResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ReferenceResponse;
import de.feu.kdmp4.packagingtoolkit.response.ServerConfigurationDataResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyIndividualListResponse;
import de.feu.kdmp4.packagingtoolkit.response.TaxonomyListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidListResponse;
import de.feu.kdmp4.packagingtoolkit.response.UuidResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewListResponse;
import de.feu.kdmp4.packagingtoolkit.response.ViewResponse;

/**
 * Contains method to encapsulate the methods of the operations classes. The caller
 * has only to use these methods and has not to know which class exactly process his
 * call. All methods only redirect the call to the operations classes.
 * @author Christopher Olbertz
 *
 */
public class OperationsFacade {
	private ClientConfigurationOperations clientConfigurationOperations;
	private ServerCommunicationOperations serverCommunicationOperations;
	private MediatorCommunicationOperations mediatorCommunicationOperations;
	
	public boolean addFileReference(final DigitalObjectResponse digitalObjectResponse, final String serverUrl) {
		return serverCommunicationOperations.addFileReference(digitalObjectResponse, serverUrl);
	}
	
	public ReferenceListResponse findReferencesOfInformationPackage(final String serverUrl, final String uuidOfInformationPackage) {
		return serverCommunicationOperations.findReferencesOfInformationPackage(serverUrl, uuidOfInformationPackage);
	}
	
	public void cancelInformationPackageCreation(final Uuid uuid, final String serverUrl) {
		serverCommunicationOperations.cancelInformationPackageCreation(uuid, serverUrl);
	}
	
	public void saveInformationPackage(final UuidResponse uuidOfInformationPackage, final String serverUrl) {
		serverCommunicationOperations.saveInformationPackage(uuidOfInformationPackage, serverUrl);
	}
	
	/**
	 * Forces the initialization of the ontology cache. Can be used if a new base ontology has been uploaded.
	 */
	public void updateOntologyCache(final String serverUrl) {
		serverCommunicationOperations.updateOntologyCache(serverUrl);
	}
	
	/**
	 * Determines the time after that the individuals have to be updated from the server. 
	 * @param configurationDirectory The path to the directory that contains the configuration file.
	 * @return The time after that the individuals have to be updated from the server in milliseconds.
	 */
	public long getIndividualsUpdateTimePath(final String configurationDirectory) {
		return clientConfigurationOperations.getIndividualsUpdateTimePath(configurationDirectory);
	}
	
	/**
	 * Saves individuals of a taxonomy as selected for an information package.
	 * @param taxonomyIndividuals The individuals that have been selected for the information package.
	 * @param serverUrl The url of the server.
	 */
	public void saveSelectedTaxonomyIndividuals(final TaxonomyIndividualListResponse taxonomyIndividuals, final String serverUrl) {
		serverCommunicationOperations.saveSelectedTaxonomyIndividuals(taxonomyIndividuals, serverUrl);
	}
	
	/**
	 * Determines the path and the name of the file with the logo from the configuration file. If the configuration
	 * file does not exist, a default value is returned. The file is saved in the images directory of the client. There is no
	 * file prefix appended to this path. 
	 * @param configurationDirectory The path to the directory that contains the configuration file. 
	 * @return The path and name of the file with the logo without a file prefix. 
	 */
	public File getLogoPath(final String configurationDirectory) {
		return clientConfigurationOperations.getLogoPath(configurationDirectory);
	}
	
	/**
	 * Saves a new file for the logo.
	 * @param temporaryLogoFile The file with the logo in the temporary directory.
	 * @param configurationDirectory The path to the directory that contains the configuration file.
	 */
	public void saveLogo(final File temporaryLogoFile, final String configurationDirectory) {
		clientConfigurationOperations.saveLogo(temporaryLogoFile, configurationDirectory);
	}
	
	public void loadInformationPackage(final String uuid, final String serverUrl) {
		serverCommunicationOperations.loadInformationPackage(uuid, serverUrl);
	}
	
	public void pingServer(final String serverUrl) {
		serverCommunicationOperations.pingServer(serverUrl);
	}
	
	public void uploadeFileToStorage(final File file, final String serverUrl) {
		serverCommunicationOperations.uploadFileToStorage(file, serverUrl);
	}
	
	public void addHttpReference(final ReferenceResponse reference, final String serverUrl) {
		serverCommunicationOperations.addHttpReference(reference, serverUrl);
	}
	
	/**
	 * Adds meta data to an information package.
	 * @param serverUrl The url of the server.
	 * @param mediatorData Contains the data that should be send to the server and the information about the
	 * information package.
	 */
	public void addMetadataToInformationPackage(String serverUrl, MediatorDataListResponse mediatorData) {
		serverCommunicationOperations.addMetadataToInformationPackage(serverUrl, mediatorData);
	}
	
	public MediatorDataListResponse collectData(final File file, final String mediatorUrl) {
		return mediatorCommunicationOperations.collectData(file, mediatorUrl);
	}
	
	public void configureRuleOntology(final File ontologyFile, final String serverUrl) {
		serverCommunicationOperations.configureRuleOntology(ontologyFile, serverUrl);
	}
	
	/**
	 * Determines the class the taxonomies start with.
	 * @param serverUrl The url of the server.
	 * @return The class the taxonomies start with.
	 */
	public OntologyClassResponse getTaxonomyStartClass(final String serverUrl)  {
		return serverCommunicationOperations.getTaxonomyStartClass(serverUrl);
	}
	
	public void saveDataSourceConfiguration(final String mediatorUrl, final DataSourceListResponse dataSourceListResponse) {
		mediatorCommunicationOperations.saveDataSourceConfiguration(mediatorUrl, dataSourceListResponse);
	}
	
	public void configureBaseOntology(final File ontologyFile, final String serverUrl) {
		serverCommunicationOperations.configureBaseOntology(ontologyFile, serverUrl);
	}
	
	public void deleteView(final int viewId, final String serverUrl) {
		serverCommunicationOperations.deleteView(viewId, serverUrl);
	}
	
	public PackageResponse createSubmissionInformationUnit(final String serverUrl, final String title) {
		return serverCommunicationOperations.createSubmissionInformationUnit(serverUrl, title);
	}
	
	public PackageResponse createSubmissionInformationPackage(final String serverUrl, final String title) {
		return serverCommunicationOperations.createSubmissionInformationPackage(serverUrl, title);
	}
	
	public IndividualListResponse findSavedIndividualsByClass(final String serverUrl, final OntologyClassResponse ontologyClassResponse) {
		return serverCommunicationOperations.findSavedIndividualsByClass(serverUrl, ontologyClassResponse);
	}
	
	public DataSourceListResponse readDataSources(final String mediatorUrl) {
		return mediatorCommunicationOperations.readDataSources(mediatorUrl);
	}
	
	public void updateIndividual(final String serverUrl, final IndividualResponse ontologyIndividual) {
		serverCommunicationOperations.updateIndividual(serverUrl, ontologyIndividual);
	}
	
	public void checkDataSourceInstallations(final String mediatorUrl) {
		mediatorCommunicationOperations.checkDataSourceInstallations(mediatorUrl);
	}
	
	public void deleteArchive(final Uuid uuid, final String serverUrl) {
		serverCommunicationOperations.deleteArchive(uuid, serverUrl);
	}
	
	public File downloadBaseOntology(final String serverUrl, final File downloadDirectory) {
		return serverCommunicationOperations.downloadBaseOntology(serverUrl, downloadDirectory);
	}
	
	public InputStream downloadNotVirtualArchive(final ArchiveResponse archiveResponse, final String serverUrl) {
		return serverCommunicationOperations.downloadNotVirtualArchive(archiveResponse, serverUrl);
	}
	
	public InputStream downloadVirtualArchive(final ArchiveResponse archiveResponse, final String serverUrl) {
		return serverCommunicationOperations.downloadVirtualArchive(archiveResponse, serverUrl);
	}
	
	public ViewListResponse getAllViews(final String serverUrl) {
		return serverCommunicationOperations.getAllViews(serverUrl);
	}
	
	public ArchiveListResponse getArchiveList(final String serverUrl) {
		return serverCommunicationOperations.getArchiveList(serverUrl);
	}
	
	public ClientConfigurationData getClientConfigurationData() {
		return clientConfigurationOperations.getClientConfigurationData();
	}
	
	public void pingMediator(final String mediatorUrl) {
		mediatorCommunicationOperations.pingMediator(mediatorUrl);
	}
	
	public DigitalObjectListResponse getDigitalObjectsOfInformationPackage(final UuidResponse uuid) {
		return serverCommunicationOperations.getDigitalObjectsOfInformationPackage(uuid);
	}
	
	public OntologyClassResponse getHierarchy(final String serverUrl) {
		return serverCommunicationOperations.getHierarchy(serverUrl);
	}
	
	public OntologyClassResponse getHierarchy(final String serverUrl, final StringListResponse classnames) {
		return serverCommunicationOperations.getHierarchy(serverUrl, classnames);
	}
	
	/*public IndividualListResponse getIndividualsByClass(String serverUrl, String classname) {
		return serverCommunicationOperations.getIndividualsByClass(serverUrl, classname);
	}*/
	
	public IndividualListResponse findPredefinedIndividualsByClass(final String serverUrl, final String iriOfClass) {
		return serverCommunicationOperations.findPredefinedIndividualsByClass(serverUrl, iriOfClass);
	}
	
	public IndividualListResponse getSavedIndividuals(final String serverUrl, final OntologyClassResponse ontologyClassResponse, 
			final UuidResponse uuidOfInformationPackage) {
		return serverCommunicationOperations.getSavedIndividuals(serverUrl, ontologyClassResponse, uuidOfInformationPackage);
	}
	
	public OntologyClassResponse getPropertiesOfClass(final String serverUrl, final String classname) {
		return serverCommunicationOperations.getPropertiesOfClass(serverUrl, classname);
	}
	
	public OntologyClassResponse getAllSkosClasses(final String serverUrl) {
		return serverCommunicationOperations.getAllSkosClasses(serverUrl);
	}
	
	public OntologyClassResponse getAllPremisClasses(final String serverUrl) {
		return serverCommunicationOperations.getAllPremisClasses(serverUrl);
	}
	
	public String getServerUrl() {
		ClientConfigurationData clientConfigurationData = clientConfigurationOperations.getClientConfigurationData();
		return clientConfigurationData.getServerUrl();
	}
	
	public File getTemporaryDirectoryOnClient() {
		return clientConfigurationOperations.getTemporaryDirectoryOnClient();
	}
	
	public ServerConfigurationDataResponse readServerConfiguration(final String serverUrl) {
		return serverCommunicationOperations.readServerConfiguration(serverUrl);
	}
	
	public void saveIndividual(final String serverUrl, final IndividualResponse individual) {
		serverCommunicationOperations.saveIndividual(serverUrl, individual);
	}
	
	public IndividualListResponse findAllIndividualsOfInformationPackage(final String serverUrl,
			final UuidResponse uuidOfInformationPackage) {
		return serverCommunicationOperations.findAllIndividualsOfInformationPackage(serverUrl, uuidOfInformationPackage);
	}
	
	public void saveNewView(final String serverUrl, final ViewResponse viewResponse) {
		serverCommunicationOperations.saveNewView(serverUrl, viewResponse);
	}
	
	public void writeClientConfigurationData(final ClientConfigurationData 
			clientConfigurationData) throws IOException {
		clientConfigurationOperations.writeClientConfigurationData(clientConfigurationData);
	}
	
	public void writeServerConfiguration(final ServerConfigurationDataResponse configurationData,
			final String serverUrl) {
		serverCommunicationOperations.writeServerConfiguration(configurationData, serverUrl);
	}

	public ClientConfigurationOperations getClientConfigurationOperations() {
		return clientConfigurationOperations;
	}

	public void setClientConfigurationOperations(final ClientConfigurationOperations clientConfigurationOperations) {
		this.clientConfigurationOperations = clientConfigurationOperations;
	}

	public ServerCommunicationOperations getServerCommunicationOperations() {
		return serverCommunicationOperations;
	}

	public void setServerCommunicationOperations(final ServerCommunicationOperations serverCommunicationOperations) {
		this.serverCommunicationOperations = serverCommunicationOperations;
	}
	
	public void setMediatorCommunicationOperations(final MediatorCommunicationOperations mediatorCommunicationOperations) {
		this.mediatorCommunicationOperations = mediatorCommunicationOperations;
	}
	
	public TaxonomyListResponse getAllTaxonomies(final String serverUrl) {
		return serverCommunicationOperations.getAllTaxonomies(serverUrl);
	}

	/**
	 * Sends the uuids of some indidivuals to the server. These individuals should be assigned to
	 * a certain information package.
	 * @param uuidsResponse Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals.
	 * @param serverUrl The url of the server. 
	 */
	public void assignIndividualsToInformationPackage(final UuidListResponse uuidsOfIndividualResponse,
			final String serverUrl) {
		serverCommunicationOperations.assignIndividualsToInformationPackage(uuidsOfIndividualResponse, serverUrl);
	}

	/**
	 * Sends the uuids of some indidivuals to the server. These individuals should be unassigned from
	 * a certain information package.
	 * @param uuidsResponse Contains the uuid of the information package as first element followed
	 * by the uuids of the individuals.
	 * @param serverUrl The url of the server.
	 */
	public void unassignIndividualsFromInformationPackage(final UuidListResponse uuidsResponse, final String serverUrl) {
		serverCommunicationOperations.unassignIndividualsFromInformationPackage(uuidsResponse, serverUrl);
	}
	
	/**
	 * Adds taxonomy individuals to an information package. 
	 * @param irisOfIndividuals The iris of the individuals from the taxonomy that should be 
	 * assigned to the information package. The first entry is the iri of the information package.
	 */
	public void assignTaxonomyIndividuals(final IriListResponse irisOfTaxonomyIndividuals, final String serverUrl) {
		serverCommunicationOperations.assignTaxonomyIndividuals(irisOfTaxonomyIndividuals, serverUrl);
	}

	/**
	 * Requests all taxonomy individuals that are assigned to a certain information package from 
	 * the server.
	 * @param uuidOfInformationPackage The uuid of the information package whose taxonomy 
	 * individuals we want to request.
	 * @param serverUrl The url of the server.
	 * @return The ris fo the taxonomy individuals that are assigned to the information package.
	 */
	public IriListResponse findAllAssignedTaxonomyIndividuals(final String serverUrl,
			final UuidResponse uuidOfInformationPackageResponse) {
		return serverCommunicationOperations.findAllAssignedTaxonomyIndividuals(serverUrl, uuidOfInformationPackageResponse);
	}
	
	/*public void assignOrUnassignIndividualToInformationPackage(UuidResponse uuidOfIndividualResponse,
			UuidResponse uuidOfInformationPackageResponse) {
		serverCommunicationOperations.
	}*/
}
