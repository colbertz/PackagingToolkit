package de.feu.kdmp4.packagingtoolkit.client.operations.interfaces;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import de.feu.kdmp4.packagingtoolkit.client.model.interfaces.ClientConfigurationData;
import de.feu.kdmp4.packagingtoolkit.exceptions.ConfigurationException;

/**
 * Contains the atomar methods for configuring the client.
 * @author Christopher Olbertz
 *
 */
public interface ClientConfigurationOperations {
	/**
	 * Reads the configuration data for the client from the configuration file.
	 * @return The configuration data of the client.
	 * @throws ConfigurationException if there was a error while reading
	 * the configuration file.
	 */
	ClientConfigurationData getClientConfigurationData();
	/**
	 * Persists configuration data.
	 * @param clientConfigurationData The configuration data to save.
	 * @throws FileNotFoundException Is thrown, when there is a problem with the
	 * configuration file. 
	 * @throws IOException 
	 */
	void writeClientConfigurationData(final ClientConfigurationData clientConfigurationData) throws IOException;
	/**
	 * Creates a file object that represents the temporary directory on the server.
	 * @return The temporary directory on the server.
	 */
	File getTemporaryDirectoryOnClient();
	/**
	 * Determines the path and the name of the file with the logo from the configuration file. If the configuration
	 * file does not exist, a default value is returned. The file is saved in the images directory of the client. The logo is a png image.
	 * @param configurationDirectory The path to the directory that contains the configuration file. 
	 * @return The file with the logo with a png prefix. 
	 */
	File getLogoPath(String configurationDirectory);
	/**
	 * Saves a new file for the logo.
	 * @param temporaryLogoFile The file with the logo in the temporary directory.
	 * @param configurationDirectory The path to the directory that contains the configuration file.
	 */
	void saveLogo(File temporaryLogoFile, String configurationDirectory);
	/**
	 * Determines the time after that the individuals have to be updated from the server. 
	 * @param configurationDirectory The path to the directory that contains the configuration file.
	 * @return The time after that the individuals have to be updated from the server in milliseconds.
	 */
	long getIndividualsUpdateTimePath(String configurationDirectory);
}
