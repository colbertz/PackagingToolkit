package de.feu.kdmp4.packagingtoolkit.factories;

import static org.junit.Assert.*; 
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorResponseFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;

public class MediatorResponseFactoryTest {
	// ****************************************
	// testCreateDataSourceToolResponse_deactivatedDataTool()
	// ****************************************
	/**
	 * Tests the creation of data source tool response objects with a deactivated data source tool.
	 */
	@Test
	public void testCreateDataSourceToolResponse_deactivatedDataTool() { 
		DataSourceTool expectedDataSourceTool = MediatorModelFactory.createDataSourceTool("abc", false);
		DataSourceToolResponse actualDataSourceTool = MediatorResponseFactory.createDataSourceToolResponse(
				expectedDataSourceTool);
		
		assertThat(actualDataSourceTool.getDataSourceToolName(), is(equalTo(expectedDataSourceTool.getToolName())));
		assertFalse(actualDataSourceTool.isActivated());
	}

	// ****************************************
	// testCreateDataSourceToolResponse_activatedDataTool()
	// ****************************************
	/**
	 * Tests the creation of data source tool response objects with an activated data source tool.
	 */
	@Test
	public void testCreateDataSourceToolResponse_activatedDataTool() {
		DataSourceTool expectedDataSourceTool = MediatorModelFactory.createDataSourceTool("abc", true);
		DataSourceToolResponse actualDataSourceTool = MediatorResponseFactory.createDataSourceToolResponse(
				expectedDataSourceTool);
		
		assertThat(actualDataSourceTool.getDataSourceToolName(), is(equalTo(expectedDataSourceTool.getToolName())));
		assertTrue(actualDataSourceTool.isActivated());
	}
	
	// ****************************************
	// testCreateDataSourceToolListResponse()
	// ****************************************
	/**
	 * Tests the creation of a data source tool list response object.
	 */
	@Test
	public void testCreateDataSourceListToolResponse() {
		DataSourceToolCollection dataSourceToolCollection = prepareTest_testCreateDataSourceToolListResponse_DataSourceToolCollection();
		DataSourceToolListResponse dataSourceToolListResponse = MediatorResponseFactory.createDataSourceToolListResponse(
				dataSourceToolCollection);
		checkResults_testCreateDataSourceToolListResponse(dataSourceToolListResponse, dataSourceToolCollection);
	}
	
	/**
	 * Creates six tools for the test.
	 * @return
	 */
	private DataSourceToolCollection prepareTest_testCreateDataSourceToolListResponse_DataSourceToolCollection() {
		DataSourceToolCollection dataSourceToolCollection = MediatorModelFactory.createEmptyDataSourceToolCollection();
		
		for (int i = 0; i < 6; i++) {
			DataSourceTool dataSourceTool = null;
			
			if (i % 2 == 0) {
				dataSourceTool = MediatorModelFactory.createDataSourceTool("tool" + i, true);
			} else {
				dataSourceTool = MediatorModelFactory.createDataSourceTool("tool" + i, false);
			}
			
			dataSourceToolCollection.addDataSourceToolToList(dataSourceTool);
		}
		
		return dataSourceToolCollection;
	}
	
	private void checkResults_testCreateDataSourceToolListResponse(DataSourceToolListResponse dataSourceToolListResponse,
			DataSourceToolCollection dataSourceToolCollection) {
		int actualSize = dataSourceToolListResponse.getDataSourceToolsCount();
		int expectedSize = dataSourceToolCollection.getDataSourceToolCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		
		for(int i = 0; i < actualSize; i++) {
			DataSourceTool dataSourceTool = dataSourceToolCollection.getDataSourceToolAt(i);
			DataSourceToolResponse dataSourceToolResponse = dataSourceToolListResponse.getDataSourceTool(i);
			String expectedToolName = dataSourceTool.getToolName();
			String actualToolName = dataSourceToolResponse.getDataSourceToolName();
			assertThat(actualToolName, is(equalTo(expectedToolName)));
		}
	}
	
	// ****************************************
	// testCreateDataSourceResponse()
	// ****************************************
	/**
	 * Tests the creation of a data source list response object.
	 */
	@Test
	public void testCreateDataSourceResponse() {
		DataSourceToolCollection dataSourceTools = prepareTest_testCreateDataSourceResponse_DataSourceToolCollection();
		DataSource dataSource = MediatorModelFactory.createDataSource("xyz", dataSourceTools);
		DataSourceResponse dataSourceResponse = MediatorResponseFactory.createDataSourceResponse(dataSource);
		checkResults_testCreateDataSourceResponse(dataSource, dataSourceResponse);
	}
	
	/**
	 * Creates four tools for the test.
	 * @return
	 */
	private DataSourceToolCollection prepareTest_testCreateDataSourceResponse_DataSourceToolCollection() {
		DataSourceToolCollection dataSourceToolCollection = MediatorModelFactory.createEmptyDataSourceToolCollection();
		
		for (int i = 0; i < 4; i++) {
			DataSourceTool dataSourceTool = null;
			
			if (i % 2 == 0) {
				dataSourceTool = MediatorModelFactory.createDataSourceTool("tool" + i, true);
			} else {
				dataSourceTool = MediatorModelFactory.createDataSourceTool("tool" + i, false);
			}
			
			dataSourceToolCollection.addDataSourceToolToList(dataSourceTool);
		}
		
		return dataSourceToolCollection;
	}
	
	/**
	 * Checks if the actual data source and the expected data source are equal.
	 * @param expectedDataSource
	 * @param actualDataSource
	 */
	private void checkResults_testCreateDataSourceResponse(DataSource expectedDataSource, DataSourceResponse actualDataSource) {
		String expectedDataSourceName = expectedDataSource.getDataSourceName();
		String actualDataSourceName = actualDataSource.getDataSourceName();
		assertThat(actualDataSourceName, is(equalTo(expectedDataSourceName)));
		
		DataSourceToolCollection expectedDataSourceTools = expectedDataSource.getDataSourceTools();
		DataSourceToolListResponse actualDataSourceTools = actualDataSource.getDataSourceTools();
		
		int expectedDataSourceToolsCount = expectedDataSourceTools.getDataSourceToolCount();
		int actualDataSourceToolsCount = actualDataSourceTools.getDataSourceToolsCount();
		assertThat(actualDataSourceToolsCount, is(equalTo(expectedDataSourceToolsCount)));
		
		for (int i = 0; i < expectedDataSourceToolsCount; i++) {
			DataSourceTool dataSourceTool = expectedDataSourceTools.getDataSourceToolAt(i);
			DataSourceToolResponse dataSourceToolResponse = actualDataSourceTools.getDataSourceTool(i);
			String expectedToolName = dataSourceTool.getToolName();
			String actualToolName = dataSourceToolResponse.getDataSourceToolName();
			assertThat(actualToolName, is(equalTo(expectedToolName)));
		}
	}
	
	// ****************************************
	// testCreateDataSourceListResponse()
	// ****************************************
	@Test
	public void testCreateDataSourceListResponse() {
		DataSourceCollection dataSources = prepareTest_testCreateDataSourceListResponse_dataSourceCollection();
		DataSourceListResponse actualDataSources = MediatorResponseFactory.createDataSourceListResponse(dataSources);
		checkResults_testCreateDataSourceListResponse(dataSources, actualDataSources);
		
	}
	
	private DataSourceCollection prepareTest_testCreateDataSourceListResponse_dataSourceCollection() {
		DataSource dataSource1 = MediatorModelFactory.createDataSource("abc", MediatorModelFactory.createEmptyDataSourceToolCollection());
		DataSource dataSource2 = MediatorModelFactory.createDataSource("xyz", MediatorModelFactory.createEmptyDataSourceToolCollection());
		DataSourceCollection dataSources = MediatorModelFactory.createEmptyDataSourceCollection();
		dataSources.addDataSource(dataSource1);
		dataSources.addDataSource(dataSource2);
		
		return dataSources;
	}
	
	private void checkResults_testCreateDataSourceListResponse(DataSourceCollection expectedDataSources, 
			DataSourceListResponse actualDataSources) {
		int actualDataSourcesCount = actualDataSources.getDataSources().size();
		int expectedDataSourcesCount = expectedDataSources.getDataSourceCount();
		assertThat(actualDataSourcesCount, is(equalTo(expectedDataSourcesCount)));
		
		for (int i = 0; i < actualDataSourcesCount; i++) {
			DataSource expectedDataSource = expectedDataSources.getDataSourceAt(i);
			DataSourceResponse actualDataSource = actualDataSources.getDataSources().get(i);
			String actualDataSourceName = actualDataSource.getDataSourceName();
			String expectedDataSourceName = expectedDataSource.getDataSourceName();
			assertThat(actualDataSourceName, is(equalTo(expectedDataSourceName)));
		}
	}
}
