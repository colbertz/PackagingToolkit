package de.feu.kdmp4.packagingtoolkit.mediator.enums;


import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class DataSourcesTest {
	@Test
	public void testIsFitsDataSources_resultTrue() {
		DataSources fitsDataSource = DataSources.FITS;
		boolean isFits = fitsDataSource.isFitsDataSource();
		assertTrue(isFits);
	}
	
	@Test
	public void testIsFitsDataSources_resultFalse() {
		DataSources fitsDataSource = DataSources.TEST;
		boolean isFits = fitsDataSource.isFitsDataSource();
		assertFalse(isFits);
	}
}
