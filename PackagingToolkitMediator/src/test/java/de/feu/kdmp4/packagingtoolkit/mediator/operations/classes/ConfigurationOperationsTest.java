package de.feu.kdmp4.packagingtoolkit.mediator.operations.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.enums.DataSources;
import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.mediator.test.api.ConfigurationTestApi;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

/**
 * Tests the class with the operations for the configuration with the help of a properties file.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationOperationsTest {
	@Rule
	public TemporaryFolder configurationFileRule = new TemporaryFolder();
	private File configurationFile;
	private ConfigurationOperations configurationOperations;
	

	private static final String FITS_ACTIVATED_TOOLS_PARAM = "fits.tools.activated";
	private static final String FITS_DEACTIVATED_TOOLS_PARAM = "fits.tools.deactivated";
	
	@Before
	public void setUp() throws IOException {
		configurationFile = configurationFileRule.newFile("config.properties");
		configurationOperations = new ConfigurationOperationsPropertiesImpl(configurationFile.getAbsolutePath());
	}

	/**
	 * Tests if the  url for the data source can be read from the configuration file.
	 * @throws IOException
	 */
	@Test
	public void testGetDataSourceUrl() throws IOException {
		ConfigurationTestApi.prepareConfigurationFileWithCorrectFitsDirectory(configurationFile);
		DataSources fitsDataSource = DataSources.FITS;
		String dataSourceUrl = configurationOperations.getDataSourceUrl(fitsDataSource);
		boolean isFitsInstallationDirectory = ConfigurationTestApi.isFitsInstallationPath(dataSourceUrl);
		assertTrue(isFitsInstallationDirectory);
	}
	
	/**
	 * Tests if the method checkDataSourceInstallations() recognizes that the installation directory of
	 * FITS is correct. This is the case if no exception is thrown.
	 * @throws IOException
	 */
	@Test
	public void testCheckFitsInstallation_resultOkay() throws IOException {
		ConfigurationTestApi.prepareConfigurationFileWithCorrectFitsDirectory(configurationFile);
		DataSources fitsDataSource = DataSources.FITS;
		String dataSourceUrl = configurationOperations.getDataSourceUrl(fitsDataSource);
		
		configurationOperations.checkDataSourceInstallations(dataSourceUrl, fitsDataSource);
	}
	
	/**
	 * Tests if the method checkDataSourceInstallations() recognizes that the installation directory of
	 * FITS is not correct. 
	 * @throws IOException
	 */
	@Test(expected = MediatorConfigurationException.class)
	public void testCheckFitsInstallation_resultException() throws IOException {
		ConfigurationTestApi.prepareConfigurationFileWithIncorrectFitsDirectory(configurationFile);
		DataSources fitsDataSource = DataSources.FITS;
		String dataSourceUrl = configurationOperations.getDataSourceUrl(fitsDataSource);
		
		configurationOperations.checkDataSourceInstallations(dataSourceUrl, fitsDataSource);
	}
	
	// *******************************************************
	// testGetToolsOfDataSource()
	// *******************************************************
	@Test
	public void testGetToolsOfDataSource() throws IOException {
		StringList expectedActivatedTools = prepareTest_testGetDataSourceUrl_activatedTools();
		StringList expectedDeactivatedTools = prepareTest_testGetDataSourceUrl_deactivatedTools();
		prepareTest_testConfigureToolsOfDataSource_configurationFile(expectedActivatedTools, expectedDeactivatedTools);
		
		DataSourceToolCollection actualDataSourceTools = configurationOperations.getToolsOfDataSource(DataSources.FITS);
		
		int expectedSize = expectedActivatedTools.getSize() + expectedDeactivatedTools.getSize();
		assertThat(actualDataSourceTools.getDataSourceToolCount(), is(equalTo(expectedSize)));
		
		for (int i = 0; i < expectedActivatedTools.getSize(); i++) {
			String expectedToolName = expectedActivatedTools.getStringAt(i);
			DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(expectedToolName);
			
			boolean toolContained = actualDataSourceTools.containsDataSourceTool(dataSourceTool);
			assertTrue(toolContained);
		}
		
		for (int i = 0; i < expectedDeactivatedTools.getSize(); i++) {
			String expectedToolName = expectedDeactivatedTools.getStringAt(i);
			DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(expectedToolName, false);
			
			boolean toolContained = actualDataSourceTools.containsDataSourceTool(dataSourceTool);
			assertTrue(toolContained);
		}
	}
	
	private StringList prepareTest_testGetDataSourceUrl_activatedTools() {
		StringList activatedTools = PackagingToolkitModelFactory.getStringList();
		activatedTools.addStringToList("tool1");
		activatedTools.addStringToList("tool2");
		activatedTools.addStringToList("tool3");
		activatedTools.addStringToList("tool4");
		
		return activatedTools;
	}
	
	private StringList prepareTest_testGetDataSourceUrl_deactivatedTools() {
		StringList activatedTools = PackagingToolkitModelFactory.getStringList();
		activatedTools.addStringToList("tool5");
		activatedTools.addStringToList("tool6");
		activatedTools.addStringToList("tool7");
		activatedTools.addStringToList("tool8");
		
		return activatedTools;
	}
	
	private void prepareTest_testConfigureToolsOfDataSource_configurationFile(StringList activatedTools, StringList deactivatedTools) throws IOException {
		try (FileWriter fileWriter = new FileWriter(configurationFile)) {
			/*for (int i = 0; i < activatedTools.getSize(); i++) {
				String toolName = activatedTools.getStringAt(i);
				listWithActivatedTools.addStringToList(toolName);				
			}
			
			for (int i = 0; i < deactivatedTools.getSize(); i++) {
				String toolName = deactivatedTools.getStringAt(i);
				listWithDeactivatedTools.addStringToList(toolName);								
			}*/
			
			String activatedToolsValue = StringUtils.createCommaSeperatedList(activatedTools);
			String deactivatedToolsValue = StringUtils.createCommaSeperatedList(deactivatedTools);
			fileWriter.write(FITS_ACTIVATED_TOOLS_PARAM + "=" + activatedToolsValue + "\n");
			fileWriter.write(FITS_DEACTIVATED_TOOLS_PARAM + "=" + deactivatedToolsValue);
		}	
	}
	
	// *******************************************************
	// testConfigureToolsOfDataSource
	// *******************************************************
	/**
	 * Tests the configuration of the tools of a data source in the properties file.
	 */
	@Test
	public void testConfigureToolsOfDataSource() {
		DataSourceToolCollection dataSourceToolCollection = prepareTest_testConfigureToolsOfDataSource_dataSourceTools();
		configurationOperations.configureToolsOfDataSource(DataSources.FITS, dataSourceToolCollection);
		checkResults_testConfigureToolsOfDataSource(dataSourceToolCollection);
	}
	
	/**
	 * Creates ten tools for the test.
	 * @return
	 */
	private DataSourceToolCollection prepareTest_testConfigureToolsOfDataSource_dataSourceTools() {
		DataSourceToolCollection dataSourceToolCollection = MediatorModelFactory.createEmptyDataSourceToolCollection();
		
		for (int i = 0; i < 10; i++) {
			DataSourceTool dataSourceTool = null;
			
			if (i % 2 == 0) {
				dataSourceTool = MediatorModelFactory.createDataSourceTool("tool" + i, true);
			} else {
				dataSourceTool = MediatorModelFactory.createDataSourceTool("tool" + i, false);
			}
			
			dataSourceToolCollection.addDataSourceToolToList(dataSourceTool);
		}
		
		return dataSourceToolCollection;
	}
	
	/**
	 * Checks if the tools that were created have been entered in the properties file.
	 * @param expectedDataSourceTools
	 */
	private void checkResults_testConfigureToolsOfDataSource(DataSourceToolCollection expectedDataSourceTools) {
		DataSourceToolCollection actualDataSourceToolCollection = configurationOperations.getToolsOfDataSource(DataSources.FITS);
		int expectedSize = expectedDataSourceTools.getDataSourceToolCount();
		int actualSize = actualDataSourceToolCollection.getDataSourceToolCount();
		
		assertThat(actualSize, is(equalTo(expectedSize)));
		
		for (int i = 0; i < expectedDataSourceTools.getDataSourceToolCount(); i++) {
			DataSourceTool expectedDataSourceTool = expectedDataSourceTools.getDataSourceToolAt(i);
			boolean containsDataSourceTool = actualDataSourceToolCollection.containsDataSourceTool(expectedDataSourceTool);
			assertTrue(containsDataSourceTool);
		}
	}
}
