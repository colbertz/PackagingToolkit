package de.feu.kdmp4.packagingtoolkit.mediator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.feu.kdmp4.packagingtoolkit.factories.MediatorResponseFactoryTest;
import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.MediatorDataCollectionTest;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.classes.ConfigurationOperationsTest;
import de.feu.kdmp4.packagingtoolkit.mediator.service.classes.MediatorServiceTest;
import de.feu.kdmp4.packagingtoolkit.mediator.wrapper.classes.FitsWrapperTest;

@RunWith(Suite.class)
@SuiteClasses({ConfigurationOperationsTest.class,
						    FitsWrapperTest.class,
						    MediatorDataCollectionTest.class,
						    MediatorResponseFactoryTest.class,
					        MediatorServiceTest.class,
					        })
public class AllMediatorTests {

}
