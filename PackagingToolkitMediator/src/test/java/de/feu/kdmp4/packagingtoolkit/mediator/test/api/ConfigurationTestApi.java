package de.feu.kdmp4.packagingtoolkit.mediator.test.api;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;


/**
 * Contains some constants and convenient methods for testing the configuration of the mediator.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationTestApi {
	private static final String FITS_INSTALLATION_URL = "/opt/fits-1.2.0";
	private static final String FITS_URL_PARAM = "fits.url";
	
	/**
	 * Writes the correct installation path of FITS in the configuration file.
	 * @param configurationFile The configuration file the installation path of FITS is written in.
	 * @throws IOException
	 */
	public static void prepareConfigurationFileWithCorrectFitsDirectory(File configurationFile) throws IOException {
		try (FileWriter fileWriter = new FileWriter(configurationFile)) {
			fileWriter.write(FITS_URL_PARAM + "=" + FITS_INSTALLATION_URL);
		} 
	}
	
	/**
	 * Checks if a given path contains the correct value of the installation path of FITS.
	 * @param path The path that should be checked.
	 * @return True if path contains a valid installation directory for FITS.
	 */
	public static boolean isFitsInstallationPath(String path) {
		if (areStringsEqual(path, FITS_INSTALLATION_URL)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Writes an incorrect installation path of FITS in the configuration file.
	 * @param configurationFile The configuration file the installation path of FITS is written in.
	 * @throws IOException
	 */
	public static void prepareConfigurationFileWithIncorrectFitsDirectory(File configurationFile) throws IOException {
		try (FileWriter fileWriter = new FileWriter(configurationFile)) {
			fileWriter.write(FITS_URL_PARAM + "=" + FITS_INSTALLATION_URL + "wrong");
		} 
	}
}
