package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import static org.junit.Assert.*;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;

/**
 * Tests the methods of MediatorDataCollection.
 * @author Christopher Olbertz
 *
 */
public class MediatorDataCollectionTest {

	// ******************************************
	// testGetMediatorData()
	// ******************************************
	/**
	 * Tests the method getMediatorData(). 
	 */
	@Test
	public void testGetMediatorData() {
		MediatorDataCollection mediatorDataCollection = prepareTest_testGetMediatorData_mediatorDataCollection();
		String key = "def";
		
		MediatorData mediatorData = mediatorDataCollection.getMediatorData(key);
		assertThat(mediatorData.getName(), is(equalTo("def")));
		assertThat(mediatorData.getValue(), is(equalTo("value2")));
	}
	
	/**
	 * Creates a collection for the test.
	 * @return The collection.
	 */
	private MediatorDataCollection prepareTest_testGetMediatorData_mediatorDataCollection() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		MediatorData mediatorData1 = MediatorModelFactory.createMediatorData("abc", "value1");
		MediatorData mediatorData2 = MediatorModelFactory.createMediatorData("def", "value2");
		MediatorData mediatorData3 = MediatorModelFactory.createMediatorData("xyz", "value3");
		
		mediatorDataCollection.addMediatorData(mediatorData1);
		mediatorDataCollection.addMediatorData(mediatorData2);
		mediatorDataCollection.addMediatorData(mediatorData3);
		
		return mediatorDataCollection;
	}

	// ******************************************
	// testGetMediatorData_WithMediatorData()
	// ******************************************
	/**
	 * Tests if a mediator data can be added to the collection.
	 */
	@Test
	public void testAddMediatorData_WithMediatorData() {
		MediatorDataCollection mediatorDataCollection = prepareTest_testAddMediatorData_WithMediatorData_mediatorDataCollection();
		MediatorData mediatorData = MediatorModelFactory.createMediatorData("xyz", "value3");
		mediatorDataCollection.addMediatorData(mediatorData);
		
		assertThat(mediatorDataCollection.getMetadataCount(), is(equalTo(3)));
		MediatorData actualMediatorData = mediatorDataCollection.getMediatorData("xyz");
		assertThat(actualMediatorData.getName(), is(equalTo("xyz")));
		assertThat(actualMediatorData.getValue(), is(equalTo("value3")));
	}
	
	/**
	 * Creates a collection for the test.
	 * @return The collection.
	 */
	private MediatorDataCollection prepareTest_testAddMediatorData_WithMediatorData_mediatorDataCollection() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		MediatorData mediatorData1 = MediatorModelFactory.createMediatorData("abc", "value1");
		MediatorData mediatorData2 = MediatorModelFactory.createMediatorData("def", "value2");
		
		mediatorDataCollection.addMediatorData(mediatorData1);
		mediatorDataCollection.addMediatorData(mediatorData2);
		
		return mediatorDataCollection;
	}
	
	// ******************************************
	// testGetMediatorData_WithMediatorDataCollection()
	// ******************************************
	/**
	 * Tests if a mediator data collection can be added to the collection.
	 */
	@Test
	public void testAddMediatorDataCollection_WithMediatorData() {
		MediatorDataCollection mediatorDataCollection1 = prepareTest_testAddMediatorDataCollection_WithMediatorData_mediatorDataCollection();
		MediatorDataCollection mediatorDataCollection2 = prepareTest_testAddMediatorDataCollection_WithMediatorData_addedMediatorDataCollection();
		 
		mediatorDataCollection1.addMediatorData(mediatorDataCollection2);
		checkResults_testAddMediatorDataCollection_WithMediatorData(mediatorDataCollection1);
	}
	
	/**
	 * Creates a collection for the test. Another collection will be added to this collection.
	 * @return The collection.
	 */
	private MediatorDataCollection prepareTest_testAddMediatorDataCollection_WithMediatorData_mediatorDataCollection() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		MediatorData mediatorData1 = MediatorModelFactory.createMediatorData("abc", "value1");
		MediatorData mediatorData2 = MediatorModelFactory.createMediatorData("def", "value2");
		
		mediatorDataCollection.addMediatorData(mediatorData1);
		mediatorDataCollection.addMediatorData(mediatorData2);
		
		return mediatorDataCollection;
	}
	
	/**
	 * Creates a collection for the test. This collection will be added to the other collection.
	 * @return The collection.
	 */
	private MediatorDataCollection prepareTest_testAddMediatorDataCollection_WithMediatorData_addedMediatorDataCollection() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		MediatorData mediatorData1 = MediatorModelFactory.createMediatorData("xxx", "value4");
		MediatorData mediatorData2 = MediatorModelFactory.createMediatorData("yyy", "value3");
		
		mediatorDataCollection.addMediatorData(mediatorData1);
		mediatorDataCollection.addMediatorData(mediatorData2);
		
		return mediatorDataCollection;
	}
	
	/**
	 * Checks if the test was successful. The number of contained elements must be four and the elements in the other list must be
	 * contained in the collection under test.
	 * @param mediatorDataCollection The collection under test.
	 */
	private void checkResults_testAddMediatorDataCollection_WithMediatorData(MediatorDataCollection mediatorDataCollection) {
		assertThat(mediatorDataCollection.getMetadataCount(), is(equalTo(4)));
		MediatorData actualMediatorData1 = mediatorDataCollection.getMediatorData("xxx");
		assertThat(actualMediatorData1.getName(), is(equalTo("xxx")));
		assertThat(actualMediatorData1.getValue(), is(equalTo("value4")));
		
		MediatorData actualMediatorData2 = mediatorDataCollection.getMediatorData("yyy");
		assertThat(actualMediatorData2.getName(), is(equalTo("yyy")));
		assertThat(actualMediatorData2.getValue(), is(equalTo("value3")));
	}
	
	// ******************************************
	// testGetMediatorDataCount()
	// ******************************************
	/**
	 * Tests if the data in the collection can be count.
	 */
	@Test
	public void testGetMediatorDataCount() {
		MediatorDataCollection mediatorDataCollection = prepareTest_testGetMetadataCount_mediatorDataCollection();
		int actualSize = mediatorDataCollection.getMetadataCount();
		assertThat(actualSize, is(equalTo(3)));
	}
	
	/**
	 * Creates a collection for the test.
	 * @return The collection.
	 */
	private MediatorDataCollection prepareTest_testGetMetadataCount_mediatorDataCollection() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		MediatorData mediatorData1 = MediatorModelFactory.createMediatorData("aaa", "value1");
		MediatorData mediatorData2 = MediatorModelFactory.createMediatorData("bbb", "value2");
		MediatorData mediatorData3 = MediatorModelFactory.createMediatorData("ccc", "value3");
		
		mediatorDataCollection.addMediatorData(mediatorData1);
		mediatorDataCollection.addMediatorData(mediatorData2);
		mediatorDataCollection.addMediatorData(mediatorData3);
		
		return mediatorDataCollection;
	}
	
	// ******************************************
	// testIsEmpty_notEmpty()
	// ******************************************
	/**
	 * Tests if the collection is not empty.
	 */
	@Test
	public void testIsEmpty_notEmpty() {
		MediatorDataCollection mediatorDataCollection = prepareTest_testIsEmpty_notEmpty_mediatorDataCollection();
		boolean empty = mediatorDataCollection.isEmpty();
		assertFalse(empty);
	}
	
	/**
	 * Creates a collection for the test.
	 * @return The collection.
	 */
	private MediatorDataCollection prepareTest_testIsEmpty_notEmpty_mediatorDataCollection() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		MediatorData mediatorData1 = MediatorModelFactory.createMediatorData("aaa", "value1");
		MediatorData mediatorData2 = MediatorModelFactory.createMediatorData("bbb", "value2");
		MediatorData mediatorData3 = MediatorModelFactory.createMediatorData("ccc", "value3");
		
		mediatorDataCollection.addMediatorData(mediatorData1);
		mediatorDataCollection.addMediatorData(mediatorData2);
		mediatorDataCollection.addMediatorData(mediatorData3);
		
		return mediatorDataCollection;
	}
	
	// ******************************************
	// testIsEmpty_empty()
	// ******************************************
	/**
	 * Tests if the collection is empty.
	 */
	@Test
	public void testIsEmpty_empty() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		boolean empty = mediatorDataCollection.isEmpty();
		assertTrue(empty);
	}
	
	// ******************************************
	// testIsEmpty_toList()
	// ******************************************
	/**
	 * Tests if a collection can be converted into a list.
	 */
	@Test
	public void testIsEmpty_toList() {
		MediatorDataCollection mediatorDataCollection = prepareTest_testToList_mediatorDataCollection();
		List<MediatorData> mediatorDataList = mediatorDataCollection.toList();
		
		checkResults_testToList(mediatorDataCollection, mediatorDataList);
	}
	
	/**
	 * Creates a collection for the test. This collection is be converted into a list.
	 * @return The collection.
	 */
	private MediatorDataCollection prepareTest_testToList_mediatorDataCollection() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		MediatorData mediatorData1 = MediatorModelFactory.createMediatorData("aaa", "value1");
		MediatorData mediatorData2 = MediatorModelFactory.createMediatorData("bbb", "value2");
		MediatorData mediatorData3 = MediatorModelFactory.createMediatorData("ccc", "value3");
		
		mediatorDataCollection.addMediatorData(mediatorData1);
		mediatorDataCollection.addMediatorData(mediatorData2);
		mediatorDataCollection.addMediatorData(mediatorData3);
		
		return mediatorDataCollection;
	}
	
	/**
	 * Tests the results of the method toList(). We check if both collections have the same size. Then the elements in both
	 * collections are compared. 
	 * @param mediatorDataCollection The data collection that has been converted into a list.
	 * @param mediatorDataList The resulting list.
	 */
	private void checkResults_testToList(MediatorDataCollection mediatorDataCollection , List<MediatorData> mediatorDataList) {
		int sizeOfList = mediatorDataList.size();
		int sizeOfCollection = mediatorDataCollection.getMetadataCount();
		assertThat(sizeOfList, is(equalTo(sizeOfCollection)));
		
		MediatorData expectedMediatorData1 = mediatorDataCollection.getMediatorData("aaa");
		boolean containsMediatorData1 = mediatorDataList.contains(expectedMediatorData1);
		assertTrue(containsMediatorData1);
		
		MediatorData expectedMediatorData2 = mediatorDataCollection.getMediatorData("bbb");
		boolean containsMediatorData2 = mediatorDataList.contains(expectedMediatorData2);
		assertTrue(containsMediatorData2);
		
		MediatorData expectedMediatorData3 = mediatorDataCollection.getMediatorData("ccc");
		boolean containsMediatorData3 = mediatorDataList.contains(expectedMediatorData3);
		assertTrue(containsMediatorData3);
	}
}