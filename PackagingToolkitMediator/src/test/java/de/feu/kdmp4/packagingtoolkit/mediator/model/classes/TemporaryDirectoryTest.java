package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.TemporaryDirectory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public class TemporaryDirectoryTest {
	private static final int TEMPORARY_FILES_COUNT = 3;
	
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder(); 
	
	/**
	 * Tests if three files are correctly added to the temporary directory. An array with the file contents is created. 
	 * Then every file is written in the temporary directory. After
	 * every writing process the following conditions are checked:
	 * <ul>
	 * 	<li>Does the file exist on the file system?</li>
	 * 	<li>Has the number of files in the temporary directory object been increased by one?</li>
	 * 	<li>Is the uuid of the file returned by the method not null?</li>
	 * 	<li>Is there a file in the data structure for the uuid returned by the method addFileToTemporaryDirectory()?</li>
	 *  </ul>
	 * @throws IOException
	 */
	@Test
	public void testAddFileToTemporaryDirectory() throws IOException {
		File newFolder = temporaryFolder.newFolder();
		TemporaryDirectory temporaryDirectory = new TemporaryDirectoryImpl(newFolder.getAbsolutePath());
		byte[] fileContent = "abcdefghijklmnopqrstuvwxyz".getBytes();
		File temporaryFolder = new File(newFolder, "temp");
		
		for (int i = 0; i < TEMPORARY_FILES_COUNT; i++) {
			Uuid uuidOfFile = temporaryDirectory.addFileToTemporaryDirectory(fileContent);
			File file = new File(temporaryFolder, uuidOfFile.toString());
			boolean fileExsists = file.exists();
			assertTrue(fileExsists);
			assertThat(temporaryDirectory.getFileCount(), is(equalTo(i+1)));
			assertNotNull(uuidOfFile);
			File fileInDataStructure = temporaryDirectory.getFile(uuidOfFile);
			assertNotNull(fileInDataStructure);
		}
	}
	
	/**
	 * Tests if files are correctly deleted from the temporary directory. First three files are created. Then
	 * the files will be removed. After every step, the following conditions are checked:
	 * <ul>
	 * 	<li>Has the number of files in the temporary directory object been decreased by one?</li>
	 * 	<li>Does the file in the file system not exist anymore?</li>
	 * <li>Is there not a file in the data structure for the uuid?</li>
	 * </ul>
	 * @throws IOException
	 */
	@Test
	public void testDeleteTemporaryFile() throws IOException {
		// Prepare the test.
		File newFolder = temporaryFolder.newFolder();
		TemporaryDirectory temporaryDirectory = new TemporaryDirectoryImpl(newFolder.getAbsolutePath());
		byte[] fileContent = "abcdefghijklmnopqrstuvwxyz".getBytes();
		File temporaryFolder = new File(newFolder, "temp");
		Uuid[] uuidsOfFiles = new Uuid[TEMPORARY_FILES_COUNT];
		
		for (int i = 0; i < TEMPORARY_FILES_COUNT; i++) {
			Uuid uuidOfFile = temporaryDirectory.addFileToTemporaryDirectory(fileContent);
			uuidsOfFiles[i] = uuidOfFile;
		}

		// Do and check
		for (int i = 0; i < uuidsOfFiles.length; i++) {			
			Uuid uuidOfFile = uuidsOfFiles[i];
			//String filename = filenames[i];
			temporaryDirectory.deleteTemporaryFile(uuidOfFile);
			
			int expectedSize = uuidsOfFiles.length - i - 1;
			assertThat(temporaryDirectory.getFileCount(), is(equalTo(expectedSize)));
			File file = new File(temporaryFolder, uuidOfFile.toString());
			boolean fileExists = file.exists();
			assertFalse(fileExists);
			File fileInDataStructure = temporaryDirectory.getFile(uuidOfFile);
			assertNull(fileInDataStructure);
		}
	}
}
