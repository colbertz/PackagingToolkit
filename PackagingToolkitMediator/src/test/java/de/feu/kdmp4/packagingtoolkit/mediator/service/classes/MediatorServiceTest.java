package de.feu.kdmp4.packagingtoolkit.mediator.service.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.enums.DataSources;
import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.TemporaryDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.classes.ConfigurationOperationsPropertiesImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.mediator.service.interfaces.MediatorService;
import de.feu.kdmp4.packagingtoolkit.mediator.wrapper.classes.FitsWrapperTest;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;

/**
 * Tests the mediator service class.
 * @author Christopher Olbertz
 *
 */
public class MediatorServiceTest {
	/**
	 * A file for testing the extraction of meta data.
	 */
	private static final String TEST_FILE_FATIMA = "/FAtiMA.pdf";
	/**
	 * The path where FITS is installed on the development machine. If the test does not work, please
	 * change this path.
	 */
	private static final String FITS_PATH = "/opt/fits-1.0.3";
	
	private ConfigurationOperations configurationOperations;
	
	private MediatorService mediatorService;
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	/**
	 * Prepares the mediator service, mocks the configuration operations and takes care that the
	 * correct installation path for FITS is returned by the configuration method.
	 * @throws IOException 
	 */
	private void prepareMediatorService() throws IOException  {
		mediatorService = new MediatorServiceImpl();
		File temporaryFolderFile = temporaryFolder.newFolder();
		mediatorService.setTemporaryDirectory(new TemporaryDirectoryImpl(temporaryFolderFile.getAbsolutePath()));
		configurationOperations = mock(ConfigurationOperationsPropertiesImpl.class);
		when(configurationOperations.getDataSourceUrl(DataSources.FITS)).thenReturn(FITS_PATH);
		mediatorService.setConfigurationOperations(configurationOperations);
		mediatorService.registerDataSources();
	}
	
	/**
	 * Tests the finding of all data sources registered on the mediator with all their information. At the moment, 
	 * this method is tested with only FITS as data source and its 13 tools.
	 */
	@Test
	public void testFindAllDataSources() throws IOException {
		prepareMediatorService();
		DataSourceListResponse expectedDataSources = prepareTest_testFindAllDataSources_dataSources();
		
		DataSourceListResponse actualDataSources = mediatorService.findAllDataSources();
		
		assertThat(actualDataSources, is(equalTo(expectedDataSources)));
	}
	
	/**
	 * Prepares the data source FITS for the test.
	 * @return The configured data source for the test.
	 */
	private DataSourceListResponse prepareTest_testFindAllDataSources_dataSources() {
		DataSourceListResponse dataSources = ResponseModelFactory.getDataSourceListResponse();
		DataSourceResponse dataSource = ResponseModelFactory.getDataSourceResponse();
		dataSource.setDataSourceName("File Information Tool Set");
		prepareTest_testFindAllDataSources_dataSourceTools(dataSource);
		dataSources.addDataSource(dataSource);
		
		return dataSources;
	}
	
	/**
	 * Gets a data source and prepares its tools. At the moment, only FITS is configured as data source on the
	 * mediator and so the test is only using this data source. 
	 * @param fitsDataSource The FITS data source that should contain 13 tools.
	 */
	private void prepareTest_testFindAllDataSources_dataSourceTools(DataSourceResponse fitsDataSource) {
		DataSourceToolListResponse dataSourceTools = ResponseModelFactory.getDataSourceToolListResponse();
		DataSourceToolResponse mediaInfo = ResponseModelFactory.getDataSourceToolResponse();
		mediaInfo.setActivated(true);
		mediaInfo.setDataSourceToolName("MediaInfo");
		
		DataSourceToolResponse audioInfo = ResponseModelFactory.getDataSourceToolResponse();
		audioInfo.setActivated(true);
		audioInfo.setDataSourceToolName("AudioInfo");
		
		DataSourceToolResponse adlTool = ResponseModelFactory.getDataSourceToolResponse();
		adlTool.setActivated(true);
		adlTool.setDataSourceToolName("ADLTool");
		
		DataSourceToolResponse vttTool = ResponseModelFactory.getDataSourceToolResponse();
		vttTool.setActivated(true);
		vttTool.setDataSourceToolName("VTTTool");

		DataSourceToolResponse droid = ResponseModelFactory.getDataSourceToolResponse();
		droid.setActivated(true);
		droid.setDataSourceToolName("Droid");
		
		DataSourceToolResponse jhove = ResponseModelFactory.getDataSourceToolResponse();
		jhove.setActivated(true);
		jhove.setDataSourceToolName("Jhove");
		
		DataSourceToolResponse fileUtility = ResponseModelFactory.getDataSourceToolResponse();
		fileUtility.setActivated(true);
		fileUtility.setDataSourceToolName("FileUtility");
		
		DataSourceToolResponse exiftool = ResponseModelFactory.getDataSourceToolResponse();
		exiftool.setActivated(true);
		exiftool.setDataSourceToolName("Exiftool");
		
		DataSourceToolResponse metadataExtractor = ResponseModelFactory.getDataSourceToolResponse();
		metadataExtractor.setActivated(true);
		metadataExtractor.setDataSourceToolName("MetadataExtractor");
		
		DataSourceToolResponse fileInfo = ResponseModelFactory.getDataSourceToolResponse();
		fileInfo.setActivated(true);
		fileInfo.setDataSourceToolName("FileInfo");
		
		DataSourceToolResponse xmlMetadata = ResponseModelFactory.getDataSourceToolResponse();
		xmlMetadata.setActivated(true);
		xmlMetadata.setDataSourceToolName("XmlMetadata");
		
		DataSourceToolResponse fFIdent = ResponseModelFactory.getDataSourceToolResponse();
		fFIdent.setActivated(true);
		fFIdent.setDataSourceToolName("FFIdent");
		
		DataSourceToolResponse tikaTool = ResponseModelFactory.getDataSourceToolResponse();
		tikaTool.setActivated(true);
		tikaTool.setDataSourceToolName("TikaTool");
		
		dataSourceTools.addDataSourceTool(mediaInfo);
		dataSourceTools.addDataSourceTool(audioInfo);
		dataSourceTools.addDataSourceTool(adlTool);
		dataSourceTools.addDataSourceTool(vttTool);
		dataSourceTools.addDataSourceTool(droid);
		dataSourceTools.addDataSourceTool(jhove);
		dataSourceTools.addDataSourceTool(fileUtility);
		dataSourceTools.addDataSourceTool(exiftool);
		dataSourceTools.addDataSourceTool(metadataExtractor);
		dataSourceTools.addDataSourceTool(fileInfo);
		dataSourceTools.addDataSourceTool(xmlMetadata);
		dataSourceTools.addDataSourceTool(fFIdent);
		dataSourceTools.addDataSourceTool(tikaTool);
		
		fitsDataSource.setDataSourceTools(dataSourceTools);
	}

	// *********************************************************
	// testGetDataFromDataSources_WithFile()
	// *********************************************************	
	/**
	 * Requests the meta data of a file from FITS and checks if the correct meta data has been extracted.
	 * @throws IOException 
	 */
	@Test
	public void testGetDataFromDataSources_WithFile() throws IOException {
		prepareMediatorService();
		
		URL urlOfFatima = FitsWrapperTest.class.getResource(TEST_FILE_FATIMA);
		File fatimaFile = new File(urlOfFatima.getFile());
		byte[] dataOfFile = null;
		
		try(InputStream inputStream = new FileInputStream(fatimaFile)) {
			//dataOfFile = new byte[inputStream.available()];
			//inputStream.read(dataOfFile);
			dataOfFile = IOUtils.toByteArray(inputStream);
		}
		
		//MediatorDataCollection mediatorDataCollection = mediatorService.getDataFromDataSources(dataOfFile);
		//checkResults_testGetDataFromDataSources_WithFile(mediatorDataCollection);
	}
	
	/**
	 * Checks the result of the test testGetDataFromDataSources_WithFile. It checks if the correct meta data are contained
	 * in mediatorDataCollection. 
	 * @param mediatorDataCollection Should contain the correct meta data.
	 */
	private void checkResults_testGetDataFromDataSources_WithFile(MediatorDataCollection mediatorDataCollection ) {
		configurationOperations = mock(ConfigurationOperationsPropertiesImpl.class);
		//when(configurationOperations.getDataSourceUrl("fits")).thenReturn(FITS_PATH);
		
		int actualSizeOfCollection = mediatorDataCollection.getMetadataCount();
		assertThat(actualSizeOfCollection, is(equalTo(10)));
		MediatorData author = MediatorModelFactory.createMediatorData("author", "TeX");
		MediatorData pageCount = MediatorModelFactory.createMediatorData("pageCount", "12");
		MediatorData hasOutline = MediatorModelFactory.createMediatorData("hasOutline", "no");
		MediatorData hasAnnotations = MediatorModelFactory.createMediatorData("hasAnnotations", "no");
		MediatorData graphicsCount = MediatorModelFactory.createMediatorData("graphicsCount", "2");
		MediatorData font = MediatorModelFactory.createMediatorData("font", "NimbusRomNo9L-ReguItal");
		MediatorData size = MediatorModelFactory.createMediatorData("size", "292715");
		MediatorData creatingApplicationName = MediatorModelFactory.createMediatorData("creatingApplicationName", "MiKTeX pdfTeX-1.40.10/TeX");
		MediatorData lastmodified = MediatorModelFactory.createMediatorData("lastmodified", "2011:08:02 03:35:52+01:00");
		MediatorData created = MediatorModelFactory.createMediatorData("created", "2011:08:02 03:35:52+01:00");
		
		MediatorData actualAuthor = mediatorDataCollection.getMediatorData("author");
		assertNotNull(actualAuthor);
		assertThat(actualAuthor, is(equalTo(author)));
		
		MediatorData actualPageCount = mediatorDataCollection.getMediatorData("pageCount");
		assertNotNull(actualPageCount);
		assertThat(actualPageCount, is(equalTo(pageCount)));
		
		MediatorData actualHasOutline = mediatorDataCollection.getMediatorData("hasOutline");
		assertNotNull(actualPageCount);
		assertThat(actualHasOutline, is(equalTo(hasOutline)));
		
		MediatorData actualHasAnnotations = mediatorDataCollection.getMediatorData("hasAnnotations");
		assertNotNull(actualHasAnnotations);
		assertThat(actualHasAnnotations, is(equalTo(hasAnnotations)));
		
		MediatorData actualGraphicsCount = mediatorDataCollection.getMediatorData("graphicsCount");
		assertNotNull(actualGraphicsCount);
		assertThat(actualGraphicsCount, is(equalTo(graphicsCount)));
		
		MediatorData actualFont = mediatorDataCollection.getMediatorData("font");
		assertNotNull(actualFont);
		assertThat(actualFont, is(equalTo(font)));
		
		MediatorData actualSize = mediatorDataCollection.getMediatorData("size");
		assertNotNull(actualSize);
		assertThat(actualSize, is(equalTo(size)));
		
		MediatorData actualCreatingApplicationName = mediatorDataCollection.getMediatorData("creatingApplicationName");
		assertNotNull(actualCreatingApplicationName);
		assertThat(actualCreatingApplicationName, is(equalTo(creatingApplicationName)));
		
		MediatorData actualLastmodified = mediatorDataCollection.getMediatorData("lastmodified");
		assertNotNull(actualLastmodified);
		assertThat(actualLastmodified, is(equalTo(lastmodified)));
		
		MediatorData actualCreated = mediatorDataCollection.getMediatorData("created");
		assertNotNull(actualCreated);
		assertThat(actualCreated, is(equalTo(created)));
	}
}
