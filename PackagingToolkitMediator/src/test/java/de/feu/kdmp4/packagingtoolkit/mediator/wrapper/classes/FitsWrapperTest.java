package de.feu.kdmp4.packagingtoolkit.mediator.wrapper.classes;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.net.URL;

import org.junit.Test;

import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.WrapperWithFile;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.classes.ConfigurationOperationsPropertiesImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;

/**
 * Tests the wrapper for the meta data extractor FITS.
 * @author Christopher Olbertz
 *
 */
public class FitsWrapperTest {
	private static final String TEST_FILE_FATIMA = "/FAtiMA.pdf";
	private ConfigurationOperations configurationOperations;
	/**
	 * The path where FITS is installed on the development machine. If the test does not work, please
	 * change this path.
	 */
	private static final String FITS_PATH = "/opt/fits-1.0.3";
	
	/**
	 * Requests the meta data of a file from FITS and checks if the correct meta data has been extracted.
	 */
	@Test
	public void testCollectDataFromFile_WithFile_Fatima() {
		configurationOperations = mock(ConfigurationOperationsPropertiesImpl.class);
		//when(configurationOperations.getDataSourceUrl("fits")).thenReturn(FITS_PATH);
		WrapperWithFile fitsWrapper = new FitsWrapperImpl(configurationOperations);
		URL urlOfFatima = FitsWrapperTest.class.getResource(TEST_FILE_FATIMA);
		File fatimaFile = new File(urlOfFatima.getFile());
		MediatorDataCollection mediatorDataCollection = fitsWrapper.collectDataFromFile(fatimaFile);
		
		int actualSizeOfCollection = mediatorDataCollection.getMetadataCount();
		assertThat(actualSizeOfCollection, is(equalTo(10)));
		MediatorData author = MediatorModelFactory.createMediatorData("author", "TeX");
		MediatorData pageCount = MediatorModelFactory.createMediatorData("pageCount", "12");
		MediatorData hasOutline = MediatorModelFactory.createMediatorData("hasOutline", "no");
		MediatorData hasAnnotations = MediatorModelFactory.createMediatorData("hasAnnotations", "no");
		MediatorData graphicsCount = MediatorModelFactory.createMediatorData("graphicsCount", "2");
		MediatorData font = MediatorModelFactory.createMediatorData("font", "NimbusRomNo9L-ReguItal");
		MediatorData size = MediatorModelFactory.createMediatorData("size", "292715");
		MediatorData creatingApplicationName = MediatorModelFactory.createMediatorData("creatingApplicationName", "MiKTeX pdfTeX-1.40.10/TeX");
		MediatorData lastmodified = MediatorModelFactory.createMediatorData("lastmodified", "2011:08:02 03:35:52+01:00");
		MediatorData created = MediatorModelFactory.createMediatorData("created", "2011:08:02 03:35:52+01:00");
		
		MediatorData actualAuthor = mediatorDataCollection.getMediatorData("author");
		assertNotNull(actualAuthor);
		assertThat(actualAuthor, is(equalTo(author)));
		
		MediatorData actualPageCount = mediatorDataCollection.getMediatorData("pageCount");
		assertNotNull(actualPageCount);
		assertThat(actualPageCount, is(equalTo(pageCount)));
		
		MediatorData actualHasOutline = mediatorDataCollection.getMediatorData("hasOutline");
		assertNotNull(actualPageCount);
		assertThat(actualHasOutline, is(equalTo(hasOutline)));
		
		MediatorData actualHasAnnotations = mediatorDataCollection.getMediatorData("hasAnnotations");
		assertNotNull(actualHasAnnotations);
		assertThat(actualHasAnnotations, is(equalTo(hasAnnotations)));
		
		MediatorData actualGraphicsCount = mediatorDataCollection.getMediatorData("graphicsCount");
		assertNotNull(actualGraphicsCount);
		assertThat(actualGraphicsCount, is(equalTo(graphicsCount)));
		
		MediatorData actualFont = mediatorDataCollection.getMediatorData("font");
		assertNotNull(actualFont);
		assertThat(actualFont, is(equalTo(font)));
		
		MediatorData actualSize = mediatorDataCollection.getMediatorData("size");
		assertNotNull(actualSize);
		assertThat(actualSize, is(equalTo(size)));
		
		MediatorData actualCreatingApplicationName = mediatorDataCollection.getMediatorData("creatingApplicationName");
		assertNotNull(actualCreatingApplicationName);
		assertThat(actualCreatingApplicationName, is(equalTo(creatingApplicationName)));
		
		MediatorData actualLastmodified = mediatorDataCollection.getMediatorData("lastmodified");
		assertNotNull(actualLastmodified);
		assertThat(actualLastmodified, is(equalTo(lastmodified)));
		
		MediatorData actualCreated = mediatorDataCollection.getMediatorData("created");
		assertNotNull(actualCreated);
		assertThat(actualCreated, is(equalTo(created)));
	}

}
