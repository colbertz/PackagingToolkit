/**
 * Contains the implementation of the model classes for the mediator.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;