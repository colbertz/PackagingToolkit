package de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces;

import de.feu.kdmp4.packagingtoolkit.mediator.enums.DataSources;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;

/**
 * Contains the operations for configuring the server.
 * @author Christopher Olbertz
 *
 */
public interface ConfigurationOperations {
	/**
	 * Gets the url of a data source on the local file system of the mediator. The url can be a url in the internet
	 * that is used to call a web service or it can be a path on the local file system of the mediator. 
	 * @param dataSource Identifies the data source and contains the information necessary for identifying its 
	 * properties in the configuration file.
	 * @return The url of the data source.
	 */
	String getDataSourceUrl(final DataSources dataSource);
	/**
	 * Configures the tools of a data source. In the configuration file there are two keys, one for the activated and
	 * one for the deactivated tools. The data source collection contains both activated and deactivated tools and
	 * the method writes them as comma separated list in the correct property key. 
	 * @param dataSource The data source whose tools should be configured.
	 * of FITS is called fits.tools.activated.
	 * @param toolsOfDataSource The tools of this data source. Contains the activated and the deactivated tools.
	 */
	void configureToolsOfDataSource(final DataSources dataSource, final DataSourceToolCollection toolsOfDataSource);
	/**
	 * Gets the activated and deactivated tools of a data source from the configuration file. 
	 * @param dataSource Identifies the data source and contains the information necessary for identifying its 
	 * properties in the configuration file.
	 * @return This collection contains the activated and the deactivated tools of a data source.
	 */
	DataSourceToolCollection getToolsOfDataSource(final DataSources dataSource);
	/**
	 * Checks the installation path of a data source. 
	 * @param installationPath The path that has to be checked.
	 * @param dataSource The data source with the key of the data source.
	 */
	void checkDataSourceInstallations(final String installationPath, final DataSources dataSource);
}
 