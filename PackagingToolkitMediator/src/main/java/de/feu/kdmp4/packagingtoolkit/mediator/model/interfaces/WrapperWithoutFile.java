package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

public interface WrapperWithoutFile {
	public MediatorDataCollection collectData();
}
