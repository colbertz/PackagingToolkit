package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

import java.util.List;

public interface MediatorDataCollection {
	/**
	 * Counts the data elements stored in the collection.
	 * @return The number of stored data elements.
	 */
	int getMetadataCount();
	/**
	 * Adds a new data element to the collection.
	 * @param mediatorData The new data element.
	 */
	void addMediatorData(MediatorData mediatorData);
	/**
	 * Gets a data element from the collection identified by its name.
	 * @param name The name of the desired element.
	 * @return The mediator data element that has been found in the collection.
	 */
	MediatorData getMediatorData(String name);
	/**
	 * Returns true if the collection is not empty. 
	 * @return True if the collection does not contain any elements.
	 */
	boolean isEmpty();
	/**
	 * Writes the elements in this collection in a list.
	 * @return The list with the elements in this collection.
	 */
	List<MediatorData> toList();
	/**
	 * Adds another MediatorDataCollection to this one.
	 * @param mediatorDataCollection The collection whose elements should be inserted in this
	 * collection.
	 */
	void addMediatorData(MediatorDataCollection mediatorDataCollection);
}
