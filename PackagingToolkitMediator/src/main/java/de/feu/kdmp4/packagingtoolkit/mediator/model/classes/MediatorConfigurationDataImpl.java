package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorConfigurationData;

// DELETE_ME
public class MediatorConfigurationDataImpl implements MediatorConfigurationData {
	/**
	 * The path where FITS is installed on the mediator. This directory must contain the directories
	 * lib and xml.
	 */
	private String fitsPathOnMediator;
	
	@Override
	public String getFitsPathOnMediator() {
		return fitsPathOnMediator;
	}
	
	@Override
	public void setFitsPathOnMediator(String fitsPathOnMediator) {
		this.fitsPathOnMediator = fitsPathOnMediator;
	}
}
