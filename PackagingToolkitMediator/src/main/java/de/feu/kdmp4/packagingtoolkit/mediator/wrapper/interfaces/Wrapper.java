package de.feu.kdmp4.packagingtoolkit.mediator.wrapper.interfaces;

import de.feu.kdmp4.packagingtoolkit.mediator.enums.DataSources;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;

public abstract class Wrapper {
	private ConfigurationOperations configurationOperations;
	private DataSources dataSource;

	public Wrapper(DataSources dataSource, ConfigurationOperations configurationOperations){
		this.dataSource = dataSource;
		this.configurationOperations = configurationOperations;
	}
	
	public void setConfigurationOperations(ConfigurationOperations configurationOperations) {
		this.configurationOperations = configurationOperations;
	}
	
	protected ConfigurationOperations getConfigurationOperations() {
		return configurationOperations;
	}

	/**
	 * Returns a name for the data source. This name can be used for example for displaying in a description.
	 * Each wrapper has to implement this method with a name for its data source.
	 * @return The name of this data source.
	 */
	public abstract String getDataSourceName();

	public abstract DataSourceToolCollection getAllDataSourceTools();
	
	public String getDataSourceKey() {
		return dataSource.getDataSourceKey();
	}
	
	public DataSources getDataSource() {
		return dataSource;
	}
}
