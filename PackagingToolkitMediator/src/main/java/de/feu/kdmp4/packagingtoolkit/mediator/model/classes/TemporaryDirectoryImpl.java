package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.TemporaryDirectory;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

public class TemporaryDirectoryImpl implements TemporaryDirectory {
	private static final String PATH_OF_TEMPORARY_DIRECTORY = "temp";
	private File temporaryDirectory;
	private Map<Uuid, File> temporaryFiles;
	
	public TemporaryDirectoryImpl(String rootDirectory) {
		temporaryDirectory = new File(rootDirectory, PATH_OF_TEMPORARY_DIRECTORY);
		if (!temporaryDirectory.exists()) {
			temporaryDirectory.mkdir();
		}
		temporaryFiles = new HashMap<>();
	}
	
	/*@Override
	public Uuid addFileToTemporaryDirectory(final byte[] fileContent, final String filename) {
		File temporaryFile = new File(temporaryDirectory, filename);
		try {
			PackagingToolkitFileUtils.writeByteArrayToFile(temporaryFile, fileContent);
			Uuid uuidOfFile = new Uuid();
			temporaryFiles.put(uuidOfFile, temporaryFile);
			return uuidOfFile;
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}*/
	
	@Override
	public Uuid addFileToTemporaryDirectory(final byte[] fileContent) {
		Uuid uuidOfFile = new Uuid();
		File temporaryFile = new File(temporaryDirectory, uuidOfFile.toString());
		try {
			PackagingToolkitFileUtils.writeByteArrayToFile(temporaryFile, fileContent);
			temporaryFiles.put(uuidOfFile, temporaryFile);
			return uuidOfFile;
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void deleteTemporaryFile(Uuid uuidOfFile) {
		File file = temporaryFiles.get(uuidOfFile);
		file.delete();
		temporaryFiles.remove(uuidOfFile);
	}
	
	@Override
	public int getFileCount() {
		return temporaryFiles.size();
	}

	@Override
	public File getFile(Uuid uuid) {
		return temporaryFiles.get(uuid);
	}
}
