package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;

public class DataSourceImpl implements DataSource {
	private String dataSourceName;
	private DataSourceToolCollection dataSourceTools;
	
	public DataSourceImpl(String dataSourceName, DataSourceToolCollection dataSourceTools) {
		this.dataSourceName = dataSourceName;
		this.dataSourceTools = dataSourceTools;
	}
	
	@Override
	public String getDataSourceName() {
		return dataSourceName;
	}
	
	@Override
	public void addDataSourceTool(DataSourceTool dataSourceTool) {
		dataSourceTools.addDataSourceToolToList(dataSourceTool);
	}

	@Override
	public DataSourceTool getDataSourceToolAt(int index) {
		return dataSourceTools.getDataSourceToolAt(index);
	}

	@Override
	public int getDataSourceToolCount() {
		return dataSourceTools.getDataSourceToolCount();
	}
	
	@Override
	public DataSourceToolCollection getDataSourceTools() {
		return dataSourceTools;
	}
}
