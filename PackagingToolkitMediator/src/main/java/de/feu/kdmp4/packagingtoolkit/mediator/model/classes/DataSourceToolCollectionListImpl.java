package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.models.abstractClasses.AbstractList;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

public class DataSourceToolCollectionListImpl extends AbstractList implements DataSourceToolCollection {
	// *************** Constructors *********************
	public DataSourceToolCollectionListImpl() {
		
	}

	/**
	 * Constructs a list with data source tools of a string list with the names of the
	 * data source tools.
	 * @param dataSourceToolNamesCollection A list with the names of the data source tools.
	 */
	public DataSourceToolCollectionListImpl(final StringList dataSourceToolNamesCollection) {
		for (int i = 0; i < dataSourceToolNamesCollection.getSize(); i++) {
			final String dataSourceToolName = dataSourceToolNamesCollection.getStringAt(i);
			final DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(dataSourceToolName);
			addDataSourceToolToList(dataSourceTool);
		}
	}
	
	//**************** Public methods *******************
	@Override
	public void addDataSourceToolToList(final DataSourceTool dataSourceTool) {
		//try {
			super.add(dataSourceTool);
/*		} catch (ListElementMayNotBeNullException e) {
			throw new ExtractorToolMayNotBeNullException(this.getClass().getName(), 
					   METHOD_ADD_EXTRACTOR_TOOL_TO_LIST, 
					   PackagingToolkitComponent.MEDIATOR);
		}*/
	}
	
	@Override
	public boolean containsDataSourceTool(DataSourceTool dataSourceTool) {
		return super.contains(dataSourceTool); 
	}
	
	@Override
	public DataSourceTool getDataSourceToolAt(final int index) {
		//try {
			DataSourceTool dataSourceTool = (DataSourceToolImpl)super.getElement(index);
			return dataSourceTool;
		/*} catch (IndexNotInRangeException e) {
			throw new ListIndexInvalidException(index, this.getClass().getName(), 
					   METHOD_GET_EXTRACTOR_TOOL_AT, 
					   PackagingToolkitComponent.MEDIATOR);
		}*/
	}
	
	@Override
	public int getDataSourceToolCount() {
		return super.getSize();
	}
	
	@Override
	public void removeDataSourceTool(final DataSourceTool dataSourceTool) {
		//try {
			super.remove(dataSourceTool);
		/*} catch (ListElementMayNotBeNullException e) {
			throw new ExtractorToolMayNotBeNullException(this.getClass().getName(), 
					METHODE_DELETE_EXTRACTOR_TOOL, 
					PackagingToolkitComponent.MEDIATOR);
		}*/
	}
	
	/*@Override
	public ExtractorToolListOutput toOutput() {
		ExtractorToolListOutput extractorToolListOutput = 
				PackagingToolkitModelFactory.getExtractorToolList();
		
		for (int i = 0; i < getExtractorToolCount(); i++) {
			ExtractorTool extractorTool = (ExtractorTool)getElement(i);
			ExtractorToolOutput extractorToolOutput = extractorTool.toOutput();
			extractorToolListOutput.addExtractorToolToList(extractorToolOutput);
		}
		
		return extractorToolListOutput;
	}*/
}