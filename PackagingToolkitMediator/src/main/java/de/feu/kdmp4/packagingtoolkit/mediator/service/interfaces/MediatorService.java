package de.feu.kdmp4.packagingtoolkit.mediator.service.interfaces;

import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.TemporaryDirectory;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;

/**
 * An interface for getting data from data sources.
 * @author Christopher Olbertz
 *
 */
public interface MediatorService {
	/**
	 * Extracts the meta data of a file and returns a collection with the extracted meta data.
	 * @param fileData The conent of the file whose meta data should be extracted.
	 * @return The collection that contains the extracted meta data.
	 */
	MediatorDataListResponse getDataFromDataSources(final byte[] fileData);
	/**
	 * Is called if the data source gets its data in another way than extracting from a file.
	 * @return
	 */
	MediatorDataListResponse getDataFromDataSources();
	/**
	 * Registers all data sources in the mediator. In a class that is implementing this interface, this
	 * method should be called in the constructor of a class or a similar method. 
	 */
	void registerDataSources();
	/**
	 * Looks for all data sources that are registered on the mediator.
	 * @return A list with all data sources.
	 */
	DataSourceListResponse findAllDataSources();
	void setConfigurationOperations(ConfigurationOperations configurationOperations);
	void setTemporaryDirectory(TemporaryDirectory temporaryDirectory);
	/**
	 * Checks the installation directories of all data sources. 
	 * @throws MediatorConfigurationException if the installation directory of a data source is not correct.
	 */
	void checkDatasourceInstallations();
	/**
	 * Saves the configuration of the data sources.
	 * @param dataSourceListResponse The list with the data source configuration that should be saved.
	 */
	void configureDataSources(DataSourceListResponse dataSourceListResponse);
}
