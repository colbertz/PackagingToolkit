/**
 * Contains the exceptions for the mediator and all related classes.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.mediator.exceptions;