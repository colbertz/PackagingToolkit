package de.feu.kdmp4.packagingtoolkit.mediator.service.classes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import de.feu.kdmp4.packagingtoolkit.mediator.enums.DataSources;
import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorResponseFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.TemporaryDirectory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.WrapperWithFile;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.WrapperWithoutFile;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.mediator.service.interfaces.MediatorService;
import de.feu.kdmp4.packagingtoolkit.mediator.wrapper.classes.FitsWrapperImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.wrapper.interfaces.Wrapper;
import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;

public class MediatorServiceImpl implements MediatorService {
	/**
	 * Contains the data sources that are registered in the mediator. The collection contains
	 * descriptions of the data sources that can for example be used to send them to the client
	 * and show them on the screen. A data source can contain several tools. These tools can be 
	 * determined with the help of this collection.  
	 */
	private DataSourceCollection availableDataSources;
	/**
	 * Contains the wrappers for the data sources that can be used with the mediator. Every
	 * data source is encapsulated by a specific wrapper.
	 */
	private List<Wrapper> wrappers;
	/**
	 * Allows the access to the method that are processing the configuration property file.
	 */
	private ConfigurationOperations configurationOperations;
	
	private TemporaryDirectory temporaryDirectory;
	
	@Override
	@PostConstruct
	public void registerDataSources() {
		createWrappers();
		createDataSources();
	}
	
	/**
	 * Creates the wrappers. This is the only method where modifications are necessary if a new wrapper
	 * should be added to the mediator. In this case, the new wrapper has to be added to the wrapper list. 
	 */
	private void createWrappers() {
		wrappers = new ArrayList<>();
		registerFitsDataSource();
	}
	
	/**
	 * Registers FITS as data source in the mediator. 
	 */
	private void registerFitsDataSource() {
		Wrapper fitsWrapper = new FitsWrapperImpl(configurationOperations);
		wrappers.add(fitsWrapper );
	}
	
	/**
	 * Creates data source objects of the wrappers for sending to the client.
	 */
	private void createDataSources() {
		availableDataSources = MediatorModelFactory.createEmptyDataSourceCollection();
		
		for (Wrapper wrapper: wrappers) {
			String dataSourceName = wrapper.getDataSourceName();
			DataSourceToolCollection dataSourceTools = wrapper.getAllDataSourceTools();
			DataSource dataSource = MediatorModelFactory.createDataSource(dataSourceName, dataSourceTools);
			availableDataSources.addDataSource(dataSource);
		}
	}
	
	@Override
	public MediatorDataListResponse getDataFromDataSources(final byte[] fileData) {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		Uuid uuidOfFile = temporaryDirectory.addFileToTemporaryDirectory(fileData);
		
		for (Wrapper wrapper: wrappers) {
			if (wrapper instanceof WrapperWithFile) {
				WrapperWithFile wrapperWithFile = (WrapperWithFile)wrapper;
				File file = temporaryDirectory.getFile(uuidOfFile);
				MediatorDataCollection mediatorDataOfFile = wrapperWithFile.collectDataFromFile(file);
				mediatorDataCollection.addMediatorData(mediatorDataOfFile);
			}
		}
		
		MediatorDataListResponse mediatorDataListResponse = MediatorResponseFactory.createMediatorDataListResponse(mediatorDataCollection);
		return mediatorDataListResponse;
	}
	
	@Override
	public void checkDatasourceInstallations() {
		for (Wrapper wrapper: wrappers) {
			DataSources dataSource = wrapper.getDataSource();
			String installationDirectory = configurationOperations.getDataSourceUrl(dataSource);
			configurationOperations.checkDataSourceInstallations(installationDirectory, dataSource);
		}
	}
	
	@Override
	public MediatorDataListResponse getDataFromDataSources() {
		MediatorDataCollection mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		
		for (Wrapper wrapper: wrappers) {
			if (wrapper instanceof WrapperWithoutFile) {
				WrapperWithoutFile wrapperWithFile = (WrapperWithoutFile)wrapper;
				MediatorDataCollection mediatorDataOfFile = wrapperWithFile.collectData();
				mediatorDataCollection.addMediatorData(mediatorDataOfFile);
			}
		}
		
		MediatorDataListResponse mediatorDataListResponse = MediatorResponseFactory.createMediatorDataListResponse(mediatorDataCollection);
		return mediatorDataListResponse;
	}
	
	@Override
	public DataSourceListResponse findAllDataSources() {
		createDataSources();
		DataSourceListResponse dataSourceListResponse = MediatorResponseFactory.createDataSourceListResponse(availableDataSources);
		return dataSourceListResponse;
	}
	
	@Override
	public void setConfigurationOperations(ConfigurationOperations configurationOperations) {
		this.configurationOperations = configurationOperations;
	}
	
	@Override
	public void setTemporaryDirectory(TemporaryDirectory temporaryDirectory) {
		this.temporaryDirectory = temporaryDirectory;
	}

	@Override
	public void configureDataSources(DataSourceListResponse dataSourceListResponse) {
		List<DataSourceResponse> dataSourcesFromClient = dataSourceListResponse.getDataSources();
		
		for (DataSourceResponse dataSourceResponse: dataSourcesFromClient) {
			String dataSourceName = dataSourceResponse.getDataSourceName();
			DataSourceToolCollection dataSourceTools = MediatorModelFactory.createEmptyDataSourceToolCollection();
			DataSourceToolListResponse dataSourceToolsFromClient = dataSourceResponse.getDataSourceTools();
			
			for (int i = 0; i < dataSourceToolsFromClient.getDataSourceToolsCount(); i++) {
				DataSourceToolResponse dataSourceToolResponse = dataSourceToolsFromClient.getDataSourceTool(i);
				String toolName = dataSourceToolResponse.getDataSourceToolName();
				DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(toolName);
				dataSourceTool.setActivated(dataSourceToolResponse.isActivated());
				dataSourceTools.addDataSourceToolToList(dataSourceTool);
			}
			
			DataSources dataSources = DataSources.determineDataSource(dataSourceName);
			configurationOperations.configureToolsOfDataSource(dataSources, dataSourceTools);
		}
	}
}
