package de.feu.kdmp4.packagingtoolkit.mediator.exceptions;

import de.feu.kdmp4.packagingtoolkit.exceptions.ProgrammingException;
import de.feu.kdmp4.packagingtoolkit.i18n.I18nExceptionUtil;
import de.feu.kdmp4.packagingtoolkit.mediator.i18n.I18nMediatorExceptionUtil;

public class DataSourceException extends ProgrammingException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8207885839250341719L;

	private DataSourceException(String message) {
		super(message);
	}
	
	/**
	 * Is thrown if an empty name for the tool of a data source occured in the application.
	 * @return The newly created exception.
	 */
	public static DataSourceException createDataSourceToolNameMayNotBeEmpty() {
		String message = I18nExceptionUtil.getExtractorToolNameMayNotBeEmptyString();
		return new DataSourceException(message);
	}
	
	/**
	 * Is thrown if an empty name for a data element occured in the application.
	 * @return The newly created exception.
	 */
	public static DataSourceException createMediatorDataNameMayNotBeEmpty() {
		String message = I18nMediatorExceptionUtil.getMediatorDataNameMayNotBeEmptyString();
		return new DataSourceException(message);
	}

	/**
	 * Is thrown if an empty value for a data element occured in the application.
	 * @return The newly created exception.
	 */
	public static DataSourceException createMediatorDataValueMayNotBeEmpty() {
		String message = I18nMediatorExceptionUtil.getMediatorDataValueMayNotBeEmptyString();
		return new DataSourceException(message);
	}
}
