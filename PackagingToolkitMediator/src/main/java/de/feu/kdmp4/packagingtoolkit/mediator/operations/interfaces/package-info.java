/**
 * Contains the interfaces that describe the atomar operations of the mediator.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces;