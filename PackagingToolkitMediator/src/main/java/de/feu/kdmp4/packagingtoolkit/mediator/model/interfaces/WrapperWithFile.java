package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

import java.io.File;

public interface WrapperWithFile {
	MediatorDataCollection collectDataFromFile(File file);
}
