package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

/**
 * Represents a data source. On the mediator, every data source is encapsulated by a wrapper. This 
 * interface and its implementation are used to describe the data sources independant from their
 * wrapper. For example they can be used for sending information about the available data sources
 * and their tools to the client.
 * @author Christopher Olbertz
 *
 */
public interface DataSource {
	/**
	 * Gets all tools that are assigned to a data source. If there are no tools for this data
	 * source, an empty collection is returned.
	 * @return A collection with all tools of this data source or an empty collection.
	 */
	//DataSourceToolCollection getAllDataSourceTools();
	/**
	 * Uses the data source to extract data from a file.
	 * @param filename The name of the file the data are extracted from.
	 * @param activatedDataSourceTools A list with the tools that are activated for this data source.
	 * @param mediatorDataCollection The collection the data are inserted to. 
	 */
	//void extractData(String filename, DataSourceToolCollection activatedDataSourceTools,
		//	MediatorDataCollection mediatorDataCollection);
	/**
	 * Uses the data source to extract data. Use this method if the data are not gathered from a file but in any
	 * other way.
	 * @param mediatorDataCollection The collection the data are inserted to. 
	 */
	//void extractData(MediatorDataCollection mediatorDataCollection);
	/**
	 * Uses the data source to extract data from a file. If you call this method, all tools of the data source
	 * are activated.
	 * @param filename The name of the file the data are extracted from.
	 * @param mediatorDataCollection The collection the data are inserted to. 
	 */
	//void extractData(String filename, MediatorDataCollection mediatorDataCollection);
	/**
	 * Adds a new data source tool to this data source.
	 * @param dataSourceTool The data source tool that should be added.
	 */
	void addDataSourceTool(DataSourceTool dataSourceTool);
	/**
	 * Determines a tool at a certain index.
	 * @param index The index of the tool in the collection.
	 * @return The tool found at index.
	 */
	DataSourceTool getDataSourceToolAt(int index);
	/**
	 * Determines the number of tools in this data source.
	 * @return The number of tools.
	 */
	int getDataSourceToolCount();
	String getDataSourceName();
	DataSourceToolCollection getDataSourceTools();
}
