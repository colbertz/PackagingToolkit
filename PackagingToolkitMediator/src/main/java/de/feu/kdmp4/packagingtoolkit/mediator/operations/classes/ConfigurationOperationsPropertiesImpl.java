package de.feu.kdmp4.packagingtoolkit.mediator.operations.classes;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import de.feu.kdmp4.packagingtoolkit.exceptions.StringException;
import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.factories.PackagingToolkitModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.enums.DataSources;
import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;
import de.feu.kdmp4.packagingtoolkit.utils.StringUtils;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;
import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsUnequal;
import static de.feu.kdmp4.packagingtoolkit.validators.StringValidator.isNullOrEmpty;

/**
 * An implementation of {@link de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations} with property files.
 * @author Christopher Olbertz
 *
 */
public class ConfigurationOperationsPropertiesImpl implements ConfigurationOperations {
	private static final String METHOD_CONFIGURE_FITS_TOOLS = "void configureFitsTools(ExtractorToolList extractorToolList)";
	private static final String PATH_CONFIGURATION_FILE = "config.properties";
	
	private static final String KEY_URL = "url";
	private static final String KEY_ACTIVATED_TOOLS = "tools.activated";
	private static final String KEY_DEACTIVATED_TOOLS = "tools.deactivated";
	private static final String FITS_XML_DIRECTORY = "xml";
	private static final String FITS_LIB_DIRECTORY = "lib";
	private File configurationFile;
	
	/**
	 * Configures the configuration file with the default path: config.properties.
	 */
	public ConfigurationOperationsPropertiesImpl() {
		configurationFile = new File(PATH_CONFIGURATION_FILE);
	}
	
	/**
	 * Configures the configuration file with another path than the default path.
	 */
	public ConfigurationOperationsPropertiesImpl(final String configurationFileName) {
		configurationFile = new File(configurationFileName);
	}
	
	@Override
	public String getDataSourceUrl(final DataSources dataSource) {
		String keyForUrl = dataSource.getDataSourceKey() + "." + KEY_URL;
		
		checkPropertyFileForReading(configurationFile);
		
		try (BufferedInputStream bufferedInputStream = new BufferedInputStream
				(new FileInputStream(configurationFile))) {
			Properties properties = new Properties();
			properties.load(bufferedInputStream);
			String pathOfDataSource = properties.getProperty(keyForUrl);
			return pathOfDataSource;
		} catch (IOException ex) {
			MediatorConfigurationException exception = MediatorConfigurationException.
					createGeneralIOException(ex, PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	/*@Override
	public void writeMediatorConfigurationData(MediatorConfigurationData mediatorConfigurationData) {
		String fitsInstallationPath = mediatorConfigurationData.getFitsPathOnMediator();
		checkFitsDirectory(fitsInstallationPath);
		try {
			Properties properties = new Properties();
			properties.setProperty(keyOfActivatedTools, activatedToolsValue);
			properties.setProperty(keyOfDeactivatedTools, deactivatedToolsValue);
			OutputStream outputStream = new FileOutputStream(configurationFile);
			properties.store(outputStream, "");
		} catch (IOException ex) {
			MediatorConfigurationException exception = MediatorConfigurationException.
					createGeneralIOException(ex, PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}*/
	
	/**
	 * Checks if a path contains a FITS installation.
	 * @param fitsDirectoryPath The path that should be checked.
	 * @throws MediatorConfigurationException is thrown if the path does not contain a FITS installation.
	 */
	private void checkFitsInstallation(final String fitsDirectoryPath) {
		File fitsDirectory = new File(fitsDirectoryPath);
		File fitsXmlDirectory = new File(fitsDirectory, FITS_XML_DIRECTORY);
		File fitsLibDirectory = new File(fitsDirectory, FITS_LIB_DIRECTORY);
		
		if (!fitsLibDirectory.exists() || !fitsXmlDirectory.exists()) {
			MediatorConfigurationException exception = MediatorConfigurationException.createWrongFitsPathException(fitsDirectoryPath);
			throw exception; 
		}
	}
	
	@Override
	public void checkDataSourceInstallations(final String installationPath, final DataSources dataSource) {
		if (dataSource.isFitsDataSource()) {
			checkFitsInstallation(installationPath);
		}
	}
	
	@Override
	public void configureToolsOfDataSource(final DataSources dataSource, final DataSourceToolCollection toolsOfDataSource) {

		checkPropertyFileForWriting(configurationFile);
		StringList activatedTools = PackagingToolkitModelFactory.getStringList();
		StringList deactivatedTools = PackagingToolkitModelFactory.getStringList();
		String dataSourceKey = dataSource.getDataSourceKey();
		String keyOfActivatedTools = dataSourceKey + "." + KEY_ACTIVATED_TOOLS;
		String keyOfDeactivatedTools = dataSourceKey + "." + KEY_DEACTIVATED_TOOLS;
		
		for (int i = 0; i < toolsOfDataSource.getDataSourceToolCount(); i++) {
			DataSourceTool dataSourceTool = toolsOfDataSource.getDataSourceToolAt(i);
			String toolName = dataSourceTool.getToolName();
			
			if (dataSourceTool.isActivated()) {
				activatedTools.addStringToList(toolName);
			}	else {
				deactivatedTools.addStringToList(toolName);
			}
		}
		
		String activatedToolsValue = StringUtils.createCommaSeperatedList(activatedTools);
		String deactivatedToolsValue = StringUtils.createCommaSeperatedList(deactivatedTools);
		
		try (BufferedInputStream bufferedInputStream = new BufferedInputStream
				(new FileInputStream(configurationFile))) {
			Properties properties = new Properties();
			properties.load(bufferedInputStream);
			//String configuredDeactivatedTools = properties.getProperty(KEY_DEACTIVATED_TOOLS);
			//String configuredActivatedTools = properties.getProperty(KEY_ACTIVATED_TOOLS);
			
			/*if (isNullOrEmpty(configuredActivatedTools) || isNullOrEmpty(configuredDeactivatedTools)) {
				activatedTools = PackagingToolkitModelFactory.getStringList();
				deactivatedTools = PackagingToolkitModelFactory.getStringList();
				
				for (int i = 0; i < toolsOfDataSource.getDataSourceToolCount(); i++) {
					DataSourceTool dataSourceTool = toolsOfDataSource.getDataSourceToolAt(i);
					String toolName = dataSourceTool.getToolName();
					
					if (areStringsEqual(toolName, DEFAULT_TOOL_NAME)) {
						activatedTools.addStringToList(toolName);
					}	else {
						deactivatedTools.addStringToList(toolName);
					}
				}
				
				activatedToolsValue = StringUtils.createCommaSeperatedList(activatedTools);
				deactivatedToolsValue = StringUtils.createCommaSeperatedList(deactivatedTools);
			}*/
			
			properties.setProperty(keyOfActivatedTools, activatedToolsValue);
			properties.setProperty(keyOfDeactivatedTools, deactivatedToolsValue);
			OutputStream outputStream = new FileOutputStream(configurationFile);
			properties.store(outputStream, "");
		} catch (IOException ex) {
			MediatorConfigurationException exception = MediatorConfigurationException.
					createGeneralIOException(ex, PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	@Override
	public DataSourceToolCollection getToolsOfDataSource(final DataSources dataSource) {
		final String dataSourceKey = dataSource.getDataSourceKey();
		final String keyOfActivatedTools = dataSourceKey + "." + KEY_ACTIVATED_TOOLS;
		final String keyOfDeactivatedTools = dataSourceKey + "." + KEY_DEACTIVATED_TOOLS;
		
		final Properties properties = new Properties();
		
		checkPropertyFileForReading(configurationFile);
		
		try (final BufferedInputStream bufferedInputStream = new BufferedInputStream
				(new FileInputStream(configurationFile))) {
			properties.load(bufferedInputStream);
			final String activatedToolsValue = properties.getProperty(keyOfActivatedTools);
			final String deactivatedToolsValue = properties.getProperty(keyOfDeactivatedTools);
			
			try {
				final StringList activatedToolsList = StringUtils.splitCommaSeparatedList(activatedToolsValue);
				final StringList deactivatedToolsList = StringUtils.splitCommaSeparatedList(deactivatedToolsValue);
				final DataSourceToolCollection toolsOfDataSource = MediatorModelFactory.createEmptyDataSourceToolCollection();
				
				for (int i = 0; i < activatedToolsList.getSize(); i++) {
					final String aString = activatedToolsList.getStringAt(i);
					final DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(aString, true);
					toolsOfDataSource.addDataSourceToolToList(dataSourceTool);
				}
				
				for (int i = 0; i < deactivatedToolsList.getSize(); i++) {
					final String aString = deactivatedToolsList.getStringAt(i);
					final DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(aString, false);
					toolsOfDataSource.addDataSourceToolToList(dataSourceTool);
				}
				
				return toolsOfDataSource;
			} catch (StringException ex) {
				// Nothing has been configured so we return an empty collection.
				return MediatorModelFactory.createEmptyDataSourceToolCollection();
			}
			
		} catch (IOException ex) {
			MediatorConfigurationException exception = MediatorConfigurationException.
					createGeneralIOException(ex, PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	// *********** Private methods ***********
	private void checkPropertyFileForReading(final File propertiesFile) {
		checkPropertyFileExists(propertiesFile, METHOD_CONFIGURE_FITS_TOOLS);
		
		if (!propertiesFile.canRead()) {
			MediatorConfigurationException exception = MediatorConfigurationException.
					createNotReadableException(PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	private void checkPropertyFileForWriting(final File propertiesFile) {
		checkPropertyFileExists(propertiesFile, METHOD_CONFIGURE_FITS_TOOLS);
		
		if (!propertiesFile.canWrite()) {
			MediatorConfigurationException exception = MediatorConfigurationException.
					createNotWritableException(PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
	
	private void checkPropertyFileExists(final File propertiesFile, final String method) {
		if (!propertiesFile.exists()) {
			MediatorConfigurationException exception = MediatorConfigurationException.
				createDoesNotExistException(PATH_CONFIGURATION_FILE);
			throw exception;
		}
	}
}