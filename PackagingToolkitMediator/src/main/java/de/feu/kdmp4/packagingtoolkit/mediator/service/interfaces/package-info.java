/**
 * Contains the service interfaces for the mediator.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.mediator.service.interfaces;