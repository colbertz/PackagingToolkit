package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

import java.io.File;

import de.feu.kdmp4.packagingtoolkit.models.classes.Uuid;

public interface TemporaryDirectory {
	Uuid addFileToTemporaryDirectory(final byte[] fileContent);
	void deleteTemporaryFile(Uuid uuidOfFile);
	int getFileCount();
	File getFile(Uuid uuid);
}
