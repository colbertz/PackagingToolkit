/**
 * Contains the classes for internationalization of the mediator. 
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.mediator.i18n;