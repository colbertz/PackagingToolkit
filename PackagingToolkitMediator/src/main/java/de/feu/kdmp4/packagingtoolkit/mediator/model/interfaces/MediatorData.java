package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

public interface MediatorData {
	/**
	 * Returns the name assigned to this data element.
	 * @return The name of the element. 
	 */
	String getName();
	/**
	 * Returns the value assigned to this data element.
	 * @return The value of the element. 
	 */
	String getValue();

}
