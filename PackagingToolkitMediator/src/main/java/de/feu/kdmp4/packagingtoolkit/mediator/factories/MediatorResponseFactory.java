package de.feu.kdmp4.packagingtoolkit.mediator.factories;

import java.util.List;

import de.feu.kdmp4.packagingtoolkit.factories.ResponseModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolListResponse;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceToolResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataResponse;

public class MediatorResponseFactory {
	public static DataSourceResponse createDataSourceResponse(DataSource dataSource) {
		DataSourceResponse dataSourceResponse = ResponseModelFactory.getDataSourceResponse();
		
		DataSourceToolCollection dataSourceTools = dataSource.getDataSourceTools();
		DataSourceToolListResponse dataSourceToolListResponse = createDataSourceToolListResponse(dataSourceTools);
		String dataSourceName = dataSource.getDataSourceName();
		dataSourceResponse.setDataSourceName(dataSourceName);
		dataSourceResponse.setDataSourceTools(dataSourceToolListResponse);
		return dataSourceResponse;
	}
	
	public static DataSourceListResponse createDataSourceListResponse(DataSourceCollection dataSources) {
		DataSourceListResponse dataSourceListResponse = ResponseModelFactory.getDataSourceListResponse();
		for (int i = 0; i < dataSources.getDataSourceCount(); i++) {
			DataSource dataSource = dataSources.getDataSourceAt(i);
			DataSourceResponse dataSourceResponse = createDataSourceResponse(dataSource);
			dataSourceListResponse.addDataSource(dataSourceResponse);
		}
		
		return dataSourceListResponse;
	}
	
	public static DataSourceToolListResponse createDataSourceToolListResponse(DataSourceToolCollection dataSourceTools) {
		DataSourceToolListResponse dataSourceToolListResponse = ResponseModelFactory.getDataSourceToolListResponse();
		for (int j = 0; j < dataSourceTools.getDataSourceToolCount(); j++) {
			DataSourceTool dataSourceTool = dataSourceTools.getDataSourceToolAt(j);
			DataSourceToolResponse dataSourceToolResponse = createDataSourceToolResponse(dataSourceTool);
			dataSourceToolListResponse.addDataSourceTool(dataSourceToolResponse);
		}
		
		return dataSourceToolListResponse;
	}
	
	public static DataSourceToolResponse createDataSourceToolResponse(DataSourceTool dataSourceTool) {
		DataSourceToolResponse dataSourceToolResponse = ResponseModelFactory.getDataSourceToolResponse();
		dataSourceToolResponse.setActivated(dataSourceTool.isActivated());
		dataSourceToolResponse.setDataSourceToolName(dataSourceTool.getToolName());
		return dataSourceToolResponse;
	}
	
	public static MediatorDataResponse createMediatorDataResponse(MediatorData mediatorData) {
		MediatorDataResponse mediatorDataResponse = new MediatorDataResponse();
		mediatorDataResponse.setName(mediatorData.getName());
		mediatorDataResponse.setValue(mediatorData.getValue());
		
		return mediatorDataResponse;
	}
	
	public static MediatorDataListResponse createMediatorDataListResponse(MediatorDataCollection mediatorDataCollection) {
		MediatorDataListResponse mediatorDataListResponse = new MediatorDataListResponse();
		List<MediatorData> mediatorDataList = mediatorDataCollection.toList();
		
		for (int i = 0; i < mediatorDataList.size(); i++) {
			MediatorData mediatorData = mediatorDataList.get(i);
			MediatorDataResponse mediatorDataResponse = createMediatorDataResponse(mediatorData);
			mediatorDataListResponse.addMediatorData(mediatorDataResponse);
		}
		
		return mediatorDataListResponse;
	}
}
