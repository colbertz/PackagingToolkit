package de.feu.kdmp4.packagingtoolkit.mediator.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.feu.kdmp4.packagingtoolkit.exceptions.UserException;
import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorApiError;
import de.feu.kdmp4.packagingtoolkit.response.StringResponse;

/**
 * This class maps exception to response objects that can be send via HTTP.
 * @author Christopher Olbertz
 *
 */
public class MediatorExceptionMapper {
	/**
	 * Maps an exception to an object of {@link de.feu.kdmp4.packagingtoolkit.response.StringResponse}.
	 * The error that is created uses the code of an internal server error.
	 * @param exception The exception to map.
	 * @return The response object that contains the exception for sending via REST.
	 */
	public static final ResponseEntity<StringResponse> mapToStringResponse(UserException exception) {
		MediatorApiError mediatorApiError = createMediatorApiError(exception);
		StringResponse stringResponse = new StringResponse(mediatorApiError);
		return new ResponseEntity<StringResponse>(stringResponse, HttpStatus.OK);
	}
	
	/**
	 * Creates an api error with the code of an internal server error.
	 * @param exception The exception that should be contained in the api error.
	 * @return The created api error.
	 */
	private static final MediatorApiError createMediatorApiError(UserException exception) {
		String summary = exception.getSummary();
		String message = exception.getMessage();
		String exceptionClass = exception.getClass().getName();
		MediatorApiError mediatorApiError = new MediatorApiError(HttpStatus.INTERNAL_SERVER_ERROR, message, exceptionClass, summary);
		return mediatorApiError;
	}
}
