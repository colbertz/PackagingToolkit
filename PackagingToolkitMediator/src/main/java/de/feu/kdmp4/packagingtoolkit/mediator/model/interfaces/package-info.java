/**
 * Contains the interface for the models of the mediator.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;