package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;

/**
 * Saves the mediator data in a HashMap. The key of the map is the name of the mediator data. 
 * @author Christopher Olbertz
 *
 */
public class MediatorDataCollectionHashMapImpl implements MediatorDataCollection {
	private Map<String, MediatorData> mediatorDataMap;
	
	public MediatorDataCollectionHashMapImpl() {
		mediatorDataMap = new HashMap<>();
	}
	
	@Override
	public MediatorData getMediatorData(String name) {
		return mediatorDataMap.get(name);
	}
	
	@Override
	public void addMediatorData(MediatorData mediatorData) {
		String key = mediatorData.getName();
		mediatorDataMap.put(key, mediatorData);
	}
	
	@Override
	public void addMediatorData(MediatorDataCollection mediatorDataCollection) {
		for(MediatorData mediatorData: mediatorDataCollection.toList()) {
			addMediatorData(mediatorData);
		}
	}
	
	@Override
	public int getMetadataCount() {
		return mediatorDataMap.size();
	}

	@Override
	public boolean isEmpty() {
		if (getMetadataCount() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public List<MediatorData> toList()  {
		Collection<MediatorData> theCollection = mediatorDataMap.values();
		List<MediatorData> theList = new ArrayList<>(theCollection);
		return theList;
	}
}