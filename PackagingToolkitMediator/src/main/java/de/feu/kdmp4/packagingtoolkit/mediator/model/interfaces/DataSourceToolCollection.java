package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

public interface DataSourceToolCollection {

	void addDataSourceToolToList(DataSourceTool dataSourceTool);

	boolean containsDataSourceTool(DataSourceTool dataSourceTool);

	int getDataSourceToolCount();

	void removeDataSourceTool(DataSourceTool dataSourceTool);

	DataSourceTool getDataSourceToolAt(int index);
	boolean isEmpty();
}
