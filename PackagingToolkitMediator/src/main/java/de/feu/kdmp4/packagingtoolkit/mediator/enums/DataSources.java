package de.feu.kdmp4.packagingtoolkit.mediator.enums;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

/**
 * This enum contains the keys for the data sources. The key is the first identifier for the configuration file. 
 * <br />
 * For example: The properties of the datasource FITS all start with fits. The property for the url of fits is called
 * fits.url.  This enum helps to avoid spelling mistakes and the resulting programming errors.
 * @author Christopher Olbertz
 *
 */
public enum DataSources {
	/* The enum value TEST is only for testing purposes, because the methods cannot be tested with only one
	 * data source. If there are more than one data sources, TEST can be deleted.
	 */
	FITS("fits"), TEST("test");
	
	private String dataSourceKey;
	
	private DataSources(String dataSourcekey) {
		this.dataSourceKey = dataSourcekey;
	}
	
	public String getDataSourceKey() {
		return dataSourceKey;
	}
	
	/**
	 * Checks if the data source is a FITS data source.
	 * @return True if the data source is FITS, false otherwise.
	 */
	public boolean isFitsDataSource() {
		if (dataSourceKey.equals(FITS.dataSourceKey)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Determines the value of this enum with the help of a string.
	 * @param dataSourceName A name or description for the data source.
	 * @return The found data source.
	 */
	public static DataSources determineDataSource(String dataSourceName) {
		if (areStringsEqual(dataSourceName, FITS.dataSourceKey) ||
			 areStringsEqual(dataSourceName, "File Information Tool Set")) {
			return DataSources.FITS;
		} else {
			return null;
		}
	}
}
