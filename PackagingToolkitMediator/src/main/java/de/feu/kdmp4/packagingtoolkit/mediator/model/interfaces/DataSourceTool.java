package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

/**
 * Describes the tools a data source can contain of. If a data source contains of several tools,
 * the tools not needed can be deactivated.
 * @author Christopher Olbertz
 *
 */
public interface DataSourceTool {
	boolean isActivated();
	String getToolName();
	void activateTool();
	void deactivateTool();
	void setActivated(boolean activated);
}