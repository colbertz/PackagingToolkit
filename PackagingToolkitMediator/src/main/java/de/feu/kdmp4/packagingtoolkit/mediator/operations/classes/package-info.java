/**
 * Contains the implementations of the operations interfaces that contains the atomar 
 * operations of the mediator.
 */
/**
 * @author Christopher Olbertz
 *
 */
package de.feu.kdmp4.packagingtoolkit.mediator.operations.classes;