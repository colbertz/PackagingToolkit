package de.feu.kdmp4.packagingtoolkit.mediator.wrapper.classes;

import java.io.File;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.mediator.enums.DataSources;
import de.feu.kdmp4.packagingtoolkit.mediator.factories.MediatorModelFactory;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.WrapperWithFile;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.mediator.wrapper.interfaces.Wrapper;
import edu.harvard.hul.ois.fits.Fits;
import edu.harvard.hul.ois.fits.FitsMetadataElement;
import edu.harvard.hul.ois.fits.FitsOutput;
import edu.harvard.hul.ois.fits.exceptions.FitsConfigurationException;
import edu.harvard.hul.ois.fits.exceptions.FitsException;
import edu.harvard.hul.ois.fits.tools.Tool;
import edu.harvard.hul.ois.fits.tools.ToolBelt;

import static de.feu.kdmp4.packagingtoolkit.utils.StringUtils.areStringsEqual;

public class FitsWrapperImpl extends Wrapper implements WrapperWithFile {
	private static final String FITS_NAME = "File Information Tool Set";
	/**
	 * The name of the tool that is activated by default.
	 */
	private static final String DEFAULT_TOOL_NAME = "FileInfo";
	
	private Queue<FitsMetadataElement> fitsMetadataElementQueue;
	private Fits fits;
	private DataSourceToolCollection dataSourceToolCollection;

	public FitsWrapperImpl(ConfigurationOperations configurationOperations) {
		super(DataSources.FITS, configurationOperations);
		fitsMetadataElementQueue = new LinkedBlockingQueue<>();
		try {
			String fitsPath = configurationOperations.getDataSourceUrl(getDataSource());
			fits = new Fits(fitsPath);
			determineAllFitsTools();
		} catch (FitsConfigurationException ex) {
			MediatorConfigurationException configurationException = MediatorConfigurationException.
					createDataSourceConfigurationException(FITS_NAME, ex);
			throw configurationException;
		}
	}
	
	@Override
	public MediatorDataCollection collectDataFromFile(File file) {
		MediatorDataCollection  mediatorDataCollection = MediatorModelFactory.createEmptyMediatorDataCollection();
		DataSourceToolCollection activatedDataSourceTools = getAllDataSourceTools();
		
		try {
			FitsOutput fitsOutput = fits.examine(file);
			List<FitsMetadataElement> techMetadataElements = fitsOutput.getTechMetadataElements();
			List<FitsMetadataElement> fileInfoElements = fitsOutput.getFileInfoElements();
			
			if (techMetadataElements != null) {
				for (FitsMetadataElement fitsMetadataElement: techMetadataElements) {
					fitsMetadataElementQueue.add(fitsMetadataElement);
				}
			}
			
			if (fileInfoElements != null) {
				for (FitsMetadataElement fitsMetadataElement: fileInfoElements) {
					fitsMetadataElementQueue.add(fitsMetadataElement);
				}
			}
			
			while (!fitsMetadataElementQueue.isEmpty()) {
				FitsMetadataElement fitsMetadataElement = fitsMetadataElementQueue.poll();
				String toolName = fitsMetadataElement.getReportingToolName();
				DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(toolName);
				
				// An empty tool collections means that every tool should be considered.
				//if (activatedDataSourceTools.containsDataSourceTool(dataSourceTool) || activatedDataSourceTools.isEmpty()) {
				addMetadataElementToMap(mediatorDataCollection, fitsMetadataElement);
				//}
			}
		} catch (FitsException ex) {
			MediatorConfigurationException configurationException = MediatorConfigurationException.
					createDataSourceConfigurationException(FITS_NAME, ex);
			throw configurationException;
		}
		
		return mediatorDataCollection;
	}
	
	/**
	 * Adds a metadata element to a map. 
	 * @param metadataElementMap The map the metadata element should inserted into.
	 * @param fitsMetadataElement The metadata element that should be added to the map.
	 * 
	 */
	private void addMetadataElementToMap(MediatorDataCollection mediatorDataCollection,
										 FitsMetadataElement fitsMetadataElement) {
		String name = fitsMetadataElement.getName();
		String value = fitsMetadataElement.getValue();

		/*MetadataElement metadataElement = metadataElementMap.getMetadataElement(name);
		if (metadataElement != null) {
			metadataElement.addValue(value);
		} else {*/ // A new element is inserted into the map.
		MediatorData mediatorData  = MediatorModelFactory.createMediatorData(name, value);
		mediatorDataCollection.addMediatorData(mediatorData);		
		
		/*if (hasConflict(fitsMetadataElement)) {
			String metadataName = fitsMetadataElement.getName();
			List<FitsMetadataElement> conflictingMetadataList = fitsOutput.getMetadataElements(metadataName);*/
			
			/* The first element is the value of the first element. Is has not a conflict to itself
			   and therefore it is not inserted into the list of conflicting metadata. */
		/*	for (int i = 1; i < conflictingMetadataList.size(); i++) {
				FitsMetadataElement conflictingFitsMetadataElement = conflictingMetadataList.get(i);
				String metadataElementName = conflictingFitsMetadataElement.getName();
				String metadataElementValue = conflictingFitsMetadataElement.getValue();
				MetadataElement conflictingMetadataElement = MediatorModelFactory.getMetadataElement(metadataElementName, metadataElementValue);
 				metadataElement.addConflictingMetadataElement(conflictingMetadataElement);
 				fitsMetadataElementQueue.poll();
			}*/
	}
	
	/**
	 * Determines all tools of FITS and fills the respective collection. This method is called once during
	 * the initialization of an instance of this class.
	 */
	private void determineAllFitsTools() {
		dataSourceToolCollection = getConfigurationOperations().getToolsOfDataSource(getDataSource());

		if (!dataSourceToolCollection.isEmpty()) {
			final ToolBelt toolBelt = fits.getToolbelt();
			final List<Tool> tools = toolBelt.getTools();
			
			for (final Tool myTool: tools) {
				final String toolName = myTool.getName();
				final DataSourceTool dataSourceTool = MediatorModelFactory.createDataSourceTool(toolName);
				
				if (areStringsEqual(toolName, DEFAULT_TOOL_NAME)) {
					dataSourceTool.activateTool();
				} else {
					dataSourceTool.deactivateTool();
				}
				
				//dataSourceToolCollection.addDataSourceToolToList(dataSourceTool);
			}
			getConfigurationOperations().configureToolsOfDataSource(getDataSource(), dataSourceToolCollection);
		} /*else {
			
		}
		
		for (Tool myTool: tools) {
			String toolName = myTool.getName();
			DataSourceTool dataSourceTool = MediatorModelFactory.getDataSourceTool(toolName);
			dataSourceToolCollection.addDataSourceToolToList(dataSourceTool);
		}*/
		
		
	}
	
	@Override
	public DataSourceToolCollection getAllDataSourceTools() {
		return dataSourceToolCollection;
	}
	
	@Override
	public String getDataSourceName() {
		return FITS_NAME;
	}
}
