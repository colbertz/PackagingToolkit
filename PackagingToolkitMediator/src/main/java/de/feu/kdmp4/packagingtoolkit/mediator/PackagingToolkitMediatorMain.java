package de.feu.kdmp4.packagingtoolkit.mediator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PackagingToolkitMediatorMain {
	public static void main(String[] args) {
		SpringApplication.run(PackagingToolkitMediatorMain.class, args);
	}
}
