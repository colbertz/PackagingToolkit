package de.feu.kdmp4.packagingtoolkit.mediator.i18n;

import java.util.ResourceBundle;

/**
 * Encapsulates the resource bundles. Only the other classes in this package
 * are allowed to call the methods of this class. Therefore it is only
 * package public.
 * @author Christopher Olbertz
 *
 */
public abstract class I18nMediatorUtil {
	// **************** Constants ***************
	/**
	 * A constant with the base name of the properties files with the internationalized 
	 * error messages.
	 */
	private static final String I18N_BASENAME_EXCEPTIONS = "exceptions";
	/**
	 * A constant with the base name of the properties files with the internationalized 
	 * messages that are not error messages.
	 */
	//private static final String I18N_BASENAME_MESSAGES = "i18n/messages";
	
	// *************** Attributes **************
	/**
	 * A resource bundle for the error messages of the exceptions thrown in the
	 * application.
	 */
	private static ResourceBundle resourceBundleExceptions;
	/**
	 * A resource bundle for messages.
	 */
	private static ResourceBundle resourceBundleMessages;
	
	// ************* Initialization ************
	/**
	 * Initializes the resource bundles.
	 */
	static {
		resourceBundleExceptions = ResourceBundle.getBundle(I18N_BASENAME_EXCEPTIONS);
		//resourceBundleMessages = ResourceBundle.getBundle(I18N_BASENAME_MESSAGES);
	}

	// *********** Public methods *************
	/**
	 * Returns the resource bundle with the error messages of the exceptions.
	 * @return The resource bundle with the error messages of the exceptions.
	 */
	public static ResourceBundle getExceptionsResourceBundle() {
		return resourceBundleExceptions;
	}
	
	/**
	 * Returns the resource bundle with the messages.
	 * @return The resource bundle with the messages.
	 */
	public static ResourceBundle getMessagesResourceBundle() {
		return resourceBundleMessages;
	}
}