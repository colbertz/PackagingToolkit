package de.feu.kdmp4.packagingtoolkit.mediator.i18n;

import java.util.ResourceBundle;

public class I18nMediatorExceptionUtil {
	private static final String MEDIATOR_DATA_NAME_EMPTY = "mediator-data-name-may-not-be-empty";
	private static final String MEDIATOR_DATA_VALUE_EMPTY = "mediator-data-value-may-not-be-empty";
	
	private static ResourceBundle exceptionsResourceBundle;
	
	static {
		exceptionsResourceBundle = I18nMediatorUtil.getExceptionsResourceBundle();
	}
	
	public static final String getMediatorDataNameMayNotBeEmptyString() {
		String message = exceptionsResourceBundle.getString(MEDIATOR_DATA_NAME_EMPTY);
		return message;
	}
	
	public static final String getMediatorDataValueMayNotBeEmptyString() {
		String message = exceptionsResourceBundle.getString(MEDIATOR_DATA_VALUE_EMPTY);
		return message;
	}
}
