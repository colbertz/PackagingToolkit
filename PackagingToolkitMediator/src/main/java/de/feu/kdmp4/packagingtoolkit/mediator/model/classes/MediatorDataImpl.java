package de.feu.kdmp4.packagingtoolkit.mediator.model.classes;

import de.feu.kdmp4.packagingtoolkit.mediator.exceptions.DataSourceException;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.validators.StringValidator;

/**
 * An element that contains data gathered by a data source of the mediator. An element is
 * identified by its name and it contain a value.
 * @author Christopher Olbertz
 *
 */
public class MediatorDataImpl implements MediatorData {
	private String name;
	private String value;
	
	public MediatorDataImpl(String name, String value) {
		if (StringValidator.isNullOrEmpty(name)) {
			DataSourceException dataSourceException = DataSourceException.createMediatorDataNameMayNotBeEmpty();
			throw dataSourceException;
		}
		this.name = name;
		
		if (StringValidator.isNullOrEmpty(value)) {
			DataSourceException dataSourceException = DataSourceException.createMediatorDataValueMayNotBeEmpty();
			throw dataSourceException;
		}
		this.value = value;
	}
	
	@Override
	public String getValue() {
		return value;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediatorDataImpl other = (MediatorDataImpl) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
