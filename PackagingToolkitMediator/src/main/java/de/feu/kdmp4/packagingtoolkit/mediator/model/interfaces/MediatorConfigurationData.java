package de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces;

/**
 * Contains the information that is used for configuring the mediator.
 * @author Christopher Olbertz
 *
 */
//DELETE_ME
public interface MediatorConfigurationData {
	String getFitsPathOnMediator();
	void setFitsPathOnMediator(String fitsPathOnMediator);
}
