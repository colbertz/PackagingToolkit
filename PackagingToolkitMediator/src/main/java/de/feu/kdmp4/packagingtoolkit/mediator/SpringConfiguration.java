package de.feu.kdmp4.packagingtoolkit.mediator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.TemporaryDirectoryImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.TemporaryDirectory;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.classes.ConfigurationOperationsPropertiesImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.operations.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.mediator.service.classes.MediatorServiceImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.service.interfaces.MediatorService;
import de.feu.kdmp4.packagingtoolkit.utils.PackagingToolkitFileUtils;

/**
 * Contains the configuration for the Spring framework. Here all dependencies between the classes, Spring is
 * administrating, are defined.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class SpringConfiguration {
	@Bean
	public MediatorService mediatorService() {
		MediatorService mediatorService = new MediatorServiceImpl();
		mediatorService.setConfigurationOperations(configurationOperations());
		mediatorService.setTemporaryDirectory(temporaryDirectory());
		
		return mediatorService;
	}
	
	@Bean
	public ConfigurationOperations configurationOperations() {
		ConfigurationOperations configurationOperations = new ConfigurationOperationsPropertiesImpl();
		return configurationOperations;
	}
	
	@Bean
	public TemporaryDirectory temporaryDirectory() {
		String applicationDirectory = PackagingToolkitFileUtils.getApplicationDirectory();
		TemporaryDirectory temporaryDirectory = new TemporaryDirectoryImpl(applicationDirectory);
		return temporaryDirectory;
	}
}
