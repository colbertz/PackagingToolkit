package de.feu.kdmp4.packagingtoolkit.mediator.communication;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.feu.kdmp4.packagingtoolkit.constants.PackagingToolkitConstants;
import de.feu.kdmp4.packagingtoolkit.constants.ParamConstants;
import de.feu.kdmp4.packagingtoolkit.constants.PathConstants;
import de.feu.kdmp4.packagingtoolkit.exceptions.mediator.MediatorConfigurationException;
import de.feu.kdmp4.packagingtoolkit.mediator.exceptions.MediatorExceptionMapper;
import de.feu.kdmp4.packagingtoolkit.mediator.service.interfaces.MediatorService;
import de.feu.kdmp4.packagingtoolkit.response.DataSourceListResponse;
import de.feu.kdmp4.packagingtoolkit.response.MediatorDataListResponse;
import de.feu.kdmp4.packagingtoolkit.response.StringResponse;

/**
 * The interface for the communication between the client and the mediator. The methods are called by the client via REST.
 * @author Christopher Olbertz 
 */
@RestController
@RequestMapping(PathConstants.MEDIATOR_CLIENT_INTERFACE + "/**")
public class MediatorCommunication {
	/**
	 * This object encapsulates the business logic of  the mediator.
	 */
	@Autowired
	private MediatorService mediatorService;

	/**
	 * Gets a file and extracts data from the file. 
	 * @param file The file that contains the data that should be extracted.
	 * @return The extracted data. 
	 */
	@PostMapping(PathConstants.MEDIATOR_COLLECT_DATA_FROM_FILE)
	public ResponseEntity<MediatorDataListResponse> collectData(@RequestParam(ParamConstants.PARAM_FILE) MultipartFile file) {
		
		try {
			byte[] fileData = file.getBytes();
			MediatorDataListResponse mediatorDataListResponse = mediatorService.getDataFromDataSources(fileData);
			return new ResponseEntity<MediatorDataListResponse>(mediatorDataListResponse, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<MediatorDataListResponse>(new MediatorDataListResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Determines the data sources that are registered on the mediator.
	 * @return A list with the data sources. Eventually they contain some tools that can be activated or deactivated.
	 */
	@RequestMapping(value = PathConstants.MEDIATOR_READ_DATASOURCES, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<DataSourceListResponse> readDataSources() { 
		final DataSourceListResponse dataSources = mediatorService.findAllDataSources();
		return new ResponseEntity<DataSourceListResponse>(dataSources, HttpStatus.OK);
	} 
	
	/**
	 * Checks the installation directories of the data sources on the mediator.
	 * @throws MediatorConfigurationException is thrown if the path does not contain a FITS installation.
	 */
	@RequestMapping(value = PathConstants.MEDIATOR_CHECK_INSTALLATION_DIRECTORIES, method = RequestMethod.GET)
	public ResponseEntity<StringResponse> checkInstallationDirectories() {
		try {
			mediatorService.checkDatasourceInstallations();
			StringResponse stringResponse = new StringResponse(PackagingToolkitConstants.I_AM_HERE);
			return new ResponseEntity<StringResponse>(stringResponse, HttpStatus.OK);
		} catch (MediatorConfigurationException exception) {
			return MediatorExceptionMapper.mapToStringResponse(exception);
		}
	}
	
	/**
	 * Is called by the client to configure the data sources of the mediator.
	 * @param dataSourceListResponse The configured data sources.
	 */
	@RequestMapping(value = PathConstants.MEDIATOR_CONFIGURE_DATA_SOURCES, method = RequestMethod.POST, 
		consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> configureServer(@RequestBody final DataSourceListResponse dataSourceListResponse) {
		mediatorService.configureDataSources(dataSourceListResponse);
		return ResponseEntity.status(HttpStatus.OK).build(); 
	}
	
	/**
	 * Is used for testing if the mediator  is available. 
	 * @return A string if the mediator can send an answer.
	 */
	@RequestMapping(value = PathConstants.MEDIATOR_PING, method = RequestMethod.GET)
	public ResponseEntity<String> receivePing() { 
		return new ResponseEntity<String>(PackagingToolkitConstants.I_AM_HERE, HttpStatus.OK);
	}
}
