package de.feu.kdmp4.packagingtoolkit.mediator.factories;

import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.DataSourceCollectionListImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.DataSourceImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.DataSourceToolCollectionListImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.DataSourceToolImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.MediatorDataCollectionHashMapImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.classes.MediatorDataImpl;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSource;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceTool;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.DataSourceToolCollection;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorData;
import de.feu.kdmp4.packagingtoolkit.mediator.model.interfaces.MediatorDataCollection;
import de.feu.kdmp4.packagingtoolkit.models.classes.StringList;

/**
 * Creates the model objects for the mediator. So the objects used in the application do not depend from an
 * implementation because only interfaces are used in the application.
 * @author Christopher Olbertz
 *
 */
public abstract class MediatorModelFactory {
	/**
	 * Creates an empty collection for tools of a data source.
	 * @return The empty collection.
	 */
	public static final DataSourceToolCollection createEmptyDataSourceToolCollection() {
		return new DataSourceToolCollectionListImpl();
	}
	
	/**
	 * Creates a collection for tools of a data source. The collection is filled with the values that are contained
	 * in a StringList. This list contains the with the names of the data soource tools.
	 * @param dataSourceToolNameCollection Contains the names of data source tools.
	 * @return The new collection.
	 */
	public static final DataSourceToolCollection createDataSourceToolCollection(final StringList dataSourceToolNameCollection) {
		return new DataSourceToolCollectionListImpl(dataSourceToolNameCollection);
	}
	
	/**
	 * Creates a data source tool.
	 * @param toolName The name of the tool.
	 * @return The created tool.
	 */
	public static final DataSourceTool createDataSourceTool(final String toolName) {
		return new DataSourceToolImpl(toolName);
	}
	
	/**
	 * Creates a data source tool.
	 * @param toolName The name of the tool.
	 * @param activated True, if the tool is activated, false otherwise.
	 * @return The created tool.
	 */
	public static final DataSourceTool createDataSourceTool(final String toolName, final boolean activated) {
		return new DataSourceToolImpl(toolName, activated);
	}
	
	/**
	 * Creates a data source.
	 * @param dataSourceName The name of the data source.
	 * @param dataSourceTools The tools that are contained in the data source.
	 * @return The created data source.
	 */
	public static final DataSource createDataSource(final String dataSourceName, final DataSourceToolCollection dataSourceTools) {
		return new DataSourceImpl(dataSourceName, dataSourceTools);
	}

	/**
	 * Creates a collection for the data that are determined by the mediator.
	 * @return An empty collection. 
	 */
	public static final MediatorDataCollection createEmptyMediatorDataCollection() {
		return new MediatorDataCollectionHashMapImpl();
	}
	
	/**
	 * Creates an object for the data extracted by the mediator.
	 * @param name The name of the data object.
	 * @param value A value extracted by the mediator.
	 * @return The object that contains a value.
	 */
	public static final MediatorData createMediatorData(final String name, final String value) {
		return new MediatorDataImpl(name, value);
	}
	
	/**
	 * Creates an empty collection for data sources.
	 * @return An empty collection for data sources.
	 */
	public static final DataSourceCollection createEmptyDataSourceCollection() {
		return new DataSourceCollectionListImpl();
	}
}