package de.feu.kdmp4.packagingtoolkit.server.moduleconfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.ConfigurationOperationsPropertiesImpl;
import de.feu.kdmp4.packagingtoolkit.server.configuration.classes.ConfigurationServiceImpl;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.MetaDataConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.OntologyConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.PackagingConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.SerializationConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.StorageConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.facades.TripleStoreConfigurationFacade;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationOperations;
import de.feu.kdmp4.packagingtoolkit.server.configuration.interfaces.ConfigurationService;

/**
 * Configures the Configuration module.
 * @author Christopher Olbertz
 *
 */
@Configuration
public class ModuleConfigurationConfiguration {
	/**
	 * Creates a bean for the operations of the module with an implementation with 
	 * properties files.
	 * @return The implementing object of the interface.
	 */
	@Bean
	public ConfigurationOperations configurationOperations() {
		ConfigurationOperations configurationOperations = new ConfigurationOperationsPropertiesImpl();
		
		return configurationOperations;
	}
	
	/**
	 * Creates a bean for the business logic of the module.
	 * @return The implementing object of the interface.
	 */
	@Bean
	public ConfigurationService configurationService() {
		ConfigurationService configurationService = new ConfigurationServiceImpl();
		configurationService.setConfigurationOperations(configurationOperations());
		
		return configurationService;
	}
	
	/**
	 * Creates an object for the facade that the module Packaging can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public PackagingConfigurationFacade packagingConfigurationFacade() {
		PackagingConfigurationFacade packagingConfigurationFacade = new PackagingConfigurationFacade();
		packagingConfigurationFacade.setConfigurationService(configurationService());
		
		return packagingConfigurationFacade;
	}
	
	/**
	 * Creates an object for the facade that the module Serialization can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public SerializationConfigurationFacade serializationConfigurationFacade() {
		SerializationConfigurationFacade serializationConfigurationFacade = new SerializationConfigurationFacade();
		serializationConfigurationFacade.setConfigurationOperations(configurationOperations());
		
		return serializationConfigurationFacade;
	}
	
	/**
	 * Creates an object for the facade that the module TripleStore can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public TripleStoreConfigurationFacade tripleStoreConfigurationFacade() {
		TripleStoreConfigurationFacade tripleStoreConfigurationFacade = new TripleStoreConfigurationFacade();
		tripleStoreConfigurationFacade.setConfigurationOperations(configurationOperations());
		tripleStoreConfigurationFacade.setConfigurationService(configurationService());
		
		return tripleStoreConfigurationFacade;
	}
	
	/**
	 * Creates an object for the facade that the module Storage can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public StorageConfigurationFacade storageConfigurationFacade() {
		StorageConfigurationFacade storageConfigurationFacade = new StorageConfigurationFacade();
		storageConfigurationFacade.setConfigurationService(configurationService());
		
		return storageConfigurationFacade;
	}
	
	/**
	 * Creates an object for the facade that the module Ontology can use for accessing the methods
	 * of this module.
	 * @return The created object.
	 */
	@Bean
	public OntologyConfigurationFacade ontologyConfigurationFacade() {
		OntologyConfigurationFacade ontologyConfigurationFacade = new OntologyConfigurationFacade();
		ontologyConfigurationFacade.setConfigurationOperations(configurationOperations());
		
		return ontologyConfigurationFacade;
	}
	
	
	@Bean
	public MetaDataConfigurationFacade metaDataConfigurationFacade() {
		MetaDataConfigurationFacade metaDataConfigurationFacade = new MetaDataConfigurationFacade();
		metaDataConfigurationFacade.setConfigurationService(configurationService());
		return metaDataConfigurationFacade;
	}
}
